<?php

	if( file_exists( '../l_config.php' ) )
	{
	    $SINGLE_FILE = true;
	    require_once( '../l_config.php' );
	    require_once( '../l-functions/settings.php' );
	    require_once( '../l-classes/actions.php' );
	    require_once( '../l-functions/upload.php' );
	    require_once( '../l-functions/attachment.php' );
	    require_once( '../l-classes/directory.php' );
	    require_once( '../l-functions/user-role.php' );
	    require_once( '../l-functions/user.php' );
	    require_once( '../l-functions/paging.php' );
	    require_once( '../l-content/languages/en.php' );
	    require_once( 'admin_functions.php' );
	    require_once( '../l-classes/user_privileges.php' );
	    require_once( '../l-classes/user_app_privileges.php' );
	    require_once( '../l-functions/kses.php' );
	    require_once( '../l-functions/rewrite.php' );
	    require_once( '../l-functions/comments.php' );
	    require_once( '../l-functions/articles.php' );
	    require_once( '../l-functions/friends.php' );
	}
	elseif( file_exists( 'l_config.php' ) )
	{
	    $SINGLE_FILE = false;
	    require_once( 'l_config.php' );
	    require_once( 'l-functions/settings.php' );
	    require_once( 'l-classes/actions.php' );
	    require_once( 'l-functions/upload.php' );
	    require_once( 'l-functions/attachment.php' );
	    require_once( 'l-classes/directory.php' );
	    require_once( 'l-functions/user-role.php' );
	    require_once( 'l-functions/user.php' );
	    require_once( 'l-functions/paging.php' );
	    require_once( 'l-content/languages/en.php' );
	    require_once( 'admin_functions.php' );
	    require_once( 'l-classes/user_privileges.php' );
	    require_once( 'l-classes/user_app_privileges.php' );
	    require_once( 'l-functions/kses.php' );
	    require_once( 'l-functions/rewrite.php' );
	    require_once( 'l-functions/comments.php' );
	    require_once( 'l-functions/articles.php' );
	    require_once( 'l-functions/friends.php' );
	}

	if( !defined( 'SITE_URL' ) )
	{
	    define( 'SITE_URL', get_meta_data( 'site_url' ) );
	}

	if( is_user_logged() )
	{
	    set_timezone( get_meta_data( 'time_zone' ) );

	    if( !empty( $_POST['dash_order'] ) && !empty( $_POST['update'] ) )
	    {
	        if( !is_user_logged() )
	        {
	            header( 'location:' . get_admin_url() . '/?state=login' );
	        }
	        else
	        {
	            update_dashboard( $_POST['update'] );
	        }
	    }

	    if( !empty( $_POST['postit'] ) )
	    {	        
	        if( isset( $_POST['share_to'] ) )
	        {
	            $saveit = save_article( $_POST['status'], '', 'publish', 'status', 'allowed', '', $_POST['share_to'] );
	        }
	        else
	        {
	            $saveit = save_article( $_POST['status'], '', 'publish', 'status', 'allowed' );
	        }
	        
	        $id = $db->insert_id();

	        if( $saveit )
	        {
	            $query = $db->prepare_query( 'SELECT * FROM l_articles WHERE larticle_status="publish" AND larticle_id=%d', $id );
	            echo dashboard_update_list( $query );
	        }
	    }

	    if( !empty( $_POST['feeds_type'] ) )
	    {
	        echo more_feeds( $_POST['page'], $_POST['feeds_type'] );	        
	    }
	}

	function get_dashboard()
	{
	    global $the_function;
	    global $db;

	    if( !is_user_logged() )
	    {
	        header( 'location:' . get_admin_url() );
	    }

	    set_template( TEMPLATE_PATH . '/dashboard.html', 'dashboard' );
	    get_seats_production_dashboard();
        add_block( 'dashboard-block', 'dblock', 'dashboard' );

        add_variable( 'site_url', site_url() );
	    add_variable( 'boat_seat_panel', get_seat_availibility_by_boat( true ) );
	    add_variable( 'trip_seat_panel', get_seat_availibility_by_trip( true ) );

	    add_variable( 'latest_feedback', get_latest_feedback() );
	    add_variable( 'last_feedback_count', get_latest_feedback_count() );

	    add_variable( 'last_reservation', ticket_last_booking() );
	    add_variable( 'last_reservation_count', ticket_last_booking_count() );
	    add_variable( 'last_30_days_chart', ticket_last_30_days_chart() );
	    add_variable( 'dashboard_memo', ticket_dashboard_memo() );

	    add_variable( 'today_pickup', ticket_today_transport( 'pickup' ) );
	    add_variable( 'today_drop_off', ticket_today_transport( 'drop-off' ) );

	    add_actions( 'section_title', 'Dashboard' );
    	add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/slick.min.css' );
	    
	    parse_template('dashboard-block','dblock', false);

        return return_template('dashboard');
	}

	function dashboard_latest_update()
	{

	}
?>