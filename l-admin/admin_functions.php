<?php

add_apps_privileges( 'global_settings', 'Settings' );
add_apps_privileges( 'users', 'Profile' );

function get_admin()
{
    set_template( TEMPLATE_PATH . '/index.html' );

    add_block( 'adminArea', 'aBlock' );

    add_variable( 'web_title', web_title() );
    add_variable( 'style_sheet', get_css() );
    add_variable( 'jquery', get_javascript( 'jquery' ) );
    add_variable( 'jquery_ui', get_javascript( 'jquery_ui' ) );
    add_variable( 'navigations', get_javascript( 'navigation' ) );
    add_variable( 'navmenu', get_nav_menu() );

    if( is_dashboard() )
    {
        add_variable( 'content_area', get_dashboard() );
    }

    if( is_global_settings() )
    {
        add_variable( 'content_area', get_global_settings() );
    }

    if( is_themes() )
    {
        add_variable( 'content_area', get_themes() );
    }

    parse_template( 'adminArea', 'aBlock' );

    print_template();
}

function set_tabs( $tabs, $selected_tab )
{
    $tab = '';

    foreach( $tabs as $key => $val )
    {
        if( $selected_tab == $key )
        {
            $tab .= '<li class="active"><a href="' . get_tab_url( $key ) . '">' . $val . '</a></li>';
        }
        else
        {
            $tab .= '<li><a href="' . get_tab_url( $key ) . '">' . $val . '</a></li>';
        }
    }

    return $tab;
}

function set_date_format()
{
	$dformat = array( 'F j, Y', 'Y/m/d', 'm/d/Y', 'd/m/Y' );
    $custome = true;
    $content = '';
    $index   = 1;

    foreach( $dformat as $val )
    {
        if( $val == get_date_format() )
        {
            $checked    = 'checked="checked"';
            $the_format = $val;
            $custome    = false;
        }
        elseif( $val != get_date_format() )
        {
            $checked    = '';
            $the_format = get_date_format();
        }

        $content .= '
        <li>
        	<input type="radio" name="the_date_format" id="date_format_' . $index . '" value="' . $val . '" ' . $checked . ' />
        	<span>' . date( $val, time() ) . '<span />
	        <script type="text/javascript">
				$(function(){
					$("#date_format_' . $index . '").click(function(){
						$("[name=date_format]").val( $("#date_format_' . $index . '").val() );
					});
				});
			</script>
		</li>';

        $index++;
    }

    $content .= '
	<li>
	    <input type="radio" name="the_date_format" id="custome_date_format" value="" ' . ( $custome ? 'checked="checked"' : '' ) . ' />
		<span>Custom Format: </span>
		<input type="text" name="date_format" value="' . $the_format . '" class="text text-small form-control" /><br />
		<script type="text/javascript">
			$(function(){
				$("#custome_date_format").click(function(){
					$("[name=date_format]").focus();
				});

				$("[name=date_format]").focus(function(){
					$("#custome_date_format").attr("checked","checked");
				});
			});
		</script>
	</li>';

	return $content;
}

function set_time_format()
{
	$tformat = array( 'g:i a', 'g:i A', 'H:i' );
    $custome = true;
    $content = '';
    $index   = 1;

    foreach( $tformat as $val )
    {
        if( $val == get_time_format() )
        {
            $checked    = 'checked="checked"';
            $the_format = $val;
            $custome    = false;
        }
        else
        {
            $checked    = '';
            $the_format = get_time_format();
        }

        $content .= '
        <li>
	        <input id="time_format_' . $index . '" type="radio" name="the_time_format" value="' . $val . '" ' . $checked . ' />
	        <span>' . date( $val, time() ) . '<span />
	        <script type="text/javascript">
				$(function(){
					$("#time_format_' . $index . '").click(function(){
						$("[name=time_format]").val( $("#time_format_' . $index . '").val() );
					});
				});
			</script>
		</li>';

        $index++;
    }

    $content .= '
	<li>
	    <input type="radio" name="the_time_format" id="custome_time_format" value="" ' . ( $custome ? 'checked="checked"' : '' ) . ' />
		<span>Custom Format: </span>
		<input type="text" name="time_format" value="' .$the_format .'" class="text text-small form-control" /><br />
		<script type="text/javascript">
			$(function(){
				$("#custome_time_format").click(function(){
					$("[name=time_format]").focus();
				});

				$("[name=time_format]").focus(function(){
					$("#custome_time_format").attr("checked","checked");
				});
			});
		</script>
	</ul>';

	return $content;
}

function get_global_settings()
{
    global $actions;

    $tabs = array( 'general' => 'General', 'reading' => 'Reading', 'writing' => 'Writing' );

    if( isset( $actions->action['tab_admin_aditional'] ) )
    {
        for( $i = 0; $i < count( $actions->action['tab_admin_aditional']['args'] ); $i++ )
        {
            for( $ii = 0; $ii < count( $actions->action['tab_admin_aditional']['args'][$i] ); $ii++ )
            {
                $tabs = array_merge( $tabs, $actions->action['tab_admin_aditional']['args'][$i][$ii] );
            }
        }
    }

    $the_tab = empty( $_GET['tab'] ) ? 'general' : $_GET['tab'];
    $update  = empty( $_POST['update'] ) ? false : $_POST['update'];

    set_template( TEMPLATE_PATH . '/global.html', 'global' );

    add_block( 'genSetting', 'settingBlock', 'global' );
    add_block( 'readingSettings', 'readingBlock', 'global' );
    add_block( 'writingSettings', 'writingBlock', 'global' );

    add_variable( 'tab', set_tabs( $tabs, $the_tab ) );

    if( $the_tab == 'general' )
    {
        add_actions( 'section_title', 'General - Settings' );

        $invite_limit = get_meta_data( 'invitation_limit', 'global_setting' );
        $invite_limit = empty( $invite_limit ) ? 10 : $invite_limit;

        if( $update )
        {
            if( update_global_settings() )
            {
                add_variable( 'alert', generate_message_block( array( 'type'=> 'success', 'content' => array( UPDATE_SUCCESS ) ) ) );
            }
            else
            {
                add_variable( 'alert', generate_message_block( array( 'type'=> 'error', 'content' => array( UPDATE_FAILED ) ) ) );
            }
        }

        set_timezone( get_meta_data( 'time_zone' ) );

        add_variable( 'smtp', get_smtp() );
        add_variable( 'email', get_email() );
        add_variable( 'web_name', web_name() );
        add_variable( 'web_title', web_title() );
        add_variable( 'tagline', web_tagline() );
        add_variable( 'site_url', HTSERVER . site_url() );
        add_variable( 'utc_time', date( 'Y/m/d H:i', time() ) );
        add_variable( 'local_time', date( 'Y/m/d H:i', time() ) );
        add_variable( 'timezone', get_timezone( get_meta_data( 'time_zone' ) ) );
        add_variable( 'date_format', set_date_format() );
        add_variable( 'time_format', set_time_format() );
        add_variable( 'invitation_limit', $invite_limit );

        add_variable( 'email_reservation', get_meta_data( 'email_reservation' ) );
        add_variable( 'email_reservation_2', get_meta_data( 'email_reservation_2' ) );
        add_variable( 'email_feedback_invotation', get_meta_data( 'email_feedback_invotation' ) );
        add_variable( 'email_feedback_submited', get_meta_data( 'email_feedback_submited' ) );
        add_variable( 'smtp_email_port', get_meta_data( 'smtp_email_port' ) );
        add_variable( 'smtp_email_address', get_meta_data( 'smtp_email_address' ) );
        add_variable( 'smtp_email_pwd', get_meta_data( 'smtp_email_pwd' ) );
        add_variable( 'r_public_key', get_meta_data( 'r_public_key' ) );
        add_variable( 'r_secret_key', get_meta_data( 'r_secret_key' ) );
        // require baris baru feedback_time di l_meta_data , lapp_name = global_setting
        add_variable( 'feedback_time', get_meta_data( 'feedback_time' ) );


        $fopt = get_meta_data( 'feedback_link_opt');
        $ccpg = get_meta_data( 'cc_payment_gateway_opt');

        add_variable( 'feedback_link_css', $fopt == 0 ? 'sr-only' : '' );
        add_variable( 'feedback_link', get_meta_data( 'feedback_link' ) );
        add_variable( 'feedback_link_opt', get_feedback_link_opt( $fopt ) );
        add_variable( 'cc_payment_gateway_opt', get_cc_payment_gateway_opt( $ccpg ) );

        parse_template( 'genSetting', 'settingBlock' );

        return return_template( 'global' );
    }
    elseif( $the_tab == 'reading' )
    {
        add_actions( 'section_title', 'Reading - Settings' );

        if( $update )
        {
            if( update_global_settings() )
            {
                if( $_POST['is_rewrite'] == 'yes' )
                {
                    create_htaccess_file();
                }
                else
                {
                    if( file_exists( ROOT_PATH . '/.htaccess' ) )
                    {
                        unlink( ROOT_PATH . '/.htaccess' );
                    }
                }

                add_variable( 'alert', generate_message_block( array( 'type'=> 'success', 'content' => array( UPDATE_SUCCESS ) ) ) );
            }
            else
            {
                add_variable( 'alert', generate_message_block( array( 'type'=> 'error', 'content' => array( UPDATE_FAILED ) ) ) );
            }
        }

        $rss_view_format = array( 'full_text' => 'Full Text', 'summary' => 'Summary' );
        $sef_format      = array( 'yes' => 'Yes', 'no' => 'No' );
        $opt_rss_format  = '';
        $opt_sef_format  = '';

        foreach( $rss_view_format as $key => $val )
        {
            $opt_rss_format .= '
        	<li>
        		<input type="radio"  name="rss_view_format" value="' . $key . '" ' . ( $key == rss_view_format() ? 'checked="checked"' : '' ) . ' />
        		<span>' . $val . '</span>
        	<li />';
        }

        foreach( $sef_format as $key => $val )
        {
            $opt_sef_format .= '
            <li>
            	<input type="radio"  name="is_rewrite" value="' . $key . '" ' . ( $key == is_rewrite() ? 'checked="checked"' : '' ) . ' />
        		<span>' . $val . '</span>
            <li />';
        }

        add_variable( 'post_viewed', post_viewed() );
        add_variable( 'rss_viewed', rss_viewed() );
        add_variable( 'status_viewed', status_viewed() );

        add_variable( 'sef_format', $opt_sef_format );
        add_variable( 'rss_view_format', $opt_rss_format );

        parse_template( 'readingSettings', 'readingBlock' );

        return return_template( 'global' );
    }
    elseif( $the_tab == 'writing' )
    {
        add_actions( 'section_title', 'Writing - Settings' );

        if( $update )
        {
            if( update_global_settings() )
            {
                add_variable( 'alert', generate_message_block( array( 'type'=> 'success', 'content' => array( UPDATE_SUCCESS ) ) ) );
            }
            else
            {
                add_variable( 'alert', generate_message_block( array( 'type'=> 'error', 'content' => array( UPDATE_FAILED ) ) ) );
            }
        }

        $email_format  = array( 'html' => 'HTML', 'plain_text' => 'Plain Text' );
        $text_editor   = array( 'tiny_mce' => 'Tiny MCE', 'none' => 'None' );
        $op_email      = '';
        $op_txt_editor = '';

        foreach( $email_format as $key => $val )
        {
            $op_email .= '
        	<li>
        		<input type="radio" name="email_format" value="' . $key .'" ' . ( $key == email_format() ? 'checked="checked"' : '' ) . ' />
        		<span>' . $val . '</span>
        	<li/>';
        }

        foreach( $text_editor as $key => $val )
        {
            $op_txt_editor .= '
        	<li>
        		<input type="radio" name="text_editor" value=" ' . $key . '" ' . ( $key == text_editor() ? 'checked="checked"' : '' ) . ' />
        		<span>' . $val . '</span>
        	<li/>';
        }

        add_variable( 'email_format', $op_email );
        add_variable( 'text_editor', $op_txt_editor );

        add_variable( 'thumbnail_image_width', thumbnail_image_width() );
        add_variable( 'thumbnail_image_height', thumbnail_image_height() );
        add_variable( 'medium_image_height', medium_image_height() );
        add_variable( 'medium_image_width', medium_image_width() );
        add_variable( 'large_image_height', large_image_height() );
        add_variable( 'large_image_width', large_image_width() );
        add_variable( 'list_viewed', list_viewed() );

        parse_template( 'writingSettings', 'writingBlock' );

        return return_template( 'global' );
    }
    else
    {
        if( isset( $actions->action['tab_admin_aditional']) )
        {
            for( $i=0; $i < count( $actions->action['tab_admin_aditional']['args'] ); $i++ )
            {
                for( $ii=0; $ii < count( $actions->action['tab_admin_aditional']['args'][$i] ); $ii++ )
                {
                    $tabs = $actions->action['tab_admin_aditional']['args'][$i][$ii];

                    foreach( $tabs as $key=>$val )
                    {
                        if( $_GET['tab']==$key )
                        {
                            $title = $val;

                            add_actions('section_title',$title);

                            if( function_exists( $actions->action['tab_admin_aditional']['func_name'][$i] ) )
                            {
                                $lbl  = 'tab_admin_aditional';
                                $j    = $i;
                                $func = is_array( $actions->action[$lbl]['args'][$j] ) ? $actions->action[$lbl]['args'][$j] : array( $actions->action[$lbl]['args'][$j] );

                                return call_user_func_array( $actions->action[$lbl]['func_name'][$j], $func );
                            }

                            break;
                        }
                    }
                }
            }
        }
    }
}

function update_global_settings()
{
    $thumbnail_image_width  = '';
    $thumbnail_image_height = '';
    $medium_image_width     = '';
    $medium_image_height    = '';
    $large_image_width      = '';
    $large_image_height     = '';
    $thumbnail_image_size   = '';
    $medium_image_size      = '';
    $large_image_size       = '';

    if( isset( $_POST['thumbnail_image_width'] ) )
    {
        if( $_POST['thumbnail_image_width'] == "" || !isInteger( $_POST['thumbnail_image_width'] ) )
        {
            $_POST['thumbnail_image_width'] = thumbnail_image_width();
        }
    }

    if( isset( $_POST['thumbnail_image_height'] ) )
    {
        if( $_POST['thumbnail_image_height'] == "" || !isInteger( $_POST['thumbnail_image_height'] ) )
        {
            $_POST['thumbnail_image_height'] = thumbnail_image_height();
        }
    }

    if( isset( $_POST['medium_image_width'] ) )
    {
        if( $_POST['medium_image_width'] == "" || !isInteger( $_POST['medium_image_width'] ) )
        {
            $_POST['medium_image_width'] = medium_image_width();
        }
    }

    if( isset( $_POST['medium_image_height'] ) )
    {
        if( $_POST['medium_image_height'] == "" || !isInteger( $_POST['medium_image_height'] ) )
        {
            $_POST['medium_image_height'] = medium_image_height();
        }
    }

    if( isset( $_POST['large_image_width'] ) )
    {
        if( $_POST['large_image_width'] == "" || !isInteger( $_POST['large_image_width'] ) )
        {
            $_POST['large_image_width'] = large_image_width();
        }
    }

    if( isset( $_POST['large_image_height'] ) )
    {
        if( $_POST['large_image_height'] == "" || !isInteger( $_POST['large_image_height'] ) )
        {
            $_POST['large_image_height'] = large_image_height();
        }
    }

    if( isset( $_POST['thumbnail_image_width'] ) && isset( $_POST['thumbnail_image_height'] ) )
    {
        $update = update_meta_data( 'thumbnail_image_size', $_POST['thumbnail_image_width'] . ':' . $_POST['thumbnail_image_height'] );
    }

    if( isset( $_POST['medium_image_width'] ) && isset( $_POST['medium_image_height'] ) )
    {
        $update = update_meta_data( 'medium_image_size', $_POST['medium_image_width'] . ':' . $_POST['medium_image_height'] );
    }

    if( isset( $_POST['large_image_width'] ) && isset( $_POST['large_image_height'] ) )
    {
        if( $large_image_size != ':' )
        {
            $update = update_meta_data( 'large_image_size', $_POST['large_image_width'] . ':' . $_POST['large_image_height'] );
        }
    }

    foreach( $_POST as $key => $val )
    {
        if( $key != 'feedback_link' )
        {
            $val = str_replace( HTSERVER, '', $val );
        }

        if( $key == 'post_viewed' || $key == 'rss_viewed' || $key == 'status_viewed' )
        {
            if( $val == '' || !isInteger( $val ) )
            {
                $val = 10;
            }
        }
        elseif( $key == 'list_viewed' )
        {
            if( $val == '' || !isInteger( $val ) )
            {
                $val = 30;
            }
        }

        $update = update_meta_data( $key, $val );
    }

    if( $update )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function set_orderby()
{
    if( isset( $_GET['order'] ) && $_GET['order'] == 'asc' )
    {
        return 'desc';
    }
    else
    {
        return 'asc';
    }
}

function set_sortable( $field )
{
    if( isset( $_GET['orderby'] ) && $_GET['orderby'] == $field )
    {
        return 'sortable';
    }
    else
    {
        return '';
    }
}

function is_dashboard()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'dashboard' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_global_settings()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'global_settings' && is_grant_app( 'global_settings' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_personal_settings()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'personal-settings' && is_grant_app( 'personal-settings' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_application()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'applications' && is_grant_app( 'applications' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_plugin()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'plugins' && is_grant_app( 'plugins' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_themes()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'themes' && is_grant_app( 'themes' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_comment()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'comments' && is_grant_app( 'comments' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_article()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'articles' && is_grant_app( 'articles' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_page()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'pages' && is_grant_app( 'pages' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_user()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'users' && is_grant_app( 'users' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_profile()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'my-profile' && isset( $_GET['tab'] ) && $_GET['tab'] == 'my-profile' )
    {
        return true;
    }

    if( ( !empty( $_GET['state'] ) && $_GET['state'] == 'users' && isset( $_GET['tab'] ) && $_GET['tab'] == 'my-profile' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_user_updates()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'my-profile' )
    {
        return true;
    }

    if( ( !empty( $_GET['state'] ) && $_GET['state'] == 'my-profile' ) && isset( $_GET['tab'] ) && $_GET['tab'] == 'my-updates' )
    {
        return true;
    }

    if( ( !empty( $_GET['state'] ) && $_GET['state'] == 'users' && isset( $_GET['tab'] ) && $_GET['tab'] == 'my-updates' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_profile_picture()
{
    if( ( !empty( $_GET['state'] ) && $_GET['state'] == 'my-profile' ) && isset( $_GET['tab'] ) && $_GET['tab'] == 'profile-picture' )
    {
        return true;
    }

    if( ( !empty( $_GET['state'] ) && $_GET['state'] == 'users' && isset( $_GET['tab'] ) && $_GET['tab'] == 'profile-picture' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_profile_eduwork()
{
    if( ( !empty( $_GET['state'] ) && $_GET['state'] == 'my-profile' ) && isset( $_GET['tab'] ) && $_GET['tab'] == 'eduwork' )
    {
        return true;
    }

    if( ( !empty( $_GET['state'] ) && $_GET['state'] == 'users' && isset( $_GET['tab'] ) && $_GET['tab'] == 'eduwork' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_logout()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'logout' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_state( $state )
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == $state && is_grant_app( $state ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_add_new()
{
    if( ( ( isset( $_GET['prc'] ) && $_GET['prc'] == 'add_new' ) || isset( $_POST['add_new'] ) ) && allow_action( $_GET['state'], 'insert' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_detail()
{
    if( ( isset( $_GET['prc'] ) && $_GET['prc'] == 'detail' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_reschedule()
{
    if( ( isset( $_GET['prc'] ) && $_GET['prc'] == 'reschedule' && allow_action( $_GET['state'], 'update' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_edit()
{
    if( ( isset( $_GET['prc'] ) && $_GET['prc'] == 'edit' && allow_action( $_GET['state'], 'update' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_edit_all()
{
    if( ( isset( $_POST['edit'] ) && $_POST['edit'] == 'Edit' && allow_action( $_GET['state'], 'update' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_edit_all_comment()
{
    if( ( isset( $_POST['edit'] ) && $_POST['edit'] == 'Edit Comments' && allow_action( $_GET['state'], 'update' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_query_error()
{
    if( ( isset( $_POST['prc'] ) && $_POST['prc'] == 'error-query' && allow_action( $_GET['state'], 'error-query' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_delete( $app_name )
{
    if( ( isset( $_POST['prc'] ) && $_POST['prc'] == 'delete' && allow_action( $app_name, 'delete' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_delete_all()
{
    if( ( isset( $_POST['delete'] ) && $_POST['delete'] == 'Delete' && allow_action( $_GET['state'], 'delete' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_confirm_delete()
{
    if( ( isset( $_POST['confirm_delete'] ) && $_POST['confirm_delete'] == 'Yes' && allow_action( $_GET['state'], 'delete' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_search()
{
    if( ( isset( $_POST['prc'] ) && $_POST['prc'] == 'search' ) )
    {
        return true;
    }

    if( ( isset( $_POST['search'] ) && $_POST['search'] == 'yes' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_orderby()
{
    if( ( isset( $_GET['orderby'] ) && !empty( $_GET['orderby'] ) ) && ( isset( $_GET['order'] ) && !empty( $_GET['order'] ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_save_changes( $state = '' )
{
    if( empty( $state ) )
    {
        if( isset( $_POST['save_changes'] ) && ( allow_action( $_GET['state'], 'update' ) || allow_action( $_GET['state'], 'insert' ) || allow_action( $_GET['state'], 'approve' ) ) )
        {
            return true;
        }
	    else
	    {
	        return false;
	    }
    }
    else
    {
        if( isset( $_POST['save_changes'] ) && ( allow_action( $state, 'update' ) || allow_action( $state, 'insert' ) || allow_action( $state, 'approve' ) ) )
        {
            return true;
        }
	    else
	    {
	        return false;
	    }
    }

}

function is_save_draft()
{
    if( isset( $_POST['save_draft'] ) && allow_action( $_GET['state'], 'insert' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_publish()
{
    if( isset( $_POST['publish'] ) && allow_action( $_GET['state'], 'insert' ) && $_COOKIE['user_type'] != 'contributor' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_print()
{
    if( isset( $_POST['print'] ) && allow_action( $_GET['state'], 'insert' ) && $_COOKIE['user_type'] != 'contributor' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_unpublish()
{
    if( isset( $_POST['unpublish'] ) && allow_action( $_GET['state'], 'insert' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_approved()
{
    if( isset( $_POST['publish'] ) && allow_action( $_GET['state'], 'approve' ) && ( is_administrator() || is_editor() ) )
    {
        return true;
    }

    if( ( isset( $_POST['prc'] ) && $_POST['prc'] == 'approve' && allow_action( $_POST['state'], 'approve' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_disapproved()
{
    if( isset( $_POST['unpublish'] ) && allow_action( $_GET['state'], 'approve' ) && ( is_administrator() || is_editor() ) )
    {
        return true;
    }

    if( ( isset( $_POST['prc'] ) && $_POST['prc'] == 'disapprove' && allow_action( $_POST['state'], 'approve' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_saved()
{
    if( isset( $_POST['article_saved'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Log Data
| -------------------------------------------------------------------------------------
*/
function save_log( $id = '', $type = '', $desc = '' )
{
    global $db;

    if( empty( $id ) || empty( $type ) )
    {
        return;
    }

    if( isset( $_COOKIE['user_id'] ) )
    {
        $s = 'INSERT INTO l_log(
                lapp_id,
                lapp_type,
                ldescription,
                lupdate_date,
                luser_id)
              VALUES( %s, %s, %s, %s, %d )';
        $q = $db->prepare_query( $s, $id, $type, $desc, date( 'Y-m-d H:i:s' ), $_COOKIE['user_id'] );

        return $db->do_query( $q );
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Log Data
| -------------------------------------------------------------------------------------
*/
function delete_log( $id = '', $type = '' )
{
    global $db;

    $s = 'DELETE FROM l_log WHERE lapp_id = %d AND lapp_type = %s';
    $q = $db->prepare_query( $s, $id, $type );

    return $db->do_query( $q );
}

/*
| -------------------------------------------------------------------------------------
| Generate Last Updated Note
| -------------------------------------------------------------------------------------
*/
function get_last_update_note( $id = '', $type = '' )
{
    global $db;

    $s = 'SELECT * FROM l_log AS a
			  LEFT JOIN l_users AS b ON a.luser_id = b.luser_id
			  WHERE a.lapp_id = %d AND a.lapp_type = %s ORDER BY a.lupdate_date DESC LIMIT 1';
    $q = $db->prepare_query( $s, $id, $type );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d    = $db->fetch_array( $r );
        $date = strtotime( $d['lupdate_date'] );
        return '<p>Last edited by ' . $d['ldisplay_name'] . ' on ' . date( 'D, j M Y', $date ) . ' at ' . date( 'g:i:a', $date ) . '</p>';
    }
}

/*
| -------------------------------------------------------------------------------------
| Generate Message
| -------------------------------------------------------------------------------------
*/
function generate_message_block( $message = array() )
{
    global $flash;

    $message_sess = $flash->render();

    if( empty( $message ) || !isset( $message['type'] ) )
    {
        if( empty( $message_sess ) )
        {
            return '';
        }
        else
        {
            foreach( $message_sess as $m )
            {
                $message = $m;
            }
        }
    }

    if( $message['type'] == 'error' )
    {
        $bg_css  = 'bg-danger';
        $txt_css = 'text-danger';
        $ico_css = 'glyphicon glyphicon-exclamation-sign';
    }
    elseif( $message['type'] == 'success' )
    {
        $bg_css  = 'bg-success';
        $txt_css = 'text-success';
        $ico_css = 'glyphicon glyphicon-ok-sign';
    }

    $message_block = '
    <fieldset class="message ' . $bg_css . '">
        <ul>';

		    foreach( $message['content'] as $value )
		    {
		        $message_block .= '<li class="' . $txt_css . '"><span class="' . $ico_css . '" aria-hidden="true"></span>' . $value . '</li>';
		    }

		    $message_block .= '
        </ul>
    </fieldset>';

    return $message_block;
}

/*
| -------------------------------------------------------------------------------------
| Generate Error Query Message
| -------------------------------------------------------------------------------------
*/
function generate_error_query_message_block()
{
    if( !isset( $_GET['error-query'] ) )
    {
        return '';
    }

    $d = json_decode( base64_decode( $_GET['error-query'] ), true );

    if( json_last_error() === JSON_ERROR_NONE )
    {
        if( isset( $d['error_code'] ) && $d['error_code'] == 1451 )
        {
            $message = 'Sorry, data can\'t be deleted. May already be used in another table';
        }
        else
        {
            $message = $d['message'];
        }

        $message_block = '
	    <fieldset class="message bg-danger">
	    	<p class="text-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>' . $message . '</p>
	    </fieldset>';

        return $message_block;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Feedback Link Option
| -------------------------------------------------------------------------------------
*/
function get_feedback_link_opt( $val = '' )
{
    return '
    <option value="0" ' . ( $val == 0 ? 'selected' : '' ) . '>Feedback Post To Website</option>
    <option value="1" ' . ( $val == 1 ? 'selected' : '' ) . '>Feedback Post To External Link</option>';
}

/*
| -------------------------------------------------------------------------------------
| Get Credit Card Payment Gateway Option
| -------------------------------------------------------------------------------------
*/
function get_cc_payment_gateway_opt( $val = '' )
{
    return '
    <option value="nicepay" ' . ( $val == 'nicepay' ? 'selected' : '' ) . '>NICEPay</option>
    <option value="doku" ' . ( $val == 'doku' ? 'selected' : '' ) . '>Doku</option>';
}

/*
| -------------------------------------------------------------------------------------
| Not Found Page
| -------------------------------------------------------------------------------------
*/
function not_found_template()
{
    set_template( TEMPLATE_PATH . '/template/not-found.html', 'not-found' );

    add_block( 'not-found-block', 'lcblock', 'not-found' );

    parse_template( 'not-found-block', 'lcblock', false );

    add_actions( 'section_title', 'Page Not Found' );

    return return_template( 'not-found' );
}

/*
| -------------------------------------------------------------------------------------
| Reschedule not Match
| -------------------------------------------------------------------------------------
*/
function not_match_schedule_template()
{
    set_template( TEMPLATE_PATH . '/template/not-match.html', 'not-found' );

    add_block( 'not-found-block', 'lcblock', 'not-found' );

    parse_template( 'not-found-block', 'lcblock', false );

    add_actions( 'section_title', 'Schedule not Match' );

    return return_template( 'not-found' );
}

/*
| -------------------------------------------------------------------------------------
| ACCESS ROLE
| -------------------------------------------------------------------------------------
*/
function is_access( $id, $state='', $sub='' )
{
    global $db;

    $s = 'SELECT
            a.luser_privilage,
            b.lrole_type
          FROM l_users_role AS b
          JOIN l_users AS a ON b.lrole_id = a.lrole_id
          WHERE a.luser_id=%d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    $f = $db->fetch_array( $r );

    if( $db->num_rows( $r ) > 0 )
    {
        $privilage = isset( $f['luser_privilage'] ) && $f['luser_privilage'] != '' ? json_decode( $f['luser_privilage'], true ) : array();

        if( !empty( $privilage ) )
        {
            if( isset( $privilage[$state] ) && isset( $privilage[$sub] ) )
            {
                if ( $privilage[$state] == '1' && $privilage[$sub] == '1' )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            if ( $f['lrole_type'] == '1' )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

?>