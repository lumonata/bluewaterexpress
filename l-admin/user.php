<?php

require_once( '../l_config.php' );
require_once( '../l_settings.php' );
require_once( '../l-functions/settings.php' );
require_once( '../l-classes/actions.php' );
require_once( '../l-functions/upload.php' );
require_once( '../l-functions/attachment.php' );
require_once( '../l-classes/directory.php' );
require_once( '../l-functions/friends.php' );
require_once( '../l-functions/user.php' );
require_once( '../l-functions/paging.php' );
require_once( '../l-content/languages/en.php' );
require_once( '../l-classes/user_privileges.php' );
require_once( '../l-classes/user_app_privileges.php' );
require_once( 'admin_functions.php' );

if( !defined( 'SITE_URL' ) )
{
    define( 'SITE_URL', get_meta_data( 'site_url' ) );
}

if( is_user_logged() )
{
    if( is_delete( 'users' ) )
    {
        if( delete_user( $_POST['id'] ) )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"failed"}';
        }
    }
    elseif( is_search() )
    {
        $s = 'SELECT * FROM l_users WHERE lusername LIKE %s OR ldisplay_name LIKE %s OR lemail = %s';
        $q = $db->prepare_query( $s, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $_POST['s'] );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            echo users_list( $r );
        }
        else
        {
            echo '
            <tr>
                <td colspan="6">
                    <p class="text-center text-danger">No result found for <em>' . $_POST['s'] . '</em>. Check your spellling or try another terms</p>
                </td>
            </tr>';
        }
    }
}
else
{
    header( 'location:' . get_admin_url() . '/?state=login' );
}

?>