<?php
    require_once('../l_config.php');
    require_once('../l_settings.php');
    require_once('../l-functions/settings.php');
    require_once('../l-classes/actions.php');
    require_once('../l-functions/upload.php');
    require_once('../l-functions/attachment.php');
    require_once('../l-classes/directory.php');
    require_once('../l-functions/user.php');
    require_once('../l-functions/paging.php');
    require_once('../l-content/languages/en.php');
    
     
    
    if(!defined('SITE_URL'))
	    define('SITE_URL',get_meta_data('site_url'));
    
    /*
	Check is the user is logged or not
	If user is not logged yet, then redirect user to the login form
    */
    if(!is_user_logged()){
	    header("location:".get_admin_url()."/?state=login");
    }else{
	
        
	
	if(!defined('TEMPLATE_PATH'));
	    define('TEMPLATE_PATH',ROOT_PATH.'/l-admin/');
	
	
	$theme=get_meta_data('admin_theme','themes');
	if(!defined('TEMPLATE_URL'))
	   define('TEMPLATE_URL',SITE_URL.'/l-admin/themes/'.$theme);
	
	if(!defined('FILES_PATH'))
	   define('FILES_PATH',ROOT_PATH.'/l-content/files');    
	
	require_once('../l-functions/template.php');
        
        if(isset($_GET['post_id'])){
            //set template
            set_template(TEMPLATE_PATH."upload-media.html",'uploadMedia');
            
            //set block
            add_block('uploadMediaBlock','upMediaBlock','uploadMedia');
            add_block('imageMediaURLBlock','iMediaBlock','uploadMedia');
            add_block('otherMediaURLBlock','oMediaBlock','uploadMedia');
            add_block('galleryMediaBlock','gMediaBlock','uploadMedia');
            
            add_actions('header_elements','get_javascript','jquery');
            //add_actions('header_elements','get_javascript','jquery.colorbox');
            //add_actions('header_elements','get_javascript','colorbox');
            add_actions('header_elements','get_javascript','upload-media');          
            add_actions('header_elements','get_javascript','jquery.filedrop');
            add_actions('header_elements','get_javascript','upload-media-dragdrop');
            
            $file_name='';
            
            add_variable('css',HTSERVER.TEMPLATE_URL.'/css/style.css');
            
            $count_gallery=count_attachment($_GET['post_id']);
            $count_lib=count_attachment();
            $tabs=array("from-computer"=>"From Computer","from-url"=>"From URL","gallery"=>"Gallery ($count_gallery)","library"=>"Library ($count_lib)");
            $tab=set_attachment_tab($tabs);
            add_variable('tabs',$tab);
            add_variable('dragdropimage',dragdropimage($_GET['type'],$_GET['post_id'],$_GET['textarea_id']));
            add_variable('textarea_id',$_GET['textarea_id']);
        }
        
        //sort order the gallery
	if(isset($_POST['update_media_order'])){
           update_attachment_order($_POST['attachment'],$_POST['start']);
    }elseif(isset($_POST['confirm_delete'])){
	    if($_POST['confirm_delete']=="yes"){
			delete_attachment($_POST['delete_id']);
	    }
	}elseif(isset($_POST['save_changes']) && $_POST['save_changes']=="save_item"){
		if(edit_attachment($_POST['attachment_id'], $_POST['title'],$_POST['order'],$_POST['alt_text'],$_POST['caption']))
		    echo "<div class=\"alert_green\">".UPDATE_SUCCESS."</div>";
	}elseif(isset($_POST['insert']) && !is_array($_POST['insert'])){
	    edit_attachment($_POST['attachment_id'],$_POST['title'],$_POST['order'],$_POST['alt_text'],$_POST['caption']);
	}elseif(isset($_POST['s']) && isset($_POST['tab'])){
	    echo search_attachment_results($_POST['s'],$_POST['tab'],$_POST['article_id'],$_POST['textarea_id']);
	}else{
            
	//File uploaded from user computer and then save it into database IF upload drag drop
			if(array_key_exists('pic',$_FILES) && $_FILES['pic']['error'] == 0 ){
					//exit_status('File was uploaded successfuly!');
		            //Create destination folder if folder is not exist yet
		            ///*
                    //if(!is_dir(FILES_PATH.'/'.upload_folder_name())){
                   //     if(!create_dir(FILES_PATH.'/'.upload_folder_name()))
                    //        exit_status("Unable to create new folder <code>".FILES_PATH.'/'.upload_folder_name."</code>");
                    //}
                    
                    $pic = $_FILES['pic'];
                    $file_name = $pic['name'];
                    $file_size = $pic['size'];
                    $file_type = $pic['type'];
                    $file_source = $pic['tmp_name'];
                    
					if(is_allow_file_type('image/'.get_extension($file_name),"image")){
						if(is_allow_file_size($file_size)){
							//if($_GET['type']=='image'){
								$post_id = $_POST['post_id'];
								$textarea_id = $_POST['textarea_id'];
                                if(upload_image_attachment($file_source,'image/'.get_extension($file_name),$file_name,$post_id)){
                                	//exit_status('File was uploaded successfuly!');

									//*
									$article_id=$post_id;
									$attach_id=$db->insert_id();
									$i=$db->num_rows($db->do_query($db->prepare_query("select * from l_attachment where larticle_id=%d",$article_id)));
									$sql=$db->prepare_query("select * from l_attachment where larticle_id=%d Order By lattach_id Desc Limit 1 ",$article_id);
								    $result=$db->do_query($sql);
								    $attch=gallery_items($result,'from-computer',$textarea_id,$i);
								    //*/
								    exit_status($attch);
                                }else{
									exit_status("<div class=\"alert_red_form\" style=\"width:93%;\">Something went wrong with your upload!</div>",'error');
								}
                        	//}
						}else{
							exit_status("<div class=\"alert_red_form\" style=\"width:93%;\">The uploaded file exceeds the <code>upload_max_filesize</code> directive in <code>php.ini</code>.</div>",'error');
						}
					}else{
						exit_status("<div class=\"alert_red_form\" style=\"width:93%;\">File type not allowed</div>",'error');
					}
					//*/
                    
					/*
	            	$upload_dir = 'uploads/';
					$allowed_ext = array('jpg','jpeg','png','gif');
					
					
					if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
						exit_status('Error! Wrong HTTP method!');
					}
		
					$pic = $_FILES['pic'];
				
					if(!in_array(get_extension($pic['name']),$allowed_ext)){
						exit_status('Only '.implode(',',$allowed_ext).' files are allowed!');
					}	
				
					if($demo_mode){
						
						// File uploads are ignored. We only log them.
						
						$line = implode('		', array( date('r'), $_SERVER['REMOTE_ADDR'], $pic['size'], $pic['name']));
						file_put_contents('log.txt', $line.PHP_EOL, FILE_APPEND);
						
						exit_status('Uploads are ignored in demo mode.');
					}
					
					
					// Move the uploaded file from the temporary 
					// directory to the uploads folder:
					
					if(move_uploaded_file($pic['tmp_name'], $upload_dir.$pic['name'])){
						exit_status('File was uploaded successfuly!');
					}
					*/
					
				}
            
            //File uploaded from user computer and then save it into database
            elseif(isset($_GET['tab']) && $_GET['tab']=='from-computer'){
                if(isset($_POST['upload'])){ //if upload button clicked
                    
                    //Create destination folder if folder is not exist yet
                    if(!is_dir(FILES_PATH.'/'.upload_folder_name())){
                        if(!create_dir(FILES_PATH.'/'.upload_folder_name()))
                            add_variable('alert',"<div class=\"alert_red_form\" style=\"width:93%;\">Unable to create new folder <code>".FILES_PATH.'/'.$folder_name."</code></div>");
                    }
                   
                    $file_name = $_FILES['media']['name'];
                    $file_size = $_FILES['media']['size'];
                    $file_type = $_FILES['media']['type'];
                    $file_source = $_FILES['media']['tmp_name'];
                    
                    
                    if(is_allow_file_type($file_type,$_GET['type'])){
                        if(is_allow_file_size($file_size)){
                            //If file type is Image
                            if($_GET['type']=='image'){
                                if(upload_image_attachment($file_source,$file_type,$file_name,$_GET['post_id'])){
				    
				    $file=attemp_actions('original_file_location'); 
				    $large_file=attemp_actions('large_file_location');
				    $medium_file=attemp_actions('medium_file_location');
				    $thumb_file=attemp_actions('medium_file_location');
				    
				    $default_title=file_name_filter($file_name);
				    
				    $d=array('lattach_id'=>$db->insert_id(),
					     'larticle_id'=>$_GET['post_id'],
					     'lattach_loc'=>$file,
					     'lattach_loc_large'=>$large_file,
					     'lattach_loc_medium'=>$medium_file,
					     'lattach_loc_thumb'=>$thumb_file,
					     'ltitle'=>$default_title,
					     'lalt_text'=>'',
					     'lcaption'=>'',
					     'mime_type'=>$file_type,
					     'upload_date'=>date(get_date_format(),time())
					     );
				  
				    add_variable('attachment_details', attachment_details($d,0,$_GET['textarea_id'],$_GET['tab']));
                                    add_actions("bottom_elements","<script type=\"text/javascript\">$('#upload_image_detail').show('slow');</script>");
                                    
                                }
                            }else{
                                if(upload_media_attachment($file_source,$file_type,$file_name,$_GET['post_id'])){
                                   
				    ///$file="http://".SITE_URL.attemp_actions('original_file_location');
				    /*START UPDATE YANA 2013-06-13*/ 				    
                    $file=attemp_actions('original_file_location'); 
                    /*END UPDATE YANA 2013-06-13*/ 
				    $default_title=file_name_filter($file_name);
				    
				    $d=array('lattach_id'=>$db->insert_id(),
					     'larticle_id'=>$_GET['post_id'],
					     'lattach_loc'=>$file,
					     'ltitle'=>$default_title,
					     'lalt_text'=>'',
					     'lcaption'=>'',
					     'mime_type'=>$file_type,
					     'upload_date'=>date(get_date_format(),time())
					     );
				   
				    add_variable('attachment_details', attachment_details($d,0,$_GET['textarea_id'],$_GET['tab']));
                                    add_actions("bottom_elements","<script type=\"text/javascript\">  $('#upload_image_detail').show('slow');</script>");
                                }
                            }
                            add_variable("delete_box",delete_confirmation_box($db->insert_id(),"Are you sure want to delete <code>".$default_title."</code> from the gallery?","upload-media.php","upload_image_detail"));
                        }else{
                            add_variable('alert',"<div class=\"alert_red_form\" style=\"width:93%;\">The uploaded file exceeds the <code>upload_max_filesize</code> directive in <code>php.ini</code>.</div>");
                        }
                    }else{
                        add_variable('alert',"<div class=\"alert_red_form\" style=\"width:93%;\">File type not allowed</div>");
                    }
                }
		
                add_variable('upload_button',upload_button());
                add_variable('cancel_button',cancel_button());
                parse_template('uploadMediaBlock','upMediaBlock');
                
            //File URL get from another website and then save it into database
            }elseif(isset($_GET['tab']) && $_GET['tab']=='from-url'){
                add_variable('insert_button',button("button=insert&type=button"));
				add_variable('type',$_GET['type']);
				add_variable('textarea_id',$_GET['textarea_id']);
                if($_GET['type']=='image'){
                    add_variable('type_label','Image');
                    parse_template('imageMediaURLBlock','iMediaBlock');
                }elseif($_GET['type']=='flash'){
                    add_variable('type_label','SWF');
                    parse_template('otherMediaURLBlock','oMediaBlock'); 
                }elseif($_GET['type']=='video'){
                    add_variable('type_label','Video');
                    parse_template('otherMediaURLBlock','oMediaBlock'); 
                }elseif($_GET['type']=='music'){
                    add_variable('type_label','Music');
                    parse_template('otherMediaURLBlock','oMediaBlock'); 
                }elseif($_GET['type']=='pdf'){
                    add_variable('type_label','PDF');
                    parse_template('otherMediaURLBlock','oMediaBlock'); 
                }elseif($_GET['type']=='doc'){
                    add_variable('type_label','Document');
                    parse_template('otherMediaURLBlock','oMediaBlock'); 
                }
		
            }
            
            
            
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        if((isset($_GET['tab']) && $_GET['tab']=='gallery')){
        	if(isSet($_POST['loadMore']) && ($_POST['loadMore']=='gallery') ){
				get_attachment_ajax($_GET['post_id']);
				exit;
			}
            if(isset($_POST['save_all_changes']) && $_POST['save_all_changes']=="Save All Changes"){
                
                for($i=$_POST['start_order'];$i<count($_POST['attachment_id'])+$_POST['start_order'];$i++){
                    edit_attachment($_POST['attachment_id'][$i],$_POST['title'][$i],$_POST['order'][$i],$_POST['alt_text'][$i],$_POST['caption'][$i]);
                }
                add_variable('response',"<div class=\"alert_green\">".UPDATE_SUCCESS."</div>");
                add_actions("bottom_elements","<script type=\"text/javascript\">$('#response').slideDown(500);$('#response').delay(3000);$('#response').slideUp(500);</script>");
            }
            if(is_delete_all_media()){
            	add_actions('section_title','Delete Comments');
                $warning="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this comment:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these comments:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
                        $title=attachment_value("ltitle", $val);
                        $warning.="<li>".$title."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$val."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_attachment_tab_url($_GET['tab'])."'\" />";
                $warning.="</div>";
                $warning.="</form>";
                
            	add_variable('attachment',$warning);
            }else{
            	if(is_confirm_delete_media()){
	            	foreach($_POST['id'] as $key=>$val){
	                       delete_attachment($val);
	                }
            	}
            	add_variable('attachment',get_attachment($_GET['post_id']));	
            }
            
            add_variable('asc_order',get_attachment_tab_url($_GET['tab'])."&sort_order=asc");
            add_variable('desc_order',get_attachment_tab_url($_GET['tab'])."&sort_order=desc");
            
            add_actions('header_elements','get_javascript','jquery_ui');
            parse_template('galleryMediaBlock','gMediaBlock'); 
        }
        
        if(isset($_GET['tab']) && $_GET['tab']=='library'){
	        if(isSet($_POST['loadMore']) && ($_POST['loadMore']=='library') ){
				get_attachment_ajax();
				exit;
			}
            if(isset($_POST['save_all_changes']) && $_POST['save_all_changes']=="Save All Changes"){
               
                for($i=$_POST['start_order'];$i<count($_POST['attachment_id'])+$_POST['start_order'];$i++){
                    edit_attachment($_POST['attachment_id'][$i],$_POST['title'][$i],$_POST['order'][$i],$_POST['alt_text'][$i],$_POST['caption'][$i]);
                }
                add_variable('response',"<div class=\"alert_green\">".UPDATE_SUCCESS."</div>");
                add_actions("bottom_elements","<script type=\"text/javascript\">$('#response').slideDown(500);$('#response').delay(3000);$('#response').slideUp(500);</script>");
            }
             if(is_delete_all_media()){
            	add_actions('section_title','Delete Comments');
                $warning="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this comment:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these comments:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
                        $title=attachment_value("ltitle", $val);
                        $warning.="<li>".$title."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$val."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_attachment_tab_url($_GET['tab'])."'\" />";
                $warning.="</div>";
                $warning.="</form>";
                
            	add_variable('attachment',$warning);
            }else{
            	if(is_confirm_delete_media()){
	            	foreach($_POST['id'] as $key=>$val){
	                       delete_attachment($val);
	                }
            	}
            	
            	add_variable('attachment',get_attachment());
            }
            add_variable('asc_order',get_attachment_tab_url($_GET['tab'])."&sort_order=asc");
            add_variable('desc_order',get_attachment_tab_url($_GET['tab'])."&sort_order=desc");
            
            add_actions('header_elements','get_javascript','jquery_ui');
            
            parse_template('galleryMediaBlock','gMediaBlock'); 
        }
        
        //Attempt the action that already add in the whole script
        //add_variable('delete_box',delete_confirmation_box($db->insert_id(),"Are you sure want to delete <code>$file_name</code> from the gallery?"));
        add_variable('header_elements',attemp_actions('header_elements'));
        add_variable('bottom_elements',attemp_actions('bottom_elements'));
        echo return_template('uploadMedia'); 
        
    }
    
    function set_attachment_tab($tabs){
        $tab='';
       
        foreach($tabs as $key=>$val){
            if($_GET['tab']==$key)
                $tab.="<li class=\"active\"><a href=\"http://".SITE_URL."/l-admin/upload-media.php?tab=".$key."&post_id=".$_GET['post_id']."&type=".$_GET['type']."&textarea_id=".$_GET['textarea_id']."\">$val</a></li>";
            else
                $tab.="<li><a href=\"http://".SITE_URL."/l-admin/upload-media.php?tab=".$key."&post_id=".$_GET['post_id']."&type=".$_GET['type']."&textarea_id=".$_GET['textarea_id']."\">$val</a></li>";
                
        }
        
        return $tab;
    }
    
    function get_attachment_tab_url($tab){
            $page_URL = 'http';
             if (!empty($_SERVER["HTTPS"]))
                    $page_URL .= "s";
                    
                    $page_URL .= "://";
             if ($_SERVER["SERVER_PORT"] != "80"){
                    $page_URL .= SITE_URL.":".$_SERVER["SERVER_PORT"]."/l-admin/upload-media.php?tab=".$tab."&post_id=".$_GET['post_id']."&type=".$_GET['type']."&textarea_id=".$_GET['textarea_id'];
             }else{
                    $page_URL .= SITE_URL."/l-admin/upload-media.php?tab=".$tab."&post_id=".$_GET['post_id']."&type=".$_GET['type']."&textarea_id=".$_GET['textarea_id'];
             }
             
             return $page_URL;
    }
    
    function dragdropimage($type,$post_id,$textarea_id){
    	$dragdropimage='<div id="dropbox">
                		<span class="message">
                		<input type="hidden" value="'.$post_id.'" name="post_id"/>
                		<input type="hidden" value="'.$textarea_id.'" name="textarea_id"/>
                		Drop images here to upload.</span>
                		</div>';
    	if($type=='image'){
    		return $dragdropimage;
    	}else{
    		return '';
    	}
    }
    
    function exit_status($str,$error=''){
		echo json_encode(array('status'=>$str,'error'=>$error));
		exit;
	}
	function get_extension($file_name){
		$ext = explode('.', $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function is_delete_all_media(){
		if((isset($_POST['delete']) && $_POST['delete']=='Delete'))
			return true;
		return false;
	}
	function is_confirm_delete_media(){
		if((isset($_POST['confirm_delete']) && $_POST['confirm_delete']=='Yes'))
			return true;
		return false;
	}
?>