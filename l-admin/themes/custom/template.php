<?php

	/*
	Title: Bluw Water Express
	Preview: preview.png
	Author: Ngurah Rai
	Author Url: -
	Description: Custom themes since March 2017
	*/

	if( !is_user_logged() )
	{
	    if( is_login_form() )
	    {
	        get_login_form();
	    }
	    elseif( is_register_form() )
	    {
	        signup_user();
	    }
	    elseif( is_thanks_page() )
	    {
	        resendPassword();
	    }
	    elseif( is_verify_account() )
	    {
	        verify_account();
	    }
	    elseif( is_forget_password() )
	    {
	        get_forget_password_form();
	    }
	    else
	    {
	        $return_url = get_admin_url() . '/?state=login&redirect=' . base64_encode( cur_pageURL() );

	        header( 'location:' . $return_url );
	    }
	}
	else
	{
	    if( empty( $_GET['state'] ) )
	    {
	        header( 'location:' . get_admin_url() . '/?state=dashboard' );	    
	    }

	    $d = fetch_user( $_COOKIE['user_id'] );

	    if( isset( $actions->action['is_use_ajax'] ) && $actions->action['is_use_ajax']['func_name'][0] )
		{			
			set_template( TEMPLATE_PATH . '/ajax.html' );
		}
	    else
	    {
	    	set_template( TEMPLATE_PATH . '/index.html' );
	    }

	    add_block( 'adminArea', 'aBlock' );

	    add_variable( 'site_url', HTSERVER . site_url() );
	    add_variable( 'web_title', web_title() );
	    add_variable( 'avatar', get_avatar( $_COOKIE['user_id'], 2 ) );
	    add_variable( 'setting_link', get_state_url( 'global_settings' ) );
	    add_variable( 'logout_link', get_state_url( 'logout' ) );

	    if( is_administrator() )
	    {
	        add_variable( 'profile_link', get_state_url( 'users&tab=my-profile' ) );
	    }
	    else
	    {
	        add_variable( 'profile_link', get_state_url( 'users&tab=my-profile' ) );
	    }

	    if( is_administrator() )
	    {
	        add_variable( 'profile_updates', get_state_url( 'users&tab=my-updates' ) );
	    }
	    else
	    {
	        add_variable( 'profile_updates', get_state_url( 'users&tab=my-updates' ) );
	    }

	    add_actions( 'header_elements', 'get_css_inc', 'bootstrap-3.3.7/css/bootstrap.min.css' );
	    add_actions( 'header_elements', 'get_css_inc', 'bootstrap-3.3.7/css/custom-theme/jquery-ui-1.10.0.custom.css' );
	    add_actions( 'header_elements', 'get_css_inc', 'jquery-timepicker-addon/jquery-ui-timepicker-addon.min.css' );
	    add_actions( 'header_elements', 'get_css_inc', 'jquery-timepicker/jquery.timepicker.css' );
	    add_actions( 'header_elements', 'get_css_inc', 'font-awesome/css/font-awesome.min.css' );
	    add_actions( 'header_elements', 'get_css_inc', 'fancybox/dist/jquery.fancybox.min.css' );
	    add_actions( 'header_elements', 'get_css_inc', 'select2/dist/css/select2.min.css' );
	    add_actions( 'header_elements', 'get_css_inc', 'chosen/chosen.min.css' );

	    add_actions( 'header_elements', 'get_css', 'style.css', '1.0.2' );
	    
	    add_actions( 'header_elements', 'get_javascript', 'jquery-3.2.1.min' );
	    
	    add_actions( 'header_elements', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
	    add_actions( 'header_elements', 'get_javascript_inc', 'jquery-timepicker/jquery.timepicker.min.js' );
	    add_actions( 'header_elements', 'get_javascript_inc', 'jquery-timepicker-addon/jquery-ui-timepicker-addon.min.js' );  
	    add_actions( 'header_elements', 'get_javascript_inc', 'jquery-validation-1.19.3/dist/jquery.validate.min.js' );
	    add_actions( 'header_elements', 'get_javascript_inc', 'jquery-validation-1.19.3/dist/additional-methods.min.js' );
	    add_actions( 'header_elements', 'get_javascript_inc', 'autonumeric-1.9.46/autoNumeric-min.js' ); 
	    add_actions( 'header_elements', 'get_javascript_inc', 'fancybox/dist/jquery.fancybox.min.js' );
	    add_actions( 'header_elements', 'get_javascript_inc', 'bootstrap-3.3.7/js/bootstrap.min.js' );
	    add_actions( 'header_elements', 'get_javascript_inc', 'select2/dist/js/select2.min.js' );
	    add_actions( 'header_elements', 'get_javascript_inc', 'chosen/chosen.jquery.min.js' );
	    
	    add_actions( 'header_elements', 'get_javascript_inc', 'datatables-1.10.13/media/js/jquery.dataTables.min.js' );
	    add_actions( 'header_elements', 'get_javascript_inc', 'datatables-1.10.13/media/js/dataTables.bootstrap.min.js' );
	    add_actions( 'header_elements', 'get_javascript_inc', 'datatables-1.10.13/extensions/RowReorder/js/dataTables.rowReorder.min.js' );

	    add_variable( 'displayname', $d['ldisplay_name'] );
	    add_variable( 'content_area', $thecontent );
	    add_variable( 'connection_menu', get_admin_menu( 'connection_menu' ) );
	    add_variable( 'settings_menu', get_admin_menu( 'settings_menu' ) );
	    add_variable( 'navmenu', get_admin_menu() );
	    add_variable( 'img_url', get_theme_img() );
	    add_variable( 'theme_url', get_theme() );
	    
	    add_variable( 'header_elements', attemp_actions( 'header_elements' ) );
	    add_variable( 'other_elements', attemp_actions( 'other_elements' ) );
	    add_variable( 'section_title', attemp_actions( 'section_title' ) );
	    add_variable( 'admin_tail', attemp_actions( 'admin_tail' ) );

	    parse_template( 'adminArea', 'aBlock' );

	    print_template();
	}

?>