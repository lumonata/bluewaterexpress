function show_popup( htitle, message, callback )
{
    jQuery('<div></div>').dialog({
        modal: true,
        draggable: false,
        resizable: false,
        title: htitle,
        open: function()
        {
            jQuery(this).html(message);
        },
        buttons:
        {
            Close: function()
            {
                jQuery( this ).dialog('close');

                if( callback!='' )
                {
                    eval(callback);
                }
            }
        }
    });
}

function show_ajax_overlay()
{
    jQuery('.section-overlay').css('display', 'block');
}

function hide_ajax_overlay()
{
    jQuery('.section-overlay').css('display', 'none');
}

function validate_email( address )
{
    var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if( reg.test(address) == false )
    {
        return false; 
    }
    else
    {
        return true;
    }
}

function to_title_case( str )
{
    if( str != null )
    {
        str = str.toLowerCase();
        
        return str.replace( /\b\S/g, function(t){ return t.toUpperCase() });
    }
    else
    {
        return str;
    }
}

function get_uri_param( param )
{
    param = param.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');

    var regex   = new RegExp('[\\?&]' + param + '=([^&#]*)');
    var results = regex.exec(location.search);

    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function remove_uri_param( url, param )
{
    var urlparts= url.split('?');

    if( urlparts.length >= 2 )
    {
        var prefix= encodeURIComponent( param )+'=';
        var pars= urlparts[1].split(/[&;]/g);

        for( var i= pars.length; i-- > 0; )
        {
            if( pars[i].lastIndexOf(prefix, 0) !== -1 )
            {  
                pars.splice(i, 1);
            }
        }

        url = urlparts[0]+'?'+pars.join('&');

        return url;
    }
    else
    {
        return url;
    }
}

function round( value, decimals )
{
  return Number( Math.round( value + 'e' + decimals ) + 'e-' + decimals );
}

function table_block_action()
{
    jQuery('.table-block [name=check_all]').unbind('click');
    jQuery('.table-block [name=check_all]').on('click', function(e){
        var is_check = jQuery(this).prop('checked');

        if( is_check )
        {
            jQuery('.button-box .btn-delete').removeClass('disabled').removeAttr('disabled');
        }
        else
        {
            jQuery('.button-box .btn-delete').addClass('disabled').attr('disabled', 'disabled');
        }

        jQuery('.table-block tbody input[type=checkbox]:not(:disabled)').prop('checked', is_check);
    });

    jQuery('.table-block tbody input[type=checkbox]').unbind('click');
    jQuery('.table-block tbody input[type=checkbox]').on('click', function(e){
        var check_length = jQuery('.table-block tbody input[type=checkbox]:checked').length;

        if( check_length > 0 )
        {
            jQuery('.button-box .btn-delete').removeClass('disabled').removeAttr('disabled');
        }
        else
        {
            jQuery('.button-box .btn-delete').addClass('disabled').attr('disabled', 'disabled');
        }
    });
}

jQuery(document).ready(function(){
    table_block_action();
    
    if( jQuery('.sidebar .inner').length > 0 )
    {
        jQuery('.sidebar .inner').niceScroll({cursorcolor:'#00F'} );
    }

    jQuery('.number-field').each(function() {
        var spinner = jQuery(this),
            input   = spinner.find('input[type="number"]'),
            btnUp   = spinner.find('.up'),
            btnDown = spinner.find('.down');

        btnUp.click(function(){
            if( !input.prop('disabled') )
            {
                var oldValue = parseFloat( input.val() );
                var min      = input.attr('min');
                var max      = input.attr('max');

                if( oldValue >= max )
                {
                    var newVal = oldValue;
                }
                else
                {
                    var newVal = oldValue + 1;
                }

                spinner.find('input').val(newVal);
                spinner.find('input').trigger('change');
            }
        });

        btnDown.click(function() {
            if( !input.prop('disabled') )
            {
                var oldValue = parseFloat( input.val() );
                var min      = input.attr('min');
                var max      = input.attr('max');

                if( oldValue <= min )
                {
                  var newVal = oldValue;
                }
                else
                {
                  var newVal = oldValue - 1;
                }

                spinner.find('input').val(newVal);
                spinner.find('input').trigger('change');
            }
        });
    });
})