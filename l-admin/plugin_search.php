<?php

    define('PLUGINS_PATH','../l-plugins');
    
    require_once('../l_config.php');
    require_once('../l-functions/kses.php');
    require_once('../l-functions/settings.php');
    require_once('../l-functions/plugins.php');
    require_once('../l-classes/directory.php');
    require_once('../l-functions/user.php');
   
    if(!is_user_logged()){
	    header("location:".get_admin_url()."/?state=login");
    }else{
        echo search_plugin($_POST['start'],$_POST['end']);
    } 
?>