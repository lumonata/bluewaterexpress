<?php

class user_privileges
{
    var $user_type;
    var $the_privileges;
    var $the_actions;

    function __construct()
    {
        if( isset( $_COOKIE['user_id'] ) && isset( $_COOKIE['password'] ) && isset( $_COOKIE['thecookie'] ) )
        {
            if( md5( $_COOKIE['password'] . $_COOKIE['user_id'] ) == $_COOKIE['thecookie'] )
            {
                global $db;
        
                $s = 'SELECT * FROM l_users_role WHERE lrole_id = %d';
                $q = $db->prepare_query( $s, $_COOKIE['user_type'] );
                $r = $db->do_query( $q );

                if( $db->num_rows( $r ) > 0 )
                {
                    $d = $db->fetch_array( $r );
                    $v = json_decode( $d['lrole_privileges'], true );

                    if( json_last_error() == JSON_ERROR_NONE )
                    {
                        foreach( $v as $privileges )
                        {
                            $this->add_privileges( $_COOKIE['user_type'], $privileges, 'insert' );
                            $this->add_privileges( $_COOKIE['user_type'], $privileges, 'update' );
                            $this->add_privileges( $_COOKIE['user_type'], $privileges, 'delete' );
                            $this->add_privileges( $_COOKIE['user_type'], $privileges, 'upload' );
                            $this->add_privileges( $_COOKIE['user_type'], $privileges, 'preview' );
                            $this->add_privileges( $_COOKIE['user_type'], $privileges, 'request' );
                            $this->add_privileges( $_COOKIE['user_type'], $privileges, 'approve' );                   
                        }
                    }
                }
            }
        }
    }

    function add_privileges( $role, $app_name, $action )
    {
        $this->the_privileges[$role][$app_name][] = $action;
    }

    function get_privileges()
    {
        return $this->the_privileges;
    }
}

$user_privileges = new user_privileges();

function add_privileges( $role, $app_name, $action )
{
    global $user_privileges;

    $user_privileges->add_privileges( $role, $app_name, $action );
}

function is_grant_app( $app_name )
{
    global $user_privileges;

    $the_privilege = $user_privileges->get_privileges();

    if( !isset( $_COOKIE['user_type'] ) )
    {
        return false;
    }

    if( isset( $the_privilege[$_COOKIE['user_type']] ) )
    {
        foreach( $the_privilege[$_COOKIE['user_type']] as $key => $val )
        {
            if( $key == $app_name )
            {
                return true;
            }
        }
    }

    return false;
}

function allow_action( $app_name, $action )
{
    global $user_privileges;

    $the_privilege = $user_privileges->get_privileges();

    if( !isset( $_COOKIE['user_type'] ) )
    {
        return false;
    }

    if( isset( $the_privilege[$_COOKIE['user_type']][$app_name] ) )
    {
        foreach( $the_privilege[$_COOKIE['user_type']][$app_name] as $key => $val )
        {
            if( $val == $action )
            {
                return true;
            }
        }
    }

    return false;
}

?>