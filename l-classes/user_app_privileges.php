<?php

class apps_privileges
{
    var $app_name;

    function __construct()
    {
        $this->app_name = array();
    }

    function add_apps_privileges( $app_name, $label )
    {
        $this->app_name[ $app_name ] = $label;
    }

    function get_apps_privileges()
    {
        return $this->app_name;
    }
}

$apps_privileges = new apps_privileges();

function add_apps_privileges( $app_name, $label )
{
    global $apps_privileges;

    $apps_privileges->add_apps_privileges( $app_name, $label );
}

?>