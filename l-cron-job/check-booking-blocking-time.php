<?php

define( 'ADMIN_PATH', realpath( dirname( __FILE__ ) ) . '/../l-admin' );

require_once( realpath( dirname( __FILE__ ) ) . '/../l_config.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-classes/db.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-classes/actions.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-functions/settings.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-classes/user_app_privileges.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/booking.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/functions.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-admin/includes/php-mailer/PHPMailerAutoload.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-admin/includes/mailchimp-mandrill-api-php/src/Mandrill.php' );

$blocking_data = get_booking_blocking_time_data();

if( !empty( $blocking_data ) )
{
	$themes = get_meta_data( 'admin_theme', 'themes' );

	define( 'SITE_URL', get_meta_data( 'site_url' ) );
	define( 'TEMPLATE_PATH', realpath( dirname( __FILE__ ) ) . '/../l-admin/themes/' . $themes );
	define( 'PLUGINS_PATH', realpath( dirname( __FILE__ ) ) . '/../l-plugins' );
	define( 'HTSERVER', empty( $_SERVER['HTTPS'] ) ? 'http://' : 'https://' );
	define( 'TEMPLATE_URL', SITE_URL . '/l-content/themes/' . $themes );
	
	require_once( realpath( dirname( __FILE__ ) ) . '/../l-functions/template.php' );

	foreach( $blocking_data as $d )
    {
        $current_time  = date( 'Y-m-d H:i:s', time() );
        $blocking_time = date( 'Y-m-d H:i:s', $d['bblockingtime'] );

        $ctime = new DateTime( $current_time );
        $btime = new DateTime( $blocking_time );

        if( $blocking_time > $current_time )
        {
        	$interval = $ctime->diff( $btime );
        	$link     = HTSERVER . SITE_URL . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $d['bid'];
        	$elapsed  = $interval->format('%y years %m months %a days %h hours %i minutes %s seconds');

	        if( ( $interval->d == 2 && $interval->m == 0 && $interval->y == 0 && $interval->h == 0 ) && $d['bblockingtime_sts_48'] == '0' )
	        {
	        	if( send_booking_blocking_time_notif_to_admin( $d['bid'], 2, $link ) )
	        	{
			        $title   = 'Blocking Time #' . $d['bticket'];
			        $content = sprintf( '<p>Hello, blocking time for reservation with ticket code #%s will expire within 48 hours. For more details please visit the following <a href="%s">link</a></p>', $d['bticket'], $link );

		        	save_ticket_notification( $d['bid'], $title, $content, 0, 0, $current_time );

		        	ticket_booking_update_data_per_field( $d['bid'], 'bblockingtime_sts_48', 1 );
	        	}
	        }
	        elseif( ( $interval->d == 1 && $interval->m == 0 && $interval->y == 0 && $interval->h == 0 ) && $d['bblockingtime_sts_24'] == '0' )
	        {
	        	if( send_booking_blocking_time_notif_to_admin( $d['bid'], 1, $link ) )
	        	{
			        $title   = 'Blocking Time #' . $d['bticket'];
			        $content = sprintf( '<p>Hello, blocking time for reservation with ticket code #%s will expire within 24 hours. For more details please visit the following <a href="%s">link</a></p>', $d['bticket'], $link );

		        	save_ticket_notification( $d['bid'], $title, $content, 0, 0, $current_time );

		        	ticket_booking_update_data_per_field( $d['bid'], 'bblockingtime_sts_24', 1 );
		        }
	        }
	    }
    }
}

?>