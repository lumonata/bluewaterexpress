<?php

define( 'ADMIN_PATH', realpath( dirname( __FILE__ ) ) . '/../l-admin' );

require_once( realpath( dirname( __FILE__ ) ) . '/../l_config.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-classes/db.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-classes/actions.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-functions/settings.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-classes/user_app_privileges.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/booking.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-admin/includes/php-mailer/PHPMailerAutoload.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-admin/includes/mailchimp-mandrill-api-php/src/Mandrill.php' );

$pending_payment_data = get_booking_pending_data();

if( !empty( $pending_payment_data ) )
{
	$themes = get_meta_data( 'admin_theme', 'themes' );

	define( 'SITE_URL', get_meta_data( 'site_url' ) );
	define( 'TEMPLATE_PATH', realpath( dirname( __FILE__ ) ) . '/../l-admin/themes/' . $themes );
	define( 'PLUGINS_PATH', realpath( dirname( __FILE__ ) ) . '/../l-plugins' );
	define( 'HTSERVER', empty( $_SERVER['HTTPS'] ) ? 'http://' : 'https://' );
	define( 'TEMPLATE_URL', SITE_URL . '/l-content/themes/' . $themes );

	require_once( realpath( dirname( __FILE__ ) ) . '/../l-functions/template.php' );
	require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/boat.php' );
	require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/area.php' );
	require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/setting.php' );
	require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/location.php' );
	require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/availability.php' );

    foreach( $pending_payment_data as $d )
    {
    	if( $d['bdate'] > '2020-03-20' )
        {
        	if( $d['bpvaliddate'] < strtotime( date( 'Y-m-d H:i:s' ) ) )
	        {
	            if( send_payment_notification_to_client( $d['bid'] ) )
	            {
	            	ticket_booking_update_status( $d['bid'], 'pf' );
	            }
	        }
	    }
    }
}

?>