<?php

define( 'ADMIN_PATH', realpath( dirname( __FILE__ ) ) . '/../l-admin' );

require_once( realpath( dirname( __FILE__ ) ) . '/../l_config.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-classes/db.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-classes/actions.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-functions/settings.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-classes/user_app_privileges.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/booking.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-plugins/ticket/include/functions.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-admin/includes/php-mailer/PHPMailerAutoload.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../l-admin/includes/mailchimp-mandrill-api-php/src/Mandrill.php' );

$data = get_recheck_reservation_data();

if( !empty( $data ) )
{
	$themes = get_meta_data( 'admin_theme', 'themes' );

	define( 'SITE_URL', get_meta_data( 'site_url' ) );
	define( 'TEMPLATE_PATH', realpath( dirname( __FILE__ ) ) . '/../l-admin/themes/' . $themes );
	define( 'PLUGINS_PATH', realpath( dirname( __FILE__ ) ) . '/../l-plugins' );
	define( 'HTSERVER', empty( $_SERVER['HTTPS'] ) ? 'http://' : 'https://' );
	define( 'TEMPLATE_URL', SITE_URL . '/l-content/themes/' . $themes );
	
	require_once( realpath( dirname( __FILE__ ) ) . '/../l-functions/template.php' );

	foreach( $data as $d )
    {
    	send_recheck_reservation_to_client( $d['bid'] );
    }
}