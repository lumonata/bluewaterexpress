<?php
session_start();

//if (empty($_SESSION['rand_code'])) {
    	//modify by YANA	
		$possible = 'abcdefghijklmnopqrstuvwxyz23456789'; //boleh pake a-z, 0-9
		$code = '';
		$i = 0;
		
		while ($i < 5) { 
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		//End Modify By Yana
	
    $_SESSION['rand_code'] = $code;
	
//}

$imgX = 120;
$imgY = 40;
$image = imagecreatetruecolor(120, 40);

$backgr_col = imagecolorallocate($image, 200,200,200);
$border_col = imagecolorallocate($image, 200,200,200);
$text_col = imagecolorallocate($image, 0,0,0); //($image, 2,78,162)

imagefilledrectangle($image, 0, 0, 120, 50, $backgr_col);
imagerectangle($image, 0, 0, 119, 38, $border_col);

$font =getcwd()."/ttf/arial.ttf"; // it's a Bitstream font check www.gnome.org for more
//$font="arial.ttf";

$font_size = 16;
$angle = 5;
$box = imagettfbbox($font_size, $angle, $font, $_SESSION['rand_code']);
$x = (int)($imgX - $box[4]) / 2;
$y = (int)($imgY - $box[5]) / 2;
imagettftext($image, $font_size, $angle, $x, $y, $text_col, $font, $_SESSION['rand_code']);

header("Content-type: image/png");
imagepng($image);
imagedestroy ($image);
?>