<?php

function get_front_passenger_list_content( $data, $is_edit = false )
{
    $content = '';

    foreach( $data as $obj )
    {
        foreach( $obj as $i => $d )
        {
        	if( $is_edit )
        	{
        		$content .= '
                <div class="person-detail person-detail-edit clearfix">
                    <div class="col-1">
	                    <p class="person">' . ucfirst( $d['bptype'] ) . ' ' . ( $i + 1 ) . '</p>
                        <div class="form-group">
                            <input type="text" class="form-control" id="" placeholder="' . $d['bpname'] . '" name="passenger[' . $d['bpid'] .'][bpname]">
                        </div>
                    </div>
                    <div class="col-2">
                        <p class="gender">Gender</p>
                        <div class="form-group">
                        	<select name="passenger[' . $d['bpid'] . '][bpgender]" class="select-option form-control" autocomplete="off">
                        		' . get_gender_option( $d['bpgender'], false ) . '
                        	</select>
                        </div>
                    </div>
                    <div class="col-3">
                        <p class="dob">Date Of Birth</p>
                        <div class="form-group">
                            <input type="text" class="form-control txt-date-' . $d['bptype'] . '" id="" placeholder="' . date( 'd M Y', strtotime( $d['bpbirthdate'] ) ) . '" name="passenger[' . $d['bpid'] . '][bpbirthdate]">
                        </div>
                    </div>
                    <div class="col-4">
                        <p class="nationality">Nationality</p>
                        <div class="form-group">
                            <select name="passenger[' . $d['bpid'] . '][lcountry_id]" class="select-option form-control" autocomplete="off">
                            	' . get_country_list_option( $d['lcountry_id'] ) . '
                            </select>
                        </div>
                    </div>
                </div>';
        	}
        	else
            {
            	$content .= '
                <ul class="row">
                    <li class="col-xs-3 col-sm-4 col-md-4">
                        <span>' . $d['bpname'] . '</span>
                    </li>
                    <li class="col-xs-1 col-sm-1 col-md-1">
                        <span>' . ucfirst( $d['bptype'] ) . '</span>
                    </li>
                    <li class="col-xs-2 col-sm-1 col-md-1">
                        <span>' . ( empty( $d['bpgender'] ) ? 'Female' : 'Male' ) . '</span>
                    </li>
                    <li class="col-xs-2 col-sm-2 col-md-2">
                        <span>' . ticket_passenger_country( $d['lcountry_id'] ) . '</span>
                    </li>
                    <li class="col-xs-2 col-sm-2 col-md-2">
                        <span>' . date( 'd M Y', strtotime( $d['bpbirthdate'] ) ) . '</span>
                    </li>
                    <li class="col-xs-2 col-sm-2 col-md-2 text-right">
                        <span>' . ( $d['bpstatus'] == 'cn' ? 'Cancelled' : 'Active' ) . '</span>
                    </li>
                </ul>';
	        }
        }
    }

    return $content;
}

function get_booking_id_by_ticket_and_email( $bticket, $bbemail )
{
    global $db;

    $s = 'SELECT a.bid
          FROM ticket_booking AS a
          LEFT JOIN ticket_agent AS b ON a.agid = b.agid
          WHERE a.bticket = %s AND a.bstt <> %s AND ( a.bbemail = %s OR b.agemail = %s )';
    $q = $db->prepare_query( $s, $bticket, 'ar', $bbemail, $bbemail );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        $d = $db->fetch_array( $r );

        return $d['bid'];
    }
}

function get_front_reservation_detail( $bid )
{
    global $db;

    $s = 'SELECT a.*, c.chname
          FROM ticket_booking AS a
          LEFT JOIN ticket_agent AS b ON b.agid = a.agid
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          WHERE a.bid = %d AND a.bstt <> %s';
    $q = $db->prepare_query( $s, $bid, 'ar' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        $d = $db->fetch_array( $r );

        $d['bstatus_txt'] = ticket_booking_status( $d['bstatus'] );
        $d['detail']      = ticket_booking_detail( $d['bid'] );

        return $d;
    }
}

function get_front_departure_availability_result( $post )
{
	global $db;
    
	extract( $post );
    extract( $sess_data );

    $dep_date = date( 'Y-m-d', strtotime( $date_of_depart ) );
    $pass_num = $adult_num + $child_num + $infant_num;

	$s = 'SELECT * FROM ticket_schedule AS a
		  LEFT JOIN ticket_route AS b ON a.rid = b.rid
		  LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
    $q = $db->prepare_query( $s, $dep_date, $dep_date, 'publish', 'publish', '1' );
    $r = $db->do_query( $q );

	set_template( TEMPLATE_PATH . '/results-loop.html', 'availability-depart-loop' );

    add_block( 'suggestion-result-loop-block', 'dsrlblock', 'availability-depart-loop' );
    add_block( 'suggestion-result-block', 'dsrblock', 'availability-depart-loop' );
    add_block( 'empty-loop-block', 'delblock', 'availability-depart-loop' );
    add_block( 'result-loop-block', 'drlblock', 'availability-depart-loop' );

    if( $db->num_rows( $r ) > 0 )
    {
	    $i = 0;
        $o = 0;

        $schedule = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $schedule[] = $d;

            $not_closed_date   = is_date_not_in_close_online_allotment_list( $d['sid'], $dep_date );
            $available_depart  = is_available_depart_on_list( $d['rid'], $depart_port, $dep_date );
            $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_port );
            $available_date    = is_available_date_on_list( $d['sid'], $dep_date );
            $not_pass_time     = is_not_pass_time_limit( $d['rid'], $sess_data, 'depart' );

        	if( $available_depart && $available_arrival && $available_date && $not_pass_time && $not_closed_date )
        	{
        	    
                $p = get_result_price( $d['sid'], $d['rid'], $dep_date, $depart_port, $destination_port, $adult_num, $child_num, $infant_num, null );
                $a = get_boat_first_attachment( $d['boid'] );

    			$inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
    			$exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                $remain_seat = remain_seat( $dep_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $depart_port, $destination_port, null, false, null, true );
                $num_of_seat = $remain_seat - $pass_num;

        		if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
        		{
                    if( is_block_request_transport( $d['rid'], $sess_data, 'depart' ) )
                    {
                        add_variable( 'include_trans_css', 'sr-only' );
                        add_variable( 'exclude_trans_css', '' );
                    }
                    else
                    {
                        add_variable( 'include_trans_css', '' );
                        add_variable( 'exclude_trans_css', 'sr-only' );
                    }

		        	add_variable( 'sid', $d['sid'] );
		        	add_variable( 'rid', $d['rid'] );
		        	add_variable( 'boid', $d['boid'] );
		        	add_variable( 'boname', $d['boname'] );
		        	add_variable( 'radio_name', 'dep_rid' );
                    add_variable( 'bopicture', $a['thumbnail'] );

		        	add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
		        	add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
		        	add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

		        	add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
		        	add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
		        	add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                    add_variable( 'btnstatus', $not_closed_date && $remain_seat > 0 && $num_of_seat >= 0 ? '' : 'disabled' );
                    add_variable( 'port_name', get_front_route_depart_first( $d['rid'], $depart_port, $destination_port ) );

                    // if( $num_of_seat < 7 )
                    // {
                    //     if( $num_of_seat <= 0 )
                    //     {
                    //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', 'No seat left' ) );
                    //     }
                    //     elseif( $num_of_seat == 1 )
                    //     {
                    //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', '1 seat left' ) );
                    //     }
                    //     else
                    //     {
                    //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', $num_of_seat . ' seats left' ) );
                    //     }
                    // }
                    // else
                    // {
                    //     add_variable( 'remain_seat', '' );
                    // }
                    add_variable( 'remain_seat', '' );

		        	if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
		        	{
                        if( !empty( $p['early_discount'] ) )
                        {
                            if( $p['early_discount']['type'] == '0' )
                            {
                                $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                            }
                            else
                            {
                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                            }

                            add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                        }

                        if( !empty( $p['seat_discount'] ) )
                        {
                            if( $p['seat_discount']['type'] == '0' )
                            {
                                $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                            }
                            else
                            {
                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                            }

                            add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                        }

                        add_variable( 'inc_disc_price', $inc_disc_price );
                        add_variable( 'exc_disc_price', $exc_disc_price );

                        add_variable( 'inc_disc_adult', $inc_disc_adult );
                        add_variable( 'inc_disc_child', $inc_disc_child );
                        add_variable( 'inc_disc_infant', $inc_disc_infant );

                        add_variable( 'exc_disc_adult', $exc_disc_adult );
                        add_variable( 'exc_disc_child', $exc_disc_child );
                        add_variable( 'exc_disc_infant', $exc_disc_infant );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                        add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
		        	}
                    elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                    {
                        if( $p['agent_discount']['type'] == '0' )
                        {
                            $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                            $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                            $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                            $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                            $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                            $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                            $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                            $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                        }
                        else
                        {
                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                            $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                            $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                        }

                        add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                

                        add_variable( 'inc_disc_price', $inc_disc_price );
                        add_variable( 'exc_disc_price', $exc_disc_price );

                        add_variable( 'inc_disc_adult', $inc_disc_adult );
                        add_variable( 'inc_disc_child', $inc_disc_child );
                        add_variable( 'inc_disc_infant', $inc_disc_infant );

                        add_variable( 'exc_disc_adult', $exc_disc_adult );
                        add_variable( 'exc_disc_child', $exc_disc_child );
                        add_variable( 'exc_disc_infant', $exc_disc_infant );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                        add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                    }
		        	else
		        	{
                        add_variable( 'discount_notif', '' );

                        add_variable( 'inc_disc_price', 0 );
                        add_variable( 'exc_disc_price', 0 );

                        add_variable( 'inc_disc_adult', 0 );
                        add_variable( 'inc_disc_child', 0 );
                        add_variable( 'inc_disc_infant', 0 );

                        add_variable( 'exc_disc_adult', 0 );
                        add_variable( 'exc_disc_child', 0 );
                        add_variable( 'exc_disc_infant', 0 );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '' );
                        add_variable( 'exclude_price_disc_number', '' );
		        	}

                    add_variable( 'rtdid', $p['rtdid'] );

		        	add_variable( 'inc_total_price', $inc_total_price );
		        	add_variable( 'exc_total_price', $exc_total_price );

                    add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                    add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

					add_variable( 'passenger_number', get_front_passenger_num_content( $adult_num, $child_num, $infant_num ) );
		        	add_variable( 'route_list', get_front_route_detail_list_content( $d['rid'], $depart_port, $destination_port ) );
                    add_variable( 'transport_label', get_front_route_detail_transport_label( $d['rid'] ) );

		        	parse_template( 'result-loop-block', 'drlblock', true );

                    if( $remain_seat > 0 )
                    {
                        $i++;
                    }
                    else
                    {
                        $o++;
                    }
	        	}
	        }
        }

        if( empty( $i )  )
        {
            // i2 adalah counter untuk menghitung jumlah suggest trip dari semua fungsi
            $i2 = 0;
            
            // i3 adalah counter untuk menghitung jumlah suggest trip dari fungsi if pertama , yang kedatangan DAN tujuannya di custom seusai request
            $i3 = 0;
            
            // i4 adalah counter untuk menghitung jumlah suggest trip dari fungsi if kedua , yang awal ATAU akhirnya di custom seusai request
            $i4 = 0;
            
            
            $suggest_sess_data = $sess_data;
            if( empty( $suggest_depart_port ) === false )
            {
                if( ($depart_port == 12 && $destination_port == 8) || ( $depart_port == 12 && $destination_port == 9)){
                    
                    foreach( $schedule as $sc )
                        {
                            $suggest_sess_data['depart_port'] = $suggest_depart_port ['from'][0];
                            $suggest_sess_data['destination_port'] = $suggest_depart_port ['to'][0];
                            $not_closed_date   = is_date_not_in_close_online_allotment_list( $sc['sid'], $dep_date );
                            $available_depart  = is_available_depart_on_list( $sc['rid'], $suggest_sess_data['depart_port'], $dep_date );
                            $available_arrival = is_available_arrival_on_list( $sc['rid'], $suggest_sess_data['destination_port'] );
                            $available_date    = is_available_date_on_list( $sc['sid'], $dep_date );
                            $not_pass_time     = is_not_pass_time_limit( $sc['rid'], $suggest_sess_data, 'depart' );
         
                            if( $available_depart && $available_arrival && $available_date && $not_pass_time && $not_closed_date )
                            {
                                $sremain_seat = remain_seat( $dep_date, $sc['sid'], $sc['rid'], $pass_num, $sc['bopassenger'], $suggest_sess_data['depart_port'], $suggest_sess_data['destination_port'], null, false, null, true );
                                $snum_of_seat = $sremain_seat - $pass_num;

                                if( $sremain_seat > 0 )
                                {
                                    $p = get_result_price( $sc['sid'], $sc['rid'], $dep_date, $suggest_sess_data['depart_port'], $suggest_sess_data['destination_port'], $adult_num, $child_num, $infant_num, null );
                                    $a = get_boat_first_attachment( $sc['boid'] );

                                    $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                                    $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                                    if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                                    {
                                        if( is_block_request_transport( $sc['rid'], $suggest_sess_data, 'depart' ) )
                                        {
                                            add_variable( 'include_trans_css', 'sr-only' );
                                            add_variable( 'exclude_trans_css', '' );
                                        }
                                        else
                                        {
                                            add_variable( 'include_trans_css', '' );
                                            add_variable( 'exclude_trans_css', 'sr-only' );
                                        }

                                        add_variable( 'sid', $sc['sid'] );
                                        add_variable( 'rid', $sc['rid'] );
                                        add_variable( 'boid', $sc['boid'] );
                                        add_variable( 'boname', $sc['boname'] );
                                        add_variable( 'radio_name', 'dep_rid' );
                                        add_variable( 'bopicture', $a['thumbnail'] );

                                        add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                        add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                        add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                        add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                        add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                        add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                        add_variable( 'port_name', get_front_route_depart_first( $sc['rid'], $suggest_sess_data['depart_port'], $suggest_sess_data['destination_port'] ) );

                                        // if( $snum_of_seat < 7 )
                                        // {
                                        //     if( $snum_of_seat <= 0 )
                                        //     {
                                        //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', 'No seat left' ) );
                                        //     }
                                        //     elseif( $snum_of_seat == 1 )
                                        //     {
                                        //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', '1 seat left' ) );
                                        //     }
                                        //     else
                                        //     {
                                        //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', $num_of_seat . ' seats left' ) );
                                        //     }
                                        // }
                                        // else
                                        // {
                                        //     add_variable( 'remain_seat', '' );
                                        // }
                                        add_variable( 'remain_seat', '' );

                                        if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                                        {
                                            if( !empty( $p['early_discount'] ) )
                                            {
                                                if( $p['early_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                            }

                                            if( !empty( $p['seat_discount'] ) )
                                            {
                                                if( $p['seat_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                            }

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }                                        
                                        elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                                        {
                                            if( $p['agent_discount']['type'] == '0' )
                                            {
                                                $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                                                $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                                            }
                                            else
                                            {
                                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                            }

                                            add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }
                                        else
                                        {
                                            add_variable( 'discount_notif', '' );

                                            add_variable( 'inc_disc_price', 0 );
                                            add_variable( 'exc_disc_price', 0 );

                                            add_variable( 'inc_disc_adult', 0 );
                                            add_variable( 'inc_disc_child', 0 );
                                            add_variable( 'inc_disc_infant', 0 );

                                            add_variable( 'exc_disc_adult', 0 );
                                            add_variable( 'exc_disc_child', 0 );
                                            add_variable( 'exc_disc_infant', 0 );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '' );
                                            add_variable( 'exclude_price_disc_number', '' );
                                        }

                                        add_variable( 'rtdid', $p['rtdid'] );

                                        add_variable( 'inc_total_price', $inc_total_price );
                                        add_variable( 'exc_total_price', $exc_total_price );

                                        add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                        add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                                        add_variable( 'passenger_number', get_front_passenger_num_content( $adult_num, $child_num, $infant_num ) );
                                        add_variable( 'route_list', get_front_route_detail_list_content( $sc['rid'], $suggest_sess_data['depart_port'], $suggest_sess_data['destination_port'] ) );
                                        add_variable( 'transport_label', get_front_route_detail_transport_label( $sc['rid'] ) );

                                        parse_template( 'suggestion-result-loop-block', 'dsrlblock', true );

                                        $i2++;
                                        $i3++;
                                    }
                                }
                            }
                        }
                }
                
                if($i3 == 0){
                    foreach( $schedule as $sc )
                        {
                            $not_closed_date   = is_date_not_in_close_online_allotment_list( $sc['sid'], $dep_date );
                            $available_depart  = is_available_depart_on_list( $sc['rid'], $depart_port, $dep_date );
                            $available_arrival = is_available_arrival_on_list( $sc['rid'], $destination_port );
                            $available_date    = is_available_date_on_list( $sc['sid'], $dep_date );
                            $not_pass_time     = is_not_pass_time_limit( $sc['rid'], $sess_data, 'depart' );
                    
                            if(!$available_depart){
                                    $node_change = 0 ;
                                    if( $depart_port == 10 && $node_change == 0){
                                        $depart_port = 8;
                                        $node_change = 1;
                                    }
                                    if( $depart_port == 8 && $node_change == 0){
                                        $depart_port = 10;
                                        $node_change = 1;
                                    }
                                    if( $depart_port == 9 && $node_change == 0){
                                        $depart_port = 14;
                                        $node_change = 1;
                                    }
                                    if( $depart_port == 14 && $node_change == 0){
                                        $depart_port = 9;
                                        $node_change = 1;
                                    }
                                    $sess_data['depart_port'] = $depart_port;
                                }
                                
                            if(!$available_arrival){
                                $node_change = 0 ;
                                // rekomnendasi jika awal atau tujuan gili trawangan menjadi gili air
                                if( $destination_port == 10 && $node_change == 0){
                                    $destination_port = 8;
                                    $node_change = 1;
                                }
                                
                                // rekomnendasi jika awal atau tujuan gili air menjadi gili trawangan
                                if( $destination_port == 8 && $node_change == 0){
                                    $destination_port = 10;
                                    $node_change = 1;
                                }
                                
                                // rekomnendasi jika awal atau tujuan teluk kode menjadi bangsal
                                if( $destination_port == 9 && $node_change == 0){
                                    $destination_port = 14;
                                    $node_change = 1;
                                }
                                
                                // rekomnendasi jika awal atau tujuan bangsal menjadi teluk kode
                                if( $destination_port == 14 && $node_change == 0){
                                    $destination_port = 9;
                                    $node_change = 1;
                                }
                                 $sess_data['depart_port'] = $destination_port;
                            }
         
                            $not_closed_date   = is_date_not_in_close_online_allotment_list( $sc['sid'], $dep_date );
                            $available_depart  = is_available_depart_on_list( $sc['rid'], $depart_port, $dep_date );
                            $available_arrival = is_available_arrival_on_list( $sc['rid'], $destination_port );
                            $available_date    = is_available_date_on_list( $sc['sid'], $dep_date );
                            $not_pass_time     = is_not_pass_time_limit( $sc['rid'], $sess_data, 'depart' );
         
                            // if($sc['sid'] == 59){
                            //         print_r($depart_port."<br>");
                            //         print_r($destination_port."<br>");
                            //         print_r($suggest_sess_data['depart_port']."<br>");
                            //         print_r($suggest_sess_data['destination_port']."<br>");
                            //         print_r($available_depart."<br>".$available_arrival."<br>".$available_date."<br>".$not_pass_time."<br>".$not_closed_date);
                                    
                            // }
                            if( $available_depart && $available_arrival && $available_date && $not_pass_time && $not_closed_date )
                            {
                                $sremain_seat = remain_seat( $dep_date, $sc['sid'], $sc['rid'], $pass_num, $sc['bopassenger'], $depart_port, $destination_port, null, false, null, true );
                                $snum_of_seat = $sremain_seat - $pass_num;

                                if( $sremain_seat > 0 )
                                {
                                    $p = get_result_price( $sc['sid'], $sc['rid'], $dep_date, $depart_port, $destination_port, $adult_num, $child_num, $infant_num, null );
                                    $a = get_boat_first_attachment( $sc['boid'] );

                                    $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                                    $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                                    if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                                    {
                                        if( is_block_request_transport( $sc['rid'], $sess_data, 'depart' ) )
                                        {
                                            add_variable( 'include_trans_css', 'sr-only' );
                                            add_variable( 'exclude_trans_css', '' );
                                        }
                                        else
                                        {
                                            add_variable( 'include_trans_css', '' );
                                            add_variable( 'exclude_trans_css', 'sr-only' );
                                        }

                                        add_variable( 'sid', $sc['sid'] );
                                        add_variable( 'rid', $sc['rid'] );
                                        add_variable( 'boid', $sc['boid'] );
                                        add_variable( 'boname', $sc['boname'] );
                                        add_variable( 'radio_name', 'dep_rid' );
                                        add_variable( 'bopicture', $a['thumbnail'] );

                                        add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                        add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                        add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                        add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                        add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                        add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                        add_variable( 'port_name', get_front_route_depart_first( $sc['rid'], $depart_port, $destination_port ) );

                                        // if( $snum_of_seat < 7 )
                                        // {
                                        //     if( $snum_of_seat <= 0 )
                                        //     {
                                        //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', 'No seat left' ) );
                                        //     }
                                        //     elseif( $snum_of_seat == 1 )
                                        //     {
                                        //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', '1 seat left' ) );
                                        //     }
                                        //     else
                                        //     {
                                        //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', $num_of_seat . ' seats left' ) );
                                        //     }
                                        // }
                                        // else
                                        // {
                                        //     add_variable( 'remain_seat', '' );
                                        // }
                                        add_variable( 'remain_seat', '' );

                                        if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                                        {
                                            if( !empty( $p['early_discount'] ) )
                                            {
                                                if( $p['early_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                            }

                                            if( !empty( $p['seat_discount'] ) )
                                            {
                                                if( $p['seat_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                            }

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }                                        
                                        elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                                        {
                                            if( $p['agent_discount']['type'] == '0' )
                                            {
                                                $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                                                $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                                            }
                                            else
                                            {
                                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                            }

                                            add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }
                                        else
                                        {
                                            add_variable( 'discount_notif', '' );

                                            add_variable( 'inc_disc_price', 0 );
                                            add_variable( 'exc_disc_price', 0 );

                                            add_variable( 'inc_disc_adult', 0 );
                                            add_variable( 'inc_disc_child', 0 );
                                            add_variable( 'inc_disc_infant', 0 );

                                            add_variable( 'exc_disc_adult', 0 );
                                            add_variable( 'exc_disc_child', 0 );
                                            add_variable( 'exc_disc_infant', 0 );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '' );
                                            add_variable( 'exclude_price_disc_number', '' );
                                        }

                                        add_variable( 'rtdid', $p['rtdid'] );

                                        add_variable( 'inc_total_price', $inc_total_price );
                                        add_variable( 'exc_total_price', $exc_total_price );

                                        add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                        add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                                        add_variable( 'passenger_number', get_front_passenger_num_content( $adult_num, $child_num, $infant_num ) );
                                        add_variable( 'route_list', get_front_route_detail_list_content( $sc['rid'], $depart_port, $destination_port ) );
                                        add_variable( 'transport_label', get_front_route_detail_transport_label( $sc['rid'] ) );

                                        parse_template( 'suggestion-result-loop-block', 'dsrlblock', true );

                                        $i2++;
                                        $i4++;
                                    }
                                }
                            }
                        }
                }
                
                if($i4 == 0){
                  foreach( $suggest_depart_port as $stype => $sdepart_port )
                    {
                        foreach( $sdepart_port as $idx => $sport )
                        {
                            foreach( $schedule as $sc )
                            {
                                
                                if( $stype == 'from' )
                                {
                                    $suggest_sess_data['depart_port'] = $sport;
                                }
                                elseif( $stype == 'to' )
                                {
                                    $suggest_sess_data['destination_port'] = $sport;
                                }
                                
                                $not_closed_date   = is_date_not_in_close_online_allotment_list( $sc['sid'], $dep_date );
                                $available_depart  = is_available_depart_on_list( $sc['rid'], $suggest_sess_data['depart_port'], $dep_date );
                                $available_arrival = is_available_arrival_on_list( $sc['rid'], $suggest_sess_data['destination_port'] );
                                $available_date    = is_available_date_on_list( $sc['sid'], $dep_date );
                                $not_pass_time     = is_not_pass_time_limit( $sc['rid'], $suggest_sess_data, 'depart' );
                                
                                if( $available_depart && $available_arrival && $available_date && $not_pass_time && $not_closed_date )
                                {
                                    $sremain_seat = remain_seat( $dep_date, $sc['sid'], $sc['rid'], $pass_num, $sc['bopassenger'], $suggest_sess_data['depart_port'], $suggest_sess_data['destination_port'], null, false, null, true );
                                    $snum_of_seat = $sremain_seat - $pass_num;
                                    if( $sremain_seat > 0 )
                                    {
                                        $p = get_result_price( $sc['sid'], $sc['rid'], $dep_date, $suggest_sess_data['depart_port'], $suggest_sess_data['destination_port'], $adult_num, $child_num, $infant_num, null );
                                        $a = get_boat_first_attachment( $sc['boid'] );
    
                                        $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                                        $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );
    
                                        if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                                        {
                                            if( is_block_request_transport( $sc['rid'], $suggest_sess_data, 'depart' ) )
                                            {
                                                add_variable( 'include_trans_css', 'sr-only' );
                                                add_variable( 'exclude_trans_css', '' );
                                            }
                                            else
                                            {
                                                add_variable( 'include_trans_css', '' );
                                                add_variable( 'exclude_trans_css', 'sr-only' );
                                            }
    
                                            add_variable( 'sid', $sc['sid'] );
                                            add_variable( 'rid', $sc['rid'] );
                                            add_variable( 'boid', $sc['boid'] );
                                            add_variable( 'boname', $sc['boname'] );
                                            add_variable( 'radio_name', 'dep_rid' );
                                            add_variable( 'bopicture', $a['thumbnail'] );
    
                                            add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                            add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                            add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );
    
                                            add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                            add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                            add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );
    
                                            add_variable( 'port_name', get_front_route_depart_first( $sc['rid'], $suggest_sess_data['depart_port'], $suggest_sess_data['destination_port'] ) );
    
                                            // if( $snum_of_seat < 7 )
                                            // {
                                            //     if( $snum_of_seat <= 0 )
                                            //     {
                                            //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', 'No seat left' ) );
                                            //     }
                                            //     elseif( $snum_of_seat == 1 )
                                            //     {
                                            //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', '1 seat left' ) );
                                            //     }
                                            //     else
                                            //     {
                                            //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', $num_of_seat . ' seats left' ) );
                                            //     }
                                            // }
                                            // else
                                            // {
                                            //     add_variable( 'remain_seat', '' );
                                            // }
                                            add_variable( 'remain_seat', '' );
    
                                            if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                                            {
                                                if( !empty( $p['early_discount'] ) )
                                                {
                                                    if( $p['early_discount']['type'] == '0' )
                                                    {
                                                        $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                        $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                        $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];
    
                                                        $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                        $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                        $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];
    
                                                        $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                                        $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                                    }
                                                    else
                                                    {
                                                        $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                        $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                        $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;
    
                                                        $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                        $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                        $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;
    
                                                        $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                        $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                    }
    
                                                    add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                                }
    
                                                if( !empty( $p['seat_discount'] ) )
                                                {
                                                    if( $p['seat_discount']['type'] == '0' )
                                                    {
                                                        $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                        $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                        $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];
    
                                                        $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                        $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                        $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];
    
                                                        $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                                        $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                                    }
                                                    else
                                                    {
                                                        $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                        $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                        $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
    
                                                        $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                        $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                        $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
    
                                                        $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                        $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                    }
    
                                                    add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                                }
    
                                                add_variable( 'inc_disc_price', $inc_disc_price );
                                                add_variable( 'exc_disc_price', $exc_disc_price );
    
                                                add_variable( 'inc_disc_adult', $inc_disc_adult );
                                                add_variable( 'inc_disc_child', $inc_disc_child );
                                                add_variable( 'inc_disc_infant', $inc_disc_infant );
    
                                                add_variable( 'exc_disc_adult', $exc_disc_adult );
                                                add_variable( 'exc_disc_child', $exc_disc_child );
                                                add_variable( 'exc_disc_infant', $exc_disc_infant );
    
                                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );
    
                                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );
    
                                                add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                                add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );
    
                                                add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                                add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                            }                                        
                                            elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                                            {
                                                if( $p['agent_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];
    
                                                    $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];
    
                                                    $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
    
                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
    
                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }
    
                                                add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                
    
                                                add_variable( 'inc_disc_price', $inc_disc_price );
                                                add_variable( 'exc_disc_price', $exc_disc_price );
    
                                                add_variable( 'inc_disc_adult', $inc_disc_adult );
                                                add_variable( 'inc_disc_child', $inc_disc_child );
                                                add_variable( 'inc_disc_infant', $inc_disc_infant );
    
                                                add_variable( 'exc_disc_adult', $exc_disc_adult );
                                                add_variable( 'exc_disc_child', $exc_disc_child );
                                                add_variable( 'exc_disc_infant', $exc_disc_infant );
    
                                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );
    
                                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );
    
                                                add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                                add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );
    
                                                add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                                add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                            }
                                            else
                                            {
                                                add_variable( 'discount_notif', '' );
    
                                                add_variable( 'inc_disc_price', 0 );
                                                add_variable( 'exc_disc_price', 0 );
    
                                                add_variable( 'inc_disc_adult', 0 );
                                                add_variable( 'inc_disc_child', 0 );
                                                add_variable( 'inc_disc_infant', 0 );
    
                                                add_variable( 'exc_disc_adult', 0 );
                                                add_variable( 'exc_disc_child', 0 );
                                                add_variable( 'exc_disc_infant', 0 );
    
                                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );
    
                                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );
    
                                                add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                                add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );
    
                                                add_variable( 'include_price_disc_number', '' );
                                                add_variable( 'exclude_price_disc_number', '' );
                                            }
    
                                            add_variable( 'rtdid', $p['rtdid'] );
    
                                            add_variable( 'inc_total_price', $inc_total_price );
                                            add_variable( 'exc_total_price', $exc_total_price );
    
                                            add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                            add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );
    
                                            add_variable( 'passenger_number', get_front_passenger_num_content( $adult_num, $child_num, $infant_num ) );
                                            add_variable( 'route_list', get_front_route_detail_list_content( $sc['rid'], $suggest_sess_data['depart_port'], $suggest_sess_data['destination_port'] ) );
                                            add_variable( 'transport_label', get_front_route_detail_transport_label( $sc['rid'] ) );
    
                                            parse_template( 'suggestion-result-loop-block', 'dsrlblock', true );
    
                                            $i2++;
                                        }
                                    }
                                }
                                
                            }
                            
                        }
                    } 
                }
            }
            if( $i2 > 0 )
            {
                add_variable( 'suggested-trip', get_location( $suggest_sess_data['depart_port'], 'lcname' ) . ' to ' . get_location( $suggest_sess_data['destination_port'], 'lcname' ) );
                if ($i4 > 0){
                    add_variable( 'suggested-trip', get_location( $depart_port, 'lcname' ) . ' to ' . get_location( $destination_port, 'lcname' ) );
                }
                parse_template( 'suggestion-result-block', 'dsrblock' );
            }

            if( empty( $o ) )
            {
                parse_template( 'empty-loop-block', 'delblock' );
            }
    	}
    }
    else
    {
    	parse_template( 'empty-loop-block', 'delblock' );
    }

    return return_template( 'availability-depart-loop' );
}

function get_front_return_availability_result( $post )
{
	global $db;

	extract( $post );
    extract( $sess_data );

    $rtn_date = date( 'Y-m-d', strtotime( $date_of_return ) );
    $pass_num = $adult_num + $child_num + $infant_num;

	$s = 'SELECT * FROM ticket_schedule AS a
		  LEFT JOIN ticket_route AS b ON a.rid = b.rid
		  LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
    $q = $db->prepare_query( $s, $rtn_date, $rtn_date, 'publish', 'publish', '1' );
    $r = $db->do_query( $q );

	set_template( TEMPLATE_PATH . '/results-loop.html', 'availability-return-loop' );

    add_block( 'suggestion-result-loop-block', 'rsrlblock', 'availability-return-loop' );
    add_block( 'suggestion-result-block', 'rsrblock', 'availability-return-loop' );
    add_block( 'empty-loop-block', 'relblock', 'availability-return-loop' );
    add_block( 'result-loop-block', 'rrlblock', 'availability-return-loop' );

    if( $db->num_rows( $r ) > 0 )
    {
	    $i = 0;
        $o = 0;

        $schedule = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $schedule[] = $d;

            $not_closed_date   = is_date_not_in_close_online_allotment_list( $d['sid'], $rtn_date );
            $available_depart  = is_available_depart_on_list( $d['rid'], $return_port, $rtn_date );
            $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_port_rtn );
            $available_date    = is_available_date_on_list( $d['sid'], $rtn_date );
            $not_pass_time     = is_not_pass_time_limit( $d['rid'], $sess_data, 'return' );

        	if( $available_depart && $available_arrival && $available_date && $not_pass_time && $not_closed_date )
        	{
                $p = get_result_price( $d['sid'], $d['rid'], $rtn_date, $return_port, $destination_port_rtn, $adult_num, $child_num, $infant_num, null );
                $a = get_boat_first_attachment( $d['boid'] );

    			$inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
    			$exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                $remain_seat = remain_seat( $rtn_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $return_port, $destination_port_rtn, null, false, null, true );
                $num_of_seat = $remain_seat - $pass_num;

        		if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
        		{
                    if( is_block_request_transport( $d['rid'], $sess_data, 'return' ) )
                    {
                        add_variable( 'include_trans_css', 'sr-only' );
                        add_variable( 'exclude_trans_css', '' );
                    }
                    else
                    {
                        add_variable( 'include_trans_css', '' );
                        add_variable( 'exclude_trans_css', 'sr-only' );
                    }

		        	add_variable( 'sid', $d['sid'] );
		        	add_variable( 'rid', $d['rid'] );
		        	add_variable( 'boid', $d['boid'] );
		        	add_variable( 'boname', $d['boname'] );
		        	add_variable( 'radio_name', 'rtn_rid' );
                    add_variable( 'bopicture', $a['thumbnail'] );

		        	add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
		        	add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
		        	add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

		        	add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
		        	add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
		        	add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );
                    
                    add_variable( 'btnstatus', $not_closed_date && $remain_seat > 0 && $num_of_seat >= 0 ? '' : 'disabled' );
                    add_variable( 'port_name', get_front_route_return_first( $d['rid'], $destination_port_rtn, $return_port ) );

                    // if( $num_of_seat < 7 )
                    // {
                    //     if( $num_of_seat <= 0 )
                    //     {
                    //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', 'No seat left' ) );
                    //     }
                    //     elseif( $num_of_seat == 1 )
                    //     {
                    //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', '1 seat left' ) );
                    //     }
                    //     else
                    //     {
                    //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', $num_of_seat . ' seats left' ) );
                    //     }
                    // }
                    // else
                    // {
                    //     add_variable( 'remain_seat', '' );
                    // }
                    add_variable( 'remain_seat', '' );

		        	if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                    {
                        if( !empty( $p['early_discount'] ) )
                        {
                            if( $p['early_discount']['type'] == '0' )
                            {
                                $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                            }
                            else
                            {
                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                            }

                            add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                        }

                        if( !empty( $p['seat_discount'] ) )
                        {
                            if( $p['seat_discount']['type'] == '0' )
                            {
                                $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                            }
                            else
                            {
                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                            }

                            add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                        }

                        add_variable( 'inc_disc_price', $inc_disc_price );
                        add_variable( 'exc_disc_price', $exc_disc_price );

                        add_variable( 'inc_disc_adult', $inc_disc_adult );
                        add_variable( 'inc_disc_child', $inc_disc_child );
                        add_variable( 'inc_disc_infant', $inc_disc_infant );

                        add_variable( 'exc_disc_adult', $exc_disc_adult );
                        add_variable( 'exc_disc_child', $exc_disc_child );
                        add_variable( 'exc_disc_infant', $exc_disc_infant );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                        add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                    }
                    elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                    {
                        if( $p['agent_discount']['type'] == '0' )
                        {
                            $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                            $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                            $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                            $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                            $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                            $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                            $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                            $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                        }
                        else
                        {
                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                            $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                            $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                        }

                        add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                

                        add_variable( 'inc_disc_price', $inc_disc_price );
                        add_variable( 'exc_disc_price', $exc_disc_price );

                        add_variable( 'inc_disc_adult', $inc_disc_adult );
                        add_variable( 'inc_disc_child', $inc_disc_child );
                        add_variable( 'inc_disc_infant', $inc_disc_infant );

                        add_variable( 'exc_disc_adult', $exc_disc_adult );
                        add_variable( 'exc_disc_child', $exc_disc_child );
                        add_variable( 'exc_disc_infant', $exc_disc_infant );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                        add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                    }
                    else
                    {
                        add_variable( 'discount_notif', '' );

                        add_variable( 'inc_disc_price', 0 );
                        add_variable( 'exc_disc_price', 0 );

                        add_variable( 'inc_disc_adult', 0 );
                        add_variable( 'inc_disc_child', 0 );
                        add_variable( 'inc_disc_infant', 0 );

                        add_variable( 'exc_disc_adult', 0 );
                        add_variable( 'exc_disc_child', 0 );
                        add_variable( 'exc_disc_infant', 0 );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '' );
                        add_variable( 'exclude_price_disc_number', '' );
                    }

                    add_variable( 'rtdid', $p['rtdid'] );

		        	add_variable( 'inc_total_price', $inc_total_price );
		        	add_variable( 'exc_total_price', $exc_total_price );

                    add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                    add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

					add_variable( 'passenger_number', get_front_passenger_num_content( $adult_num, $child_num, $infant_num ) );
		        	add_variable( 'route_list', get_front_route_detail_list_content( $d['rid'], $return_port, $destination_port_rtn ) );
                    add_variable( 'transport_label', get_front_route_detail_transport_label( $d['rid'] ) );

		        	parse_template( 'result-loop-block', 'rrlblock', true );

                    if( $remain_seat > 0 )
                    {
                        $i++;
                    }
                    else
                    {
                        $o++;
                    }
		        }
	        }
        }

        if( empty( $i )  )
        {
            $i2 = 0;

            $suggest_sess_data = $sess_data;

            if( empty( $suggest_return_port ) === false )
            {
                foreach( $suggest_return_port as $stype => $sreturn_port )
                {
                    foreach( $sreturn_port as $idx => $sport )
                    {
                        foreach( $schedule as $sc )
                        {
                            if( $stype == 'from' )
                            {
                                $suggest_sess_data['return_port'] = $sport;
                            }
                            elseif( $stype == 'to' )
                            {
                                $suggest_sess_data['destination_port_rtn'] = $sport;
                            }

                            $not_closed_date   = is_date_not_in_close_online_allotment_list( $sc['sid'], $rtn_date );
                            $available_depart  = is_available_depart_on_list( $sc['rid'], $suggest_sess_data['return_port'], $rtn_date );
                            $available_arrival = is_available_arrival_on_list( $sc['rid'], $suggest_sess_data['destination_port_rtn'] );
                            $available_date    = is_available_date_on_list( $sc['sid'], $rtn_date );
                            $not_pass_time     = is_not_pass_time_limit( $sc['rid'], $suggest_sess_data, 'return' );

                            if( $available_depart && $available_arrival && $available_date && $not_pass_time && $not_closed_date )
                            {
                                $sremain_seat = remain_seat( $rtn_date, $sc['sid'], $sc['rid'], $pass_num, $sc['bopassenger'], $suggest_sess_data['return_port'], $suggest_sess_data['destination_port_rtn'], null, false, null, true );
                                $snum_of_seat = $sremain_seat - $pass_num;

                                if( $sremain_seat > 0 )
                                {
                                    $p = get_result_price( $sc['sid'], $sc['rid'], $rtn_date, $suggest_sess_data['return_port'], $suggest_sess_data['destination_port_rtn'], $adult_num, $child_num, $infant_num, null );
                                    $a = get_boat_first_attachment( $sc['boid'] );

                                    $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                                    $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                                    if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                                    {
                                        if( is_block_request_transport( $sc['rid'], $suggest_sess_data, 'return' ) )
                                        {
                                            add_variable( 'include_trans_css', 'sr-only' );
                                            add_variable( 'exclude_trans_css', '' );
                                        }
                                        else
                                        {
                                            add_variable( 'include_trans_css', '' );
                                            add_variable( 'exclude_trans_css', 'sr-only' );
                                        }

                                        add_variable( 'sid', $sc['sid'] );
                                        add_variable( 'rid', $sc['rid'] );
                                        add_variable( 'boid', $sc['boid'] );
                                        add_variable( 'boname', $sc['boname'] );
                                        add_variable( 'radio_name', 'rtn_rid' );
                                        add_variable( 'bopicture', $a['thumbnail'] );

                                        add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                        add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                        add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                        add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                        add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                        add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                        add_variable( 'port_name', get_front_route_return_first( $sc['rid'], $suggest_sess_data['destination_port_rtn'], $suggest_sess_data['return_port'] ) );

                                        // if( $snum_of_seat < 7 )
                                        // {
                                        //     if( $snum_of_seat <= 0 )
                                        //     {
                                        //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', 'No seat left' ) );
                                        //     }
                                        //     elseif( $snum_of_seat == 1 )
                                        //     {
                                        //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', '1 seat left' ) );
                                        //     }
                                        //     else
                                        //     {
                                        //         add_variable( 'remain_seat', sprintf( '<small>%s</small>', $num_of_seat . ' seats left' ) );
                                        //     }
                                        // }
                                        // else
                                        // {
                                        //     add_variable( 'remain_seat', '' );
                                        // }
                                        add_variable( 'remain_seat', '' );

                                        if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                                        {
                                            if( !empty( $p['early_discount'] ) )
                                            {
                                                if( $p['early_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                            }

                                            if( !empty( $p['seat_discount'] ) )
                                            {
                                                if( $p['seat_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                            }

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }
                                        elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                                        {
                                            if( $p['agent_discount']['type'] == '0' )
                                            {
                                                $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                                                $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                                            }
                                            else
                                            {
                                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                            }

                                            add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }
                                        else
                                        {
                                            add_variable( 'discount_notif', '' );

                                            add_variable( 'inc_disc_price', 0 );
                                            add_variable( 'exc_disc_price', 0 );

                                            add_variable( 'inc_disc_adult', 0 );
                                            add_variable( 'inc_disc_child', 0 );
                                            add_variable( 'inc_disc_infant', 0 );

                                            add_variable( 'exc_disc_adult', 0 );
                                            add_variable( 'exc_disc_child', 0 );
                                            add_variable( 'exc_disc_infant', 0 );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '' );
                                            add_variable( 'exclude_price_disc_number', '' );
                                        }

                                        add_variable( 'rtdid', $p['rtdid'] );

                                        add_variable( 'inc_total_price', $inc_total_price );
                                        add_variable( 'exc_total_price', $exc_total_price );

                                        add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                        add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                                        add_variable( 'passenger_number', get_front_passenger_num_content( $adult_num, $child_num, $infant_num ) );
                                        add_variable( 'route_list', get_front_route_detail_list_content( $sc['rid'], $suggest_sess_data['return_port'], $suggest_sess_data['destination_port_rtn'] ) );
                                        add_variable( 'transport_label', get_front_route_detail_transport_label( $sc['rid'] ) );

                                        parse_template( 'suggestion-result-loop-block', 'rsrlblock', true );

                                        $i2++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if( $i2 > 0 )
            {
                add_variable( 'suggested-trip', get_location( $suggest_sess_data['return_port'], 'lcname' ) . ' to ' . get_location( $suggest_sess_data['destination_port_rtn'], 'lcname' ) );

                parse_template( 'suggestion-result-block', 'rsrblock' );
            }

            if( empty( $o ) )
            {
                parse_template( 'empty-loop-block', 'relblock' );
            }
        }
    }
    else
    {
		parse_template( 'empty-loop-block', 'relblock' );
    }

    return return_template( 'availability-return-loop' );
}

function get_passenger_num_detail( $adult = 0, $child = 0, $infant = 0 )
{
	$content = array();

	if( !empty( $adult ) )
	{
		$content[] = count( $adult ) > 1 ? $adult . ' adults' : $adult . ' adult';
	}

	if( !empty( $child ) )
	{
		$content[] = count( $child ) > 1 ? $child . ' childs' : $child . ' child';
	}

	if( !empty( $infant ) )
	{
		$content[] = count( $infant ) > 1 ? $infant . ' infants' : $infant . ' infant';
	}

	if( !empty( $content ) )
	{
		return implode( ', ', $content );
	}
}

function get_type_of_route_txt( $type = 0 )
{
	if( empty( $type ) )
	{
		return 'One Way Trip';
	}
	elseif( $type = 1 )
	{
		return 'Return Trip';
	}
}

function get_front_route_depart_first( $rid, $depart, $arrive )
{
	global $db;
    $s = 'SELECT * FROM ticket_route_detail AS a
		  INNER JOIN ticket_location AS b ON a.lcid = b.lcid
		  WHERE rid = %d ORDER BY rdid ASC';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );

    $list   = '';
    $hopid  = '';
    $dep    = false;
    $ret    = false;

    $rt = get_route( $rid );

    if( $rt['rtype'] == '1' && !empty( $rt['rhoppingpoint'] ) )
    {
        $hopid = $rt['rhoppingpoint'];
    }

    $dnum = 1;
    $anum = 1;

    while( $d = $db->fetch_array( $r ) )
    {
    	if( $d['lcid'] == $depart && $dep == false )
    	{
    		$style = '';
    		$dep   = true;
    		$list .= $d['lcname']. ' (' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . ')';
    		return $list;
    	}
    }
}

function get_front_route_return_first( $rid, $depart, $arrive )
{
	global $db;

    $s = 'SELECT * FROM ticket_route_detail AS a
		  INNER JOIN ticket_location AS b ON a.lcid = b.lcid
		  WHERE rid = %d ORDER BY rdid ASC';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );

    $list   = '';
    $hopid  = '';
    $dep    = false;
    $ret    = false;

    $rt = get_route( $rid );

    if( $rt['rtype'] == '1' && !empty( $rt['rhoppingpoint'] ) )
    {
        $hopid = $rt['rhoppingpoint'];
    }

    $dnum = 1;
    $anum = 1;

    while( $d = $db->fetch_array( $r ) )
    {
    	if( $d['lcid'] == $arrive && $ret == false )
    	{
    		$style = '';
    		$ret   = true;
    		$list .= $d['lcname']. ' (' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . ')';
    		return $list;
    	}
    }

}

function get_front_route_detail_list_content( $rid, $depart, $arrive )
{
	global $db;

    $s = 'SELECT * FROM ticket_route_detail AS a
		  INNER JOIN ticket_location AS b ON a.lcid = b.lcid
		  WHERE rid = %d ORDER BY rdid ASC';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );

    $list   = '';
    $hopid  = '';
    $dep    = false;
    $ret    = false;

    $rt = get_route( $rid );

    if( $rt['rtype'] == '1' && !empty( $rt['rhoppingpoint'] ) )
    {
        $hopid = $rt['rhoppingpoint'];
    }

    $dnum = 1;
    $anum = 1;
    $count=0;
    $class="";

    while( $d = $db->fetch_array( $r ) )
    {
    	if( $d['lcid'] == $depart && $dep == false )
    	{
    		$style = '';
    		$dep   = true;
    		$class = "route-custom ";

            $variable_depart = strtotime( $d['rdtime'] );
            $variable_time_depart = date( 'H:i', strtotime( $d['rdtime'] ) ) ;
            add_variable('custom_departname', $d['lcname'].' ( '.$variable_time_depart.' )'   );
    	}
    	elseif( $d['lcid'] == $arrive && $ret == false )
    	{
    		$style = '';
    		$ret   = true;
    		$class = "route-custom ";
    	}
    	else
    	{
    		if( $dep == false )
    		{
    			$style = 'style="text-decoration:line-through; display:none;"';
    		}
    		elseif( $dep == true && $ret == true )
    		{
    			$style = 'style="text-decoration:line-through; display:none;"';
    		}
    	}

        //-- Set Label
        if( $d['rdtype'] == '3' )
        {
            $lbl = 'Clearance';
        }
        else if( $d['rdtype'] == '2' )
        {
            $arr = get_location( $arrive, 'lctype' );

            if( in_array( $arr, array( 0, 2 ) ) )
            {
                $lbl = 'Arr. ' . $anum . ' - ';

                $anum++;
            }
            else
            {
                $lbl = 'Dep. ' . $dnum . ' - ';

                $dnum++;
            }
        }
        else if( $d['rdtype'] == '1' )
        {
            $lbl = 'Arr. ' . $anum . ' - ';

            $anum++;
        }
        else if( $d['rdtype'] == '0' )
        {
            $lbl = 'Dep. ' . $dnum . ' - ';

            $dnum++;
        }
        $time_before_depart = isset($time_before_depart) ? $time_before_depart: "";
        $time_before_arrival = isset($time_before_arrival) ? $time_before_arrival: "";

        if($time_before_depart == "" && $time_before_arrival == ""){
            $time_before_depart = $d['rdtime2'];
            $time_before_arrival = $d['rdtime'] ;
        }

        $t_arrival = strtotime( $d['rdtime'] );
        $lastTime  = strtotime($time_before_depart);
        $firstTime   = strtotime( $d['rdtime'] );
        $timeDiff   = ($firstTime - $lastTime) / 60;

        $list .= '<li class= "different r-'.$count.'"' . $style . '><b>'.$timeDiff.' Minutes</b> <span class="arrow-down"></span></li>';

        $time_before_depart = $d['rdtime2'];
        $time_before_arrival = $d['rdtime'] ;


        if( $d['lcid'] == $hopid  )
        {
            if( $d['lcid'] == $arrive )
            {
                $arrival_time = strtotime( $d['rdtime2'] );
                $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>' . $d['lcname'] . '</b> </li>';
                $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
            }
            if( $lbl == "Clearance" )
            {
                $deapart_time = strtotime( $d['rdtime']);
                $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Clearance </b>  <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b> Port Clearance : ' . $d['lcname'] . '</b> </li>';
                $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' .  date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
            }
            if($count != 0 && $arrive != $d['lcid'] && $lbl != "Clearance")
            {
                $deapart_time = strtotime( $d['rdtime']);
                $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>' . $d['lcname'] . '</b> </li>';
                $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
            }
        }
        else
        {
                if($count == 0){
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>' . $d['lcname'] . '</b> </li>';
                    $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>'.date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                }
                if($arrive == $d['lcid']){
                    $deapart_time = strtotime( $d['rdtime'] );
                    $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>'. $d['lcname'] . '</b> </li>';
                }
                if( $lbl == "Clearance" )
                {
                    $deapart_time = strtotime( $d['rdtime']);
                    $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Clearance </b>  <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b> </li>';
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>  Port Clearance : ' . $d['lcname'] . '</b> </li>';
                    $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                }
                if($count != 0 && $arrive != $d['lcid'] && $lbl != "Clearance"){
                    // if($timeDiff == 10){
                    //     $deapart_time = strtotime( $d['rdtime'] );
                    //     $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( '-5 minutes', $deapart_time ) ) . '</b></li>';
                    //     $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>'. $d['lcname'] . '</b> </li>';
                    //     $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';

                    // }
                    // else{
                        $deapart_time = strtotime( $d['rdtime'] );
                        $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                        $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>'. $d['lcname'] . '</b> </li>';
                        $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                    // }
                }
        }

        if($style == ""){
            $count = $count + 1;
        }
    }

    return $list;
}
function get_front_passenger_num_content( $adult = 0, $child = 0, $infant = 0 )
{
	$content = '';

	if( !empty( $adult ) )
	{
		$content .= '<li>Adult <span>(' . $adult . ')</span></li>';
	}

	if( !empty( $child ) )
	{
		$content .= '<li>Child <span>(' . $child . ')</span></li>';
	}

	if( !empty( $infant ) )
	{
		$content .= '<li>Infant <span>(' . $infant . ')</span></li>';
	}

	return $content;
}

function get_front_route_detail_transport( $rid, $trans = 0 )
{
    if( $trans == 1 )
    {
    	$content = get_front_route_detail_transport_label( $rid );
    }
    else
    {
    	$content = '<li>Excluded</li>';
    }

    return $content;
}

function get_front_route_detail_transport_label( $rid )
{
    global $db;

    $s = 'SELECT COUNT( b.taid ) AS num FROM ticket_route_detail AS a
          RIGHT JOIN ticket_route_pickup_drop AS b ON b.rdid = a.rdid
          WHERE a.rid = %d';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $d = $db->fetch_array( $r );

        if( $d[ 'num' ] > 0 )
        {
            $content = '
            <li>Shared <span>(Free)</span></li>
            <li>Private <span>(Addtional)</span></li>';
        }
        else
        {
            $content = '
            <li>No Transport <span>(Own Transport)</span></li>';
        }
    }
    else
    {
        $content = '
        <li>No Transport <span>(Own Transport)</span></li>';
    }

    return $content;
}

function get_front_rate_price_content( $rid, $adult = 0, $child = 0, $infant = 0, $trip = array(), $return = false )
{
    if( empty( $trip ) )
    {
        return '';
    }
    else
    {
        extract( $trip );

        $content = '';

        if( !empty( $adult ) )
        {
            if( empty( $adult_price ) )
            {
                $content .= '<li><span>Free</span></li>';
            }
            else
            {
                $content .= '<li><span>' . number_format( $adult_price, 0, ',', '.' ) . '/adult</span></li>';
            }
        }

        if( !empty( $child ) )
        {
            if( empty( $child_price ) )
            {
                $content .= '<li><span>Free</span></li>';
            }
            else
            {
                $content .= '<li><span>' . number_format( $child_price, 0, ',', '.' ) . '/child</span></li>';
            }
        }

        if( !empty( $infant ) )
        {
            if( empty( $infant_price ) )
            {
                $content .= '<li><span>Free</span></li>';
            }
            else
            {
                $content .= '<li><span>' . number_format( $infant_price, 0, ',', '.' ) . '/infant</span></li>';
            }
        }

        return $content;
    }
}

function get_front_add_ons_content( $date, $trip_type, $sid = null, $bsource = null, $depart = null, $arrive = null, $exclude = false )
{
    global $db;

    $book = booking_item();
    $date = date( 'Y-m-d', strtotime( $date ) );
    $tval = base64_encode( json_encode( array( $sid, ( $exclude ? 0 : 1 ) ) ) );

    $s = 'SELECT * FROM ticket_add_ons AS a WHERE a.aostatus = %s AND a.aostart <= %s AND a.aoend >= %s';
    $q = $db->prepare_query( $s, '1', $date, $date );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $tprc = array( 'departure' => 0, 'return' => 0 );
        $list = '';

        while( $d = $db->fetch_array( $r ) )
        {
            //-- Check Schedule In Play
            if( empty( $d['aosid'] ) )
            {
                $scheck = true;
            }
            else
            {
                $aosid = json_decode( $d['aosid'], true );
                $aosid = $aosid !== null && json_last_error() === JSON_ERROR_NONE ? $aosid : array();

                if( empty( $aosid ) )
                {
                    $scheck = true;
                }
                else
                {
                    if( in_array( $sid, $aosid ) )
                    {
                        $scheck = true;
                    }
                    else
                    {
                        $scheck = false;
                    }
                }
            }

            //-- Check Booking Source In Play
            if( empty( $d['aobsource'] ) )
            {
                $bcheck = true;
            }
            else
            {
                $aobsource = json_decode( $d['aobsource'], true );
                $aobsource = $aobsource !== null && json_last_error() === JSON_ERROR_NONE ? $aobsource : array();

                if( empty( $aobsource ) )
                {
                    $bcheck = true;
                }
                else
                {
                    if( in_array( $bsource, $aobsource ) )
                    {
                        $bcheck = true;
                    }
                    else
                    {
                        $bcheck = false;
                    }
                }
            }

            //-- Check Point In Play
            if( empty( $d['aodepart'] ) && empty( $d['aoarrive'] ) )
            {
                $pcheck = true;
            }
            elseif( empty( $d['aodepart'] ) && !empty( $d['aoarrive'] ) )
            {
                $aoarrive = json_decode( $d['aoarrive'], true );
                $aoarrive = $aoarrive !== null && json_last_error() === JSON_ERROR_NONE ? $aoarrive : array();

                if( empty( $aoarrive ) )
                {
                    $pcheck = true;
                }
                else
                {
                    if( in_array( $arrive, $aoarrive ) )
                    {
                        $pcheck = true;
                    }
                    else
                    {
                        $pcheck = false;
                    }
                }
            }
            elseif( !empty( $d['aodepart'] ) && empty( $d['aoarrive'] ) )
            {
                $aodepart = json_decode( $d['aodepart'], true );
                $aodepart = $aodepart !== null && json_last_error() === JSON_ERROR_NONE ? $aodepart : array();

                if( empty( $aodepart ) )
                {
                    $pcheck = true;
                }
                else
                {
                    if( in_array( $depart, $aodepart ) )
                    {
                        $pcheck = true;
                    }
                    else
                    {
                        $pcheck = false;
                    }
                }
            }
            elseif( !empty( $d['aodepart'] ) && !empty( $d['aoarrive'] ) )
            {
                $aodepart = json_decode( $d['aodepart'], true );
                $aodepart = $aodepart !== null && json_last_error() === JSON_ERROR_NONE ? $aodepart : array();

                if( empty( $aodepart ) )
                {
                    $dcheck = true;
                }
                else
                {
                    if( in_array( $depart, $aodepart ) )
                    {
                        $dcheck = true;
                    }
                    else
                    {
                        $dcheck = false;
                    }
                }

                $aoarrive = json_decode( $d['aoarrive'], true );
                $aoarrive = $aoarrive !== null && json_last_error() === JSON_ERROR_NONE ? $aoarrive : array();

                if( empty( $aoarrive ) )
                {
                    $acheck = true;
                }
                else
                {
                    if( in_array( $arrive, $aoarrive ) )
                    {
                        $acheck = true;
                    }
                    else
                    {
                        $acheck = false;
                    }
                }

                if( $dcheck || $acheck )
                {
                    $pcheck = true;
                }
                else
                {
                    $pcheck = false;
                }
            }

            if( $scheck && $bcheck && $pcheck )
            {
                $notes    = '';
                $checked  = '';
                $quantity = 1;

                if( isset( $book['sess_data']['trip'][ $trip_type ]['addons'] ) )
                {
                    $found = array_search( $d['aoid'], array_combine( array_keys( $book['sess_data']['trip'][ $trip_type ]['addons'] ), array_column( $book['sess_data']['trip'][ $trip_type ]['addons'], 'id' ) ) );

                    if( $found !== false )
                    {
                        $checked  = 'checked';
                        $quantity = $book['sess_data']['trip'][ $trip_type ]['addons'][ $found ]['paxs'];
                        $notes    = $book['sess_data']['trip'][ $trip_type ]['addons'][ $found ]['notes'];

                        $tprc[ $trip_type ] = $tprc[ $trip_type ] + $book['sess_data']['trip'][ $trip_type ]['addons'][ $found ]['price'] * $quantity;
                    }
                }

                $list .= '
                <li class="' . ( $exclude && $d['aotype'] == '1' ? 'sr-only' : '' ) . '">
                    <label>
                        <input class="add-ons-check" data-aotype="' . $d['aotype'] . '" data-trip="' . $trip_type . '" type="checkbox" name="addons[' . $trip_type . '][' . $d['aoid'] . '][id]" value="' . $d['aoid'] . '" ' . $checked . ' autocomplete="off" />
                        <span>' . $d['aoname'] . '</span>
                    </label>
                    <div class="row">
                        <div class="cols col-md-10 clearfix">';

                            $picture = json_decode( $d['aopicture'], true );
                            $picture = $picture !== null && json_last_error() === JSON_ERROR_NONE ? $picture : array();

                            if( !empty( $picture ) && isset( $picture['medium'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $picture['medium'] ) )
                            {
                                $list .= '<img src="' . HTSERVER . site_url() . '/l-plugins/ticket/uploads/featured/' . $picture['medium'] . '" alt="Featured Image" />';
                            }

                            $list .= '
                            <div class="bx">
                                <p>' . $d['aobrief'] . '</p>';

                                if( !empty( $d['aodesc'] ) )
                                {
                                    $list .= '<a data-base-class="popup-add-ons" data-fancybox="addons-' . $sid . '-' . $d['aoid'] . '" data-src="#popup-add-ons-' . $sid . '-' . $d['aoid'] . '" href="javascript:;">View detail</a>';
                                }

                                $list .= '
                                <div class="clearfix">
                                    <textarea class="add-ons-notes" name="addons[' . $trip_type . '][' . $d['aoid'] . '][notes]" placeholder="Note here your choice/additional info..." row="2">' . $notes . '</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-12 ' . ( $d['aotype'] == '1' ? 'sr-only' : '' ) . '">
                            <div class="form-group">';

                                if( $d['aodiscount'] > 0 )
                                {
                                    $list .= '<p><b>Price :</b> <s style="color:red;">' . number_format( $d['aoprice'], 0, ',', '.' ) . '</s> &raquo; ' . number_format( ( $d['aoprice'] - $d['aodiscount'] ), 0, ',', '.' ) . '</p>';
                                }
                                else
                                {
                                    $list .= '<p><b>Price :</b> ' . number_format( $d['aoprice'], 0, ',', '.' ) . '</p>';
                                }

                                $list .= '
                                <input type="text" class="sr-only add-ons-price" name="addons[' . $trip_type . '][' . $d['aoid'] . '][price]" value="' . ( $d['aoprice'] - $d['aodiscount'] ) . '" />
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">QTY</div>
                                    <div class="number-field">
                                        <input type="number" data-trip="' . $trip_type . '" class="text form-control text-right add-ons-pax" name="addons[' . $trip_type . '][' . $d['aoid'] . '][paxs]" value="' . $quantity . '" min="0" oninput="this.value = Math.abs(this.value)">
                                        <div class="number-nav">
                                            <div class="number-button up">+</div>
                                            <div class="number-button down">-</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sr-only">
                            <div id="popup-add-ons-' . $sid . '-' . $d['aoid'] . '">
                                <h2>' . $d['aoname'] . '</h2>';

                                if( !empty( $picture ) && isset( $picture['large'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $picture['large'] ) )
                                {
                                    $list .= '
                                    <figure>
                                        <img src="' . HTSERVER . site_url() . '/l-plugins/ticket/uploads/featured/' . $picture['large'] . '" alt="Featured Image" />
                                    </figure>';
                                }

                                $list .= '
                                <article>
                                    ' . $d['aodesc'] . '
                                </article
                            </div>
                        </div>
                    </div>
                </li>';
            }
        }

        if( !empty( $list ) )
        {
            $content = '
            <div class="col-4 col-add-ons">
                <div class="text">
                    <div class="ao-title">Popular with our guests :</div>
                    <div class="ao-content">
                        <ul>
                            ' . $list . '
                        </ul>
                    </div>
                </div>
                <div class="text-desc">
                    <p class="addons-price-' . $trip_type . '">' . number_format( $tprc[ $trip_type ], 0, ',', '.' ) . '</p>
                </div>
            </div>';

            return $content;
        }
    }
}

function get_front_pickup_drop_off_transport( $rid, $from, $to, $transport_type = 0, $sess = array(), $is_return = false )
{
    $content   = '';
    $dtrans    = get_pickup_drop_list_data( $rid, $from );
    $atrans    = get_pickup_drop_list_data( $rid, $to );
    $trip_type = $is_return ? 'return' : 'departure';

    if( isset( $dtrans['pickup'] ) )
    {
        if( empty( $transport_type ) )
        {
            $content .= '
            <p class="own-trans">
                <b>Pickup : </b>
                <em>Own Transport &nbsp;</em>
                <span class="text-danger">Latest check in 30 minutes prior to departure</span>
                <input type="text" class="sr-only" name="trans[' . $trip_type . '][pickup][type]" value="2" />
            </p>';
        }
        else
        {
            $content .= get_front_pickup_drop_off_transport_detail( 'pickup', $dtrans['pickup'], $sess, $is_return );
        }
    }
    else
    {
        $ltype = get_location( $from, 'lctype' );

        if( in_array( $ltype, array( '0', '2' ) ) )
        {
            $content .= '
            <p class="own-trans">
                <b>Pickup : </b>
                <em>Own Transport &nbsp;</em>
                <span class="text-danger">Latest check in 30 minutes prior to departure</span>
                <input type="text" class="sr-only" name="trans[' . $trip_type . '][pickup][type]" value="2" />
            </p>';
        }
    }

    if( isset( $atrans['drop-off'] ) )
    {
        if( empty( $transport_type ) )
        {
            $content .= '
            <p class="own-trans">
                <b>Drop-off : </b>
                <em>Own Transport &nbsp;</em>
                <span class="text-danger">Latest check in 30 minutes prior to departure</span>
                <input type="text" class="sr-only" name="trans[' . $trip_type . '][drop-off][type]" value="2" />
            </p>';
        }
        else
        {
            $content .= get_front_pickup_drop_off_transport_detail( 'drop-off', $atrans['drop-off'], $sess, $is_return );

        }
    }
    else
    {
        $ltype = get_location( $from, 'lctype' );

        if( in_array( $ltype, array( '1', '2' ) ) )
        {
            $content .= '
            <p class="own-trans">
                <b>Drop-off : </b>
                <em>Own Transport &nbsp;</em>
                <span class="text-danger">Latest check in 30 minutes prior to departure</span>
                <input type="text" class="sr-only" name="trans[' . $trip_type . '][drop-off][type]" value="2" />
            </p>';
        }
    }

    return '
    <div class="text">'
        . $content . '
    </div>';
}

function get_front_pickup_drop_off_transport_detail( $trans_type, $trans = array(), $sess = array(), $is_return = false )
{
	extract( $sess['trip'] );

    //-- Get Transport Fee and Flight Text
    $flight = $trans_type == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
    $pass   = $sess['adult_num'] + $sess['child_num'] + $sess['infant_num'];
    $count  = ceil( $pass / 4 );

    if( $is_return )
    {
        // ttype diset sementara menjadi owntransport
        $ttype   = isset( $return[$trans_type]['transport_type'] ) ? $return[$trans_type]['transport_type'] : 2;
        $ftime   = isset( $return[$trans_type]['flight_time'] ) ? $return[$trans_type]['flight_time'] : '';
        $hid     = isset( $return[$trans_type]['hotel_id'] ) ? $return[$trans_type]['hotel_id'] : '';
        $taid    = isset( $return[$trans_type]['area_id'] ) ? $return[$trans_type]['area_id'] : '';
        $airport = isset( $return[$trans_type]['airport'] ) ? $return[$trans_type]['airport'] : 0;
        $rpfrom  = isset( $return[$trans_type]['rpfrom'] ) ? $return[$trans_type]['rpfrom'] : '';
        $rpto    = isset( $return[$trans_type]['rpto'] ) ? $return[$trans_type]['rpto'] : '';
        $hstatus = empty( $taid ) ? 'disabled' : '';
        $astatus = $ttype == 2 ? 'disabled' : '';

        $area_option  = get_front_area_option_by_trip( $taid, $trans, $count, $ttype );
        $hotel_option = get_front_hotel_option_by_trip( $hid, $trans, $count, $ttype );
		$content = '
        <div class="dropoff-text">
            ' . ucfirst( $trans_type ) . ' Address :
        </div>
        <div class="container-select-choose clearfix">
            <div class="select-opt-transport">
                <select class="option transport-type" name="trans[return][' . $trans_type . '][type]" autocomplete="off">                    
                    <option value="0" ' . ( $ttype == 0 ? 'selected' : '' ) . '>Shared Transport</option>
                    <option value="1" ' . ( $ttype == 1 ? 'selected' : '' ) . '>Private Transport</option>
                    <option value="2" ' . ( $ttype == 2 ? 'selected' : '' ) . '>Own Transport</option>
                </select>
            </div>
            <div class="select-opt-area">
                <select class="option hotels-area required" name="trans[return][' . $trans_type . '][taid]" autocomplete="off" data-error="Area can\'t be empty" ' . $astatus . '>
                    <option value="" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="0" data-fee="0" data-fee-num="Free">No Accommodation Booked</option>
                    ' . $area_option . '
                </select>
            </div>
            <div class="select-opt-hotel">
                <select class="option hotels required" name="trans[return][' . $trans_type . '][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" ' . $hstatus . '>
                    ' . $hotel_option . '
                </select>
                <div class="area-text">
                    <div class="input-group flight-time ' . ( $airport == 1 ? '' : 'sr-only' ) . '">
                        <span class="input-group-addon">' . $flight . ' : </span>
                        <input type="text" class="trans-ftime" name="trans[return][' . $trans_type . '][ftime]" value="' . $ftime . '" readonly="readonly" />
                        <input type="text" class="trans-airport sr-only" name="trans[return][' . $trans_type . '][taairport]" value="' . $airport . '" readonly="readonly" />
                    </div>
                    <input type="text" name="trans[return][' . $trans_type . '][trip-type]"  class="trip-type sr-only" value="return" />
                    <input type="text" name="trans[return][' . $trans_type . '][rpto]"  class="trans-rpto sr-only" value="' . $rpto . '" />
                    <input type="text" name="trans[return][' . $trans_type . '][rpfrom]"  class="trans-rpfrom sr-only" value="' . $rpfrom . '" />
                    <input type="text" name="trans[return][' . $trans_type . '][trans-type]"  class="trans-type sr-only" value="' . $trans_type . '" />
                </div>
            </div>
        </div>';
	}
	else
	{
        $ttype   = isset( $departure[$trans_type]['transport_type'] ) ? $departure[$trans_type]['transport_type'] : 2;
        $ftime   = isset( $departure[$trans_type]['flight_time'] ) ? $departure[$trans_type]['flight_time'] : '';
        $hid     = isset( $departure[$trans_type]['hotel_id'] ) ? $departure[$trans_type]['hotel_id'] : '';
        $taid    = isset( $departure[$trans_type]['area_id'] ) ? $departure[$trans_type]['area_id'] : '';
        $airport = isset( $departure[$trans_type]['airport'] ) ? $departure[$trans_type]['airport'] : 0;
        $rpfrom  = isset( $departure[$trans_type]['rpfrom'] ) ? $departure[$trans_type]['rpfrom'] : '';
        $rpto    = isset( $departure[$trans_type]['rpto'] ) ? $departure[$trans_type]['rpto'] : '';
        $hstatus = empty( $taid ) ? 'disabled' : '';
        $astatus = $ttype == 2 ? 'disabled' : '';

        $area_option  = get_front_area_option_by_trip( $taid, $trans, $count, $ttype );
        $hotel_option = get_front_hotel_option_by_trip( $hid, $trans, $count, $ttype );
        // <option value="0" ' . ( $ttype == 0 ? 'selected' : '' ) . '>Shared Transport</option>
        $content = '
        <div class="pickups-text">
            ' . ucfirst( $trans_type ) . ' Address :
        </div>
        <div class="container-select-choose clearfix">
            <div class="select-opt-transport">
                <select class="option transport-type" name="trans[departure][' . $trans_type . '][type]" autocomplete="off">                   
                    <option value="0" ' . ( $ttype == 0 ? 'selected' : '' ) . '>Shared Transport</option>
                    <option value="1" ' . ( $ttype == 1 ? 'selected' : '' ) . '>Private Transport</option>
                    <option value="2" ' . ( $ttype == 2 ? 'selected' : '' ) . '>Own Transport</option>
                </select>
            </div>
            <div class="select-opt-area">
                <select class="option hotels-area required" name="trans[departure][' . $trans_type . '][taid]" autocomplete="off" data-error="Area can\'t be empty" ' . $astatus . '>
                    <option value="" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="0" data-fee="0" data-fee-num="Free">No Accommodation Booked</option>
                    ' . $area_option . '
                </select>
            </div>
            <div class="select-opt-hotel">
                <select class="option hotels required" name="trans[departure][' . $trans_type . '][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" ' . $hstatus . '>
                    ' . $hotel_option . '
                </select>

                <div class="area-text">
                    <div class="input-group flight-time ' . ( $airport == 1 ? '' : 'sr-only' ) . '">
                        <span class="input-group-addon">' . $flight . ' : </span>
                        <input type="text" class="trans-ftime" name="trans[departure][' . $trans_type . '][ftime]" value="' . $ftime . '" readonly="readonly" />
                        <input type="text" class="trans-airport sr-only" name="trans[departure][' . $trans_type . '][taairport]" value="' . $airport . '" readonly="readonly" />
                    </div>
                    <input type="text" name="trans[departure][' . $trans_type . '][trip-type]" class="trip-type sr-only" value="departure" />
                    <input type="text" name="trans[departure][' . $trans_type . '][rpto]"  class="trans-rpto sr-only" value="' . $rpto . '" />
                    <input type="text" name="trans[departure][' . $trans_type . '][rpfrom]"  class="trans-rpfrom sr-only" value="' . $rpfrom . '" />
                    <input type="text" name="trans[departure][' . $trans_type . '][trans-type]"  class="trans-type sr-only" value="' . $trans_type . '" />
                </div>
            </div>
        </div>';
	}

	return $content;
}

function get_front_pickup_drop_off_transport_price( $rid, $from, $to, $transport_type = 0, $sess = array(), $return = false )
{
    $dtrans    = get_pickup_drop_list_data( $rid, $from );
    $atrans    = get_pickup_drop_list_data( $rid, $to );
    $trip_type = $return ? 'return' : 'departure';
    $content   = '';

    if( empty( $transport_type ) )
    {
        $trans_class = isset( $dtrans['pickup'] ) && isset( $atrans['drop-off'] ) ? 'both' : '';

        if( isset( $dtrans['pickup'] ) )
        {
            $content .= '
            <div class="own-transport pickup-trans trans ' . $trans_class . '">
                <p>Additional Fee</p>
                <input type="text" name="trans[' . $trip_type . '][pickup][trans-fee]" class="pickup-trans-fee sr-only" value="0" />
            </div>';
        }

        if( isset( $atrans['drop-off'] ) )
        {
            $content .= '
            <div class="own-transport drop-off-trans trans ' . $trans_class . '">
                <p>Additional Fee</p>
                <input type="text" name="trans[' . $trip_type . '][drop-off][trans-fee]" class="drop-off-trans-fee sr-only" value="0" />
            </div>';
        }
    }
    else
    {
        if( isset( $sess['trip'] ) )
        {
            $trip_data   = $return ? $sess['trip']['return'] : $sess['trip']['departure'];
            $trans_class = isset( $dtrans['pickup'] ) && isset( $atrans['drop-off'] ) ? 'both' : '';

            if( isset( $dtrans['pickup'] ) )
            {
                $fee      = isset( $trip_data['pickup']['transport_fee'] ) ? $trip_data['pickup']['transport_fee'] : 0;
                $content .= '
                <div class="pickup-trans trans has-opt ' . $trans_class . '">
                    <p>' . ( $fee == 0 ? 'Additional Fee' : number_format( $fee, 0, ',', '.' ) ) . '</p>
                    <input type="text" name="trans[' . $trip_type . '][pickup][trans-fee]" class="pickup-trans-fee sr-only" value="' . $fee . '" />
                </div>';
            }

            if( isset( $atrans['drop-off'] ) )
            {
                $fee      = isset( $trip_data['drop-off']['transport_fee'] ) ? $trip_data['drop-off']['transport_fee'] : 0;
                $content .= '
                <div class="drop-off-trans trans has-opt ' . $trans_class . '">
                    <p>' . ( $fee == 0 ? 'Additional Fee' : number_format( $fee, 0, ',', '.' ) ) . '</p>
                    <input type="text" name="trans[' . $trip_type . '][drop-off][trans-fee]" class="drop-off-trans-fee sr-only" value="' . $fee . '" />
                </div>';
            }
        }
        else
        {
            if( isset( $dtrans['pickup'] ) )
            {
                $content .= '
                <div class="pickup-trans trans">
                    <p>Additional Fee</p>
                    <input type="text" name="trans[' . $trip_type . '][pickup][trans-fee]" class="pickup-trans-fee sr-only" value="0" />
                </div>';
            }

            if( isset( $atrans['drop-off'] ) )
            {
                $content .= '
                <div class="drop-off-trans trans">
                    <p>Additional Fee</p>
                    <input type="text" name="trans[' . $trip_type . '][drop-off][trans-fee]" class="drop-off-trans-fee sr-only" value="0" />
                </div>';
            }
        }
    }

    return $content;
}

function get_front_area_option_by_trip( $taid, $trans, $count = 0, $ttype = 0 )
{
    $options   = '';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        foreach( $trans as $d )
        {
            $fee      = $d['tafee'] * $count;
            $fee_num  = number_format( $fee, 0, ',', '.' );
            $selected = $taid == $d['taid'] ? 'selected' : '';

            $options .= '
            <option value="' . $d['taid'] . '"
                data-supporttrans="' . $d['tatypeforonline'] . '"
                data-rpfrom="' . $d['rpfrom'] . '"
                data-rpto="' . $d['rpto'] . '"
                data-area-id="' . $d['taid'] . '"
                data-area="' . $d['taname'] . '"
                data-airport="' . $d['taairport'] . '"
                data-fee="' . $fee . '"
                data-fee-num="' . $fee_num . '" ' . $selected . '>
                ' . $d['taname'] . '
            </option>';
        }

        return $options;
    }
}

function get_front_hotel_option_by_trip( $hid, $trans, $count = 0, $ttype = 0 )
{
    global $db;

    $options   = '
    <option value="1" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="" data-address="" data-phone="" data-email="" data-fee="0" data-fee-num="Free" ' . ( $hid == 1  ? 'selected' : '' ) . '>Not in list/To be Advised</option>
    <!--option value="2" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="" data-address="" data-phone="" data-email="" data-fee="0" data-fee-num="Free" ' . ( $hid == 2 ? 'selected' : '' ) . '>Other Hotel</option-->';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        $rpfrom = array();
        $rpto   = array();
        $area   = array();
        $lcid   = '';

        foreach( $trans as $t )
        {
            $rpfrom[ $t['taid'] ] = $t['rpfrom'];
            $rpto[ $t['taid'] ]   = $t['rpto'];

            $area[] = $t['taid'];
            $lcid   = $t['lcid'];
        }

        $s = 'SELECT * FROM ticket_hotel AS a
              LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
              LEFT JOIN ticket_transport_area_fee AS c ON c.taid = b.taid
              WHERE a.hstatus = %d AND c.lcid = %s AND a.taid IN ( ' . implode( ',', $area ) . ' ) ORDER BY a.hname';
        $q = $db->prepare_query( $s, 1, $lcid );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            $fee      = $d['tafee'] * $count;
            $fee_num  = number_format( $fee, 0, ',', '.' );
            $selected = $hid == $d['hid'] ? 'selected' : '';

            $options .= '
            <option value="' . $d['hid'] . '"
                data-supporttrans="' . $d['tatypeforonline'] . '"
                data-rpfrom="' . $rpfrom[ $d['taid'] ] . '"
                data-rpto="' . $rpto[ $d['taid'] ] . '"
                data-area-id="' . $d['taid'] . '"
                data-area="' . $d['taname'] . '"
                data-airport="' . $d['taairport'] . '"
                data-fee="' . $fee . '"
                data-fee-num="' . $fee_num . '" ' . $selected . '
                data-address="' . $d['haddress'] . '"
                data-phone="' . $d['hphone'] . '"
                data-email="' . $d['hemail'] . '">
                ' . $d['hname'] . '
            </option>';
        }

        return $options;
    }
}

function get_front_country_list_option( $id = null, $use_empty = true, $empty_text = 'Select Country' )
{
    global $db;

    $s = 'SELECT * FROM l_country WHERE lcountry_id <> %d ORDER BY lcountry ASC';
    $q = $db->prepare_query( $s, 248 );
    $r = $db->do_query( $q );

    $content = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $content .= '<option value="' . $d['lcountry_id'] . '" ' . ( $d['lcountry_id'] == $id ? 'selected' : '' ) . '>' . $d['lcountry'] . '</option>';
        }
    }

    return $content;
}

function passenger_front_field_list_content( $adult = 0, $child = 0, $infant = 0, $is_return = false )
{
	$content   = '';
	$trip_type = $is_return ? 'return' : 'departure';

	if( !empty( $adult ) )
	{
		for( $i = 0; $i < $adult; $i++ )
		{
    		$content .= '
    		<div class="form-passenger">
                <div class="head-form-passenger">
                    Adult ' . ( $i + 1 ) . '
                </div>
                <div class="form-group full-name">
                    <label for="">Full Name <span>*</span></label>
                    <input type="text" class="form-control required" name="gfullname[' . $trip_type . '][adult][' . ( $i + 1 ) . ']" />
                </div>
                <div class="form-group gender">
                    <label style="width:100%;" for="">Gender<span>*</span></label>
                    <div class="circle">
                        <input id="gender-option-1" value="1" name="ggender[' . $trip_type . '][adult][' . ( $i + 1 ) . ']" autocomplete="off" type="radio" checked />
                        <span>Male</span>
                    </div>
                    <div class="circle">
                        <input id="gender-option-2" value="0" name="ggender[' . $trip_type . '][adult][' . ( $i + 1 ) . ']" autocomplete="off" type="radio"  />
                        <span>Female</span>
                    </div>
                </div>
                <div class="form-group birthday">
                    <label for="">Date Of Birth<span>*</span></label>
                    <input type="text" class="form-control txt-date required" name="gbirth[' . $trip_type . '][adult][' . ( $i + 1 ) . ']" readonly />
                </div>
                <div class="form-group nationality">
                    <label for="">Nationality <span>*</span></label>
                    <select class="form-control required" name="gcountry[' . $trip_type . '][adult][' . ( $i + 1 ) . ']">
						' . get_front_country_list_option() . '
                    </select>
                </div>
            </div>';
		}
	}

	if( !empty( $child ) )
	{
		for( $i = 0; $i < $child; $i++ )
		{
    		$content .= '
    		<div class="form-passenger">
                <div class="head-form-passenger">
                    Child ' . ( $i + 1 ) . '
                </div>
                <div class="form-group full-name">
                    <label for="">Full Name <span>*</span></label>
                    <input type="text" class="form-control required" name="gfullname[' . $trip_type . '][child][' . ( $i + 1 ) . ']" />
                </div>
                <div class="form-group gender">
                    <label style="width:100%;" for="">Gender<span>*</span></label>
                    <div class="circle">
                        <input id="gender-option-1" value="1" name="ggender[' . $trip_type . '][child][' . ( $i + 1 ) . ']" autocomplete="off" type="radio" checked />
                        <span>Male</span>
                    </div>
                    <div class="circle">
                        <input id="gender-option-2" value="0" name="ggender[' . $trip_type . '][child][' . ( $i + 1 ) . ']" autocomplete="off" type="radio"  />
                        <span>Female</span>
                    </div>
                </div>
                <div class="form-group birthday">
                    <label for="">Date Of Birth<span>*</span></label>
                    <input type="text" class="form-control txt-date-child required" name="gbirth[' . $trip_type . '][child][' . ( $i + 1 ) . ']" readonly />
                </div>
                <div class="form-group nationality">
                    <label for="">Nationality <span>*</span></label>
                    <select class="form-control required" name="gcountry[' . $trip_type . '][child][' . ( $i + 1 ) . ']">
						' . get_front_country_list_option() . '
                    </select>
                </div>
            </div>';
		}
	}

	if( !empty( $infant ) )
	{
		for( $i = 0; $i < $infant; $i++ )
		{
    		$content .= '
    		<div class="form-passenger">
                <div class="head-form-passenger">
                    Infant ' . ( $i + 1 ) . '
                </div>
                <div class="form-group full-name">
                    <label for="">Full Name <span>*</span></label>
                    <input type="text" class="form-control required" name="gfullname[' . $trip_type . '][infant][' . ( $i + 1 ) . ']" />
                </div>
                <div class="form-group gender">
                    <label style="width:100%;" for="">Gender<span>*</span></label>
                    <div class="circle">
                        <input id="gender-option-1" value="1" name="ggender[' . $trip_type . '][infant][' . ( $i + 1 ) . ']" autocomplete="off" type="radio" checked />
                        <span>Male</span>
                    </div>
                    <div class="circle">
                        <input id="gender-option-2" value="0" name="ggender[' . $trip_type . '][infant][' . ( $i + 1 ) . ']" autocomplete="off" type="radio"  />
                        <span>Female</span>
                    </div>
                </div>
                <div class="form-group birthday">
                    <label for="">Date Of Birth<span>*</span></label>
                    <input type="text" class="form-control txt-date-infant required" name="gbirth[' . $trip_type . '][infant][' . ( $i + 1 ) . ']" readonly />
                </div>
                <div class="form-group nationality">
                    <label for="">Nationality <span>*</span></label>
                    <select class="form-control required" name="gcountry[' . $trip_type . '][infant][' . ( $i + 1 ) . ']">
						' . get_front_country_list_option() . '
                    </select>
                </div>
            </div>';
		}
	}

	return $content;
}

function get_front_transport_data( $trip, $trans_type )
{
    global $db;

    $transport = '';

    if( empty( $trans_type ) )
    {
        if( isset( $trip['pickup'] ) )
        {
            $transport .= '<li>Pickup <br><span>Own Transport</span></li>';
        }

        if( isset( $trip['drop-off'] ) )
        {
            $transport .= '<li>Drop-off <br><span>Own Transport</span></li>';
        }
    }
    else
    {
        if( isset( $trip['pickup'] ) )
        {
            $taid   = isset( $trip['pickup']['area_id'] ) ? $trip['pickup']['area_id'] : '';
            $trans  = isset( $trip['pickup']['transport_type'] ) ? $trip['pickup']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '<li>Pickup <br><span>Own Transport</span></li>';
            }
            else
            {
                if( $trans !='' && $taid != '' )
                {
                    $taname = get_transport_area( $taid, 'taname' );
                    $rpto   = isset( $trip['pickup']['rpto'] ) && !empty( $trip['pickup']['rpto'] ) ? date( 'H:i', strtotime( $trip['pickup']['rpto'] ) ) : '';
                    $rpfrom = isset( $trip['pickup']['rpfrom'] ) && !empty( $trip['pickup']['rpfrom'] ) ? date( 'H:i', strtotime( $trip['pickup']['rpfrom'] ) ) : '';
                    $note   = empty( $rpfrom ) && empty( $rpto ) ? '' : '<br /><span>in between ' . $rpfrom . ' - ' . $rpto . '</span>';

                    $transport .= '
                    <li>
                        Pickup <br />
                        <span>' . ( $trans == 0 ? 'Shared Transport to ' . $taname : 'Private Transport to ' . $taname ) . '</span>
                        ' . $note . '
                    </li>';
                }
                elseif( $trans !='' && $taid == '' )
                {
                    $transport .= '<li>Pickup <br /><span>' . ( $trans == 0 ? 'Shared Transport' : 'Private Transport' ) . '</span></li>';
                }
                else
                {
                    $transport .= '<li>Pickup <br><span>-</span></li>';
                }
            }
        }

        if( isset( $trip['drop-off'] ) )
        {
            $taid  = isset( $trip['drop-off']['area_id'] ) ? $trip['drop-off']['area_id'] : '';
            $trans = isset( $trip['drop-off']['transport_type'] ) ? $trip['drop-off']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '<li>Drop-off <br><span>Own Transport</span></li>';
            }
            else
            {
                if( $trans !='' && $taid != '' )
                {
                    $taname = get_transport_area( $taid, 'taname' );
                    $rpto   = isset( $trip['drop-off']['rpto'] ) && !empty( $trip['drop-off']['rpto'] )  ? date( 'H:i', strtotime( $trip['drop-off']['rpto'] ) ) : '';
                    $rpfrom = isset( $trip['drop-off']['rpfrom'] ) && !empty( $trip['drop-off']['rpfrom'] )  ? date( 'H:i', strtotime( $trip['drop-off']['rpfrom'] ) ) : '';
                    $note   = empty( $rpfrom ) && empty( $rpto ) ? '' : '<br /><span>in between ' . $rpfrom . ' - ' . $rpto . '</span>';

                    $transport .= '
                    <li>
                        Drop-off <br />
                        <span>' . ( $trans == 0 ? 'Shared Transport to ' . $taname : 'Private Transport to ' . $taname ) . '</span>
                        ' . $note . '
                    </li>';
                }
                elseif( $trans !='' && $taid == '' )
                {
                    $transport .= '<li>Drop-off <br /><span>' . ( $trans == 0 ? 'Shared Transport' : 'Private Transport' ) . '</span></li>';
                }
                else
                {
                    $transport .= '<li>Drop-off <br><span>-</span></li>';
                }
            }
        }
    }

    return $transport;
}

function get_front_depart_transport_detail( $trip, $trans_type )
{
    global $db;

    $transport = '';

	if( empty( $trans_type ) )
	{
        if( isset( $trip['pickup'] ) )
        {
            $transport .= '
    		<div class="pickup-detail-passenger">
                <div class="title">Pickup</div>
                <div class="text">Own Transport</div>
                <div class="form-group">
                    <label for="">Driver\'s Name</label>
                    <input type="text" class="form-control" name="dep_pickup_driver_name" />
                </div>
                <div class="form-group">
                    <label for="">Driver\'s Phone</label>
                    <input type="text" class="form-control" name="dep_pickup_driver_phone" />
                </div>
            </div>';
        }

        if( isset( $trip['drop-off'] ) )
        {
            $transport .= '
            <div class="dropoff-detail-passenger">
                <div class="title">Drop-off</div>
                <div class="text">Own Transport</div>
                <div class="form-group">
                    <label for="">Driver\'s Name</label>
                    <input type="text" class="form-control" name="dep_dropoff_driver_name" />
                </div>
                <div class="form-group">
                    <label for="">Driver\'s Phone</label>
                    <input type="text" class="form-control" name="dep_dropoff_driver_phone" />
                </div>
            </div>';
        }
	}
	else
	{
        if( isset( $trip['pickup'] ) )
        {
            $airport = isset( $trip['pickup']['airport'] ) ? $trip['pickup']['airport'] : '';
            $taid    = isset( $trip['pickup']['area_id'] ) ? $trip['pickup']['area_id'] : '';
            $hid     = isset( $trip['pickup']['hotel_id'] ) ? $trip['pickup']['hotel_id'] : '';
            $ftime   = isset( $trip['pickup']['flight_time'] ) ? $trip['pickup']['flight_time'] : '';
            $trans   = isset( $trip['pickup']['transport_type'] ) ? $trip['pickup']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '
                <div class="pickup-detail-passenger">
                    <div class="title">Pickup</div>
                    <div class="text">Own Transport</div>
                    <div class="form-group">
                        <label for="">Driver\'s Name</label>
                        <input type="text" class="form-control" name="dep_pickup_driver_name" />
                    </div>
                    <div class="form-group">
                        <label for="">Driver\'s Phone</label>
                        <input type="text" class="form-control" name="dep_pickup_driver_phone" />
                    </div>
                </div>';
            }
            else
            {
                if( $trans != '' && $taid != '' )
                {
                    $s = 'SELECT a.taname FROM ticket_transport_area AS a WHERE a.taid = %d';
                    $q = $db->prepare_query( $s, $taid );
                    $r = $db->do_query( $q );
                    $d = $db->fetch_array( $r );

                    $type_trans = $trans == 0 ? 'Shared Transport to ' . $d['taname'] : 'Private Transport to ' . $d['taname'];
                }
                elseif( $trans != '' && $taid == '' )
                {
                    $type_trans = $trans == 0 ? 'Shared Transport' : 'Private Transport';
                }
                else
                {
                    $type_trans = '-';
                }

                if( in_array( $hid, array( 1, 2 ) ) )
                {
                    $transport .= '
                    <div class="pickup-detail-passenger">
                        <div class="title">Pickup</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">' . ( $hid == 1 ? 'Hotel to be advised' : 'Other Hotel' ) . '<br/>
                                Please note that some hotels may be outside the free pick up/drop-offs area and may be subject to additional fees.
                            </div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '" style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">Hotel/Villa Name</label>
                                    <input type="text" class="form-control" name="dep_pickup_hotel_name" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Address</label>
                                    <input type="text" class="form-control" name="dep_pickup_hotel_address" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Phone</label>
                                    <input type="text" class="form-control" name="dep_pickup_hotel_phone" value="" />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                elseif( empty( $hid ) )
                {
                    $transport .= '
                    <div class="pickup-detail-passenger">
                        <div class="title">Pickup</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">No Accommodation Booked</div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '" style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">Hotel/Villa Name</label>
                                    <input type="text" class="form-control" name="dep_pickup_hotel_name" value="" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Address</label>
                                    <input type="text" class="form-control" name="dep_pickup_hotel_address" value="" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Phone</label>
                                    <input type="text" class="form-control" name="dep_pickup_hotel_phone" value="" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                else
                {
                    $d = get_hotel( $hid );
                    $t = $d['taairport'] == '1' ? 'Airport' : 'Hotel/Villa';

                    $transport .= '
                    <div class="pickup-detail-passenger">
                        <div class="title">Pickup</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">' . $t . ' Name <span>*</span></label>
                                    <input type="text" class="form-control required" name="dep_pickup_hotel_name" value="' . $d['hname'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">' . $t . ' Address <span>*</span></label>
                                    <input type="text" class="form-control required" name="dep_pickup_hotel_address" value="' . $d['haddress'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">' . $t . ' Phone</label>
                                    <input type="text" class="form-control" name="dep_pickup_hotel_phone" value="' . $d['hphone'] . '" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
            }
        }

        if( isset( $trip['drop-off'] ) )
        {
            $airport = isset( $trip['drop-off']['airport'] ) ? $trip['drop-off']['airport'] : '';
            $taid    = isset( $trip['drop-off']['area_id'] ) ? $trip['drop-off']['area_id'] : '';
            $hid     = isset( $trip['drop-off']['hotel_id'] ) ? $trip['drop-off']['hotel_id'] : '';
            $ftime   = isset( $trip['drop-off']['flight_time'] ) ? $trip['drop-off']['flight_time'] : '';
            $trans   = isset( $trip['drop-off']['transport_type'] ) ? $trip['drop-off']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '
                <div class="dropoff-detail-passenger">
                    <div class="title">Drop-off</div>
                    <div class="text">Own Transport</div>
                    <div class="form-group">
                        <label for="">Driver\'s Name</label>
                        <input type="text" class="form-control" name="dep_dropoff_driver_name" />
                    </div>
                    <div class="form-group">
                        <label for="">Driver\'s Phone</label>
                        <input type="text" class="form-control" name="dep_dropoff_driver_phone" />
                    </div>
                </div>';
            }
            else
            {
                if( $trans != '' && $taid != '' )
                {
                    $s = 'SELECT a.taname FROM ticket_transport_area AS a WHERE a.taid = %d';
                    $q = $db->prepare_query( $s, $taid );
                    $r = $db->do_query( $q );
                    $d = $db->fetch_array( $r );

                    $type_trans = $trans == 0 ? 'Shared Transport to ' . $d['taname'] : 'Private Transport to ' . $d['taname'];
                }
                elseif( $trans !='' && $taid == '' )
                {
                    $type_trans = $trans == 0 ? 'Shared Transport' : 'Private Transport';
                }
                else
                {
                    $type_trans = '-';
                }

                if( in_array( $hid, array( 1, 2 ) ) )
                {
                    $transport .= '
                    <div class="dropoff-detail-passenger">
                        <div class="title">Drop-off</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">' . ( $hid == 1 ? 'Hotel to be advised' : 'Other Hotel' ) . '<br/>
                                Please note that some hotels may be outside the free pick up/drop-offs area and may be subject to additional fees.
                            </div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '" style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">Hotel/Villa Name</label>
                                    <input type="text" class="form-control" name="dep_dropoff_hotel_name" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Address</label>
                                    <input type="text" class="form-control" name="dep_dropoff_hotel_address" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Phone</label>
                                    <input type="text" class="form-control" name="dep_dropoff_hotel_phone" value="" />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                elseif( empty( $hid ) )
                {
                    $transport .= '
                    <div class="dropoff-detail-passenger">
                        <div class="title">Drop-off</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">No Accommodation Booked</div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '" style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">Hotel/Villa Name</label>
                                    <input type="text" class="form-control" name="dep_dropoff_hotel_name" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Address</label>
                                    <input type="text" class="form-control" name="dep_dropoff_hotel_address" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Phone</label>
                                    <input type="text" class="form-control" name="dep_dropoff_hotel_phone" value="" />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                else
                {
                    $d = get_hotel( $hid );
                    $t = $d['taairport'] == '1' ? 'Airport' : 'Hotel/Villa';

                    $transport .= '
                    <div class="dropoff-detail-passenger">
                        <div class="title">Drop-off</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">' . $t . ' Name <span>*</span></label>
                                    <input type="text" class="form-control required" name="dep_dropoff_hotel_name" value="' . $d['hname'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">' . $t . ' Address <span>*</span></label>
                                    <input type="text" class="form-control required" name="dep_dropoff_hotel_address" value="' . $d['haddress'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">' . $t . ' Phone</label>
                                    <input type="text" class="form-control" name="dep_dropoff_hotel_phone" value="' . $d['hphone'] . '" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
            }
        }
	}

    return $transport;
}

function get_front_return_transport_detail( $trip, $trans_type )
{
    global $db;

    $transport = '';

    if( empty( $trans_type ) )
    {
        if( isset( $trip['pickup'] ) )
        {
            $transport .= '
            <div class="pickup-detail-passenger">
                <div class="title">Pickup</div>
                <div class="text">Own Transport</div>
                <div class="form-group">
                    <label for="">Driver\'s Name</label>
                    <input type="text" class="form-control" name="rtn_pickup_driver_name" />
                </div>
                <div class="form-group">
                    <label for="">Driver\'s Phone</label>
                    <input type="text" class="form-control" name="rtn_pickup_driver_phone" />
                </div>
            </div>';
        }

        if( isset( $trip['drop-off'] ) )
        {
            $transport .= '
            <div class="dropoff-detail-passenger">
                <div class="title">Drop-off</div>
                <div class="text">Own Transport</div>
                <div class="form-group">
                    <label for="">Driver\'s Name</label>
                    <input type="text" class="form-control" name="rtn_dropoff_driver_name" />
                </div>
                <div class="form-group">
                    <label for="">Driver\'s Phone</label>
                    <input type="text" class="form-control" name="rtn_dropoff_driver_phone" />
                </div>
            </div>';
        }
    }
    else
    {
        if( isset( $trip['pickup'] ) )
        {
            $airport = isset( $trip['pickup']['airport'] ) ? $trip['pickup']['airport'] : '';
            $taid    = isset( $trip['pickup']['area_id'] ) ? $trip['pickup']['area_id'] : '';
            $hid     = isset( $trip['pickup']['hotel_id'] ) ? $trip['pickup']['hotel_id'] : '';
            $ftime   = isset( $trip['pickup']['flight_time'] ) ? $trip['pickup']['flight_time'] : '';
            $trans   = isset( $trip['pickup']['transport_type'] ) ? $trip['pickup']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '
                <div class="pickup-detail-passenger">
                    <div class="title">Pickup</div>
                    <div class="text">Own Transport</div>
                    <div class="form-group">
                        <label for="">Driver\'s Name</label>
                        <input type="text" class="form-control" name="rtn_pickup_driver_name" />
                    </div>
                    <div class="form-group">
                        <label for="">Driver\'s Phone</label>
                        <input type="text" class="form-control" name="rtn_pickup_driver_phone" />
                    </div>
                </div>';
            }
            else
            {
                if( $trans != '' && $taid != '' )
                {
                    $s = 'SELECT a.taname FROM ticket_transport_area AS a WHERE a.taid = %d';
                    $q = $db->prepare_query( $s, $taid );
                    $r = $db->do_query( $q );
                    $d = $db->fetch_array( $r );

                    $type_trans = $trans == 0 ? 'Shared Transport to ' . $d['taname'] : 'Private Transport to ' . $d['taname'];
                }
                elseif( $trans != '' && $taid == '' )
                {
                    $type_trans = $trans == 0 ? 'Shared Transport' : 'Private Transport';
                }
                else
                {
                    $type_trans = '-';
                }

                if( in_array( $hid, array( 1, 2 ) ) )
                {
                    $transport .= '
                    <div class="pickup-detail-passenger">
                        <div class="title">Pickup</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">' . ( $hid == 1 ? 'Hotel to be advised' : 'Other Hotel' ) . '<br/>
                                Please note that some hotels may be outside the free pick up/drop-offs area and may be subject to additional fees.
                            </div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '" style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">Hotel/Villa Name</label>
                                    <input type="text" class="form-control" name="rtn_pickup_hotel_name" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Address</label>
                                    <input type="text" class="form-control" name="rtn_pickup_hotel_address" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Phone</label>
                                    <input type="text" class="form-control" name="rtn_pickup_hotel_phone" value="" />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                elseif( empty( $hid ) )
                {
                    $transport .= '
                    <div class="pickup-detail-passenger">
                        <div class="title">Pickup</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">No Accommodation Booked</div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '" style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">Hotel/Villa Name</label>
                                    <input type="text" class="form-control" name="rtn_pickup_hotel_name" value="" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Address</label>
                                    <input type="text" class="form-control" name="rtn_pickup_hotel_address" value="" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Phone</label>
                                    <input type="text" class="form-control" name="rtn_pickup_hotel_phone" value="" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                else
                {
                    $d = get_hotel( $hid );
                    $t = $d['taairport'] == '1' ? 'Airport' : 'Hotel/Villa';

                    $transport .= '
                    <div class="pickup-detail-passenger">
                        <div class="title">Pickup</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">' . $t . ' Name <span>*</span></label>
                                    <input type="text" class="form-control required" name="rtn_pickup_hotel_name" value="' . $d['hname'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">' . $t . ' Address <span>*</span></label>
                                    <input type="text" class="form-control required" name="rtn_pickup_hotel_address" value="' . $d['haddress'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">' . $t . ' Phone  <span>*</span></label>
                                    <input type="text" class="form-control required" name="rtn_pickup_hotel_phone" value="' . $d['hphone'] . '" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
            }
        }

        if( isset( $trip['drop-off'] ) )
        {
            $airport = isset( $trip['drop-off']['airport'] ) ? $trip['drop-off']['airport'] : '';
            $taid    = isset( $trip['drop-off']['area_id'] ) ? $trip['drop-off']['area_id'] : '';
            $hid     = isset( $trip['drop-off']['hotel_id'] ) ? $trip['drop-off']['hotel_id'] : '';
            $ftime   = isset( $trip['drop-off']['flight_time'] ) ? $trip['drop-off']['flight_time'] : '';
            $trans   = isset( $trip['drop-off']['transport_type'] ) ? $trip['drop-off']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '
                <div class="dropoff-detail-passenger">
                    <div class="title">Drop-off</div>
                    <div class="text">Own Transport</div>
                    <div class="form-group">
                        <label for="">Driver\'s Name</label>
                        <input type="text" class="form-control" name="rtn_dropoff_driver_name" />
                    </div>
                    <div class="form-group">
                        <label for="">Driver\'s Phone</label>
                        <input type="text" class="form-control" name="rtn_dropoff_driver_phone" />
                    </div>
                </div>';
            }
            else
            {
                if( $trans != '' && $taid != '' )
                {
                    $s = 'SELECT a.taname FROM ticket_transport_area AS a WHERE a.taid = %d';
                    $q = $db->prepare_query( $s, $taid );
                    $r = $db->do_query( $q );
                    $d = $db->fetch_array( $r );

                    $type_trans = $trans == 0 ? 'Shared Transport to ' . $d['taname'] : 'Private Transport to ' . $d['taname'];
                }
                elseif( $trans != '' && $taid == '' )
                {
                    $type_trans = $trans == 0 ? 'Shared Transport' : 'Private Transport';
                }
                else
                {
                    $type_trans = '-';
                }

                if( in_array( $hid, array( 1, 2 ) ) )
                {
                    $transport .= '
                    <div class="dropoff-detail-passenger">
                        <div class="title">Drop-off</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">' . ( $hid == 1 ? 'Hotel to be advised' : 'Other Hotel' ) . '<br/>
                                Please note that some hotels may be outside the free pick up/drop-offs area and may be subject to additional fees.
                            </div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '" style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">Hotel/Villa Name</label>
                                    <input type="text" class="form-control" name="rtn_dropoff_hotel_name" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Address</label>
                                    <input type="text" class="form-control" name="rtn_dropoff_hotel_address" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Phone</label>
                                    <input type="text" class="form-control" name="rtn_dropoff_hotel_phone" value="" />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                elseif( empty( $hid ) )
                {
                    $transport .= '
                    <div class="dropoff-detail-passenger">
                        <div class="title">Drop-off</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">No Accommodation Booked</div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '" style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">Hotel/Villa Name</label>
                                    <input type="text" class="form-control" name="rtn_dropoff_hotel_name" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Address</label>
                                    <input type="text" class="form-control" name="rtn_dropoff_hotel_address" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Phone</label>
                                    <input type="text" class="form-control" name="rtn_dropoff_hotel_phone" value="" />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                else
                {
                    $d = get_hotel( $hid );
                    $t = $d['taairport'] == '1' ? 'Airport' : 'Hotel/Villa';

                    $transport .= '
                    <div class="dropoff-detail-passenger">
                        <div class="title">Drop-off</div>
                        <div class="text">' . $type_trans . '</div>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div style="margin-top:10px;">
                                <div class="form-group">
                                    <label for="">' . $t . ' Name <span>*</span></label>
                                    <input type="text" class="form-control required" name="rtn_dropoff_hotel_name" value="' . $d['hname'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">' . $t . ' Address <span>*</span></label>
                                    <input type="text" class="form-control required" name="rtn_dropoff_hotel_address" value="' . $d['haddress'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label for="">' . $t . ' Phone  <span>*</span></label>
                                    <input type="text" class="form-control required" name="rtn_dropoff_hotel_phone" value="' . $d['hphone'] . '" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
            }
        }
    }

    return $transport;
}

function get_front_suggestion_accommodation( $arrive, $trip = array() )
{
    global $db;

    $s = 'SELECT * FROM ticket_hotel AS a
          LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
          LEFT JOIN ticket_transport_area_fee AS c ON c.taid = b.taid
          WHERE a.hstatus = %d AND c.lcid = %d AND b.taairport <> %s ORDER BY RAND() LIMIT 1';
    $q = $db->prepare_query( $s, 1, $arrive, '1' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $content = '
        <div class="form-group">
            <label for="">Hotel/Villa Name</label>
            <input type="text" class="form-control" name="hotel_name" value="' . $d['hname'] . '" />
        </div>
        <div class="form-group">
            <label for="">Hotel/Villa Address</label>
            <input type="text" class="form-control" name="hotel_address" value="' . $d['haddress'] . '" />
        </div>
        <div class="form-group">
            <label for="">Hotel/Villa Phone</label>
            <input type="text" class="form-control" name="hotel_phone" value="' . $d['hphone'] . '" />
        </div>
        <div class="form-group">
            <label for="">Hotel/Villa Email</label>
            <input type="text" class="form-control" name="hotel_email" value="' . $d['hemail'] . '" />
        </div>';
    }
    else
    {
        $content = '
        <div class="form-group">
            <label for="">Hotel/Villa Name</label>
            <input type="text" class="form-control" name="hotel_name" />
        </div>
        <div class="form-group">
            <label for="">Hotel/Villa Address</label>
            <input type="text" class="form-control" name="hotel_address" />
        </div>
        <div class="form-group">
            <label for="">Hotel/Villa Phone</label>
            <input type="text" class="form-control" name="hotel_phone" />
        </div>
        <div class="form-group">
            <label for="">Hotel/Villa Email</label>
            <input type="text" class="form-control" name="hotel_email" />
        </div>';
    }

    return $content;
}

function get_pickup_dropoff_transport_edit_field( $trip = array() )
{
    extract( $trip );

    $dtrans = get_pickup_drop_list_data( $rid, $bdfrom_id );
    $atrans = get_pickup_drop_list_data( $rid, $bdto_id );
    $count  = ceil( ( $num_adult + $num_child + $num_infant ) / 4 );

    $content = '
    <div class="transport-pickup-dropoff">';

        foreach( $transport as $type => $obj )
        {
            foreach( $obj as $d )
            {
                $trans_type  = $d['bttrans_type'];
                $trans_loc   = $type == 'pickup' ? $bdfrom : $bdto;
                $trans_css   = $d['bttrans_type'] == '2' ? 'sr-only' : '';
                $trans_fee   = $d['bttrans_fee'] == 0 ? 'Free' : number_format( $d['bttrans_fee'], 0, ',', '.' );

                $content .= '
                <div class="pickup-transport-wrapp">
                    <div class="pickup-transport">
                        <div class="col-1">
                            <p class="title">' . ucfirst( $type ) . ' - ' . $trans_loc . '</p>
                            <select class="select-modal-edit form-control transport-type" name="transport[' . $d['btid'] . '][bttrans_type]">
                                ' . get_front_transport_type_option( $trans_type ) . '
                            </select>
                            <div class="hidden-block ' . $trans_css . '">
                                <p class="title-second">Area</p>';

                                if( $d['bttrans_type'] == '2' )
                                {
                                    $content .= '
                                    <select class="select-modal-edit form-control hotels-area" name="transport[' . $d['btid'] . '][taid]" autocomplete="off">
                                        <option value="" data-rpfrom="" data-rpto="" data-area="" data-airport="0">Select Area</option>';

                                        if( $type == 'pickup' && isset( $dtrans['pickup'] ) )
                                        {
                                            $content .= get_front_area_option_by_trip( $d['taid'], $dtrans['pickup'], $count, $trans_type  );
                                        }
                                        elseif( $type == 'drop-off' && isset( $atrans['drop-off'] ) )
                                        {
                                            $content .= get_front_area_option_by_trip( $d['taid'], $atrans['drop-off'], $count, $trans_type  );
                                        }

                                        $content .= '
                                    </select>';
                                }
                                else
                                {
                                    $content .= '
                                    <input class="form-control" type="text" value="' . $d['taname'] . '" readonly="" >
                                    <input class="sr-only" type="text" name="transport[' . $d['btid'] . '][taid]" value="' . $d['taid'] . '">';
                                }

                                $content .= '
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="hidden-block ' . $trans_css . '">
                                <div class="form-group">
                                    <label for="" class="first">Hotel/Villa Name</label>
                                    <select class="select-modal-edit form-control hotels" name="transport[' . $d['btid'] . '][hid]">';

                                        if( $type == 'pickup' && isset( $dtrans['pickup'] ) )
                                        {
                                            $content .= get_front_hotel_option_by_trip( $d['hid'], $dtrans['pickup'], $count, $trans_type  );
                                        }
                                        elseif( $type == 'drop-off' && isset( $atrans['drop-off'] ) )
                                        {
                                            $content .= get_front_hotel_option_by_trip( $d['taid'], $atrans['drop-off'], $count, $trans_type  );
                                        }

                                        $content .= '
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Address</label>
                                    <input type="text" class="form-control h-address" name="transport[' . $d['btid'] . '][bthoteladdress]" value="' . $d['bthoteladdress'] . '" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Phone</label>
                                    <input type="text" class="form-control h-phone" name="transport[' . $d['btid'] . '][bthotelphone]" value="' . $d['bthotelphone']. '" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="">Hotel/Villa Email</label>
                                    <input type="text" class="form-control h-email" name="transport[' . $d['btid'] . '][bthotelemail]" value="' . $d['bthotelemail'] . '" readonly>
                                </div>
                                <div class="form-group sr-only">
                                    <input type="text" class="form-control h-transfee" name="transport[' . $d['btid'] . '][bttrans_fee]" value="' . $d['bttrans_fee']  . '">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
            }
        }

        $content .= '
    </div>';

    return $content;
}

function get_detail_add_ons_content( $trip )
{
    if( empty( $trip['add-ons'] ) === false )
    {
        $content = '
        <div class="passenger-details add-ons-detail">
            <div class="p-detail-block">
                <ul class="row row-head">
                    <li class="col-xs-3 col-sm-4 col-md-4"><span>Add-ons</span></li>
                    <li class="col-xs-3 col-sm-4 col-md-4"><span>Description</span></li>
                    <li class="col-xs-3 col-sm-2 col-md-2 text-right"><span>Price (IDR)</span></li>
                    <li class="col-xs-3 col-sm-2 col-md-2 text-right"><span>Total Price (IDR)</span></li>
                </ul>';

                foreach( $trip['add-ons'] as $d )
                {
                    $content .= '
                    <ul class="row">
                        <li class="col-xs-3 col-sm-4 col-md-4"><span>' . $d['aoname'] . '</span></li>
                        <li class="col-xs-3 col-sm-4 col-md-4"><span>' . nl2br( $d['aobrief'] ) . '</span></li>
                        <li class="col-xs-3 col-sm-2 col-md-2 text-right"><span>' . $d['baopax'] . ' x ' . number_format( $d['baoprice'], 0, ',', '.' ) . '</span></li>
                        <li class="col-xs-3 col-sm-2 col-md-2 text-right"><span>' . number_format( ( $d['baoprice'] * $d['baopax'] ), 0, ',', '.' ) . '</span></li>
                    </ul>';
                }

                $content .= '
            </div>
        </div>';

        return $content;
    }
}

function get_detail_transport_content( $transport = array(), $from, $to )
{
    $content = '';

    foreach( $transport as $type => $obj )
    {
        foreach( $obj as $d )
        {
            $trans_loc  = $type == 'pickup' ? $from : $to;
            $trans_area = get_transport_area( $d['taid'], 'taname' );
            $trans_type = $d['bttrans_type'] == '0' ? 'Shared Transport' : ( $d['bttrans_type'] == '1' ? 'Privated Transport' : 'Own Transport' );
            $trans_css  = $d['bttrans_type'] == '2' ? 'sr-only' : '';
            $trans_fee  = $d['bttrans_fee'] == 0 ? 'Free' : number_format( $d['bttrans_fee'], 0, ',', '.' );

            $hotel_addr  = empty( $d['bthoteladdress'] ) ? '' : '<br>' . $d['bthoteladdress'];
            $hotel_phone = empty( $d['bthotelphone'] ) ? '' : '<br>P. ' . $d['bthotelphone'];
            $hotel_email = empty( $d['bthotelemail'] ) ? '' : '<br>E. ' . $d['bthotelemail'];

            $content .= '
            <div class="transport-pickup-dropoff">
                <div class="pickup-transport">
                    <p class="title">' . ucfirst( $type ) . ' - ' . $trans_loc . '</p>
                    <p class="body"><strong>' . $trans_type . '</strong><br>' . $trans_area . '</p>
                    <p class="desc ' . $trans_css . '">' . $d['bthotelname'] . $hotel_addr . $hotel_phone . $hotel_email . '</p>
                </div>
                <div class="transport-fee">
                    <p class="title">Transport Fee</p>
                    <p>' . $trans_fee . '</p>
                </div>
            </div>';
        }
    }

    return $content;
}

function get_random_word( $len = 10 )
{
    $word = array_merge( range( 'a', 'z' ), range( 'A', 'Z' ) );
    shuffle( $word );
    return substr( implode( $word ), 0, $len );
}

function get_front_transport_type_option( $value )
{
	return '
    <option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>Shared Transport</option>
    <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Private Transport</option>
    <option value="2" ' . ( $value == '2' ? 'selected' : '' ) . '>Own Transport</option>';
}

function get_feedback_link( $bid )
{
    $feedback_link_opt = get_meta_data( 'feedback_link_opt' );

    if( $feedback_link_opt == 1 )
    {
        $feedback_link = get_meta_data( 'feedback_link' );

        if( empty( $feedback_link ) )
        {
            $feedback_link = HTSERVER . site_url() . '/detail-reservation/?id=' . $bid . '&type=feedback';
        }
    }
    else
    {
        $feedback_link = HTSERVER . site_url() . '/detail-reservation/?id=' . $bid . '&type=feedback';
    }

    return $feedback_link;
}

function get_feedback_cron_data()
{
    global $db;

    $s = 'SELECT
            a.*,
            b.agemail,
            b.agname,
            c.chname
          FROM ticket_booking AS a
          LEFT JOIN ticket_agent AS b ON a.agid = b.agid
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          WHERE a.bstatus = %s AND a.bstt <> %s';
    $q = $db->prepare_query( $s, 'pa', 'ar' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $detail_booking = ticket_booking_detail( $d['bid'], ( $d['btype'] == '1' ? 'return' : 'departure' ) );
            $finish_date    = strtotime( $detail_booking['bddate'] );
            $current_date   = strtotime( date( 'Y-m-d' ) );

            if( $current_date > $finish_date && $d['bfeedback'] == 0 )
            {
                $data[ $d['bid'] ] = array(
                    'ticket' => $d['bticket'],
                    'name' => $d['bbname'],
                    'email' => $d['bbemail'],
                    'date' => $detail_booking['bddate']
                );
            }
        }

        return $data;
    }
}

function get_feedback_link_to_client( $data, $bid )
{
    return '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Feedback Link</title>
            <style></style>
        </head>
        <body style="margin:0; padding:0; font-family:Arial; font-size:14px; color:rgb(73, 73, 85);">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:rgb(230,238,239);">
                <tr>
                    <td align="center" valign="top" style="padding:20px;">
                        <table border="0" cellpadding="15" cellspacing="0" width="600" id="emailContainer" style="background-color:rgb(255,255,255);">
                            <tr>
                                <td align="left" valign="middle" style="border-bottom:1px solid rgb(198, 216, 220);" colspan="3">
                                    <img src="' . get_theme_img() . '/logo.png" alt="Blue Water Express" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" colspan="3">
                                    <h2 style="color:rgb(62, 117, 128); font-size:18px; margin:0;">Notification</h2>
                                    ' . return_email_setting( 'feedback_email_content', array( 'customer_name' => $data['name'], 'feedback_link' => get_feedback_link( $bid ) ) ) . '
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
    </html>';
}

function set_message_page_content( $page_title, $title, $content )
{
    set_template( TEMPLATE_PATH . '/message.html' );

    add_variable( 'title', $title );
    add_variable( 'content', $content );
    add_variable( 'page_title', $page_title );
}

function get_booking_conversion()
{
    extract( $_GET );

    $data  = isset( $id ) && !empty( $id ) ? ticket_booking_all_data( $id ) : array();
    $pdata = isset( $data['payment'] ) && !empty( $data['payment'] ) ? end( $data['payment'] ) : '';
    $pidr  = isset( $data['payment'] ) && !empty( $data['payment'] ) ? array_sum( array_column( $data['payment'], 'ptotal' ) ) : 0;
    $pusd  = isset( $data['payment'] ) && !empty( $data['payment'] ) ? array_sum( array_column( $data['payment'], 'ptotalusd' ) ) : 0;

    if( isset( $pdata['pstatus'] ) && $pdata['pstatus'] == '1' )
    {
        $currency   = $data['bpaymethod'] == 3 ? 'USD' : 'IDR';
        $ptotal     = $data['bpaymethod'] == 3 ? $pusd : $pidr;

        if( $ptotal != 0 )
        {
            $trip = array();
            $fee  = 0;

            foreach( $data['detail'] as $type => $obj )
            {
                foreach( $obj as $bdid => $d )
                {
                    $to   = get_location( $d['bdto_id'] );
                    $from = get_location( $d['bdfrom_id'] );
                    $cat  = $to['lctype'] == '1' ? 'Out' : 'Return';
                    $paxs = $d['num_adult'] + $d['num_child'] + $d['num_infant'];

                    $trip[] = '{
                        "name": "' . $d['rname'] . '",
                        "id": "' . $d['rid'] . '",
                        "price": "' . $d['total'] . '",
                        "category": "' . $cat . '",
                        "variant": "' . $from['lcalias'] . '-' . $to['lcalias'] .'",
                        "quantity": ' . $paxs . ',
                        "coupon": ""
                    }';

                    if( isset( $d['transport'] ) )
                    {
                        foreach( $d['transport'] as $ttype => $tobj )
                        {
                            foreach( $tobj as $dt )
                            {
                                $fee += $dt['bttrans_fee'];
                            }
                        }
                    }
                }
            }

            $conversion = '
            <!-- Google Code for Thank you payment Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 690647859;
                var google_conversion_label = "29sxCOmqk7kBELPmqckC";
                var google_conversion_value = ' . $ptotal . ';
                var google_conversion_orderid = "' . $data['bticket'] . '";
                var google_conversion_currency = "' . $currency . '";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
            <script type="text/javascript">
                jQuery(document).ready(function(){
                    show_popup("Notification", "Please also check your booking confirmation in junk mail!");
                });
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/690647859/?value=' . $ptotal . '&amp;currency_code=' . $currency . '&amp;label=29sxCOmqk7kBELPmqckC&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>';

            $conversion .= '
            <script type="text/javascript">
                // Send transaction data with a pageview if available
                // when the page loads. Otherwise, use an event when the transaction
                // data becomes available.
                var dataLayer = window.dataLayer || [];

                dataLayer.push({
                    "event": "transaction",
                    "ecommerce": {
                        "purchase": {
                            "actionField": {
                                "id": "' . $data['bticket'] . '",
                                "affiliation": "' . ( empty( $data['agid'] ) ? $data['chname'] : get_agent( $data['agid'], 'agname' ) ) . '",
                                "revenue": "' . $data['btotal'] . '",
                                "tax":"0",
                                "shipping": "' . $fee . '",
                                "coupon": "' . ( empty( $data['pmcode'] ) ? 'None' : $data['pmcode'] ) . '"
                            },
                            "products": [' . implode( ',', $trip ) . ']
                        }
                    }
                });
            </script>';
        }
        else
        {
            $conversion = '';
        }
    }
    else
    {
        $conversion = '';
    }

    return $conversion;
}

function send_feedback_link_to_client( $data, $bid )
{
    try
    {
        $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

        $mail = new Mandrill( $keys );

        $email_to   = $data['email'];
        $email_cc   = get_meta_data( 'email_reservation' );
        $email_from = get_meta_data( 'smtp_email_address' );
        $email_data = get_feedback_link_to_client( $data, $bid );

        $message  = array(
            'subject'    => 'Feedback #' . $data['ticket'],
            'from_name'  => get_meta_data('web_title'),
            'from_email' => $email_from,
            'html'       => $email_data,
            'to' => array(
                array(
                    'email' => $email_to,
                    'name'  => '',
                    'type'  => 'to'
                ),
                array(
                    'email' => $email_cc,
                    'name'  => '',
                    'type'  => 'cc'
                )
            ),
            'preserve_recipients' => true,
            'headers' => array( 'Reply-To' => $email_from )
        );

        $async   = false;
        $result  = $mail->messages->send( $message, $async );

        if( isset( $result[0]['status'] ) )
        {
            if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    catch( Mandrill_Error $e )
    {
        return false;
    }
}

function is_front_availability_result()
{
	$cek_url = cek_url();

    if( $cek_url[0] == 'availability-result' )
    {
    	return true;
    }
}

function is_front_review_booking()
{
	$cek_url = cek_url();

    if( $cek_url[0] == 'review-booking' )
    {
    	return true;
    }
}

function is_front_detail_booking()
{
	$cek_url = cek_url();

    if( $cek_url[0] == 'detail-booking' )
    {
    	return true;
    }
}

function is_front_detail_reservation()
{
	$cek_url = cek_url();

    if( $cek_url[0] == 'detail-reservation' )
    {
    	return true;
    }
}

function is_front_feedback_reservation()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'feedback-reservation' )
    {
        return true;
    }
}

function is_front_complete_booking()
{
	$cek_url = cek_url();

    if( count( $cek_url ) == 2 && $cek_url[0] == 'booking' && $cek_url[1] == 'complete' )
    {
    	return true;
    }
}

function is_front_subscribing()
{
	$cek_url = cek_url();

    if( $cek_url[0] == 'subscribing')
    {
    	return true;
    }
}

function is_front_canceled_booking()
{
	$cek_url = cek_url();

    if( count( $cek_url ) == 2 && $cek_url[0] == 'booking' && $cek_url[1] == 'canceled' && !empty( $_GET ) )
    {
    	return true;
    }
}

function is_front_invalid_booking()
{
    $cek_url = cek_url();

    if( count( $cek_url ) == 2 && $cek_url[0] == 'booking' && $cek_url[1] == 'invalid' && !empty( $_GET ) )
    {
        return true;
    }
}

function is_front_failed_payment()
{
    $cek_url = cek_url();

    if( count( $cek_url ) == 2 && $cek_url[0] == 'booking' && $cek_url[1] == 'failed-payment' && !empty( $_GET ) )
    {
        return true;
    }
}

function is_front_done_payment()
{
    $cek_url = cek_url();

    if( count( $cek_url ) == 2 && $cek_url[0] == 'booking' && $cek_url[1] == 'done-payment' && !empty( $_GET ) )
    {
        return true;
    }
}

function is_front_pay_booking()
{
	$cek_url = cek_url();

    if( count( $cek_url ) == 2 && $cek_url[0] == 'booking' && $cek_url[1] == 'payment' && !empty( $_GET ) )
    {
    	return true;
    }
}

function is_front_paypal_trouble()
{
    $cek_url = cek_url();

    if( count( $cek_url ) == 2 && $cek_url[0] == 'booking' && $cek_url[1] == 'paypal-trouble' && !empty( $_GET ) )
    {
        return true;
    }
}

function is_front_redirect_second_payment()
{
    $cek_url = cek_url();

    if( count( $cek_url ) == 2 && $cek_url[0] == 'booking' && $cek_url[1] == 'redirect-secondary-payment' && !empty( $_GET ) )
    {
        return true;
    }
}

function is_front_recheck_reservation()
{
    $cek_url = cek_url();

    if( count( $cek_url ) == 1 && $cek_url[0] == 'recheck-reservation' )
    {
        $web_title = web_name();

        add_actions( 'meta_title', get_meta_title( 'Recheck Reservation - ' . $web_title ) );
        add_actions( 'meta_keywords', get_meta_keywords( 'Recheck Reservation - ' . $web_title ) );
        add_actions( 'meta_description', get_meta_description( 'Recheck Reservation - ' . $web_title ) );

        return true;
    }
    else
    {
        return false;
    }
}

function is_testing_script()
{
    $cek_url = cek_url();

    if( count( $cek_url ) == 1 && $cek_url[0] == 'testing-script' )
    {
        return true;
    }
}

?>
