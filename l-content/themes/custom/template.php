<?php

/*
	Title: Bluewater Express
	Preview: template.png
	Author: Ngurah
	Author Url: -
	Description: a template for Bluewater Express website
*/

require_once 'include/checkin.php';
require_once 'include/agent.php';
require_once 'include/api.php';
require_once 'functions.php';

//-- Set General Actions
$version = '1.1.7';

add_actions( 'include_css', 'get_css_inc', 'bootstrap-3.3.7/css/bootstrap-theme.min.css' );
add_actions( 'include_css', 'get_css_inc', 'bootstrap-3.3.7/css/bootstrap.min.css' );
add_actions( 'include_css', 'get_css_inc', 'font-awesome/css/font-awesome.min.css' );
add_actions( 'include_css', 'get_css_inc', 'jquery-ui-1.12.1/jquery-ui.min.css' );

add_actions( 'include_css', 'get_custom_css', 'https://fonts.googleapis.com/css?family=Open+Sans:600,400,400' );
add_actions( 'include_css', 'get_custom_css', HTSERVER . TEMPLATE_URL . '/css/select2.min.css' );
add_actions( 'include_css', 'get_custom_css', HTSERVER . TEMPLATE_URL . '/css/custom.css?v=' . $version );
add_actions( 'include_css', 'get_custom_css', HTSERVER . TEMPLATE_URL . '/css/mobile.css?v=' . $version );
add_actions( 'include_css', 'get_custom_css', HTSERVER . TEMPLATE_URL . '/css/dropzone.css?v=' . $version );

add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
add_actions( 'include_js', 'get_javascript_inc', 'bootstrap-3.3.7/js/bootstrap.min.js' );
add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
add_actions( 'include_js', 'get_custom_javascript', HTSERVER . TEMPLATE_URL . '/js/select2.min.js' );
add_actions( 'include_js', 'get_custom_javascript', HTSERVER . TEMPLATE_URL . '/js/dropzone.js' );

add_actions( 'header_elements', 'get_css', 'dropzone.css' );

if( isset( $actions->action['is_use_ajax'] ) && $actions->action['is_use_ajax']['func_name'][0] )
{
	set_template( TEMPLATE_PATH . '/ajax.html' );
}
elseif( is_api() )
{
    if( is_v1() )
    {
        set_api_request();
    }
    elseif( is_v2() )
    {
        set_api_v2_request();
    }
}
elseif( is_agent_login() )
{
	set_template( TEMPLATE_PATH . '/login.html' );
        
    add_variable( 'usertype', 'agent' );
    add_variable( 'title', 'Agent Login' );
    add_variable( 'alert', post_agent_login() );
    add_variable( 'forgot_password_link', get_agent_url() . '/forget-password' );
}
elseif( is_logout() )
{
    do_agent_logout();
}
elseif( is_agent_forget_password() )
{
	set_template( TEMPLATE_PATH . '/forget-password.html' );
    
    add_variable( 'usertype', 'agent' );
    add_variable( 'login_link',  get_agent_url() . '/login' );
    add_variable( 'forget_alert', post_agent_forget_password() );
}
elseif( is_agent_admin() )
{
    if( is_agent_popup_detail_booking() || is_agent_popup_edit_booking() )
    {
        set_template( TEMPLATE_PATH . '/admin-ajax.html' );
    }
	else
    {
        set_template( TEMPLATE_PATH . '/admin.html' );
    }

	$thecontent = get_admin_contents();
}
elseif( is_checker_login() )
{
    if( is_checker_logged() )
    {
        header( 'location:' . HTSERVER . site_url() . '/checkin' );
    }
    else
    {
        set_template( TEMPLATE_PATH . '/login.html' );
        
        add_variable( 'usertype', 'checker' );
        add_variable( 'title', 'Checker Login' );
        add_variable( 'alert', post_checker_login() );
        add_variable( 'forgot_password_link', HTSERVER . site_url() . '/checker/forget-password' );
    }
}
elseif( is_checker_forget_password() )
{
    set_template( TEMPLATE_PATH . '/forget-password.html' );
    
    add_variable( 'usertype', 'checker' );
    add_variable( 'login_link', HTSERVER . site_url() . '/checker/login' );
    add_variable( 'forget_alert', post_checker_forget_password() );
}
elseif( is_checkin() )
{
    if( is_checker_logged() )
    {
        proceed_checkin();

        set_template( TEMPLATE_PATH . '/checkin.html' );

        add_variable( 'flash_message', generate_message_block() );
        add_variable( 'trip_and_passenger', get_trip_and_passenger() );
        add_variable( 'background_image', get_background_image_url() );
        add_variable( 'ajax_url', HTSERVER . site_url() . '/checkin-ajax' );
        add_variable( 'barcodeval', isset( $_POST['barcodeval'] ) ? $_POST['barcodeval'] : '' );

        add_actions( 'include_css', 'get_custom_css', 'https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css' );
        add_actions( 'include_js', 'get_custom_javascript', 'https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js' );
    }
    else
    {
        header( 'location:' . HTSERVER . site_url() . '/checker/login' );
    }
}
elseif( is_home() )
{
    $site_url = site_url();

	set_template( TEMPLATE_PATH . '/index.html' );

    add_variable( 'background_image', get_background_image_url() );
    add_variable( 'chid', get_meta_data( 'channel_id', 'ticket_setting' ) );
    add_variable( 'depart_port_list', get_availibility_loc_option( 'From ',null,'from') );
    add_variable( 'destination_port_list', get_availibility_loc_option( 'To ',null,'to') );
    add_variable( 'return_port_list', get_availibility_loc_option( 'Return From',null,'to') );
    add_variable( 'destination_to_port_list', get_availibility_loc_option( 'Return To ',null,'from') );

    add_variable( 'maction', HTSERVER . $site_url . '/detail-reservation' );
	add_variable( 'subscribeaction', HTSERVER . site_url() . '/ticket-suscribe-mailchimp' );
	add_variable( 'htsserver', HTSERVER );
    add_variable( 'get_pmcode', isset( $_GET['pmcode'] ) ? $_GET['pmcode'] : '' );

    add_variable( 'action', HTSERVER . $site_url . '/ticket-front-booking-process' );
}
elseif( is_front_availability_result() )
{
    $site_url = site_url();
    $booking  = booking_item();

    //-- Redirect If empty booking
    if( isset( $booking['sess_data'] ) && !empty( $booking['sess_data'] ) )
    {
        extract( $booking['sess_data'] );

        //-- Redirect If date of depart greater than current date
        if( strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
        {
            header( 'location:' . HTSERVER . $site_url );
        }

        set_template( TEMPLATE_PATH . '/results.html' );

        add_variable( 'sess_id', $booking['sess_id'] );

        if( empty( $date_of_return ) )
        {
            add_variable( 'return_css', 'sr-only' );
        }
        else
        {
            add_variable( 'return_css', '' );
            add_variable( 'return_port', $return_port );
            add_variable( 'return_date', $date_of_return );
            add_variable( 'destination_port_rtn', $destination_port_rtn );
            add_variable( 'return_date_field', date( 'd-F-Y', strtotime( $date_of_return ) ) );
            add_variable( 'return_date_format', date( 'D j F Y', strtotime( $date_of_return ) ) );
            add_variable( 'return_availability_result', get_front_return_availability_result( $booking ) );
            add_variable( 'return_route', get_location( $return_port, 'lcname' ) . ' to ' . get_location( $destination_port_rtn, 'lcname' ) );
        }

        if( empty( $type_of_route ) )
        {
            add_variable( 'one_way_css', 'checked' );
            add_variable( 'return_way_css', '' );

            add_variable( 'return_date_css', 'disabled');
            add_variable( 'return_port_css', 'disabled' );
            add_variable( 'destination_to_port_css', 'disabled' );
        }
        else
        {
            add_variable( 'one_way_css', '' );
            add_variable( 'return_way_css', 'checked' );

            add_variable( 'return_date_css', '');
            add_variable( 'return_port_css', '' );
            add_variable( 'destination_to_port_css', '' );
        }

        add_variable( 'depart_port', $depart_port );
        add_variable( 'depart_date', $date_of_depart );
        add_variable( 'depart_date_field', date( 'd-F-Y', strtotime( $date_of_depart ) ) );
        add_variable( 'depart_date_format', date( 'D j F Y', strtotime( $date_of_depart ) ) );
        add_variable( 'departure_availability_result', get_front_departure_availability_result( $booking ) );
        add_variable( 'depart_route', get_location( $depart_port, 'lcname' ) . ' to ' . get_location( $destination_port, 'lcname' ) );

        add_variable( 'get_pmcode', $get_pmcode );
        add_variable( 'adult_num', $adult_num );
        add_variable( 'child_num', $child_num );
        add_variable( 'infant_num', $infant_num );
        add_variable( 'type_of_route', $type_of_route );
        add_variable( 'destination_port', $destination_port );
        add_variable( 'type_of_route_txt', get_type_of_route_txt( $type_of_route ) );
        add_variable( 'passenger_num_detail', get_passenger_num_detail( $adult_num, $child_num, $infant_num ) );
        add_variable( 'depart_port_txt', get_location( $depart_port, 'lcname' ) );
        add_variable( 'destination_port_txt', get_location( $destination_port, 'lcname' ) );

        add_variable( 'chid', $chid );
        add_variable( 'depart_port_list', get_availibility_loc_option( 'From', $depart_port,'from' ) );
        add_variable( 'destination_port_list', get_availibility_loc_option( 'To', $destination_port,'to' ) );
        add_variable( 'return_port_list', get_availibility_loc_option( 'Return From', $return_port,'to' ) );
        add_variable( 'destination_to_port_list', get_availibility_loc_option( 'Return To', $destination_port_rtn,'from' ) );

        add_variable( 'subscribeaction', HTSERVER . site_url() . '/ticket-suscribe-mailchimp' );
        add_variable( 'htsserver', HTSERVER );

        add_variable( 'action', HTSERVER . $site_url . '/ticket-front-booking-process' );
    }
    else
    {
        header( 'location:' . HTSERVER . $site_url );
    }
}
elseif( is_front_review_booking() )
{
    $site_url = site_url();
    $booking  = booking_item();

    //-- Redirect If empty booking
    if( isset( $booking['sess_data'] ) && !empty( $booking['sess_data'] ) )
    {
        extract( $booking['sess_data'] );

        //-- Redirect If date of depart greater than current date
        if( strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
        {
            header( 'location:' . HTSERVER . $site_url );
        }

        //-- Redirect If empty trip
        if( !isset( $trip['departure'] ) )
        {
            header( 'location:' . HTSERVER . $site_url . '/availability-result' );
        }

        set_template( TEMPLATE_PATH . '/review.html' );

        add_variable( 'sess_id', $booking['sess_id'] );

        //-- Check if have error data
        if( isset( $_GET['error'] ) )
        {
            add_variable( 'message', generate_message_block( json_decode( base64_decode( $_GET['error'] ), true ) ) );
        }

        $bsource = empty( $agid ) ? $chid : $chid . '|' . $agid;

        if( isset( $trip['return'] ) )
        {
            add_variable( 'return_css', '' );
            add_variable( 'border_css', 'border-btm');
            add_variable( 'return_port', $return_port );
            add_variable( 'return_date', $date_of_return );
            add_variable( 'destination_port_rtn', $destination_port_rtn );
            add_variable( 'return_date_format', date( 'D j F Y', strtotime( $date_of_return ) ) );
            add_variable( 'return_route', get_location( $return_port, 'lcname' ) . ' to ' . get_location( $destination_port_rtn, 'lcname' ) );

            add_variable( 'return_rid', $trip['return']['id'] );
            add_variable( 'return_boid', $trip['return']['boid'] );
            add_variable( 'return_price', $trip['return']['total'] );
            add_variable( 'return_boat_name', get_boat( $trip['return']['boid'], 'boname' ) );

            if( $trip['return']['discount'] > 0 )
            {
                add_variable( 'return_price_disc_num', '<span class="disc-price">' . number_format( $trip['return']['total'], 0, ',', '.' ) . '</span>' );
                add_variable( 'return_price_num', number_format( ( $trip['return']['total'] - $trip['return']['discount'] ), 0, ',', '.' ) );
            }
            else
            {
                add_variable( 'return_price_num', number_format( $trip['return']['total'], 0, ',', '.' ) );
            }

            add_variable( 'return_passenger_number', get_front_passenger_num_content( $adult_num, $child_num, $infant_num ) );
            add_variable( 'return_trans', get_front_route_detail_transport( $trip['return']['id'], $trip['return']['transport']  ) );
            add_variable( 'return_route_list', get_front_route_detail_list_content( $trip['return']['id'], $return_port, $destination_port_rtn ) );
            add_variable( 'return_rate_price', get_front_rate_price_content( $trip['return']['id'], $adult_num, $child_num, $infant_num, $trip['return'], true ) );
            add_variable( 'return_add_ons', get_front_add_ons_content( $date_of_return, 'return', $trip['return']['sid'], $bsource, $return_port, $destination_port_rtn ) );
            add_variable( 'return_dropoff_transport', get_front_pickup_drop_off_transport( $trip['return']['id'], $return_port, $destination_port_rtn, $trip['return']['transport'], $booking['sess_data'], true ) );
            add_variable( 'return_dropoff_transport_price', get_front_pickup_drop_off_transport_price( $trip['return']['id'], $return_port, $destination_port_rtn, $trip['return']['transport'], $booking['sess_data'], true ) );
        }
        else
        {
            add_variable( 'return_css', 'sr-only' );
            add_variable( 'border_css', '');
        }

        add_variable( 'depart_port', $depart_port );
        add_variable( 'depart_date', $date_of_depart );
        add_variable( 'depart_date_format', date( 'D j F Y', strtotime( $date_of_depart ) ) );
        add_variable( 'depart_route', get_location( $depart_port, 'lcname' ) . ' to ' . get_location( $destination_port, 'lcname' ) );

        add_variable( 'depart_rid', $trip['departure']['id'] );
        add_variable( 'depart_boid', $trip['departure']['boid'] );
        add_variable( 'depart_price', $trip['departure']['total'] );
        add_variable( 'depart_boat_name', get_boat( $trip['departure']['boid'], 'boname' ) );

        if( $trip['departure']['discount'] > 0 )
        {
            add_variable( 'depart_price_disc_num', '<span class="disc-price">' . number_format( $trip['departure']['total'], 0, ',', '.' ) . '</span>' );
            add_variable( 'depart_price_num', number_format( ( $trip['departure']['total'] - $trip['departure']['discount'] ), 0, ',', '.' ) );
        }
        else
        {
            add_variable( 'depart_price_num', number_format( $trip['departure']['total'], 0, ',', '.' ) );
        }

        add_variable( 'depart_passenger_number', get_front_passenger_num_content( $adult_num, $child_num, $infant_num ) );
        add_variable( 'depart_trans', get_front_route_detail_transport( $trip['departure']['id'], $trip['departure']['transport'] ) );
        add_variable( 'depart_route_list', get_front_route_detail_list_content( $trip['departure']['id'], $depart_port, $destination_port ) );
        add_variable( 'depart_rate_price', get_front_rate_price_content( $trip['departure']['id'], $adult_num, $child_num, $infant_num, $trip['departure'] ) );
        add_variable( 'depart_add_ons', get_front_add_ons_content( $date_of_depart, 'departure', $trip['departure']['sid'], $bsource, $depart_port, $destination_port ) );
        add_variable( 'depart_pickup_transport', get_front_pickup_drop_off_transport( $trip['departure']['id'], $depart_port, $destination_port, $trip['departure']['transport'], $booking['sess_data'] ) );
        add_variable( 'depart_pickup_transport_price', get_front_pickup_drop_off_transport_price( $trip['departure']['id'], $depart_port, $destination_port, $trip['departure']['transport'], $booking['sess_data'] ) );

        add_variable( 'discount_css', empty( $promo_code ) ? 'sr-only' : '' );
        add_variable( 'freelance_code_css', empty( $freelance_code ) ? 'sr-only' : '' );
        add_variable( 'promo_code', empty( $promo_code ) ? '-' : 'Promo Code : ' . $promo_code );
        add_variable( 'discount_price_num', empty( $discount ) ? '' : number_format( $discount, 0, ',', '.' ) );

        add_variable( 'get_pmcode', $get_pmcode );
        add_variable( 'img_url', get_theme_img() );
        add_variable( 'type_of_route', $type_of_route );
        add_variable( 'destination_port', $destination_port );
        add_variable( 'subtotal_num', number_format ( $subtotal, 0, ',', '.' ) );
        add_variable( 'grand_total_price', number_format ( $grandtotal, 0, ',', '.' ) );

        add_variable( 'subscribeaction', HTSERVER . site_url() . '/ticket-suscribe-mailchimp' );
        add_variable( 'htsserver', HTSERVER );

        add_variable( 'ajax_link', HTSERVER . $site_url .'/ticket-availability-ajax/' );
        add_variable( 'action', HTSERVER . $site_url . '/ticket-front-booking-process' );

        add_actions( 'include_css', 'get_css_inc', 'jquery-timepicker-addon/jquery-ui-timepicker-addon.min.css' );
        add_actions( 'include_css', 'get_custom_css', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' );
 
        add_actions( 'include_js', 'get_javascript_inc', 'autonumeric-1.9.46/autoNumeric-min.js' ); 
        add_actions( 'include_js', 'get_custom_javascript', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' );
        add_actions( 'include_js', 'get_javascript_inc', 'jquery-timepicker-addon/jquery-ui-timepicker-addon.min.js' );
    }
    else
    {
        header( 'location:' . HTSERVER . $site_url );
    }
}
elseif( is_front_detail_booking() )
{
    $site_url = site_url();
    $booking  = booking_item();

    //-- Redirect If empty session booking
    if( isset( $booking['sess_data'] ) && !empty( $booking['sess_data'] ) )
    {
        extract( $booking['sess_data'] );

        //-- Redirect If date of depart greater than current date
        if( strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
        {
            header( 'location:' . HTSERVER . $site_url );
        }

        //-- Redirect If empty trip
        if( !isset( $trip['departure'] ) )
        {
            header( 'location:' . HTSERVER . $site_url . '/availability-result' );
        }

        set_template( TEMPLATE_PATH . '/booking-detail.html' );

        add_variable( 'sess_id', $booking['sess_id'] );

        if( isset( $trip['return'] ) )
        {
            $rd  = get_route_detail_content( $trip['return']['id'], $return_port, $destination_port_rtn );

            add_variable( 'return_to_port', $rd['arrive'] );
            add_variable( 'return_etd', $rd['depart_time'] );
            add_variable( 'return_eta', $rd['arrive_time'] );
            add_variable( 'return_from_port', $rd['depart'] );
            add_variable( 'return_boat_name', get_boat( $trip['return']['boid'], 'boname' ) );
            add_variable( 'return_date_format', date( 'D j F Y', strtotime( $date_of_return ) ) );
            add_variable( 'return_passenger', get_passenger_num_detail_content( $adult_num, $child_num, $infant_num ) );
            add_variable( 'return_passenger_field_list', passenger_front_field_list_content( $adult_num, $child_num, $infant_num, true ) );
            add_variable( 'return_transport_detail', get_front_return_transport_detail( $trip['return'], $trip['return']['transport'] ) );
            add_variable( 'return_transport', get_front_transport_data( $trip['return'], $trip['return']['transport'] ) );
        }
        else
        {
            add_variable( 'return_css', 'sr-only' );
        }

        $dd  = get_route_detail_content( $trip['departure']['id'], $depart_port, $destination_port );
        $var = get_variable_by_port_type( $depart_port, $destination_port );
        $aid = $var['drop_sts'] ? $dd['depart'] : $dd['arrive'];

        add_variable( 'depart_to_port', $dd['arrive'] );
        add_variable( 'depart_etd', $dd['depart_time'] );
        add_variable( 'depart_eta', $dd['arrive_time'] );
        add_variable( 'depart_from_port', $dd['depart'] );
        add_variable( 'depart_boat_name', get_boat( $trip['departure']['boid'], 'boname' ) );
        add_variable( 'depart_date_format', date( 'D j F Y', strtotime( $date_of_depart ) ) );
        add_variable( 'd_date',  date( 'Y-m-d', strtotime( $date_of_depart ) ) );
        add_variable( 'depart_passenger', get_passenger_num_detail_content( $adult_num, $child_num, $infant_num ) );
        add_variable( 'depart_passenger_field_list', passenger_front_field_list_content( $adult_num, $child_num, $infant_num ) );
        add_variable( 'depart_transport_detail', get_front_depart_transport_detail( $trip['departure'], $trip['departure']['transport'] ) );
        add_variable( 'depart_transport', get_front_transport_data( $trip['departure'], $trip['departure']['transport'] ) );

        add_variable( 'bbcountry', get_front_country_list_option() );
        add_variable( 'payment_method_option', get_payment_method_option() );
        add_variable( 'grand_total_price', number_format ( $grandtotal, 0, ',', '.' ) );

        add_variable( 'suggestion_accommodation_area', $aid );
        add_variable( 'suggestion_accommodation', get_front_suggestion_accommodation( $aid, $trip['departure'] ) );

        add_variable( 'subscribeaction', HTSERVER . site_url() . '/ticket-suscribe-mailchimp' );
        add_variable( 'htsserver', HTSERVER );
        
        add_variable( 'action', HTSERVER . $site_url . '/ticket-front-booking-process' );
    }
    else
    {
        header( 'location:' . HTSERVER . $site_url );
    }
}
elseif( is_front_pay_booking() )
{
    if( !empty( $_GET ) )
    {
        extract( $_GET );

        if( $type == 'paypal' )
        {
            $data = ticket_booking_all_data( $id );

            if( empty( $data ) )
            {
                header( 'Location: ' . HTSERVER . site_url() . '/booking/invalid/?id=' . $id );
            }
            elseif( !in_array( $data['bstatus'], array( 'pp', 'ol' ) ) )
            {
                header( 'Location: ' . HTSERVER . site_url() . '/booking/done-payment/?id=' . $id );
            }
            else
            {
                if( isset( $paymentId ) )
                {
                    if( execute_booking_payment_with_paypal( $paymentId, $PayerID, $id, $btotal ) )
                    {
                        header( 'Location: ' . HTSERVER . site_url() . '/booking/complete/?type=paypal&id=' . $id );
                    }
                    else
                    {
                        header( 'Location: ' . HTSERVER . site_url() . '/booking/failed-payment/?id=' . $id );
                    }
                }
                else
                {
                    $approval_url = create_booking_payment_with_paypal( $data );

                    if( empty( $approval_url ) )
                    {
                        header( 'Location: ' . HTSERVER . site_url() . '/booking/paypal-trouble/?id=' . $id );
                    }
                    else
                    {
                        set_template( TEMPLATE_PATH . '/paypal.html' );
                        add_variable( 'approval_url', $approval_url );
                    }
                }
            }
        }
        elseif( $type == 'nicepay' )
        {
            if( isset( $process ) )
            {
                if( $process == 'result' )
                {
                    $data = execute_payment_result_with_nicepay();

                    if( empty( $data ) )
                    {
                        header( 'Location: ' . HTSERVER . site_url() . '/booking/invalid/' );
                    }
                    else
                    {
                        if( $data['result'] == 'success' )
                        {
                            $bdata = ticket_booking_all_data( $data['bid'] );

                            if( empty( $bdata ) )
                            {
                                header( 'Location: ' . HTSERVER . site_url() . '/booking/invalid/?id=' . $data['bid'] );
                            }
                            else
                            {
                                extract( $bdata );

                                if( isset( $payment['pstatus'] ) && $payment['pstatus'] == '0' )
                                {
                                    //-- Info Transfer Payment Journey to client
                                    set_template( TEMPLATE_PATH . '/confirmation.html' );

                                    add_block( 'bca-block', 'bcablock' );
                                    add_block( 'bni-block', 'bniblock' );
                                    add_block( 'bri-block', 'briblock' );
                                    add_block( 'cimb-block', 'cimbblock' );
                                    add_block( 'permata-block', 'permatablock' );
                                    add_block( 'mandiri-block', 'mandiriblock' );
                                    add_block( 'maybank-block', 'maybankblock' );
                                    add_block( 'danamon-block', 'danamonblock' );
                                    add_block( 'keb-hana-block', 'kebhanablock' );

                                    add_variable( 'bticket', $bticket );
                                    add_variable( 'pbankname', $payment['pbankname'] );
                                    add_variable( 'pvirtualacc', $payment['pvirtualacc'] );
                                    add_variable( 'paccountname', $payment['paccountname'] );
                                    add_variable( 'btotal', number_format( $btotal, 0, '', ',' ) );
                                    add_variable( 'pvirtualaccvaliddate', date( 'd-m-Y', strtotime( $payment['pvirtualaccvaliddate'] ) ) );
                                    add_variable( 'pvirtualaccvalidtime', date( 'H:i:s', strtotime( $payment['pvirtualaccvalidtime'] ) ) );

                                    if( $payment['pbankname'] == 'BCA' )
                                    {
                                        parse_template( 'bca-block', 'bcablock' );
                                    }
                                    elseif( $payment['pbankname'] == 'BNI' )
                                    {
                                        parse_template( 'bni-block', 'bniblock' );
                                    }
                                    elseif( $payment['pbankname'] == 'Mandiri' )
                                    {
                                        parse_template( 'mandiri-block', 'mandiriblock' );
                                    }
                                    elseif( $payment['pbankname'] == 'Permata' )
                                    {
                                        parse_template( 'permata-block', 'permatablock' );
                                    }
                                    elseif( $payment['pbankname'] == 'BII Maybank' )
                                    {
                                        parse_template( 'maybank-block', 'maybankblock' );
                                    }
                                    elseif( $payment['pbankname'] == 'BRI' )
                                    {
                                        parse_template( 'bri-block', 'briblock' );
                                    }
                                    elseif( $payment['pbankname'] == 'CIMB Niaga' )
                                    {
                                        parse_template( 'cimb-block', 'cimbblock' );
                                    }
                                    elseif( $payment['pbankname'] == 'KEB Hana Bank' )
                                    {
                                        parse_template( 'keb-hana-block', 'kebhanablock' );
                                    }
                                    elseif( $payment['pbankname'] == 'Danamon' )
                                    {
                                        parse_template( 'danamon-block', 'danamonblock' );
                                    }
                                }
                                elseif( isset( $payment['pstatus'] ) && $payment['pstatus'] == '1' )
                                {
                                    header( 'Location: ' . HTSERVER . site_url() . '/booking/done-payment/?id=' . $data['bid'] );
                                }
                                else
                                {
                                    header( 'location:' . HTSERVER . site_url() . '/booking/payment/?type=nicepay&id=' . $data['bid'] );
                                }
                            }
                        }
                        elseif( $data['result'] == 'paid' )
                        {
                            header( 'location:' . HTSERVER . site_url() . '/booking/complete/?type=nicepay&id=' . $data['bid'] );
                        }
                        elseif( $data['result'] == 'failed' )
                        {
                            $cctype = get_meta_data( 'cc_payment_gateway_opt');

                            if( $cctype == 'nicepay' )
                            {
                                header( 'Location: ' . HTSERVER . site_url() . '/booking/redirect-secondary-payment/?type=doku&id=' . $data['bid'] );
                            }
                            else
                            {
                                // fungsi langsung kirim email pdf saat gagal pemabayaran tiket tanpa cronjob
                                if( send_payment_notification_to_admin( $data['bid'] ) )
								{
									send_payment_notification_to_client( $data['bid'] );

									ticket_booking_update_status( $data['bid'], 'pf' );
								}
                                header( 'Location: ' . HTSERVER . site_url() . '/booking/failed-payment/?id=' . $data['bid'] );
                            }
                        }
                    }
                }
                elseif( $process == 'notify' )
                {
                    execute_payment_notify_by_nicepay();

                    exit;
                }
            }
            else
            {
                $data = ticket_booking_all_data( $id );

                if( empty( $data ) )
                {
                    header( 'Location: ' . HTSERVER . site_url() . '/booking/invalid/?id=' . $id );
                }
                elseif( !in_array( $data['bstatus'], array( 'pp', 'ol' ) ) )
                {
                    header( 'Location: ' . HTSERVER . site_url() . '/booking/done-payment/?id=' . $id );
                }
                else
                {
                    if( $data['bpaymethod'] == 5 )
                    {
                        $response = create_booking_payment_with_nicepay( $data );

                        if( empty( $response ) )
                        {
                            $cctype = get_meta_data( 'cc_payment_gateway_opt');

                            if( $cctype == 'nicepay' )
                            {
                                header( 'Location: ' . HTSERVER . site_url() . '/booking/redirect-secondary-payment/?type=doku&id=' . $id );
                            }
                            else
                            {
                                header( 'Location: ' . HTSERVER . site_url() . '/booking/failed-payment/?id=' . $did );
                            }
                        }
                        else
                        {
                            set_template( TEMPLATE_PATH . '/nicepay-credit-card.html' );

                            $stamp = date('Ymdhis');
                            $imid  = get_meta_data( 'nicepay_imid', 'ticket_setting' );
                            $mkey  = get_meta_data( 'nicepay_merchant_key', 'ticket_setting' );
                            $token = hash( 'sha256', $stamp . $imid . $response->data->referenceNo . $response->data->amt . $mkey );

                            add_variable( 'bid', $id );
                            add_variable( 'timestamp', $stamp );
                            add_variable( 'merchantToken', $token );
                            add_variable( 'txid', $response->data->tXid );
                            add_variable( 'grand_total', $response->data->amt );
                            add_variable( 'imid', get_meta_data( 'nicepay_imid', 'ticket_setting' ) );
                            add_variable( 'nicepay_action', 'https://www.nicepay.co.id/nicepay/direct/v2/payment' );
                            add_variable( 'callBackUrl', get_meta_data( 'nicepay_callback_url', 'ticket_setting' ) . '&bid=' . $id );
 
                            add_actions( 'include_js', 'get_javascript_inc', 'autonumeric-1.9.46/autoNumeric-min.js' );
                            add_actions( 'include_js', 'get_javascript_inc', 'jquery-mask/dist/jquery.mask.min.js' );
                        }
                    }
                }
            }
        }
        elseif( $type == 'doku' )
        {
            if( isset( $process ) )
            {
                if( execute_booking_payment_with_doku() )
                {
                    list( $var, $bcode, $num ) = explode( '-', $_POST['TRANSIDMERCHANT'] );

                    $bid = ticket_booking_data_by_code( $bcode, 'bid' );

                    header( 'location:' . HTSERVER . site_url() . '/booking/complete/?type=doku&id=' . $bid );
                }
                else
                {
                    $cctype = get_meta_data( 'cc_payment_gateway_opt');

                    if( $cctype == 'doku' )
                    {
                        list( $var, $bcode, $num ) = explode( '-', $_POST['TRANSIDMERCHANT'] );

                        $bid = ticket_booking_data_by_code( $bcode, 'bid' );

                        header( 'Location: ' . HTSERVER . site_url() . '/booking/redirect-secondary-payment/?type=nicepay&id=' . $bid );
                    }
                    else
                    {
                        // fungsi langsung kirim email pdf saat gagal pemabayaran tiket tanpa cronjob
                        if( send_payment_notification_to_admin( $data['bid'] ) )
						{
							send_payment_notification_to_client( $data['bid'] );

							ticket_booking_update_status( $data['bid'], 'pf' );
						}
                        header( 'location:' . HTSERVER . site_url() . '/booking/failed-payment/?id=' . $bid );
                    }
                }
            }
            else
            {
                $data = ticket_booking_all_data( $id );

                if( empty( $data ) )
                {
                    header( 'Location: ' . HTSERVER . site_url() . '/booking/invalid/?id=' . $id );
                }
                elseif( !in_array( $data['bstatus'], array( 'pp', 'ol' ) ) )
                {
                    header( 'Location: ' . HTSERVER . site_url() . '/booking/done-payment/?id=' . $id );
                }
                else
                {
                    set_template( TEMPLATE_PATH . '/doku.html' );

                    $btotal = $data['btotal'] - $data['bonhandtotal'];

                    $doku_url        = get_meta_data( 'doku_url', 'ticket_setting' );
                    $doku_mall_id    = get_meta_data( 'doku_mall_id', 'ticket_setting' );
                    $doku_shared_key = get_meta_data( 'doku_shared_key', 'ticket_setting' );

                    $refcode  = generate_doku_reference_code( $data['bcode'] );
                    $total    = number_format( $btotal, 2, '.', '' );
                    $words    = sha1( $total . $doku_mall_id . $doku_shared_key . $refcode );
                    $basket   = '#' . $data['bticket'] . ',' . $total . ',1,' . $total;

                    add_variable( 'bbname', $data['bbname'] );
                    add_variable( 'bbemail', $data['bbemail'] );
                    add_variable( 'bbphone', empty( $data['bbphone'] ) ? $data['bbphone2'] : $data['bbphone'] );
                    add_variable( 'bbcountry', empty( $data['agid'] ) ? get_country( $data['bbcountry'], 'lcountry' ) : '' );

                    add_variable( 'bid', $id );
                    add_variable( 'words', $words );
                    add_variable( 'btotal', $total );
                    add_variable( 'basket', $basket );
                    add_variable( 'refcode', $refcode );
                    add_variable( 'mall_id', $doku_mall_id );
                    add_variable( 'session_id', get_random_word() );
                    add_variable( 'current_date', date( 'YmdHis', time() ) );
                    add_variable( 'doku_url', empty( $doku_url ) ? 'http://staging.doku.com/Suite/Receive' : 'https://pay.doku.com/Suite/Receive' );
                }
            }
        }
    }
}
elseif( is_front_detail_reservation() )
{
    $id = '';

    if( isset( $_GET['id'] ) )
    {
        $id = $_GET['id'];
    }
    else if( isset( $_GET['code'] ) )
    {
        $id = get_booking_id_by_ticket( $_GET['code'] );
    }

    if( empty( $id ) === false )
    {
        $message = run_booking_update();
        $data    = get_front_reservation_detail( $id );
        
        if( empty( $data ) )
        {
            header( 'Location: ' . HTSERVER . site_url() . '/booking/invalid/?id=' . $id );
        }
        else
        {
            extract( $data['detail'] );

            add_variable( 'site_url', site_url() );
            add_variable( 'booking_id', $data['bid'] );
            add_variable( 'bticket', $data['bticket'] );
            add_variable( 'bstatus_message', $data['bstatus_txt'] );
            add_variable( 'bbcountry', ticket_passenger_country( $data['bbcountry'] ) );

            add_variable( 'bbname', $data['bbname'] );
            add_variable( 'bbemail', $data['bbemail'] );
            add_variable( 'bbphone', empty( $data['bbphone'] ) ? $data['bbphone2'] : $data['bbphone'] );

            if( empty( $data['agid'] ) )
            {
                $bsource = $data['chname'];
                $bbedit  = '';

                add_variable( 'agid', '' );
            }
            else
            {
                $agid    = empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'];
                $bsource = get_agent( $agid, 'agname' );
                $bbedit  = 'disabled';

                add_variable( 'agid', $data['agid'] );
            }

            if( isset( $departure ) )
            {
                foreach( $departure as $dp_trip )
                {
                    extract( $dp_trip );

                    if( $bdtranstype == '1' )
                    {
                        $dep_trans_detail = '';

                        foreach( $transport as $ttype => $trans_data )
                        {
                            foreach( $trans_data as $td )
                            {
                                $rpto   = date( 'H:i', strtotime( $td['btrpto'] ) );
                                $rpfrom = date( 'H:i', strtotime( $td['btrpfrom'] ) );
                                $area   = get_transport_area( $td['taid'], 'taname' );
                                $trsloc = $ttype == 'pickup' ? $bdfrom : $bdto;

                                $dep_trans_detail .= '
                                <p class="heading-pickup">' . ucfirst( $ttype ) . '</p>
                                <p class="text">' . ( $td['bttrans_type'] == '0' ? 'Shared Transport, ' : 'Privated Transport, ' ) . '<br><span>' . $area . '</span></p>';
                            }
                        }

                        add_variable( 'dep_trans_detail', $dep_trans_detail );
                    }
                    else
                    {
                        add_variable( 'dep_trans_detail', 'Own Transport' );
                    }

                    add_variable( 'dep_bdto', $bdto );
                    add_variable( 'dep_bdfrom', $bdfrom );
                    add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                    add_variable( 'dep_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                    add_variable( 'dep_pass_num_detail', get_front_passenger_num_content( $num_adult, $num_child, $num_infant ) );
                }
            }

            add_variable( 'btype', ticket_booking_type( $data['btype'] ) );
            add_variable( 'bdate', date( 'd M Y', strtotime( $data['bdate'] ) ) );

            add_variable( 'edit_link', HTSERVER . site_url() . '/detail-reservation/?id=' . $data['bid'] . '&type=edit' );
            add_variable( 'detail_link', HTSERVER . site_url() . '/detail-reservation/?id=' . $data['bid'] . '&type=detail' );
            add_variable( 'canceled_link', HTSERVER . site_url() . '/detail-reservation/?id=' . $data['bid'] . '&type=canceled' );
            add_variable( 'feedback_link', HTSERVER . site_url() . '/detail-reservation/?id=' . $data['bid'] . '&type=feedback' );

            add_variable( 'edit_link_css', in_array( $data['bstatus'], array( 'cr', 'cn' ) ) ? 'sr-only' : '' );
            add_variable( 'canceled_link_css', in_array( $data['bstatus'], array( 'cr', 'cn' ) ) ? 'sr-only' : '' );

            if( isset( $_GET['type'] ) )
            {
                set_template( TEMPLATE_PATH . '/reservation-popup-' . $_GET['type'] . '.html' );

                if( $_GET['type'] == 'detail' )
                {
                    add_block( 'detail-departure-loop-block', 'dtdl-block' );
                    add_block( 'detail-departure-block', 'dtd-block' );
                    add_block( 'detail-return-loop-block', 'dtrl-block' );
                    add_block( 'detail-return-block', 'dtr-block' );
                    add_block( 'main-block', 'm-block' );
                }
                else
                {
                    add_block( 'main-block', 'm-block' );
                }

                add_variable( 'bchannel', $bsource );
                add_variable( 'booked_by_edit', $bbedit );
                add_variable( 'bbcountry_list_option', get_country_list_option( $data['bbcountry'] ) );

                //-- Go Trip
                if( isset( $departure ) )
                {
                    foreach( $departure as $dp_trip )
                    {
                        extract( $dp_trip );

                        $var = get_variable_by_port_type( $bdfrom_id, $bdto_id );

                        $dep_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                        $dep_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                        $dep_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                        $dep_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';

                        add_variable( 'dep_bdto', $bdto );
                        add_variable( 'dep_bdfrom', $bdfrom );
                        add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                        add_variable( 'dep_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                        add_variable( 'dep_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );
                        add_variable( 'dep_passenger', get_front_passenger_list_content( $passenger ) );
                        add_variable( 'dep_passenger_edit', get_front_passenger_list_content( $passenger, true ) );

                        add_variable( 'dep_pa_display', $dep_pa_display );
                        add_variable( 'dep_pc_display', $dep_pc_display );
                        add_variable( 'dep_pi_display', $dep_pi_display );
                        add_variable( 'dep_pt_display', $dep_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );

                        add_variable( 'dep_add_ons', get_detail_add_ons_content( $dp_trip ) );
                        add_variable( 'dep_transport', get_detail_transport_content( $transport, $bdfrom, $bdto ) );
                        add_variable( 'dep_transport_edit', get_pickup_dropoff_transport_edit_field( $dp_trip ) );

                        parse_template( 'detail-departure-loop-block', 'dtdl-block', true );
                    }

                    parse_template( 'detail-departure-block', 'dtd-block' );
                }

                if( $data['btype'] == '1' )
                {
                    //-- Back Trip
                    if( isset( $return ) )
                    {
                        foreach( $return as $rt_trip )
                        {
                            extract( $rt_trip );

                            $rtn_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                            $rtn_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                            $rtn_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                            $rtn_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';
                            $rtn_lctype      = get_location_by_name( $bdfrom, 'lctype' );

                            add_variable( 'rtn_bdto', $bdto );
                            add_variable( 'rtn_bdfrom', $bdfrom );
                            add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                            add_variable( 'rtn_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                            add_variable( 'rtn_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );
                            add_variable( 'rtn_passenger', get_front_passenger_list_content( $passenger ) );
                            add_variable( 'rtn_passenger_edit', get_front_passenger_list_content( $passenger, true ) );

                            add_variable( 'rtn_discount', $discount );
                            add_variable( 'rtn_price_per_adult', $price_per_adult );
                            add_variable( 'rtn_price_per_child', $price_per_child );
                            add_variable( 'rtn_price_per_infant', $price_per_infant );

                            add_variable( 'rtn_pa_display', $rtn_pa_display );
                            add_variable( 'rtn_pc_display', $rtn_pc_display );
                            add_variable( 'rtn_pi_display', $rtn_pi_display );
                            add_variable( 'rtn_pt_display', $rtn_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );

                            add_variable( 'rtn_add_ons', get_detail_add_ons_content( $rt_trip ) );
                            add_variable( 'rtn_transport', get_detail_transport_content( $transport, $bdfrom, $bdto ) );
                            add_variable( 'rtn_transport_edit', get_pickup_dropoff_transport_edit_field( $rt_trip, true ) );

                            parse_template( 'detail-return-loop-block', 'dtrl-block', true );
                        }

                        parse_template( 'detail-return-block', 'dtr-block' );
                    }
                }

                if( empty( $message ) === false )
                {
                    add_variable( 'feedback_status', $message['type'] );
                    add_variable( 'feedback_message', $message['content'][0] );
                }

                add_variable( 'subtotal', $data['bsubtotal'] );
                add_variable( 'discount', $data['bdiscount'] );
                add_variable( 'sub_css', empty( $data['pmcode'] ) ? 'sr-only' : '' );
                add_variable( 'pmcode', empty( $data['pmcode'] ) ? '' : 'Code : ' . $data['pmcode'] );

                add_variable( 'subtotal_display', number_format( $data['bsubtotal'], 0, ',', '.' ) );
                add_variable( 'discount_display', number_format( $data['bdiscount'], 0, ',', '.' ) );
                add_variable( 'grandtotal_display', number_format( $data['btotal'], 0, ',', '.' ) );

                add_variable( 'maction', '' );
                add_variable( 'home_link', HTSERVER . site_url() );

                add_actions( 'include_js', 'get_custom_javascript', HTSERVER . TEMPLATE_URL . '/js/numeral.min.js' );
            }
            else
            {
                set_template( TEMPLATE_PATH . '/reservation-detail.html' );

                if( $data['btype'] == '1' )
                {
                    if( isset( $return ) )
                    {
                        foreach( $return as $rt_trip )
                        {
                            $finish_date = strtotime( $rt_trip['bddate'] );

                            break;
                        }
                    }
                }
                else
                {
                    if( isset( $departure ) )
                    {
                        foreach( $departure as $dp_trip )
                        {
                            $finish_date = strtotime( $dp_trip['bddate'] );

                            break;
                        }
                    }
                }

                if( isset( $finish_date ) )
                {
                    $current_date  = strtotime( date( 'Y-m-d' ) );
                    $feedback_data = get_feedback_by_booking_id( $data['bid'] );

                    if( $current_date > $finish_date && in_array( $data['bstatus'], array( 'pa', 'ca', 'pp' ) ) && empty( $feedback_data ) )
                    {
                        add_variable( 'feedback_css', '' );
                    }
                    else
                    {
                        add_variable( 'feedback_css', 'sr-only' );
                    }
                }
                else
                {
                    add_variable( 'feedback_css', 'sr-only' );
                }

                add_variable( 'maction', HTSERVER . site_url() . '/detail-reservation' );
				add_variable( 'subscribeaction', HTSERVER . site_url() . '/ticket-suscribe-mailchimp' );
				add_variable( 'htsserver', HTSERVER );

                add_actions( 'include_css', 'get_css_inc', 'fancybox/dist/jquery.fancybox.min.css' );
                add_actions( 'include_js', 'get_javascript_inc', 'fancybox/dist/jquery.fancybox.min.js' );
            }
        }
    }
    else
    {
        if( !empty( $_POST ) )
        {
            extract( $_POST );
        }

        if( isset( $find_reservation ) )
        {
            $bid = get_booking_id_by_ticket_and_email( $bticket, $bbemail );

            if( empty( $bid ) )
            {
                $title   = 'Not Valid Reservation';
                $ptitle  = 'Booking Detail - BlueWater Express';
                $content = '<p>Sorry, we can\'t find reservation with ticket number #' . $bticket .' and booking email ' . $bbemail . '<br>
                            If you need any help or any question, please contact us at
                            <a href="mailto:' . get_meta_data( 'email_reservation' ) . '?subject=feedback">' . get_meta_data( 'email_reservation' ) . '</a> or call
                            <a href="tel:' . get_meta_data( 'phone' ) . '">' . get_meta_data( 'phone' ) . '</a></p>';

                set_message_page_content( $ptitle, $title, $content );
            }
            else
            {
                header( 'location:' . HTSERVER . site_url() . '/detail-reservation/?id=' . $bid );
            }
        }
    }
}
elseif( is_front_feedback_reservation() )
{
    if( isset( $_GET['id'] ) )
    {
        $message = run_booking_update();
        $data    = get_front_reservation_detail( $_GET['id'] );

        extract( $data );

        if( empty( $data ) )
        {
            header( 'Location: ' . HTSERVER . site_url() . '/booking/invalid/?id=' . $id );
        }
        else
        {
            set_template( TEMPLATE_PATH . '/feedback.html' );

            add_variable( 'booking_id', $data['bid'] );
            add_variable( 'bticket', $data['bticket'] );
            add_variable( 'bbemail', $data['bbemail'] );
            add_variable( 'bbname', $data['bbname'] );
            add_variable( 'message', generate_message_block( $message ) );

            add_variable( 'home_link', HTSERVER . site_url() );
            add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-feedback-ajax/' );
        }
    }
    else
    {
        $title   = 'Page Not Found';
        $ptitle  = 'Page Not Found - BlueWater Express';
        $content = '<p>The page you are looking for does not exist.<br />Return to the <a href="' . HTSERVER . site_url() . '">home page.</a></p>';

        set_message_page_content( $ptitle, $title, $content );
    }
}
elseif( is_front_complete_booking() )
{
    set_template( TEMPLATE_PATH . '/complete.html' );
    $title      = 'Thank You for Using Our Services';
    $ptitle     = 'Payment Complete - BlueWater Express';
    $content    = '<p>We always strive in providing the best services to our guest, should you need any help or any question, please contact us at
                   <a href="mailto:' . get_meta_data( 'email_reservation' ) . '?subject=feedback">' . get_meta_data( 'email_reservation' ) . '</a> or call
                   <a href="tel:' . get_meta_data( 'phone' ) . '">' . get_meta_data( 'phone' ) . '</a></p>';

    add_variable( 'title', $title );
    add_variable( 'content', $content );
    add_variable( 'page_title', $ptitle );
	add_variable( 'recomendation_villa',get_recomendation_villa() );
    add_variable( 'conversion', get_booking_conversion() );
}
elseif( is_front_subscribing() )
{
    set_template( TEMPLATE_PATH . '/thankyou.html' );
    $title      = 'Thank you for subscribing';
    $ptitle     = 'Subscribe Complete - BlueWater Express';
    $content    = '<p>Your first subscription email will arriving soon, should you need any help or any question, please contact us at
                   <a href="mailto:' . get_meta_data( 'email_reservation' ) . '?subject=feedback">' . get_meta_data( 'email_reservation' ) . '</a> or call
                   <a href="tel:' . get_meta_data( 'phone' ) . '">' . get_meta_data( 'phone' ) . '</a></p>';

    add_variable( 'title', $title );
    add_variable( 'content', $content );
    add_variable( 'page_title', $ptitle );
}
elseif( is_front_canceled_booking() )
{
    $title   = 'Canceled';
    $ptitle  = 'Payment Canceled - BlueWater Express';
    $content = '<p>Sorry, it seems you canceled the payment process.</p>
                <p>We always strive in providing the best services to our guest, should you need any help or any question, please contact us at
                <a href="mailto:' . get_meta_data( 'email_reservation' ) . '?subject=feedback">' . get_meta_data( 'email_reservation' ) . '</a> or call
                <a href="tel:' . get_meta_data( 'phone' ) . '">' . get_meta_data( 'phone' ) . '</a></p>';

    set_message_page_content( $ptitle, $title, $content );
}
elseif( is_front_invalid_booking() )
{
    $title   = 'Not Valid Booking';
    $ptitle  = 'Booking Invalid - BlueWater Express';
    $content = '<p>Sorry, we can\'t find this booking data<br>Please <a href="mailto:' . get_meta_data( 'email_reservation' ) . '">notify us</a> for our support</p>';

    set_message_page_content( $ptitle, $title, $content );
}
elseif( is_front_paypal_trouble() )
{
    $title   = 'Trouble With Paypal';
    $ptitle  = 'Paypal Trouble - BlueWater Express';
    $content = '<p>Sorry this request to paypal payment has been failed<br>Please <a href="mailto:' . get_meta_data( 'email_reservation' ) . '">notify us</a> for our support</p>';

    set_message_page_content( $ptitle, $title, $content );
}
elseif( is_front_failed_payment() )
{
    if( !empty( $_GET ) )
    {
        extract( $_GET );
    }

    if( isset( $id ) )
    {
        if( ticket_booking_update_status( $id, 'pf' ) )
        {
            send_payment_notification_to_client( $id );
        }
    }

    $title   = 'Payment Failed';
    $ptitle  = 'Payment Failed - BlueWater Express';
    $content = '<p>Sorry, it seems your payment process failed.</p>
                <p>If you need any help or any question, please contact us at
                <a href="mailto:' . get_meta_data( 'email_reservation' ) . '?subject=feedback">' . get_meta_data( 'email_reservation' ) . '</a> or call
                <a href="tel:' . get_meta_data( 'phone' ) . '">' . get_meta_data( 'phone' ) . '</a></p>';

    set_message_page_content( $ptitle, $title, $content );
}
elseif( is_front_done_payment() )
{
    $title   = 'Payment Already Done';
    $ptitle  = 'Payment Done - BlueWater Express';
    $content = '<p>Sorry, it seems payment has been done already for this booking ticket</p>
                <p>We always strive in providing the best services to our guest, should you need any help or any question, please contact us at
                <a href="mailto:' . get_meta_data( 'email_reservation' ) . '?subject=feedback">' . get_meta_data( 'email_reservation' ) . '</a> or call
                <a href="tel:' . get_meta_data( 'phone' ) . '">' . get_meta_data( 'phone' ) . '</a></p>';

    set_message_page_content( $ptitle, $title, $content );
}
elseif( is_front_redirect_second_payment() )
{
    if( !empty( $_GET ) )
    {
        extract( $_GET );
    }

    if( isset( $id ) && isset( $type ) )
    {
        $title   = 'Payment Failed';
        $ptitle  = 'Payment Failed - BlueWater Express';
        $content = '<p>Sorry, it seems your payment process with ' . ( $type == 'doku' ? 'NICEPay' : 'DOKU' ) . ' failed.</p>
                    <p>We will try with another payment gateway, please wait until your browser redirect or click
                    <a class="redirect-link" href="' . HTSERVER . site_url() . '/booking/payment/?type=' . $type . '&id=' . $id . '">this link</a></p>';

        set_message_page_content( $ptitle, $title, $content );
    }
    else
    {
        $title   = 'Payment Failed';
        $ptitle  = 'Payment Failed - BlueWater Express';
        $content = '<p>Sorry, it seems your payment process failed.</p>
                    <p>If you need any help or any question, please contact us at
                    <a href="mailto:' . get_meta_data( 'email_reservation' ) . '?subject=feedback">' . get_meta_data( 'email_reservation' ) . '</a> or call
                    <a href="tel:' . get_meta_data( 'phone' ) . '">' . get_meta_data( 'phone' ) . '</a></p>';

        set_message_page_content( $ptitle, $title, $content );
    }
}
elseif( is_front_recheck_reservation() )
{
    $id = '';

    if( isset( $_GET['id'] ) )
    {
        $id = $_GET['id'];
    }
    else if( isset( $_GET['code'] ) )
    {
        $id = get_booking_id_by_ticket( $_GET['code'] );
    }

    if( empty( $id ) === false )
    {
        run_booking_recheck();
        
        $data = get_front_reservation_detail( $id );
    
        if( empty( $data ) )
        {
            header( 'Location: ' . HTSERVER . site_url() . '/booking/invalid/?id=' . $id );
        }
        else
        {
            extract( $data['detail'] );

            set_template( TEMPLATE_PATH . '/recheck.html' );

            add_block( 'detail-departure-loop-block', 'dtdl-block' );
            add_block( 'detail-departure-block', 'dtd-block' );
            add_block( 'detail-return-loop-block', 'dtrl-block' );
            add_block( 'detail-return-block', 'dtr-block' );
            add_block( 'main-block', 'm-block' );

            add_variable( 'flash_message', generate_message_block() );
            add_variable( 'background_image', get_background_image_url() );

            add_variable( 'site_url', site_url() );
            add_variable( 'booking_id', $data['bid'] );
            add_variable( 'bticket', $data['bticket'] );
            add_variable( 'bstatus_message', $data['bstatus_txt'] );
            add_variable( 'btype_txt', get_type_of_route_txt( $data[ 'btype' ] ) );
            add_variable( 'bbcountry', ticket_passenger_country( $data['bbcountry'] ) );

            add_variable( 'bbname', $data['bbname'] );
            add_variable( 'bbemail', $data['bbemail'] );
            add_variable( 'bbphone', empty( $data['bbphone'] ) ? $data['bbphone2'] : $data['bbphone'] );

            if( empty( $data['agid'] ) )
            {
                $bsource = $data['chname'];
                $bbedit  = '';

                add_variable( 'agid', '' );
            }
            else
            {
                $agid    = empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'];
                $bsource = get_agent( $agid, 'agname' );
                $bbedit  = 'disabled';

                add_variable( 'agid', $data['agid'] );
            }

            if( isset( $departure ) )
            {
                $ftrip = reset( $departure );

                add_variable( 'depart_to_txt', $ftrip[ 'bdto' ] );
                add_variable( 'depart_from_txt', $ftrip[ 'bdfrom' ] );

                foreach( $departure as $dp_trip )
                {
                    extract( $dp_trip );

                    if( $bdtranstype == '1' )
                    {
                        $dep_trans_detail = '';

                        foreach( $transport as $ttype => $trans_data )
                        {
                            foreach( $trans_data as $td )
                            {
                                $rpto   = date( 'H:i', strtotime( $td['btrpto'] ) );
                                $rpfrom = date( 'H:i', strtotime( $td['btrpfrom'] ) );
                                $area   = get_transport_area( $td['taid'], 'taname' );
                                $trsloc = $ttype == 'pickup' ? $bdfrom : $bdto;

                                $dep_trans_detail .= '
                                <p class="heading-pickup">' . ucfirst( $ttype ) . '</p>
                                <p class="text">' . ( $td['bttrans_type'] == '0' ? 'Shared Transport, ' : 'Privated Transport, ' ) . '<br><span>' . $area . '</span></p>';
                            }
                        }

                        add_variable( 'dep_trans_detail', $dep_trans_detail );
                    }
                    else
                    {
                        add_variable( 'dep_trans_detail', 'Own Transport' );
                    }

                    add_variable( 'dep_bdto', $bdto );
                    add_variable( 'dep_bdfrom', $bdfrom );
                    add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                    add_variable( 'dep_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                    add_variable( 'dep_pass_num_detail', get_front_passenger_num_content( $num_adult, $num_child, $num_infant ) );
                }
            }
            
            add_variable( 'btype', ticket_booking_type( $data['btype'] ) );
            add_variable( 'bdate', date( 'd M Y', strtotime( $data['bdate'] ) ) );

            add_variable( 'bchannel', $bsource );
            add_variable( 'booked_by_edit', $bbedit );
            add_variable( 'bbcountry_list_option', get_country_list_option( $data['bbcountry'] ) );

            //-- Go Trip
            if( isset( $departure ) && !empty( $departure ) )
            {
                foreach( $departure as $dp_trip )
                {
                    extract( $dp_trip );

                    $var = get_variable_by_port_type( $bdfrom_id, $bdto_id );

                    $dep_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                    $dep_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                    $dep_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                    $dep_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';

                    add_variable( 'dep_bdto', $bdto );
                    add_variable( 'dep_bdfrom', $bdfrom );
                    add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                    add_variable( 'dep_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                    add_variable( 'dep_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );

                    add_variable( 'dep_pa_display', $dep_pa_display );
                    add_variable( 'dep_pc_display', $dep_pc_display );
                    add_variable( 'dep_pi_display', $dep_pi_display );
                    add_variable( 'dep_pt_display', $dep_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );

                    if( !empty( $transport ) )
                    {
                        $trans = '';

                        foreach( $transport as $type => $obj )
                        {
                            $trans .= '
                            <div class="col-xs-12 col-list">
                                <div class="box">';

                                    foreach( $obj as $d )
                                    {
                                        $trans_loc  = $type == 'pickup' ? $bdfrom : $bdto;
                                        $trans_area = get_transport_area( $d['taid'], 'taname' );
                                        $trans_type = $d['bttrans_type'] == '0' ? 'Shared Transport' : ( $d['bttrans_type'] == '1' ? 'Privated Transport' : 'Own Transport' );
                                        $trans_css  = $d['bttrans_type'] == '2' ? 'sr-only' : '';
                                        $trans_fee  = $d['bttrans_fee'] == 0 ? 'Free' : number_format( $d['bttrans_fee'], 0, ',', '.' );

                                        $hotel_addr  = empty( $d['bthoteladdress'] ) ? '' : '<br>' . $d['bthoteladdress'];
                                        $hotel_phone = empty( $d['bthotelphone'] ) ? '' : '<br>P. ' . $d['bthotelphone'];
                                        $hotel_email = empty( $d['bthotelemail'] ) ? '' : '<br>E. ' . $d['bthotelemail'];

                                        $trans .= '
                                        <p class="ctitle">' . ucfirst( $type ) . ' - ' . $trans_loc . '</p>
                                        <p class="body"><strong>' . $trans_type . '</strong><br>' . $trans_area . '</p>
                                        <p class="desc ' . $trans_css . '">' . $d['bthotelname'] . $hotel_addr . $hotel_phone . $hotel_email . '</p>';
                                    }

                                    $trans .= '
                                </div>
                            </div>';
                        }

                        add_variable( 'dep_transport', $trans );
                    }

                    if( empty( $dp_trip['add-ons'] ) === false )
                    {
                        $adds = '
                        <div class="col-xs-12 col-list col-blue">
                            <div class="box">
                                <p class="ctitle">Add-ons</p>
                            </div>
                        </div>';

                        foreach( $dp_trip['add-ons'] as $d )
                        {
                            $adds .= '
                            <div class="col-xs-12 col-list">
                                <div class="box">
                                    ' . $d['baopax'] . ' x '. $d['aoname'] . '
                                </div>
                            </div>';
                        }

                        add_variable( 'dep_add_ons', $adds );
                    }

                    if( !empty( $passenger ) )
                    {
                        $pass = '
                        <div class="col-xs-12 col-list col-blue">
                            <div class="box">
                                <p class="ctitle">Passenger Details</p>
                            </div>
                        </div>';

                        foreach( $passenger as $obj )
                        {
                            foreach( $obj as $i => $d )
                            {
                                if( $d['bpstatus'] == 'cn' )
                                {
                                    continue;
                                }

                                $pass .= '
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-list">
                                    <div class="box">
                                        <p class="ctitle">Name</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="" value="' . $d['bpname'] . '" name="passenger[' . $d['bpid'] .'][bpname]">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-list">
                                    <div class="box">
                                        <p class="ctitle">Gender</p>
                                        <div class="form-group">
                                            <select name="passenger[' . $d['bpid'] . '][bpgender]" class="select-option form-control" autocomplete="off">
                                                ' . get_gender_option( $d['bpgender'], false ) . '
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-list">
                                    <div class="box">
                                        <p class="ctitle">Nationality</p>
                                        <div class="form-group">
                                            <select name="passenger[' . $d['bpid'] . '][lcountry_id]" class="select-option form-control" autocomplete="off">
                                                ' . get_country_list_option( $d['lcountry_id'] ) . '
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-list">
                                    <div class="box">
                                        <p class="ctitle">Date Of Birth</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control txt-date-' . $d['bptype'] . '" id="" value="' . date( 'd M Y', strtotime( $d['bpbirthdate'] ) ) . '" name="passenger[' . $d['bpid'] . '][bpbirthdate]">
                                        </div>
                                    </div>
                                </div>';
                            }
                        }

                        add_variable( 'dep_passenger', $pass );
                    }

                    parse_template( 'detail-departure-loop-block', 'dtdl-block', true );
                }

                parse_template( 'detail-departure-block', 'dtd-block' );
            }

            //-- Back Trip
            if( isset( $return ) && !empty( $return ) )
            {
                foreach( $return as $rt_trip )
                {
                    extract( $rt_trip );

                    $rtn_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                    $rtn_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                    $rtn_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                    $rtn_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';
                    $rtn_lctype      = get_location_by_name( $bdfrom, 'lctype' );

                    add_variable( 'rtn_bdto', $bdto );
                    add_variable( 'rtn_bdfrom', $bdfrom );
                    add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                    add_variable( 'rtn_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                    add_variable( 'rtn_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );

                    add_variable( 'rtn_discount', $discount );
                    add_variable( 'rtn_price_per_adult', $price_per_adult );
                    add_variable( 'rtn_price_per_child', $price_per_child );
                    add_variable( 'rtn_price_per_infant', $price_per_infant );

                    add_variable( 'rtn_pa_display', $rtn_pa_display );
                    add_variable( 'rtn_pc_display', $rtn_pc_display );
                    add_variable( 'rtn_pi_display', $rtn_pi_display );
                    add_variable( 'rtn_pt_display', $rtn_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );

                    if( !empty( $transport ) )
                    {
                        $trans = '';

                        foreach( $transport as $type => $obj )
                        {
                            $trans .= '
                            <div class="col-xs-12 col-list">
                                <div class="box">';

                                    foreach( $obj as $d )
                                    {
                                        $trans_loc  = $type == 'pickup' ? $bdfrom : $bdto;
                                        $trans_area = get_transport_area( $d['taid'], 'taname' );
                                        $trans_type = $d['bttrans_type'] == '0' ? 'Shared Transport' : ( $d['bttrans_type'] == '1' ? 'Privated Transport' : 'Own Transport' );
                                        $trans_css  = $d['bttrans_type'] == '2' ? 'sr-only' : '';
                                        $trans_fee  = $d['bttrans_fee'] == 0 ? 'Free' : number_format( $d['bttrans_fee'], 0, ',', '.' );

                                        $hotel_addr  = empty( $d['bthoteladdress'] ) ? '' : '<br>' . $d['bthoteladdress'];
                                        $hotel_phone = empty( $d['bthotelphone'] ) ? '' : '<br>P. ' . $d['bthotelphone'];
                                        $hotel_email = empty( $d['bthotelemail'] ) ? '' : '<br>E. ' . $d['bthotelemail'];

                                        $trans .= '
                                        <p class="ctitle">' . ucfirst( $type ) . ' - ' . $trans_loc . '</p>
                                        <p class="body"><strong>' . $trans_type . '</strong><br>' . $trans_area . '</p>
                                        <p class="desc ' . $trans_css . '">' . $d['bthotelname'] . $hotel_addr . $hotel_phone . $hotel_email . '</p>';
                                    }

                                    $trans .= '
                                </div>
                            </div>';
                        }

                        add_variable( 'rtn_transport', $trans );
                    }
                    
                    if( empty( $rt_trip['add-ons'] ) === false )
                    {
                        $adds = '
                        <div class="col-xs-12 col-list col-blue">
                            <div class="box">
                                <p class="ctitle">Add-ons</p>
                            </div>
                        </div>';

                        foreach( $rt_trip['add-ons'] as $d )
                        {
                            $adds .= '
                            <div class="col-xs-12 col-list">
                                <div class="box">
                                    ' . $d['baopax'] . ' x '. $d['aoname'] . '
                                </div>
                            </div>';
                        }

                        add_variable( 'rtn_add_ons', $adds );
                    }

                    if( !empty( $passenger ) )
                    {
                        $pass = '
                        <div class="col-xs-12 col-list col-blue">
                            <div class="box">
                                <p class="ctitle">Passenger Details</p>
                            </div>
                        </div>';

                        foreach( $passenger as $obj )
                        {
                            foreach( $obj as $i => $d )
                            {
                                if( $d['bpstatus'] == 'cn' )
                                {
                                    continue;
                                }

                                $pass .= '
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-list">
                                    <div class="box">
                                        <p class="ctitle">Name</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="" value="' . $d['bpname'] . '" name="passenger[' . $d['bpid'] .'][bpname]">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-list">
                                    <div class="box">
                                        <p class="ctitle">Gender</p>
                                        <div class="form-group">
                                            <select name="passenger[' . $d['bpid'] . '][bpgender]" class="select-option form-control" autocomplete="off">
                                                ' . get_gender_option( $d['bpgender'], false ) . '
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-list">
                                    <div class="box">
                                        <p class="ctitle">Nationality</p>
                                        <div class="form-group">
                                            <select name="passenger[' . $d['bpid'] . '][lcountry_id]" class="select-option form-control" autocomplete="off">
                                                ' . get_country_list_option( $d['lcountry_id'] ) . '
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-list">
                                    <div class="box">
                                        <p class="ctitle">Date Of Birth</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control txt-date-' . $d['bptype'] . '" id="" value="' . date( 'd M Y', strtotime( $d['bpbirthdate'] ) ) . '" name="passenger[' . $d['bpid'] . '][bpbirthdate]">
                                        </div>
                                    </div>
                                </div>';
                            }
                        }

                        add_variable( 'rtn_passenger', $pass );
                    }

                    parse_template( 'detail-return-loop-block', 'dtrl-block', true );
                }

                parse_template( 'detail-return-block', 'dtr-block' );
            }

            add_variable( 'subtotal', $data['bsubtotal'] );
            add_variable( 'discount', $data['bdiscount'] );
            add_variable( 'sub_css', empty( $data['pmcode'] ) ? 'sr-only' : '' );
            add_variable( 'pmcode', empty( $data['pmcode'] ) ? '' : 'Code : ' . $data['pmcode'] );

            add_variable( 'subtotal_display', number_format( $data['bsubtotal'], 0, ',', '.' ) );
            add_variable( 'discount_display', number_format( $data['bdiscount'], 0, ',', '.' ) );
            add_variable( 'grandtotal_display', number_format( $data['btotal'], 0, ',', '.' ) );

            add_variable( 'maction', '' );
            add_variable( 'home_link', HTSERVER . site_url() );

            add_actions( 'include_js', 'get_custom_javascript', HTSERVER . TEMPLATE_URL . '/js/numeral.min.js' );
            add_actions( 'include_js', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.12/ouibounce.min.js' );
        }
    }
}
elseif( is_testing_script() )
{
    global $db;

    // $retval = 1;

    // $db->begin();

    // $s = 'SELECT * 
    //       FROM ticket_booking_detail AS a 
    //       LEFT JOIN ticket_booking_transport AS b ON b.bdid = a.bdid 
    //       RIGHT JOIN ticket_booking AS c ON a.bid = c.bid
    //       WHERE a.bddate BETWEEN %s AND %s AND b.taid <> "" AND c.bstt = "aa"
    //       ORDER BY a.bddate ASC';
    // $q = $db->prepare_query( $s, '2022-05-31', '2023-12-31' );
    // $r = $db->do_query( $q );

    // if( !is_array( $r ) )
    // {
    //     $dta = array();

    //     $content = '
    //     <table border="1" cellpadding="5">
    //         <tr>
    //             <th>No</th>
    //             <th>Ticket</th>
    //             <th>Route</th>
    //             <th>Area</th>
    //             <th>From</th>
    //             <th>To</th>
    //             <th>Old Time</th>
    //             <th>New Time</th>
    //         </tr>';

    //         $no = 1;

    //         while( $d = $db->fetch_array( $r ) )
    //         {
    //             if( $d[ 'bttype' ] == 'pickup' )
    //             {
    //                 $lcid = $d[ 'bdfrom_id' ];
    //             }
    //             else
    //             {
    //                 $lcid = $d[ 'bdto_id' ];    
    //             }

    //             //-- GET pickup/drop-off time
    //             $s2 = 'SELECT
    //                     b.rpfrom,
    //                     b.rpto
    //                   FROM ticket_route_detail AS a
    //                   LEFT JOIN ticket_route_pickup_drop AS b ON b.rdid = a.rdid
    //                   WHERE a.rid = %d AND a.lcid = %d AND b.taid = %d';
    //             $q2 = $db->prepare_query( $s2, $d[ 'rid' ], $lcid, $d[ 'taid' ] );
    //             $r2 = $db->do_query( $q2 );
    //             $d2 = $db->fetch_array( $r2 );

    //             if( $d2[ 'rpfrom' ] != '' && $d2[ 'rpto' ] != '' )
    //             {
    //                 if( $d2[ 'rpfrom' ] != $d[ 'btrpfrom' ] || $d2[ 'rpto' ] != $d[ 'btrpto' ] )
    //                 {
    //                     $content .= '
    //                     <tr>
    //                         <td>' . $no . '</td>
    //                         <td>' . $d[ 'bticket' ] . '</td>
    //                         <td>' . get_route( $d[ 'rid' ], 'rname' ) . '</td>
    //                         <td>' . get_transport_area( $d[ 'taid' ], 'taname' ) . '</td>
    //                         <td>' . $d[ 'bdfrom' ] . '</td>
    //                         <td>' . $d[ 'bdto' ] . '</td>
    //                         <td>' . $d[ 'btrpfrom' ] . ' - ' . $d[ 'btrpto' ] . '</td>
    //                         <td>' . $d2[ 'rpfrom' ] . ' - ' . $d2[ 'rpto' ] . '</td>
    //                     </tr>';

                        //-- Update Booking Transport
                        // $r2 = $db->update( 'ticket_booking_transport', array(
                        //     'btrpfrom' => $d2[ 'rpfrom' ],
                        //     'btrpto'   => $d2[ 'rpto' ]
                        // ), array( 'btid' => $d[ 'btid' ] ) );

                        // if( is_array( $r2 ) )
                        // {
                        //     $retval = 0;
                        // }

    //                     $no++;
    //                 }
    //             }
    //         }

    //         $content .= '
    //     </table>';

    //     echo $content;
    // }


    // if( $retval == 0 )
    // {
    //     $db->rollback();

    //     echo '<p>failed</p>';
    // }
    // else
    // {
    //     $db->commit();

    //     echo '<p>success</p>';
    // }

    exit;
}
else
{
    $title   = 'Page Not Found';
    $ptitle  = 'Page Not Found - BlueWater Express';
    $content = '<p>The page you are looking for does not exist.<br />Return to the <a href="' . HTSERVER . site_url() . '">home page.</a></p>';

    set_message_page_content( $ptitle, $title, $content );
}

add_actions( 'include_js', 'get_custom_javascript', HTSERVER . TEMPLATE_URL . '/js/script.js?v=' . $version );

if( !is_front_detail_reservation() && !is_front_recheck_reservation() )
{
    add_block( 'main-block', 'm-block' );

    add_actions( 'include_js', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.12/ouibounce.min.js' );
}

add_variable( 'meta_description', attemp_actions( 'meta_description' ) );
add_variable( 'meta_keywords', attemp_actions( 'meta_keywords' ) );
add_variable( 'meta_title', attemp_actions( 'meta_title' ) );

add_variable( 'bootstrap_css', get_css_inc('bootstrap-3.3.7/css/bootstrap.min.css') );
add_variable( 'login_css', get_admin_css( 'login.css' ) );

add_variable( 'logout_link', get_agent_admin_url( 'logout' ) );
add_variable( 'forget_alert', post_agent_forget_password() );
add_variable( 'admin_img_url', get_admin_theme_img() );
add_variable( 'admin_url', get_admin_theme_url() );
add_variable( 'agent_url', get_agent_url() );
add_variable( 'alert', post_agent_login() );
add_variable( 'web_title', web_title() );

add_variable( 'site_url', site_url() );
add_variable( 'template_url', TEMPLATE_URL );
add_variable( 'http', HTSERVER );

add_variable( 'admin_tail', attemp_actions( 'admin_tail' ) );
add_variable( 'include_js', attemp_actions( 'include_js' ) );
add_variable( 'include_css', attemp_actions( 'include_css' ) );
add_variable( 'section_title', attemp_actions( 'section_title' ) );
add_variable( 'header_elements', attemp_actions( 'header_elements' ) );

add_variable( 'content_area', $thecontent );

parse_template( 'main-block', 'm-block' );

print_template();

?>
