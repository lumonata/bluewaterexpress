<?php

function get_admin_contents()
{
	add_actions( 'header_elements', 'get_css_inc', 'bootstrap-3.3.7/css/bootstrap.min.css' );
    add_actions( 'header_elements', 'get_css_inc', 'bootstrap-3.3.7/css/custom-theme/jquery-ui-1.10.0.custom.css' );
    add_actions( 'header_elements', 'get_css_inc', 'chosen/chosen.min.css' );
    add_actions( 'header_elements', 'get_admin_css' );

    add_actions( 'header_elements', 'get_javascript', 'jquery-3.2.1.min' );
    add_actions( 'header_elements', 'get_javascript_inc', 'chosen/chosen.jquery.min.js' );
    add_actions( 'header_elements', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
    add_actions( 'header_elements', 'get_javascript_inc', 'bootstrap-3.3.7/js/bootstrap.min.js' );
    add_actions( 'header_elements', 'get_javascript_inc', 'jquery-timepicker/jquery.timepicker.min.js' );
    add_actions( 'header_elements', 'get_javascript_inc', 'jquery-timepicker-addon/jquery-ui-timepicker-addon.min.js' );

    if( is_agent_logged() )
    {
        if( is_agent_dashboard() )
        {
            return agent_dashboard_content();
        }
        elseif( is_agent_reservation( 'new' ) )
        {
            return agent_new_reservation_content();
        }
        elseif( is_agent_reservation( 'availability' ) )
        {
            if( is_check_availability() )
            {
                return agent_check_availability_result();
            }
            elseif( is_review_booking() )
            {
                return agent_review_booking();
            }
            elseif( is_detail_booking() )
            {
                return agent_detail_booking();
            }
        }
        elseif( is_agent_reservation( 'booking' ) || is_agent_reservation( 'canceled' )  )
        {
            if( is_agent_view_booking() || is_agent_popup_detail_booking() )
            {
                return agent_reservation_show_detail( $_GET['id'] );
            }
            elseif( is_agent_edit_booking() || is_agent_popup_edit_booking() )
            {
                return agent_reservation_edit_detail( $_GET['id'] );
            }
            else
            {
                return agent_reservation_list_content();
            }
        }
        elseif( is_agent_profile_setting( 'profile' ) )
        {
            return agent_profile_content();
        }
        elseif( is_agent_availability() )
        {
            return agent_availability_content();
        }
        elseif( is_agent_profile_setting( 'sub-agent' ) )
        {
            header( 'location:' . get_agent_admin_url( 'dashboard' ) );

            if( is_num_sub_agent( $_COOKIE['agid'] ) == 0 && !isset( $_GET['prc'] ) )
            {
                header( 'location:' . get_agent_admin_url( 'profile-setting&sub=sub-agent&prc=add-new' ) );
            }

            if( is_agent_add_new() )
            {
                return sub_agent_add_new();
            }
            elseif( is_agent_edit() )
            {
                if( isset( $_GET['id'] ) && get_agent( $_GET['id'] ) )
                {
                    return sub_agent_edit();
                }
                else
                {
                    return not_found_template();
                }
            }
            elseif( is_agent_delete_all() )
            {
                return sub_agent_batch_delete();
            }
            elseif( is_agent_confirm_delete() )
            {
                foreach( $_POST['id'] as $key=>$val )
                {
                    delete_agent( $val );
                }
            }

            return sub_agent_list_content();
        }
        elseif( is_agent_outstanding_report() )
        {
            return agent_outstanding_report_content();
        }
        elseif( is_agent_promo_report() )
        {
            return agent_promo_report_content();
        }
        elseif( is_agent_admin_ajax() )
        {
            return agent_admin_ajax();
        }
    }
    else
    {
        header( 'location:' . get_agent_url() . '/login/' );
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Dashboard Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_dashboard_content()
{
    $site_url = site_url();
    $data     = get_agent( $_COOKIE['agid'] );

    set_template( TEMPLATE_PATH . '/template/dashboard.html', 'd-template' );
    add_block( 'dashboard-block', 'd-block', 'd-template' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'chid', $data['chid'] );
    add_variable( 'agcod', $data['agcod'] );

    add_variable( 'last_reservation', agent_last_reservation() );
    add_variable( 'destination_list', get_availibility_loc_option() );
    add_variable( 'from_list', get_availibility_loc_option( null, null, 'from' ) );
    add_variable( 'to_list', get_availibility_loc_option( null, null, 'to' ) );
    add_variable( 'reservation_link', get_agent_admin_url( 'reservation&sub=booking' ) );
    add_variable( 'remain_seat_panel', get_seat_availability_by_agent( $data['agid'] ) );

    add_variable( 'site_url', $site_url );
    add_variable( 'action', HTSERVER . $site_url .'/ticket-agent-booking-process' );

    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/slick.min.css' );

    parse_template( 'dashboard-block', 'd-block', false );

    return return_template( 'd-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent New Reservation Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_new_reservation_content()
{
    $site_url = site_url();
    $data     = get_agent( $_COOKIE['agid'] );

    set_template( TEMPLATE_PATH . '/template/reservation/form.html', 'fm-template' );
    add_block( 'form-block', 'fm-block', 'fm-template' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'chid', $data['chid'] );
    add_variable( 'agcod', $data['agcod'] );

    add_variable( 'from_list', get_availibility_loc_option( null,null, 'from' ) );
    add_variable( 'to_list', get_availibility_loc_option( null, null, 'to' ) );
    add_variable( 'return_from_list', get_availibility_loc_option( null, null, 'to' ) );
    add_variable( 'return_to_list', get_availibility_loc_option( null, null, 'from' ) );
    
    add_variable( 'site_url', $site_url );
    add_variable( 'ajax_link', HTSERVER. $site_url .'/ticket-availability-ajax/' );
    add_variable( 'action', HTSERVER. $site_url .'/ticket-agent-booking-process' );

    parse_template( 'form-block', 'fm-block', false );

    return return_template( 'fm-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Check Availability Result
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_check_availability_result()
{
    $site_url = site_url();
    $booking  = booking_item();

    //-- Redirect If empty booking
    if( !isset( $booking['sess_id'] ) || !isset( $booking['sess_data'] ) )
    {
        header( 'location:' . get_agent_admin_url( 'reservation&sub=availability' ) );
    }

    extract( $booking['sess_data'] );

    //-- Redirect If date of depart greater than current date
    if( strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
    {
        header( 'location:' . get_agent_admin_url( 'reservation&sub=availability' ) );
    }

    set_template( TEMPLATE_PATH . '/template/availability/search-result.html', 'availability' );
    add_block( 'search-result-block', 'srblock', 'availability' );

    add_variable( 'sess_id', $booking['sess_id'] );

    if( empty( $date_of_return ) )
    {
        add_variable( 'return_css', 'sr-only' );
    }
    else
    {
        add_variable( 'return_css', '' );
        add_variable( 'return_port', $return_port );
        add_variable( 'return_date', $date_of_return );
        add_variable( 'destination_port_rtn', $destination_port_rtn );
        add_variable( 'return_date_field', date( 'd F Y', strtotime( $date_of_return ) ) );
        add_variable( 'return_date_format', date( 'D j F Y', strtotime( $date_of_return ) ) );
        add_variable( 'return_availability_result', get_return_availability_result( $booking['sess_data'], $agid, true ) );
        add_variable( 'return_route', get_location( $return_port, 'lcname' ) . ' to ' . get_location( $destination_port_rtn, 'lcname' ) );
    }

    if( empty( $type_of_route ) )
    {
        add_variable( 'one_way_css', 'checked' );
        add_variable( 'return_way_css', '' );

        add_variable( 'return_date_css', 'disabled');
        add_variable( 'return_port_css', 'disabled' );
        add_variable( 'destination_to_port_css', 'disabled' );
    }
    else
    {
        add_variable( 'one_way_css', '' );
        add_variable( 'return_way_css', 'checked' );

        add_variable( 'return_date_css', '');
        add_variable( 'return_port_css', '' );
        add_variable( 'destination_to_port_css', '' );
    }

    add_variable( 'depart_port', $depart_port );
    add_variable( 'depart_date', $date_of_depart );
    add_variable( 'depart_date_field', date( 'd F Y', strtotime( $date_of_depart ) ) );
    add_variable( 'depart_date_format', date( 'D j F Y', strtotime( $date_of_depart ) ) );
    add_variable( 'departure_availability_result', get_departure_availability_result( $booking['sess_data'], $agid, true ) );
    add_variable( 'depart_route', get_location( $depart_port, 'lcname' ) . ' to ' . get_location( $destination_port, 'lcname' ) );

    add_variable( 'chid', $chid );
    add_variable( 'agid', $agid );
    add_variable( 'adult_num', $adult_num );
    add_variable( 'child_num', $child_num );
    add_variable( 'infant_num', $infant_num );
    add_variable( 'type_of_route', $type_of_route );
    add_variable( 'destination_port', $destination_port );

    add_variable( 'depart_port_list', get_availibility_loc_option( 'From', $depart_port, 'from' ) );
    add_variable( 'destination_port_list', get_availibility_loc_option( 'To', $destination_port, 'to' ) );
    add_variable( 'return_port_list', get_availibility_loc_option( 'Return From', $return_port, 'from' ) );
    add_variable( 'destination_to_port_list', get_availibility_loc_option( 'Return To', $destination_port_rtn, 'to' ) );

    add_variable( 'action', HTSERVER . $site_url .'/ticket-agent-booking-process' );

    parse_template( 'search-result-block', 'srblock', false );

    add_actions( 'section_title', 'Search Availability Result' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'availability' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Review Booking
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_review_booking()
{
    $site_url = site_url();
    $booking  = booking_item();

    //-- Redirect If empty booking
    if( !isset( $booking['sess_id'] ) || !isset( $booking['sess_data'] ) )
    {
        header( 'location:' . get_agent_admin_url( 'reservation&sub=availability&step=search-result' ) );
    }

    extract( $booking['sess_data'] );

    //-- Redirect If empty trip
    if( empty( $trip ) )
    {
        header( 'location:' . get_agent_admin_url( 'reservation&sub=availability&step=search-result' ) );
    }

    //-- Redirect If date of depart greater than current date
    if( strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
    {
        header( 'location:' . get_agent_admin_url( 'reservation&sub=availability' ) );
    }

    set_template( TEMPLATE_PATH . '/template/availability/review.html', 'availability' );
    add_block( 'review-block', 'rvblock', 'availability' );

    add_variable( 'sess_id', $booking['sess_id'] );

    //-- Check if have error data
    if( isset( $_GET['error'] ) )
    {
        add_variable( 'message', generate_message_block( json_decode( base64_decode( $_GET['error'] ), true ) ) );
    }

    $bsource = empty( $agid ) ? $chid : $chid . '|' . $agid;

    if( isset( $trip['return'] ) )
    {
        add_variable( 'return_css', '' );
        add_variable( 'return_port', $return_port );
        add_variable( 'return_date', $date_of_return );
        add_variable( 'destination_port_rtn', $destination_port_rtn );
        add_variable( 'return_date_format', date( 'D j F Y', strtotime( $date_of_return ) ) );
        add_variable( 'return_route', get_location( $return_port, 'lcname' ) . ' to ' . get_location( $destination_port_rtn, 'lcname' ) );

        add_variable( 'return_rid', $trip['return']['id'] );
        add_variable( 'return_boid', $trip['return']['boid'] );
        add_variable( 'return_price', $trip['return']['total'] );
        add_variable( 'return_boat_name', get_boat( $trip['return']['boid'], 'boname' ) );

        if( $trip['return']['discount'] > 0 )
        {
            add_variable( 'return_price_disc_num', '<span class="disc-price">' . number_format( $trip['return']['total'], 0, ',', '.' ) . '</span>' );
            add_variable( 'return_price_num', number_format( ( $trip['return']['total'] - $trip['return']['discount'] ), 0, ',', '.' ) );
        }
        else
        {
            add_variable( 'return_price_num', number_format( $trip['return']['total'], 0, ',', '.' ) );
        }

        add_variable( 'return_trans', get_route_detail_transport( $trip['return']['transport']  ) );
        add_variable( 'return_passenger_number', get_passenger_num_content( $adult_num, $child_num, $infant_num ) );
        add_variable( 'return_route_list', get_route_detail_list_content( $trip['return']['id'], $return_port, $destination_port_rtn ) );
        add_variable( 'return_rate_price', get_agent_rate_price_content( $trip['return']['id'], $adult_num, $child_num, $infant_num, $trip['return'], true ) );
        add_variable( 'return_add_ons', get_agent_add_ons_content( $date_of_return, 'return', $trip['return']['sid'], $bsource, $return_port, $destination_port_rtn ) );
        add_variable( 'return_dropoff_transport', get_agent_pickup_drop_off_transport( $trip['return']['id'], $return_port, $destination_port_rtn, $trip['return']['transport'], $booking['sess_data'], true ) );
    }
    else
    {
        add_variable( 'return_css', 'sr-only' );
    }

    add_variable( 'depart_port', $depart_port );
    add_variable( 'depart_date', $date_of_depart );
    add_variable( 'depart_date_format', date( 'D j F Y', strtotime( $date_of_depart ) ) );
    add_variable( 'depart_route', get_location( $depart_port, 'lcname' ) . ' to ' . get_location( $destination_port, 'lcname' ) );

    add_variable( 'depart_rid', $trip['departure']['id'] );
    add_variable( 'depart_boid', $trip['departure']['boid'] );
    add_variable( 'depart_price', $trip['departure']['total'] );
    add_variable( 'depart_boat_name', get_boat( $trip['departure']['boid'], 'boname' ) );

    if( $trip['departure']['discount'] > 0 )
    {
        add_variable( 'depart_price_disc_num', '<span class="disc-price">' . number_format( $trip['departure']['total'], 0, ',', '.' ) . '</span>' );
        add_variable( 'depart_price_num', number_format( ( $trip['departure']['total'] - $trip['departure']['discount'] ), 0, ',', '.' ) );
    }
    else
    {
        add_variable( 'depart_price_num', number_format( $trip['departure']['total'], 0, ',', '.' ) );
    }

    add_variable( 'depart_trans', get_route_detail_transport( $trip['departure']['transport'] ) );
    add_variable( 'depart_passenger_number', get_passenger_num_content( $adult_num, $child_num, $infant_num ) );
    add_variable( 'depart_route_list', get_route_detail_list_content( $trip['departure']['id'], $depart_port, $destination_port ) );
    add_variable( 'depart_rate_price', get_agent_rate_price_content( $trip['departure']['id'], $adult_num, $child_num, $infant_num, $trip['departure'] ) );
    add_variable( 'depart_add_ons', get_agent_add_ons_content( $date_of_depart, 'departure', $trip['departure']['sid'], $bsource, $depart_port, $destination_port ) );
    add_variable( 'depart_pickup_transport', get_agent_pickup_drop_off_transport( $trip['departure']['id'], $depart_port, $destination_port, $trip['departure']['transport'], $booking['sess_data'] ) );

    add_variable( 'discount_css', empty( $promo_code ) ? 'sr-only' : '' );
    add_variable( 'freelance_code_css', empty( $freelance_code ) ? 'sr-only' : '' );
    add_variable( 'promo_code', empty( $promo_code ) ? '-' : 'Promo Code : ' . $promo_code );
    add_variable( 'discount_price_num', empty( $discount ) ? '' : number_format( $discount, 0, ',', '.' ) );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'type_of_route', $type_of_route );
    add_variable( 'destination_port', $destination_port );
    add_variable( 'subtotal_num', number_format ( $subtotal, 0, ',', '.' ) );
    add_variable( 'grand_total_price', number_format ( $grandtotal, 0, ',', '.' ) );

    add_variable( 'action', HTSERVER . $site_url .'/ticket-agent-booking-process' );
    add_variable( 'ajax_link', HTSERVER. $site_url .'/ticket-availability-ajax/' );

    add_actions( 'section_title', 'Review Booking' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
    add_actions( 'header_elements', 'get_css_inc', 'jquery-timepicker-addon/jquery-ui-timepicker-addon.min.css' );
    add_actions( 'header_elements', 'get_custom_css', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' );

    add_actions( 'header_elements', 'get_javascript_inc', 'autonumeric-1.9.46/autoNumeric-min.js' );
    add_actions( 'header_elements', 'get_javascript_inc', 'jquery-timepicker-addon/jquery-ui-timepicker-addon.min.js' );
    add_actions( 'header_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' );

    parse_template( 'review-block', 'rvblock', false );

    return return_template( 'availability' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Detail Booking
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_detail_booking()
{
    $site_url = site_url();
    $booking  = booking_item();

    //-- Redirect If empty session booking
    if( !isset( $booking['sess_id'] ) || !isset( $booking['sess_data'] ) )
    {
        header( 'location:' . get_agent_admin_url( 'reservation&sub=availability&step=review' ) );
    }

    extract( $booking['sess_data'] );

    //-- Redirect If empty trip
    if( empty( $trip ) )
    {
        header( 'location:' . get_agent_admin_url( 'reservation&sub=availability&step=search-result' ) );
    }

    //-- Redirect If date of depart greater than current date
    if( strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
    {
        header( 'location:' . get_agent_admin_url( 'reservation&sub=availability' ) );
    }

    set_template( TEMPLATE_PATH . '/template/availability/detail.html', 'availability' );
    add_block( 'detail-block', 'dtblock', 'availability' );

    add_variable( 'sess_id', $booking['sess_id'] );

    if( isset( $trip['return'] ) )
    {
        $rd  = get_route_detail_content( $trip['return']['id'], $return_port, $destination_port_rtn );

        add_variable( 'return_to_port', $rd['arrive'] );
        add_variable( 'return_etd', $rd['depart_time'] );
        add_variable( 'return_eta', $rd['arrive_time'] );
        add_variable( 'return_from_port', $rd['depart'] );
        add_variable( 'return_boat_name', get_boat( $trip['return']['boid'], 'boname' ) );
        add_variable( 'return_date_format', date( 'D j F Y', strtotime( $date_of_return ) ) );
        add_variable( 'return_transport', get_transport_data( $trip['return'], $trip['return']['transport'] ) );
        add_variable( 'return_passenger', get_passenger_num_detail_content( $adult_num, $child_num, $infant_num ) );
        add_variable( 'return_passenger_field_list', passenger_field_list_content( $adult_num, $child_num, $infant_num, true ) );
        add_variable( 'return_transport_detail', get_return_transport_detail( $trip['return'], $trip['return']['transport'], true ) );
    }
    else
    {
        add_variable( 'return_css', 'sr-only' );
    }

    if( empty( $agid ) )
    {
        add_variable( 'phone', '' );
        add_variable( 'email', '' );
        add_variable( 'full_name', '' );
        add_variable( 'payment', 4 );
    }
    else
    {
        $agent = get_agent( $agid );

        add_variable( 'phone', $agent['agphone'] );
        add_variable( 'email', $agent['agemail'] );
        add_variable( 'full_name', $agent['agname'] );
        add_variable( 'payment', $agent['agpayment_type'] == 'Credit' ? 8 : 4 );

        add_variable( 'agid', $agent['agid'] );
        add_variable( 'agname', $agent['agname'] );
        add_variable( 'agemail', empty( $agent['agemail'] ) ? '-' : $agent['agemail'] );
        add_variable( 'agphone', empty( $agent['agphone'] ) ? $agent['agmobile'] : $agent['agphone'] );
        add_variable( 'agaddress', empty( $agent['agaddress'] ) ? '' : nl2br( $agent['agaddress'] ) );
        add_variable( 'sub_agent_option', get_sub_agent_option( $agent['agid'] ) );
    }

    $dd  = get_route_detail_content( $trip['departure']['id'], $depart_port, $destination_port );
    $var = get_variable_by_port_type( $depart_port, $destination_port );
    $aid = $var['drop_sts'] ? $dd['depart'] : $dd['arrive'];

    add_variable( 'depart_to_port', $dd['arrive'] );
    add_variable( 'depart_etd', $dd['depart_time'] );
    add_variable( 'depart_eta', $dd['arrive_time'] );
    add_variable( 'depart_from_port', $dd['depart'] );
    add_variable( 'depart_boat_name', get_boat( $trip['departure']['boid'], 'boname' ) );
    add_variable( 'depart_date_format', date( 'D j F Y', strtotime( $date_of_depart ) ) );
    add_variable( 'd_date',  date( 'Y-m-d', strtotime( $date_of_depart ) ) );
    add_variable( 'depart_transport', get_transport_data( $trip['departure'], $trip['departure']['transport'] ) );
    add_variable( 'depart_passenger', get_passenger_num_detail_content( $adult_num, $child_num, $infant_num ) );
    add_variable( 'depart_passenger_field_list', passenger_field_list_content( $adult_num, $child_num, $infant_num ) );
    add_variable( 'depart_transport_detail', get_depart_transport_detail( $trip['departure'], $trip['departure']['transport'], true ) );

    add_variable( 'country_list_option', get_country_list_option() );
    add_variable( 'grand_total_price', number_format ( $grandtotal, 0, ',', '.' ) );

    add_variable( 'suggestion_accommodation_area', $aid );
    add_variable( 'suggestion_accommodation', get_suggestion_accommodation( $aid, $trip['departure'] ) );
    add_variable( 'action', HTSERVER . $site_url .'/ticket-agent-booking-process' );

    parse_template( 'detail-block', 'dtblock', false );

    add_actions( 'section_title', 'Detail Booking' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'availability' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Reservation List Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_reservation_list_content()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_agent_admin_url( 'reservation&sub=' . $_GET['sub'] ) . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    $filter = get_agent_filter_booking();

    extract( $filter );

    set_template( TEMPLATE_PATH . '/template/reservation/list.html', 'ls-template' );

    add_block( 'list-block', 'ls-block', 'ls-template' );

    add_variable( 'search', $search );
    add_variable( 'sub', $_GET['sub'] );
    add_variable( 'date_end', $date_end );
    add_variable( 'date_start', $date_start );

    add_variable( 'site_url', site_url() );
    add_variable( 'limit', post_viewed() );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'ajax_url', get_agent_admin_url( 'ajax-request' ) );
    add_variable( 'action', get_agent_admin_url( 'reservation&sub=' . $_GET['sub'] ) );

    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'location_option', get_location_option( $lcid, true, 'All Departure', 'from') );
    add_variable( 'location2_option', get_location_option( $lcid2, true, 'All Arrival Point', 'to') );
    add_variable( 'chanel_option', get_channel_option( $chid, true, 'All Sales Channel') );
    add_variable( 'status_option', get_booking_status_option( $status, true, 'All Status') );

    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'canceled' )
    {
        add_variable( 'status_option_css', 'sr-only' );
        add_variable( 'page_title', 'Cancelation' );
    }
    else
    {
        add_variable( 'status_option_css', 'select-option' );
        add_variable( 'page_title', 'Reservation' );
    }

    add_actions( 'header_elements', 'get_css_inc', 'fancybox/dist/jquery.fancybox.min.css' );
    add_actions( 'header_elements', 'get_javascript', 'jquery.base64.min' );
    add_actions( 'header_elements', 'get_javascript_inc', 'fancybox/dist/jquery.fancybox.min.js' );
    add_actions( 'header_elements', 'get_javascript_inc', 'datatables-1.10.13/media/js/jquery.dataTables.min.js' );
    add_actions( 'header_elements', 'get_javascript_inc', 'datatables-1.10.13/media/js/dataTables.bootstrap.min.js' );

    parse_template( 'list-block', 'ls-block', false );

    return return_template( 'ls-template' );
}

/*
| -------------------------------------------------------------------------------------
| Agent Reservation Table Query
| -------------------------------------------------------------------------------------
*/
function agent_reservation_table_query( $chid = '', $lcid = '', $lcid2 = '', $rid = '', $date_start = '', $date_end = '', $status = '', $search = '', $sub = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        0  => 'a.bdate',
        1  => 'a.bbrevagent',
        4  => 'a.bbooking_staf',
        5  => 'c.bddate',
        7  => 'c.bddeparttime',
        8  => 'c.bdarrivetime',
        10 => 'a.bbrevstatus'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $o[] = 'a.bid DESC';

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.bdate DESC, a.bid DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $w = '';

        if( $chid != '' )
        {
            $w .= $db->prepare_query( ' AND a.chid = %d', $chid );
        }

        if( $lcid != '' )
        {
            $w .= $db->prepare_query( ' AND c.bdfrom = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid );
        }

        if( $lcid2 != '' )
        {
            $w .= $db->prepare_query( ' AND c.bdto = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid2 );
        }

        if( $rid != '' )
        {
            $w .= $db->prepare_query( ' AND c.rid = %d', $rid );
        }

        if( $date_start != '' && $date_end != '' )
        {
            $w .= $db->prepare_query( ' AND c.bddate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $date_start ) ), date( 'Y-m-d', strtotime( $date_end ) ) );
        }

        if( $sub == 'canceled' )
        {
            $w .= $db->prepare_query( ' AND a.bstatus = %s', 'cr' );
        }
        else
        {
            if( !empty( $status ) )
            {
                if( $status == 'pp' )
                {
                    $w .= $db->prepare_query( ' AND a.bstatus IN( %s, %s )', 'pp', 'ol' );
                }
                else
                {
                    $w .= $db->prepare_query( ' AND a.bstatus = %s', $status );
                }
            }
            else
            {
                $w .= $db->prepare_query( ' AND a.bstatus NOT IN( %s, %s )', 'ol', 'pf' );
            }
        }

        $s = 'SELECT
                a.bid,
                a.bcode,
                a.agid,
                a.sagid,
                a.bticket,
                b.chcode,
                b.chname,
                a.bbname,
                a.bdate,
                a.bstatus,
                c.bdid,
                c.bdfrom,
                c.bdto,
                c.bddate,
                c.bdtype,
                c.bddeparttime,
                c.bdarrivetime,
                a.bbrevtype,
                a.bbrevstatus,
                a.bbrevagent,
                a.bcreason,
                a.bcmessage,
                a.bbphone,
                a.brcdate,
                a.bonhandtotal,
                a.btotal,
                a.bbooking_staf,
                a.bbemail,
                a.bagremark,
                c.total,
                d.agname,
                c.num_adult,
                c.num_child,
                c.num_infant,
                ( c.num_adult + c.num_child + c.num_infant ) AS num_pax,
                (
                    SELECT GROUP_CONCAT( CONVERT( bpname USING utf8 ) SEPARATOR "<br/><br/>" )
                    FROM ticket_booking_passenger AS a2
                    WHERE a2.bdid = c.bdid
                    ORDER BY a2.bpid ASC
                ) AS pass_name
              FROM ticket_booking AS a
              LEFT JOIN ticket_channel AS b ON a.chid = b.chid
              LEFT JOIN ticket_booking_detail AS c ON c.bid = a.bid
              LEFT JOIN ticket_agent AS d ON d.agid = a.agid
              WHERE a.bstt <> "ar" AND a.agid = ' . $_COOKIE['agid'] . ' ' . $w . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $bsearch = array();
        $cols[0] = 'a.bticket';
        $cols[2] = '(
            SELECT GROUP_CONCAT( CONVERT( bpname USING utf8 ) SEPARATOR "<br/><br/>" )
            FROM ticket_booking_passenger AS a2
            WHERE a2.bdid = c.bdid
            ORDER BY a2.bpid ASC
        )';

        foreach( $cols as $col )
        {
            $bsearch[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                a.bid,
                a.bcode,
                a.agid,
                a.sagid,
                a.bticket,
                b.chcode,
                b.chname,
                a.bbname,
                a.bdate,
                a.bstatus,
                c.bdid,
                c.bdfrom,
                c.bdto,
                c.bddate,
                c.bdtype,
                c.bddeparttime,
                c.bdarrivetime,
                a.bbrevtype,
                a.bbrevstatus,
                a.bbrevagent,
                a.bcreason,
                a.bcmessage,
                a.bbphone,
                a.brcdate,
                a.bonhandtotal,
                a.btotal,
                a.bbooking_staf,
                a.bbemail,
                a.bagremark,
                c.total,
                d.agname,
                c.num_adult,
                c.num_child,
                c.num_infant,
                ( c.num_adult + c.num_child + c.num_infant ) AS num_pax,
                (
                    SELECT GROUP_CONCAT( CONVERT( bpname USING utf8 ) SEPARATOR "<br/><br/>" )
                    FROM ticket_booking_passenger AS a2
                    WHERE a2.bdid = c.bdid
                    ORDER BY a2.bpid ASC
                ) AS pass_name
              FROM ticket_booking AS a
              LEFT JOIN ticket_channel AS b ON a.chid = b.chid
              LEFT JOIN ticket_booking_detail AS c ON c.bid = a.bid
              LEFT JOIN ticket_agent AS d ON d.agid = a.agid
              WHERE a.bstt <> "ar" AND a.agid = ' . $_COOKIE['agid'] . ' AND ( ' . implode( ' OR ', $bsearch ) . ' ) ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data   = array();
    $filter = array( 'chid' => $chid, 'lcid' => $lcid, 'lcid2' => $lcid2, 'rid' => $rid, 'date_start' => $date_start, 'date_end' => $date_end, 'bstatus' => $status, 'sbooking' => $search, 'sub' => $sub );

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $cnccss    = $d2['bstatus'] == 'cn' ? 'sr-only' : '-';
            $trans     = ticket_booking_transport_detail( $d2['bdid'] );
            $brcdate   = $d2['brcdate'] == '0000-00-00' ? '' : date( 'd M Y', strtotime( $d2['brcdate'] ) );
            $pass_name = $d2['pass_name'] . '<br /><br />' . 'Pax : ' . $d2['num_pax'] . ' | Adult : ' . $d2['num_adult'] . ' | Child : ' . $d2['num_child'] . ' | Infant : ' . $d2['num_infant'] . '<span class="sr-only">' . $d2['num_pax'] . '|' .  $d2['num_adult'] . '|' . $d2['num_child'] . '|' . $d2['num_infant'] . '</span>';

            $data[] = array(
                'transport'   => $trans,
                'cancel_css'  => $cnccss,
                'brcdate'     => $brcdate,
                'guest_name'  => $pass_name,
                'id'          => $d2['bid'],
                'bdid'        => $d2['bdid'],
                'agid'        => $d2['agid'],
                'ref_code'    => $d2['bcode'],
                'phone'       => $d2['bbphone'],
                'no_ticket'   => $d2['bticket'],
                'bstatus'     => $d2['bstatus'],
                'bbemail'     => $d2['bbemail'],
                'bcreason'    => $d2['bcreason'],
                'bcmessage'   => $d2['bcmessage'],
                'rsv_type'    => $d2['bbrevtype'],
                'rsv_status'  => $d2['bbrevstatus'],
                'ref_agent'   => $d2['bbrevagent'],
                'bonhandtotal'=> $d2['bonhandtotal'],
                'booked_by'   => $d2['bbooking_staf'],
                'remarks'     => $d2['bagremark'],
                'route'       => $d2['bdfrom'] . ' - ' . $d2['bdto'],
                'status'      => ticket_booking_status( $d2['bstatus'] ),
                'total'       => number_format( $d2['total'], 0, ',', '.' ),
                'btotal'      => number_format( $d2['btotal'], 0, ',', '.' ),
                'rev_date'    => date( 'd M Y', strtotime( $d2['bdate'] ) ),
                'dept_date'   => date( 'd M Y', strtotime( $d2['bddate'] ) ),
                'dept_time'   => date( 'H:i', strtotime( $d2['bddeparttime'] ) ),
                'arrive_time' => date( 'H:i', strtotime( $d2['bdarrivetime'] ) ),
                'chanel'      => empty( $d2['agid'] ) ? $d2['chname'] : $d2['agname'],
                'edit_link'   => get_agent_admin_url( 'reservation&sub=booking&prc=edit&id=' . $d2['bid'] . '&filter=' . base64_encode( json_encode( $filter ) ) ),
                'detail_link' => get_agent_admin_url( 'reservation&sub=booking&prc=popup-detail&id=' . $d2['bid'] . '&filter=' . base64_encode( json_encode( $filter ) ) )
            );
        }
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Last Reservation List Table
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_last_reservation()
{
    global $db;

    $s = 'SELECT
            a.bid,
            a.bticket,
            b.chcode,
            a.bbname,
            a.bdate,
            a.bstatus,
            c.bdid,
            c.bdfrom,
            c.bddate,
            c.bdto,
            c.bddeparttime,
            d.lcid,
            e.agname
          FROM ticket_booking AS a
          LEFT JOIN ticket_channel AS b ON a.chid = b.chid
          LEFT JOIN ticket_booking_detail AS c ON c.bid = a.bid
          LEFT JOIN ticket_location AS d ON c.bdfrom = d.lcname
          LEFT JOIN ticket_agent AS e ON a.sagid = e.agid
          WHERE a.bstt <> %s AND a.agid = %d AND c.bdtype = %s AND a.bstatus NOT IN ( %s, %s )
          ORDER BY a.bid DESC LIMIT 5';
    $q = $db->prepare_query( $s, 'ar', $_COOKIE['agid'], 'departure', 'cr', 'cn' );
    $r = $db->do_query( $q );

    set_template( TEMPLATE_PATH . '/template/reservation/table.html', 'tb-template' );

    add_block( 'table-block', 'tb-block', 'tb-template' );

    add_variable( 'list', agent_reservation_list_data( $r ) );

    add_actions( 'header_elements', 'get_css_inc', 'fancybox/dist/jquery.fancybox.min.css' );
    add_actions( 'header_elements', 'get_javascript_inc', 'fancybox/dist/jquery.fancybox.min.js' );

    parse_template( 'table-block','tb-block', false );

    return return_template( 'tb-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Reservation List Table Data
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_reservation_list_data( $result, $i = 1 )
{
    global $db;

    $list = '';

    if( $db->num_rows( $result ) == 0 )
    {
        if( isset( $_POST['s'] ) && !empty( $_POST['s'] ) )
        {
            return '<tr><td colspan="9"><p class="text-center text-danger">No result found for <em>"' . $_POST['s'] . '"</em>. Check your spellling or try another terms</p></td></tr>';
        }
        else
        {
            return '<tr><td colspan="9"><p class="text-center text-danger">No data found</p></td></tr>';
        }
    }

    set_template( TEMPLATE_PATH . '/template/reservation/loop.html', 'lp-template' );

    add_block( 'loop-block', 'lp-loop', 'lp-template' );

    while( $d = $db->fetch_array( $result ) )
    {
        $pass = ticket_passenger_list( $d['bdid'] );

        add_variable( 'id', $d['bid'] );
        add_variable( 'chanel', $d['chcode'] );
        add_variable( 'no_ticket', $d['bticket'] );
        add_variable( 'dept_time', $d['bddeparttime'] );
        add_variable( 'route', $d['bdfrom'] . ' - ' . $d['bdto'] );
        add_variable( 'rev_date', date('d M Y', strtotime( $d['bdate'] ) ) );
        add_variable( 'dept_date', date( 'd M Y', strtotime( $d['bddate'] ) ) );
        add_variable( 'booked_by', empty( $d['agname'] ) ? $_COOKIE['agname'] : $d['agname'] );

        add_variable( 'status', ticket_booking_status( $d['bstatus'] ) );
        add_variable( 'cancel_css', $d['bstatus'] == 'cn' ? 'sr-only' : '' );
        add_variable( 'guest_name', ticket_passenger_list_name( $pass ) );

        add_variable( 'ajax_url', get_agent_admin_url( 'ajax-request' ) );
        add_variable( 'detail_link', get_agent_admin_url( 'reservation&sub=booking&prc=popup-detail&id=' . $d['bid'] ) );
        add_variable( 'edit_link', get_agent_admin_url( 'reservation&sub=booking&prc=popup-edit&id=' . $d['bid'] ) );
        add_variable( 'action', get_agent_admin_url( 'reservation&sub=booking' ) );

        parse_template('loop-block', 'lp-loop', true);
    }

    return return_template('lp-template');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Reservation Detail
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_reservation_show_detail( $bid = '' )
{
    $message = '';
    $data    = ticket_booking_all_data( $bid  );

    if( !empty( $data ) )
    {
        extract( $data['detail'] );

        $site_url        = site_url();
        $bstatus_message = ticket_booking_status( $data['bstatus'] );

        set_template( TEMPLATE_PATH . '/template/reservation/detail.html', 'rv-detail' );

        add_block( 'detail-departure-loop-block', 'dtdl-block', 'rv-detail' );
        add_block( 'detail-departure-block', 'dtd-block', 'rv-detail' );
        add_block( 'detail-return-loop-block', 'dtrl-block', 'rv-detail' );
        add_block( 'detail-return-block', 'dtr-block', 'rv-detail' );
        add_block( 'detail-block', 'dt-block', 'rv-detail' );

        add_variable( 'prc', $_GET['prc'] );
        add_variable( 'site_url', $site_url );
        add_variable( 'bcode', $data['bcode'] );
        add_variable( 'booking_id', $data['bid'] );
        add_variable( 'bstatus', $data['bstatus'] );
        add_variable( 'bticket', $data['bticket'] );
        add_variable( 'bcreason', $data['bcreason'] );
        add_variable( 'bcmessage', $data['bcmessage'] );
        add_variable( 'bstatus_message', $bstatus_message );
        add_variable( 'bpaymethod', get_payment_method_by_id( $data['bpaymethod'] ) );
        add_variable( 'cancelation_css', in_array( $data['bstatus'], array( 'cr', 'cn' ) ) ? '' : 'sr-only' );

        add_variable( 'brefund', number_format( $data['brefund'], 0, ',', '.' ) );
        add_variable( 'btransactionfee', number_format( $data['btransactionfee'], 0, ',', '.' ) );
        add_variable( 'bcancellationfee', number_format( $data['bcancellationfee'], 0, ',', '.' ) );

        if( $data['bcdate'] != '0000-00-00' )
        {
            add_variable( 'cancelation_date_css', '' );
            add_variable( 'bcdate', date( 'd F Y', strtotime( $data['bcdate'] ) ) );
        }
        else
        {
            add_variable( 'cancelation_date_css', 'sr-only' );
            add_variable( 'bcdate', '' );
        }

        if( $data['brcdate'] != '0000-00-00' )
        {
            add_variable( 'req_cancelation_date_css', '' );
            add_variable( 'brcdate', date( 'd F Y', strtotime( $data['brcdate'] ) ) );
        }
        else
        {
            add_variable( 'req_cancelation_date_css', 'sr-only' );
            add_variable( 'brcdate', '' );
        }

        add_variable( 'bbcountry', ticket_passenger_country( $data['bbcountry'] ) );
        add_variable( 'bbooking_staf', empty( $data['bbooking_staf'] ) ? '-' : $data['bbooking_staf'] );
        add_variable( 'bblockingtime', empty( $data['bblockingtime'] ) ? '-' : date( 'd F Y H:i', $data['bblockingtime'] ) );

        add_variable( 'bbname', $data['bbname'] );
        add_variable( 'bbemail', $data['bbemail'] );
        add_variable( 'bbphone', $data['bbphone'] );

        add_variable( 'bagremark', $data['bagremark'] );
        add_variable( 'bhotelname', $data['bhotelname'] );
        add_variable( 'bremarkcss', empty( $data['bagremark'] ) ? 'sr-only' : '' );
        add_variable( 'baccommodationcss', empty( $data['bhotelname'] ) ? 'sr-only' : '' );
        add_variable( 'bhoteladdress', empty( $data['bhoteladdress'] ) ? '' : $data['bhoteladdress'] . '<br />' );
        add_variable( 'bhotelphone', empty( $data['bhotelphone'] ) ? '' : 'P. ' . $data['bhotelphone'] . '<br />' );
        add_variable( 'bhotelemail', empty( $data['bhotelemail'] ) ? '' : 'E. ' . $data['bhotelemail'] . '<br />' );

        add_variable( 'bbrevtype', empty( $data['bbrevtype'] ) ? '-' : $data['bbrevtype'] );
        add_variable( 'bbrevagent', empty( $data['bbrevagent'] ) ? '-' : $data['bbrevagent'] );
        add_variable( 'bbrevstatus', empty( $data['bbrevstatus'] ) ? '-' : $data['bbrevstatus'] );

        add_variable( 'btype', ticket_booking_type( $data['btype'] ) );
        add_variable( 'bdate', date( 'd M Y', strtotime( $data['bdate'] ) ) );
        add_variable( 'class_booking_status', generateSefUrl( $bstatus_message ) );

        if( empty( $data['agid'] ) )
        {
            add_variable( 'bchannel', $data['chname'] );
        }
        else
        {
            $agid = empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'];

            add_variable( 'bchannel', get_agent( $agid, 'agname' ) );
        }

        //-- Go Trip
        if( isset( $departure ) )
        {
            foreach( $departure as $key => $dp_trip )
            {
                extract( $dp_trip );

                $int = get_diff_date( $bddate . ' ' . $bddeparttime, date( 'Y-m-d H:i:s' ) );
                $var = get_variable_by_port_type( $bdfrom_id, $bdto_id );

                $dep_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                $dep_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                $dep_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                $dep_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';

                add_variable( 'dep_bdto', $bdto );
                add_variable( 'dep_bdfrom', $bdfrom );
                add_variable( 'dep_num_adult', $num_adult );
                add_variable( 'dep_num_child', $num_child );
                add_variable( 'dep_num_infant', $num_infant );
                add_variable( 'dep_bddeparttime', $bddeparttime );
                add_variable( 'dep_bdarrivetime', $bdarrivetime );

                add_variable( 'dep_discount', $discount );
                add_variable( 'dep_price_per_adult', $price_per_adult );
                add_variable( 'dep_price_per_child', $price_per_child );
                add_variable( 'dep_price_per_infant', $price_per_infant );

                add_variable( 'dep_pa_display', $dep_pa_display );
                add_variable( 'dep_pc_display', $dep_pc_display );
                add_variable( 'dep_pi_display', $dep_pi_display );
                add_variable( 'dep_pt_display', $dep_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );
                add_variable( 'dep_passenger', ticket_passenger_list_content( $passenger ) );

                add_variable( 'dep_bdtype', ucfirst( $bdtype ) );
                add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'dep_add_ons', ticket_detail_add_ons_content( $dp_trip ) );
                add_variable( 'dep_transport_css', empty( $transport ) ? 'sr-only' : '' );
                add_variable( 'dep_transport', ticket_detail_transport_content( $transport, $bdfrom, $bdto ) );

                add_variable( 'baaccommodationarea', $var['drop_sts'] ? $bdfrom : $bdto );

                add_variable( 'dep_css', '' );
                add_variable( 'dep_trans_css', '' );
                add_variable( 'edit_link_css', ( ( $int->days == 1 && $int->h == 0 && $int->i == 0 && $int->s == 0 ) || $int->days == 0 )  ? 'sr-only' : '' );

                parse_template( 'detail-departure-loop-block', 'dtdl-block', true );
            }

            parse_template( 'detail-departure-block', 'dtd-block' );
        }
        else
        {
            add_variable( 'dep_css', 'sr-only' );
            add_variable( 'dep_trans_css', 'sr-only' );
            add_variable( 'edit_link_css', 'sr-only' );
        }

        //-- Back Trip
        if( isset( $return ) )
        {
            foreach( $return as $key => $rt_trip )
            {
                extract( $rt_trip );

                $rtn_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                $rtn_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                $rtn_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                $rtn_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';

                add_variable( 'rtn_bdto', $bdto );
                add_variable( 'rtn_bdfrom', $bdfrom );
                add_variable( 'rtn_bddeparttime', $bddeparttime );
                add_variable( 'rtn_bdarrivetime', $bdarrivetime );

                add_variable( 'rtn_discount', $discount );
                add_variable( 'rtn_price_per_adult', $price_per_adult );
                add_variable( 'rtn_price_per_child', $price_per_child );
                add_variable( 'rtn_price_per_infant', $price_per_infant );

                add_variable( 'rtn_pa_display', $rtn_pa_display );
                add_variable( 'rtn_pc_display', $rtn_pc_display );
                add_variable( 'rtn_pi_display', $rtn_pi_display );
                add_variable( 'rtn_pt_display', $rtn_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );
                add_variable( 'rtn_passenger', ticket_passenger_list_content( $passenger ) );

                add_variable( 'rtn_bdtype', ucfirst( $bdtype ) );
                add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'rtn_add_ons', ticket_detail_add_ons_content( $rt_trip ) );
                add_variable( 'rtn_transport_css', empty( $transport ) ? 'sr-only' : '' );
                add_variable( 'rtn_transport', ticket_detail_transport_content( $transport, $bdfrom, $bdto ) );

                add_variable( 'rtn_css', '' );
                add_variable( 'rtn_trans_css', '' );

                parse_template( 'detail-return-loop-block', 'dtrl-block', true );
            }

            parse_template( 'detail-return-block', 'dtr-block' );
        }
        else
        {
            add_variable( 'rtn_css', 'sr-only' );
            add_variable( 'rtn_trans_css', 'sr-only' );
        }

        if( empty( $data['pmcode'] ) )
        {
            add_variable( 'sub_css', 'sr-only' );
            add_variable( 'subtotal_display', number_format( $data['bsubtotal'], 0, ',', '.' ) );
            add_variable( 'discount_display', number_format( $data['bdiscount'], 0, ',', '.' ) );
            add_variable( 'grandtotal_display', number_format( $data['btotal'], 0, ',', '.' ) );
        }
        else
        {
            add_variable( 'pmcode', 'Code : ' . $data['pmcode'] );
            add_variable( 'subtotal_display', number_format( $data['bsubtotal'], 0, ',', '.' ) );
            add_variable( 'discount_display', number_format( $data['bdiscount'], 0, ',', '.' ) );
            add_variable( 'grandtotal_display', number_format( $data['btotal'], 0, ',', '.' ) );
        }

        add_variable( 'img_url', get_theme_img() );
        add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-booking-ajax' );

        add_actions( 'section_title', 'Booking Detail' );
        add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/numeral.min.js' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        parse_template( 'detail-block', 'dt-block' );

        return return_template( 'rv-detail' );
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Reservation Detail
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_reservation_edit_detail( $bid )
{
    $site_url = site_url();
    $message  = run_booking_update( true );
    $data     = ticket_booking_all_data( $bid  );

    if( !empty( $data ) )
    {
        extract( $data['detail'] );

        $bstatus_message = ticket_booking_status( $data['bstatus'] );

        set_template( TEMPLATE_PATH . '/template/reservation/edit.html', 'edt-form' );

        add_block( 'form-departure-loop-block', 'fmdl-block', 'edt-form' );
        add_block( 'form-return-loop-block', 'fmrl-block', 'edt-form' );
        add_block( 'form-departure-block', 'fmd-block', 'edt-form' );
        add_block( 'form-return-block', 'fmr-block', 'edt-form' );
        add_block( 'edit-block', 'edt-block', 'edt-form' );

        add_variable( 'site_url', $site_url );
		add_variable( 'gemail', $data['gemail'] );
		add_variable( 'gmobile', $data['gmobile'] );
        add_variable( 'bcode', $data['bcode'] );
        add_variable( 'booking_id', $data['bid'] );
        add_variable( 'bstatus', $data['bstatus'] );
        add_variable( 'bticket', $data['bticket'] );
        add_variable( 'bstatus_message', $bstatus_message );

        add_variable( 'bbname', $data['bbname'] );
        add_variable( 'bbemail', $data['bbemail'] );
        add_variable( 'bbphone', $data['bbphone'] );

        if( empty( $data['agid'] ) )
        {
            add_variable( 'bsource', $data['chname'] );
        }
        else
        {
            $agid = empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'];

            add_variable( 'bsource', get_agent( $agid, 'agname' ) );
        }

        add_variable( 'bagremark', $data['bagremark'] );
        add_variable( 'bbrevagent', $data['bbrevagent'] );
        add_variable( 'bbooking_staf', $data['bbooking_staf'] );
        add_variable( 'bbrevtype_option', get_revtype_option( $data['bbrevtype'] ) );
        add_variable( 'bbrevstatus_option', get_revstatus_option( $data['bbrevstatus'] ) );
        add_variable( 'bbcountry_list_option', get_country_list_option( $data['bbcountry'] ) );

        add_variable( 'btype', ticket_booking_type( $data['btype'] ) );
        add_variable( 'bdate', date( 'd F Y', strtotime( $data['bdate'] ) ) );

        //-- Go Trip
        if( isset( $departure ) )
        {
            foreach( $departure as $dp_trip )
            {
                extract( $dp_trip );

                $dep_trans = get_transport_edit_field( $dp_trip, true );

                add_variable( 'dep_item', ticket_item_list_edit_content( $agid, $data['bdate'], $dp_trip, $data, true ) );
                add_variable( 'dep_passenger', ticket_passenger_list_edit_content( $passenger ) );
                add_variable( 'dep_add_ons', ticket_add_ons_edit_content( $dp_trip ) );

                add_variable( 'dep_transport', $dep_trans );
                add_variable( 'dep_bdtype', ucfirst( $bdtype ) );
                add_variable( 'dep_trans_css', empty( $dep_trans ) ? 'sr-only' : '' );

                parse_template( 'form-departure-loop-block', 'fmdl-block', true );
            }

            parse_template( 'form-departure-block', 'fmd-block' );
        }

        //-- Back Trip
        if( isset( $return ) )
        {
            foreach( $return as $rt_trip )
            {
                extract( $rt_trip );

                $rtn_trans = get_transport_edit_field( $rt_trip, true );

                add_variable( 'rtn_item', ticket_item_list_edit_content( $agid, $data['bdate'], $rt_trip, $data, true ) );
                add_variable( 'rtn_passenger', ticket_passenger_list_edit_content( $passenger ) );
                add_variable( 'rtn_add_ons', ticket_add_ons_edit_content( $rt_trip ) );

                add_variable( 'rtn_transport', $rtn_trans );
                add_variable( 'rtn_bdtype', ucfirst( $bdtype ) );
                add_variable( 'rtn_trans_css', empty( $rtn_trans ) ? 'sr-only' : '' );

                parse_template( 'form-return-loop-block', 'fmrl-block', true );
            }

            parse_template( 'form-return-block', 'fmr-block' );
        }
        else
        {
            add_variable( 'rtn_css', 'sr-only' );
            add_variable( 'rtn_trans_css', 'sr-only' );
        }

        $btotal       = $data['btotal'];
        $bsubtotal    = $data['bsubtotal'];
        $bdiscount    = $data['bdiscount'];
        $bonhandtotal = $data['bonhandtotal'];
        $remaintotal  = $data['btotal'] - $data['bonhandtotal'];

        if( empty( $data['pmcode'] ) )
        {
            add_variable( 'sub_css', 'sr-only' );
        }
        else
        {
            add_variable( 'pmcode', 'Code : ' . $data['pmcode'] );
        }

        add_variable( 'onhandtotal_display', $bonhandtotal );
        add_variable( 'remaintotal_display', $remaintotal );
        add_variable( 'subtotal_display', $bsubtotal );
        add_variable( 'discount_display', $bdiscount );
        add_variable( 'grandtotal_display', $btotal );

        add_variable( 'bhotelname', $data['bhotelname'] );
        add_variable( 'bhoteladdress', $data['bhoteladdress'] );
        add_variable( 'bhotelphone', $data['bhotelphone'] );
        add_variable( 'bhotelemail', $data['bhotelemail'] );

        add_variable( 'subtotal', $data['bsubtotal'] );
        add_variable( 'discount', $data['bdiscount'] );
        add_variable( 'onhandtotal', $data['bonhandtotal'] );
        add_variable( 'message', generate_message_block( $message ) );
        add_variable( 'ajax_url', get_agent_admin_url( 'ajax-request' ) );
        add_variable( 'payment_detail_content', get_payment_edit_field( $data ) );
        add_variable( 'state_url', get_agent_admin_url( 'reservation&sub=booking' ) );
        add_variable( 'popup_link', get_agent_admin_url( 'reservation&sub=booking&prc=popup-edit-trip&id=' . $bid ) );

        add_actions( 'section_title', 'Booking Detail' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
        add_actions( 'header_elements', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/numeral.min.js?v=' . TICKET_VERSION );
        add_actions( 'header_elements', 'get_javascript_inc', 'autonumeric-1.9.46/autoNumeric-min.js' );
                
        parse_template( 'edit-block', 'edt-block' );

        return return_template( 'edt-form' );
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Availability Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_availability_content()
{
    $site_url = site_url();
    $data     = get_agent( $_COOKIE['agid'] );

    set_template( TEMPLATE_PATH . '/template/availability.html', 'av-template' );

    add_block( 'availability-block', 'av-block', 'av-template' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'chid', $data['chid'] );
    add_variable( 'agcod', $data['agcod'] );

    add_variable( 'destination_list', get_availibility_loc_option() );
    add_variable( 'from_list', get_availibility_loc_option( null, null, 'from' ) );
    add_variable( 'to_list', get_availibility_loc_option( null, null, 'to' ) );
    add_variable( 'boat_seat_panel', get_seat_availibility_by_boat() );
    add_variable( 'trip_seat_panel', get_seat_availibility_by_trip() );
    add_variable( 'calendar_detail', get_agent_allotment_calendar_detail( $data['agid'] ) );

    add_variable( 'site_url', $site_url );
    add_variable( 'action', HTSERVER . $site_url .'/ticket-agent-booking-process' );
    add_variable( 'ajax_url', get_agent_admin_url( 'ajax-request' ) );

    parse_template( 'availability-block', 'av-block', false );

    return return_template( 'av-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Profile Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_profile_content()
{
    $message = run_update_agent_profile();
    $data    = get_agent( $_COOKIE['agid'] );

    set_template( TEMPLATE_PATH . '/template/profile.html', 'pf-template' );

    add_block( 'profile-block', 'pf-block', 'pf-template' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'agname', $data['agname'] );
    add_variable( 'agaddress', $data['agaddress'] );
    add_variable( 'agemail', $data['agemail'] );
    add_variable( 'agcp', $data['agcp'] );
    add_variable( 'agmobile', $data['agmobile'] );
    add_variable( 'agphone', $data['agphone'] );
    add_variable( 'agfax', $data['agfax'] );
    add_variable( 'agophone', $data['agophone'] );
    add_variable( 'agusername', $data['agusername'] );
    add_variable( 'message', generate_message_block( $message ) );

    add_variable( 'site_url', site_url() );
    add_variable( 'is_read_only', 'readonly');
    add_variable( 'action', get_agent_admin_url( 'profile-setting&sub=profile' ) );

    parse_template( 'profile-block', 'pf-block', false );

    return return_template( 'pf-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Report Content - Promo Code
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_promo_report_content()
{
    global $db;

    $filter = get_filter_agent_promo_code_report( $_COOKIE['agid'] );
    $data   = get_agent_promo_code_report( $filter );

    extract( $data );
    extract( $filter );

    set_template( TEMPLATE_PATH . '/template/report/promo-code-report.html', 'report' );

    add_block( 'report-data-block', 'rd-block', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );
    add_block( 'no-report-block', 'nrp-block', 'report' );

    if( empty( $data['items'] ) )
    {
        add_variable( 'bdate', $bdate );
        add_variable( 'bdateto', $bdateto );
        add_variable( 'booking_source', $chid );
        add_variable( 'report_based_on_option', get_based_on_option( $rbased ) );
        add_variable( 'report_type_option', get_report_type_option( $rtype, 'transport' ) );

        add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
        add_variable( 'promo_code_option', get_promo_code_option( $promo_code, false ) );
        add_variable( 'booking_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );

        parse_template( 'no-report-block', 'nrp-block', false );
    }
    else
    {
        $i = 0;

        $category = array();
        $series   = array();

        foreach( $items as $pmcode => $groups )
        {
            $tblcontent = '
            <div class="item">
                <div class="rs-table">
                    <table id="reservation-table" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th width="15%">Date</th>
                                <th width="35%">Booking Ticket</th>
                                <th>Travel Date</th>
                                <th>Route</th>
                                <th class="text-right">Commission (IDR)</th>
                            </tr>
                        </thead>
                        <tbody>';

                            $total_sum_by_code = 0;

                            foreach( $groups['items'] as $d )
                            {
                                $total_sum_by_code += $d['bcommission'];

                                $tblcontent .= '
                                <tr>
                                    <td>' . date( 'd F Y', strtotime( $d['bdate'] ) ) . '</td>
                                    <td>Booking #' . $d['bticket'] .'</td>
                                    <td>' . $d['bddate'] . '</td>
                                    <td>' . $d['route'] . '</td>
                                    <td class="text-right">' . number_format( $d['bcommission'], 0, '.' , '.' ) . '</td>
                                </tr>';
                            }

                            $tblcontent .= '
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4" class="text-right">Total :</th>
                                <th class="text-right">' . number_format( $total_sum_by_code, 0, '.' , '.' ) . '</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>';

            add_variable( 'pmcode', $pmcode );
            add_variable( 'trip_data', $tblcontent );
            add_variable( 'total_used', $groups['totalbycode'] );
            add_variable( 'trip_item_cls', empty( $i ) ? 'opened' : '' );
            add_variable( 'total_used_percent', round( ( $groups['totalbycode'] * 100 ) / $total, 2 ) );

            parse_template( 'report-data-block', 'rd-block', true );

            $i++;
        }

        add_variable( 'bdate', $bdate );
        add_variable( 'bdateto', $bdateto );
        add_variable( 'booking_source', $chid );
        add_variable( 'report_based_on_option', get_based_on_option( $rbased ) );
        add_variable( 'report_type_option', get_report_type_option( $rtype, 'transport' ) );

        add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
        add_variable( 'promo_code_option', get_promo_code_option( $promo_code, false ) );
        add_variable( 'booking_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );
        add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
        add_variable( 'site_url', HTSERVER . site_url() );

        parse_template( 'report-block', 'rp-block', false );
    }

    add_actions( 'section_title', 'Report - Promo Code' );
    add_actions( 'header_elements', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Report Content - Promo Code Data
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_agent_promo_code_report( $filter )
{
    global $db;

    extract( $filter );

    $w = array();

    $odate = ( $rbased == 1 ) ? 'b.bddate' : 'a.bdate';
    $based = ( $rbased == 1 ) ? 'b.bddate AS bdate,' : 'a.bdate AS bdate,';

    if( $rtype != '' && $bdate != '' )
    {
        if( $rtype == '0'  )
        {
            $date = date( 'Y-m-d', strtotime( $bdate ) );

            $w[]  = $db->prepare_query( $odate .' = %s', $date );
        }
        elseif( $rtype == '1' )
        {
            $date = date( 'Y-m-d', strtotime( '1 ' . $bdate ) );

            $w[]  = $db->prepare_query( ' YEAR( ' . $odate . ' ) = YEAR( %s ) AND MONTH( '. $odate .' ) = MONTH( %s )', $date, $date );
        }
        elseif( $rtype == '2' )
        {
            $date = date( 'Y-m-d', strtotime( '1 Jan ' . $bdate ) );

            $w[]  = $db->prepare_query( ' YEAR( '. $odate .' ) = YEAR( %s )', $date );
        }
        elseif( $rtype == '3' )
        {
            $date_from = date( 'Y-m-d', strtotime( $bdate ) );
            $date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

            $w[] = $db->prepare_query( $odate .' BETWEEN %s AND %s', $date_from, $date_to );
        }
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = '( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'a.bstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'a.bstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' a.bstatus = %s', $bstatus );
        }

        $w[] = $b;
    }
    else
    {
        $w[] = 'a.bstatus NOT IN ("cn","bc","pf","rf")';
    }

    if( $promo_code != '' )
    {
        if( is_array( $promo_code ) )
        {
            $epromo_code = end( $promo_code );

            $p = '( ';

            foreach( $promo_code as $st )
            {
                if( $st == $epromo_code )
                {
                    $p .= $db->prepare_query( 'a.pmcode = %s', $st );
                }
                else
                {
                    $p .= $db->prepare_query( 'a.pmcode = %s OR ', $st );
                }
            }

            $p .= ' )';
        }
        else
        {
            $p = $db->prepare_query( ' a.pmcode = %s', $promo_code );
        }

        $w[] = $p;
    }
    else
    {
        $w[] = ' a.pmcode <> ""';
    }

    if( empty( $w ) === false )
    {
        $where = implode( ' AND ' , $w );
    }

    $q = 'SELECT '
            . $based .
            ' a.bid,
            a.agid,
            a.pmcode,
            a.bticket,
            a.bdiscount,
            a.bcommission,
            a.bcommissiontype,
            a.bcommissioncondition,
            a.badditionaldisc,
            b.bdstatus,
            b.bddate,
            b.bdfrom,
            b.bdto,
            b.num_adult,
            b.num_child,
            b.num_infant,
            b.net_price_per_adult,
            b.net_price_per_child,
            b.net_price_per_infant,
            b.disc_price_per_adult,
            b.disc_price_per_child,
            b.disc_price_per_infant
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          WHERE a.bstt <> "ar" AND a.agid = "' . $_COOKIE['agid'] . '" AND ' . $where . '
          ORDER BY a.bdate ASC';
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return array( 'total' => 0, 'items' => array() );
    }
    else
    {
        $items  = array();
        $data   = array();
        $darray = array();
        $rcount = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $npmcode = get_pmcode( strtoupper( trim( $d['pmcode'] ) ) );

            $data[ $npmcode ][ $d['bid'] ][] = $d;
        }

        foreach( $data as $pmcode => $obj )
        {
            foreach( $obj as $bid => $dt )
            {
                $dtotal = 0;
                $ctotal = 0;

                foreach( $dt as $d )
                {
                    //-- Check Base Total
                    //-- 0 = after discount
                    //-- 1 = before discount
                    if( $d['bcommissioncondition'] == '0' && $d['badditionaldisc'] == '2' )
                    {
                        $total   = ( ( ( $d['net_price_per_adult'] - $d['disc_price_per_adult'] ) * $d['num_adult'] ) + ( ( $d['net_price_per_child'] - $d['disc_price_per_child'] ) * $d['num_child'] ) + ( ( $d['net_price_per_infant'] - $d['disc_price_per_infant'] ) * $d['num_infant'] ) );
                        $dtotal += $total;
                    }
                    else
                    {
                        $total   = ( ( $d['net_price_per_adult'] * $d['num_adult'] ) + ( $d['net_price_per_child'] * $d['num_child'] ) + ( $d['net_price_per_infant'] * $d['num_infant'] ) );
                        $dtotal += $total;
                    }

                    //-- Calculate Cancel Trip
                    if( $d['bdstatus'] == 'cn' )
                    {
                        $ctotal += $total;
                    }

                    if( end( $dt ) == $d )
                    {
                        $rcount++;

                        //-- Check again commission condition
                        //-- If use discount => total - bdiscount
                        if( $d['bcommissioncondition'] == '0' )
                        {
                            $dtotal = $dtotal - $d['bdiscount'];
                        }

                        //-- Check Commission Type
                        //-- 0 = %
                        //-- 1 = Rp.
                        if( $d['bcommissiontype'] == '0' )
                        {
                            $bcommission = $dtotal * ( $d['bcommission'] / 100 );
                        }
                        else
                        {
                            $bcommission = ( $dtotal - $ctotal ) * ( $d['bcommission'] / $dtotal );
                        }

                        $items[ $pmcode ][ 'totalbycode' ] = count( $data[ $pmcode ] );
                        $items[ $pmcode ][ 'items' ][]     = array(
                            'pmcode'      => $pmcode,
                            'bdate'       => $d['bdate'],
                            'bcommission' => $bcommission,
                            'bticket'     => $d['bticket'],
                            'route'       => $d['bdfrom'] . ' - ' . $d['bdto'],
                            'bddate'      => date( 'd F Y', strtotime( $d['bddate'] ) )
                        );
                    }
                }
            }
        }

        return array( 'total' => $rcount, 'items' => $items );
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Report Content - Outstanding Payment
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_outstanding_report_content()
{
    global $db;

    $filter = get_agent_outstanding_report_filter();

    extract( $filter );

    if( is_agent_report_filter_view() )
    {
        $w = '';

        if( $atstatus != '' )
        {
            $w .= $db->prepare_query( ' AND atstatus = %d', $atstatus );
        }

        if( $bticket != '' )
        {
            $w .= $db->prepare_query( ' AND bticket = %s', $bticket );
        }

        if( $date_start != '' && $date_end != '' )
        {
            $dt_start = date( 'Y-m-d', strtotime( $date_start ) );
            $dt_end   = date( 'Y-m-d', strtotime( $date_end ) );

            if( $atdatetype == 1 )
            {
                $w .= $db->prepare_query( ' AND b.bdate BETWEEN %s AND %s', $dt_start, $dt_end );
            }
            elseif( $atdatetype == 2 )
            {
                $w .= $db->prepare_query( ' AND d.bddate BETWEEN %s AND %s', $dt_start, $dt_end );
            }
        }


        $s = 'SELECT *
              FROM ticket_agent_transaction AS a
              LEFT JOIN ticket_booking AS b ON a.bid = b.bid
              LEFT JOIN ticket_agent AS c ON a.agid = c.agid
			  LEFT JOIN ticket_booking_detail AS d ON d.bid = b.bid
              WHERE b.bstt <> "ar" AND c.agpayment_type = "Credit" AND (
                SELECT GROUP_CONCAT( DISTINCT( a2.bdpstatus ) )
                FROM ticket_booking_detail AS a2
                WHERE a2.bid = a.bid
              ) NOT IN( "cn","bc" ) AND d.bdtype = "departure"
              AND a.agid = %d ' . $w . '
              ORDER BY a.atdate ASC';
        $q = $db->prepare_query( $s, $_COOKIE['agid'] );
        $r = $db->do_query( $q );
    }
    else
    {
        $s = 'SELECT *
              FROM ticket_agent_transaction AS a
              LEFT JOIN ticket_booking AS b ON a.bid = b.bid
              LEFT JOIN ticket_agent AS c ON a.agid = c.agid
			  LEFT JOIN ticket_booking_detail AS d ON d.bid = b.bid
              WHERE b.bstt <> "ar" AND c.agpayment_type = "Credit" AND (
                SELECT GROUP_CONCAT( DISTINCT( a2.bdpstatus ) )
                FROM ticket_booking_detail AS a2
                WHERE a2.bid = a.bid
              ) NOT IN( "cn","bc" ) AND d.bdtype = "departure"
              AND a.agid = %d ORDER BY a.atdate ASC';
        $q = $db->prepare_query( $s, $_COOKIE['agid'] );
        $r = $db->do_query( $q );
    }

    set_template( TEMPLATE_PATH . '/template/report/list.html', 'ls-template' );
    
    add_block( 'list-block', 'ls-block', 'ls-template' );

    add_variable( 'agid', $_COOKIE['agid'] );
    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );

    add_variable( 'date_end', $date_end );
    add_variable( 'date_start', $date_start );
    add_variable( 'boking_ticket', $bticket );

    add_variable( 'site_url', site_url() );
    add_variable( 'list', agent_outstanding_report_list_data( $r ) );
    add_variable( 'date_option', get_agent_report_date_option( $atdatetype ) );
    add_variable( 'action', get_agent_admin_url( 'report&sub=outstanding-report' ) );
    add_variable( 'status_option', get_agent_report_status_option( $atstatus, true, 'All Status' ) );

    add_actions( 'header_elements', 'get_css_inc', 'fancybox/dist/jquery.fancybox.min.css' );
    add_actions( 'header_elements', 'get_javascript_inc', 'fancybox/dist/jquery.fancybox.min.js' );

    parse_template( 'list-block', 'ls-block', false );

    return return_template( 'ls-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Agent Report List Table Data
| -------------------------------------------------------------------------------------------------------------------------
*/
function agent_outstanding_report_list_data( $result )
{
    global $db;

    if( $db->num_rows( $result ) == 0 )
    {
        return '<tr><td colspan="9"><p class="text-center text-danger">No data found</p></td></tr>';
    }

    set_template( TEMPLATE_PATH . '/template/report/loop.html', 'lp-template' );

    add_block( 'loop-block', 'lp-loop', 'lp-template' );

    $data = array();

    $atoutstanding = 0;

    while( $d = $db->fetch_array( $result ) )
    {
        if( $d['atstatus'] == 1 )
        {
            $d['atstatus'] = 'Confirmed';
        }
        elseif( $d['atstatus'] == 2 )
        {
            $d['atstatus'] = 'Paid Balance';
        }
        elseif( $d['atstatus'] == 3 )
        {
            $d['atstatus'] = 'Canceled, cancelation fee applied';
        }

        $atoutstanding = $atoutstanding + ( $d['atdebet'] - $d['atcredit'] );

        $d['atoutstanding'] = $atoutstanding;

        $data[] = $d;
    }

    krsort( $data );

    $url       = get_agent_admin_url( 'report&page=' );
    $page      = empty( $_GET['page'] ) ? 1 : (int) $_GET['page'];
    $total     = count( $data );
    $viewed    = list_viewed();
    $limit     = post_viewed();
    $ttl_pages = ceil( $total / $limit );
    $page      = max( $page, 1 );
    $page      = min( $page, $ttl_pages );
    $offset    = $limit * ( $page - 1 );
    $offset    = $offset < 0 ? 0 : $offset;
    $data      = array_slice( $data, $offset, $limit );

    foreach( $data as $d )
    {
        add_variable( 'bticket', $d['bticket'] );
        add_variable( 'atgname', $d['atgname'] );
        add_variable( 'atstatus', $d['atstatus'] );
        add_variable( 'bdate', date( 'd F, Y', strtotime( $d['bdate'] ) ) );
        add_variable( 'bddate', date( 'd F, Y', strtotime( $d['bddate'] ) ) );
        add_variable( 'atdate', date( 'd F, Y', strtotime( $d['atdate'] ) ) );
        add_variable( 'atdebet', number_format( $d['atdebet'], 0, '', '.' ) );
        add_variable( 'atcredit', number_format( $d['atcredit'], 0, '', '.' ) );
        add_variable( 'bbrevagent', empty( $d['bbrevagent'] ) ? '-' : $d['bbrevagent'] );
        add_variable( 'atoutstanding', number_format( $d['atoutstanding'], 0, '', '.' ) );

        parse_template('loop-block', 'lp-loop', true);
    }

    add_variable( 'paging', paging( $url, $total, $page, $limit, $limit ) );

    return return_template('lp-template');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Sub Agent List Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function sub_agent_list_content()
{
    global $db;

    $s = 'SELECT * FROM ticket_agent WHERE agparent = %d ORDER BY agid DESC';
    $q = $db->prepare_query( $s, $_COOKIE['agid'] );
    $r = $db->do_query( $q );

    set_template( TEMPLATE_PATH . '/template/sub-agent/list.html', 'ls-template' );

    add_block( 'list-block', 'ls-block', 'ls-template' );

    add_variable( 'site_url', site_url() );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', sub_agent_list_data( $r ) );
    add_variable( 'action', get_agent_admin_url( 'profile-setting&sub=sub-agent' ) );
    add_variable( 'add_new_link', get_agent_admin_url( 'profile-setting&sub=sub-agent&prc=add-new' ) );

    parse_template( 'list-block', 'ls-block', false );

    return return_template( 'ls-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Sub Agent List Table Data
| -------------------------------------------------------------------------------------------------------------------------
*/
function sub_agent_list_data( $r )
{
    global $db;

    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="7">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( TEMPLATE_PATH . '/template/sub-agent/loop.html', 'lp-template' );

    add_block( 'loop-block', 'lp-block', 'lp-template' );

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'id', $d['agid'] );
        add_variable( 'agname', $d['agname'] );
        add_variable( 'agemail', $d['agemail'] );
        add_variable( 'agaddress', $d['agaddress'] );
        add_variable( 'agcp', empty( $d['agcp'] ) ? '-' : $d['agcp'] );
        add_variable( 'agstatus', $d['agstatus'] == '0' ? 'Active' : 'Suspended' );
        add_variable( 'agphone', empty( $d['agphone'] ) ? $d['agmobile'] : $d['agphone'] );

        add_variable( 'edit_link', get_agent_admin_url( 'profile-setting&sub=sub-agent&prc=edit&id=' . $d['agid'] ) );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-agent-ajax/' );

        parse_template( 'loop-block', 'lp-block', true );
    }

    return return_template( 'lp-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Add New Sub Agent Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function sub_agent_add_new()
{
    $message = run_save_sub_agent();
    $data    = get_agent();

    set_template( TEMPLATE_PATH . '/template/sub-agent/form.html', 'fm-template' );

    add_block( 'form-block', 'fm-block', 'fm-template' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'agname', $data['agname'] );
    add_variable( 'agparent', $data['agparent'] );
    add_variable( 'agaddress', $data['agaddress'] );
    add_variable( 'agemail', $data['agemail'] );
    add_variable( 'agcp', $data['agcp'] );
    add_variable( 'agmobile', $data['agmobile'] );
    add_variable( 'agphone', $data['agphone'] );
    add_variable( 'agfax', $data['agfax'] );
    add_variable( 'agophone', $data['agophone'] );
    add_variable( 'agusername', isset( $_POST['agnusername'] ) ? $_POST['agnusername'] : $data['agusername'] );
    add_variable( 'agstatus', get_agent_status_option( $data['agstatus'] ) );

    add_variable( 'site_url', site_url() );
    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'cancel_link', get_agent_admin_url( 'profile-setting&sub=sub-agent' ) );

    parse_template( 'form-block', 'fm-block', false );

    return return_template( 'fm-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Edit Sub Agent Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function sub_agent_edit()
{
    $message = run_update_sub_agent();
    $data    = get_agent( $_GET['id'] );

    set_template( TEMPLATE_PATH . '/template/sub-agent/form.html', 'fm-template' );

    add_block( 'form-block', 'fm-block', 'fm-template' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'agname', $data['agname'] );
    add_variable( 'agparent', $data['agparent'] );
    add_variable( 'agaddress', $data['agaddress'] );
    add_variable( 'agemail', $data['agemail'] );
    add_variable( 'agcp', $data['agcp'] );
    add_variable( 'agmobile', $data['agmobile'] );
    add_variable( 'agphone', $data['agphone'] );
    add_variable( 'agfax', $data['agfax'] );
    add_variable( 'agophone', $data['agophone'] );
    add_variable( 'agusername', $data['agusername'] );
    add_variable( 'agstatus', get_agent_status_option( $data['agstatus'] ) );

    add_variable( 'site_url', site_url() );
    add_variable( 'is_read_only', 'readonly');
    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'cancel_link', get_agent_admin_url( 'profile-setting&sub=sub-agent' ) );

    parse_template( 'form-block', 'fm-block', false );

    return return_template( 'fm-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Delete Batch Sub Agent Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function sub_agent_batch_delete()
{
    set_template( TEMPLATE_PATH . '/template/sub-agent/batch-delete.html', 'dl-template' );

    add_block( 'loop-block', 'lp-block', 'dl-template' );
    add_block( 'delete-block', 'dl-block', 'dl-template' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_agent( $val );

        add_variable( 'agname',  $d['agname'] );
        add_variable( 'agid', $d['agid'] );

        parse_template( 'loop-block', 'lp-block', true);
    }

    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' sub agent? :' );
    add_variable( 'action', get_agent_admin_url( 'profile-setting&sub=sub-agent' ) );

    parse_template( 'delete-block', 'dl-block', false );

    return return_template( 'dl-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Run Save Sub Agent
| -------------------------------------------------------------------------------------------------------------------------
*/
function run_save_sub_agent()
{
    if( is_agent_publish() )
    {
        $error = validate_sub_agent_data();

        if( empty($error) )
        {
            $post_id = save_sub_agent();

            header( 'location:' . get_agent_admin_url( 'profile-setting&sub=sub-agent&prc=add-new&result=1' ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This sub agent successfully saved' ) );
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Run Update Sub Agent
| -------------------------------------------------------------------------------------------------------------------------
*/
function run_update_sub_agent()
{
    if( is_agent_publish() )
    {
        $error = validate_sub_agent_data();

        if( empty($error) )
        {
            $post_id = update_sub_agent();

            return array( 'type'=> 'success', 'content' => array( 'This sub agent successfully edited' ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This sub agent successfully edited' ) );
    }
}

/*
| -------------------------------------------------------------------------------------
| Validate Sub Agent
| -------------------------------------------------------------------------------------
*/
function validate_sub_agent_data()
{
    $error = array();

    if( isset( $_POST['agname'] ) && empty( $_POST['agname'] ) )
    {
        $error[] = 'Sub Agent name can\'t be empty';
    }

    if( isset( $_POST['agaddress'] ) && empty( $_POST['agaddress'] ) )
    {
        $error[] = 'Address can\'t be empty';
    }

    if( isset( $_POST['agemail'] ) )
    {
        if( empty( $_POST['agemail'] ) )
        {
            $error[] = 'Email can\'t be empty';
        }
        else
        {
            if( !isEmailAddress( $_POST['agemail'] ) )
            {
                $error[] = 'Invalid email format ( <em>' . $_POST['agemail'] . '</em> )';
            }
        }
    }

    if( isset( $_POST['agphone'] ) && empty( $_POST['agphone'] ) && isset( $_POST['agmobile'] ) && empty( $_POST['agmobile'] ) )
    {
        $error[] = 'Phone or mobile number can\'t be empty';
    }

    if( isset( $_POST['agnusername'] ) )
    {
        if( empty( $_POST['agnusername'] ) && is_agent_add_new() )
        {
            $error[] = 'Username can\'t be empty';
        }
        elseif( is_exist_agent_username( $_POST['agnusername'] ) && is_agent_add_new() )
        {
            $error[] = 'This username already exists';
        }
    }

    if( is_agent_add_new()  )
    {
        if( empty( $_POST['agnpassword'] )  || strlen( $_POST['agnpassword'] ) < 7 )
        {
            $error[] = 'The password should be at least seven characters long';
        }

        if( $_POST['agnpassword'] != $_POST['agrpassword'] )
        {
            $error[] = 'Password do not match';
        }
    }
    elseif( is_agent_edit() )
    {
        if( !empty( $_POST['agnpassword'] ) )
        {
            if( strlen( $_POST['agnpassword'] ) < 7 )
            {
                $error[] = 'Password should be at least seven characters long';
            }
            elseif( $_POST['agnpassword'] != $_POST['agrpassword'] )
            {
                $error[] = 'Password do not match';
            }
        }
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Sub Agent Email
| -------------------------------------------------------------------------------------
*/
function is_exist_agent_username( $username )
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM ticket_agent WHERE agusername = %s', $username );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Sub Agent List Count
| -------------------------------------------------------------------------------------
*/
function is_num_sub_agent( $id = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_agent WHERE agparent = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Save Sub Agent
| -------------------------------------------------------------------------------------
*/
function save_sub_agent()
{
    global $db;

    $s = 'SELECT chid, luser_id, agtype FROM ticket_agent WHERE agid = %d';
    $q = $db->prepare_query( $s, $_COOKIE['agid'] );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $s = 'INSERT INTO ticket_agent(
            chid,
            agtype,
            agparent,
            agname,
            agaddress,
            agemail,
            agcp,
            agmobile,
            agphone,
            agfax,
            agophone,
            agstatus,
            agusername,
            agpassword,
            agreateddate,
            luser_id) VALUES( %d, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s,
            $d['chid'],
            $d['agtype'],
            $_POST['agparent'],
            $_POST['agname'],
            $_POST['agaddress'],
            $_POST['agemail'],
            $_POST['agcp'],
            $_POST['agmobile'],
            $_POST['agphone'],
            $_POST['agfax'],
            $_POST['agophone'],
            $_POST['agstatus'],
            $_POST['agnusername'],
            md5( $_POST['agnpassword'] ),
            date( 'Y-m-d H:i:s' ),
            $d['luser_id'] );

    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();

        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Sub Agent
| -------------------------------------------------------------------------------------
*/
function update_sub_agent()
{
    global $db;

    if( isset( $_POST['agnpassword'] ) && !empty( $_POST['agnpassword'] ) )
    {
        $s = 'UPDATE ticket_agent SET
                agid = %d,
                agparent = %d,
                agname = %s,
                agaddress = %s,
                agemail = %s,
                agcp = %s,
                agmobile = %s,
                agphone = %s,
                agfax = %s,
                agophone = %s,
                agstatus = %s,
                agpassword = %s
              WHERE agid = %d';
        $q = $db->prepare_query( $s,
                $_POST['agid'],
                $_COOKIE['agid'],
                $_POST['agname'],
                $_POST['agaddress'],
                $_POST['agemail'],
                $_POST['agcp'],
                $_POST['agmobile'],
                $_POST['agphone'],
                $_POST['agfax'],
                $_POST['agophone'],
                $_POST['agstatus'],
                md5( $_POST['agnpassword'] ),
                $_POST['agid'] );
    }
    else
    {
        $s = 'UPDATE ticket_agent SET
                agid = %d,
                agparent = %d,
                agname = %s,
                agaddress = %s,
                agemail = %s,
                agcp = %s,
                agmobile = %s,
                agphone = %s,
                agfax = %s,
                agophone = %s,
                agstatus = %s
              WHERE agid = %d';
        $q = $db->prepare_query( $s,
                $_POST['agid'],
                $_COOKIE['agid'],
                $_POST['agname'],
                $_POST['agaddress'],
                $_POST['agemail'],
                $_POST['agcp'],
                $_POST['agmobile'],
                $_POST['agphone'],
                $_POST['agfax'],
                $_POST['agophone'],
                $_POST['agstatus'],
                $_POST['agid'] );
    }

    if( $db->do_query( $q ) )
    {
        return $_POST['agid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Run Update Agent Profile
| -------------------------------------------------------------------------------------
*/
function run_update_agent_profile()
{
    if( is_agent_publish() )
    {
        $error = validate_agent_profile_data();

        if( empty($error) )
        {
            $post_id = update_agent_profile();

            return array( 'type'=> 'success', 'content' => array( 'This agent profile successfully edited' ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This agent profile successfully edited' ) );
    }
}

/*
| -------------------------------------------------------------------------------------
| Validate Agent Profile
| -------------------------------------------------------------------------------------
*/
function validate_agent_profile_data()
{
    $error = array();

    if( isset( $_POST['agname'] ) && empty( $_POST['agname'] ) )
    {
        $error[] = 'Agent name can\'t be empty';
    }

    if( isset( $_POST['agaddress'] ) && empty( $_POST['agaddress'] ) )
    {
        $error[] = 'Address can\'t be empty';
    }

    if( isset( $_POST['agemail']) )
    {
        if( empty( $_POST['agemail'] ) )
        {
            $error[] = 'Email can\'t be empty';
        }
        else
        {
            if( !isEmailAddress( $_POST['agemail'] ) )
            {
                $error[] = 'Invalid email format ( <em>' . $email . '</em> )';
            }
        }
    }

    if( isset( $_POST['agphone'] ) && empty( $_POST['agphone'] ) && isset( $_POST['agmobile'] ) && empty( $_POST['agmobile'] ) )
    {
        $error[] = 'Phone or mobile number can\'t be empty';
    }

    if( isset( $_POST['agnpassword'] ) && !empty( $_POST['agnpassword'] ) )
    {
        if( strlen( $_POST['agnpassword'] ) < 7 )
        {
            $error[] = 'Password should be at least seven characters long';
        }
        elseif( $_POST['agnpassword'] != $_POST['agrpassword'] )
        {
            $error[] = 'Password do not match';
        }
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Update Agent Profile
| -------------------------------------------------------------------------------------
*/
function update_agent_profile()
{
    global $db;

    if( isset( $_POST['agnpassword'] ) && !empty( $_POST['agnpassword'] ) )
    {
        $s = 'UPDATE ticket_agent SET
                agid = %d,
                agname = %s,
                agaddress = %s,
                agemail = %s,
                agcp = %s,
                agmobile = %s,
                agphone = %s,
                agfax = %s,
                agophone = %s,
                agpassword = %s
              WHERE agid = %d';
        $q = $db->prepare_query( $s,
                $_POST['agid'],
                $_POST['agname'],
                $_POST['agaddress'],
                $_POST['agemail'],
                $_POST['agcp'],
                $_POST['agmobile'],
                $_POST['agphone'],
                $_POST['agfax'],
                $_POST['agophone'],
                md5( $_POST['agnpassword'] ),
                $_POST['agid'] );
    }
    else
    {
        $s = 'UPDATE ticket_agent SET
                agid = %d,
                agname = %s,
                agaddress = %s,
                agemail = %s,
                agcp = %s,
                agmobile = %s,
                agphone = %s,
                agfax = %s,
                agophone = %s
              WHERE agid = %d';
        $q = $db->prepare_query( $s,
                $_POST['agid'],
                $_POST['agname'],
                $_POST['agaddress'],
                $_POST['agemail'],
                $_POST['agcp'],
                $_POST['agmobile'],
                $_POST['agphone'],
                $_POST['agfax'],
                $_POST['agophone'],
                $_POST['agid'] );
    }

    if( $db->do_query( $q ) )
    {
        return $_POST['agid'];
    }
}

function is_agent_logged()
{
    if( isset( $_COOKIE['agid'] ) && isset( $_COOKIE['agpassword'] ) && isset( $_COOKIE['agentcookie'] ) )
    {
        if( md5( $_COOKIE['agpassword'] . $_COOKIE['agid'] ) == $_COOKIE['agentcookie'] )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function post_agent_login()
{
    if( isset( $_POST['login'] ) && ( isset( $_POST['usertype'] ) && $_POST['usertype'] == 'agent' ) )
    {
        return validate_agent_login();
    }
}

function validate_agent_login()
{
    if( empty( $_POST['username'] ) || empty( $_POST['password'] ) )
    {
        return '<div class="alert_red">Empty Username or Password.</div>';
    }
    else
    {
        if( is_exist_agent( $_POST['username'] ) && is_match_agent_password() )
        {
            $d = fetch_agent( $_POST['username'] );

            if( trim( $d['agtype'] ) == 'Net Rate' )
            {
                setcookie( 'agid', $d['agid'], false, '/' );
                setcookie( 'agtype', $d['agtype'], false, '/' );
                setcookie( 'agname', $d['agname'], false, '/' );
                setcookie( 'agusername', $_POST['username'], false, '/' );
                setcookie( 'agpassword', $_POST['password'], false, '/' );
                setcookie( 'agentcookie', md5( $_POST['password'] . $d['agid'] ), false, '/' );

                if( is_redirect() )
                {
                    header( 'location:' . base64_decode( $_GET['redirect'] ) );
                }
                else
                {
                    header( 'location:' . get_agent_url() . '/admin?state=dashboard' );
                }
            }
            else
            {
                return '<div class="alert_red">Wrong Username or Password.</div>';
            }
        }
        else
        {
            return '<div class="alert_red">Wrong Username or Password.</div>';
        }
    }
}

function post_agent_forget_password()
{
    if( count( $_POST ) > 0 && isset( $_POST['forget_password'] ) )
    {
        return validate_agent_forget_password();
    }
}

function validate_agent_forget_password()
{
    if( empty( $_POST['username'] ) )
    {
        return '<div class="alert_red">Please enter your username. You will receive a new password via e-mail.</div>';
    }
    else
    {
        if( is_exist_agent( $_POST['username'] ) )
        {
            $new_password = random_string();

            $agent = fetch_agent( $_POST['username'] );

            if( reset_agent_password( $agent['agid'], $new_password ) )
            {
                $return = reset_agent_password_email( $agent['agemail'], $agent['agusername'], $agent['agname'], $new_password );

                return '<div class="alert_red">Your request to reset this account password has been successfully. Please check your email to see the new password</div>';
            }
            else
            {
                return '<div class="alert_red">Sorry your request to reset this account password has been failed. Please try again later</div>';
            }
        }
        else
        {
            return '<div class="alert_red">Can\'t find any agent with this username</div>';
        }
    }
}

function reset_agent_password( $id, $new_pwd )
{
    global $db;

    $d = get_agent( $id );

    $s = 'UPDATE ticket_agent SET agpassword = %s WHERE agid = %d';
    $q = $db->prepare_query( $s, md5( $new_pwd ), $id );

    if( $db->do_query( $q ) )
    {
        return true;
    }
}

function reset_agent_password_email( $agemail, $agusername, $agname, $new_password )
{
    try
    {
        $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

        $mail = new Mandrill( $keys );

        $web_title  = get_meta_data('web_title');
        $email_from = get_meta_data('smtp_email_address');
        $email_data = get_reset_agent_password_email_data( $agemail, $agusername, $agname, $new_password );

        $message  = array(
            'subject'    => 'Reset Password: Your new password on' . $web_title,
            'from_name'  => $web_title,
            'from_email' => $email_from,
            'html'       => $email_data,
            'to' => array(
                array(
                    'email' => $agemail,
                    'name'  => '',
                    'type'  => 'to'
                )
            ),
            'headers' => array( 'Reply-To' => $email_from )
        );

        $async   = false;
        $result  = $mail->messages->send( $message, $async );

        if( isset( $result[0]['status'] ) )
        {
            if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    catch( Mandrill_Error $e )
    {
        return false;
    }
}

function get_reset_agent_password_email_data( $agemail, $agusername, $agname, $new_password )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/email/new-password-notification-to-agent.html', 'email' );

    add_block( 'email-block', 'e-block', 'email' );

    add_variable( 'agname', $agname );
    add_variable( 'agemail', $agemail );
    add_variable( 'agusername', $agusername );
    add_variable( 'agnewpwd', $new_password );
    add_variable( 'web_name', trim( web_name() ) );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'login_link', HTSERVER . site_url() . '/agent/login' );

    parse_template( 'email-block', 'e-block' );

    return return_template( 'email' );
}

function is_exist_agent( $username )
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM ticket_agent WHERE agusername = %s', $username );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_match_agent_password()
{
    global $db;

    $s = 'SELECT * FROM ticket_agent WHERE agusername=%s AND agpassword=%s AND agstatus = %s';
    $q = $db->prepare_query( $s, $_POST['username'], md5( $_POST['password'] ), 0 );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function fetch_agent( $username )
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM ticket_agent WHERE agusername = %s', $username );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return $db->fetch_array( $r );
    }
}

function do_agent_logout()
{
    setcookie( 'agid', '', false, '/' );
    setcookie( 'agtype', '', false, '/' );
    setcookie( 'agname', '', false, '/' );
    setcookie( 'agusername', '', false, '/' );
    setcookie( 'agpassword', '', false, '/' );
    setcookie( 'agentcookie', '', false, '/' );

    header( 'location:' . get_agent_url() . '/login' );
}

function get_agent_add_ons_content( $date, $trip_type, $sid = null, $bsource = null, $depart = null, $arrive = null, $exclude = false )
{
    global $db;

    $book = booking_item();
    $date = date( 'Y-m-d', strtotime( $date ) );
    $tval = base64_encode( json_encode( array( $sid, ( $exclude ? 0 : 1 ) ) ) );

    $s = 'SELECT * FROM ticket_add_ons AS a WHERE a.aostatus = %s AND a.aostart <= %s AND a.aoend >= %s';
    $q = $db->prepare_query( $s, '1', $date, $date );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $tprc = array( 'departure' => 0, 'return' => 0 );
        $list = '';

        while( $d = $db->fetch_array( $r ) )
        {
            //-- Check Schedule In Play
            if( is_null( $d['aosid'] ) )
            {
                $scheck = true;
            }
            else
            {
                $aosid = json_decode( $d['aosid'], true );
                $aosid = $aosid !== null && json_last_error() === JSON_ERROR_NONE ? $aosid : array();

                if( empty( $aosid ) )
                {
                    $scheck = true;
                }
                else
                {
                    if( in_array( $sid, $aosid ) )
                    {
                        $scheck = true;
                    }
                    else
                    {
                        $scheck = false;
                    }
                }
            }

            //-- Check Booking Source In Play
            if( is_null( $d['aobsource'] ) )
            {
                $bcheck = true;
            }
            else
            {
                $aobsource = json_decode( $d['aobsource'], true );
                $aobsource = $aobsource !== null && json_last_error() === JSON_ERROR_NONE ? $aobsource : array();

                if( empty( $aobsource ) )
                {
                    $bcheck = true;
                }
                else
                {
                    if( in_array( $bsource, $aobsource ) )
                    {
                        $bcheck = true;
                    }
                    else
                    {
                        $bcheck = false;
                    }
                }
            }

            //-- Check Point In Play
            if( empty( $d['aodepart'] ) && empty( $d['aoarrive'] ) )
            {
                $pcheck = true;
            }
            elseif( empty( $d['aodepart'] ) && !empty( $d['aoarrive'] ) )
            {
                $aoarrive = json_decode( $d['aoarrive'], true );
                $aoarrive = $aoarrive !== null && json_last_error() === JSON_ERROR_NONE ? $aoarrive : array();

                if( empty( $aoarrive ) )
                {
                    $pcheck = true;
                }
                else
                {
                    if( in_array( $arrive, $aoarrive ) )
                    {
                        $pcheck = true;
                    }
                    else
                    {
                        $pcheck = false;
                    }
                }
            }
            elseif( !empty( $d['aodepart'] ) && empty( $d['aoarrive'] ) )
            {
                $aodepart = json_decode( $d['aodepart'], true );
                $aodepart = $aodepart !== null && json_last_error() === JSON_ERROR_NONE ? $aodepart : array();

                if( empty( $aodepart ) )
                {
                    $pcheck = true;
                }
                else
                {
                    if( in_array( $depart, $aodepart ) )
                    {
                        $pcheck = true;
                    }
                    else
                    {
                        $pcheck = false;
                    }
                }
            }
            elseif( !empty( $d['aodepart'] ) && !empty( $d['aoarrive'] ) )
            {
                $aodepart = json_decode( $d['aodepart'], true );
                $aodepart = $aodepart !== null && json_last_error() === JSON_ERROR_NONE ? $aodepart : array();

                if( empty( $aodepart ) )
                {
                    $dcheck = true;
                }
                else
                {
                    if( in_array( $depart, $aodepart ) )
                    {
                        $dcheck = true;
                    }
                    else
                    {
                        $dcheck = false;
                    }
                }

                $aoarrive = json_decode( $d['aoarrive'], true );
                $aoarrive = $aoarrive !== null && json_last_error() === JSON_ERROR_NONE ? $aoarrive : array();

                if( empty( $aoarrive ) )
                {
                    $acheck = true;
                }
                else
                {
                    if( in_array( $arrive, $aoarrive ) )
                    {
                        $acheck = true;
                    }
                    else
                    {
                        $acheck = false;
                    }
                }

                if( $dcheck || $acheck )
                {
                    $pcheck = true;
                }
            }

            if( $scheck && $bcheck && $pcheck )
            {
                $notes    = '';
                $checked  = '';
                $quantity = 1;

                if( isset( $book['sess_data']['trip'][ $trip_type ]['addons'] ) )
                {
                    $found = array_search( $d['aoid'], array_combine( array_keys( $book['sess_data']['trip'][ $trip_type ]['addons'] ), array_column( $book['sess_data']['trip'][ $trip_type ]['addons'], 'id' ) ) );
                    
                    if( $found !== false )
                    {
                        $checked  = 'checked';
                        $quantity = $book['sess_data']['trip'][ $trip_type ]['addons'][ $found ]['paxs'];
                        $notes    = $book['sess_data']['trip'][ $trip_type ]['addons'][ $found ]['notes'];

                        $tprc[ $trip_type ] = $tprc[ $trip_type ] + $book['sess_data']['trip'][ $trip_type ]['addons'][ $found ]['price'] * $quantity;
                    }
                }

                $list .= '
                <li class="' . ( $exclude && $d['aotype'] == '1' ? 'sr-only' : '' ) . '">
                    <label>
                        <input class="add-ons-check" data-aotype="' . $d['aotype'] . '" data-trip="' . $trip_type . '" type="checkbox" name="addons[' . $trip_type . '][' . $d['aoid'] . '][id]" value="' . $d['aoid'] . '" ' . $checked . ' autocomplete="off" />
                        <span>' . $d['aoname'] . '</span>
                    </label>
                    <div class="row">
                        <div class="cols col-md-10">';

                            $picture = json_decode( $d['aopicture'], true );
                            $picture = $picture !== null && json_last_error() === JSON_ERROR_NONE ? $picture : array();

                            if( !empty( $picture ) && isset( $picture['medium'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $picture['medium'] ) )
                            {
                                $list .= '<img src="' . HTSERVER . site_url() . '/l-plugins/ticket/uploads/featured/' . $picture['medium'] . '" alt="Featured Image" />';
                            }

                            $list .= '
                            <div class="bx">
                                <p>' . $d['aobrief'] . '</p>';

                                if( !empty( $d['aodesc'] ) )
                                {
                                    $list .= '<a data-base-class="popup-add-ons" data-fancybox="addons-' . $sid . '-' . $d['aoid'] . '" data-src="#popup-add-ons-' . $sid . '-' . $d['aoid'] . '" href="javascript:;">View detail</a>';
                                }

                                $list .= '
                                <div class="clearfix">
                                    <textarea class="add-ons-notes" name="addons[' . $trip_type . '][' . $d['aoid'] . '][notes]" placeholder="Note here your choice/additional info..." row="2">' . $notes . '</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-12 ' . ( $d['aotype'] == '1' ? 'sr-only' : '' ) . '">
                            <div class="form-group">';

                                if( $d['aodiscount'] > 0 )
                                {
                                    $list .= '<p><b>Price :</b> <s style="color:red;">' . number_format( $d['aoprice'], 0, ',', '.' ) . '</s> &raquo; ' . number_format( ( $d['aoprice'] - $d['aodiscount'] ), 0, ',', '.' ) . '</p>';
                                }
                                else
                                {
                                    $list .= '<p><b>Price :</b> ' . number_format( $d['aoprice'], 0, ',', '.' ) . '</p>';
                                }

                                $list .= '
                                <input type="text" class="sr-only add-ons-price" name="addons[' . $trip_type . '][' . $d['aoid'] . '][price]" value="' . ( $d['aoprice'] - $d['aodiscount'] ) . '" />
                            </div>
                            <div class="form-group">          
                                <div class="input-group">
                                    <div class="input-group-addon">QTY</div>
                                    <div class="number-field">
                                        <input type="number" data-trip="' . $trip_type . '" class="text form-control text-right add-ons-pax" name="addons[' . $trip_type . '][' . $d['aoid'] . '][paxs]" value="' . $quantity . '" min="0" oninput="this.value = Math.abs(this.value)">
                                        <div class="number-nav">
                                            <div class="number-button up">+</div>
                                            <div class="number-button down">-</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="sr-only">
                            <div id="popup-add-ons-' . $sid . '-' . $d['aoid'] . '">
                                <h2>' . $d['aoname'] . '</h2>';

                                if( !empty( $picture ) && isset( $picture['large'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $picture['large'] ) )
                                {
                                    $list .= '
                                    <figure>
                                        <img src="' . HTSERVER . site_url() . '/l-plugins/ticket/uploads/featured/' . $picture['large'] . '" alt="Featured Image" />
                                    </figure>';
                                }

                                $list .= '
                                <article>
                                    ' . $d['aodesc'] . '
                                </article
                            </div>
                        </div>
                    </div>
                </li>';
            }
        }

        if( !empty( $list ) )
        {
            $content = '
            <div class="row row-eq-height row-transport">
                <div class="col-md-10 shadow">
                    <div class="row">
                        <div class="col col-md-12 add-ons">
                            <div class="text">
                                <div class="ao-title">Popular with our guests :</div>
                                <div class="ao-content">
                                    <ul>
                                        ' . $list . '
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-2 price">
                    <div class="add-ons-price">
                        <p class="addons-price-' . $trip_type . '">' . number_format( $tprc[ $trip_type ], 0, ',', '.' ) . '</p>
                    </div>
                </div>
            </div>';

            return $content;
        }
    }
}

function get_agent_pickup_drop_off_transport( $rid, $from, $to, $transport_type = 0, $sess = array(), $is_return = false )
{
    $content   = '';
    $dtrans    = get_pickup_drop_list_data( $rid, $from );
    $atrans    = get_pickup_drop_list_data( $rid, $to );
    $trip_type = $is_return ? 'return' : 'departure';    

    if( isset( $dtrans['pickup'] ) )
    {        
        $content .= '
        <div class="row row-eq-height row-transport">
            <div class="col-md-10 shadow">
                <div class="row">
                    <div class="col col-md-12 trans">
                        <div class="text">';

                            if( empty( $transport_type ) )
                            {
                                $content .= '
                                <p class="own-trans">
                                    <b>Pickup Address: </b>
                                    <em>Own Transport &nbsp;</em>
                                    <span class="text-danger">Latest check in 30 minutes prior to departure</span>
                                    <input type="text" class="sr-only" name="trans[' . $trip_type . '][pickup][type]" value="2" />
                                </p>';
                            }
                            else
                            {
                                $content .= get_agent_pickup_drop_off_transport_detail( 'pickup', $dtrans['pickup'], $sess, $is_return );
                            }

                            $content .= '
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-md-2 price">';

                if( empty( $transport_type ) || !isset( $sess['trip'] ) )
                {
                    $content .= '
                    <div class="pickup-trans trans">
                        <p>Additional Fee</p>
                        <input type="text" name="trans[' . $trip_type . '][pickup][trans-fee]" class="pickup-trans-fee sr-only" value="0" />
                    </div>';
                }
                else
                {
                    if( isset( $sess['trip'] ) )
                    {
                        $trip_data = $is_return ? $sess['trip']['return'] : $sess['trip']['departure'];
                        $trans_fee = isset( $trip_data['pickup']['transport_fee'] ) ? $trip_data['pickup']['transport_fee'] : 0;

                        $content .= '
                        <div class="pickup-trans trans">
                            <p>' . ( $trans_fee == 0 ? 'Additional Fee' : number_format( $trans_fee, 0, ',', '.' ) ) . '</p>
                            <input type="text" name="trans[' . $trip_type . '][pickup][trans-fee]" class="pickup-trans-fee sr-only" value="' . $trans_fee . '" />
                        </div>';
                    }
                }
                
                $content .= '
            </div>
        </div>';
    }

    if( isset( $atrans['drop-off'] ) )
    {        
        $content .= '
        <div class="row row-eq-height row-transport">
            <div class="col-md-10 shadow">
                <div class="row">
                    <div class="col col-md-12 trans">
                        <div class="text">';

                            if( empty( $transport_type ) )
                            {
                                $content .= '
                                <p class="own-trans">
                                    <b>Drop-off Address: </b>
                                    <em>Own Transport &nbsp;</em>
                                    <span class="text-danger">Latest check in 30 minutes prior to departure</span>
                                    <input type="text" class="sr-only" name="trans[' . $trip_type . '][drop-off][type]" value="2" />
                                </p>';
                            }
                            else
                            {
                                $content .= get_agent_pickup_drop_off_transport_detail( 'drop-off', $atrans['drop-off'], $sess, $is_return );

                            }

                            $content .= '
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-md-2 price">';            

                if( empty( $transport_type ) || !isset( $sess['trip'] ) )
                {
                    $content .= '
                    <div class="drop-off-trans trans">
                        <p>Additional Fee</p>
                        <input type="text" name="trans[' . $trip_type . '][drop-off][trans-fee]" class="drop-off-trans-fee sr-only" value="0" />
                    </div>';
                }
                else
                {
                    if( isset( $sess['trip'] ) )
                    {
                        $trip_data = $is_return ? $sess['trip']['return'] : $sess['trip']['departure'];
                        $trans_fee = isset( $trip_data['drop-off']['transport_fee'] ) ? $trip_data['drop-off']['transport_fee'] : 0;
                        
                        $content .= '
                        <div class="drop-off-trans trans">
                            <p>' . ( $trans_fee == 0 ? 'Additional Fee' : number_format( $trans_fee, 0, ',', '.' ) ) . '</p>
                            <input type="text" name="trans[' . $trip_type . '][drop-off][trans-fee]" class="drop-off-trans-fee sr-only" value="' . $trans_fee . '" />
                        </div>';
                    }
                }
                
                $content .= '
            </div>
        </div>';
    }

    return $content;
}

function get_agent_pickup_drop_off_transport_detail( $trans_type, $trans, $sess = array(), $is_return = false )
{
    extract( $sess['trip'] );

    //-- Get Transport Fee and Flight Text
    $flight = $trans_type == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
    $pass   = $sess['adult_num'] + $sess['child_num'] + $sess['infant_num'];
    $count  = ceil( $pass / 4 );

    if( $is_return )
    {
        $ttype   = isset( $return[$trans_type]['transport_type'] ) ? $return[$trans_type]['transport_type'] : 0;
        $ftime   = isset( $return[$trans_type]['flight_time'] ) ? $return[$trans_type]['flight_time'] : '';
        $hid     = isset( $return[$trans_type]['hotel_id'] ) ? $return[$trans_type]['hotel_id'] : '';
        $taid    = isset( $return[$trans_type]['area_id'] ) ? $return[$trans_type]['area_id'] : '';
        $airport = isset( $return[$trans_type]['airport'] ) ? $return[$trans_type]['airport'] : 0;
        $rpfrom  = isset( $return[$trans_type]['rpfrom'] ) ? $return[$trans_type]['rpfrom'] : '';
        $rpto    = isset( $return[$trans_type]['rpto'] ) ? $return[$trans_type]['rpto'] : '';
        $hstatus = empty( $taid ) ? 'disabled' : '';
        $astatus = $ttype == 2 ? 'disabled' : '';

        $area_option  = get_agent_area_option_by_trip( $taid, $trans, $count, $ttype );
        $hotel_option = get_agent_hotel_option_by_trip( $hid, $trans, $count, $ttype );
        // <option value="0" ' . ( $ttype == 0 ? 'selected' : '' ) . '>Shared Transport</option>
        $content = '
        <p>
            <b>' . ucfirst( $trans_type ) . ' Address: </b>
            <select class="select-option transport-type" name="trans[return][' . $trans_type . '][type]" autocomplete="off">
                <option value="0" ' . ( $ttype == 0 ? 'selected' : '' ) . '>Shared Transport</option>
                <option value="1" ' . ( $ttype == 1 ? 'selected' : '' ) . '>Private Transport</option>
                <option value="2" ' . ( $ttype == 2 ? 'selected' : '' ) . '>Own Transport</option>
            </select>
            <select class="select-option hotels-area required" name="trans[return][' . $trans_type . '][taid]" autocomplete="off" data-error="Area can\'t be empty" ' . $astatus . '>
                <option value="" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="0" data-fee="0" data-fee-num="Free">No Accommodation Booked</option>
                ' . $area_option . '
            </select>
            <select class="select-option hotels required" name="trans[return][' . $trans_type . '][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" ' . $hstatus . '>
                ' . $hotel_option . '
            </select>
            <span class="flight-time ' . ( $airport == 1 ? '' : 'sr-only' ) . '">
                <b>' . $flight . ' : </b>
                <input type="text" class="trans-ftime" name="trans[return][' . $trans_type . '][ftime]" value="' . $ftime . '" readonly="readonly" />
                <input type="text" class="trans-airport sr-only" name="trans[return][' . $trans_type . '][taairport]" value="' . $airport . '" readonly="readonly" />
            </span>
            <input type="text" name="trans[return][' . $trans_type . '][rpto]"  class="trans-rpto sr-only" value="" />
            <input type="text" name="trans[return][' . $trans_type . '][rpfrom]"  class="trans-rpfrom sr-only" value="" />
            <input type="text" name="trans[return][' . $trans_type . '][trip-type]"  class="trip-type sr-only" value="return" />
            <input type="text" name="trans[return][' . $trans_type . '][trans-type]"  class="trans-type sr-only" value="' . $trans_type . '" />
        </p>';
    }
    else
    {
        $ttype   = isset( $departure[$trans_type]['transport_type'] ) ? $departure[$trans_type]['transport_type'] : 0;
        $ftime   = isset( $departure[$trans_type]['flight_time'] ) ? $departure[$trans_type]['flight_time'] : '';
        $hid     = isset( $departure[$trans_type]['hotel_id'] ) ? $departure[$trans_type]['hotel_id'] : '';
        $taid    = isset( $departure[$trans_type]['area_id'] ) ? $departure[$trans_type]['area_id'] : '';
        $airport = isset( $departure[$trans_type]['airport'] ) ? $departure[$trans_type]['airport'] : 0;
        $rpfrom  = isset( $departure[$trans_type]['rpfrom'] ) ? $departure[$trans_type]['rpfrom'] : '';
        $rpto    = isset( $departure[$trans_type]['rpto'] ) ? $departure[$trans_type]['rpto'] : '';
        $hstatus = empty( $taid ) ? 'disabled' : '';
        $astatus = $ttype == 2 ? 'disabled' : '';

        $area_option  = get_agent_area_option_by_trip( $taid, $trans, $count );
        $hotel_option = get_agent_hotel_option_by_trip( $hid, $trans, $count );
        // <option value="0" ' . ( $ttype == 0 ? 'selected' : '' ) . '>Shared Transport</option>
        $content = '
        <p>
            <b>' . ucfirst( $trans_type ) . ' Address: </b>
            <select class="select-option transport-type" name="trans[departure][' . $trans_type . '][type]" autocomplete="off">
                <option value="0" ' . ( $ttype == 0 ? 'selected' : '' ) . '>Shared Transport</option>
                <option value="1" ' . ( $ttype == 1 ? 'selected' : '' ) . '>Private Transport</option>
                <option value="2" ' . ( $ttype == 2 ? 'selected' : '' ) . '>Own Transport</option>
            </select>
            <select class="select-option hotels-area required" name="trans[departure][' . $trans_type . '][taid]" autocomplete="off" data-error="Area can\'t be empty" ' . $astatus . '>
                <option value="" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="0" data-fee="0" data-fee-num="Free">No Accommodation Booked</option>
                ' . $area_option . '
            </select>
            <select class="select-option hotels required" name="trans[departure][' . $trans_type . '][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" ' . $hstatus . '>
                ' . $hotel_option . '
            </select>
            <span class="flight-time ' . ( $airport == 1 ? '' : 'sr-only' ) . '">
                <b>' . $flight . ' : </b>
                <input type="text" class="trans-ftime" name="trans[departure][' . $trans_type . '][ftime]" value="' . $ftime . '" readonly="readonly" />
                <input type="text" class="trans-airport sr-only" name="trans[departure][' . $trans_type . '][taairport]" value="' . $airport . '" readonly="readonly" />
            </span>
            <input type="text" name="trans[departure][' . $trans_type . '][trip-type]" class="trip-type sr-only" value="departure" />
            <input type="text" name="trans[departure][' . $trans_type . '][rpto]"  class="trans-rpto sr-only" value="' . $rpto . '" />
            <input type="text" name="trans[departure][' . $trans_type . '][rpfrom]"  class="trans-rpfrom sr-only" value="' . $rpfrom . '" />
            <input type="text" name="trans[departure][' . $trans_type . '][trans-type]"  class="trans-type sr-only" value="' . $trans_type . '" />
        </p>';
    }

    return $content;
}

function get_agent_area_option_by_trip( $taid, $trans, $count = 0, $ttype = 0 )
{
    $options   = '';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        foreach( $trans as $d )
        {
            $fee      = $d['tafee'] * $count;
            $fee_num  = number_format( $fee, 0, ',', '.' );
            $selected = $taid == $d['taid'] ? 'selected' : '';

            $options .= '
            <option value="' . $d['taid'] . '"
                data-supporttrans="' . $d['tatypeforagent'] . '"
                data-rpfrom="' . $d['rpfrom'] . '"
                data-rpto="' . $d['rpto'] . '"
                data-area-id="' . $d['taid'] . '"
                data-area="' . $d['taname'] . '"
                data-airport="' . $d['taairport'] . '"
                data-fee="' . $fee . '"
                data-fee-num="' . $fee_num . '" ' . $selected . '>
                ' . $d['taname'] . '
            </option>';
        }

        return $options;
    }
}

function get_agent_hotel_option_by_trip( $hid, $trans, $count = 0, $ttype = 0 )
{
    global $db;

    $options   = '
    <option value="1" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="" data-address="" data-phone="" data-email="" data-fee="0" data-fee-num="Free" ' . ( $hid == 1  ? 'selected' : '' ) . '>Not in list/To be Advised</option>
    <!--option value="2" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="" data-address="" data-phone="" data-email="" data-fee="0" data-fee-num="Free" ' . ( $hid == 2 ? 'selected' : '' ) . '>Other Hotel</option-->';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        $rpfrom = array();
        $rpto   = array();
        $area   = array();
        $lcid   = '';

        foreach( $trans as $t )
        {
            $rpfrom[ $t['taid'] ] = $t['rpfrom'];
            $rpto[ $t['taid'] ]   = $t['rpto'];

            $area[] = $t['taid'];
            $lcid   = $t['lcid'];
        }

        $s = 'SELECT * FROM ticket_hotel AS a
              LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
              LEFT JOIN ticket_transport_area_fee AS c ON c.taid = b.taid
              WHERE a.hstatus = %d AND c.lcid = %s AND a.taid IN ( ' . implode( ',', $area ) . ' ) ORDER BY a.hname';
        $q = $db->prepare_query( $s, 1, $lcid );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            $fee      = $d['tafee'] * $count;
            $fee_num  = number_format( $fee, 0, ',', '.' );
            $selected = $hid == $d['hid'] ? 'selected' : '';

            $options .= '
            <option value="' . $d['hid'] . '"
                data-supporttrans="' . $d['tatypeforagent'] . '"
                data-rpfrom="' . $rpfrom[ $d['taid'] ] . '"
                data-rpto="' . $rpto[ $d['taid'] ] . '"
                data-area-id="' . $d['taid'] . '"
                data-area="' . $d['taname'] . '"
                data-airport="' . $d['taairport'] . '"
                data-fee="' . $fee . '"
                data-fee-num="' . $fee_num . '" ' . $selected . '
                data-address="' . $d['haddress'] . '"
                data-phone="' . $d['hphone'] . '"
                data-email="' . $d['hemail'] . '">
                ' . $d['hname'] . '
            </option>';
        }

        return $options;
    }
}

function get_agent_url()
{
	return HTSERVER . site_url() . '/agent';
}

function get_agent_admin_url( $state = '' )
{
    return HTSERVER . site_url() . '/agent/admin/' . ( empty( $state ) ? '' : '?state=' . $state );
}

function get_agent_filter_booking()
{
    $filter = array( 'rid' => '', 'lcid' => '', 'lcid2' => '', 'chid' => '', 'status' => ( isset( $_GET['sub'] ) && $_GET['sub'] == 'canceled' ? 'cr' : '' ), 'date_start' => '', 'date_end' => '', 'search' => '', 'sub' => '' );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array( 'rid' => $rid, 'lcid' => $lcid, 'lcid2' => $lcid2, 'chid' => $chid, 'status' => $bstatus, 'date_start' => $date_start, 'date_end' => $date_end, 'search' => $sbooking, 'sub' => $sub  );
    }

    return $filter;
}

function get_agent_outstanding_report_filter()
{
    $filter = array( 'bticket' => '', 'date_start' => '', 'date_end' => '', 'atstatus' => '', 'atdatetype' => '' );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $filter = array( 'bticket' => $bticket, 'date_start' => $date_start, 'date_end' => $date_end, 'atstatus' => $atstatus, 'atdatetype' => $atdatetype );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $filter = array( 'bticket' => $bticket, 'date_start' => $date_start, 'date_end' => $date_end, 'atstatus' => $atstatus, 'atdatetype' => $atdatetype );
    }

    return $filter;
}

function get_filter_agent_promo_code_report( $agid )
{
    $agent  = get_agent( $agid );
    $filter = array( 'promo_code' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ), 'rbased' => '0', 'rtype' => '1', 'bdate' => date( 'F Y', time() ), 'bdateto' => '', 'chid' => $agent['chid'] . '|' . $agent['agid'], 'agentpanel' => 1 );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $pmcode  = isset( $pmcode ) ? $pmcode : '';

        $filter  = array( 'promo_code' => $pmcode, 'bstatus' => $bstatus, 'rbased' => $rbased, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'chid' => $agent['chid'] . '|' . $agent['agid'], 'agentpanel' => 1 );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $pmcode  = isset( $pmcode ) ? $pmcode : '';

        $filter  = array( 'promo_code' => $pmcode, 'bstatus' => $bstatus, 'rbased' => $rbased, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'chid' => $agent['chid'] . '|' . $agent['agid'], 'agentpanel' => 1 );
    }

    return $filter;
}

function get_agent_rate_price_content( $rid, $adult = 0, $child = 0, $infant = 0, $trip = array(), $return = false )
{
    $type    = $return ? 'return' : 'departure';
    $content = '';

    extract( $trip );

    if( !empty( $adult ) )
    {
        if( empty( $adult_price ) )
        {
            $content .= '<p><span class="ptxt">Free</span></p>';
        }
        else
        {
            $content .= '<p><span class="ptxt">IDR ' . number_format( $adult_price, 0, ',', '.' ) . '/adult</span></p>';
        }
    }

    if( !empty( $child ) )
    {
        if( empty( $child_price ) )
        {
            $content .= '<p><span class="ptxt">Free</span></p>';
        }
        else
        {
            $content .= '<p><span class="ptxt">IDR ' . number_format( $child_price, 0, ',', '.' ) . '/child</span></p>';
        }
    }

    if( !empty( $infant ) )
    {
        if( empty( $infant_price ) )
        {
            $content .= '<p><span class="ptxt">Free</span></p>';
        }
        else
        {
            $content .= '<p><span class="ptxt">IDR ' . number_format( $infant_price, 0, ',', '.' ) . '/infant</span></p>';
        }
    }

    return $content;
}

function get_agent_report_date_option( $atdatetype = '' )
{
    $option = '
    <option value="1" ' . ( $atdatetype == 1 ? 'selected' : '' ) . ' >Booking Date</option>
    <option value="2" ' . ( $atdatetype == 2 ? 'selected' : '' ) . ' >Travel Date</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Report Option
| -------------------------------------------------------------------------------------
*/
function get_agent_report_status_option( $atstatus = '', $use_empty = true, $empty_text = 'Select Status' )
{
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="1" ' . ( $atstatus == 1 ? 'selected' : '' ) . ' >Confirmed</option>
    <option value="2" ' . ( $atstatus == 2 ? 'selected' : '' ) . ' >Paid Balance</option>
    <option value="3" ' . ( $atstatus == 3 ? 'selected' : '' ) . ' >Canceled, cancelation fee applied</option>';

    return $option;
}

function is_agent_login()
{
	$cek_url = cek_url();

	if( $cek_url[0] == 'agent' && isset( $cek_url[1] ) && $cek_url[1] == 'login' )
	{
		add_actions( 'meta_title', get_meta_title( 'Agent - Login' ) );
        add_actions( 'meta_keywords', get_meta_keywords( 'Agent - Login' ) );
        add_actions( 'meta_description', get_meta_description( 'Agent - Login' ) );

		return true;
	}
	else
	{
		return false;
	}
}

function is_agent_forget_password()
{
	$cek_url = cek_url();

	if( $cek_url[0] == 'agent' && isset( $cek_url[1] ) && $cek_url[1] == 'forget-password' )
	{
        add_actions( 'meta_title', get_meta_title( 'Agent - Forget Password' ) );
        add_actions( 'meta_keywords', get_meta_keywords( 'Agent - Forget Password' ) );
        add_actions( 'meta_description', get_meta_description( 'Agent - Forget Password' ) );
		return true;
	}
	else
	{
		return false;
	}
}

function is_agent_admin()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $cek_url[1] ) && $cek_url[1] == 'admin' )
    {
        if( isset( $_GET['state'] ) )
        {
            $state = ' - ' . ucfirst( $_GET['state'] );

            add_variable( 'reservation_sub_class', $_GET['state'] == 'reservation' ? '' : 'display:none;' );
            add_variable( 'profile_setting_sub_class', $_GET['state'] == 'profile-setting' ? '' : 'display:none;' );

            add_actions( 'meta_title', get_meta_title( 'Admin Panel' . $state ) );
            add_actions( 'meta_keywords', get_meta_keywords( 'Admin Panel' . $state ) );
            add_actions( 'meta_description', get_meta_description( 'Admin Panel' . $state ) );
        }
        else
        {
            add_actions( 'meta_title', get_meta_title( 'Admin Panel' ) );
            add_actions( 'meta_keywords', get_meta_keywords( 'Admin Panel' ) );
            add_actions( 'meta_description', get_meta_description( 'Admin Panel' ) );
        }

        add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/menu.css' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        return true;
    }
    else
    {
        return false;
    }
}

function is_api()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'api' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_v1()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'api' && isset( $cek_url[1] ) && $cek_url[1] == 'v1' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_v2()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'api' && isset( $cek_url[1] ) && $cek_url[1] == 'v2' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_dashboard()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'dashboard' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_report()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'report' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_outstanding_report()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'report' )
    {
        if( isset( $_GET['sub'] ) && $_GET['sub'] == 'outstanding-report' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function is_agent_promo_report()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'report' )
    {
        if( isset( $_GET['sub'] ) && $_GET['sub'] == 'promo-code-report' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function is_agent_report_filter_view()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'report' && isset( $_POST['filter'] ) || isset( $_GET['filter'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_availability()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'availability' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_reservation( $sub = '' )
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'reservation' )
    {
        if( empty( $sub ) )
        {
            return true;
        }
        else
        {
            if( isset( $_GET['sub'] ) && $_GET['sub'] == $sub )
            {
                return true;
            }
        }

        return false;
    }
    else
    {
        return false;
    }
}

function is_agent_view_booking()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'reservation' )
    {
        if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'detail' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function is_agent_edit_booking()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'reservation' )
    {
        if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'edit' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function is_agent_popup_detail_booking()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'reservation' )
    {
        if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'popup-detail' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function is_agent_popup_edit_booking()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'reservation' )
    {
        if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'popup-edit' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function is_agent_profile_setting( $sub = '' )
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'profile-setting' )
    {
        if( empty( $sub ) )
        {
            return true;
        }
        else
        {
            if( isset( $_GET['sub'] ) && $_GET['sub'] == $sub )
            {
                return true;
            }
        }

        return false;
    }
    else
    {
        return false;
    }
}

function is_agent_add_new()
{
    if( is_agent_admin() && isset( $_GET['prc'] ) && $_GET['prc'] == 'add-new' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_edit()
{
    if( is_agent_admin() && isset( $_GET['prc'] ) && $_GET['prc'] == 'edit' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_delete()
{
    if( is_agent_admin() && isset( $_GET['prc'] ) && $_GET['prc'] == 'delete' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_delete_all()
{
    if( is_agent_admin() && isset( $_POST['delete'] ) && $_POST['delete'] == 'Delete' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_publish()
{
    if( is_agent_admin() && isset( $_POST['publish'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_confirm_delete()
{
    if( is_agent_admin() && isset( $_POST['confirm_delete'] ) && $_POST['confirm_delete'] == 'Yes' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_agent_admin_ajax()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'agent' && isset( $_GET['state'] ) && $_GET['state'] == 'ajax-request' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function agent_admin_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_agent_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        extract( $_POST );

        $data = agent_reservation_table_query( $chid, $lcid, $lcid2, $rid, $date_start, $date_end, $status, $sbooking, $sub );

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-agent-allotment' )
    {
        $data = get_agent_allotment( $_POST['month'], $_POST['year'], $_POST['sid'], $_POST['agid'] );

        if( !empty( $data ) )
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'agent-reservation-show-detail' )
    {
        echo agent_reservation_show_detail( $_POST['bid'] );
    }


    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'agent-request-cancelation' )
    {
        $dt = ticket_booking_detail_by_id( $_POST['bdid'] );

        if( empty( $dt ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            $sctime = strtotime( date( 'Y-m-d H:i:s' ) );
            $sdtime = strtotime( $dt['bddate'] . ' ' . $dt['bddeparttime'] );

            if( $sctime < $sdtime )
            {
                $current_time = date( 'Y-m-d H:i:s', $sctime );
                $depart_time  = date( 'Y-m-d H:i:s', $sdtime );

                $ctime = new DateTime( $current_time );
                $dtime = new DateTime( $depart_time );

                $interval = $ctime->diff( $dtime );

                if( $interval->days > 7 || ( $interval->days == 7 && $interval->h >= 12 ) )
                {
                    $res = booking_cancel_ticket( $_POST );

                    if( $res != 0 )
                    {
                        send_booking_cancelation_request_to_admin( $res, 'booking-cancel', 'ticket' );
                        send_booking_cancelation_notif_to_client( $res, 'booking-cancel', 'ticket' );

                        $d = ticket_booking_data( $_POST['bid'] );

                        echo json_encode( array( 'result' => 'success', 'status' => ticket_booking_status( $d['bstatus'] ) ) );
                    }
                    else
                    {
                        echo json_encode( array( 'result' => 'failed' ) );
                    }
                }
                else
                {
                    if( ticket_booking_cancel_request( $_POST ) )
                    {
                        $d = ticket_booking_data( $_POST['bid'] );

                        echo json_encode( array( 'result' => 'success', 'status' => ticket_booking_status( $d['bstatus'] ) ) );
                    }
                    else
                    {
                        echo json_encode( array( 'result' => 'failed' ) );
                    }
                }
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'add-new-addons' )
    {
        echo add_new_addons_content( $_POST['bdid'] );
    }

    exit;
}

?>
