<?php

use \Firebase\JWT\JWT;

function set_api_request()
{
    header('Content-type: application/json; charset=utf-8');
    
	$req = get_uri_sef();

	if( isset( $_POST['token'] ) && empty( $_POST['token'] ) === false )
	{
		try
		{
			$data = JWT::decode( $_POST['token'], 'gilibookings', [ 'HS256' ] );

            if( isset( $data->live ) && $data->live === true )
            {
    			if( $req == 'check-availability' )
    			{
    				$result = api_availability( $data );

    				echo json_encode( $result );
    			}
    			else if( $req == 'select-trip' )
    			{
    				$result = api_select_trip( $data );

    				echo json_encode( $result );
    			}
    			else if( $req == 'add-booking' )
    			{
    				$result = api_add_booking( $data );

    				echo json_encode( $result );
    			}
            }
            else
            {
                echo json_encode( array( 'result' => 'failed', 'message' => 'Not valid token' ) );
            }
		}
		catch( \Exception $e )
		{
			echo json_encode( array( 'result' => 'failed', 'message' => $e->getMessage() ) );
		}
	}
	else
	{
		echo json_encode( array( 'result' => 'failed', 'message' => 'Not valid token' ) );
	}

	exit;
}

function api_availability( $data )
{
	extract( $_POST );

	if( $type_of_route == 1 )
	{
		$fields = array(
			'token',
			'type_of_route', 
			'date_of_depart',
			'date_of_return',
			'depart_port_to',
			'return_port_to',
			'depart_port_from',
			'return_port_from'
		);
	}
	else
	{
		$fields = array(
			'token',
			'type_of_route', 
			'date_of_depart', 
			'depart_port_to', 
			'depart_port_from',
		);
	}

	foreach( $_POST as $key => $value )
	{
		if( in_array( $key, $fields ) )
		{
			if( ( $idx = array_search( $key, $fields ) ) !== false )
			{
			    unset( $fields[ $idx ] );
			}
		}
	}
	
	if( empty( $fields ) === false )
	{
		return array( 'result' => 'failed', 'message' => 'You must set value for this parameter : ' . implode( ', ', $fields ) );
	}
	else
	{
		if( isset( $date_of_depart ) && strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
	    {
	    	return array( 'result' => 'failed', 'message' => 'Departure day may not be greater than today' );
	    }
	    else
	    {
		    $port_type        = get_location( $depart_port_from, 'lctype' );
		    $agent            = get_agent( $data->agid );

			$date_of_return   = isset( $date_of_return ) ? $date_of_return : '';
		    $return_port_from = isset( $return_port_from ) ? $return_port_from : '';
		    $return_port_to   = isset( $return_port_to ) ? $return_port_to : '';

		    $depart_route     = get_location( $depart_port_from, 'lcname' ) . ' to ' . get_location( $depart_port_to, 'lcname' );
		    $return_route     = '';

		    if( empty( $return_port_from ) == false && empty( $return_port_to ) == false )
		    {
		    	$return_route = get_location( $return_port_from, 'lcname' ) . ' to ' . get_location( $return_port_to, 'lcname' );
		    }

		    $param = array( 
		        'port_type'            => $port_type,
		        'type_of_route'        => $type_of_route,
		        'date_of_depart'       => $date_of_depart,
		        'date_of_return'       => $date_of_return,
		        'destination_port'     => $depart_port_to,
		        'destination_port_rtn' => $return_port_to,
		        'depart_port'          => $depart_port_from,
		        'return_port'          => $return_port_from,
		        'depart_route'         => $depart_route,
		        'return_route'         => $return_route,
		        'chid'                 => $agent['chid'],
		        'agid'                 => $agent['agid']
		    );

			$param[ 'availability' ][ 'departure' ] = api_departure_availability_result( $param, $agent['agid'] );

		    if( empty( $date_of_return ) === false )
		    {
		    	$param[ 'availability' ][ 'return' ] = api_return_availability_result( $param, $agent['agid'] );
		    }

		    return array( 'result' => 'success', 'data' => $param );
	    }
	}
}

function api_departure_availability_result( $post, $agid = '' )
{
    global $db;

    extract( $post );

    $dep_date = date( 'Y-m-d', strtotime( $date_of_depart ) );
    $api_data = array();

    $s = 'SELECT * FROM ticket_schedule AS a
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
    $q = $db->prepare_query( $s, $dep_date, $dep_date, 'publish', 'publish', '1' );
    $r = $db->do_query( $q );
    
    if( $db->num_rows( $r ) > 0 )
    {
    	$i = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $not_closed_date   = is_date_not_in_close_allotment_list( $agid, $d['sid'], $dep_date );
            $available_depart  = is_available_depart_on_list( $d['rid'], $depart_port, $dep_date );
            $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_port );
            $not_pass_time     = is_not_pass_time_limit( $d['rid'], $post, 'depart' );
            $available_date    = is_available_date_on_list( $d['sid'], $dep_date );
            
            if( $available_depart && $available_arrival && $available_date && $not_pass_time )
            {
            	$seat = api_remain_seat( $dep_date, $d['sid'], $d['rid'], $d['bopassenger'], $depart_port, $destination_port, $agid );

            	if( $seat > 0 )
            	{
            		$p = get_result_price( $d['sid'], $d['rid'], $dep_date, $depart_port, $destination_port, 0, 0, 0, $agid );

	    			$inc_price = array_sum( array_map( function( $a ){ return $a['price']; }, $p['inc_trans'] ) );
	    			$exc_price = array_sum( array_map( function( $a ){ return $a['price']; }, $p['exc_trans'] ) );

            		if( empty( $inc_price ) === false && $not_closed_date )
        			{
                        if( empty( $p['early_discount'] ) === false || empty( $p['seat_discount'] ) === false )
                        {
                            if( empty( $p['early_discount'] ) === false )
                            {
                                if( $p['early_discount']['type'] == '0' )
                                {
                                    $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                    $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                    $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];
                                }
                                else
                                {
                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                }

                                $discount_notif = empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'];
                            }

                            if( empty( $p['seat_discount'] ) === false )
                            {
                                if( $p['seat_discount']['type'] == '0' )
                                {
                                    $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                    $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                    $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];
                                }
                                else
                                {
                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                }

                                $discount_notif = empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'];
                            }
                        }
                        else
                        {
                            $inc_disc_adult  = 0;
                            $inc_disc_child  = 0;
                            $inc_disc_infant = 0;
                        	$discount_notif  = '';
                        }

                        $adult_price  = empty( $p['inc_trans']['adult']['price'] ) ? 'Free' : number_format( $p['inc_trans']['adult']['price'], 0, ',', '.' );
                        $child_price  = empty( $p['inc_trans']['child']['price'] ) ? 'Free' : number_format( $p['inc_trans']['child']['price'], 0, ',', '.' );
                        $infant_price = empty( $p['inc_trans']['infant']['price'] ) ? 'Free' : number_format( $p['inc_trans']['infant']['price'], 0, ',', '.' );

                        $route_list = get_route_detail_list_content( $d['rid'], $depart_port, $destination_port, true );
                        $trip_value = base64_encode( implode( '|', array(
                            1, 
                            $d['rid'], 
                            $d['sid'], 
                            $d['boid'], 
                            $p['rtdid'], 
                            $p['inc_trans']['adult']['price'], 
                            $p['inc_trans']['child']['price'], 
                            $p['inc_trans']['infant']['price'], 
                            $p['inc_trans']['adult']['selling_price'], 
                            $p['inc_trans']['child']['selling_price'], 
                            $p['inc_trans']['infant']['selling_price'],
                            ( isset( $inc_disc_adult ) ? $inc_disc_adult : 0 ), 
                            ( isset( $inc_disc_child ) ? $inc_disc_child : 0 ), 
                            ( isset( $inc_disc_infant ) ? $inc_disc_infant : 0 )
                        )));

                        $api_data[ $i ][ 'seat' ] = $seat;
                        $api_data[ $i ][ 'list' ][ 'include_trans' ] = array(
                            'trans_type'   => 1,
                            'route_list'   => $route_list,
                            'trip_value'   => $trip_value,
                            'boat_name'    => $d['boname'],
                            'adult_price'  => $adult_price, 
                            'child_price'  => $child_price, 
                            'infant_price' => $infant_price,
                            'disc_notif'   => $discount_notif,
                            'transport'    => array( array( 'name' => 'Shared ( Free )' ), array( 'name' => 'Private ( Additional )' ) ),
                        );
        			}

        			if( empty( $exc_price ) === false && $not_closed_date )
        			{
                        if( empty( $p['early_discount'] ) === false || empty( $p['seat_discount'] ) === false )
                        {
                            if( empty( $p['early_discount'] ) === false )
                            {
                                if( $p['early_discount']['type'] == '0' )
                                {
                                    $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                    $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                    $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];
                                }
                                else
                                {
                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                }

                                $discount_notif = empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'];
                            }

                            if( empty( $p['seat_discount'] ) === false )
                            {
                                if( $p['seat_discount']['type'] == '0' )
                                {
                                    $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                    $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                    $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                }
                                else
                                {
                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                }

                                $discount_notif = empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'];
                            }
                        }
                        else
                        {
                            $exc_disc_adult  = 0;
                            $exc_disc_child  = 0;
                            $exc_disc_infant = 0;
                            $discount_notif  = '';
                        }

                        $adult_price  = empty( $p['exc_trans']['adult']['price'] ) ? 'Free' : number_format( $p['exc_trans']['adult']['price'], 0, ',', '.' );
                        $child_price  = empty( $p['exc_trans']['child']['price'] ) ? 'Free' : number_format( $p['exc_trans']['child']['price'], 0, ',', '.' );
                        $infant_price = empty( $p['exc_trans']['infant']['price'] ) ? 'Free' : number_format( $p['exc_trans']['infant']['price'], 0, ',', '.' );

                        $route_list = get_route_detail_list_content( $d['rid'], $depart_port, $destination_port, true );
                        $trip_value = base64_encode( implode( '|', array(
                            1, 
                            $d['rid'], 
                            $d['sid'], 
                            $d['boid'], 
                            $p['rtdid'], 
                            $p['exc_trans']['adult']['price'], 
                            $p['exc_trans']['child']['price'], 
                            $p['exc_trans']['infant']['price'], 
                            $p['exc_trans']['adult']['selling_price'], 
                            $p['exc_trans']['child']['selling_price'], 
                            $p['exc_trans']['infant']['selling_price'],
                            ( isset( $exc_disc_adult ) ? $exc_disc_adult : 0 ), 
                            ( isset( $exc_disc_child ) ? $exc_disc_child : 0 ), 
                            ( isset( $exc_disc_infant ) ? $exc_disc_infant : 0 )
                        )));

                        $api_data[ $i ][ 'seat' ] = $seat;
                        $api_data[ $i ][ 'list' ][ 'exclude_trans' ] = array(
                            'trans_type'   => 0,
                            'route_list'   => $route_list,
                            'trip_value'   => $trip_value,
                            'boat_name'    => $d['boname'],
                            'adult_price'  => $adult_price, 
                            'child_price'  => $child_price, 
                            'infant_price' => $infant_price, 
                            'disc_notif'   => $discount_notif,
                            'transport'    => array( array( 'name' => 'Excluded' ) )
                        );
        			}

                    $i++;
            	}
            }
        }

        if( empty( $i )  )
        {
        	return array( 'Ooops, sold out for that date and route! If your dates are flexible please try day before or after.' );
        }
        else
        {
    		return $api_data;
        }
    }
    else
    {
        return array( 'Ooops, sold out for that date and route! If your dates are flexible please try day before or after.' );
    }
}

function api_return_availability_result( $post, $agid = '' )
{
    global $db;

    extract( $post );

    $rtn_date = date( 'Y-m-d', strtotime( $date_of_return ) );
    $api_data = array();

	$s = 'SELECT * FROM ticket_schedule AS a
		  LEFT JOIN ticket_route AS b ON a.rid = b.rid
		  LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
	$q = $db->prepare_query( $s, $rtn_date, $rtn_date, 'publish', 'publish', '1' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
	    $i = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $not_closed_date   = is_date_not_in_close_allotment_list( $agid, $d['sid'], $rtn_date );
            $available_depart  = is_available_depart_on_list( $d['rid'], $return_port, $rtn_date );
            $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_port_rtn );
            $not_pass_time     = is_not_pass_time_limit( $d['rid'], $post, 'return' );
            $available_date    = is_available_date_on_list( $d['sid'], $rtn_date );
            
            if( $available_depart && $available_arrival && $available_date && $not_pass_time )
            {
            	$seat = api_remain_seat( $rtn_date, $d['sid'], $d['rid'], $d['bopassenger'], $return_port, $destination_port_rtn, $agid );

            	if( $seat > 0 )
            	{
            		$p = get_result_price( $d['sid'], $d['rid'], $rtn_date, $return_port, $destination_port_rtn, 0, 0, 0, $agid );

	    			$inc_price = array_sum( array_map( function( $a ){ return $a['price']; }, $p['inc_trans'] ) );
	    			$exc_price = array_sum( array_map( function( $a ){ return $a['price']; }, $p['exc_trans'] ) );

            		if( empty( $inc_price ) === false && $not_closed_date )
        			{
                        if( empty( $p['early_discount'] ) === false || empty( $p['seat_discount'] ) === false )
                        {
                            if( empty( $p['early_discount'] ) === false )
                            {
                                if( $p['early_discount']['type'] == '0' )
                                {
                                    $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                    $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                    $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];
                                }
                                else
                                {
                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                }

                                $discount_notif = empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'];
                            }

                            if( empty( $p['seat_discount'] ) === false )
                            {
                                if( $p['seat_discount']['type'] == '0' )
                                {
                                    $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                    $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                    $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];
                                }
                                else
                                {
                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                }

                                $discount_notif = empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'];
                            }
                        }
                        else
                        {
                            $inc_disc_adult  = 0;
                            $inc_disc_child  = 0;
                            $inc_disc_infant = 0;
                            $discount_notif  = '';
                        }

                        $adult_price  = empty( $p['inc_trans']['adult']['price'] ) ? 'Free' : number_format( $p['inc_trans']['adult']['price'], 0, ',', '.' );
                        $child_price  = empty( $p['inc_trans']['child']['price'] ) ? 'Free' : number_format( $p['inc_trans']['child']['price'], 0, ',', '.' );
                        $infant_price = empty( $p['inc_trans']['infant']['price'] ) ? 'Free' : number_format( $p['inc_trans']['infant']['price'], 0, ',', '.' );

                        $route_list = get_route_detail_list_content( $d['rid'], $return_port, $destination_port_rtn, true );
                        $trip_value = base64_encode( implode( '|', array(
                            1, 
                            $d['rid'], 
                            $d['sid'], 
                            $d['boid'], 
                            $p['rtdid'], 
                            $p['inc_trans']['adult']['price'], 
                            $p['inc_trans']['child']['price'], 
                            $p['inc_trans']['infant']['price'], 
                            $p['inc_trans']['adult']['selling_price'], 
                            $p['inc_trans']['child']['selling_price'], 
                            $p['inc_trans']['infant']['selling_price'],
                            ( isset( $inc_disc_adult ) ? $inc_disc_adult : 0 ), 
                            ( isset( $inc_disc_child ) ? $inc_disc_child : 0 ), 
                            ( isset( $inc_disc_infant ) ? $inc_disc_infant : 0 )
                        )));

                        $api_data[ $i ][ 'seat' ] = $seat;
                        $api_data[ $i ][ 'list' ][ 'include_trans' ] = array(
                            'trans_type'   => 1,
                            'route_list'   => $route_list,
                            'trip_value'   => $trip_value,
                            'boat_name'    => $d['boname'],
                            'adult_price'  => $adult_price, 
                            'child_price'  => $child_price, 
                            'infant_price' => $infant_price,
                            'disc_notif'   => $discount_notif,
                            'transport'    => array( array( 'name' => 'Shared ( Free )' ), array( 'name' => 'Private ( Additional )' ) ),
                        );
        			}

        			if( empty( $exc_price ) === false && $not_closed_date )
        			{
                        if( empty( $p['early_discount'] ) === false || empty( $p['seat_discount'] ) === false )
                        {
                            if( empty( $p['early_discount'] ) === false )
                            {
                                if( $p['early_discount']['type'] == '0' )
                                {
                                    $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                    $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                    $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];
                                }
                                else
                                {
                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                }

                                $discount_notif = empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'];
                            }

                            if( empty( $p['seat_discount'] ) === false )
                            {
                                if( $p['seat_discount']['type'] == '0' )
                                {
                                    $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                    $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                    $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                }
                                else
                                {
                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                }

                                $discount_notif = empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'];
                            }
                        }
                        else
                        {
                            $exc_disc_adult  = 0;
                            $exc_disc_child  = 0;
                            $exc_disc_infant = 0;
                            $discount_notif  = '';
                        }

                        $adult_price  = empty( $p['exc_trans']['adult']['price'] ) ? 'Free' : number_format( $p['exc_trans']['adult']['price'], 0, ',', '.' );
                        $child_price  = empty( $p['exc_trans']['child']['price'] ) ? 'Free' : number_format( $p['exc_trans']['child']['price'], 0, ',', '.' );
                        $infant_price = empty( $p['exc_trans']['infant']['price'] ) ? 'Free' : number_format( $p['exc_trans']['infant']['price'], 0, ',', '.' );

                        $route_list = get_route_detail_list_content( $d['rid'], $return_port, $destination_port_rtn, true );
                        $trip_value = base64_encode( implode( '|', array(
                            1, 
                            $d['rid'], 
                            $d['sid'], 
                            $d['boid'], 
                            $p['rtdid'], 
                            $p['exc_trans']['adult']['price'], 
                            $p['exc_trans']['child']['price'], 
                            $p['exc_trans']['infant']['price'], 
                            $p['exc_trans']['adult']['selling_price'], 
                            $p['exc_trans']['child']['selling_price'], 
                            $p['exc_trans']['infant']['selling_price'],
                            ( isset( $exc_disc_adult ) ? $exc_disc_adult : 0 ), 
                            ( isset( $exc_disc_child ) ? $exc_disc_child : 0 ), 
                            ( isset( $exc_disc_infant ) ? $exc_disc_infant : 0 )
                        )));

                        $api_data[ $i ][ 'seat' ] = $seat;
                        $api_data[ $i ][ 'list' ][ 'exclude_trans' ] = array(
                            'trans_type'   => 0,
                            'route_list'   => $route_list,
                            'trip_value'   => $trip_value,
                            'boat_name'    => $d['boname'],
                            'adult_price'  => $adult_price, 
                            'child_price'  => $child_price, 
                            'infant_price' => $infant_price,
                            'disc_notif'   => $discount_notif,
                            'transport'    => array( array( 'name' => 'Excluded' ) )
                        );
        			}

        			$i++;
            	}
            }
        }

        if( empty( $i )  )
        {
        	return array( 'Ooops, sold out for that date and route! If your dates are flexible please try day before or after.' );
        }
        else
        {
    		return $api_data;
        }
    }
    else
    {
        return array( 'Ooops, sold out for that date and route! If your dates are flexible please try day before or after.' );
    }
}

function api_remain_seat( $date, $sid, $rid, $capacity, $depart_port, $destination_port, $agid = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_schedule AS a LEFT JOIN ticket_route AS b ON a.rid = b.rid WHERE a.sid = %d AND a.rid = %d';
    $q = $db->prepare_query( $s, $sid, $rid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( $d['rtype'] == '1' && !empty( $d['rhoppingpoint'] ) )
    {
        $time = strtotime( $date );

        $total_agent_allot_arr = get_total_all_agent_daily_allotment( $sid, $time );
    	$total_agent_allot     = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
        $total_online_allot    = get_seat_availibility_online_num_by_schedule( $capacity, $rid, $sid, $time );
        $rcapacity             = $capacity - $total_agent_allot - $total_online_allot;

        $spoint = get_route_point_list( $rid, 0 );
        $epoint = get_route_point_list( $rid, 1 );
        $hpoint = get_route_point_list( $rid, 2 );
        
        //-- A = spoint
        //-- B = hpoint
        //-- C = epoint
        //-- X = capacity
        if( in_array( $depart_port, $spoint ) && in_array( $destination_port, $epoint ) )
        {
            //-- Get max capacity from start point to end point
            //-- If BC > AB --> ACmax = X - AB - AC
            //-- If BC > AB --> ACmax = X - BC - AC
            $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true );
            $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true );
            $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true );

            //-- Maximum seat for AC
            if( $ab_seat > $bc_seat )
            {
                $max_seat = $rcapacity - $ac_seat - $ab_seat;
            }
            elseif( $ab_seat < $bc_seat )
            {
                $max_seat = $rcapacity - $ac_seat - $bc_seat;
            }
            else
            {
                $max_seat = $rcapacity - $ab_seat - $ac_seat - $bc_seat;
            }
            
            if( $max_seat <= 0 )
            {
                return 0;
            }
            else
            {
                $seat = $max_seat;
            }
        }
        elseif( in_array( $depart_port, $spoint ) && in_array( $destination_port, $hpoint ) )
        {
            //-- Get max capacity from start point to hopping point
            //-- ABmax = X - AC - AB
            $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true );
            $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true );
            
            //-- Maximum seat for AB
            $max_seat = $rcapacity - $ac_seat - $ab_seat;
            
            if( $max_seat <= 0 )
            {
                return 0;
            }
            else
            {
                $seat = $max_seat;
            }
        }
        elseif( in_array( $depart_port, $hpoint ) && in_array( $destination_port, $epoint ) )
        {
            //-- Get max capacity from hopping point to end point
            //-- BCmax = X - AC - BC
            $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true );
            $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true );

            //-- Maximum seat for BC
            $max_seat = $rcapacity - $ac_seat - $bc_seat;
            
            if( $max_seat <= 0 )
            {
                return 0;
            }
            else
            {
                $seat = $max_seat;
            }
        }
        else
        {
            $seat = $rcapacity;
        }

        if( $seat <= 0 )
        {
            return 0;
        }
        else
        {
            if( isset( $total_agent_allot_arr[ $time ][ $rid ][ $agid ][ 'allot' ] ) )
            {
                if( $total_agent_allot_arr[ $time ][ $rid ][ $agid ][ 'allot' ] <= 0 )
                {
                    return 0;
                }
                else
                {
                    if( $total_agent_allot_arr[ $time ][ $rid ][ $agid ][ 'allot' ] > $seat )
                    {
                        return $seat;
                    }
                    else
                    {
                        return $total_agent_allot_arr[ $time ][ $rid ][ $agid ][ 'allot' ];
                    }
                }
            }
            else
            {
                return 0;
            }
        }
    }
    else
    {
        $seat  = get_seat_availibility_num_by_schedule( $capacity, $rid, $sid, strtotime( $date ), $agid, true );

        if( $seat <= 0 )
        {
            return 0;
        }
        else
        {
            return $seat;
        }
    }
}

function api_select_trip( $data )
{
	extract( $_POST );

	if( $type_of_route == 1 )
	{
		$fields = array(
			'token',
			'adult_num', 
			'child_num', 
			'infant_num', 
			'depart_value', 
			'return_value',
			'type_of_route', 
			'date_of_depart',
			'date_of_return',
			'depart_port_to',
			'return_port_to',
			'depart_port_from',
			'return_port_from'
		);
	}
	else
	{
		$fields = array(
			'token',
			'adult_num', 
			'child_num', 
			'infant_num', 
			'depart_value',
			'type_of_route', 
			'date_of_depart', 
			'depart_port_to', 
			'depart_port_from',
		);
	}

	foreach( $_POST as $key => $value )
	{
		if( in_array( $key, $fields ) )
		{
			if( ( $idx = array_search( $key, $fields ) ) !== false )
			{
			    unset( $fields[ $idx ] );
			}
		}
	}

	if( empty( $fields ) === false )
	{
		return array( 'result' => 'failed', 'message' => 'You must set value for this parameter : ' . implode( ', ', $fields ) );
	}
	else
	{
		if( isset( $date_of_depart ) && strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
	    {
	    	return array( 'result' => 'failed', 'message' => 'Departure day may not be greater than today' );
	    }
	    else
	    {
		    $port_type        = get_location( $depart_port_from, 'lctype' );
		    $agent            = get_agent( $data->agid );

			$date_of_return   = isset( $date_of_return ) ? $date_of_return : '';
		    $return_port_from = isset( $return_port_from ) ? $return_port_from : '';
		    $return_port_to   = isset( $return_port_to ) ? $return_port_to : '';

		    $depart_route     = get_location( $depart_port_from, 'lcname' ) . ' to ' . get_location( $depart_port_to, 'lcname' );
		    $return_route     = '';

		    if( empty( $return_port_from ) == false && empty( $return_port_to ) == false )
		    {
		    	$return_route = get_location( $return_port_from, 'lcname' ) . ' to ' . get_location( $return_port_to, 'lcname' );
		    }
		    
		    $param = array( 
		        'adult_num'            => $adult_num,
		        'child_num'            => $child_num,
		        'infant_num'           => $infant_num,
		        'port_type'            => $port_type,
		        'type_of_route'        => $type_of_route,
		        'date_of_depart'       => $date_of_depart,
		        'date_of_return'       => $date_of_return,
		        'destination_port'     => $depart_port_to,
		        'destination_port_rtn' => $return_port_to,
		        'depart_port'          => $depart_port_from,
		        'return_port'          => $return_port_from,
		        'depart_route'         => $depart_route,
		        'return_route'         => $return_route,
		        'chid'                 => $agent['chid'],
		        'agid'                 => $agent['agid'],
		        'promo_code'           => '',
		        'freelance_code'       => '',
		        'subtotal'             => 0,
		        'discount'             => 0,
		        'grandtotal'           => 0
		    );

		    if( $type_of_route == 1 )
		    {
		        $param['trip'] = array();

		        $drid    = '';
		        $dsid    = '';
		        $dboid   = '';
		        $drtdid  = '';
		        $dtrans  = '';
		        $daprice = 0;
		        $dcprice = 0;
		        $diprice = 0;
		        $dadisc  = 0;
		        $dcdisc  = 0;
		        $didisc  = 0;
		        $dtprice = 0;
		        $dtdisc  = 0;

		        $rrid    = '';
		        $rsid    = '';
		        $rboid   = '';
		        $rrtdid  = '';
		        $rtrans  = '';
		        $raprice = 0;
		        $rcprice = 0;
		        $riprice = 0;
		        $radisc  = 0;
		        $rcdisc  = 0;
		        $ridisc  = 0;
		        $rtprice = 0;
		        $rtdisc  = 0;

		        if( empty( $depart_value ) === false )
		        {
		        	list( $dtrans, $drid, $dsid, $dboid, $drtdid, $daprice, $dcprice, $diprice, $dasprice, $dcsprice, $disprice, $dadisc, $dcdisc, $didisc ) = explode( '|', base64_decode( $depart_value ) );

		        	$dboname  = get_boat( $dboid, 'boname' );
		        	$dpas_num = get_passenger_num_content( $adult_num, $child_num, $infant_num, true );
		        	$droutes  = get_route_detail_list_content( $drid, $depart_port_from, $depart_port_to, true );

		        	$dtprice = ( $daprice * $adult_num )  + ( $dcprice * $child_num ) + ( $diprice * $infant_num );
		        	$dtdisc  = ( $dadisc * $adult_num ) + ( $dcdisc * $child_num ) + ( $didisc * $infant_num );

		        	$dprice_disc = $dtprice - $dtdisc;

				    if( $dtrans == 1 )
				    {
                        $dtransport = array( array( 'name' => 'Shared ( Free )' ), array( 'name' => 'Private ( Additional )' ) );
				    }
				    else
				    {
                        $dtransport = array( array( 'name' => 'Excluded' ) );
				    }

		            $param['trip']['departure'] = array( 
		                'rid'               => $drid,
		                'sid'               => $dsid,
		                'boid'              => $dboid,
		                'boname'            => $dboname,
		                'rtdid'             => $drtdid,
		                'adult_price'       => $daprice,
		                'child_price'       => $dcprice,
		                'infant_price'      => $diprice,
		                'sell_adult_price'  => $dasprice,
		                'sell_child_price'  => $dcsprice,
		                'sell_infant_price' => $disprice,
		                'disc_adult'        => $dadisc,
		                'disc_child'        => $dcdisc,
		                'disc_infant'       => $didisc,
		                'disc_price'        => $dtdisc,		                
                        'passenger_list'    => $dpas_num,
                        'transport'         => $dtransport,
                        'route_list'        => $droutes,
                        'price'             => $dtprice,
                        'total_price'       => $dprice_disc,
		                'trans_type'        => $dtrans,
		                'depart_value'      => $depart_value
		            );

		        	$param['trip']['departure']['rates'] = get_rate_price_content( $drid, $adult_num, $child_num, $infant_num, $param['trip']['departure'], false, true, true );
		        	$param['trip']['departure']['transport_option'] = get_pickup_drop_off_transport( $drid, $depart_port_from, $depart_port_to, $dtrans, $param, false, true, true );
		        }

		        if( empty( $return_value ) === false )
		        {
		        	list( $rtrans, $rrid, $rsid, $rboid, $rrtdid, $raprice, $rcprice, $riprice, $rasprice, $rcsprice, $risprice, $radisc, $rcdisc, $ridisc ) = explode( '|', base64_decode( $return_value ) );

		        	$rboname  = get_boat( $rboid, 'boname' );
		        	$rpas_num = get_passenger_num_content( $adult_num, $child_num, $infant_num, true );
		        	$rroutes  = get_route_detail_list_content( $rrid, $return_port_from, $return_port_to, true );

		        	$rtprice = ( $raprice * $adult_num )  + ( $rcprice * $child_num ) + ( $riprice * $infant_num );
		        	$rtdisc  = ( $radisc * $adult_num ) + ( $rcdisc * $child_num ) + ( $ridisc * $infant_num );
		        	
		        	$rtprice_disc = $rtprice - $rtdisc;

				    if( $rtrans == 1 )
				    {
                        $rtransport = array( array( 'name' => 'Shared ( Free )' ), array( 'name' => 'Private ( Additional )' ) );
				    }
				    else
				    {
                        $rtransport = array( array( 'name' => 'Excluded' ) );
				    }

		            $param['trip']['return'] = array( 
		                'rid'               => $rrid,
		                'sid'               => $rsid,
		                'boid'              => $rboid,
		                'rtdid'             => $rrtdid,
		                'boname'            => $rboname,
		                'adult_price'       => $raprice,
		                'child_price'       => $rcprice,
		                'infant_price'      => $riprice,
		                'sell_adult_price'  => $rasprice,
		                'sell_child_price'  => $rcsprice,
		                'sell_infant_price' => $risprice,
		                'disc_adult'        => $radisc,
		                'disc_child'        => $rcdisc,
		                'disc_infant'       => $ridisc,
		                'disc_price'        => $rtdisc,
                        'passenger_list'    => $rpas_num,
                        'transport'         => $rtransport,
                        'route_list'        => $rroutes,
                        'price'             => $rtprice,
                        'total_price'       => $rtprice_disc,
		                'trans_type'        => $rtrans,
		                'return_value'      => $return_value
		            );

		        	$param['trip']['return']['rates'] = get_rate_price_content( $rrid, $adult_num, $child_num, $infant_num, $param['trip']['departure'], false, true, true );
		        	$param['trip']['return']['transport_option'] = get_pickup_drop_off_transport( $rrid, $return_port_from, $return_port_to, $rtrans, $param, true, true, true );
		        }

		        $param['discount']   = 0;
		        $param['subtotal']   = $dprice_disc + $rtprice_disc;
		        $param['grandtotal'] = $dprice_disc + $rtprice - $rtdisc;
		    }
		    else
		    {
		        $param['trip'] = array();

		        $drid    = '';
		        $dsid    = '';
		        $dboid   = '';
		        $drtdid  = '';
		        $dtrans  = '';
		        $daprice = 0;
		        $dcprice = 0;
		        $diprice = 0;
		        $dadisc  = 0;
		        $dcdisc  = 0;
		        $didisc  = 0;
		        $dtprice = 0;
		        $dtdisc  = 0;

		        if( empty( $depart_value ) === false )
		        {
		            list( $dtrans, $drid, $dsid, $dboid, $drtdid, $daprice, $dcprice, $diprice, $dasprice, $dcsprice, $disprice, $dadisc, $dcdisc, $didisc ) = explode( '|', base64_decode( $depart_value ) );
		            
		        	$dboname  = get_boat( $dboid, 'boname' );
		        	$dpas_num = get_passenger_num_content( $adult_num, $child_num, $infant_num, true );
		        	$droutes  = get_route_detail_list_content( $drid, $depart_port_from, $depart_port_to, true );

		        	$dtprice = ( $daprice * $adult_num )  + ( $dcprice * $child_num ) + ( $diprice * $infant_num );
		        	$dtdisc  = ( $dadisc * $adult_num ) + ( $dcdisc * $child_num ) + ( $didisc * $infant_num );
		        	
		        	$dprice_disc = $dtprice - $dtdisc;

				    if( $dtrans == 1 )
				    {
                        $dtransport = array( array( 'name' => 'Shared ( Free )' ), array( 'name' => 'Private ( Additional )' ) );
				    }
				    else
				    {
                        $dtransport = array( array( 'name' => 'Excluded' ) );
				    }

		            $param['trip']['departure'] = array( 
		                'rid'               => $drid,
		                'sid'               => $dsid,
		                'boid'              => $dboid,
		                'boname'            => $dboname,
		                'rtdid'             => $drtdid,
		                'adult_price'       => $daprice,
		                'child_price'       => $dcprice,
		                'infant_price'      => $diprice,
		                'sell_adult_price'  => $dasprice,
		                'sell_child_price'  => $dcsprice,
		                'sell_infant_price' => $disprice,
		                'disc_adult'        => $dadisc,
		                'disc_child'        => $dcdisc,
		                'disc_infant'       => $didisc,
		                'disc_price'        => $dtdisc,
                        'passenger_number'  => $dpas_num,
                        'transport'         => $dtransport,
                        'route_list'        => $droutes,
                        'price'             => $dtprice,
                        'total_price'       => $dprice_disc,
		                'trans_type'        => $dtrans,
		                'depart_value'      => $depart_value
		            );

		        	$param['trip']['departure']['rates'] = get_rate_price_content( $drid, $adult_num, $child_num, $infant_num, $param['trip']['departure'], false, true, true );
		        	$param['trip']['departure']['transport_option'] = get_pickup_drop_off_transport( $drid, $depart_port_from, $depart_port_to, $dtrans, $param, false, true, true );
		        }

		        $param['subtotal']       = $dprice_disc;
		        $param['discount']       = 0;
		        $param['grandtotal']     = $dprice_disc;
		    }
		       
		    $param['country_option'] = get_country_list_option( null, false, 'Select Nationality', true );

		    return array( 'result' => 'success', 'data' => $param );
		}
	}
}

function api_add_booking( $data )
{
	$message = api_check_valid_transport_time( $_POST );

    if( empty( $message ) )
    {
		$bid = api_save_transaction( $_POST, $data );

        if( empty( $bid ) )
        {
        	return array( 'result' => 'failed' );
        }
        else
        {
        	return array( 'result' => 'success' );
        }
	}
	else
	{
		return array( 'result' => 'error', 'message' => $message );
	}
}

function api_save_transaction( $post, $data )
{
    global $db;

    $retval = 1;
    
    $db->begin();

	$ag = get_agent( $data->agid );
    $id = api_save_booking( $post, $ag );

    if( empty( $id ) )
    {
    	$retval = 0;
    }
    else
    {
    	$did = api_save_booking_detail( $id, $post, $ag );
        $tid = api_save_booking_agent_transaction( $id, $post, $ag );

        if( empty( $did ) || empty( $tid ) )
        {   
	    	$retval = 0;
	    }
    }

    if( $retval == 0 )
    {
        $db->rollback();

        return false;
    }
    else
    {
        $db->commit();

        $dt = ticket_booking_all_data( $id );
        
        save_log( $id, 'reservation', 'New Reservation #' . $dt['bticket'] . ' From API - ' . $ag['agname'] );

        return $id;
    }
}

function api_save_booking( $post, $agent )
{
    global $db;

    extract( $post );
    extract( $agent );

	$discount = 0;
    $subtotal = 0;

    foreach( $trip as $trip_type => $obj )
    {
    	extract( $obj );

    	list( $dtrans, $drid, $dsid, $dboid, $drtdid, $daprice, $dcprice, $diprice, $dasprice, $dcsprice, $disprice, $dadisc, $dcdisc, $didisc ) = explode( '|', base64_decode( $value ) );

    	$dtprice = ( $daprice * $adult_num )  + ( $dcprice * $child_num ) + ( $diprice * $infant_num );
    	$dtdisc  = ( $dadisc * $adult_num ) + ( $dcdisc * $child_num ) + ( $didisc * $infant_num );

    	$transfee  = array_sum( array_column( $transport[ $trip_type ], 'trans_fee') );
    	$subtotal += $transfee + ( $dtprice - $dtdisc );
    }

    $grandtotal = $subtotal + $discount;
	
    $bticket = generate_ticket_number();
    $bcode   = generate_code_number( $type_of_route );

    $s = 'INSERT INTO ticket_booking(
            chid,
            btype,
            bcode,
            bticket,
            bhotelname,
            bhoteladdress,
            bhotelphone,
            bhotelemail,
            bremark,
            pmcode,
            freelancecode,
            bsubtotal,
            bdiscount,
            btotal,
            bonhandtotal,
            bdate,
            bstatus,
            bpvaliddate,
            bpaymethod,
            bblockingtime,
            agid,
            bbname,
            bbphone,
            bbphone2,
            bbemail,
            bbcountry,
            bbrevtype,
            bbrevstatus,
            bbrevagent,
            bbooking_staf) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
    $q = $db->prepare_query( $s,
            $chid,
            $type_of_route, 
            $bcode, 
            $bticket, 
            $hotel_name,  
            $hotel_address,  
            $hotel_phone,
            $hotel_email, 
            '',
            '',
            '',
            $subtotal,  
            $discount,  
            $grandtotal,
            0,
            date( 'Y-m-d' ),
            'ca',
            0,
            8,
            0,
            $agid, 
            $book_by_name, 
            $book_by_phone, 
            $book_by_phone2, 
            $book_by_email, 
            $book_by_country, 
            'FIT', 
            'Tentative', 
            '', 
            '' );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return 0;
    }
    else
    {
    	return $db->insert_id();
    }
}

function api_save_booking_detail( $id, $post, $agent )
{
    global $db;

    extract( $post );
    extract( $agent );

    $error = 0;

    foreach( $trip as $type => $d )
    {
    	list( $dtrans, $drid, $dsid, $dboid, $drtdid, $daprice, $dcprice, $diprice, $dasprice, $dcsprice, $disprice, $dadisc, $dcdisc, $didisc ) = explode( '|', base64_decode( $d['value'] ) );

    	$dtprice = ( $daprice * $adult_num )  + ( $dcprice * $child_num ) + ( $diprice * $infant_num );
    	$dtdisc  = ( $dadisc * $adult_num ) + ( $dcdisc * $child_num ) + ( $didisc * $infant_num );
    	
        if( $type == 'departure' )
        {
            $route  = get_route_detail_content( $drid, $depart_port_from, $depart_port_to );
            $bddate = date( 'Y-m-d', strtotime( $date_of_depart ) );
            
            $bdfrom_id = $depart_port_from;
            $bdto_id   = $depart_port_to;
        }
        elseif( $type == 'return' )
        {
            $route  = get_route_detail_content( $drid, $return_port_from, $return_port_to );
            $bddate = date( 'Y-m-d', strtotime( $date_of_return ) );
            
            $bdfrom_id = $return_port_from;
            $bdto_id   = $return_port_to;
        }

        $dtfee  = array_sum( array_column( $transport[ $type ], 'trans-fee') );
        $dtotal = $dtfee + ( $dtprice - $dtdisc );

        if( $agpayment_type == 'Pre Paid' )
        {
            if( $agtype == 'Net Rate' )
            {
                $commission = 0;
                $commission_type = '0';
            }
            else
            {
                if( isset( $agfreelance[0] ) )
                {
                    $commission = $agfreelance[0]['agfcommission'];
                    $commission_type = $agfreelance[0]['agfcommission_type'];
                }
                else
                {
                    $commission = 0;
                    $commission_type = '0';
                }
            }
        }
        else
        {
            $commission = 0;
            $commission_type = '0';
        }

        $s = 'INSERT INTO ticket_booking_detail(
                bid,
                boid,
                rid,
                sid,
                bdtype,
                bdfrom,
                bdto,
                bdfrom_id,
                bdto_id,
                bddeparttime,
                bdarrivetime,
                bddate,
                bdtranstype,
                num_adult,
                num_child,
                num_infant,
                price_per_adult,
                price_per_child,
                price_per_infant,
                selling_price_per_adult,
                selling_price_per_child,
                selling_price_per_infant,
                net_price_per_adult,
                net_price_per_child,
                net_price_per_infant,
                disc_price_per_adult,
                disc_price_per_child,
                disc_price_per_infant,
                commission,
                commission_type,
                subtotal,
                transport_fee,
                discount,
                total,
                bdpstatus,
                bdrevstatus) VALUES( %d, %d, %d, %d, %s, %s, %s, %d, %d, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
        $q = $db->prepare_query( $s,
                $id, 
                $dboid, 
                $drid,
                $dsid,
                $type,  
                $route['depart'],  
                $route['arrive'],
                $bdfrom_id,
                $bdto_id,
                $route['depart_time'],  
                $route['arrive_time'],
                $bddate,
                $dtrans,
                $adult_num,
                $child_num,
                $infant_num,
                $daprice,
                $dcprice,
                $diprice,
                $dasprice,
                $dcsprice,
                $disprice,
                $dasprice,
                $dcsprice,
                $disprice,
                $dadisc,
                $dcdisc,
                $didisc,
                $commission,
                $commission_type,
                $dtprice,
                $dtfee,
                $dtdisc,
                $dtotal,
                'ca',
                'Tentative');
        $r = $db->do_query( $q );

	    if( is_array( $r ) )
	    {
	        return 0;
	    }
	    else
	    {
	    	$bdid = $db->insert_id();
            $bpid = api_save_booking_passenger( $bdid, $type, $post );
            $btid = api_save_booking_transport( $bdid, $type, $post );

            if( empty( $bdid ) || empty( $bpid ) || empty( $btid ) )
            {
            	$error++;
            }
	    }
    }

    if( $error == 0 )
    {
    	return 1;
    }
    else
    {
    	return 0;
    }
}

function api_save_booking_passenger( $bdid, $type, $post )
{
    global $db;

    extract( $post );

    $bpid = array();

    foreach( $guest_detail[ 'fullname' ][ $type ] as $ptype => $names )
    {
        foreach( $names as $idx => $name )
        {
            $birthday = empty( $guest_detail['birth'][ $type ][ $ptype ][ $idx ] ) ? '' : date( 'Y-m-d', strtotime( $guest_detail['birth'][ $type ][ $ptype ][ $idx ] ) );
            $country  = $guest_detail['country'][ $type ][ $ptype ][ $idx ];
            $gender   = $guest_detail['gender'][ $type ][ $ptype ][ $idx ];

            $s = 'INSERT INTO ticket_booking_passenger(
                    bdid,
                    bptype,
                    bpname,
                    bpgender,
                    bpbirthdate,
                    lcountry_id ) VALUES( %d, %s, %s, %s, %s, %d )';
            $q = $db->prepare_query( $s, $bdid, $ptype, $name, $gender, $birthday, $country );
            $r = $db->do_query( $q );

		    if( is_array( $r ) )
		    {
		        return 0;
		    }
		    else
		    {
		    	$bpid[] = $db->insert_id();
		    }
        }
    }

    return $bpid;
}

function api_save_booking_transport( $bdid, $type, $post )
{
    global $db;

    extract( $post );

    $btid = array();

    foreach( $transport[ $type ] as $dt )
    {
        if( $dt['type'] == 2 )
        {
            $s = 'INSERT INTO ticket_booking_transport( bdid, bttype, bttrans_type, bttrans_fee ) VALUES( %d, %s, %s, %s )';
            $q = $db->prepare_query( $s, $bdid, $dt['trans_type'], $dt['type'], $dt['trans_fee'] );
        }
        else
        {
            if( empty( $dt['hid'] ) || empty( $dt['taid'] ) )
            {
	            $s = 'INSERT INTO ticket_booking_transport( bdid, bttype, bttrans_type, bttrans_fee ) VALUES( %d, %s, %s, %s )';
	            $q = $db->prepare_query( $s, $bdid, $dt['trans_type'], $dt['type'], $dt['trans_fee'] );
           	}
           	else
           	{               
                $s = 'INSERT INTO ticket_booking_transport( bdid, hid, taid, bttype, bttrans_type, btrpfrom, btrpto, bttrans_fee ) VALUES( %d, %d, %d, %s, %s, %s, %s, %s )';
                $q = $db->prepare_query( $s, $bdid, $dt['hid'], $dt['taid'], $dt['trans_type'], $dt['type'], $dt['rpfrom'], $dt['rpto'], $dt['trans_fee'] );
           	}          
        }

    	$r = $db->do_query( $q );

	    if( is_array( $r ) )
	    {
	        return 0;
	    }
	    else
	    {
	    	$btid[] = $db->insert_id();
	    }
    }

    return $btid;
}

function api_save_booking_agent_transaction( $id, $post, $agent )
{
    global $db;

    extract( $post );
    extract( $agent );

    $dt = ticket_booking_all_data( $id );

	foreach( $guest_detail[ 'fullname' ][ 'departure' ] as $ptype => $names )
    {
        foreach( $names as $idx => $name )
        {
            $atgname[] = $name;
        }
    }

    $s = 'INSERT INTO ticket_agent_transaction( agid, bcode, bid, atdate, atgname, atdebet, atcredit, atstatus ) VALUES( %d, %s, %d, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s, $agid, $dt['bcode'], $id, date( 'Y-m-d' ), implode( ', ', $atgname ), $dt['btotal'], 0, 1 );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return 0;
    }
    else
    {
    	return $db->insert_id();
    }
}

function api_check_valid_transport_time( $post )
{
	global $db;
    
    extract( $post );

    if( isset( $transport ) )
    {
        $error = array();            

        foreach( $transport as $trip_type => $obj )
        {
            foreach( $obj as $trans_type => $dt )
            {
                if( isset( $dt['ftime'] ) && isset( $dt['taairport'] ) && $dt['taairport'] == 1 )
                {
                    if( empty( $dt['ftime'] ) )
                    {
                        if( $trans_type == 'pickup' )
                        {
                            $error[] = 'Sorry ' . $trip_type . ' trip flight landing time can\'t be empty';
                        }
                        elseif( $trans_type == 'drop-off' )
                        {
                            $error[] = 'Sorry ' . $trip_type . ' trip flight take off time can\'t be empty';
                        }
                    }
                    else
                    {
                    	list( $dtrans, $drid, $dsid, $dboid, $drtdid, $dtprice, $daprice, $dcprice, $diprice, $dasprice, $dcsprice, $disprice, $dtdisc, $dadisc, $dcdisc, $didisc ) = explode( '|', base64_decode( $post['trip'][$trip_type]['value'] ) );

                        $s = 'SELECT a.rdtime, b.rpcot
                              FROM ticket_route_detail AS a
                              LEFT JOIN ticket_route_pickup_drop AS b ON b.rdid = a.rdid
                              WHERE a.rid = %d AND b.taid = %d AND a.lcid = %d';
                        $q = $db->prepare_query( $s, $drid, $dt['taid'], $post['depart_port_to'] );
                        $r = $db->do_query( $q );

                        if( $db->num_rows( $r ) > 0 )
                        {
                            $d = $db->fetch_array( $r );

                            $sftime = strtotime( $dt['ftime'] );
                            $sddate = $trip_type == 'departure' ? $post['date_of_depart'] : $post['date_of_return'];
                            $sdtime = strtotime( $sddate . ' ' . $d['rdtime'] );

                            if( $trans_type == 'pickup' )
                            {
                                //-- Check flight time
                                //-- Set error if flight landing time
                                //-- Greater than boat depart/arrive time
                                if( $sdtime > $sftime )
                                {
                                    $flight_time = date( 'Y-m-d H:i:s', $sftime );
                                    $depart_time = date( 'Y-m-d H:i:s', $sdtime );

                                    $ftime = new DateTime( $flight_time );
                                    $dtime = new DateTime( $depart_time );

                                    $interval = $ftime->diff( $dtime );
                                    
                                    if( $interval->days < 1 && $interval->h < $d['rpcot'] )
                                    {
                                        $error[] = 'Sorry can\'t accept ' . $trip_type . ' trip transport request base on your flight landing time';
                                    }
                                }
                                else
                                {
                                    $error[] = 'Sorry can\'t accept ' . $trip_type . ' trip transport request base on your flight landing time';
                                }
                            }
                            elseif( $trans_type == 'drop-off' )
                            {
                                //-- Check flight time
                                //-- Set error if boat depart/arrive time 
                                //-- Greater than flight take off time
                                if( $sdtime < $sftime )
                                {
                                    $flight_time = date( 'Y-m-d H:i:s', $sftime );
                                    $depart_time = date( 'Y-m-d H:i:s', $sdtime );

                                    $ftime = new DateTime( $flight_time );
                                    $dtime = new DateTime( $depart_time );

                                    $interval = $ftime->diff( $dtime );
                                    
                                    if( $interval->days < 1 && $interval->h < $d['rpcot'] )
                                    {
                                        $error[] = 'Sorry can\'t accept ' . $trip_type . ' trip transport request base on your flight take off time';
                                    }
                                }
                                else
                                {
                                    $error[] = 'Sorry can\'t accept ' . $trip_type . ' trip transport request base on your flight take off time';
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if( empty( $error ) === false )
    {
        return $error;
    }
}

?>