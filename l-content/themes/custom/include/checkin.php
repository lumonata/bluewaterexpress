<?php

add_actions( 'checkin-ajax_page', 'checkin_ajax' );

if( is_page() && get_appname() == 'checkin-ajax' )
{
    run_actions( 'checkin-ajax_page' );
}

function is_checker_logged()
{
    if( isset( $_COOKIE['user_id'] ) && isset( $_COOKIE['password'] ) && isset( $_COOKIE['thecookie'] ) )
    {
        if( md5( $_COOKIE['password'] . $_COOKIE['user_id'] ) == $_COOKIE['thecookie'] )
        {
            global $db;
    
            $s = 'SELECT * FROM l_users_role WHERE lrole_id = %d';
            $q = $db->prepare_query( $s, $_COOKIE['user_type'] );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
                $d = $db->fetch_array( $r );
                $v = json_decode( $d['lrole_privileges'], true );

                if( json_last_error() == JSON_ERROR_NONE && in_array( 'checkin', $v ) )
                {
                	return true;
                }
           	}
        }
    }
    
    return false;
}

function is_checker_login()
{
	$cek_url = cek_url();

	if( $cek_url[0] == 'checker' && isset( $cek_url[1] ) && $cek_url[1] == 'login' )
	{
		add_actions( 'meta_title', get_meta_title( 'Checker - Login' ) );
        add_actions( 'meta_keywords', get_meta_keywords( 'Checker - Login' ) );
        add_actions( 'meta_description', get_meta_description( 'Checker - Login' ) );

		return true;
	}
	else
	{
		return false;
	}
}

function is_checker_forget_password()
{
	$cek_url = cek_url();

	if( $cek_url[0] == 'checker' && isset( $cek_url[1] ) && $cek_url[1] == 'forget-password' )
	{
        add_actions( 'meta_title', get_meta_title( 'Checker - Forget Password' ) );
        add_actions( 'meta_keywords', get_meta_keywords( 'Checker - Forget Password' ) );
        add_actions( 'meta_description', get_meta_description( 'Checker - Forget Password' ) );

		return true;
	}
	else
	{
		return false;
	}
}

function is_checkin()
{
    $cek_url = cek_url();

    if( count( $cek_url ) == 1 && $cek_url[0] == 'checkin' )
	{
		$web_title = web_name();

        add_actions( 'meta_title', get_meta_title( 'Checkin - ' . $web_title ) );
        add_actions( 'meta_keywords', get_meta_keywords( 'Checkin - ' . $web_title ) );
        add_actions( 'meta_description', get_meta_description( 'Checkin - ' . $web_title ) );

		return true;
	}
	else
	{
		return false;
	}
}

function get_trip_and_passenger()
{
    if( isset( $_POST['checkin'] ) )
    {
        $barcode = parse_url( $_POST[ 'barcodeval' ], PHP_URL_QUERY );

        if( empty( $barcode ) )
        {
            $bticket = $_POST[ 'barcodeval' ];
        }
        else
        {
            list( $label, $bticket ) = explode( '=', $barcode );
        }

        if( isset( $bticket ) && !empty( $bticket ) )
        {
            $dt = ticket_booking_pure_data_by_ticket( $bticket );

            $content = '
            <div class="form-process">
                <div class="col container-form-select">
                    <p>Schedules</p>
                    <select id="from" class="select-option from required" name="bdid" data-placeholder="Select Schedule">
                        <option value=""></option>';

                        foreach( $dt[ 'detail' ] as $bdtype => $bdetail )
                        {
                            $content .= '
                            <optgroup label="' . ucfirst( $bdtype ) . '">';

                                foreach( $bdetail as $bdid => $d )
                                {
                                    if( isset( $_POST[ 'bdid' ] ) && $_POST[ 'bdid' ] == $bdid )
                                    {
                                        $content .= '<option value="' . $bdid . '" selected>' . get_route( $d[ 'rid' ], 'rname' ) . '</option>';
                                    }
                                    else
                                    {
                                        $content .= '<option value="' . $bdid . '">' . get_route( $d[ 'rid' ], 'rname' ) . '</option>';
                                    }
                                }

                                $content .= '
                            </optgroup>';
                        }

                        $content .= '
                    </select>
                </div>';

                foreach( $dt[ 'detail' ] as $bdtype => $bdetail )
                {
                    foreach( $bdetail as $bdid => $d )
                    {
                        foreach( $d[ 'passenger' ] as $bptype => $pass )
                        {
                            if( isset( $_POST[ 'bdid' ] ) && $_POST[ 'bdid' ] == $bdid )
                            {
                                $class = 'col col-pass col-' . $bdid;
                            }
                            else
                            {
                                $class = 'col col-pass col-' . $bdid . ' sr-only';
                            }

                            $content .= '
                            <div class="' . $class . '">
                                <p>Passenger List</p>
                                <ul class="row row-passenger">';

                                    foreach( $pass as $p )
                                    {
                                        $content .= '
                                        <li class="text-right">
                                            <label>' . $p[ 'bpname' ] . '</label>';

                                            if( isset( $_POST[ 'passenger' ][ $bdid ] ) && in_array( $p[ 'bpid' ], $_POST[ 'passenger' ][ $bdid ] ) )
                                            {
                                                $content .= '<input type="checkbox" data-toggle="toggle" data-on="Show" data-off="Not Show" data-offstyle="danger" name="passenger[' . $bdid . '][]" value="' . $p[ 'bpid' ] . '" checked>';
                                            }
                                            else
                                            {
                                                $content .= '<input type="checkbox" data-toggle="toggle" data-on="Show" data-off="Not Show" data-offstyle="danger" name="passenger[' . $bdid . '][]" value="' . $p[ 'bpid' ] . '">';
                                            }

                                            $content .= '
                                        </li>';
                                    }

                                    $content .= '
                                </ul>
                            </div>';
                        }
                    }
                }

                $content .= '
                <button class="checkin-button" type="submit" name="checkin">Checkin</button>
            </div>';

            return $content;
        }
    }
}

function proceed_checkin()
{
    if( isset( $_POST['checkin'] ) )
    {
        $error = array();

        if( empty( $_POST[ 'bdid' ] ) )
        {
            $error[] = 'Please select the schedule first';
        }
        else
        {
            if( !isset( $_POST[ 'passenger' ][ $_POST[ 'bdid' ] ] ) )
            {
                $error[] = 'Pease select the passenger data to check in';
            }
        }

        if( empty( $error ) )
        {            
            global $flash;
            global $db;

            $retval = 1;

            $db->begin();

            $q = $db->prepare_query( 'SELECT a.cpid FROM ticket_booking_checkin AS a WHERE a.bdid = %d', $_POST[ 'bdid' ] );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
                $r = $db->update( 'ticket_booking_checkin', array(
                    'cpdetail' => json_encode( $_POST[ 'passenger' ][ $_POST[ 'bdid' ] ] ),
                    'luser_id' => $_COOKIE['user_id'],
                    'cpdate'   => time(),
                ), array( 'bdid' => $_POST[ 'bdid' ] ));
            }
            else
            {
                $r = $db->insert( 'ticket_booking_checkin', array(
                    'cpdetail' => json_encode( $_POST[ 'passenger' ][ $_POST[ 'bdid' ] ] ),
                    'luser_id' => $_COOKIE['user_id'],
                    'bdid'     => $_POST[ 'bdid' ],
                    'cpdate'   => time(),
                ));
            }

            if( is_array( $r ) )
            {
                $retval = 0;
            }

            if( $retval == 0 )
            {
                $db->rollback();

                $error_mess = '
                <fieldset class="message bg-danger">
                    <ul>
                        <li class="text-danger">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <b>Failed to proceed checkin passenger</b>
                        </li>
                    </ul>
                </fieldset>';

                add_variable( 'message', $error_mess );
            }
            else
            {
                $db->commit();

                $flash->add( array( 'type'=> 'success', 'content' => array( 'Successfully checkin the passenger' ) ) );

                header( 'location:' . HTSERVER . site_url() . '/checkin' );

                exit;
            }
        }
        else
        {
            $error_mess = '
            <fieldset class="message bg-danger">
                <ul>';

                    foreach( $error as $e )
                    {
                        $error_mess .= '
                        <li class="text-danger">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <b>' . $e . '</b>
                        </li>';
                    }

                    $error_mess .= '
                </ul>
            </fieldset>';

            add_variable( 'message', $error_mess );
        }
    }
}

function post_checker_login()
{
	if( isset( $_POST['login'] ) && ( isset( $_POST['usertype'] ) && $_POST['usertype'] == 'checker' ) )
    {
        return validate_checker_login();
    }
}

function validate_checker_login()
{
    if( empty( $_POST['username'] ) || empty( $_POST['password'] ) )
    {
        return '<div class="alert_red">Empty Username or Password.</div>';
    }
    else
    {
        if( is_exist_user( $_POST['username'] ) && is_match_password() )
        {
    		$d = fetch_user( $_POST['username'] );

            setcookie( 'username', $_POST['username'], false, '/' );
            setcookie( 'password', $_POST['password'], false, '/' );
            setcookie( 'user_id', $d['luser_id'], false, '/' );
            setcookie( 'user_type', $d['lrole_id'], false, '/' );
            setcookie( 'user_name', $d['ldisplay_name'], false, '/' );
            setcookie( 'thecookie', md5( $_POST['password'] . $d['luser_id'] ), false, '/' );

            header( 'location:' . HTSERVER . site_url() . '/checkin' );

            exit;
        }
        else
        {
            return '<div class="alert_red">Wrong Username or Password.</div>';
        }
    }
}

function post_checker_forget_password()
{
    if( count( $_POST ) > 0 && isset( $_POST['forget_password'] ) )
    {
        return validate_checker_forget_password();
    }
}

function validate_checker_forget_password()
{
    if( empty( $_POST[ 'username' ] ) )
    {
        return '<div class="alert_red">Please enter your username. You will receive a new password via e-mail.</div>';
    }
    else
    {
        if( is_exist_user( $_POST[ 'username' ] ) )
        {            
            $new_password = random_string();
            
            $user_id = fetch_user_ID_by_username( $_POST[ 'username' ] );
            
            $user = fetch_user( $user_id );
            
            if( reset_user_password( $user_id, $new_password ) )
            {
                $return = reset_password_email( $user['lemail'], $user['lusername'], $user['ldisplay_name'], $new_password );

                return '<div class="alert_red">Your request to reset this account password has been successfully. Please check your email to see the new password</div>';
            }
            else
            {
                return '<div class="alert_red">Sorry your request to reset this account password has been failed. Please try again later</div>';
            }	            
        }
        else
        {
            return '<div class="alert_red">Can\'t find any agent with this username</div>';
        }
    }
}

function checkin_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-booking-data' )
    {
        $barcode = parse_url( $_POST[ 'barcode' ], PHP_URL_QUERY );

        if( empty( $barcode ) )
        {
            $bticket = $_POST[ 'barcode' ];
        }
        else
        {
            list( $label, $bticket ) = explode( '=', $barcode );
        }

        if( isset( $bticket ) && !empty( $bticket ) )
        {
            $dta = ticket_booking_pure_data_by_ticket( $bticket );

            if( !empty( $dta ) )
            {
                $content = '
                <div class="form-process">
                    <div class="col container-form-select">
                        <p>Schedules</p>
                        <select id="from" class="select-option from required" name="bdid" data-placeholder="Select Schedule">
                            <option value=""></option>';

                            foreach( $dta[ 'detail' ] as $bdtype => $bdetail )
                            {
                                $content .= '
                                <optgroup label="' . ucfirst( $bdtype ) . '">';

                                    foreach( $bdetail as $bdid => $d )
                                    {
                                        $content .= '<option value="' . $bdid . '">' . get_route( $d[ 'rid' ], 'rname' ) . '</option>';
                                    }

                                    $content .= '
                                </optgroup>';
                            }

                            $content .= '
                        </select>
                    </div>';

                    foreach( $dta[ 'detail' ] as $bdtype => $bdetail )
                    {
                        foreach( $bdetail as $bdid => $dt )
                        {
                            $q = $db->prepare_query( 'SELECT a.cpdetail FROM ticket_booking_checkin AS a WHERE a.bdid = %d', $bdid );
                            $r = $db->do_query( $q );

                            $cpdetail = array();

                            if( $db->num_rows( $r ) > 0 )
                            {
                                $d = $db->fetch_array( $r );

                                $cpdetail = json_decode( $d[ 'cpdetail' ], true );
                            }

                            foreach( $dt[ 'passenger' ] as $bptype => $pass )
                            {
                                $content .= '
                                <div class="col col-pass col-' . $bdid . ' sr-only">
                                    <p>Passenger List</p>
                                    <ul class="row row-passenger">';

                                        foreach( $pass as $p )
                                        {
                                            $content .= '
                                            <li class="text-right">
                                                <label>' . $p[ 'bpname' ] . '</label>';

                                                if( !empty( $cpdetail ) && in_array( $p[ 'bpid' ], $cpdetail ) )
                                                {
                                                    $content .= '<input type="checkbox" data-toggle="toggle" data-on="Show" data-off="Not Show" data-offstyle="danger" name="passenger[' . $bdid . '][]" value="' . $p[ 'bpid' ] . '" checked>';
                                                }
                                                else
                                                {
                                                    $content .= '<input type="checkbox" data-toggle="toggle" data-on="Show" data-off="Not Show" data-offstyle="danger" name="passenger[' . $bdid . '][]" value="' . $p[ 'bpid' ] . '">';
                                                }

                                                $content .= '
                                            </li>';
                                        }

                                        $content .= '
                                    </ul>
                                </div>';
                            }
                        }
                    }

                    $content .= '
                    <button class="checkin-button" type="submit" name="checkin">Checkin</button>
                </div>';
            }
            else
            {
                $content = '
                <div class="mess bg-danger">
                    <ul>
                        <li class="text-danger">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <b>Failed to load your booking code!</b>
                        </li>
                    </ul>
                </div>';
            }
        }
        else
        {
            $content = '
            <div class="mess bg-danger">
                <ul>
                    <li class="text-danger">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <b>Please input your booking code!</b>
                    </li>
                </ul>
            </div>';
        }

        echo $content;
    }

    exit;
}

?>