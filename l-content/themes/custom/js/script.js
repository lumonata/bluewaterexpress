function scroll_to_anchor()
{
    jQuery('html, body').animate({
        scrollTop: jQuery('.has-error:eq(0)').offset().top - 50
    }, 500);    
}

function validate_availability_form()
{
    var type  = jQuery('[name=route_type]').val();
    var error = 0;
    var sum   = 0;

    jQuery('.required:not(:disabled)').each(function(){
        var vals = jQuery(this).val();

        if( vals == '' )
        {
            jQuery(this).parent().addClass('has-error');
            error++;
        }
    });

    jQuery('.prequired').each(function(){
        var vals = jQuery(this).val();
            sum  = sum + parseInt(vals);

        if( vals == '' )
        {
            jQuery(this).parent().addClass('has-error');
            error++;
        }
    });

    if( error == 0 )
    {
        if( sum == 0 )
        {
            show_popup('Warning', 'Number of passenger can\'t be empty');

            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        show_popup('Warning', 'Please check again your data before submited request, some of field may be empty or not valid');

        return false;
    }
}

function validate_booking_form()
{
    var dep_rid = jQuery('[name=dep_rid]:checked').val();
    var rtn_rid = jQuery('[name=rtn_rid]:checked').val();
    var type    = jQuery('[name=type_of_route]').val();

    if( typeof dep_rid == 'undefined' && type == 1 && typeof rtn_rid == 'undefined' )
    {
        show_popup('Warning', 'Please choose your departure and return package');
        return false;
    }
    else if( dep_rid == 'undefined' )
    {
        show_popup('Warning', 'Please choose your departure package');
        return false;
    }
    else if( type == 1 && typeof rtn_rid == 'undefined' )
    {
        show_popup('Warning', 'Please choose your return package');
        return false;
    }
    else
    {
        return true;
    }
}

function validate_booking_detail()
{
    var error = 0;

    jQuery('.required').each(function(i, e){
        var vals = jQuery(this).val();

        if(  vals == '' )
        {
            jQuery(this).parent().addClass('has-error');
            error++;
        }
        else
        {
            if( jQuery(this).hasClass('text-email') && !validate_email( vals ) )
            {
                jQuery(this).parent().addClass('has-error');
                error++;
            }     
        }
    });

    if( error == 0 )
    {
        return true;
    }
    else
    {
        show_popup('Warning', 'Please check again your data before submited request, some of field may be empty or not valid', 'scroll_to_anchor()');

        return false;
    }
}

function validate_payment()
{
    var error = 0;

    jQuery('.required').each(function(i, e){
        var vals = jQuery(this).val();

        if( vals == '' )
        {
            jQuery(this).parent().addClass('has-error');
            error++;
        }
        else
        {
            if( jQuery(this).hasClass('text-email') && !validate_email( vals ) )
            {
                jQuery(this).parent().addClass('has-error');
                error++;
            }     
        }
    });

    if( error == 0 )
    {
        return true;
    }
    else
    {
        show_popup('Warning', 'Please check again your data before confirm, some of field may be empty or not valid', 'scroll_to_anchor()');

        return false;
    }
}

function validate_email( address )
{
    var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if( reg.test(address) == false )
    {
        return false; 
    }
    else
    {
        return true;
    }
}

function search_availability_func()
{
    jQuery('.close').on('click', function(){
        jQuery('.form-search-overlay').removeClass('height-overlay');
    });

    jQuery('.container-edit-search').on('click', function(){
        jQuery('.form-search-overlay').addClass('height-overlay');
    });   
}

function show_popup( htitle, message, callback )
{
    jQuery('<div></div>').dialog({
        modal: true,
        draggable: false,
        resizable: false,
        title: htitle,
        open: function()
        {
            jQuery(this).html(message);
        },
        buttons:
        {
            Close: function()
            {
                jQuery( this ).dialog('close');

                if( callback!='' )
                {
                    eval(callback);
                }
            }
        }
    });
}

function show_popup_mailchimp( htitle, message, callback )
{
    jQuery('<div></div>').dialog({
        modal: true,
        draggable: false,
        resizable: false,
        title: htitle,
        dialogClass: 'popup-mailchimp',
        open: function()
        {
            jQuery(this).append('<img class="image-icon" src="https://www.bluewater-express.com/staging-rsv/l-content/themes/custom/images/conggratulation.svg"/>');
            jQuery(this).append('<div class="title-status">Success !</div>');
            jQuery(this).append(message);
        }
    });
}

function show_popup_mailchimp_false( htitle, message, callback )
{
    jQuery('<div></div>').dialog({
        modal: true,
        draggable: false,
        resizable: false,
        title: htitle,
        dialogClass: 'popup-mailchimp',
        open: function()
        {
            jQuery(this).append('<img class="image-icon" src="https://www.bluewater-express.com/staging-rsv/l-content/themes/custom/images/sorry.svg"/>');
            jQuery(this).append('<div class="title-status-sorry">Sorry !</div>');
            jQuery(this).append(message);
        }
    });
}

function init_availability_search()
{
    jQuery('.depart_date').datepicker({
        minDate:0,
        dateFormat: 'dd-MM-yy',
        onSelect: function(selectedDate) {
            jQuery('.return_date').datepicker('option', 'minDate', selectedDate);
        },
        beforeShow: function() {
            if( screen.width < 561 )
            {
                jQuery(this).datepicker('option', 'numberOfMonths', 1);
            }
            else
            {
                jQuery(this).datepicker('option', 'numberOfMonths', 2);
            }
        }
    });

    jQuery('.return_date').datepicker({
        minDate:0,
        defaultDate: '+1d',
        dateFormat: 'dd-MM-yy',
        onSelect: function(selectedDate) {
            jQuery('.depart_date').datepicker('option', 'maxDate', selectedDate);
        },
        beforeShow: function() {
            if( screen.width < 561 )
            {
                jQuery(this).datepicker('option', 'numberOfMonths', 1);
            }
            else
            {
                jQuery(this).datepicker('option', 'numberOfMonths', 2);
            }
        }
    });

    jQuery('[name=route_type]').on('change', function(){
        var vals = jQuery(this).val();

        jQuery('.required, .prequired').parent().removeClass('has-error');

        if( vals == 0 )
        {
            jQuery('[name=destination_return]').prop('disabled', true).val('').select2();
            jQuery('[name=destination_return_to]').prop('disabled', true).val('').select2();
            jQuery('[name=return_date]').prop('disabled', true).val('');
        }
        else
        {
            jQuery('[name=destination_return]').prop('disabled', false);
            jQuery('[name=destination_return_to]').prop('disabled', false);
            jQuery('[name=return_date]').prop('disabled', false);
        }
    });

    jQuery('[name=check_availability]').on('click', function(){
        if( validate_availability_form() )
        {
            return true;
        }

        return false;
    });

    jQuery('.required, .prequired').on('focusin', function(){
        jQuery(this).parent().removeClass('has-error');
    });

    jQuery('.select-option').select2();

    init_number_func();
}

function init_booking_review()
{
    jQuery('.container-price-select [name=dep_rid]').on('change', function(e){
        var len = jQuery('[name=type_of_route]').val() == 0 ? 1 : 2;

        if( jQuery('.container-price-select [type=radio]:checked').length == len )
        {
            jQuery('.continue-reservation .btn-select').removeAttr('disabled').css('background-color', 'rgb(220, 91, 14)');
        }

        if( this.checked )
        {
            jQuery('.departure-list .item').removeClass('selected');
            jQuery(this).parent('.item').addClass('selected');
        }
    });

    jQuery('.container-price-select [name=rtn_rid]').on('change', function(e){
        var len = jQuery('[name=type_of_route]').val() == 0 ? 1 : 2;

        if( jQuery('.container-price-select [type=radio]:checked').length == len )
        {
            jQuery('.continue-reservation .btn-select').removeAttr('disabled').css('background-color', 'rgb(220, 91, 14)');
        }

        if( this.checked )
        {
            jQuery('.return-list .item').removeClass('selected');
            jQuery(this).parents('.item').addClass('selected');
        }
    });

    jQuery('[name=review_booking]').on('click', function(){
        if( validate_booking_form() )
        {
            return true;
        }
        else
        {
            return false;
        }
    });
}

function init_number_func(){
    jQuery('.number-field').each(function() {
        var spinner = jQuery(this),
            input   = spinner.find('input[type="number"]'),
            btnUp   = spinner.find('.up'),
            btnDown = spinner.find('.down'),
            min     = input.attr('min'),
            max     = input.attr('max');

        btnUp.click(function(){
            if( !input.prop('disabled') )
            {
                var oldValue = parseFloat( input.val() );

                if( oldValue >= max )
                {
                    var newVal = oldValue;
                }
                else
                {
                    var newVal = oldValue + 1;
                }

                spinner.find('input').val(newVal);
                spinner.find('input').trigger('change');
            }
        });

        btnDown.click(function(){
            if( !input.prop('disabled') )
            {
                var oldValue = parseFloat( input.val() );

                if( oldValue <= min )
                {
                  var newVal = oldValue;
                }
                else
                {
                  var newVal = oldValue - 1;
                }

                spinner.find('input').val(newVal);
                spinner.find('input').trigger('change');
            }
        });
    });
}