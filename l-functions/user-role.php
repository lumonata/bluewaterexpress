<?php

add_actions( 'users-role-ajax_page', 'users_role_ajax' );

/*
| -------------------------------------------------------------------------------------
| Role Table List
| -------------------------------------------------------------------------------------
*/
function users_role_table()
{
    global $db;

    $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
    $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';
    $site_url = site_url();

    $s = 'SELECT * FROM l_users_role ORDER BY lrole_id DESC';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    set_template( TEMPLATE_PATH . '/template/role/list.html', 'role' );

    add_block( 'list-block', 'lcblock', 'role' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', users_role_table_data( $r ) );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'admin&sub=role' ) );
    add_variable( 'add_new_link', get_state_url( 'admin&sub=role&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'admin&sub=role&prc=edit' ) );
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Admin Role List' );

    return return_template( 'role' );
}

/*
| -------------------------------------------------------------------------------------
| Role Table List Item
| -------------------------------------------------------------------------------------
*/
function users_role_table_data( $r )
{
    global $db;

    $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
    $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';
    
    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="3">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( TEMPLATE_PATH . '/template/role/loop.html', 'role-loop' );

    add_block( 'loop-block', 'lcloop', 'role-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'id', $d['lrole_id'] );
        add_variable( 'lrole_name', $d['lrole_name'] );
        add_variable( 'delete_checkbox_attr', $d['lrole_default']=='1' ? 'disabled' : '' );
        add_variable( 'delete_link_css', $d['lrole_default']=='1' ? 'hidden' : '' );

        add_variable( 'edit_link', get_state_url( $state . $substate . '&prc=edit&id=' . $d['lrole_id'] ) );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/users-role-ajax/' );

        parse_template( 'loop-block', 'lcloop', true );
    }

    return return_template( 'role-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Role
| -------------------------------------------------------------------------------------
*/
function users_add_new_role()
{
    $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
    $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';

    run_save_role( $state, $substate );

    $data = get_role();

    set_template( TEMPLATE_PATH . '/template/role/form.html', 'role' );
    add_block( 'form-block', 'lcblock', 'role' );

    add_variable( 'lrole_id', $data['lrole_id'] );
    add_variable( 'lrole_name', $data['lrole_name'] );
    add_variable( 'lrole_description', $data['lrole_description'] );
    add_variable( 'lrole_type', get_role_type_option(  $data['lrole_type'] ) );
    add_variable( 'lrole_privileges', get_role_privileges_option( $data['lrole_privileges'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( $state . $substate . '&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( $state . $substate ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'New Admin Role' );

    return return_template( 'role' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Role
| -------------------------------------------------------------------------------------
*/
function users_edit_role()
{
    $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
    $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';
    
    run_update_role( $state, $substate );

    $data = get_role( $_GET['id'] );

    set_template( TEMPLATE_PATH . '/template/role/form.html', 'role' );
    add_block( 'form-block', 'lcblock', 'role' );

    add_variable( 'lrole_id', $data['lrole_id'] );
    add_variable( 'lrole_name', $data['lrole_name'] );
    add_variable( 'lrole_description', $data['lrole_description'] );
    add_variable( 'lrole_type', get_role_type_option(  $data['lrole_type'] ) );
    add_variable( 'lrole_privileges', get_role_privileges_option( $data['lrole_privileges'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( $state . $substate ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'role' ) );
    add_variable( 'action', get_state_url( $state . $substate . '&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'delete_class', $data['lrole_default'] == '1' ? 'sr-only' : '' );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/users-role-ajax/' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Edit Admin Role' );

    return return_template( 'role' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Role
| -------------------------------------------------------------------------------------
*/
function users_batch_delete_role()
{
    if( isset( $_POST['select'] ) && !empty( $_POST['select'] ) )
    {
        $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
        $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';
        
        set_template( TEMPLATE_PATH . '/template/role/batch-delete.html', 'role' );
        add_block( 'loop-block', 'lclblock', 'role' );
        add_block( 'delete-block', 'lcblock', 'role' );

        foreach( $_POST['select'] as $key=>$val )
        {
            $d = get_role( $val );

            add_variable('lrole_name',  $d['lrole_name'] );
            add_variable('lrole_id', $d['lrole_id'] );

            parse_template('loop-block', 'lclblock', true);
        }

        add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' role? :' );
        add_variable( 'action', get_state_url( $state . $substate ) );

        parse_template( 'delete-block', 'lcblock', false );
        
        add_actions( 'section_title', 'Delete role' );

        return return_template( 'role' );
    }
}

function run_save_role( $state = '', $substate = '' )
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_role_data();

        if( empty( $error ) )
        {
            $post_id  = save_role();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to save new role' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New role successfully saved' ) ) );

                header( 'location:' . get_state_url( $state . $substate .'&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_role( $state = '', $substate = '' )
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_role_data();

        if( empty( $error ) )
        {
            $post_id  = update_role();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this role' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This role successfully edited' ) ) );

                header( 'location:' . get_state_url( $state . $substate .'&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_role_data()
{
    $error = array();

    if( isset( $_POST['lrole_name'] ) && empty( $_POST['lrole_name'] ) )
    {
        $error[] = 'Role name can\'t be empty';
    }

    if( isset( $_POST['lrole_type'] ) && $_POST['lrole_type'] == '' )
    {
        $error[] = 'Role type can\'t be empty';
    }

    if( !isset( $_POST['lrole_privileges'] ) )
    {
        $error[] = 'Privileges can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Role List Count
| -------------------------------------------------------------------------------------
*/
function is_num_role()
{
    global $db;

    $s = 'SELECT * FROM l_users_role';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Role By ID
| -------------------------------------------------------------------------------------
*/
function get_role( $id = '', $field = '' )
{
    global $db;

    $data = array( 
        'lrole_id'          => ( isset( $_POST['lrole_id'] ) ? $_POST['lrole_id'] : null ), 
        'lrole_name'        => ( isset( $_POST['lrole_name'] ) ? $_POST['lrole_name'] : '' ), 
        'lrole_type'        => ( isset( $_POST['lrole_type'] ) ? $_POST['lrole_type'] : '' ), 
        'lrole_privileges'  => ( isset( $_POST['lrole_privileges'] ) ? $_POST['lrole_privileges'] : '' ),
        'lrole_description' => ( isset( $_POST['lrole_description'] ) ? $_POST['lrole_description'] : '' ),
        'lrole_default'     => ( isset( $_POST['lrole_default'] ) ? $_POST['lrole_default'] : '' )
    );

    $s = 'SELECT * FROM l_users_role WHERE lrole_id = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'lrole_id' => $d['lrole_id'], 
                'lrole_name' => $d['lrole_name'],
                'lrole_type' => $d['lrole_type'],
                'lrole_privileges' => $d['lrole_privileges'],
                'lrole_description' => $d['lrole_description'],
                'lrole_default' => $d['lrole_default']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Role List
| -------------------------------------------------------------------------------------
*/
function get_role_list()
{
    global $db;

    $s = 'SELECT * FROM l_users_role';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(  
            'lrole_id' => $d['lrole_id'], 
            'lrole_name' => $d['lrole_name'],
            'lrole_type' => $d['lrole_type'],
            'lrole_privileges' => $d['lrole_privileges'],
            'lrole_description' => $d['lrole_description'],
            'lrole_default' => $d['lrole_default']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Role
| -------------------------------------------------------------------------------------
*/
function save_role()
{
    global $db;

    $privileges = isset( $_POST['lrole_privileges'] ) ? json_encode( $_POST['lrole_privileges'], true ) : '';
    
    $s = 'INSERT INTO l_users_role( 
            lrole_name, 
            lrole_type, 
            lrole_privileges, 
            lrole_description, 
            lrole_default ) 
          VALUES( %s, %s, %s, %s, %s )';
    $q = $db->prepare_query( $s, 
            $_POST['lrole_name'],  
            $_POST['lrole_type'],  
            $privileges,  
            $_POST['lrole_description'],  
            '0' );
       
    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();
        
        save_log( $id, 'role', 'Add new admin role - ' . $_POST['lrole_name'] );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Role
| -------------------------------------------------------------------------------------
*/
function update_role()
{
    global $db;

    $curr_role  = get_role( $_POST['lrole_id'] );
    $privileges = isset( $_POST['lrole_privileges'] ) ? json_encode( $_POST['lrole_privileges'], true ) : '';

    $s = 'UPDATE l_users_role SET 
            lrole_name = %s, 
            lrole_type = %s, 
            lrole_privileges = %s, 
            lrole_description = %s
          WHERE lrole_id = %d';     
    $q = $db->prepare_query( $s, 
            $_POST['lrole_name'], 
            $_POST['lrole_type'], 
            $privileges, 
            $_POST['lrole_description'], 
            $_POST['lrole_id'] );
       
    if( $db->do_query( $q ) )
    {
        if( $curr_role['lrole_name'] != $_POST['lrole_name'] )
        {
            save_log( $_POST['lrole_id'], 'role', 'Update admin role - ' . $curr_role['lrole_name'] . ' to ' . $_POST['lrole_name'] );
        }
        else
        {
            save_log( $_POST['lrole_id'], 'role', 'Update admin role - ' . $_POST['lrole_name'] );
        }

        return $_POST['lrole_id'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Role
| -------------------------------------------------------------------------------------
*/
function delete_role( $id = '', $is_ajax = false )
{
    global $db;

    $d = get_role( $id );

    $s = 'DELETE FROM l_users_role WHERE lrole_id = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
            $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';

            return header( 'location:' . get_state_url( $state . $substate .'&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'role', 'Delete admin role - ' . $d['lrole_name'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Role Option
| -------------------------------------------------------------------------------------
*/
function get_role_option( $lrole_id = '' )
{
    $owner  = get_role_list();
    $option = '<option value="">Select Role</option>';

    if( !empty( $owner ) )
    {
        foreach( $owner as $d )
        {
            $option .= '<option value="' . $d['lrole_id'] . '" ' . ( $d['lrole_id'] == $lrole_id ? 'selected' : '' ) . ' >' . $d['lrole_name'] . '</option>';   
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Role Type Option
| -------------------------------------------------------------------------------------
*/
function get_role_type_option( $lrole_type = '' )
{
    if( isset( $_POST['lrole_type'] ) )
    {
        $option = '
        <option value="">Select role types</option>
        <option value="0" ' . ( $_POST['lrole_type']=='0' ? 'selected' : '' ) . '>Read only</option>
        <option value="1" ' . ( $_POST['lrole_type']=='1' ? 'selected' : '' ) . '>Add/Edit/Delete</option>';
    }
    else
    {
        $option = '
        <option value="">Select role types</option>
        <option value="0" ' . ( $lrole_type=='0' ? 'selected' : '' ) . '>Read only</option>
        <option value="1" ' . ( $lrole_type=='1' ? 'selected' : '' ) . '>Add/Edit/Delete</option>';
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Role Privileges Option
| -------------------------------------------------------------------------------------
*/
function get_role_privileges_option( $lrole_privileges )
{
    global $apps_privileges;

    $role  = json_decode( $lrole_privileges, true );
    $dchck = is_array( $role ) && in_array( 'dashboard', $role ) ? 'checked' : '';
    $item  = '
    <ul class="clearfix">
        <li>
            <div class="checkbox">
                <input id="dashboard-menu" name="lrole_privileges[]" autocomplete="off" type="checkbox" value="dashboard" ' . $dchck . ' />
                <label for="dashboard-menu">Dashboard</label>
            </div>
        </li>';

        if( isset( $apps_privileges ) && !empty( $apps_privileges ) )
        {
            foreach( $apps_privileges as $apps_name )
            {
                foreach( $apps_name as $key => $value )
                {
                    $chck  = is_array( $role ) && in_array( $key, $role ) ? 'checked' : '';
                    $item .= '
                    <li>
                        <div class="checkbox">
                            <input id="' . $key . '-menu" name="lrole_privileges[]" autocomplete="off" type="checkbox" value="' . $key . '" ' . $chck . ' />
                            <label for="' . $key . '-menu">' . $value . '</label>
                        </div>
                    </li>';
                }
            }
        }
        
        $item .= '
    </ul>';

    return $item;
}

function get_role_privileges_users_option( $luser_privilage )
{
    global $apps_privileges;

    $roles = json_decode( $luser_privilage, true );
    $apps  = (array) $apps_privileges;

    if( is_array( $apps['app_name'] ) && !empty( $apps['app_name'] ) ) 
    {
        $item = '
        <ul class="row" id="app_privilage">';

            foreach( $apps['app_name'] as $app_name => $app_label ) 
            {
                $acces = isset( $roles[ $app_name ] ) ? $roles[ $app_name ] : 0;
                $item .= '
                <li class="col-md-4">
                    <div class="checkbox">
                        <label>' . $app_label . '</label>
                    </div>
                    <div class="checkbox" style="border:none;padding: 10px 20px;">
                        <label>
                            <input id="booking-source-menu" name="app_privilage[' . $app_name . ']" autocomplete="off" type="radio" value="1" ' . ( $acces == '1' ? 'checked' : '' ) . '/>
                            <span for="booking-source-menu">Add/Edit/Delete</span>
                        </label>
                    </div>
                    <div class="checkbox" style="border-bottom: 1px dashed rgb(198, 216, 220);padding: 0 20px 20px 20px">
                        <label>
                            <input id="booking-source-menu" name="app_privilage[' . $app_name . ']" autocomplete="off" type="radio" value="0" ' . ( $acces == '0' ? 'checked' : '' ) . '/>
                            <span for="booking-source-menu">Read only</span>
                        </label>
                    </div>
                </li>';
            }

            $item .= '
        </ul>';
    }

    return $item;
}

function add_role_privileges_users_option( $role_id )
{
    global $db;
    global $apps_privileges;

    $s = 'SELECT lrole_privileges, lrole_type FROM l_users_role WHERE lrole_id=%d';
    $p = $db->prepare_query( $s, $role_id );
    $r = $db->do_query( $p );
    $d = $db->fetch_array( $r );

    if ( !empty( $d ) && $d['lrole_privileges'] != '' ) 
    {
        $roles  = json_decode( $d['lrole_privileges'], true );
        $item   = '
        <ul class="clearfix" id="app_privilage">';

        if ( isset( $apps_privileges ) && !empty( $apps_privileges ) ) 
        {
            foreach( $apps_privileges as $apps_name )
            {
                foreach ( $apps_name as $app_key => $app_val ) 
                {
                    if ( in_array( $app_key, $roles ) ) 
                    {
                        if ( $d['lrole_type'] == '0' ) 
                        {
                            $item .= '
                            <li>
                                <div class="checkbox">
                                    <label for="'. $app_key .'-menu">'. $app_val .'</label>
                                </div>
                                <div class="checkbox" style="border:none;padding: 10px 20px;">
                                    <label>
                                        <input id="booking-source-menu" name="app_privilage['. $app_key .']["access"]" autocomplete="off" type="radio" value="1" />
                                        <span for="booking-source-menu">Add/Edit/Delete</span>
                                    </label>
                                </div>
                                <div class="checkbox" style="border-bottom: 1px dashed rgb(198, 216, 220);padding: 0 20px 20px 20px">
                                    <label>
                                        <input id="booking-source-menu" name="app_privilage['. $app_key .']["access"]" autocomplete="off" type="radio" value="0" checked/>
                                        <span for="booking-source-menu">Read only</span>
                                    </label>
                                </div>
                            </li>';
                        } 
                        else 
                        {
                            $item .= '
                            <li>
                                <div class="checkbox">
                                    <label for="'. $app_key .'-menu">'. $app_val .'</label>
                                </div>
                                <div class="checkbox" style="border:none;padding: 10px 20px;">
                                    <label>
                                        <input id="booking-source-menu" name="app_privilage['. $app_key .']["access"]" autocomplete="off" type="radio" value="1" checked/>
                                        <span for="booking-source-menu">Add/Edit/Delete</span>
                                    </label>
                                </div>
                                <div class="checkbox" style="border-bottom: 1px dashed rgb(198, 216, 220);padding: 0 20px 20px 20px">
                                    <label>
                                        <input id="booking-source-menu" name="app_privilage['. $app_key .']["access"]" autocomplete="off" type="radio" value="0"/>
                                        <span for="booking-source-menu">Read only</span>
                                    </label>
                                </div>
                            </li>';
                        }
                    }
                }
            }
        }

        $item .= '
        </ul>';
    }

    return $item;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function users_role_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-role' )
    {
        $d = delete_role( $_POST['lrole_id'], true );

        if( $d===true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }

    if ( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get_role_privilage' ) 
    {
        echo add_role_privileges_users_option( $_POST['role_id'] );
    }
}
?>