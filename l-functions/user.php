<?php

if( !defined( 'FILES_PATH' ) );
{
	define( 'FILES_PATH', ROOT_PATH . '/l-content/files' );
}

/**
 * To check if the URI requested is call login form.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */
function is_login_form()
{
    if( empty( $_GET['state'] ) || $_GET['state'] == 'login' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * To check if the URI requested is call registration form.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */
function is_register_form()
{
    if( isset( $_GET['state'] ) && $_GET['state'] == 'register' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * To check if the URI requested is call thanks form after registration.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */

function is_thanks_page()
{
    if( isset( $_GET['state'] ) && $_GET['state'] == 'thanks' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * To check if the URI requested is call email verification form.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */
function is_verify_account()
{
    if( isset( $_GET['state'] ) && $_GET['state'] == 'verify' && isset( $_GET['token'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * To check if the URI requested is call forget password form.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */
function is_forget_password()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'forget_password' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * To check if the URI contain redirection.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */
function is_redirect()
{
    if( !empty( $_GET['redirect'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * To check if the user already logi or not.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */
function is_user_logged()
{
    if( isset( $_COOKIE['user_id'] ) && isset( $_COOKIE['password'] ) && isset( $_COOKIE['thecookie'] ) )
    {
        if( md5( $_COOKIE['password'] . $_COOKIE['user_id'] ) == $_COOKIE['thecookie'] )
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
    }
    else
    {
        return false;
    }
}

/**
 * Login POST action and validate the input.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */

function post_login()
{
    if( count( $_POST ) > 0 )
    {
        return validate_login();
    }
}

/**
 * Validate the login input.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */
function validate_login()
{   
    if( empty( $_POST['username'] ) || empty( $_POST['password'] ) )
    {
        return "<div class=\"alert_red\">Empty Username or Password.</div>";
    }
    else
    {
        if( is_exist_user( $_POST['username'] ) && is_match_password() )
        {
            if( isset( $_POST['remember_login'] ) )
            {
        		$d = fetch_user( $_POST['username'] );

                setcookie( 'username', $_POST['username'], time() + 60 * 60 * 24 * 365, '/' );
                setcookie( 'password', $_POST['password'], time() + 60 * 60 * 24 * 365, '/' );
                setcookie( 'user_id', $d['luser_id'], time() + 60 * 60 * 24 * 365, '/' );
                setcookie( 'user_type', $d['lrole_id'], time() + 60 * 60 * 24 * 365, '/' );
                setcookie( 'user_name', $d['ldisplay_name'], time() + 60 * 60 * 24 * 365, '/' );	
                setcookie( 'thecookie', md5( $_POST['password'] . $d['luser_id'] ), time() + 60 * 60 * 24 * 365, '/' );                
            }
            else
            {
        		$d = fetch_user( $_POST['username'] );

                setcookie( 'username', $_POST['username'], false, '/' );
                setcookie( 'password', $_POST['password'], false, '/' );
                setcookie( 'user_id', $d['luser_id'], false, '/' );
                setcookie( 'user_type', $d['lrole_id'], false, '/' );
                setcookie( 'user_name', $d['ldisplay_name'], false, '/' );
                setcookie( 'thecookie', md5( $_POST['password'] . $d['luser_id'] ), false, '/' );
            }

            if( is_redirect() )
            {	                
                header( 'location:' . base64_decode( $_GET['redirect'] ) );
            }
            else
            {
                header( 'location:' . get_admin_url() . '/?state=dashboard' );
            }
        }
        else
        {
            return '<div class="alert_red">Wrong Username or Password.</div>';
        }
    }
}

/**
 * Logout action to destroy the cookie
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */
function do_logout()
{	    
    setcookie( 'username', '', time() - 3600, '/' );
    setcookie( 'password', '', time() - 3600, '/' );
    setcookie( 'user_id', '', time() - 3600, '/' );
    setcookie( 'user_type', '', time() - 3600, '/' );
    setcookie( 'user_name', '', time() - 3600, '/' );
    setcookie( 'thecookie', '', time() - 3600, '/' );
    
    header( 'location:' . get_state_url( 'login' ) );
}

/**
 * To check the if the user exist or not.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @param string $username username
 * 
 * @return boolean 
 */
function is_exist_user( $username )
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM l_users WHERE lusername = %s', $username );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * To check if the email address is exist or not.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @param string $email email address
 * 
 * @return boolean 
 */
function is_exist_email( $email )
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM l_users WHERE lemail = %s', $email );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * To count l_user table on database.
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return Integer number of users 
 */
function is_num_users()
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM l_users' );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/**
 * To check if the password that sent from the POST variable are match with the mention user name
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return boolean 
 */
function is_match_password()
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM l_users WHERE lusername=%s AND lpassword=%s AND lstatus = 1', $_POST['username'], md5( $_POST['password'] ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * Login Form Design
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return string login form html
 *  
 */
function get_login_form()
{
    set_template( TEMPLATE_PATH . '/login.html' );
    
    add_block( 'mainBlock', 'mBlock' );

    if( isset( $_COOKIE['username'] ) )
    {
        add_variable( 'username', $_COOKIE['username'] );
    }
    
    if( isset( $_COOKIE['password'] ) )
    {
        add_variable( 'password', $_COOKIE['password'] );
    }
    
    add_variable( 'web_title', web_title() );
    add_variable( 'alert', post_login() );
    add_variable( 'style_sheet', get_css( 'login.css' ) );
    add_variable( 'login_action', cur_pageURL() );
    add_variable( 'admin_url', get_admin_url() );
    add_variable( 'img_url', get_theme_img() );

    parse_template( 'mainBlock', 'mBlock' );
    print_template();
}

/**
 * The Sign Up URL
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return string Sign up URL 
 */
function signup_url()
{
    return HTSERVER . site_url() . '/l-admin/?state=register';
}

/**
 * The Sign In URL
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return string Sign In URL 
 */
function signin_url()
{
    return HTSERVER . site_url() . '/l-admin/?state=login&redirect=' . cur_pageURL();
}

/**
 * URL of user profile when login
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return string User  
 */
function user_url( $id )
{
    return HTSERVER . site_url() . '/l-admin/?state=my-profile&amp;id=' . $id;
}

/**
 * Call the Login Form Design
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return string Login HTML Design   
 */
function sign_in_form()
{	    
    set_template( TEMPLATE_PATH . '/login.html', 'sign_in_form' );

    add_block( 'signin_form', 'mBlock', 'sign_in_form' );
    
    if( isset( $_SERVER['HTTP_REFERER'] ) )
    {
        add_variable( 'action', get_admin_url() . '/?redirect=' . $_SERVER['HTTP_REFERER'] );
    }
    else
    {
        add_variable( 'action', get_admin_url() . '/' );
    }	    
    
    add_variable( 'signup_url', signup_url() );
    parse_template( 'signin_form', 'mBlock' );
    return return_template( 'sign_in_form' );
}

/**
 * Call the sign up form. This function is also checking if the user sign up are invited by other user
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return string Sign Up HTML Design  
 */
function signup_user()
{
    $alert     = '';
    $tokenmail = '';

    set_template( TEMPLATE_PATH . '/signup.html', 'signup_form' );

    add_block( 'signupBlock', 'sgnBlock', 'signup_form' );

    if( isset( $_GET['token'] ) )
    {
        $tokenmail = $_GET['token'];
    }
    
    if( isset( $_POST['signup'] ) )
    {
        $validation_user = is_valid_user_input( $_POST['username'], $_POST['first_name'], $_POST['last_name'], $_POST['password'], $_POST['repassword'], $_POST['email'], $_POST['sex'], "" );
        
        if( $validation_user == "OK" )
        {	            
            if( !empty( $_POST['birthday'] ) && !empty( $_POST['birthmonth'] ) && !empty( $_POST['birthyear'] ) )
            {
                $thebirthday = $_POST['birthmonth'] . "/" . $_POST['birthday'] . "/" . $_POST['birthyear'];
                $thebirthday = date( "Y-m-d", strtotime( $thebirthday ) );
            }
            else
            {
                $thebirthday = "0000-00-00";
            }	            
            
            $display_name = $_POST['first_name'] . " " . $_POST['last_name'];
            
            if( save_user( $_POST['username'], $_POST['password'], $_POST['email'], $_POST['sex'], "standard", $thebirthday, 0, $display_name ) )
            {
                $user_id = $db->insert_id();
                
                $invite_limit = get_meta_data( "invitation_limit" );

                add_additional_field( $user_id, "invite_limit", $invite_limit, "user" );
                add_additional_field( $user_id, 'first_name', $_POST['first_name'], 'user' );
                add_additional_field( $user_id, 'last_name', $_POST['last_name'], 'user' );
                
                $inviter_id = 0;
                
                if( isset( $_GET['iid'] ) && isset( $_GET['ie'] ) )
                {
                    $inviter_id  = $_GET['iid'];
                    $invitr_user = fetch_user( $_GET['iid'] );

                    if( isset( $invitr_user['luser_id'] ) )
                    {
                        $rel           = add_friendship( $_GET['iid'], $user_id, 'pending' );
                        $friendship_id = $db->insert_id();
                        
                        if( $rel )
                        {
                            $rel = add_friendship( $user_id, $_GET['iid'], 'onrequest' );

                            if( $rel )
                            {
                                if( isset( $_GET['enc_ulid'] ) )
                                {
                                    $dec_ulid         = base64_decode( $_GET['enc_ulid'] );
                                    $user_friend_list = get_friend_list( $_GET['iid'] );
                                    if( in_array( $dec_ulid, $user_friend_list['friends_list_id'] ) )
                                    {
                                        add_friend_list_rel( $friendship_id, $dec_ulid );
                                    }
                                }	                                
                            }
                        }
                    }
                }

                $token = md5( $_POST['username'] . $_POST['email'] . $_POST['password'] ) . "." . $tokenmail;
                $uep   = array(
					'key' => array(
						'username' => $_POST['username'],
						'email' => $_POST['email'],
						'password' => $_POST['password'],
						'token' => $token 
					) 
                );

                $uep   = base64_encode( json_encode( $uep ) );

                send_register_notification( $_POST['username'], $_POST['email'], $_POST['password'], $token, $inviter_id );

                header( "location:" . get_admin_url() . "/?state=thanks&uep=" . $uep );
            }
        }
        else
        {
            $alert = $validation_user;
        }
    }
    
    add_variable( 'web_title', web_title() );
    add_variable( 'style_sheet', get_css() );
    add_variable( 'jquery', get_javascript( 'jquery' ) );
    add_variable( 'alert', $alert );

    $sex  = "<select name=\"sex\">";
    $sex .= "<option value=\"\">Select Sex:</option>";
    $sexar = array(
		'1' => 'Male',
		'2' => 'Female' 
    );

    foreach( $sexar as $key => $val )
    {
        if( isset( $_POST['sex'] ) )
        {
            if( $key == $_POST['sex'] )
            {
                $sex .= "<option value=\"$key\" selected=\"selected\">$val</option>";
            }
        }

        $sex .= "<option value=\"$key\">$val</option>";
    }
    $sex .= "</select>";

    add_variable( 'sex', $sex );

    if( isset( $_POST['username'] ) )
    {
        add_variable( 'username', $_POST['username'] );
    }

    if( isset( $_POST['first_name'] ) )
    {
        add_variable( 'first_name', $_POST['first_name'] );
    }

    if( isset( $_POST['last_name'] ) )
    {
        add_variable( 'last_name', $_POST['last_name'] );
    }

    if( isset( $_POST['email'] ) )
    {
        add_variable( 'email', $_POST['email'] );
    }

    if( isset( $_GET['ie'] ) )
    {
        add_variable( 'email', base64_decode( $_GET['ie'] ) );
    }
    
    $birthday   = ( isset( $_POST['birthday'] ) ) ? $_POST['birthday'] : "";
    $birthmonth = ( isset( $_POST['birthmonth'] ) ) ? $_POST['birthmonth'] : "";
    $birthyear  = ( isset( $_POST['birthyear'] ) ) ? $_POST['birthyear'] : "";

    get_date_picker( "tail", $birthday, $birthmonth, $birthyear );

    if( isset( $_GET['iid'] ) && isset( $_GET['ie'] ) )
    {
        $invitr_user = fetch_user( $_GET['iid'] );

        if( isset( $invitr_user['luser_id'] ) )
        {
            $ihtml = "
            <tr>
    			<td><img src='" . get_avatar( $invitr_user['luser_id'], 2 ) . "' alt=\"" . $invitr_user['ldisplay_name'] . "\" title=\"" . $invitr_user['ldisplay_name'] . "\" /></td>
    			<td>
    				 <h2>You've been invited by " . $invitr_user['ldisplay_name'] . "</h2>
    				 Join " . web_name() . " and <strong>connect with your friends</strong>,<br /> 
    				 <strong>share news to others</strong> or <strong>share privately </strong> only to <br /> your friend list
    			</td>
    		</tr>";
            add_variable( 'inviter', $ihtml );
        }	        
    }
    
    add_variable( 'tail', attemp_actions( 'tail' ) );

    parse_template( 'signupBlock', 'sgnBlock' );
    print_template( 'signup_form' );
}

/**
 * Password reseter form. In this function the sending process also executed here 
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return string Resend HTML Design  
 */
function resendPassword()
{
    if( !isset( $_GET['uep'] ) )
    {
        header( "location:" . get_admin_url() . "/?state=login" );
    }
    
    $uep = base64_decode( $_GET['uep'] );
    $uep = json_decode( $uep, true );
    
    if( !is_array( $uep ) )
    {
        header( "location:" . get_admin_url() . "/?state=login" );
    }

    if( isset( $_POST['resend'] ) )
    {
        $token = $uep['key']['token'];
        
        send_register_notification( $uep['key']['username'], $uep['key']['email'], $uep['key']['password'], $token );
        add_variable( 'resendto', " Resent to: " . $uep['key']['email'] );

        parse_template( 'thanksPage', 'thxBlock' );
        print_template( 'signup_form' );
    }

    set_template( TEMPLATE_PATH . "/resendPassword.html", "resend" );

    add_block( 'thanksPage', 'thxBlock', "resend" );
    
    add_variable( 'web_title', web_title() );
    add_variable( 'style_sheet', get_css() );
    add_variable( 'email', $uep['key']['email'] );
    add_variable( 'username', $uep['key']['username'] );
    add_variable( 'password', $uep['key']['password'] );

    parse_template( 'thanksPage', 'thxBlock' );
    print_template( 'resend' );    
}

/**
 * Function to verify email activation process  
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @return string The activation process status and the login form when active  
 */
function verify_account()
{
    global $db;

    set_template( TEMPLATE_PATH . "/verifyAccount.html", "verify" );

    add_block( 'verifyPage', 'vrfBlock', "verify" );
    
    if( !isset( $_GET['token'] ) )
    {
        header( "location:" . get_admin_url() . "/?state=login" );
    }
    
    $themail = explode( ".", $_GET['token'] );
    
    $sql          = $db->prepare_query( "SELECT * FROM l_users WHERE lactivation_key=%s AND lstatus=0", $themail[0] );
    $r            = $db->do_query( $sql );
    $invited_user = $db->fetch_array( $r );
    
    if( $db->num_rows( $r ) > 0 )
    {
        $query  = $db->prepare_query( "UPDATE l_users SET lstatus=1 WHERE lactivation_key=%s", $themail[0] );
        $result = $db->do_query( $query );

        if( $result )
        {
            add_friend_list( $invited_user['luser_id'], "Work" );
            add_friend_list( $invited_user['luser_id'], "School" );
            add_friend_list( $invited_user['luser_id'], "Family" );
            
            if( !empty( $themail[1] ) )
            {
                $query                 = $db->prepare_query( "UPDATE l_comments SET lcomment_status='approved' WHERE lcomentator_email=%s", base64_decode( $themail[1] ) );
                $update_comment_status = $db->do_query( $query );
            }

            $status = "
            <div class=\"alert_yellow\">
				Activation process succeeded, thanks for doing the activation. 
				Please sign in using the form below:
			</div>";
            $status .= "<h2>Sign In</h2>";
            $status .= "
            <form method=\"post\" action=\"" . get_admin_url() . "/\">
				<table cellspacing=\"0\" >
					<tr>
						<td>Username:</td>
						<td><input type=\"text\" name=\"username\" class=\"inputtext\" style=\"width:300px;\"  /></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type=\"password\" name=\"password\" class=\"inputtext\" style=\"width:300px;\"  /></td>
					</tr>
					<tr>
						<td></td>
						<td><input type=\"submit\" value=\"Sign In\" name=\"login\" class=\"button\" /></td>
					</tr>
				</table>
			</form>";

            if( isset( $_GET['iid'] ) && !empty( $_GET['iid'] ) )
            {	                
                $invitr_user = fetch_user( $_GET['iid'] );

                add_friend_to_admin( $invited_user['luser_id'], $_GET['iid'] );

                $edit_f = edit_friendship( $_GET['iid'], $invited_user['luser_id'], 'connected', true );

                if( $edit_f )
                {
                    request_approved_mail( $invitr_user['lemail'], $invitr_user['ldisplay_name'], $invited_user['luser_id'], $invited_user['lsex'] );
                }
            }
            else
            {
                add_friend_to_admin( $invited_user['luser_id'] );
            }	            
        }
    }
    else
    {
        $status = "<p>You have done the activation process before.</p>";
    }

    add_variable( 'web_title', web_title() );
    add_variable( 'style_sheet', get_css() );
    add_variable( 'status', $status );

    parse_template( 'verifyPage', 'vrfBlock' );
    print_template( 'verify' );
}

/**
 * Function to handle forgot password action 
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * 
 * 
 * @return string Action status  
 */
function post_forget_password()
{
    if( count( $_POST ) > 0 )
    {
        return validate_forget_password();
    }
    else
    {
        return "<div class=\"alert_yellow\">Please enter your username or e-mail address. You will receive a new password via e-mail.</div>";
    }
}

/**
 * Function to check the email input, when request a new password
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * 
 * 
 * @return string Action status  
 */
function validate_forget_password()
{
    if( empty( $_POST['user_email'] ) )
    {
        return "<div class=\"alert_yellow\">Please enter your username or e-mail address. You will receive a new password via e-mail.</div>";
    }
    else
    {
        if( is_exist_email( $_POST['user_email'] ) )
        {	            
            $new_password = random_string();
            
            $user_id = fetch_user_ID_by_email( $_POST['user_email'] );
            $user    = fetch_user( $user_id );
            
            if( reset_user_password( $user_id, $new_password ) )
            {
                $return = reset_password_email( $_POST['user_email'], $user['lusername'], $user['ldisplay_name'], $new_password );
            }
            else
            {
                return false;
            }	            
        }
        elseif( is_exist_user( $_POST['user_email'] ) )
        {
            
            $new_password = random_string();
            
            $user_id = fetch_user_ID_by_username( $_POST['user_email'] );
            
            $user = fetch_user( $user_id );
            
            if( reset_user_password( $user_id, $new_password ) )
            {
                $return = reset_password_email( $user['lemail'], $user['lusername'], $user['ldisplay_name'], $new_password );
            }
            else
            {
                return false;
            }	            
        }
        else
        {
            return "<div class=\"alert_yellow\">Please enter your valid username or e-mail address.</div>";
        }
        
        if( $return )
        {
            return "<div class=\"alert_yellow\">Your password has been reseted. Please check your email.</div>";
        }	        
    }
}

/**
 * Call Forget Password HTML Design 
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * 
 * 
 * @return string HTML Design  
 */
function get_forget_password_form()
{
    set_template( TEMPLATE_PATH . '/forget_password.html' );

    add_block( 'mainBlock', 'mBlock' );

    add_variable( 'web_title', web_title() );
    add_variable( 'alert', post_forget_password() );
    add_variable( 'style_sheet', get_css( 'login.css' ) );
    add_variable( 'login_action', cur_pageURL() );
    add_variable( 'admin_url', get_admin_url() );
    add_variable( 'img_url', get_theme_img() );

    parse_template( 'mainBlock', 'mBlock' );
    print_template();
}

/**
 * Is used in admin area, to get the list of registered user. Navigation button are configured here. 
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * 
 * 
 * @return string list of registered user in HTML  
 */
function get_users_list( $tabs = '' )
{
    global $db;

    $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
    $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';
	$site_url = site_url();

    if( is_search() )
    {
    	$s = 'SELECT * FROM l_users WHERE lusername LIKE %s OR ldisplay_name LIKE %s OR lemail=%s';
        $q = $db->prepare_query( $s, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $_POST['s'] );
        $r = $db->do_query( $q );
    }
    else
    {
    	$s = 'SELECT * FROM l_users';
        $q = $db->prepare_query( $s);
        $r = $db->do_query( $q );
    }	    

    set_template( TEMPLATE_PATH . '/template/users/list.html', 'users-template' );

    add_block( 'list-block', 'l-block', 'users-template' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', users_list( $r ));
    add_variable( 'action', get_state_url( $state . $substate ) );
    add_variable( 'add_new_link', get_state_url( $state . $substate . '&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( $state . $substate . '&prc=edit' ) );

    add_actions( 'section_title', 'Users' );

    parse_template( 'list-block', 'l-block', false );

    return return_template( 'users-template' ); 
}

/**
 * Is used in admin area, to get the detail list of registered user.  
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * 
 * 
 * @return string list of registered user in HTML  
 */
function users_list( $r )
{
    global $db;

    $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
    $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';
    
    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="6">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( TEMPLATE_PATH . '/template/users/loop.html', 'users-loop' );

    add_block( 'loop-block', 'lloop', 'users-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'id', $d['luser_id'] );
        add_variable( 'email', $d['lemail'] );
        add_variable( 'phone', $d['lphone'] );
        add_variable( 'username', $d['lusername'] );
        add_variable( 'display_name', $d['ldisplay_name'] );
        add_variable( 'status', $d['lstatus'] == 0 ? 'Pending' : ( $d['lstatus'] == 1 ? 'Active' : 'Blocked' ) );
        add_variable( 'role', get_role( $d['lrole_id'], 'lrole_name' ) );
        add_variable( 'profile', user_url( $d['luser_id'] ) );
        add_variable( 'avatar', get_avatar( $d['luser_id'], 3 ) );
        add_variable( 'my_profile', get_state_url( 'my-profile' ) );
        add_variable( 'edit_link', get_state_url( $state . $substate . '&prc=edit&id=' . $d['luser_id'] ) );

        parse_template( 'loop-block', 'lloop', true );
    }

	return return_template( 'users-loop' );
}

/**
 * Will return the user type in array   
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * 
 * 
 * @return array type of user  
 */
function user_type()
{
    global $db;

    $user_type = array(
        'standard' => 'Standard',
        'contributor' => 'Contributor',
        'author' => 'Author',
        'editor' => 'Editor',
        'administrator' => 'Administrator' 
    );

    $s = 'SELECT * FROM l_meta_data WHERE lapp_name = %s';
    $q = $db->prepare_query( $s, 'user_privileges' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $value = json_decode($d['lmeta_value'], true);
            $user_type[$d['lmeta_name']] = $value['name'];
        }
    }

    return $user_type;
}

/**
 * Save user to database   
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @param string $username unique username
 * @param string $password unique password
 * @param string $email email address
 * @param integer $sex 1=Male, 2=Female
 * @param string $user_type Default user type are: standard, contributor, author, editor, administrator
 * @param string $birthday The date format is Y-m-d H:i:s
 * @param string $status the user status(0=pendding activation,1=active, 2=blocked)
 * 
 * @return boolean True if the insert process is success  
 */
function save_user( $username = '', $display_name = '', $password = '', $email = '', $role = '', $phone = '', $status = 0, $user_privilage=array() )
{
    global $db;

    $regdate        = date( 'Y-m-d H:i:s' );
    $activation_key = md5( $username . $email . $password );
    $display_name   = empty( $display_name ) ? $username : $display_name;
    $u_priv         = is_array( $user_privilage ) && !empty( $user_privilage ) ? json_encode( $user_privilage ) : '';

    $s = 'INSERT INTO l_users( 
			lrole_id,
			lusername, 
			ldisplay_name,
			lpassword, 
			lemail,
			lphone,
			lregistration_date,
			lactivation_key, 
			lstatus,
			ldlu,
            luser_privilage)
		   VALUES ( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
    $q = $db->prepare_query( $s, $role, $username, $display_name, md5( $password ), $email, $phone, $regdate, $activation_key, $status, $regdate, $u_priv );
    
    return $db->do_query( $q );	    
}

/**
 * Automatically add new user as administrator firend   
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @param integer $friend_id The id of the new user 
 * 
 * 
 * @return boolean True if the insert process is success  
 */
function add_friend_to_admin( $friend_id, $inviter = 0 )
{
    global $db;
    
    $administrator = fetch_user_per_type( 'administrator' );

    foreach( $administrator as $key => $value )
    {
        if( isset( $inviter ) )
        {
            if( inviter != $value )
            {
                $return = add_friendship( $value, $friend_id, 'connected' );

                if( $return )
                {
                    $return = add_friendship( $friend_id, $value, 'connected' );
                }
            }
            
        }
        else
        {
            $return = add_friendship( $value, $friend_id, 'connected' );

            if( $return )
            {
                $return = add_friendship( $friend_id, $value, 'connected' );
            }
        }
    }
    return $return;
}

/**
 * Update user password to DB   
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @param integer $user_id User ID 
 * @param password $new_password New Password
 * 
 * @return boolean True if the reset process is success  
 */
function reset_user_password( $user_id, $new_password )
{
    global $db;

    if( empty( $user_id ) || empty( $new_password ) )
    {
        return;
    }
    
    $sql = $db->prepare_query( "UPDATE l_users SET lpassword=%s WHERE luser_id=%d", md5( $new_password ), $user_id );
    
    return $db->do_query( $sql );
}

/**
 * Used to edit user database   
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @param integer $id edited user ID
 * @param string $display_name User display name
 * @param string $password New password if any change
 * @param string $email New email address if applicable
 * @param integer $sex 1=Male, 2=Female
 * @param string $user_type Default user type are: standard, contributor, author, editor, administrator
 * @param string $birthday The date format is Y-m-d H:i:s
 * @param string $status the user status(0=pendding activation,1=active, 2=blocked)
 * 
 * @return boolean True if the insert process is success  
 */
function edit_user( $id, $display_name = '', $password = '', $email = '', $role = '', $phone = '', $status = 0, $user_privilage=array() )
{
    global $db;

    $u_priv = '';

    if ( is_array( $user_privilage ) && !empty( $user_privilage ) ) 
    {
        $u_priv = $db->prepare_query( ',luser_privilage=%s', json_encode( $user_privilage ) );
    }

    if( empty( $password ) )
    {
    	$s = 'UPDATE l_users SET 
				lrole_id = %s,
				ldisplay_name = %s,
				lphone = %s,
				lstatus = %d,
				ldlu = %s'
                . $u_priv .
			  ' WHERE luser_id = %d';
        $q = $db->prepare_query( $s, $role, $display_name, $phone, $status, date( 'Y-m-d H:i:s' ), $id );
    }
    else
    {
    	$s = 'UPDATE l_users SET 
				lrole_id = %s,
				ldisplay_name = %s,
				lphone = %s,
				lpassword = %s,
				lstatus = %d,
				ldlu = %s'
                . $u_priv .
			 'WHERE luser_id = %d';
        $q = $db->prepare_query( $s, $role, $display_name, $phone, md5( $password ), $status, date( 'Y-m-d H:i:s' ), $id );
    }
    
    return $db->do_query( $q );
}

/**
 * Delete user from database   
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @param integer $id deleted user ID
 * 
 * @return boolean True if the insert process is success  
 */
function delete_user( $id )
{
    global $db;

    if( $id != 1 )
    {
        $q = $db->prepare_query( 'DELETE FROM l_users WHERE luser_id=%d', $id );
        
        if( is_user_logged() )
        {
            if( $db->do_query( $q ) )
            {
                return delete_friendship( $id );
            }
        }
        else
        {
            return $db->do_query( $q );
        }
    }
    else
    {
    	return false;
    }
}

/**
 * Validate the user input   
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 * @param string $username unique username
 * @param string $password unique password
 * @param string $re_password must have same value with password
 * @param string $email Email address
 * @param integer $sex 1=Male, 2=Female
 * @param string $website User website address
 * 
 * @return string when data are good return OK, if not return the alert text  
 */
function is_valid_user_input( $username, $display_name, $password, $re_password, $email, $role )
{
	$error = array();

    if( empty( $username ) )
    {
        $error[] = 'Please specifiy your username';
    }
    else
    {
    	if( strlen( $username ) < 5 )
	    {
	        $error[] = 'Username <em>' . $username . '</em> should be at least five characters long';
	    }

	    if( is_exist_user( $username ) && ( is_add_new() || is_register_form() ) )
	    {
	        $error[] = 'Username <em>' . $username . '</em> is not available, please try another username';
	    }
    }

    if( empty( $email ) )
    {
        $error[] = 'Please specifiy your email';
    }
    else
    {
	    if( is_exist_email( $email ) && ( is_add_new() || is_register_form() ) )
	    {
	        $error[] = 'This email is already taken. Please choose another email';
	    }

	    if( !isEmailAddress( $email ) )
	    {
	        $error[] = 'Invalid email format ( <em>' . $email . '</em> )';
	    }
    }

    if( empty( $role ) )
    {
        $error[] = 'Please specifiy your user role';
    }

    if( is_add_new() || is_category( 'appname=register' ) || $_GET['state'] == 'register' )
    {
        if( empty( $password ) || strlen( $password ) < 7 )
        {
        	$error[] = 'The password should be at least seven characters long';
        }
        
        if( $password != $re_password )
        {
        	$error[] = 'Password do not match';
        }
    }
    elseif( is_edit() || is_edit_all() )
    {
        if( !empty( $password ) )
        {
            if( strlen( $password ) < 7 )
            {
        		$error[] = 'Password should be at least seven characters long';
            }
            elseif( $password != $re_password )
            {
        		$error[] = 'Password do not match';
            }
        }	        
    }

    return $error;
}

/**
 * Each user can edit his/her user profile when they login. This function will be called when they want to edit the profile.
 * All process are happen here.    
 *
 * @author Wahya Biantara
 *
 * @since alpha
 * 
 *  
 * @return string The HTML design of edit profile  
 */
function edit_profile()
{
    global $db;

    run_update_profile();

    $data = get_admin_user_data( $_COOKIE['user_id'] );

    set_template( TEMPLATE_PATH . '/template/users/profile.html', 'profile' );

    add_block( 'profile-block', 'p-block', 'profile' );

    add_variable( 'lemail', $data['lemail'] );
    add_variable( 'lphone', $data['lphone'] );
    add_variable( 'lstatus', $data['lstatus'] );
    add_variable( 'luser_id', $data['luser_id'] );
    add_variable( 'lrole_id', $data['lrole_id'] );
    add_variable( 'lusername', $data['lusername'] );
    add_variable( 'lpassword', $data['lpassword'] );
    add_variable( 'lrpassword', $data['lrpassword'] );
    add_variable( 'ldisplay_name', $data['ldisplay_name'] );

    add_variable( 'is_email_disabled', 'readonly' );
    add_variable( 'is_username_disabled', 'readonly' );

    add_variable( 'message', generate_message_block() );

    add_actions( 'section_title', 'My Profile' );
    
    parse_template( 'profile-block', 'p-block' );
    
    return return_template( 'profile' );
}

function run_update_profile()
{
    global $flash;

    if( is_save_changes() )
    {
        $error = is_valid_user_input( $_POST['lusername'], $_POST['ldisplay_name'], $_POST['lpassword'], $_POST['lrpassword'], $_POST['lemail'], $_POST['lrole_id'] );              

        if( empty( $error ) )
        {
            if( edit_user( $_POST['luser_id'], $_POST['ldisplay_name'], $_POST['lpassword'], $_POST['lemail'], $_POST['lrole_id'], $_POST['lphone'], $_POST['lstatus'] ) )
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This admin users successfully edited' ) ) );

                header( 'location:' . get_state_url( 'users&tab=my-profile' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

/**
 * This function is used to view the user profile. If you click your friend profile, then this function will be called
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return string The profile HTML design   
 */
function user_profile( $user_id )
{
    $user       = fetch_user( $user_id );
    $website    = get_additional_field( $user_id, 'website', 'user' );
    $first_name = get_additional_field( $user_id, 'first_name', 'user' );
    $last_name  = get_additional_field( $user_id, 'last_name', 'user' );
    $one_liner  = get_additional_field( $user_id, 'one_liner', 'user' );
    $location   = get_additional_field( $user_id, 'location', 'user' );
    $bio        = get_additional_field( $user_id, 'bio', 'user' );
    
    $html = "<h2>Basic Information</h2>";
    $html .= "<table width='100%' cellpadding='0' cellspacing='0'>";
    
    //First Name
    if( !empty( $first_name ) )
    {
        $html .= "<tr>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right'>";
        $html .= "<strong>Frist Name</strong>";
        $html .= "</td>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;'>";
        $html .= $first_name;
        $html .= "</td>";
        $html .= "</tr>";
    }
    //Last Name
    if( !empty( $last_name ) )
    {
        $html .= "<tr>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right'>";
        $html .= "<strong>Last Name</strong>";
        $html .= "</td>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;'>";
        $html .= $last_name;
        $html .= "</td>";
        $html .= "</tr>";
    }
    
    //One Liner
    if( !empty( $one_liner ) )
    {
        $html .= "<tr>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right'>";
        $html .= "<strong>One Liner</strong>";
        $html .= "</td>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;'>";
        $html .= $one_liner;
        $html .= "</td>";
        $html .= "</tr>";
    }
    
    //Location
    if( !empty( $location ) )
    {
        $html .= "<tr>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right'>";
        $html .= "<strong>Location</strong>";
        $html .= "</td>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;'>";
        $html .= $location;
        $html .= "</td>";
        $html .= "</tr>";
    }
    //Sex
    if( !empty( $user['lsex'] ) )
    {
        $html .= "<tr>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right'>";
        $html .= "<strong>Sex</strong>";
        $html .= "</td>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;'>";
        $html .= ( $user['lsex'] == 1 ) ? 'Male' : 'Female';
        $html .= "</td>";
        $html .= "</tr>";
    }
    //Bio
    if( !empty( $bio ) )
    {
        $html .= "<tr>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right'>";
        $html .= "<strong>Short Bio</strong>";
        $html .= "</td>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;'>";
        $html .= $bio;
        $html .= "</td>";
        $html .= "</tr>";
    }
    $html .= "</table>";
    $html .= "<h2>Contact Information</h2>";
    $html .= "<table width='100%' cellpadding='0' cellspacing='0'>";
    if( !empty( $user['lemail'] ) )
    {
        $html .= "<tr>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right'>";
        $html .= "<strong>Email</strong>";
        $html .= "</td>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;'>";
        $html .= $user['lemail'];
        $html .= "</td>";
        $html .= "</tr>";
    }
    
    if( $website != HTSERVER )
    {
        $html .= "<tr>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right'>";
        $html .= "<strong>Website</strong>";
        $html .= "</td>";
        $html .= "<td style='padding:5px 10px;border-bottom:1px solid #f0f0f0;'>";
        $html .= "<a href=\"" . $website . "\" rel=\"nofollow\">" . $website . "</a>";
        $html .= "</td>";
        $html .= "</tr>";
    }
    
    $html .= "</table>";
    
    $school  = get_eduwork( 'school', $user_id );
    $college = get_eduwork( 'college', $user_id );
    $work    = get_eduwork( 'work', $user_id );
    if( !empty( $school ) || !empty( $college ) || !empty( $work ) )
    {
        $html .= "<h2>Education &amp; Work</h2>";
        $html .= "<table width='100%' cellpadding='0' cellspacing='0'>";
        if( !empty( $work ) )
        {
            $html .= "<tr>";
            $html .= "<td style='padding:10px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right' valign='top'>";
            $html .= "<strong>Work</strong>";
            $html .= "</td>";
            $html .= "<td style='padding:0px 10px;border-bottom:1px solid #f0f0f0;'>";
            $html .= $work;
            $html .= "</td>";
            $html .= "</tr>";
        }
        if( !empty( $college ) )
        {
            $html .= "<tr>";
            $html .= "<td style='padding:10px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right' valign='top'>";
            $html .= "<strong>Grad School</strong>";
            $html .= "</td>";
            $html .= "<td style='padding:0px 10px;border-bottom:1px solid #f0f0f0;'>";
            $html .= $college;
            $html .= "</td>";
            $html .= "</tr>";
        }
        if( !empty( $school ) )
        {
            $html .= "<tr>";
            $html .= "<td style='padding:10px 10px;border-bottom:1px solid #f0f0f0;width:100px;' align='right' valign='top'>";
            $html .= "<strong>High School</strong>";
            $html .= "</td>";
            $html .= "<td style='padding:0px 10px;border-bottom:1px solid #f0f0f0;'>";
            $html .= $school;
            $html .= "</td>";
            $html .= "</tr>";
        }
        $html .= "</table>";
    }
    return $html;
}

/**
 * This function is used to show the user feeds update. 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return string Selected user Feeds   
 */
function user_updates()
{
    $is_friend      = false;
    $status_privacy = false;
    
    $content_left = '';
    
    if( !is_user_logged() )
        return;
    global $db;
    
    if( !empty( $_GET['id'] ) )
    {
        $id = $_GET['id'];
        $id = kses( $id, array ());
        if( is_my_friend( $_COOKIE['user_id'], $id ) || is_my_friend( $_COOKIE['user_id'], $id, 'unfollow' ) )
        {
            $query = $db->prepare_query( "SELECT * FROM l_articles 
										 WHERE larticle_status='publish' AND
										 lpost_by = %d AND
										 	( lshare_to in (
										 		SELECT a.lfriends_list_id
												FROM l_friends_list_rel a, l_friendship b
												WHERE a.lfriendship_id=b.lfriendship_id AND b.lfriend_id=%d and lstatus='connected'
										 	    )OR lshare_to=0
										 	)
										 ORDER BY lpost_date DESC", $id, $_COOKIE['user_id'] );
            
            $feed_type = 'friend_feed';
            $is_friend = true;
        }
        else
        {
            $query     = $db->prepare_query( "SELECT * FROM l_articles 
										 WHERE larticle_status='publish' AND
										 lpost_by = %d AND
										 lshare_to=0
										 ORDER BY lpost_date DESC", $id );
            $feed_type = 'everyone_friend_feed';
            
        }
        
        
        
    }
    else
    {
        
        $id        = $_COOKIE['user_id'];
        $query     = $db->prepare_query( "SELECT * FROM l_articles 
										 WHERE larticle_status='publish' AND
										 lpost_by = %d
										 ORDER BY lpost_date DESC", $id );
        $feed_type = 'my_feeds';
        $is_friend = true;
    }
    
    
    $result = $db->do_query( $query );
    $data   = $db->fetch_array( $result );
    
    if( !$is_friend ) //IF Not Friend
    {
        if( status_privacy( $id ) == 'public' )
            $status_privacy = true;
    }
    else
    {
        $status_privacy = true;
    }
    
    if( is_administrator( $_COOKIE['user_id'] ) )
        $status_privacy = true;
    
    
    if( $status_privacy )
        $content_left = dashboard_latest_update( $query, get_meta_data( 'comment_per_page' ), false, $feed_type );
    
    $user = fetch_user( $id );
    add_actions( 'section_title', ucwords( $user['ldisplay_name'] ) );
    $add_friend_button = "";
    
    if( $id == $_COOKIE['user_id'] )
    {
        ///$tabs=array('my-updates'=>'My Updates','my-profile'=>'My Profile','profile-picture'=>'Profile Picture','eduwork'=>'Education & Work');
        $tabs = array(
             'my-updates' => 'My Updates',
            'my-profile' => 'My Profile',
            'profile-picture' => 'Profile Picture' 
        );
        if( isset( $_GET['tab'] ) )
            $selected_tab = $_GET['tab'];
        else
            $selected_tab = 'my-updates';
        
        $the_tabs = set_tabs( $tabs, $selected_tab );
        
    }
    else
    {
        $the_tabs = '';
        $id       = kses( $_GET['id'], array ());
        if( isset( $_GET['tab'] ) && $_GET['tab'] == 'profile' )
        {
            $content_left = user_profile( $id );
            
            if( $status_privacy )
            {
                $the_tabs = "<li><a href=\"" . get_state_url( 'my-profile' ) . "&id=" . $id . "\">" . ucwords( $user['ldisplay_name'] ) . " Updates</a></li>";
                $the_tabs .= "<li class=\"active\"><a href=\"" . get_state_url( 'my-profile' ) . "&tab=profile&id=" . $id . "\">Profile</a></li>";
            }
            else
            {
                $the_tabs = "<li class=\"active\"><a href=\"" . get_state_url( 'my-profile' ) . "&tab=profile&id=" . $id . "\">Profile</a></li>";
            }
            
        }
        else
        {
            if( $status_privacy )
            {
                $the_tabs = "<li class=\"active\"><a href=\"" . get_state_url( 'my-profile' ) . "&id=" . $id . "\">" . ucwords( $user['ldisplay_name'] ) . " Updates</a></li>";
                $the_tabs .= "<li><a href=\"" . get_state_url( 'my-profile' ) . "&tab=profile&id=" . $id . "\">Profile</a></li>";
            }
            else
            {
                $content_left = user_profile( $id );
                $the_tabs .= "<li class=\"active\"><a href=\"" . get_state_url( 'my-profile' ) . "&tab=profile&id=" . $id . "\">Profile</a></li>";
            }
        }
        
        if( !is_my_friend( $_COOKIE['user_id'], $id, 'connected' ) )
        {
            $add_friend_button = add_friend_button( $id, 0 );
        }
        
        if( is_my_friend( $_COOKIE['user_id'], $id, 'connected' ) )
        {
            if( !is_administrator( $id ) )
            {
                $friendship        = search_friendship( $_COOKIE['user_id'], $id );
                $add_friend_button = add_friend_button( $id, $friendship['friendship_id'][0], 'unfollow' );
            }
        }
        
        if( is_my_friend( $_COOKIE['user_id'], $id, 'pending' ) )
            $add_friend_button = "
			<div style='margin-bottom:10px;'>
				" . colek_button( $id ) . "&nbsp;
				<span style='color:#cacaca;'>Friend request pending.</span>
			</div>";
        
        if( is_my_friend( $_COOKIE['user_id'], $id, 'onrequest' ) )
        {
            $friendship        = search_friendship( $_COOKIE['user_id'], $id );
            $add_friend_button = add_friend_button( $id, $friendship['friendship_id'][0], 'confirm' );
        }
        if( is_my_friend( $_COOKIE['user_id'], $id, 'unfollow' ) )
        {
            $friendship        = search_friendship( $_COOKIE['user_id'], $id );
            $add_friend_button = add_friend_button( $id, $friendship['friendship_id'][0], 'follow' );
        }
    }
    
    
    
    $friends_html = friend_thumb_list( $id );
    
    
    $alert = '';
    
    
    
    $img = '<div style="text-align:left;width:100%;overflow:hidden;margin-bottom:10px;">
        		<img src="' . get_avatar( $id, 1 ) . '" title="' . $user['ldisplay_name'] . '" alt="' . $user['ldisplay_name'] . '" style="" />
        		<h2 style="color:#ef451e;">' . ucwords( $user['ldisplay_name'] ) . '</h2>
        	</div>';
    
    
    $content = "<h1>" . ucwords( $user['ldisplay_name'] ) . "</h1>";
    $content .= "<ul class=\"tabs\">
   				$the_tabs
			</ul>";
    $content .= "<div class=\"tab_container\">";
    $content .= "<div  style=\"margin:10px 0 10px 5px;\">";
    $content .= "<div id=\"dashboard_left\">";
    $content .= "<div class=\"home_plug1\">" . $content_left . "</div>";
    $content .= "</div>";
    $content .= "<div id=\"profile_right\" >$img";
    $content .= "<div style=\"margin:10px 0;\" class=\"clearfix\">";
    $content .= "<h2>Tagged as</h2>";
    $content .= get_user_tags( $id );
    $content .= "</div>";
    $content .= "<div style=\"background:#f0f0f0;border-bottom:1px solid #ccc;margin-bottom:10px;padding:3px;text-align:right;\"></div>";
    $content .= $add_friend_button;
    $content .= $friends_html;
    $content .= attemp_actions( 'user_updates_right' );
    $content .= "</div>";
    $content .= "</div>";
    $content .= "</div>";
    return $content;
}

/**
 * Edit the user picture profile 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return string The picture profile HTML Design    
 */
function edit_profile_picture()
{
    global $db;
    $thealert = "";
    //if upload actions
    if( isset( $_POST['upload'] ) )
    {
        
        $file_name   = $_FILES['theavatar']['name'];
        $file_size   = $_FILES['theavatar']['size'];
        $file_type   = $_FILES['theavatar']['type'];
        $file_source = $_FILES['theavatar']['tmp_name'];
        
        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {
                $fix_file_name = file_name_filter( $_COOKIE['username'] );
                $file_ext      = file_name_filter( $file_name, true );
                
                $file_name_1 = $fix_file_name . '-1' . $file_ext;
                $file_name_2 = $fix_file_name . '-2' . $file_ext;
                $file_name_3 = $fix_file_name . '-3' . $file_ext;
                $thefilename = $file_name_1 . '|' . $file_name_2 . '|' . $file_name_3;
                
                $destination1 = FILES_PATH . "/users/" . $file_name_1;
                $destination2 = FILES_PATH . "/users/" . $file_name_2;
                $destination3 = FILES_PATH . "/users/" . $file_name_3;
                
                //upload_resize($file_source, $destination3, $file_type, 32,38);
                //upload_resize($file_source, $destination2, $file_type, 50,60);
                //upload_resize($file_source, $destination1, $file_type, 250,300);
                
                upload_crop( $file_source, $destination3, $file_type, 32, 32 );
                upload_crop( $file_source, $destination2, $file_type, 50, 50 );
                upload_crop( $file_source, $destination1, $file_type, 250, 300 );
                
                $sql = $db->prepare_query( "UPDATE l_users
	                                                       SET lavatar=%s
	                                                       WHERE luser_id=%d", $thefilename, $_COOKIE['user_id'] );
                $r   = $db->do_query( $sql );
                header( "location:" . cur_pageURL() );
            }
            else
            {
                $thealert = "<div class=\"alert_yellow\">The maximum file size is 2MB</div>";
            }
        }
        else
        {
            $thealert = "<div class=\"alert_yellow\">The maximum file size is 2MB</div>";
            
        }
    }
    
    ///$tabs=array('my-updates'=>'My Updates','my-profile'=>'My Profile','profile-picture'=>'Profile Picture','eduwork'=>'Education & Work');
    $tabs = array(
         'my-updates' => 'My Updates',
        'my-profile' => 'My Profile',
        'profile-picture' => 'Profile Picture' 
    );
    if( isset( $_GET['tab'] ) )
        $selected_tab = $_GET['tab'];
    else
        $selected_tab = 'my-profile';
    
    
    $the_tabs = set_tabs( $tabs, $selected_tab );
    
    //set template
    set_template( TEMPLATE_PATH . "/users.html", 'users' );
    
    //set block
    add_block( 'usersEdit', 'uEdit', 'users' );
    add_block( 'usersAddNew', 'uAddNew', 'users' );
    add_block( 'profilePicture', 'pPicture', 'users' );
    add_block( 'educationWork', 'eduWork', 'users' );
    
    //set the page Title
    add_actions( 'section_title', 'Edit Profile Picture' );
    
    //set varibales
    //$d=fetch_user($_COOKIE['user_id']);
    $get_avatar = get_avatar( $_COOKIE['user_id'] );
    $theavatar  = "";
    if( !empty( $get_avatar ) )
    {
        $d         = fetch_user( $_COOKIE['user_id'] );
        $theavatar = "<img src=\"$get_avatar\" alt=\"" . $d['ldisplay_name'] . "\" title=\"" . $d['ldisplay_name'] . "\" />";
    }
    add_variable( 'the_avatar', $theavatar );
    add_variable( 'prc', 'Edit Profile Picture' );
    add_variable( 'tabs', $the_tabs );
    add_variable( 'upload_button', upload_button() );
    add_variable( 'alert', $thealert );
    
    
    parse_template( 'profilePicture', 'pPicture' );
    
    return return_template( 'users' );
}

/**
 * Get the User Education and Work Information 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return string The Education and Work HTML Design    
 */
function get_eduwork( $eduwork, $user_id )
{
    
    $edw  = get_additional_field( $user_id, $eduwork, 'user' );
    $edw  = json_decode( $edw, true );
    $html = '';
    if( $eduwork == 'work' )
    {
        
        $i = 0;
        if( count( $edw ) > 0 )
            foreach( $edw as $key => $val )
            {
                $present     = ( $val['to_period'] != 'present' ) ? false : true;
                $from_period = explode( " ", $val['from_period'] );
                $to_period   = explode( " ", $val['to_period'] );
                
                $html .= "<div class=\"the_work clearfix\" id='the_work_" . $i . "'>
				    		<div class=\"comp_name\">";
                $html .= "<strong>" . $key;
                
                if( !empty( $val['position'] ) )
                    $html .= " - " . $val['position'];
                
                if( !empty( $val['city'] ) )
                    $html .= " (" . $val['city'] . ") </strong>";
                
                $html .= "<br /><label>" . $val['from_period'] . " to " . $val['to_period'] . "</label><br />
					    		<span style='font-size:10px;'>" . $val['jobdes'] . "</span>
				    		</div>";
                if( isset( $_COOKIE['user_id'] ) && $user_id == $_COOKIE['user_id'] )
                    $html .= "<div class=\"action\"><a href=\"javascript:;\" id=\"work_edit_" . $i . "\">Edit</a> | <a href=\"javascript:;\" rel=\"delete_work_" . $i . "\">Delete</a></div>";
                $html .= "</div>";
                
                if( isset( $_COOKIE['user_id'] ) && $user_id == $_COOKIE['user_id'] )
                {
                    $html .= "<script type='text/javascript'>
	    						$(function(){
	    							$('#the_work_" . $i . "').click(function(){
	    								$('input[name=company_name]').val('" . $key . "');
	    								$('input[name=position]').val('" . $val['position'] . "');
	    								$('input[name=city]').val('" . $val['city'] . "');
	    								$('textarea[name=jobdes]').val('" . $val['jobdes'] . "');
	    								$('select[name=from_month_period]').val('" . $from_period[0] . "');
	    								$('select[name=from_year_period]').val('" . $from_period[1] . "');";
                    if( $present )
                    {
                        $html .= "$('select[name=to_month_period]').hide();
										$('select[name=to_year_period]').hide();
										$('#present_text').show();
										$('input[name=present]').attr('checked','checked');
										";
                    }
                    else
                    {
                        $html .= "$('select[name=to_month_period]').val('" . $to_period[0] . "');
	    								$('select[name=to_year_period]').val('" . $to_period[1] . "');
	    								$('select[name=to_month_period]').show();
										$('select[name=to_year_period]').show();
										$('#present_text').hide();
										$('input[name=present]').removeAttr('checked');
										";
                    }
                    
                    $html .= "	});
	    						});
	    					</script>";
                    add_actions( 'admin_tail', 'delete_confirmation_box', 'work_' . $i, "Are you sure want to delete " . $key . " from your Work Experience list?", '../l-functions/user.php', 'the_work_' . $i, 'eduwork=' . $eduwork . '&delete_key=' . $key );
                    //$html.=delete_confirmation_box('work_'.$i, "Are you sure want to delete ".$key." from your Work Experience list?", '../l-functions/user.php', 'the_work_'.$i,'eduwork='.$eduwork.'&delete_key='.$key);
                }
                $i++;
            }
        
    }
    elseif( $eduwork == 'college' )
    {
        $i = 0;
        if( count( $edw ) > 0 )
            foreach( $edw as $key => $val )
            {
                $html .= "<div class=\"the_school clearfix\" id=\"college_" . $i . "\">
				    		<div class=\"shool_name\" >
					    		<strong>" . $key . "</strong><br />
					    		<label>";
                if( !empty( $val['concentrations'] ) )
                    $html .= $val['concentrations'];
                
                if( !empty( $val['concentrations'] ) && !empty( $val['class_year'] ) )
                    $html .= " - ";
                
                if( !empty( $val['class_year'] ) )
                    $html .= $val['class_year'];
                
                $html .= "</label></div>";
                
                if( isset( $_COOKIE['user_id'] ) && $user_id == $_COOKIE['user_id'] )
                    $html .= "<div class=\"action\"><a href=\"javascript:;\">Edit</a> | <a href=\"javascript:;\" rel=\"delete_college_" . $i . "\">Delete</a></div>";
                $html .= "</div>";
                
                if( isset( $_COOKIE['user_id'] ) && $user_id == $_COOKIE['user_id'] )
                {
                    $html .= "<script type='text/javascript'>
	    					$(function(){
	    						$('#college_" . $i . "').click(function(){
	    							$('input[name=college]').val('" . $key . "');
	    							$('select[name=collage_class_year]').val('" . $val['class_year'] . "');
	    							$('input[name=concentrations]').val('" . $val['concentrations'] . "');
	    						});
	    					});
	    				</script>";
                    $html .= delete_confirmation_box( 'college_' . $i, "Are you sure want to delete " . $key . " from your College/University list?", '../l-functions/user.php', 'college_' . $i, 'eduwork=' . $eduwork . '&delete_key=' . $key );
                }
                $i++;
            }
    }
    elseif( $eduwork == 'school' )
    {
        $i = 0;
        if( count( $edw ) > 0 )
            foreach( $edw as $key => $val )
            {
                $html .= "<div class=\"the_school clearfix\" id=\"the_school_" . $i . "\">
				    		<div class=\"shool_name\">
					    		<strong>" . $key . "</strong><br />
					    		<label>" . $val . "</label>
				    		</div>";
                if( isset( $_COOKIE['user_id'] ) && $user_id == $_COOKIE['user_id'] )
                    $html .= "<div class=\"action\"><a href=\"javascript:;\">Edit</a> | <a href=\"javascript:;\" rel=\"delete_school_" . $i . "\">Delete</a></div>";
                $html .= "</div>";
                
                if( isset( $_COOKIE['user_id'] ) && $user_id == $_COOKIE['user_id'] )
                {
                    $html .= "<script type='text/javascript'>
	    					$(function(){
	    						$('#the_school_" . $i . "').click(function(){
	    							$('input[name=school_name]').val('" . $key . "');
	    							$('select[name=school_class_year]').val('" . $val . "');
	    						});
	    					});
	    				</script>";
                    $html .= delete_confirmation_box( 'school_' . $i, "Are you sure want to delete " . $key . " from your high school list?", '../l-functions/user.php', 'the_school_' . $i, 'eduwork=' . $eduwork . '&delete_key=' . $key );
                }
                $i++;
            }
    }
    return $html;
}

/**
 * Manage user education and work 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return string The Education and Work HTML Design    
 */
function profile_eduwork()
{
    global $db;
    $thealert = "";
    
    
    ///$tabs=array('my-updates'=>'My Updates','my-profile'=>'My Profile','profile-picture'=>'Profile Picture','eduwork'=>'Education & Work');
    $tabs = array(
         'my-updates' => 'My Updates',
        'my-profile' => 'My Profile',
        'profile-picture' => 'Profile Picture' 
    );
    
    if( isset( $_GET['tab'] ) )
        $selected_tab = $_GET['tab'];
    else
        $selected_tab = 'my-profile';
    
    
    $the_tabs = set_tabs( $tabs, $selected_tab );
    
    //set template
    set_template( TEMPLATE_PATH . "/users.html", 'users' );
    
    //set block
    add_block( 'usersEdit', 'uEdit', 'users' );
    add_block( 'usersAddNew', 'uAddNew', 'users' );
    add_block( 'profilePicture', 'pPicture', 'users' );
    add_block( 'educationWork', 'eduWork', 'users' );
    
    //set the page Title
    add_actions( 'section_title', 'Education &amp; Work' );
    
    //set varibales
    $year_to = date( "Y" ) + 7;
    $year    = "";
    $month   = array(
         'January' => 'January',
        'February' => 'February',
        'March' => 'March',
        'April' => 'April',
        'May' => 'May',
        'June' => 'June',
        'July' => 'July',
        'Augusts' => 'Augusts',
        'September' => 'September',
        'October' => 'October',
        'November' => 'November',
        'December' => 'December' 
    );
    for( $y = $year_to; $y >= 1910; $y-- )
    {
        $year .= "<option value=\"" . $y . "\">" . $y . "</option>";
    }
    foreach( $month as $key => $val )
    {
        $month .= "<option value=\"" . $key . "\">" . $val . "</option>";
    }
    add_variable( 'year', $year );
    add_variable( 'month', $month );
    add_variable( 'school_list', get_eduwork( 'school', $_COOKIE['user_id'] ) );
    add_variable( 'collage_list', get_eduwork( 'college', $_COOKIE['user_id'] ) );
    add_variable( 'work_list', get_eduwork( 'work', $_COOKIE['user_id'] ) );
    add_variable( 'prc', 'Education &amp; Work' );
    add_variable( 'tabs', $the_tabs );
    add_variable( 'upload_button', upload_button() );
    add_variable( 'alert', $thealert );
    
    
    parse_template( 'educationWork', 'eduWork' );
    
    return return_template( 'users' );
}

/**
 * Grab user data by the user type  
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return array user data by user type    
 */
function fetch_user_per_type( $user_type )
{
    global $db;
    
    if( !empty( $user_type ) )
    {
        $sql = $db->prepare_query( "select * from l_users where lrole_id=%s", $user_type );
        $r   = $db->do_query( $sql );
        while( $data = $db->fetch_array( $r ) )
        {
            $user[] = $data['luser_id'];
        }
        return $user;
    }
}

/**
 * Grab user ID data by Email  
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return User ID   
 */
function fetch_user_ID_by_email( $email )
{
    global $db;
    
    if( !empty( $email ) )
    {
        $sql  = $db->prepare_query( "select * from l_users where lemail=%s", $email );
        $r    = $db->do_query( $sql );
        $data = $db->fetch_array( $r );
        return $data['luser_id'];
        
    }
}

/**
 * Grab user ID data by username  
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return User ID 
 */
function fetch_user_ID_by_username( $username )
{
    global $db;
    
    if( !empty( $username ) )
    {
        
        $sql  = $db->prepare_query( "select * from l_users where lusername=%s", $username );
        $r    = $db->do_query( $sql );
        $data = $db->fetch_array( $r );
        return $data['luser_id'];
        
    }
}

/**
 * Grab user data by the user ID or User name
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return array user data by ID or Username    
 */
function fetch_user( $id )
{
    global $db;
    
    $sql = $db->prepare_query( "select * from l_users where luser_id=%d", $id );
    $r   = $db->do_query( $sql );
    if( $db->num_rows( $r ) > 0 )
    {
        return $d = $db->fetch_array( $r );
    }
    else
    {
        $sql = $db->prepare_query( "select * from l_users where lusername=%s", $id );
        $r1  = $db->do_query( $sql );
        return $d = $db->fetch_array( $r1 );
    }
}

/**
 * Create the option display name from First and Last Name
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 *  
 * @return array Display name options    
 */
function opt_display_name( $id )
{
    $d                             = fetch_user( $id );
    $display_name[$d['lusername']] = $d['lusername'];
    
    $first_name = get_additional_field( $id, 'first_name', 'user' );
    if( !empty( $first_name ) )
        $display_name[$first_name] = $first_name;
    
    $last_name = get_additional_field( $id, 'last_name', 'user' );
    if( !empty( $last_name ) )
        $display_name[$last_name] = $last_name;
    
    if( !empty( $first_name ) && !empty( $last_name ) )
    {
        $display_name[$first_name . " " . $last_name] = $first_name . " " . $last_name;
        $display_name[$last_name . " " . $first_name] = $last_name . " " . $first_name;
    }
    
    return $display_name;
}
/**
 * Check if the given user_id is a standard user or no 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 * @param integer $user_id User ID
 *  
 * @return boolean     
 */
function is_standard_user( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 5 )
            return true;
        else
            return false;
    }
    else
    {
        $user = fetch_user( $user_id );
        if( $user['lrole_id'] == 5 )
            return true;
        else
            return false;
    }
}
/**
 * Check if the given user_id is a contributor or no 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 * @param integer $user_id User ID
 *  
 * @return boolean     
 */
function is_contributor( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 4 )
            return true;
        else
            return false;
    }
    else
    {
        $user = fetch_user( $user_id );
        if( $user['lrole_id'] == 4 )
            return true;
        else
            return false;
    }
}
/**
 * Check if the given user_id is an author or no 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 * @param integer $user_id User ID
 *  
 * @return boolean     
 */
function is_author( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 2 )
            return true;
        else
            return false;
    }
    else
    {
        $user = fetch_user( $user_id );
        if( $user['lrole_id'] == 2 )
            return true;
        else
            return false;
    }
}
/**
 * Check if the given user_id is an editor or no 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 * @param integer $user_id User ID
 *  
 * @return boolean     
 */
function is_editor( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 3 )
            return true;
        else
            return false;
    }
    else
    {
        $user = fetch_user( $user_id );
        if( $user['lrole_id'] == 3 )
            return true;
        else
            return false;
    }
}
/**
 * Check if the given user_id is an administrator or no 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 * @param integer $user_id User ID
 *  
 * @return boolean     
 */
function is_administrator( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 1 )
            return true;
        else
            return false;
    }
    else
    {
        $user = fetch_user( $user_id );
        if( $user['lrole_id'] == 1 )
            return true;
        else
            return false;
    }
}

/**
 * Get the user avatar image 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 * @param integer $user_id User ID
 * @param integer $image_size 1=Large,2=Medium,3=Small 
 *  
 * @return boolean     
 */
function get_avatar( $user_id, $image_size = 1 )
{
    $d = fetch_user( $user_id );
    if( empty( $d['lavatar'] ) )
    {
        if( $d['lsex'] == 1 )
            return HTSERVER . site_url() . '/l-content/files/users/man-' . $image_size . '.jpg';
        else
            return HTSERVER . site_url() . '/l-content/files/users/woman-' . $image_size . '.jpg';
    }
    else
    {
        $file_name = explode( '|', $d['lavatar'] );
        switch( $image_size )
        {
            case 1:
                $thefile = $file_name[0];
                break;
            case 2:
                $thefile = $file_name[1];
                break;
            case 3:
                $thefile = $file_name[2];
                break;
        }
        if( file_exists( FILES_PATH . '/users/' . $thefile ) )
        {
            return HTSERVER . site_url() . '/l-content/files/users/' . $thefile;
        }
        else
        {
            if( $d['lsex'] == 1 )
                return HTSERVER . site_url() . '/l-content/files/users/man-' . $image_size . '.jpg';
            else
                return HTSERVER . site_url() . '/l-content/files/users/woman-' . $image_size . '.jpg';
        }
    }
}

/**
 * Get the display name of user 
 *  
 *     
 *
 * @author Wahya Biantara
 * 
 * @since alpha
 * 
 * @param integer $id User ID
 *  
 *  
 * @return boolean     
 */
function get_display_name( $id )
{
    $data = fetch_user( $id );
    return $data['ldisplay_name'];
}

function get_user_tags( $user_id )
{
    global $db;
    //Expertise Tags
    $exprt_tags_result = fetch_rulerel_by_group_type( $user_id, "profile", "tags" );
    $thetag            = "";
    
    $i = 1;
    while( $tags = $db->fetch_array( $exprt_tags_result ) )
    {
        $thetag .= "<div class=\"people_tag_list_wrapper clearfix\">";
        $thetag .= "<div class=\"people_tag_list clearfix\" >";
        $thetag .= "<a href=\"" . get_state_url( 'people' ) . "&tag=" . $tags['lsef'] . "\" class=\"expert_tag_name\" style=\"width:auto;font-size:12px;display:block;color:#333;\">" . trim( $tags['lname'] ) . "</a>";
        $thetag .= "</div>";
        $thetag .= "<div class=\"numof_tag_list clearfix\" >
								<a href=\"" . get_state_url( 'people' ) . "&tag=" . $tags['lsef'] . "\" class=\"expert_tag_name\" style=\"width:auto;font-size:12px;display:block;color:#333;\">" . $tags['lcount'] . "</a>
							  </div>";
        $thetag .= "</div>";
        
        $i++;
    }
    return $thetag;
}

function set_privileges_admin()
{
    if( is_user_logged() )
    {
        global $db;
    
        $s = 'SELECT * FROM l_users_role WHERE lrole_id = %d';
        $q = $db->prepare_query( $s, $_COOKIE['user_type'] );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $d = $db->fetch_array( $r );
            $v = json_decode( $d['lrole_privileges'], true );

            if( json_last_error() == JSON_ERROR_NONE )
            {
                foreach( $v as $privileges )
                {
                    add_privileges( $_COOKIE['user_type'], $privileges, 'insert' );
                    add_privileges( $_COOKIE['user_type'], $privileges, 'update' );
                    add_privileges( $_COOKIE['user_type'], $privileges, 'delete' );
                    add_privileges( $_COOKIE['user_type'], $privileges, 'upload' );
                    add_privileges( $_COOKIE['user_type'], $privileges, 'preview' );
                    add_privileges( $_COOKIE['user_type'], $privileges, 'request' );
                    add_privileges( $_COOKIE['user_type'], $privileges, 'approve' );                   
                }
            }
        }
    }
}

function get_admin_user()
{
    if( is_add_new() )
    {
	    $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
	    $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';

    	run_save_admin_user( $state, $substate );

    	$data = get_admin_user_data();

        set_template( TEMPLATE_PATH . '/template/users/form.html', 'users-template' );

        add_block( 'form-block', 'fblock', 'users-template' );

		add_variable( 'luser_id', $data['luser_id'] );
		add_variable( 'lusername', $data['lusername'] );
        add_variable( 'ldisplay_name', $data['ldisplay_name'] );
        add_variable( 'lpassword', $data['lpassword'] );
        add_variable( 'lrpassword', $data['lrpassword'] );
        add_variable( 'lemail', $data['lemail'] );
        add_variable( 'lphone', $data['lphone'] );
        add_variable( 'lpassword', $data['lpassword'] );
        add_variable( 'lrpassword', $data['lrpassword'] );
        add_variable( 'role_option', get_role_option( $data['lrole_id'] ) );
        add_variable( 'status_option', get_admin_users_status_option( $data['lstatus'] ) );
		add_variable( 'message', generate_message_block() );
		add_variable( 'cancel_link', get_state_url( $state . $substate ) );
		add_variable( 'delete_class', 'sr-only' );

        add_variable( 'ajax_link', HTSERVER . site_url() . '/users-role-ajax/' );
        
        add_actions( 'section_title', 'Add New User' );

        parse_template( 'form-block', 'fblock', false );
        
        return return_template( 'users-template' );	        
    }
    elseif( is_edit() )
    {
	    $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
	    $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';

    	run_update_admin_user( $state, $substate );

    	$data = get_admin_user_data( $_GET['id'] );

        set_template( TEMPLATE_PATH . '/template/users/form.html', 'users-template' );

        add_block( 'form-block', 'fblock', 'users-template' );

        if ( isset( $data['luser_privilage'] ) && $data['luser_privilage'] != '' ) 
        {
            $user_privilage = get_role_privileges_users_option( $data['luser_privilage'] );
        }
        else
        {
            $user_privilage = add_role_privileges_users_option( $data['lrole_id'] );
        }

		add_variable( 'luser_id', $data['luser_id'] );
		add_variable( 'lusername', $data['lusername'] );
        add_variable( 'ldisplay_name', $data['ldisplay_name'] );
        add_variable( 'lpassword', $data['lpassword'] );
        add_variable( 'lrpassword', $data['lrpassword'] );
        add_variable( 'lemail', $data['lemail'] );
        add_variable( 'lphone', $data['lphone'] );
        add_variable( 'lpassword', $data['lpassword'] );
        add_variable( 'lrpassword', $data['lrpassword'] );
		add_variable( 'is_username_disabled', 'readonly' );
		add_variable( 'is_email_disabled', 'readonly' );
        add_variable( 'role_option', get_role_option( $data['lrole_id'] ) );
        add_variable( 'status_option', get_admin_users_status_option( $data['lstatus'] ) );
		add_variable( 'message', generate_message_block() );
		add_variable( 'cancel_link', get_state_url( $state . $substate ) );
		add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'users' ) );

        add_variable( 'user_privilage', $user_privilage );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/users-role-ajax/' );

        add_actions( 'section_title', 'Edit User' );

        parse_template( 'form-block', 'fblock', false );
        
        return return_template( 'users-template' );
    }
    elseif( is_delete_all() )
    {
        $state    = isset( $_GET['state'] ) ? $_GET['state'] : '';
        $substate = isset( $_GET['sub'] ) ? '&sub=' . $_GET['sub'] : '';

        set_template( TEMPLATE_PATH . '/template/users/batch-delete.html', 'users-template' );
        add_block( 'loop-block', 'bdlblock', 'users-template' );
        add_block( 'delete-block', 'bdblock', 'users-template' );

        foreach( $_POST['select'] as $key=>$val )
        {
            $d = fetch_user( $val );

            add_variable( 'ldisplay_name',  $d['ldisplay_name'] );
            add_variable( 'luser_id', $d['luser_id'] );

            parse_template('loop-block', 'bdlblock', true);
        }

        add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this user? :' : 'these users? :' ) );
        add_variable( 'action', get_state_url( $state . $substate ) );

        parse_template( 'delete-block', 'bdblock', false );
        
        add_actions( 'section_title', 'Delete User' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        return return_template( 'users-template' );
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key => $val )
        {
            delete_user( $val );
        }
    }
    elseif( is_profile() )
    {
        return edit_profile();
    }
    elseif( is_profile_picture() )
    {
        return edit_profile_picture();
    }

    if( is_num_users() > 0 )
    {
        return get_users_list();
    }
}

function run_save_admin_user( $state = '', $substate = '' )
{
    global $db;
    global $flash;

    if( is_save_changes() )
    {
    	$error = is_valid_user_input( $_POST['lusername'], $_POST['ldisplay_name'], $_POST['lpassword'], $_POST['lrpassword'], $_POST['lemail'], $_POST['lrole_id'] );

        if( empty( $error ) )
        {
            if( save_user( $_POST['lusername'], $_POST['ldisplay_name'], $_POST['lpassword'], $_POST['lemail'], $_POST['lrole_id'], $_POST['lphone'], $_POST['lstatus'], $_POST['app_privilage'] ) )
            {
                $user_id = $db->insert_id();

                if( empty( $user_id ) )
                {
                    $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new admin users' ) ) );
                }
                else
                {
                    $flash->add( array( 'type'=> 'success', 'content' => array( 'New admin users successfully saved' ) ) );

                    header( 'location:' . get_state_url(  $state . $substate . '&prc=add_new' ) );

                    exit;
                }
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_admin_user( $state = '', $substate = '' )
{
    global $flash;

    if( is_save_changes() )
    {
        $error = is_valid_user_input( $_POST['lusername'], $_POST['ldisplay_name'], $_POST['lpassword'], $_POST['lrpassword'], $_POST['lemail'], $_POST['lrole_id'] );	            

        if( empty( $error ) )
        {
            if( edit_user( $_GET['id'], $_POST['ldisplay_name'], $_POST['lpassword'], $_POST['lemail'], $_POST['lrole_id'], $_POST['lphone'], $_POST['lstatus'], $_POST['app_privilage'] ) )
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This admin users successfully edited' ) ) );

                header( 'location:' . get_state_url(  $state . $substate . '&prc=edit&id=' . $_GET['id'] ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function get_admin_user_data( $id = '' )
{
	global $db;

    $data = array( 
        'luser_id'          => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : null ), 
        'lrole_id'          => ( isset( $_POST['lrole_id'] ) ? $_POST['lrole_id'] : '' ), 
        'lusername'         => ( isset( $_POST['lusername'] ) ? $_POST['lusername'] : '' ), 
        'ldisplay_name'     => ( isset( $_POST['ldisplay_name'] ) ? $_POST['ldisplay_name'] : '' ), 
        'lpassword'         => ( isset( $_POST['lpassword'] ) ? $_POST['lpassword'] : '' ),
        'lrpassword'        => ( isset( $_POST['lrpassword'] ) ? $_POST['lrpassword'] : '' ),
        'lemail'            => ( isset( $_POST['lemail'] ) ? $_POST['lemail'] : '' ),
        'lphone'            => ( isset( $_POST['lphone'] ) ? $_POST['lphone'] : '' ),
        'lstatus'           => ( isset( $_POST['lstatus'] ) ? $_POST['lstatus'] : '' ),
        'luser_privilage'   => ( isset( $_POST['luser_privilage'] ) ? $_POST['luser_privilage'] : '' )
    );

    $s = 'SELECT * FROM l_users WHERE luser_id = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'luser_id' => $d['luser_id'], 
                'lrole_id' => $d['lrole_id'],
                'lusername' => $d['lusername'],
                'ldisplay_name' => $d['ldisplay_name'],
                'lpassword' => $data['lpassword'],
        		'lrpassword' => $data['lrpassword'],
                'lemail' => $d['lemail'], 
                'lphone' => $d['lphone'], 
                'lstatus' => $d['lstatus'],
                'luser_privilage' => $d['luser_privilage']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

function get_admin_users_status_option( $status )
{
	return '
	<option value="0" ' . ( $status == 0 ? 'selected' : '' ) . '>Pending</option>
	<option value="1" ' . ( $status == 1 ? 'selected' : '' ) . '>Active</option>
	<option value="2" ' . ( $status == 2 ? 'selected' : '' ) . '>Blocked</option>';
}

?>