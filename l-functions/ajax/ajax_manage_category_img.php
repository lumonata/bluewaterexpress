<?php
require("../../l_config.php");
require("../../l-functions/upload.php");
require_once("../settings.php");

if(isset($_POST['type']) && $_POST['type']=='add_image') 
	insert_category_image($_FILES, $_POST);
	
if(isset($_POST['type']) && $_POST['type']=='refresh_image') 
	get_category_image($_POST,true);


function get_category_image($post,$is_api=false)
{
	$param = json_encode(array('result'=>'failed'));
	if($post['app_id']<>0)
	{
		global $db;
		$s = 'SELECT * FROM l_meta_data WHERE lapp_id=%d AND lapp_name=%s AND lmeta_name=%s';
		$q = $db->prepare_query($s,$post['app_id'],$post['app_name'],$post['meta_name']);
		$r = $db->do_query($q);	
		$n = $db->num_rows($r);
		
		if($n > 0)
		{
			$data = array();
			while ($d=$db->fetch_array($r))
			{
				$data = array('app_id'=>$d['lapp_id'],'file_name'=>$d['lmeta_value']);
			}
			
			$param = array('result'=>'success','data'=>$data);
		}
	}
	
	if($is_api) echo json_encode($param); else return $param;
}

function insert_category_image($file, $post){
	
	$attach = get_category_image($post);
	
	if(!defined('FILES_PATH'))
		define('FILES_PATH',ROOT_PATH.'/l-content/files');  
		
	if(!defined('FILES_LOCATION'))
		define('FILES_LOCATION','/l-content/files');  
	
	$file_name = $file['file_name']['name'];
	$file_name = character_filter($file_name);
	$file_name = file_name_filter($file_name).'-'.time().file_name_filter($file_name,true);
	$file_type = $file['file_name']['type'];
	
	$folder_name  = date("Y",time()).date("m",time());
	$source       = $file['file_name']['tmp_name'];
	$destination  = FILES_PATH.'/'.$folder_name.'/'.$file_name;
	$new_file_loc = FILES_LOCATION.'/'.$folder_name.'/'.$file_name;
	
	$param = json_encode(array('result'=>'failed'));
	
	if(upload_crop($source,$destination,$file_type,thumbnail_image_width(),thumbnail_image_height()))
	{
		$app_id = $post['app_id'];
		if($post['app_id']==0) $app_id = strtotime(date('m/d/Y', time()));
		
		global $db;
		
		if($attach['result']=='success')
		{
			// DELETE OLD IMAGE
			$new_file = ROOT_PATH.$attach['data']['file_name'];
			if(file_exists($new_file)){
				unlink($new_file);
			}	
			
			$s = 'UPDATE l_meta_data SET lapp_id=%d, lmeta_value=%s WHERE lapp_id=%s';
			$q = $db->prepare_query($s,$app_id,$new_file_loc,$app_id);
			$r = $db->do_query($q);
		}
		else
		{
			$s = 'INSERT INTO l_meta_data (lmeta_name,lmeta_value,lapp_name,lapp_id) VALUES (%s,%s,%s,%d)';
			$q = $db->prepare_query($s,$post['meta_name'],$new_file_loc,$post['app_name'],$app_id);
			$r = $db->do_query($q);
		}
		
		if($r) $param = json_encode(array('result'=>'success','data'=>array('app_id'=>$app_id,'file_name'=>$new_file_loc)));
	}
	
	echo $param;
}
?>