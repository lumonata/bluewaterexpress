<?php



	define( 'PLUGINS_PATH', ROOT_PATH . '/l-plugins' );

	define( 'APPS_PATH', ROOT_PATH . '/l-apps' );

	define( 'FUNCTIONS_PATH', ROOT_PATH . '/l-functions' );

	define( 'CLASSES_PATH', ROOT_PATH . '/l-classes' );

	define( 'ADMIN_PATH', ROOT_PATH . '/l-admin' );

	define( 'CONTENT_PATH', ROOT_PATH . '/l-content' );



	define( 'HTSERVER', empty( $_SERVER['HTTPS'] ) ? 'http://' : 'https://' );

	require_once( ROOT_PATH . '/l-classes/flash-message.php' );
	require_once( ROOT_PATH . '/l-classes/admin_menu.php' );

	require_once( ROOT_PATH . '/l-classes/user_privileges.php' );

	require_once( ROOT_PATH . '/l-classes/user_app_privileges.php' );
    require_once( ROOT_PATH . '/l-vendor/vendor/autoload.php' );
	require_once( ROOT_PATH . '/l-admin/includes/php-mailer/PHPMailerAutoload.php' );
	require_once( ROOT_PATH . '/l-admin/includes/mailchimp-mandrill-api-php/src/Mandrill.php' );
	require_once( ROOT_PATH . '/l-admin/admin_functions.php' );

	require_once( ROOT_PATH . '/l-functions/themes.php' );

	require_once( ROOT_PATH . '/l-functions/kses.php' );

	require_once( ROOT_PATH . '/l-classes/directory.php' );

	require_once( ROOT_PATH . '/l-functions/paging.php' );

	require_once( ROOT_PATH . '/l_settings.php' );

	require_once( ROOT_PATH . '/l-functions/settings.php' );

	require_once( ROOT_PATH . '/l-functions/mail.php' );

	require_once( ROOT_PATH . '/l-functions/rewrite.php' );

	require_once( ROOT_PATH . '/l-functions/upload.php' );

	require_once( ROOT_PATH . '/l-content/languages/' . is_language( 'en' ) . '.php' );

	require_once( ROOT_PATH . '/l-classes/post.php' );

	require_once( ROOT_PATH . '/l-functions/articles.php' );

	require_once( ROOT_PATH . '/l-classes/actions.php' );

	require_once( ROOT_PATH . '/l-functions/notifications.php' );

	require_once( ROOT_PATH . '/l-functions/taxonomy.php' );

	require_once( ROOT_PATH . '/l-functions/plugins.php' );

	require_once( ROOT_PATH . '/l-functions/personal-settings.php' );

	require_once( ROOT_PATH . '/l-functions/comments.php' );

	require_once( ROOT_PATH . '/l-functions/feeds.php' );

	require_once( ROOT_PATH . '/l-functions/menus.php' );

	require_once( ROOT_PATH . '/l-functions/friends.php' );

	require_once( ROOT_PATH . '/l-functions/people.php' );



	if( !defined( 'SITE_URL' ) )

	{

	    define( 'SITE_URL', get_meta_data( 'site_url' ) );

	}



	$SINGLE_FILE  = false;

	$table_prefix = 'l_';



	define( 'SMTP_SERVER', get_meta_data( 'smtp_server' ) );



	set_timezone( get_meta_data( 'time_zone' ) );



	require_once( ROOT_PATH . '/l_functions.php' );

	require( ROOT_PATH . '/l_themes.php' );



?>