<?php

	if( !isset( $l_load ) )
	{
	    $l_load = true;
	    
	    define( 'ABSPATH', dirname( __FILE__ ) . '/' );

	    if( file_exists( ABSPATH . 'l_config.php' ) )
	    {
	        require_once( ABSPATH . 'l_config.php' );
	        require_once( ABSPATH . 'l_include.php' );
	    }
	    elseif( file_exists( dirname( ABSPATH ) . 'l_config.php' ) )
	    {
	        require_once( dirname( ABSPATH ) . 'l_config.php' );
	        require_once( dirname( ABSPATH ) . 'l_include.php' );
	    }
	    else
	    {
	        define( 'TEMPLATE_URL', getcwd() );

	        require_once( ABSPATH . '/l-functions/error_handler.php' );
	        require_once( ABSPATH . '/l-functions/template.php' );

	        echo l_die( 'Config File Not Found!' );
	    }    
	}

?>