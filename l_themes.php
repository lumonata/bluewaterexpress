<?php

	if( $L_ADMIN == false )

	{

	    $theme = set_front_end_themes();

	    require_once( ROOT_PATH . '/l-functions/template.php' );

	    $thecontent = set_front_end_content();

	}

	else

	{

	    $theme = set_admin_themes();

	    require_once( ROOT_PATH . '/l-functions/template.php' );

	    $thecontent = set_admin_content();

	}



	$theme = empty( $theme ) ? 'default' : $theme;



	function set_admin_content()

	{

		//-- Set privileges of login user

		//-- baseon privileges setting

		set_privileges_admin();

		

	    if( is_dashboard() )

	    {

	        $thecontent = get_dashboard();

	    }

	    elseif( is_global_settings() )

	    {

	        $thecontent = get_global_settings();

	    }

	    elseif( is_personal_settings() )

	    {

	        $thecontent = personal_settings();

	    }

	    elseif( is_admin_themes() )

	    {

	        $thecontent = get_themes();

	    }

	    elseif( is_admin_page() )

	    {

	        $thecontent = get_admin_page();

	    }

	    elseif( is_admin_article() )

	    {

	        $thecontent = get_admin_article();

	    }

	    elseif( is_profile_eduwork() )

	    {

	        $thecontent = profile_eduwork();

	    }

	    elseif( is_profile_picture() )

	    {

	        $thecontent = edit_profile_picture();

	    }

	    elseif( is_profile() )

	    {

	        $thecontent = edit_profile();

	    }

	    elseif( is_user_updates() )

	    {

	        $thecontent = user_updates();

	    }

	    elseif( is_admin_user() )

	    {

	        $thecontent = get_admin_user();

	    }

	    elseif( is_admin_plugin() )

	    {

	        $thecontent = get_plugins();

	    }

	    elseif( is_admin_comment() )

	    {

	        $thecontent = admin_comments();

	    }

	    elseif( is_logout() )

	    {

	        do_logout();

	    }

	    else

	    {

	        if( isset( $_GET['sub'] ) )

	        {

	            if( is_grant_app( $_GET['sub'] ) )

	            {

	                $thecontent = run_actions( $_GET['sub'] );

	            }	            

	        }

	        elseif( isset( $_GET['state'] ) )

	        {

	            if( is_grant_app( $_GET['state'] ) )

	            {

	                $thecontent = run_actions( $_GET['state'] );

	            }

	        }



	        if( empty( $thecontent ) )

	        {

	            $thecontent = '

	            <div class="wrapper-block not-found-block">

				    <div class="container-fluid">

				        <div class="row">

				            <div class="col-md-12"> 

				                <h1>Access Denied</h1>

				            </div>

				        </div>

				        <div class="row">

				            <div class="col-md-12">

				            	<div class="message">

				            		<div class="bg-danger">

				            			<p class="text-danger">

				            				<span class="glyphicon glyphicon-exclamation-sign"></span>

				            				Sorry You don\'t have an authorization to access this page, or your page can\'t be found

				            			</p>

				            		</div>

				            	</div>

				            </div>

				        </div>

				    </div>

				</div>';



	            add_actions( 'section_title', 'No Authorization' );

	        }

	    }



	    return $thecontent;

	}



	function set_front_end_content()

	{

	    if( is_home() )

	    {

	        $thecontent = the_looping_articles();

	    }

	    elseif( is_details() )

	    {

	        $thecontent = article_detail();

	    }

	    elseif( is_category() )

	    {

	        $thecontent = the_looping_categories();

	    }

	    elseif( is_tag() )

	    {

	        $thecontent = the_looping_tags();

	    }

	    elseif( is_category( 'appname=register' ) )

	    {

	        $thecontent = signup_user();

	    }

	    elseif( is_category( 'appname=login' ) )

	    {	        

	        add_actions( 'meta_title', 'Login' );



	        $thecontent = sign_in_form();

	    }

	    elseif( is_category( 'appname=verify' ) )

	    {

	        $thecontent = verify_account();

	    }

	    elseif( is_page( "page_name=register" ) )

	    {

	        $thecontent = signup_user();

	    }

	    elseif( is_page( "page_name=login" ) )

	    {

	        $thecontent = signup_user();

	    }

	    elseif( is_page() )

	    {

	        if( get_appname() == 'pages' )

	        {

	            $thecontent = article_detail();

	        }

	        else

	        {

	            $thecontent = run_actions( get_appname() . '_page' );

	        }

	    }

	    else

	    {	        

	        $thecontent = run_actions( 'thecontent' );



	        if( empty( $thecontent ) )

	        {

	            $thecontent = 'Data not found';

	        }

	    }



	    add_actions( 'header', 'get_custome_bg' );



	    return $thecontent;

	}



	function set_admin_themes()

	{

	    define( 'L_ADMIN', TRUE );



	    if( is_preview() )

	    {

	        $theme = $_GET['theme'];

	    }

	    else

	    {

	        $theme = get_meta_data( 'admin_theme', 'themes' );

	    }

	    define( 'TEMPLATE_PATH', ROOT_PATH . '/l-admin/themes/' . $theme );

	    define( 'TEMPLATE_URL', SITE_URL . '/l-admin/themes/' . $theme );

	    

	    define( 'FRONT_TEMPLATE_PATH', ROOT_PATH . '/l-content/themes' );

	    define( 'FRONT_TEMPLATE_URL', SITE_URL . '/l-content/themes' );

	    

	    define( 'ADMIN_TEMPLATE_PATH', ROOT_PATH . '/l-admin/themes' );

	    define( 'ADMIN_TEMPLATE_URL', SITE_URL . '/l-admin/themes' );

	    

	    return $theme;

	}



	function set_front_end_themes()

	{

	    if( is_preview() )

	    {

	        $theme = $_GET['theme'];

	    }

	    else

	    {

	        $theme = get_meta_data( 'front_theme', 'themes' );

	    }

	    

	    define( 'TEMPLATE_PATH', ROOT_PATH . '/l-content/themes/' . $theme );

	    define( 'TEMPLATE_URL', SITE_URL . '/l-content/themes/' . $theme );

	    

	    define( 'FRONT_TEMPLATE_PATH', ROOT_PATH . '/l-content/themes' );

	    define( 'FRONT_TEMPLATE_URL', SITE_URL . '/l-content/themes' );

	    

	    define( 'L_ADMIN', FALSE );

	    

	    return $theme;

	}



	function get_custome_bg()

	{

	    $bgcolor      = get_meta_data( 'custome_bg_color', 'themes' );

	    $bgimage_prev = get_meta_data( 'custome_bg_image_preview', 'themes' );

	    $bgimage      = get_meta_data( 'custome_bg_image', 'themes' );

	    $bgpos        = get_meta_data( 'custome_bg_pos', 'themes' );

	    $bgrepeat     = get_meta_data( 'custome_bg_repeat', 'themes' );

	    $bgattach     = get_meta_data( 'custome_bg_attachment', 'themes' );

	    

	    $style_ent = '';

	    $style     = '';

	    

	    if( !empty( $bgcolor ) )

	    {

	        $style_ent .= 'background-color:#' . $bgcolor . ';';

	    }

	    

	    if( !empty( $bgimage ) )

	    {

	        $style_ent .= 'background-image: url("http://' . site_url() . $bgimage . '");';

	    }	    

	    

	    if( !empty( $bgpos ) )

	    {

	        $style_ent .= 'background-position:' . $bgpos . ';';

	    }

	    

	    if( !empty( $bgrepeat ) )

	    {

	        $style_ent .= 'background-repeat:' . $bgrepeat . ';';

	    }	    

	    

	    if( !empty( $bgattach ) )

	    {

	        $style_ent .= 'background-attachment:' . $bgattach . ';';

	    }

	    

	    if( !empty( $style_ent ) )

	    {

	        return $style = '<style type="text/css">body{ ' . $style_ent . ' }</style>';

	    }	    

	}


	if( file_exists( TEMPLATE_PATH . '/template.php' ) )

	{

	    require_once( TEMPLATE_PATH . '/template.php' );

	}

	else

	{

	    echo l_die( '<h1>Template File Not Found</h1><p>Make sure that you already create the <code>template.php</code> at ' . TEMPLATE_PATH . '</p>' );

	}



?>