<?php

/*
    Plugin Name: ticket
    Plugin URL: 
    Description: It's plugin  is use for Pacha Express tickecting
    Author: Yana
    Author URL: http://l.com/about-us
    Version: 1.0.0
    Created Date : 9 September 2013
*/

date_default_timezone_set( 'UTC' );

if( session_status() == PHP_SESSION_NONE )
{
    session_start();
}

/*
| -------------------------------------------------------------------------------------
| Reservation
| -------------------------------------------------------------------------------------
*/
add_apps_privileges( 'reservation', 'Reservation' );
add_apps_privileges( 'availability', 'Check Availability' );
add_apps_privileges( 'new-booking', 'New Reservation' );
add_apps_privileges( 'booking', 'Reservation List' );
add_apps_privileges( 'booking-notification', 'Send Notification' );
add_apps_privileges( 'notification', 'Notification List' );

/*
| -------------------------------------------------------------------------------------
| Schedules
| -------------------------------------------------------------------------------------
*/
add_apps_privileges( 'schedules', 'Schedules' );
add_apps_privileges( 'reschedule', 'Reschedule' );
add_apps_privileges( 'rate-period', 'Rate Period' );
add_apps_privileges( 'rate', 'Rate' );

add_apps_privileges( 'location', 'Location' );
add_apps_privileges( 'promo', 'Promo Code' );
add_apps_privileges( 'route', 'Route' );
add_apps_privileges( 'schedule', 'Manage Schedule' );
add_apps_privileges( 'exception', 'Manage Exception' );
add_apps_privileges( 'add-ons', 'Add-ons' );
add_apps_privileges( 'allotment', 'Allotment' );
add_apps_privileges( 'by-schedule', 'Allotment - By Schedule' );
add_apps_privileges( 'by-agent', 'Allotment - By Agent' );
add_apps_privileges( 'by-online', 'Allotment - Online' );
add_apps_privileges( 'all-agent', 'Allotment - All Agent' );

/*
| -------------------------------------------------------------------------------------
| Booking Source
| -------------------------------------------------------------------------------------
*/
add_apps_privileges( 'booking-source', 'Booking Source' );
add_apps_privileges( 'channel', 'Channel' );
add_apps_privileges( 'agent', 'Agent' );
add_apps_privileges( 'rate-category', 'Rate Category' );
add_apps_privileges( 'agent-discount', 'Agent Discount' );

/*
| -------------------------------------------------------------------------------------
| Admin
| -------------------------------------------------------------------------------------
*/
add_apps_privileges( 'admin', 'Admin' );
add_apps_privileges( 'role', 'Admin Role' );
add_apps_privileges( 'log', 'Admin Log' );
add_apps_privileges( 'manage-admin', 'Manage Admin' );
add_apps_privileges( 'guest-feedback', 'Guest Feedback' );

/*
| -------------------------------------------------------------------------------------
| Boat
| -------------------------------------------------------------------------------------
*/
add_apps_privileges( 'boat', 'Boat' );

/*
| -------------------------------------------------------------------------------------
| Report
| -------------------------------------------------------------------------------------
*/
add_apps_privileges( 'report', 'Report' );
add_apps_privileges( 'passenger-list-report', 'Passenger List Report' );
add_apps_privileges( 'transport-list-report', 'Transport List Report' );
add_apps_privileges( 'port-clearance-report', 'Port Clearance Report' );
add_apps_privileges( 'reservation-report', 'Reservation Report' );
add_apps_privileges( 'reservation-timeline-report', 'Reservation Timeline Report' );
add_apps_privileges( 'reservation-addons-report', 'Add-ons Report' );
add_apps_privileges( 'daily-pax-numbers-report', 'Daily Pax Numbers Report' );
add_apps_privileges( 'seats-production-report', 'Seats Production Report' );
add_apps_privileges( 'revenue-report', 'Revenue Report' );
add_apps_privileges( 'transport-revenue-report', 'Transport Revenue Report' );
add_apps_privileges( 'average-rates-report', 'Average Rates Report' );
add_apps_privileges( 'hs-sales-sum-report', 'Hi-Season Sales Sum. Report' );
add_apps_privileges( 'market-segment-report', 'Market Segment Report' );
add_apps_privileges( 'passenger-list', 'Port Clearance - Passenger List' );
add_apps_privileges( 'port-clearance-doc', 'Port Clearance Doc.' );
add_apps_privileges( 'promo-code-report', 'Promo Code Report' );
add_apps_privileges( 'feedback-report', 'Feedback Report' );

/*
| -------------------------------------------------------------------------------------
| Transport
| -------------------------------------------------------------------------------------
*/
add_apps_privileges( 'transport', 'Transport' );
add_apps_privileges( 'pickup-guest', 'Pickup Guest' );
add_apps_privileges( 'drop-off-guest', 'Drop-off Guest' );
add_apps_privileges( 'area', 'Manage Area' );
add_apps_privileges( 'driver', 'Driver' );
add_apps_privileges( 'vehicle-owner', 'Vehicle Owner' );
add_apps_privileges( 'vehicle', 'Vehicle' );
add_apps_privileges( 'transport-list', 'Transport List' );
add_apps_privileges( 'pickup-list', 'Pickup List' );
add_apps_privileges( 'drop-off-list', 'Drop-off List' );
add_apps_privileges( 'hotel', 'Hotel' );

/*
| -------------------------------------------------------------------------------------
| Outstanding Payment
| -------------------------------------------------------------------------------------
*/
add_apps_privileges( 'outstanding-payment', 'Outstanding Payment' );

/*
| -------------------------------------------------------------------------------------
| Outstanding Payment
| -------------------------------------------------------------------------------------
*/
add_apps_privileges( 'checkin', 'Checkin Passenger' );

/*
| -------------------------------------------------------------------------------------
| Run Actions
| -------------------------------------------------------------------------------------
*/
add_actions( 'manage-admin', 'get_admin_user' );
add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/menu.css' );

/*
| -------------------------------------------------------------------------------------
| Add Sidebar Menu
| -------------------------------------------------------------------------------------
*/
add_main_menu( array(
    'reservation' => 'Reservation',
    'schedules' => 'Schedule',
    'booking-source' => 'Booking Source',
    'admin' => 'Admin',
    'boat' => 'Boat',
    'report' => 'Report',
    'transport' => 'Transport',
    'outstanding-payment' => 'Outstanding Payment'
));

add_sub_menu( 'reservation', array(
    'availability' => 'Check Availability',
    'new-booking' => 'New Reservation',
    'booking' => 'Reservation List',
    'reschedule' => 'Reschedule',
    'booking-notification' => 'Send Notification'
));

add_sub_menu( 'schedules', array(
    'rate-period' => 'Rate Period',
    'rate' => 'Rate',
    'location' => 'Location',
    'promo' => 'Promo Code',
    'route' => 'Route',
    'schedule' => 'Manage Schedule',
    'exception' => 'Manage Exception',
    'add-ons' => 'Add-ons',
    'allotment' => 'Allotment'
));

add_sub_menu( 'allotment', array(
    'by-schedule' => 'By Schedule',
    'by-agent' => 'By Agent',
    'by-online' => 'Online',
    'all-agent' => 'All Agent'
));

add_sub_menu( 'booking-source', array(
    'rate-category' => 'Rate Category',
    'channel' => 'Channel',
    'agent' => 'Agent',
    'agent-discount' => 'Agent Discount'
));

add_sub_menu( 'admin', array(
    'role' => 'Admin Role',
    'log' => 'Admin Log',
    'manage-admin' => 'Manage Admin',
    'guest-feedback' => 'Guest Feedback',
));

add_sub_menu( 'report', array(
    'passenger-list-report' => 'Passenger List',
    'transport-list-report' => 'Transport List',
    'port-clearance-report' => 'Port Clearance',
    'reservation-report' => 'Reservation Report',
    'reservation-addons-report' => 'Add-ons Report',
    'daily-pax-numbers-report' => 'Daily Pax Numbers Report',
    'seats-production-report' => 'Seats Production',
    'revenue-report' => 'Revenue Report',
    'transport-revenue-report' => 'Transport Revenue Report',
    'average-rates-report' => 'Average Rates Report',
    'hs-sales-sum-report' => 'Hi-Season Sales Sum.',
    'market-segment-report' => 'Marker Segment',
    'promo-code-report' => 'Promo Code Report',
    'feedback-report' => 'Feedback Report'
));

add_sub_menu( 'port-clearance-report', array(
    'passenger-list' => 'Passenger List',
    'port-clearance-doc' => 'Port Clearance Doc.'
));

add_sub_menu( 'transport', array(
    'pickup-guest' => 'Pickup Guest',
    'drop-off-guest' => 'Drop-off Guest',
    'area' => 'Manage Area',
    'driver' => 'Driver',
    'vehicle-owner' => 'Vehicle Owner',
    'vehicle' => 'Vehicle',
    'transport-list' => 'Transport List',
    'hotel' => 'Hotel'
));

add_sub_menu( 'transport-list', array(
    'pickup-list' => 'Pickup List',
    'drop-off-list' => 'Drop-off List'
));

define( 'TICKET_VERSION', '1.8.7' );

include( 'include/booking-notification.php' );
include( 'include/paypal/bootstrap.php' );
include( 'include/availability.php' );
include( 'include/new-booking.php' );
include( 'include/booking.php' );

include( 'include/reschedule.php' );
include( 'include/rate.php' );
include( 'include/rate-category.php' );
include( 'include/rate-period.php' );
include( 'include/location.php' );
include( 'include/promo.php' );
include( 'include/route.php' );
include( 'include/schedule.php' );
include( 'include/exception.php' );
include( 'include/add-ons.php' );
include( 'include/allotment.php' );

include( 'include/channel.php' );
include( 'include/agent.php' );
include( 'include/agent-discount.php' );

include( 'include/role.php' );
include( 'include/log.php' );
include( 'include/feedback.php' );

include( 'include/boat.php' );

include( 'include/area.php' );
include( 'include/driver.php' );
include( 'include/vehicle-owner.php' );
include( 'include/vehicle.php' );
include( 'include/hotel.php' );
include( 'include/port-clearance.php' );
include( 'include/report.php' );
include( 'include/export.php' );
include( 'include/transport.php' );
include( 'include/notification.php' );
include( 'include/outstanding-payment.php' );

include( 'include/backup-restore.php' );
include( 'include/functions.php' );
include( 'include/setting.php' );
?>