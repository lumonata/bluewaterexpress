function gen_chart( dt_chart )
{
    if( typeof dt_chart != 'undefined' )
    {
        jQuery('.report-chart-legend').html('');

        var legend  = jQuery('.report-chart-legend');
        var options = jQuery.parseJSON( jQuery.base64.decode( dt_chart ) );

        jQuery.each( options.series, function(i, e){
            legend.append('<a><span></span>'+e.name+'</a>');
        });
        
        options.chart.marginTop = legend.innerHeight() + 50;

        var report = Highcharts.chart('report-chart', options );

        jQuery(report.series).each(function(i, e){
            jQuery('.report-chart-legend a:eq('+i+')').css('border-bottom-color', e.color);
        });

        jQuery('.report-chart-legend a').click(function (){
            var idx = jQuery(this).index();

            jQuery(this).toggleClass('leg-clicked');
            jQuery('.highcharts-legend-item:eq('+idx+')').trigger('click');
        });
    }
    else
    {
        jQuery('#report-chart').html('');
        jQuery('#report-chart-legend').html('');
    }
}