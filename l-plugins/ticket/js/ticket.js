function set_filter_period( val, from, to, is_reservation = false )
{
    if( val == '' )
    {
        jQuery('[name=' + from + ']').addClass('sr-only');
        jQuery('[name=' + to + ']').addClass('sr-only');
    }
    else if( val == 0 )
    {
        if( is_reservation )
        {
            jQuery('[name=' + from + ']').removeClass('sr-only');
            jQuery('[name=' + from + ']').datepicker({
                dateFormat:'dd MM yy',
                yearRange: '-15:+1',
                beforeShow : function(input, inst){
                    jQuery('#ui-datepicker-div').removeClass('hide-calendar hide-month');
                },
                onSelect: function(date){
                    var date2 = jQuery('[name=' + to + ']').datepicker('getDate');

                    if( date2 == null )
                    {
                        jQuery('[name=' + to + ']').datepicker('setDate', date);
                    }

                    jQuery('[name=' + to + ']').datepicker('option', 'minDate', date);
                }
            });

            jQuery('[name=' + to + ']').removeClass('sr-only');
            jQuery('[name=' + to + ']').datepicker({
                dateFormat:'dd MM yy',
                yearRange: '-15:+1',
                onClose: function(date){
                    var date2 = jQuery('[name=' + from + ']').datepicker('getDate');

                    if( date2 == null )
                    {
                        jQuery('[name=' + from + ']').datepicker('setDate', date);
                    }
                    
                    jQuery('[name=' + from + ']').datepicker('option', 'maxDate', date);
                }
            });
        }
        else
        {            
            jQuery('[name=' + from + ']').removeClass('sr-only');
            jQuery('[name=' + from + ']').datepicker({
                dateFormat:'dd MM yy',
                yearRange: '-15:+1',
                beforeShow : function(input, inst){
                    jQuery('#ui-datepicker-div').removeClass('hide-calendar hide-month');                   
                }
            });
        
            if( jQuery('[name=' + from + ']').val() == '' )
            {
                jQuery('[name=' + from + ']').datepicker('setDate', new Date());
            }

            jQuery('[name=' + to + ']').datepicker('setDate', '');
            jQuery('[name=' + to + ']').addClass('sr-only');
        }
    }
    else if( val == 1 )
    {
        jQuery('[name=' + from + ']').removeClass('sr-only');
        jQuery('[name=' + from + ']').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
            yearRange: '-15:+1',
            onClose: function( dateText, inst ){ 
                var month = jQuery('#ui-datepicker-div .ui-datepicker-month :selected').val();
                var year  = jQuery('#ui-datepicker-div .ui-datepicker-year :selected').val();
                var date  = new Date( year, month, 1 );
                
                jQuery(this).datepicker('setDate', date).trigger('change');
            },
            beforeShow : function(input, inst){                        
                jQuery('#ui-datepicker-div').addClass('hide-calendar');

                var datestr = jQuery(this).val();

                if( datestr.length > 0 )
                {
                    var year  = new Date( Date.parse( '1 ' + datestr ) ).getFullYear();
                    var month = new Date( Date.parse( '1 ' + datestr ) ).getMonth() + 1;

                    jQuery(this).datepicker('option', 'defaultDate', new Date( year, month - 1, 1 ));
                    jQuery(this).datepicker('setDate', new Date( year, month - 1, 1 ));
                }
            }
        });

        if( jQuery('[name=' + from + ']').val() == '' )
        {
            jQuery('[name=' + from + ']').datepicker('setDate', new Date());
        }

        jQuery('[name=' + to + ']').datepicker('setDate', '');
        jQuery('[name=' + to + ']').addClass('sr-only');
    }
    else if( val == 2 )
    {
        jQuery('[name=' + from + ']').removeClass('sr-only');
        jQuery('[name=' + from + ']').datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy',
            yearRange: '-15:+1',
            onClose: function( dateText, inst ){ 
                var year  = jQuery('#ui-datepicker-div .ui-datepicker-year :selected').val();
                var date  = new Date( year, 1, 1 );
                
                jQuery(this).datepicker('setDate', date).trigger('change');
            },
            beforeShow : function(input, inst){                        
                jQuery('#ui-datepicker-div').addClass('hide-calendar hide-month');

                var datestr = jQuery(this).val();

                if( datestr.length > 0 )
                {
                    var year  = new Date( Date.parse( '1 Jan ' + datestr ) ).getFullYear();

                    jQuery(this).datepicker('option', 'defaultDate', new Date( year, 1, 1 ));
                    jQuery(this).datepicker('setDate', new Date( year, 1, 1 ));
                }
            }
        });
        
        if( jQuery('[name=' + from + ']').val() == '' )
        {
            jQuery('[name=' + from + ']').datepicker('setDate', new Date());
        }

        jQuery('[name=' + to + ']').datepicker('setDate', '');
        jQuery('[name=' + to + ']').addClass('sr-only');
    }
    else if( val == 3 )
    {
        jQuery('[name=' + from + ']').removeClass('sr-only');
        jQuery('[name=' + from + ']').datepicker({
            dateFormat:'dd MM yy',
            yearRange: '-15:+1',
            beforeShow : function(input, inst){
                jQuery('#ui-datepicker-div').removeClass('hide-calendar hide-month');
            },
            onSelect: function(date){
                var date2 = jQuery('[name=' + to + ']').datepicker('getDate');

                if( date2 == null )
                {
                    jQuery('[name=' + to + ']').datepicker('setDate', date);
                }

                jQuery('[name=' + to + ']').datepicker('option', 'minDate', date);
            }
        });

        jQuery('[name=' + to + ']').removeClass('sr-only');
        jQuery('[name=' + to + ']').datepicker({
            dateFormat:'dd MM yy',
            yearRange: '-15:+1',
            onClose: function(date){
                var date2 = jQuery('[name=' + from + ']').datepicker('getDate');

                if( date2 == null )
                {
                    jQuery('[name=' + from + ']').datepicker('setDate', date);
                }
                
                jQuery('[name=' + from + ']').datepicker('option', 'maxDate', date);
            }
        });
        
        if( jQuery('[name=' + from + ']').val() == '' )
        {
            jQuery('[name=' + from + ']').datepicker('setDate', new Date());
        }
        
        if( jQuery('[name=' + to + ']').val() == '' )
        {
            jQuery('[name=' + to + ']').datepicker('setDate', new Date());
        }
    }
    else if( val == 4 )
    {
        jQuery('[name=' + from + ']').removeClass('sr-only');
        jQuery('[name=' + from + ']').datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy',
            yearRange: '-15:+1',
            onClose: function( dateText, inst ){ 
                var year  = jQuery('#ui-datepicker-div .ui-datepicker-year :selected').val();
                var year2  = jQuery('[name=' + to + ']').val();    
                var date  = new Date( year, 1, 1 );

                if( year2 == "" )
                {
                    jQuery('[name=' + to + ']').datepicker('setDate', date).trigger('change');
                }
                
                jQuery(this).datepicker('setDate', date).trigger('change');
            },
            beforeShow : function(input, inst){                        
                jQuery('#ui-datepicker-div').addClass('hide-calendar hide-month');

                var datestr = jQuery('[name=' + to + ']').val();

                if( datestr.length > 0 )
                {
                    var year  = new Date( Date.parse( '1 Jan ' + datestr ) ).getFullYear();
                    
                    jQuery(this).datepicker('option', 'maxDate', new Date( year, 1, 1 ));
                    jQuery(this).datepicker('option', 'yearRange', ( year - 5 ) + ':' + year );                   
                }
            }
        });

        jQuery('[name=' + to + ']').removeClass('sr-only');
        jQuery('[name=' + to + ']').datepicker({
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy',
            yearRange: '-15:+1',
            onClose: function( dateText, inst ){ 
                var year  = jQuery('#ui-datepicker-div .ui-datepicker-year :selected').val();
                var date  = new Date( year, 1, 1 );

                jQuery(this).datepicker('setDate', date).trigger('change');
            },
            beforeShow : function(input, inst){                        
                jQuery('#ui-datepicker-div').addClass('hide-calendar hide-month');

                var datestr = jQuery('[name=' + from + ']').val();

                if( datestr.length > 0 )
                {
                    var year  = new Date( Date.parse( '1 Jan ' + datestr ) ).getFullYear();
                    jQuery(this).datepicker('option', 'minDate', new Date( year, 1, 1 ));

                    if ( datestr >= new Date().getFullYear()) 
                    {
                        jQuery(this).datepicker('option', 'yearRange', year + ':' + year );                       
                    }
                    else
                    {
                        var min = new Date().getFullYear() - datestr;
                        if ( min >= 5 ) 
                        {
                            jQuery(this).datepicker('option', 'yearRange', year + ':' + ( year + 5 ) );
                        }
                        else
                        {
                            jQuery(this).datepicker('option', 'yearRange', year + ':' + ( year + min ) );
                        }
                    }                    
                }
            }
        });
    }
}