<?php

add_actions( 'promo', 'ticket_promo' );
add_actions( 'ticket-promo-ajax_page', 'ticket_promo_ajax' );

function ticket_promo()
{
    if( is_num_promo() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'schedules&sub=promo&prc=add_new' ) );

        exit;
    }

	if( is_add_new() )
    {
        return ticket_add_new_promo();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_promo( $_GET['id'] ) )
        {
            return ticket_edit_promo();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_promo();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_promo( $val );
        }
    }

    return ticket_promo_table();
}

/*
| -------------------------------------------------------------------------------------
| Promo Code Table List
| -------------------------------------------------------------------------------------
*/
function ticket_promo_table()
{
    $site_url = site_url();

    extract( get_filter_promo() );

	set_template( PLUGINS_PATH . '/ticket/tpl/promo/list.html', 'promo' );

    add_block( 'list-block', 'pmblock', 'promo' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );
    add_variable( 'pmstatus', get_promo_status_option( $pmstatus ) );

    add_variable( 'action', get_state_url( 'schedules&sub=promo' ) );
    add_variable( 'edit_link', get_state_url( 'schedules&sub=promo&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=promo&prc=add_new' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-promo-ajax/' );

    parse_template( 'list-block', 'pmblock', false );

    add_actions( 'section_title', 'Promo Code List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'promo' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Promo Code
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_promo()
{
	$message  = run_save_promo();
    $data     = get_promo();
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/promo/form.html', 'promo' );

    add_block( 'form-block', 'pmblock', 'promo' );

    add_variable( 'pmid', $data['pmid'] );
    add_variable( 'pmcode', $data['pmcode'] );
    add_variable( 'pmname', $data['pmname'] );
    add_variable( 'pmlimit', $data['pmlimit'] );
    add_variable( 'pmdiscount', $data['pmdiscount'] );
    add_variable( 'pmcommission', $data['pmcommission'] );
    add_variable( 'check_opt', $data['pmuselimit']=='1' ? 'checked' : '' );
    add_variable( 'radio_valid_opt', $data['pmstatus']=='1' ? 'checked' : '' );
    add_variable( 'radio_on_hold_opt', $data['pmstatus']=='2' ? 'checked' : '' );
    add_variable( 'radio_book24inadvance_no_opt', $data['pmbook24inadvance']=='1' ? 'checked' : '' );
    add_variable( 'radio_book24inadvance_yes_opt', $data['pmbook24inadvance']=='2' ? 'checked' : '' );
    add_variable( 'radio_additionaldisc_no_opt', $data['pmadditionaldisc']=='1' ? 'checked' : '' );
    add_variable( 'radio_additionaldisc_yes_opt', $data['pmadditionaldisc']=='2' ? 'checked' : '' );
    add_variable( 'radio_apply_on_channel_opt', $data['apply_on']=='0' ? 'checked' : '' );
    add_variable( 'radio_apply_on_agent_opt', $data['apply_on']=='1' ? 'checked' : '' );

    add_variable( 'pmsid', get_schedule_option( $data['pmsid'], false ) );
    add_variable( 'pmbaseon', get_promo_baseon_option( $data['pmbaseon'] ) );
    add_variable( 'chid', get_booking_source_option( $data['chid'], null, true, 'All Booking Source', true ) );
    add_variable( 'agid', get_promo_agent_option( $data['agid'], true, 'All Agent Type' ) );
    add_variable( 'pmapplication', get_promo_apps_option( $data['pmapplication'] ) );
    add_variable( 'pmdiscounttype', get_promo_disc_type_option( $data['pmdiscounttype'] ) );
    add_variable( 'pmcommissiontype', get_promo_commission_type_option( $data['pmcommissiontype'] ) );
    add_variable( 'pmto', empty( $data['pmto'] ) ? '' : date( 'd F Y', strtotime( $data['pmto'] ) ) );
    add_variable( 'pmfrom', empty( $data['pmfrom'] ) ? '' : date( 'd F Y', strtotime( $data['pmfrom'] ) ) );
    add_variable( 'pmcommissioncondition', get_promo_commission_condition_option( $data['pmcommissioncondition'] ) );

    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=promo' ) );
    add_variable( 'action', get_state_url( 'schedules&sub=promo&prc=add_new' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'pmblock', false );

    add_actions( 'section_title', 'New Promo Code' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'promo' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Promo Code
| -------------------------------------------------------------------------------------
*/
function ticket_edit_promo()
{
    $message  = run_update_promo();
    $data     = get_promo( $_GET['id'] );
    $site_url = site_url();
    
    set_template( PLUGINS_PATH . '/ticket/tpl/promo/form.html', 'promo' );

    add_block( 'form-block', 'pmblock', 'promo' );

    add_variable( 'site_url', $site_url);
    add_variable( 'pmid', $data['pmid'] );
    add_variable( 'pmcode', $data['pmcode'] );
    add_variable( 'pmname', $data['pmname'] );
    add_variable( 'pmlimit', $data['pmlimit'] );
    add_variable( 'pmlimit_day', $data['pmlimit_day'] );
    add_variable( 'pmdiscount', $data['pmdiscount'] );
    add_variable( 'pmcommission', $data['pmcommission'] );
    add_variable( 'check_opt', $data['pmuselimit']=='1' ? 'checked' : '' );
    add_variable( 'radio_valid_opt', $data['pmstatus']=='1' ? 'checked' : '' );
    add_variable( 'radio_on_hold_opt', $data['pmstatus']=='2' ? 'checked' : '' );
    add_variable( 'radio_book24inadvance_no_opt', $data['pmbook24inadvance']=='1' ? 'checked' : '' );
    add_variable( 'radio_book24inadvance_yes_opt', $data['pmbook24inadvance']=='2' ? 'checked' : '' );
    add_variable( 'radio_additionaldisc_no_opt', $data['pmadditionaldisc']=='1' ? 'checked' : '' );
    add_variable( 'radio_additionaldisc_yes_opt', $data['pmadditionaldisc']=='2' ? 'checked' : '' );
    add_variable( 'radio_apply_on_channel_opt', $data['apply_on']=='0' ? 'checked' : '' );
    add_variable( 'radio_apply_on_agent_opt', $data['apply_on']=='1' ? 'checked' : '' );

    add_variable( 'pmsid', get_schedule_option( $data['pmsid'], false ) );
    add_variable( 'pmbaseon', get_promo_baseon_option( $data['pmbaseon'] ) );
    add_variable( 'chid', get_booking_source_option( $data['chid'], null, true, 'All Booking Source', true ) );
    add_variable( 'agid', get_promo_agent_option( $data['agid'], true, 'All Agent Type' ) );
    add_variable( 'pmapplication', get_promo_apps_option( $data['pmapplication'] ) );
    add_variable( 'pmdiscounttype', get_promo_disc_type_option( $data['pmdiscounttype'] ) );
    add_variable( 'pmcommissiontype', get_promo_commission_type_option( $data['pmcommissiontype'] ) );
    add_variable( 'pmto', empty( $data['pmto'] ) ? '' : date( 'd F Y', strtotime( $data['pmto'] ) ) );
    add_variable( 'pmfrom', empty( $data['pmfrom'] ) ? '' : date( 'd F Y', strtotime( $data['pmfrom'] ) ) );
    add_variable( 'pmcommissioncondition', get_promo_commission_condition_option( $data['pmcommissioncondition'] ) );

    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=promo' ) );
    add_variable( 'action', get_state_url( 'schedules&sub=promo&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'delete_class', '' );

    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'promo' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-promo-ajax/' );

    parse_template( 'form-block', 'pmblock', false );

    add_actions( 'section_title', 'Edit Promo Code' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'promo' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Promo Code
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_promo()
{
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/promo/batch-delete.html', 'promo' );

    add_block( 'loop-block', 'pmlblock', 'promo' );
    add_block( 'delete-block', 'pmblock', 'promo' );

    foreach( $_POST['select'] as $key => $val )
    {
        $d = get_promo( $val );

        add_variable( 'pmid', $d['pmid'] );
        add_variable( 'pmname', $d['pmname'] );

        parse_template( 'loop-block', 'pmlblock', true );
    }

    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' promo?' );
    add_variable( 'action', get_state_url( 'schedules&sub=promo' ) );

    parse_template( 'delete-block', 'pmblock', false );

    add_actions( 'section_title', 'Delete Promo Code' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'promo' );
}

function run_save_promo()
{
    if( is_save_draft() || is_publish() )
    {
        $error = validate_promo_data();

        if( empty($error) )
        {
			if( check_promo_code() )
			{
				return array( 'type'=> 'error', 'content' =>  array ( 'Your promo code or name, have been registered' ) );
			}
			else
			{
				$post_id = save_promo();
				
				header( 'location:'.get_state_url( 'schedules&sub=promo&prc=add_new&result=1' ) );
				
				exit;
			}
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'New promo code successfully saved' ) );
    }
}

function run_update_promo()
{
    if( is_save_draft() || is_publish() )
    {
        $error = validate_promo_data();

        if( empty($error) )
        {
            $post_id = update_promo();

            header( 'location:'.get_state_url( 'schedules&sub=promo&prc=edit&result=1&id=' . $post_id ) );

            exit;
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This promo code successfully edited' ) );
    }
}

function validate_promo_data()
{
    $error = array();

    if( isset($_POST['pmname']) && empty( $_POST['pmname'] ) )
    {
        $error[] = 'Promo name can\'t be empty';
    }

    if( isset($_POST['pmcode']) && empty( $_POST['pmcode'] ) )
    {
        $error[] = 'Promo code can\'t be empty';
    }

    if( isset($_POST['pmdiscount']) && empty( $_POST['pmdiscount'] ) )
    {
        $error[] = 'Discount value can\'t be empty';
    }

    if( !isset($_POST['pmstatus']) )
    {
        $error[] = 'Promo status can\'t be empty';
    }

    if( isset($_POST['pmfrom']) && empty( $_POST['pmfrom'] ) )
    {
        $error[] = 'Start promo date can\'t be empty';
    }

    if( isset($_POST['pmto']) && empty( $_POST['pmto'] ) )
    {
        $error[] = 'End promo date can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo Code List Count
| -------------------------------------------------------------------------------------
*/
function is_num_promo()
{
    global $db;

    $s = 'SELECT * FROM ticket_promo';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Promo Code Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_promo_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1  => 'a.pmname',
        2  => 'a.pmfrom',
        3  => 'a.pmto',
        4  => 'a.pmdiscount',
        5  => 'a.pmcode',
        6  => 'a.pmstatus'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.pmid DESC';
    }

    $w = array();

    if( empty( $_POST['pmstatus'] ) === false )
    {
        $w[] = $db->prepare_query( 'pmstatus = %s', $_POST['pmstatus'] );
    }

    if( empty( $rdata['search']['value']) )
    {
        $where = '';

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' AND ', $w );
        }

        $s = 'SELECT * FROM ticket_promo AS a ' . $where . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        if( empty( $w ) === false )
        {
            $where = implode( ' AND ', $w ) . ' AND ( ' . implode( ' OR ', $search ) . ' )';
        }
        else
        {
            $where = implode( ' OR ', $search );
        }

        $s = 'SELECT * FROM ticket_promo AS a WHERE ' . $where . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $pmdiscount = $d2['pmdiscounttype'] == '0' ? $d2['pmdiscount'] . ' %' : 'Rp. ' . number_format( $d2['pmdiscount'], 0, ',', '.' );
            $pmstatus   = $d2['pmstatus'] == '1' ? 'Valid' : ( $d2['pmstatus'] == '2' ? 'On Hold' : '-' );

            $data[] = array(
                'pmstatus'   => $pmstatus,
                'pmdiscount' => $pmdiscount,
                'pmid'       => $d2['pmid'],
                'pmname'     => $d2['pmname'],
                'pmcode'     => $d2['pmcode'],
                'pmto'       => date('d F Y', strtotime( $d2['pmto'] ) ),
                'pmfrom'     => date('d F Y', strtotime( $d2['pmfrom'] ) ),
                'edit_link'  => get_state_url( 'schedules&sub=promo&prc=edit&id=' . $d2['pmid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo Code By ID
| -------------------------------------------------------------------------------------
*/
function get_promo( $id='' )
{
    global $db;

    $data = array(
        'pmid'                  => ( isset( $_POST['pmid'] ) ? $_POST['pmid'] : null ),
        'agid'                  => ( isset( $_POST['chid'] ) ? $_POST['chid'] : null ),
        'chid'                  => ( isset( $_POST['chid'] ) ? $_POST['chid'] : null ),
        'pmsid'                 => ( isset( $_POST['pmsid'] ) ? $_POST['pmsid'] : '' ),
        'pmname'                => ( isset( $_POST['pmname'] ) ? $_POST['pmname'] : '' ),
        'pmcode'                => ( isset( $_POST['pmcode'] ) ? $_POST['pmcode'] : '' ),
        'pmfrom'                => ( isset( $_POST['pmfrom'] ) ? $_POST['pmfrom'] : '' ),
        'pmto'                  => ( isset( $_POST['pmto'] ) ? $_POST['pmto'] : '' ),
        'pmstatus'              => ( isset( $_POST['pmstatus'] ) ? $_POST['pmstatus'] : 1 ),
        'pmbook24inadvance'     => ( isset( $_POST['pmbook24inadvance'] ) ? $_POST['pmbook24inadvance'] : '1' ),
        'pmcommission'          => ( isset( $_POST['pmcommission'] ) ? $_POST['pmcommission'] : 0 ),
        'pmcommissiontype'      => ( isset( $_POST['pmcommissiontype'] ) ? $_POST['pmcommissiontype'] : '' ),
        'pmcommissioncondition' => ( isset( $_POST['pmcommissioncondition'] ) ? $_POST['pmcommissioncondition'] : '' ),
        'pmdiscount'            => ( isset( $_POST['pmdiscount'] ) ? $_POST['pmdiscount'] : 0 ),
        'pmdiscounttype'        => ( isset( $_POST['pmdiscounttype'] ) ? $_POST['pmdiscounttype'] : '' ),
        'pmadditionaldisc'      => ( isset( $_POST['pmadditionaldisc'] ) ? $_POST['pmadditionaldisc'] : '1' ),
        'pmuselimit'            => ( isset( $_POST['pmuselimit'] ) ? $_POST['pmuselimit'] : '' ),
        'pmbaseon'              => ( isset( $_POST['pmbaseon'] ) ? $_POST['pmbaseon'] : '' ),
        'pmapplication'         => ( isset( $_POST['pmapplication'] ) ? $_POST['pmapplication'] : array( '0', '1', '2' ) ),
        'pmlimit'               => ( isset( $_POST['pmlimit'] ) ? $_POST['pmlimit'] : 0 ),
        'pmlimit_day'           => ( isset( $_POST['pmlimit_day'] ) ? $_POST['pmlimit_day'] : 0 ),
        'apply_on'              => ( isset( $_POST['apply_on'] ) ? $_POST['apply_on'] : 0 )
    );

    $s = 'SELECT * FROM ticket_promo WHERE pmid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $pmapplication = empty( $d['pmapplication'] ) ? array( '0', '1', '2' ) : json_decode( $d['pmapplication'], true );
            $pmsid         = empty( $d['pmsid'] ) ? '' : json_decode( $d['pmsid'], true );

            $data = array(
                'pmid'                  => $d['pmid'],
                'agid'                  => $d['agid'],
                'chid'                  => $d['chid'],
                'pmsid'                 => $pmsid,
                'pmname'                => $d['pmname'],
                'pmcode'                => $d['pmcode'],
                'pmfrom'                => $d['pmfrom'],
                'pmto'                  => $d['pmto'],
                'pmstatus'              => $d['pmstatus'],
                'pmbook24inadvance'     => $d['pmbook24inadvance'],
                'pmcommission'          => $d['pmcommission'],
                'pmcommissiontype'      => $d['pmcommissiontype'],
                'pmcommissioncondition' => $d['pmcommissioncondition'],
                'pmdiscount'            => $d['pmdiscount'],
                'pmdiscounttype'        => $d['pmdiscounttype'],
                'pmadditionaldisc'      => $d['pmadditionaldisc'],
                'pmuselimit'            => $d['pmuselimit'],
                'pmbaseon'              => $d['pmbaseon'],
                'pmapplication'         => $pmapplication,
                'pmlimit'               => $d['pmlimit'],
                'pmlimit_day'           => $d['pmlimit_day'],
                'apply_on'              => $d['apply_on'],
            );
        }
    }
    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo List
| -------------------------------------------------------------------------------------
*/
function get_promo_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_promo ORDER BY pmid';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data[] = $d;
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo List By Agent
| -------------------------------------------------------------------------------------
*/
function get_promo_list_by_agent( $agid = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_promo WHERE agid = %d ORDER BY pmid';
    $q = $db->prepare_query( $s, $agid );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data[] = $d;
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Cek Promo Code
| -------------------------------------------------------------------------------------
*/
function check_promo_code()
{
    global $db;

    if( isset( $_POST['pmname'] ) && $_POST['pmname'] != '' && isset( $_POST['pmcode'] ) && $_POST['pmcode'] != '' )
    {
        $s = 'SELECT * FROM ticket_promo WHERE pmname = %s OR pmcode = %s';
        $q = $db->prepare_query( $s, $_POST['pmname'], $_POST['pmcode']);
        $d = $db->do_query( $q ) ;
        $n = $db->num_rows( $d );
        
		if( $n > 0 )
		{
			return true;
		}
		else
		{
			return false;
		}
    }
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------
| Save Promo Code
| -------------------------------------------------------------------------------------
*/
function save_promo()
{
    global $db;

    $uselimit = isset( $_POST['pmuselimit'] ) ? '1' : '0';
    $pmsid    = isset( $_POST['pmsid'] ) && empty( $_POST['pmsid'] ) === false ? json_encode( $_POST['pmsid'] ) : '';
    
    if( isset( $_POST['apply_on'] )  && $_POST['apply_on'] == 0 )
    {
        $s = 'INSERT INTO ticket_promo(
                chid,
                pmsid,
                pmname,
                pmcode,
                pmbaseon,
                pmapplication,
                pmfrom,
                pmto,
                pmstatus,
                pmbook24inadvance,
                pmcommission,
                pmcommissiontype,
                pmcommissioncondition,
                pmdiscount,
                pmdiscounttype,
                pmadditionaldisc,
                pmuselimit,
                pmlimit,
                pmlimit_day,
                pmcreateddate,
                luser_id,
                apply_on)
              VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %d )';
        $q = $db->prepare_query( $s,
                $_POST['chid'],
                $pmsid,
                $_POST['pmname'],
                $_POST['pmcode'],
                $_POST['pmbaseon'],
                json_encode( $_POST['pmapplication'] ),
                date( 'Y-m-d', strtotime( $_POST['pmfrom'] ) ),
                date( 'Y-m-d', strtotime( $_POST['pmto'] ) ),
                $_POST['pmstatus'],
                $_POST['pmbook24inadvance'],
                $_POST['pmcommission'],
                $_POST['pmcommissiontype'],
                $_POST['pmcommissioncondition'],
                $_POST['pmdiscount'],
                $_POST['pmdiscounttype'],
                $_POST['pmadditionaldisc'],
                $uselimit,
                $_POST['pmlimit'],
                $_POST['pmlimit_day'],
                date( 'Y-m-d H:i:s' ),
                $_COOKIE['user_id'],
                $_POST['apply_on']);
    }
    else
    {
        $s = 'INSERT INTO ticket_promo(
                agid,
                pmsid,
                pmname,
                pmcode,
                pmbaseon,
                pmapplication,
                pmfrom,
                pmto,
                pmstatus,
                pmbook24inadvance,
                pmcommission,
                pmcommissiontype,
                pmcommissioncondition,
                pmdiscount,
                pmdiscounttype,
                pmadditionaldisc,
                pmuselimit,
                pmlimit,
                pmlimit_day,
                pmcreateddate,
                luser_id,
                apply_on)
              VALUES( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %d )';
        $q = $db->prepare_query( $s,
                $_POST['agid'],
                $pmsid,
                $_POST['pmname'],
                $_POST['pmcode'],
                $_POST['pmbaseon'],
                json_encode( $_POST['pmapplication'] ),
                date( 'Y-m-d', strtotime( $_POST['pmfrom'] ) ),
                date( 'Y-m-d', strtotime( $_POST['pmto'] ) ),
                $_POST['pmstatus'],
                $_POST['pmbook24inadvance'],
                $_POST['pmcommission'],
                $_POST['pmcommissiontype'],
                $_POST['pmcommissioncondition'],
                $_POST['pmdiscount'],
                $_POST['pmdiscounttype'],
                $_POST['pmadditionaldisc'],
                $uselimit,
                $_POST['pmlimit'],
                $_POST['pmlimit_day'],
                date( 'Y-m-d H:i:s' ),
                $_COOKIE['user_id'],
                $_POST['apply_on']);
    }

    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();

        save_log( $id, 'promo', 'Add new promo - ' . $_POST['pmname'] );

        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Promo Code
| -------------------------------------------------------------------------------------
*/
function update_promo()
{
    global $db;
    $current  = get_promo( $_POST['pmid'] );
    $uselimit = isset( $_POST['pmuselimit'] ) ? '1' : '0';
    $pmsid    = isset( $_POST['pmsid'] ) && empty( $_POST['pmsid'] ) === false ? json_encode( $_POST['pmsid'] ) : '';
    
    if( isset( $_POST['apply_on'] )  && $_POST['apply_on'] == 0)
    {
        $s = 'UPDATE ticket_promo SET
                chid = %d,
                agid = %d,
                pmsid = %s,
                pmname = %s,
                pmcode = %s,
                pmbaseon = %s,
                pmapplication = %s,
                pmfrom = %s,
                pmto = %s,
                pmstatus = %s,
                pmbook24inadvance = %s,
                pmcommission = %s,
                pmcommissiontype = %s,
                pmcommissioncondition = %s,
                pmdiscount = %s,
                pmdiscounttype = %s,
                pmadditionaldisc = %s,
                pmuselimit = %s,
                pmlimit = %s,
                pmlimit_day = %s,
                apply_on = %s
              WHERE pmid = %d';
        $q = $db->prepare_query( $s,
                $_POST['chid'],
                0,
                $pmsid,
                $_POST['pmname'],
                $_POST['pmcode'],
                $_POST['pmbaseon'],
                json_encode( $_POST['pmapplication'] ),
                date( 'Y-m-d', strtotime( $_POST['pmfrom'] ) ),
                date( 'Y-m-d', strtotime( $_POST['pmto'] ) ),
                $_POST['pmstatus'],
                $_POST['pmbook24inadvance'],
                $_POST['pmcommission'],
                $_POST['pmcommissiontype'],
                $_POST['pmcommissioncondition'],
                $_POST['pmdiscount'],
                $_POST['pmdiscounttype'],
                $_POST['pmadditionaldisc'],
                $uselimit,
                $_POST['pmlimit'],
                $_POST['pmlimit_day'],
                $_POST['apply_on'],
                $_POST['pmid'] );
    }
    else
    {
        $s = 'UPDATE ticket_promo SET
                agid = %d,
                chid = %d,
                pmsid = %s,
                pmname = %s,
                pmcode = %s,
                pmbaseon = %s,
                pmapplication = %s,
                pmfrom = %s,
                pmto = %s,
                pmstatus = %s,
                pmbook24inadvance = %s,
                pmcommission = %s,
                pmcommissiontype = %s,
                pmcommissioncondition = %s,
                pmdiscount = %s,
                pmdiscounttype = %s,
                pmadditionaldisc = %s,
                pmuselimit = %s,
                pmlimit = %s,
                pmlimit_day = %s,
                apply_on = %s
              WHERE pmid = %d';
        $q = $db->prepare_query( $s,
                $_POST['agid'],
                0,
                $pmsid,
                $_POST['pmname'],
                $_POST['pmcode'],
                $_POST['pmbaseon'],
                json_encode( $_POST['pmapplication'] ),
                date( 'Y-m-d', strtotime( $_POST['pmfrom'] ) ),
                date( 'Y-m-d', strtotime( $_POST['pmto'] ) ),
                $_POST['pmstatus'],
                $_POST['pmbook24inadvance'],
                $_POST['pmcommission'],
                $_POST['pmcommissiontype'],
                $_POST['pmcommissioncondition'],
                $_POST['pmdiscount'],
                $_POST['pmdiscounttype'],
                $_POST['pmadditionaldisc'],
                $uselimit,
                $_POST['pmlimit'],
                $_POST['pmlimit_day'],
                $_POST['apply_on'],
                $_POST['pmid'] );
    }
    if( $db->do_query( $q ) )
    {
        if( $current['pmname'] != $_POST['pmname'] )
        {
            save_log( $_POST['pmid'], 'promo', 'Update promo - ' . $current['pmname'] . ' to ' . $_POST['pmname'] );
        }
        else
        {
            save_log( $_POST['pmid'], 'promo', 'Update promo - ' . $_POST['pmname'] );
        }

        return $_POST['pmid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Promo Code Limit per Day
| -------------------------------------------------------------------------------------
*/
function get_promo_limit_perday($pmcode)
{
    global $db;
    $limit = 0;
    $s = 'SELECT
            pmlimit_day
          FROM ticket_promo
          WHERE pmcode = %s';
    $q = $db->prepare_query( $s, $pmcode);
    $r = $db->do_query( $q );


		while( $d = $db->fetch_array( $r ) )
		{
			$limit +=  $d['pmlimit_day'];
		}
		return $limit;
}

/*
| -------------------------------------------------------------------------------------
| Update Promo Code
| -------------------------------------------------------------------------------------
*/
function delete_promo( $id = '', $is_ajax = false )
{
    global $db;

    $s = 'SELECT b.pmid, b.pmname, a.bid
          FROM ticket_booking AS a
          RIGHT JOIN ticket_promo AS b ON a.pmcode = b.pmcode
          WHERE b.pmid = %d AND a.bstt <> %s ';
    $q = $db->prepare_query( $s, $id, 'ar' );
    $r = $db->do_query( $q );

    if( isset( $r['error_code'] ) )
    {
        $mess = array( 'result' => 'error-query', 'error_code' => 1451 );

        if( $is_ajax )
        {
            return $mess;
        }
        else
        {
            header( 'location:' . get_state_url( 'schedules&sub=promo&error-query=' . base64_encode( json_encode( $mess ) ) ) );

            exit;
        }
    }
    else
    {
        $d = $db->fetch_array( $r );
        $n = $db->num_rows( $r );

        if( ( $n > 0 && is_null( $d['bid'] ) ) || $n == 0 )
        {
            $s2 = 'DELETE FROM ticket_promo WHERE pmid = %d';
            $q2 = $db->prepare_query( $s2, $id );
            $r2 = $db->do_query( $q2 );

            if( isset( $r2['error_code'] ) )
            {
                if( $is_ajax )
                {
                    return $r2;
                }
                else
                {
                    header( 'location:' . get_state_url( 'schedules&sub=promo&error-query=' . base64_encode( json_encode( $r ) ) ) );

                    exit;
                }
            }
            else
            {
                save_log( $id, 'promo', 'Delete promo - ' . $d['pmname'] );

                return true;
            }
        }
        else
        {
            $mess = array( 'result' => 'failed' );

            if( $is_ajax )
            {
                return $mess;
            }
            else
            {
                return false;
            }
        }
    }
}

function get_promo_code_option( $pmcode = '', $use_empty = true, $empty_text = 'Select Promo Code' )
{
    $uri = cek_url();

    if( $uri[0] == 'agent' && isset( $_COOKIE['agid'] ) )
    {
        $promo = get_promo_list_by_agent( $_COOKIE['agid'] );
    }
    else
    {
        $promo = get_promo_list();
    }

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $promo ) )
    {
        foreach( $promo as $d )
        {
            if( is_array( $pmcode ) )
            {
                $option .= '<option value="' . $d['pmcode'] . '" ' . ( in_array( $d['pmcode'], $pmcode ) ? 'selected' : '' ) . ' >' . $d['pmcode'] . '</option>';
            }
            else
            {
                $option .= '<option value="' . $d['pmcode'] . '" ' . ( $pmcode == $d['pmcode'] ? 'selected' : '' ) . '>' . $d['pmcode'] . '</option>';
            }
        }
    }

    return $option;
}

function get_promo_agent_option( $agid = '', $use_empty = true, $empty_text = 'Select Agent' )
{
    $agent  = get_agent_list();
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( empty( $agent ) === false )
    {
        foreach( $agent as $d )
        {
            if( $d['agstatus'] != '1' )
            {
                $option .= '<option value="' . $d['agid'] . '" ' . ( $agid == $d['agid'] ? 'selected' : '' ) . '>' . $d['agname'] . '</option>';
            }
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo Status Option
| -------------------------------------------------------------------------------------
*/
function get_promo_status_option( $status='', $use_empty = true, $empty_text = 'Select Status' )
{
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="1" ' . ( $status=='1' ? 'selected' : '' ) . '>Valid</option>
    <option value="2" ' . ( $status=='2' ? 'selected' : '' ) . '>On Hold</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo Disc Type Option
| -------------------------------------------------------------------------------------
*/
function get_promo_disc_type_option( $type='' )
{
    $option = '
    <option value="0" ' . ( $type=='0' ? 'selected' : '' ) . '>%</option>
    <option value="1" ' . ( $type=='1' ? 'selected' : '' ) . '>Rp.</option>
    <option value="2" ' . ( $type=='2' ? 'selected' : '' ) . '>Industry Rate</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo Commission Type Option
| -------------------------------------------------------------------------------------
*/
function get_promo_commission_type_option( $type='' )
{
    $option = '
    <option value="0" ' . ( $type=='0' ? 'selected' : '' ) . '>%</option>
    <option value="1" ' . ( $type=='1' ? 'selected' : '' ) . '>Rp.</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo Commission Condition Option
| -------------------------------------------------------------------------------------
*/
function get_promo_commission_condition_option( $type='' )
{
    $option = '
    <option value="0" ' . ( $type=='0' ? 'selected' : '' ) . '>Applied at price after discount</option>
    <option value="1" ' . ( $type=='1' ? 'selected' : '' ) . '>Applied at price before discount</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo Disc Base On Option
| -------------------------------------------------------------------------------------
*/
function get_promo_baseon_option( $baseon='' )
{
    $option = '
    <option value="0" ' . ( $baseon=='0' ? 'selected' : '' ) . '>Travel Date</option>
    <option value="1" ' . ( $baseon=='1' ? 'selected' : '' ) . '>Booking Date</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Promo Disc Base On Option
| -------------------------------------------------------------------------------------
*/
function get_promo_apps_option( $apps=array() )
{
    $option = '
    <option value="0" ' . ( in_array( '0', $apps ) ? 'selected' : '' ) . '>Admin</option>
    <option value="1" ' . ( in_array( '1', $apps ) ? 'selected' : '' ) . '>Frontend</option>
    <option value="2" ' . ( in_array( '2', $apps ) ? 'selected' : '' ) . '>Agent</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Filter
| -------------------------------------------------------------------------------------
*/

function get_filter_promo()
{
    $filter = array( 'pmstatus' => '' );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $filter  = array( 'pmstatus' => $pmstatus );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $filter  = array( 'pmstatus' => $pmstatus );
    }

    return $filter;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_promo_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_promo_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-promo' )
    {
        $d = delete_promo( $_POST['pmid'], true );

        if( $d === true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}

?>
