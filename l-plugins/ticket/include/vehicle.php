<?php

add_actions( 'vehicle', 'ticket_vehicle' );
add_actions( 'ticket-vehicle-ajax_page', 'ticket_vehicle_ajax' );

function ticket_vehicle()
{
    if( is_num_vehicle() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'transport&sub=vehicle&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_vehicle();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_vehicle( $_GET['id'] ) )
        {
            return ticket_edit_vehicle();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_vehicle();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_vehicle( $val );
        }
    }

    return ticket_vehicle_table();
}

/*
| -------------------------------------------------------------------------------------
| Vehicle Table List
| -------------------------------------------------------------------------------------
*/
function ticket_vehicle_table()
{
	global $db;

    $site_url = site_url();

    extract( get_filter_vehicle_owner() );

    $w     = '';
    $where = '';

    if ( $fowner != '' ) 
    {
        $w[] = $db->prepare_query( 'void=%d', $fowner );
    }

    if ( $fstatus != '' ) 
    {
        $w[] = $db->prepare_query( 'vstatus=%s', $fstatus );
    }

    if ( !empty( $w ) ) 
    {
        $where = ' WHERE '. implode( ' AND ', $w );
    }

    $s = 'SELECT * FROM ticket_transport_vehicle' . $where . ' ORDER BY vid DESC';
    $q = $db->prepare_query( $s );
	$r = $db->do_query( $q );

	set_template( PLUGINS_PATH . '/ticket/tpl/vehicle/list.html', 'vehicle' );
    add_block( 'list-block', 'lcblock', 'vehicle' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', ticket_vehicle_table_data( $r ) );
    add_variable( 'message', generate_error_query_message_block() );
    add_variable( 'owner_list', get_vehicle_owner_option( $fowner ) );
    add_variable( 'status_list', get_vehicle_status( $fstatus ) );

    add_variable( 'action', get_state_url( 'transport&sub=vehicle' ) );
    add_variable( 'add_new_link', get_state_url( 'transport&sub=vehicle&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'transport&sub=vehicle&prc=edit' ) );
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Vehicle List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'vehicle' );
}

/*
| -------------------------------------------------------------------------------------
| Vehicle Table List Item
| -------------------------------------------------------------------------------------
*/
function ticket_vehicle_table_data( $r, $i = 1 )
{
    global $db;
    
    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="6">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/vehicle/loop.html', 'vehicle-loop' );

    add_block( 'loop-block', 'lcloop', 'vehicle-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'id', $d['vid'] );
        add_variable( 'vname', $d['vname'] );
        add_variable( 'vpassenger', $d['vpassenger'] );
        add_variable( 'vowner', get_vehicle_owner( $d['void'], 'voname' ) );
        add_variable( 'vstatus', $d['vstatus'] == '0' ? 'Ready' : 'Maintenance' );
        add_variable( 'vusedfor', $d['vusedfor'] == '0' ? 'Private Transport' : ( $d['vusedfor'] == '1' ? 'Shared Transport' : 'Private & Shared' ) );

        add_variable( 'edit_link', get_state_url( 'transport&sub=vehicle&prc=edit&id=' . $d['vid'] ) );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-vehicle-ajax/' );

        parse_template( 'loop-block', 'lcloop', true );
    }

    return return_template( 'vehicle-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Vehicle
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_vehicle()
{
	$message = run_save_vehicle();
    $data    = get_vehicle();

	set_template( PLUGINS_PATH . '/ticket/tpl/vehicle/form.html', 'vehicle' );
    add_block( 'form-block', 'lcblock', 'vehicle' );

    add_variable( 'vid', $data['vid'] );
    add_variable( 'vname', $data['vname'] );
    add_variable( 'vpassenger', $data['vpassenger'] );
    add_variable( 'void', get_vehicle_owner_option( $data['void'] ) );
    add_variable( 'vusedfor', get_vehicle_usedfor_option( $data['vusedfor'] ) );
    add_variable( 'radio_ready_opt', $data['vstatus']=='0' ? 'checked' : '' );
    add_variable( 'radio_maintenance_opt', $data['vstatus']=='1' ? 'checked' : '' );

    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'transport&sub=vehicle&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=vehicle' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'New Vehicle' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'vehicle' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Vehicle
| -------------------------------------------------------------------------------------
*/
function ticket_edit_vehicle()
{
    $message = run_update_vehicle();
    $data    = get_vehicle( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/vehicle/form.html', 'vehicle' );
    add_block( 'form-block', 'lcblock', 'vehicle' );

    add_variable( 'vid', $data['vid'] );
    add_variable( 'vname', $data['vname'] );
    add_variable( 'vpassenger', $data['vpassenger'] );
    add_variable( 'void', get_vehicle_owner_option( $data['void'] ) );
    add_variable( 'vusedfor', get_vehicle_usedfor_option( $data['vusedfor'] ) );
    add_variable( 'radio_ready_opt', $data['vstatus']=='0' ? 'checked' : '' );
    add_variable( 'radio_maintenance_opt', $data['vstatus']=='1' ? 'checked' : '' );

    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'transport&sub=vehicle&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-vehicle-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=vehicle' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'vehicle' ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Edit Vehicle' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'vehicle' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Vehicle
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_vehicle()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/vehicle/batch-delete.html', 'vehicle' );
    add_block( 'loop-block', 'lclblock', 'vehicle' );
    add_block( 'delete-block', 'lcblock', 'vehicle' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_vehicle( $val );

        add_variable('vname',  $d['vname'] );
        add_variable('vid', $d['vid'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' vehicle? :' );
    add_variable('action', get_state_url('transport&sub=vehicle'));

    parse_template( 'delete-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Delete vehicle' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'vehicle' );
}

function run_save_vehicle()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_vehicle_data();

        if( empty($error) )
        {
            $post_id = save_vehicle();
            
            header( 'location:'.get_state_url( 'transport&sub=vehicle&prc=add_new&result=1' ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'New vehicle successfully saved' ) );
    }
}

function run_update_vehicle()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_vehicle_data();

        if( empty($error) )
        {
            $post_id = update_vehicle();
            
            header( 'location:'.get_state_url( 'transport&sub=vehicle&prc=edit&result=1&id=' . $post_id ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This vehicle successfully edited' ) );
    }
}

function validate_vehicle_data()
{
    $error = array();

    if( isset($_POST['vname']) && empty( $_POST['vname'] ) )
    {
        $error[] = 'Vehicle can\'t be empty';
    }

    if( isset($_POST['vpassenger']) && empty( $_POST['vpassenger'] ) )
    {
        $error[] = 'Passenger can\'t be empty';
    }

    if( isset($_POST['void']) && empty( $_POST['void'] ) )
    {
        $error[] = 'Owner by can\'t be empty';
    }

    if( !isset($_POST['vstatus']) )
    {
        $error[] = 'Status can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle List Count
| -------------------------------------------------------------------------------------
*/
function is_num_vehicle()
{
    global $db;

    $s = 'SELECT * FROM ticket_transport_vehicle';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle By ID
| -------------------------------------------------------------------------------------
*/
function get_vehicle( $id = '', $field = '' )
{
    global $db;

    $data = array( 
        'vid'          => ( isset( $_POST['vid'] ) ? $_POST['vid'] : null ), 
        'void'         => ( isset( $_POST['void'] ) ? $_POST['void'] : '' ), 
        'vname'        => ( isset( $_POST['vname'] ) ? $_POST['vname'] : '' ), 
        'vpassenger'   => ( isset( $_POST['vpassenger'] ) ? $_POST['vpassenger'] : 0 ), 
        'vusedfor'     => ( isset( $_POST['vusedfor'] ) ? $_POST['vusedfor'] : '' ), 
        'vstatus'      => ( isset( $_POST['vstatus'] ) ? $_POST['vstatus'] : '' ),
        'vcreateddate' => ( isset( $_POST['vcreateddate'] ) ? $_POST['vcreateddate'] : '' ), 
        'luser_id'     => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' )
    );

    $s = 'SELECT * FROM ticket_transport_vehicle WHERE vid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'vid' => $d['vid'], 
                'void' => $d['void'], 
                'vname' => $d['vname'], 
                'vpassenger' => $d['vpassenger'], 
                'vusedfor' => $d['vusedfor'], 
                'vstatus' => $d['vstatus'],
                'vcreateddate' => $d['vcreateddate'],
                'luser_id' => $d['luser_id']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle Capacity
| -------------------------------------------------------------------------------------
*/
function get_vehicle_capacity( $vid )
{
    return get_vehicle( $vid, 'vpassenger' );
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle List
| -------------------------------------------------------------------------------------
*/
function get_vehicle_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_transport_vehicle';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array( 
            'vid' => $d['vid'], 
            'void' => $d['void'], 
            'vname' => $d['vname'], 
            'vpassenger' => $d['vpassenger'], 
            'vusedfor' => $d['vusedfor'], 
            'vstatus' => $d['vstatus'],
            'vcreateddate' => $d['vcreateddate'],
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Vehicle
| -------------------------------------------------------------------------------------
*/
function save_vehicle()
{
    global $db;
    
    $s = 'INSERT INTO ticket_transport_vehicle( 
            void,
            vname,
            vpassenger,
            vusedfor,
            vstatus,
            vcreateddate, 
            luser_id ) VALUES( %d, %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s, 
            $_POST['void'], 
            $_POST['vname'], 
            $_POST['vpassenger'], 
            $_POST['vusedfor'], 
            $_POST['vstatus'], 
            date( 'Y-m-d H:i:s' ), 
            $_COOKIE['user_id'] );
       
    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();
        
        save_log( $id, 'vehicle', 'Add new vehicle - ' . $_POST['vname'] );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Vehicle
| -------------------------------------------------------------------------------------
*/
function update_vehicle()
{
    global $db;

    $d = get_vehicle( $_POST['vid'] );

    $s = 'UPDATE ticket_transport_vehicle SET 
            void = %d,
            vname = %s,
            vpassenger = %s,
            vusedfor = %s,
            vstatus = %s
          WHERE vid = %d';     
    $q = $db->prepare_query( $s,  
            $_POST['void'], 
            $_POST['vname'], 
            $_POST['vpassenger'], 
            $_POST['vusedfor'], 
            $_POST['vstatus'], 
            $_POST['vid'] );
       
    if( $db->do_query( $q ) )
    {
        $u      = array( 'Private Transport', 'Shared Transport', 'Private and Shared' );
        $s      = array( 'Ready', 'Maintenance' );
        $logs   = array();
        $logs[] = $d['vname'] != $_POST['vname'] ? 'Change Vehicle Name : ' . $d['vname'] . ' to ' . $_POST['vname'] : '';
        $logs[] = $d['vpassenger'] != $_POST['vpassenger'] ? 'Change Vehicle Passenger : ' . $d['vpassenger'] . ' to ' . $_POST['vpassenger'] : '';
        $logs[] = $d['vusedfor'] != $_POST['vusedfor'] ? 'Change Vehicle Used For : ' . $u[ $d['vusedfor'] ] . ' to ' . $u[ $_POST['vusedfor'] ] : '';
        $logs[] = $d['void'] != $_POST['void'] ? 'Change Vehicle Owned : ' . get_data_by_id( 'voname', 'ticket_transport_vehicle_owner', 'void', $d['void'] ) . ' to ' . get_data_by_id( 'voname', 'ticket_transport_vehicle_owner', 'void', $_POST['void'] ) : '';
        $logs[] = $d['vstatus'] != $_POST['vstatus'] ? 'Change Status : ' . $s[ $d['vstatus'] ] . ' to ' . $s[ $_POST['vstatus'] ] : '';

        if ( !empty( $logs ) ) 
        {
            foreach ( $logs as $i => $log ) 
            {
                if ( $log == '' ) 
                {
                    unset( $logs[$i] );
                }
            }

            save_log( $_POST['vid'], 'vehicle', 'Update Vehicle - ' . $d['vname'] . '<br/>' . implode( '<br/>', $logs ) );
        }

        return $_POST['vid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Vehicle
| -------------------------------------------------------------------------------------
*/
function delete_vehicle( $id='', $is_ajax = false )
{
    global $db;

    $d = get_vehicle( $id );

    $s = 'DELETE FROM ticket_transport_vehicle WHERE vid = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'transport&sub=vehicle&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'vehicle', 'Delete vehicle - ' . $d['vname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle Option
| -------------------------------------------------------------------------------------
*/
function get_vehicle_option( $vid = '', $use_empty = true, $empty_text = 'Select Vehicle' )
{
    $vehicle = get_vehicle_list();
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $vehicle ) )
    {
        foreach( $vehicle as $d )
        {
            $option .= '<option value="' . $d['vid'] . '" ' . ( $d['vid'] == $vid ? 'selected' : '' ) . ' >' . $d['vname'] . '</option>';   
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle Option
| -------------------------------------------------------------------------------------
*/
function get_vehicle_usedfor_option( $usedfor = '' )
{
    return '
    <option value="0" ' . ( $usedfor=='0' ? 'selected' : '' ) . ' >Private Transport</option>
    <option value="1" ' . ( $usedfor=='1' ? 'selected' : '' ) . ' >Shared Transport</option>
    <option value="2" ' . ( $usedfor=='2' ? 'selected' : '' ) . ' >Private and Shared</option>';
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle Status
| -------------------------------------------------------------------------------------
*/
function get_vehicle_status( $vstatus = '' )
{

    return '
    <option value="">Select Status</option>
    <option value="0" ' . ( $vstatus=='0' ? 'selected' : '' ) . ' >Ready</option>
    <option value="1" ' . ( $vstatus=='1' ? 'selected' : '' ) . ' >Maintenance</option>';
}

/*
| -------------------------------------------------------------------------------------
| Filter
| -------------------------------------------------------------------------------------
*/

function get_filter_vehicle_owner()
{
    $filter = array( 'fowner' => '', 'fstatus' => '' );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $filter  = array( 'fowner' => $fowner, 'fstatus' => $fstatus );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $filter  = array( 'fowner' => $fowner, 'fstatus' => $fstatus );
    }

    return $filter;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_vehicle_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-vehicle' )
    {
        $d = delete_vehicle( $_POST['vid'], true );

        if( $d === true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}

?>