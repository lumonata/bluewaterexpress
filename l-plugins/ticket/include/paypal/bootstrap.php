<?php

$composerAutoload = dirname(__DIR__) . '/paypal/vendor/autoload.php';

if (!file_exists($composerAutoload))
{
    echo "The 'vendor' folder is missing. You must run 'composer update' to resolve application dependencies.\nPlease see the README for more information.\n";
    exit(1);
}

require $composerAutoload;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

date_default_timezone_set(@date_default_timezone_get());

function getApiContext( $clientId, $clientSecret )
{
    $apiContext = new ApiContext(
        new OAuthTokenCredential(
            $clientId,
            $clientSecret
        )
    );

    $apiContext->setConfig(
        array(
            'mode' => 'live',
            'log.LogEnabled' => true,
            'log.FileName' => '../PayPal.log',
            'log.LogLevel' => 'DEBUG',
            'cache.enabled' => true
        )
    );

    return $apiContext;
}
