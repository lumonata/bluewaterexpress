<?php

add_actions( 'agent', 'ticket_agent' );
add_actions( 'ticket-agent-ajax_page', 'ticket_agent_ajax' );
add_actions( 'agent-export-csv_page', 'ticket_agent_export_csv' );

function ticket_agent()
{
    $is_access = is_access( $_COOKIE['user_id'], $_GET['state'], $_GET['sub'] );

    if( is_num_agent() == 0 && !isset( $_GET['prc'] ) && $is_access )
    {
        header( 'location:' . get_state_url( 'booking-source&sub=agent&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        if ( $is_access )
        {
            return ticket_add_new_agent();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_detail() )
    {
        if( isset( $_GET['id'] ) && get_agent( $_GET['id'] ) )
        {
            return ticket_detail_agent();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_agent( $_GET['id'] ) && $is_access )
        {
            return ticket_edit_agent();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        if ( $is_access )
        {
            return ticket_batch_delete_agent();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_confirm_delete() && $is_access )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_agent( $val );
        }
    }

    return ticket_agent_table();
}

/*
| -------------------------------------------------------------------------------------
| Agent Table List
| -------------------------------------------------------------------------------------
*/
function ticket_agent_table()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url( 'booking-source&sub=agent' ) . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    $filter   = ticket_filter_agent();
    $site_url = site_url();

    extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/agent/list.html', 'agent' );

    add_block( 'list-block', 'lcblock', 'agent' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'channel_option', get_channel_option( $chid ) );
    add_variable( 'sales_person_option', get_agent_sales_person_option( $agsp ) );

    add_variable( 'site_url', HTSERVER . site_url() );
    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );

    add_variable( 'action', get_state_url( 'booking-source&sub=agent' ) );
    add_variable( 'edit_link', get_state_url( 'booking-source&sub=agent&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'booking-source&sub=agent&prc=add_new' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-agent-ajax' );

    parse_template( 'list-block', 'lcblock', false );

    add_actions( 'section_title', 'Agent List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'agent' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Agent
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_agent()
{
	run_save_agent();

    $site_url = site_url();
    $data     = get_agent();

	set_template( PLUGINS_PATH . '/ticket/tpl/agent/form.html', 'agent' );
    add_block( 'form-block', 'lcblock', 'agent' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'agname', $data['agname'] );

    add_variable( 'agsp', $data['agsp'] );
    add_variable( 'agusername', $data['agusername'] );
    add_variable( 'agtopupval', $data['agtopupval'] );
    add_variable( 'agfvdatefrom', $data['agfreelance'][0]['agfvdatefrom'] );
    add_variable( 'agfvdateto', $data['agfreelance'][0]['agfvdateto'] );
    add_variable( 'agfcommission', $data['agfreelance'][0]['agfcommission'] );
    add_variable( 'agfreelance_code', $data['agfreelance'][0]['agfreelance_code'] );
    add_variable( 'agfneverend_check', $data['agfreelance'][0]['agfneverend'] == '0' ? '' : 'checked' );

    add_variable( 'agaddress', $data['agaddress'] );
    add_variable( 'agemail', $data['agemail'] );
    add_variable( 'agcp', $data['agcp'] );
    add_variable( 'agposition', $data['agposition'] );
    add_variable( 'agmobile', $data['agmobile'] );
    add_variable( 'agphone', $data['agphone'] );
    add_variable( 'agfax', $data['agfax'] );
    add_variable( 'agophone', $data['agophone'] );

    add_variable( 'cagaddress', $data['cagaddress'] );
    add_variable( 'cagemail', $data['cagemail'] );
    add_variable( 'cagcp', $data['cagcp'] );
    add_variable( 'cagposition', $data['cagposition'] );
    add_variable( 'cagmobile', $data['cagmobile'] );
    add_variable( 'cagphone', $data['cagphone'] );
    add_variable( 'cagfax', $data['cagfax'] );
    add_variable( 'cagophone', $data['cagophone'] );

    add_variable( 'iagaddress', $data['iagaddress'] );
    add_variable( 'iagemail', $data['iagemail'] );
    add_variable( 'iagcp', $data['iagcp'] );
    add_variable( 'iagposition', $data['iagposition'] );
    add_variable( 'iagmobile', $data['iagmobile'] );
    add_variable( 'iagphone', $data['iagphone'] );
    add_variable( 'iagfax', $data['iagfax'] );
    add_variable( 'iagophone', $data['iagophone'] );

    add_variable( 'gagaddress', $data['gagaddress'] );
    add_variable( 'gagemail', $data['gagemail'] );
    add_variable( 'gagcp', $data['gagcp'] );
    add_variable( 'gagposition', $data['gagposition'] );
    add_variable( 'gagmobile', $data['gagmobile'] );
    add_variable( 'gagphone', $data['gagphone'] );
    add_variable( 'gagfax', $data['gagfax'] );
    add_variable( 'gagophone', $data['gagophone'] );

    add_variable( 'chid', get_channel_option( $data['chid'] ) );
    add_variable( 'agcod', get_agcod_option( $data['agcod'] ) );
    add_variable( 'agtopup', get_agent_topup_option( $data['agtopup'] ) );
    add_variable( 'agcontract', set_agent_contract( $data['agcontract'] ) );
    add_variable( 'agtype', get_agent_rate_type_option( $data['agtype'] ) );
    add_variable( 'agstatus', get_agent_status_option( $data['agstatus'] ) );
    add_variable( 'agfcommission_type', get_agent_commission_type_option() );
    add_variable( 'agpayment_type', get_agent_payment_type_option( $data['agstatus'] ) );
    add_variable( 'agrate_validity', get_agrate_validity_content( $data['agnetrate'][0]['agdatevalidity'] ) );

    add_variable( 'topup_cls', $data['agtopup'] == '1' ? '' : 'sr-only' );
    add_variable( 'net_rate_cls', $data['agtype'] == 'Net Rate' ? '' : 'sr-only' );
    add_variable( 'freelance_cls', $data['agtype'] == 'Freelance Code' ? '' : 'sr-only' );
    add_variable( 'net_rate_list', get_net_rate_list( $data['agid'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'booking-source&sub=agent&prc=add_new' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-agent-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'booking-source&sub=agent' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'New Agent' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'agent' );
}

/*
| -------------------------------------------------------------------------------------
| Detail Agent
| -------------------------------------------------------------------------------------
*/
function ticket_detail_agent()
{
    $data = get_agent( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/agent/detail.html', 'agent' );
    add_block( 'detail-block', 'dtblock', 'agent' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'agname', $data['agname'] );

    add_variable( 'agaddress', $data['cagaddress'] );
    add_variable( 'agcp', $data['cagcp'] );
    add_variable( 'agemail', $data['cagemail'] );
    add_variable( 'agmobile', $data['cagmobile'] );
    add_variable( 'agphone', $data['cagphone'] );
    add_variable( 'agfax', $data['cagfax'] );
    add_variable( 'agophone', $data['cagophone'] );

    add_variable( 'agsp', $data['agsp'] );
    add_variable( 'agusername', $data['agusername'] );
    add_variable( 'agpayment_type', $data['agpayment_type'] );
    add_variable( 'agtype', $data['agtype'] );
    add_variable( 'chname', get_channel( $data['chid'], 'chname' ) );

    add_variable( 'chid', get_channel_option( $data['chid'] ) );
    add_variable( 'agstatus', get_agent_status_text( $data['agstatus'] ) );
    add_variable( 'net_rate_list', get_detail_rate_list( $data ) );

    add_variable( 'edit_link', get_state_url( 'booking-source&sub=agent&prc=edit&id=' . $data['agid'] ) );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-agent-ajax/' );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'agent' ) );

    parse_template( 'detail-block', 'dtblock', false );

    add_actions( 'section_title', 'Detail Agent' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'agent' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Agent
| -------------------------------------------------------------------------------------
*/
function ticket_edit_agent()
{
    run_update_agent();

    $site_url = site_url();
    $data     = get_agent( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/agent/form.html', 'agent' );
    add_block( 'form-block', 'lcblock', 'agent' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'agname', $data['agname'] );

    add_variable( 'agsp', $data['agsp'] );
    add_variable( 'agusername', $data['agusername'] );
    add_variable( 'agtopupval', $data['agtopupval'] );
    add_variable( 'agfvdatefrom', $data['agfreelance'][0]['agfvdatefrom'] );
    add_variable( 'agfvdateto', $data['agfreelance'][0]['agfvdateto'] );
    add_variable( 'agfcommission', $data['agfreelance'][0]['agfcommission'] );
    add_variable( 'agfreelance_code', $data['agfreelance'][0]['agfreelance_code'] );
    add_variable( 'agfneverend_check', $data['agfreelance'][0]['agfneverend'] == '0' ? '' : 'checked' );

    add_variable( 'agaddress', $data['agaddress'] );
    add_variable( 'agemail', $data['agemail'] );
    add_variable( 'agcp', $data['agcp'] );
    add_variable( 'agposition', $data['agposition'] );
    add_variable( 'agmobile', $data['agmobile'] );
    add_variable( 'agphone', $data['agphone'] );
    add_variable( 'agfax', $data['agfax'] );
    add_variable( 'agophone', $data['agophone'] );
    add_variable( 'agpercentage', $data['agpercentage'] );

    add_variable( 'cagaddress', $data['cagaddress'] );
    add_variable( 'cagemail', $data['cagemail'] );
    add_variable( 'cagcp', $data['cagcp'] );
    add_variable( 'cagposition', $data['cagposition'] );
    add_variable( 'cagmobile', $data['cagmobile'] );
    add_variable( 'cagphone', $data['cagphone'] );
    add_variable( 'cagfax', $data['cagfax'] );
    add_variable( 'cagophone', $data['cagophone'] );

    add_variable( 'iagaddress', $data['iagaddress'] );
    add_variable( 'iagemail', $data['iagemail'] );
    add_variable( 'iagcp', $data['iagcp'] );
    add_variable( 'iagposition', $data['iagposition'] );
    add_variable( 'iagmobile', $data['iagmobile'] );
    add_variable( 'iagphone', $data['iagphone'] );
    add_variable( 'iagfax', $data['iagfax'] );
    add_variable( 'iagophone', $data['iagophone'] );

    add_variable( 'gagaddress', $data['gagaddress'] );
    add_variable( 'gagemail', $data['gagemail'] );
    add_variable( 'gagcp', $data['gagcp'] );
    add_variable( 'gagposition', $data['gagposition'] );
    add_variable( 'gagmobile', $data['gagmobile'] );
    add_variable( 'gagphone', $data['gagphone'] );
    add_variable( 'gagfax', $data['gagfax'] );
    add_variable( 'gagophone', $data['gagophone'] );

    add_variable( 'chid', get_channel_option( $data['chid'] ) );
    add_variable( 'agcod', get_agcod_option( $data['agcod'] ) );
    add_variable( 'agtopup', get_agent_topup_option( $data['agtopup'] ) );
    add_variable( 'agcontract', set_agent_contract( $data['agcontract'] ) );
    add_variable( 'agtype', get_agent_rate_type_option( $data['agtype'] ) );
    add_variable( 'agstatus', get_agent_status_option( $data['agstatus'] ) );
    add_variable( 'agpayment_type', get_agent_payment_type_option( $data['agpayment_type'] ) );
    add_variable( 'agrate_validity', get_agrate_validity_content( $data['agnetrate'][0]['agdatevalidity'] ) );
    add_variable( 'agfcommission_type', get_agent_commission_type_option( $data['agfreelance'][0]['agfcommission_type'] ) );

    add_variable( 'topup_cls', $data['agtopup'] == '1' ? '' : 'sr-only' );
    add_variable( 'net_rate_cls', $data['agtype'] == 'Net Rate' ? '' : 'sr-only' );
    add_variable( 'freelance_cls', $data['agtype'] == 'Freelance Code' ? '' : 'sr-only' );
    add_variable( 'net_rate_list', get_net_rate_list( $data['agid'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'booking-source&sub=agent&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-agent-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'booking-source&sub=agent' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'agent' ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'Edit Agent' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'agent' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Agent
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_agent()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/agent/batch-delete.html', 'agent' );
    add_block( 'loop-block', 'lclblock', 'agent' );
    add_block( 'delete-block', 'lcblock', 'agent' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_agent( $val );

        add_variable( 'agname',  $d['agname'] );
        add_variable( 'agid', $d['agid'] );

        parse_template( 'loop-block', 'lclblock', true);
    }

    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' agent? :' );
    add_variable( 'action', get_state_url( 'booking-source&sub=agent' ) );

    parse_template( 'delete-block', 'lcblock', false );

    add_actions( 'section_title', 'Delete agent' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'agent' );
}

function run_save_agent()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_agent_data();

        if( empty( $error ) )
        {
            $post_id = save_agent();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new agent' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New agent successfully saved' ) ) );

                header( 'location:' . get_state_url( 'booking-source&sub=agent&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_agent()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_agent_data();

        if( empty( $error ) )
        {
            $post_id = update_agent();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this agent' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This agent successfully edited' ) ) );

                header( 'location:' . get_state_url( 'booking-source&sub=agent&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_agent_data()
{
    $error = array();

    if( isset( $_POST['agname'] ) && empty( $_POST['agname'] ) )
    {
        $error[] = 'Agent name can\'t be empty';
    }

    if( isset( $_POST['cagaddress'] ) && empty( $_POST['cagaddress'] ) )
    {
        $error[] = 'Contracting Address can\'t be empty';
    }

    if( isset( $_POST['cagemail'] ) && empty( $_POST['cagemail'] ) )
    {
        $error[] = 'Contracting Email can\'t be empty';
    }

    if( isset( $_POST['cagphone'] ) && empty( $_POST['cagphone'] ) && isset( $_POST['cagmobile'] ) && empty( $_POST['cagmobile'] ) )
    {
        $error[] = 'Contracting Phone or mobile number can\'t be empty';
    }

    if( isset( $_POST['agemail'] ) && empty( $_POST['agemail'] ) )
    {
        $error[] = 'Booking/Cancellation Email can\'t be empty';
    }

    if( isset( $_POST['iagemail'] ) && empty( $_POST['iagemail'] ) )
    {
        $error[] = 'Invoicing/Payment Email can\'t be empty';
    }

    if( isset( $_POST['chid'] ) && empty( $_POST['chid'] ) )
    {
        $error[] = 'Sales channel can\'t be empty';
    }

    if( $_POST['agtype'] == 'Net Rate' )
    {
        if( empty( $_POST['agvdatefrom'] ) || empty( $_POST['agvdatefrom'] ) )
        {
            $error[] = 'Rate validity can\'t be empty';
        }

        if( is_add_new()  )
        {
            if( empty( $_POST['agpassword'] )  || strlen( $_POST['agpassword'] ) < 7 )
            {
                $error[] = 'The password should be at least seven characters long';
            }

            if( $_POST['agpassword'] != $_POST['agrpassword'] )
            {
                $error[] = 'Password do not match';
            }

            if( isset( $_POST['agusername'] ) && empty( $_POST['agusername'] ) )
            {
                $error[] = 'Username can\'t be empty';
            }

            if( !empty( $_POST['agusername'] ) && is_duplicate_username_agent( $_POST['agusername'] )  )
            {
                $error[] = 'Username <b>"' . $_POST['agusername'] .'"</b> already registered before, use another username';
            }
        }
        elseif( is_edit() )
        {
            if( !empty( $_POST['agpassword'] ) )
            {
                if( strlen( $_POST['agpassword'] ) < 7 )
                {
                    $error[] = 'Password should be at least seven characters long';
                }
                elseif( $_POST['agpassword'] != $_POST['agrpassword'] )
                {
                    $error[] = 'Password do not match';
                }
            }

            if( isset( $_POST['agusername'] ) && !empty( $_POST['agusername'] ) )
            {
                $current_username = get_agent( $_GET['id'], 'agusername' );

                if( $_POST['agusername'] != $current_username && is_duplicate_username_agent( $_POST['agusername'] )  )
                {
                    $error[] = 'Username <b>"' . $_POST['agusername'] .'"</b> already registered before, use another username';
                }
            }
        }
    }
    elseif( $_POST['agtype'] == 'Freelance Code' )
    {
        if( empty( $_POST['agfvdatefrom'] ) || empty( $_POST['agfvdateto'] ) )
        {
            $error[] = 'Code validity can\'t be empty';
        }

        if( empty( $_POST['agfreelance_code'] ) )
        {
            $error[] = 'Freelance code can\'t be empty';
        }

        if( is_add_new()  )
        {
            if( empty( $_POST['agpassword'] )  || strlen( $_POST['agpassword'] ) < 7 )
            {
                $error[] = 'The password should be at least seven characters long';
            }

            if( $_POST['agpassword'] != $_POST['agrpassword'] )
            {
                $error[] = 'Password do not match';
            }

            if( isset( $_POST['agusername'] ) && empty( $_POST['agusername'] ) )
            {
                $error[] = 'Username can\'t be empty';
            }

            if( !empty( $_POST['agusername'] ) && is_duplicate_username_agent( $_POST['agusername'] )  )
            {
                $error[] = 'Username <b>"' . $_POST['agusername'] .'"</b> already registered before, use another username';
            }
        }
        elseif( is_edit() )
        {
            if( !empty( $_POST['agpassword'] ) )
            {
                if( strlen( $_POST['agpassword'] ) < 7 )
                {
                    $error[] = 'Password should be at least seven characters long';
                }
                elseif( $_POST['agpassword'] != $_POST['agrpassword'] )
                {
                    $error[] = 'Password do not match';
                }
            }

            if( isset( $_POST['agusername'] ) && !empty( $_POST['agusername'] ) )
            {
                $current_username = get_agent( $_GET['id'], 'agusername' );

                if( $_POST['agusername'] != $current_username && is_duplicate_username_agent( $_POST['agusername'] )  )
                {
                    $error[] = 'Username <b>"' . $_POST['agusername'] .'"</b> already registered before, use another username';
                }
            }
        }
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Agent List Count
| -------------------------------------------------------------------------------------
*/
function is_num_agent()
{
    global $db;

    $s = 'SELECT * FROM ticket_agent';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Check Username Agent Exist
| -------------------------------------------------------------------------------------
*/
function is_duplicate_username_agent( $agusername = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_agent WHERE agusername = %s';
    $q = $db->prepare_query( $s, $agusername );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Agent Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_agent_table_query( $chid = '', $agsp = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        0  => 'a.agid',
        1  => 'a.agname',
        2  => 'a.cagaddress',
        3  => 'a.agtype',
        5  => 'b.chname',
        6  => 'a.agpayment_type',
        7  => 'a.agstatus',
        8  => 'a.agsp'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.agid ASC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $w = array();

        if( $chid != '' )
        {
            $w[] = $db->prepare_query( 'b.chid = %d', $chid );
        }

        if( $agsp != '' )
        {
            $w[] = $db->prepare_query( 'a.agsp = %s', $agsp );
        }

        $s = 'SELECT
                a.agid,
                a.agtype,
                a.agname,
                a.agsp,
                a.cagaddress,
                a.cagemail,
                a.cagphone,
                a.cagmobile,
                a.agpayment_type,
                a.agstatus,
                b.chname
              FROM ticket_agent AS a
              LEFT JOIN ticket_channel AS b ON a.chid = b.chid';
        $s = $s . ( empty( $w ) ? ' ORDER BY ' . $order : ' WHERE ' . implode( ' AND ', $w ) . ' ORDER BY ' . $order );
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                a.agid,
                a.agtype,
                a.agname,
                a.agsp,
                a.cagaddress,
                a.cagemail,
                a.cagphone,
                a.cagmobile,
                a.agpayment_type,
                a.agstatus,
                b.chname
              FROM ticket_agent AS a
              LEFT JOIN ticket_channel AS b ON a.chid = b.chid
              WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            if( $d2['agtype'] == 'Net Rate' )
            {
                $s3 = 'SELECT agvdateto FROM ticket_agent_net_rate WHERE agid = %d ORDER BY agnid DESC LIMIT 1';
                $q3 = $db->prepare_query( $s3, $d2['agid'] );
                $r3 = $db->do_query( $q3 );

                if( is_array( $r3 ) )
                {
                    $agvalidto = '-';
                }
                else
                {
                    $d3 = $db->fetch_array( $r3 );

                    $agvalidto = date( 'd F Y', strtotime( $d3['agvdateto'] ) );
                }
            }
            elseif( $d2['agtype'] == 'Freelance Code' )
            {
                $s3 = 'SELECT agfneverend, agvdateto FROM ticket_agent_freelance_code WHERE agid = %d ORDER BY agfid DESC LIMIT 1';
                $q3 = $db->prepare_query( $s3, $d2['agid'] );
                $r3 = $db->do_query( $q3 );

                if( is_array( $r3 ) )
                {
                    $agvalidto = '-';
                }
                else
                {
                    $d3 = $db->fetch_array( $r3 );

                    if( $d3['agfneverend'] == '0' )
                    {
                        $agvalidto = date( 'd F Y', strtotime( $d3['agfvdateto'] ) );
                    }
                    else
                    {
                        $agvalidto = '&infin;';
                    }
                }
            }
            else
            {
                $agvalidto = '-';
            }

            $agstatus = $d2['agstatus'] == '0' ? 'Active' : 'Suspended';
            $agphone  = empty( $d2['cagphone'] ) ? $d2['cagmobile'] : $d2['cagphone'];

            $data[] = array(
                'agphone'        => $agphone,
                'agstatus'       => $agstatus,
                'agvalidto'      => $agvalidto,
                'agid'           => $d2['agid'],
                'agsp'           => $d2['agsp'],
                'agchanel'       => $d2['chname'],
                'agtype'         => $d2['agtype'],
                'agname'         => $d2['agname'],
                'agemail'        => $d2['cagemail'],
                'agaddress'      => $d2['cagaddress'],
                'agpayment_type' => $d2['agpayment_type'],
                'view_link'      => get_state_url( 'booking-source&sub=agent&prc=detail&id=' . $d2['agid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

function ticket_get_agent_table_export( $filter )
{
    global $db;

    extract( $filter );

    $w = array();

    if( $chid != '' )
    {
        $w[] = $db->prepare_query( 'c.chid = %d', $chid );
    }

    if( $agsp != '' )
    {
        $w[] = $db->prepare_query( 'a.agsp = %s', $agsp );
    }

    $where = !empty( $w ) ? ' WHERE ' . implode( ' AND ' , $w ) : '';

    $s  = 'SELECT
            a.agid,
            a.agtype,
            a.agname,
            c.chcode,
            c.chname,
            a.agaddress,
            a.agemail,
            a.agcp,
            a.agsp,
            a.agmobile,
            a.agphone,
            a.agfax,
            a.agophone,
            a.agposition,
            a.agpayment_type,
            a.agcod,
            a.agstatus,
            a.cagaddress,
            a.cagemail,
            a.cagcp,
            a.cagposition,
            a.cagmobile,
            a.cagphone,
            a.cagfax,
            a.cagophone,
            a.iagaddress,
            a.iagemail,
            a.iagcp,
            a.iagposition,
            a.iagmobile,
            a.iagphone,
            a.iagfax,
            a.iagophone,
            a.gagaddress,
            a.gagemail,
            a.gagcp,
            a.gagposition,
            a.gagmobile,
            a.gagphone,
            a.gagfax,
            a.gagophone
         FROM ticket_channel AS c
         JOIN ticket_agent AS a ON c.chid = a.chid'
         . $where . ' ORDER BY a.agid ASC';
    $q  = $db->do_query( $s );
    $dt = array();

    if ( $db->num_rows( $q ) > 0 )
    {
        while ( $d = $db->fetch_array( $q ) )
        {
            if( $d['agtype'] == 'Net Rate' )
            {
                $r3 = $db->do_query( 'SELECT agvdateto FROM ticket_agent_net_rate WHERE agid = ' . $d['agid'] . ' ORDER BY agnid DESC LIMIT 1' );

                if( is_array( $r3 ) )
                {
                    $agvalidto = '-';
                }
                else
                {
                    $d3 = $db->fetch_array( $r3 );

                    $agvalidto = date( 'd F Y', strtotime( $d3['agvdateto'] ) );
                }
            }
            elseif( $d['agtype'] == 'Freelance Code' )
            {
                $r3 = $db->do_query( 'SELECT agfneverend, agvdateto FROM ticket_agent_freelance_code WHERE agid = ' . $d['agid'] . ' ORDER BY agfid DESC LIMIT 1' );

                if( is_array( $r3 ) )
                {
                    $agvalidto = '-';
                }
                else
                {
                    $d3 = $db->fetch_array( $r3 );

                    if( $d3['agfneverend'] == '0' )
                    {
                        $agvalidto = date( 'd F Y', strtotime( $d3['agfvdateto'] ) );
                    }
                    else
                    {
                        $agvalidto = '&infin;';
                    }
                }
            }
            else
            {
                $agvalidto = '-';
            }

            $dt[] = array(
                'agent'     => $d['agname'],
                'chn'       => $d['chcode'],
                'channel'   => $d['chname'],
                'cp'        => $d['agcp'],
                'position'  => $d['agposition'],
                'email'     => $d['agemail'],
                'mobile'    => $d['agmobile'],
                'phone'     => $d['agphone'],
                'ophone'    => $d['agophone'],
                'fax'       => $d['agfax'],
                'address'   => $d['agaddress'],
                'ccp'       => $d['cagcp'],
                'cposition' => $d['cagposition'],
                'cemail'    => $d['cagemail'],
                'cmobile'   => $d['cagmobile'],
                'cphone'    => $d['cagphone'],
                'cophone'   => $d['cagophone'],
                'cfax'      => $d['cagfax'],
                'caddress'  => $d['cagaddress'],
                'icp'       => $d['iagcp'],
                'iposition' => $d['iagposition'],
                'iemail'    => $d['iagemail'],
                'imobile'   => $d['iagmobile'],
                'iphone'    => $d['iagphone'],
                'iophone'   => $d['iagophone'],
                'ifax'      => $d['iagfax'],
                'iaddress'  => $d['iagaddress'],
                'gcp'       => $d['gagcp'],
                'gposition' => $d['gagposition'],
                'gemail'    => $d['gagemail'],
                'gmobile'   => $d['gagmobile'],
                'gphone'    => $d['gagphone'],
                'gophone'   => $d['gagophone'],
                'gfax'      => $d['gagfax'],
                'gaddress'  => $d['gagaddress'],
                'sp'        => $d['agsp'],
                'payment'   => $d['agpayment_type'],
                'cod'       => $d['agcod'],
                'rvalid'    => $agvalidto,
                'status'    => $d['agstatus'] == '0' ? 'Active' : 'Suspended'
            );
        }
    }

    return $dt;
}

/*
| -------------------------------------------------------------------------------------
| Get Agent By ID
| -------------------------------------------------------------------------------------
*/
function get_agent( $id = '', $field = '' )
{
    global $db;

    $data = array(
        'agid'           => ( isset( $_POST['agid'] ) ? $_POST['agid'] : null ),
        'chid'           => ( isset( $_POST['chid'] ) ? $_POST['chid'] : '' ),
        'agtype'         => ( isset( $_POST['agtype'] ) ? $_POST['agtype'] : 'Net Rate' ),
        'agname'         => ( isset( $_POST['agname'] ) ? $_POST['agname'] : '' ),
        'agaddress'      => ( isset( $_POST['agaddress'] ) ? $_POST['agaddress'] : '' ),
        'agemail'        => ( isset( $_POST['agemail'] ) ? $_POST['agemail'] : '' ),
        'agcp'           => ( isset( $_POST['agcp'] ) ? $_POST['agcp'] : '' ),
        'agposition'     => ( isset( $_POST['agposition'] ) ? $_POST['agposition'] : '' ),
        'agsp'           => ( isset( $_POST['agsp'] ) ? $_POST['agsp'] : '' ),
        'agtopup'        => ( isset( $_POST['agtopup'] ) ? $_POST['agtopup'] : '' ),
        'agtopupval'     => ( isset( $_POST['agtopupval'] ) ? $_POST['agtopupval'] : 0 ),
        'agmobile'       => ( isset( $_POST['agmobile'] ) ? $_POST['agmobile'] : '' ),
        'agphone'        => ( isset( $_POST['agphone'] ) ? $_POST['agphone'] : '' ),
        'agfax'          => ( isset( $_POST['agfax'] ) ? $_POST['agfax'] : '' ),
        'agophone'       => ( isset( $_POST['agophone'] ) ? $_POST['agophone'] : '' ),
        'agusername'     => ( isset( $_POST['agusername'] ) ? $_POST['agusername'] : '' ),
        'agpassword'     => ( isset( $_POST['agpassword'] ) ? $_POST['agpassword'] : '' ),
        'agstatus'       => ( isset( $_POST['agstatus'] ) ? $_POST['agstatus'] : '' ),
        'agcod'          => ( isset( $_POST['agcod'] ) ? $_POST['agcod'] : '' ),
        'agreateddate'   => ( isset( $_POST['agreateddate'] ) ? $_POST['agreateddate'] : '' ),
        'agpercentage'   => ( isset( $_POST['agpercentage'] ) ? $_POST['agpercentage'] : '' ),
        'luser_id'       => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' ),
        'cagaddress'     => ( isset( $_POST['cagaddress'] ) ? $_POST['cagaddress'] : '' ),
        'cagemail'       => ( isset( $_POST['cagemail'] ) ? $_POST['cagemail'] : '' ),
        'cagcp'          => ( isset( $_POST['cagcp'] ) ? $_POST['cagcp'] : '' ),
        'cagposition'    => ( isset( $_POST['cagposition'] ) ? $_POST['cagposition'] : '' ),
        'cagmobile'      => ( isset( $_POST['cagmobile'] ) ? $_POST['cagmobile'] : '' ),
        'cagphone'       => ( isset( $_POST['cagphone'] ) ? $_POST['cagphone'] : '' ),
        'cagfax'         => ( isset( $_POST['cagfax'] ) ? $_POST['cagfax'] : '' ),
        'cagophone'      => ( isset( $_POST['cagophone'] ) ? $_POST['cagophone'] : '' ),
        'iagaddress'     => ( isset( $_POST['iagaddress'] ) ? $_POST['iagaddress'] : '' ),
        'iagemail'       => ( isset( $_POST['iagemail'] ) ? $_POST['iagemail'] : '' ),
        'iagcp'          => ( isset( $_POST['iagcp'] ) ? $_POST['iagcp'] : '' ),
        'iagposition'    => ( isset( $_POST['iagposition'] ) ? $_POST['iagposition'] : '' ),
        'iagmobile'      => ( isset( $_POST['iagmobile'] ) ? $_POST['iagmobile'] : '' ),
        'iagphone'       => ( isset( $_POST['iagphone'] ) ? $_POST['iagphone'] : '' ),
        'iagfax'         => ( isset( $_POST['iagfax'] ) ? $_POST['iagfax'] : '' ),
        'iagophone'      => ( isset( $_POST['iagophone'] ) ? $_POST['iagophone'] : '' ),
        'gagaddress'     => ( isset( $_POST['gagaddress'] ) ? $_POST['gagaddress'] : '' ),
        'gagemail'       => ( isset( $_POST['gagemail'] ) ? $_POST['gagemail'] : '' ),
        'gagcp'          => ( isset( $_POST['gagcp'] ) ? $_POST['gagcp'] : '' ),
        'gagposition'    => ( isset( $_POST['gagposition'] ) ? $_POST['gagposition'] : '' ),
        'gagmobile'      => ( isset( $_POST['gagmobile'] ) ? $_POST['gagmobile'] : '' ),
        'gagphone'       => ( isset( $_POST['gagphone'] ) ? $_POST['gagphone'] : '' ),
        'gagfax'         => ( isset( $_POST['gagfax'] ) ? $_POST['gagfax'] : '' ),
        'gagophone'      => ( isset( $_POST['gagophone'] ) ? $_POST['gagophone'] : '' ),
        'agparent'       => ( isset( $_POST['agparent'] ) ? $_POST['agparent'] : ( isset( $_COOKIE['agid'] ) ? $_COOKIE['agid'] : 0 ) ),
        'agpayment_type' => ( isset( $_POST['agpayment_type'] ) ? $_POST['agpayment_type'] : '' ),
        'agnetrate'      => get_agent_net_rate( isset( $_POST['agid'] ) ? $_POST['agid'] : null ),
        'agcontract'     => get_agent_contract( isset( $_POST['agid'] ) ? $_POST['agid'] : null ),
        'agfreelance'    => get_agent_freelance_code( isset( $_POST['agid'] ) ? $_POST['agid'] : null )
    );

    $s = 'SELECT * FROM ticket_agent WHERE agid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'agid'           => $d['agid'],
                'chid'           => $d['chid'],
                'agtype'         => $d['agtype'],
                'agname'         => $d['agname'],
                'agaddress'      => $d['agaddress'],
                'agemail'        => $d['agemail'],
                'agcp'           => $d['agcp'],
                'agposition'     => $d['agposition'],
                'agsp'           => $d['agsp'],
                'agtopup'        => $d['agtopup'],
                'agtopupval'     => $d['agtopupval'],
                'agmobile'       => $d['agmobile'],
                'agphone'        => $d['agphone'],
                'agfax'          => $d['agfax'],
                'agophone'       => $d['agophone'],
                'agusername'     => $d['agusername'],
                'agpassword'     => $d['agpassword'],
                'agstatus'       => $d['agstatus'],
                'agcod'          => $d['agcod'],
                'agreateddate'   => $d['agreateddate'],
                'agpercentage'   => $d['agpercentage'],
                'luser_id'       => $d['luser_id'],
                'cagaddress'     => $d['cagaddress'],
                'cagemail'       => $d['cagemail'],
                'cagcp'          => $d['cagcp'],
                'cagposition'    => $d['cagposition'],
                'cagmobile'      => $d['cagmobile'],
                'cagphone'       => $d['cagphone'],
                'cagfax'         => $d['cagfax'],
                'cagophone'      => $d['cagophone'],
                'iagaddress'     => $d['iagaddress'],
                'iagemail'       => $d['iagemail'],
                'iagcp'          => $d['iagcp'],
                'iagposition'    => $d['iagposition'],
                'iagmobile'      => $d['iagmobile'],
                'iagphone'       => $d['iagphone'],
                'iagfax'         => $d['iagfax'],
                'iagophone'      => $d['iagophone'],
                'gagaddress'     => $d['gagaddress'],
                'gagemail'       => $d['gagemail'],
                'gagcp'          => $d['gagcp'],
                'gagposition'    => $d['gagposition'],
                'gagmobile'      => $d['gagmobile'],
                'gagphone'       => $d['gagphone'],
                'gagfax'         => $d['gagfax'],
                'gagophone'      => $d['gagophone'],
                'agparent'       => $d['agparent'],
                'agpayment_type' => $d['agpayment_type'],
                'agnetrate'      => get_agent_net_rate( $d['agid'] ),
                'agcontract'     => get_agent_contract( $d['agid'] ),
                'agfreelance'    => get_agent_freelance_code( $d['agid'] )
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}
/*
| -------------------------------------------------------------------------------------
| Get Agent Percentage Rate
| -------------------------------------------------------------------------------------
*/
function get_agent_percentage( $id = '' )
{
    global $db;

    if( empty( $id ) && isset( $_POST['agpercentage'] ) )
    {
        return $_POST['agpercentage'];
    }

    $s = 'SELECT agpercentage FROM ticket_agent WHERE agid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $percentage_rate = json_decode( $d['agpercentage'], true );

        if( $percentage_rate !== null && json_last_error() === JSON_ERROR_NONE )
        {
            $total_percentage_rate = 100 - $percentage_rate;
            return $total_percentage_rate;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Contract File By ID
| -------------------------------------------------------------------------------------
*/
function get_agent_contract( $id = '' )
{
    global $db;

    if( empty( $id ) && isset( $_POST['agcontract'] ) )
    {
        return $_POST['agcontract'];
    }

    $s = 'SELECT agcontract FROM ticket_agent WHERE agid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $files = json_decode( $d['agcontract'], true );

        if( $files !== null && json_last_error() === JSON_ERROR_NONE )
        {
            return $files;
        }
    }
}

function set_agent_contract( $contracts )
{
    if( !empty( $contracts ) )
    {
        $site_url = site_url();
        $content  = '
        <div class="contract-file-wrapp clearfix">
            <div class="contract-file-list clearfix">';

                foreach( $contracts as $i => $contract )
                {
                    if( file_exists( PLUGINS_PATH . '/ticket/uploads/contract/' . $contract['filename'] ) && !empty( $contract['filename'] ) )
                    {
                        $content .= '
                        <div class="inner">
                            <div class="form-group">
                                <label>Contract Type</label>
                                <select class="select-option" name="agcontract[' . $i . '][type]" autocomplete="off">
                                    ' . get_agent_contract_type_option( $contract['type'] ) . '
                                </select>
                                <figure class="contract-file">
                                    <img src="../l-plugins/ticket/images/contract.svg" alt="Contract">
                                </figure>
                                <div class="link-action">
                                    <a class="view-contract" href="' . HTSERVER . $site_url . '/l-plugins/ticket/uploads/contract/' . $contract['filename'] . '" target="_blank">View Contract</a>
                                    <a class="delete-contract" data-index="' . $i . '" data-filename="' . $contract['filename'] . '">Delete Contract</a>
                                    <input type="text" class="sr-only" name="agcontract[' . $i . '][filename]" value="' . $contract['filename'] . '" autocomplete="off">
                                </div>
                            </div>
                        </div>';
                    }
                }

                $content .= '
            </div>
            <div class="contract-action">
                <div class="manage-contract">
                    <p>Add Contract</p>
                    <input type="file" name="contract_file" />
                </div>
            </div>
        </div>';

        return $content;
    }
    else
    {
        $content  = '
        <div class="contract-file-wrapp clearfix">
            <div class="contract-file-list clearfix"></div>
            <div class="contract-action">
                <div class="manage-contract">
                    <p>Add Contract</p>
                    <input type="file" name="contract_file" />
                </div>
            </div>
        </div>';

        return $content;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Net Rate By ID
| -------------------------------------------------------------------------------------
*/
function get_agent_net_rate( $id, $lcid = 0, $lcid_to = 0 )
{
    global $db;

    if( empty( $lcid ) )
    {
        $s = 'SELECT * FROM ticket_agent_net_rate WHERE agid = %d';
        $q = $db->prepare_query( $s, $id );
        $r = $db->do_query( $q );
    }
    else
    {
        if( empty( $lcid_to ) )
        {
            $s = 'SELECT * FROM ticket_agent_net_rate WHERE agid = %d AND lcid = %d';
            $q = $db->prepare_query( $s, $id, $lcid );
            $r = $db->do_query( $q );
        }
        else
        {
            $s = 'SELECT * FROM ticket_agent_net_rate WHERE agid = %d AND lcid = %d AND lcid_to = %d';
            $q = $db->prepare_query( $s, $id, $lcid, $lcid_to );
            $r = $db->do_query( $q );
        }
    }

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        $agratevalid = get_agrate_validity( $id );
        $agnetprice  = array();
        $agvdatefrom = '';
        $agvdateto   = '';

        while( $d = $db->fetch_array( $r ) )
        {
            $index       = $d['lcid'] . '|' . ( empty( $d['lcid_to'] ) ? 0 : $d['lcid_to'] );
            $agvdatefrom = date( 'd F Y', strtotime( $d['agvdatefrom'] ) );
            $agvdateto   = date( 'd F Y', strtotime( $d['agvdateto'] ) );

            $agnetprice[ $agvdatefrom . '|' . $agvdateto ][ $index ] = array(
                'adult_rate_inc_trans' => $d['adult_rate_inc_trans'],
                'child_rate_inc_trans' => $d['child_rate_inc_trans'],
                'infant_rate_inc_trans' => $d['infant_rate_inc_trans'],
                'adult_rate_exc_trans' => $d['adult_rate_exc_trans'],
                'child_rate_exc_trans' => $d['child_rate_exc_trans'],
                'infant_rate_exc_trans' => $d['infant_rate_exc_trans']
            );
        }

        $data[] = array(
            'agid' => $id,
            'agvdatefrom' => $agvdatefrom,
            'agvdateto' => $agvdateto,
            'agdatevalidity' => $agratevalid,
            'agnetprice' => $agnetprice
        );
    }
    else
    {
        $index = $lcid . '|' . $lcid_to;

        $data[] = array(
            'agid' => $id,
            'agvdatefrom' => '',
            'agvdateto' =>  '',
            'agdatevalidity' => array(),
            'agnetprice' => array(
                $index => array(
                    'adult_rate_inc_trans' => 0,
                    'child_rate_inc_trans' => 0,
                    'infant_rate_inc_trans' => 0,
                    'adult_rate_exc_trans' => 0,
                    'child_rate_exc_trans' => 0,
                    'infant_rate_exc_trans' => 0
                )
            )
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Freelance Code By ID
| -------------------------------------------------------------------------------------
*/
function get_agent_freelance_code( $id )
{
    global $db;

    $s = 'SELECT * FROM ticket_agent_freelance_code WHERE agid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );;

        $data[] = array(
            'agid' => $d['agid'],
            'agfvdatefrom' => date( 'd F Y', strtotime( $d['agfvdatefrom'] ) ),
            'agfvdateto' =>  date( 'd F Y', strtotime( $d['agfvdateto'] ) ),
            'agfneverend' => $d['agfneverend'],
            'agfcommission' => $d['agfcommission'],
            'agfcommission_type' => $d['agfcommission_type'],
            'agfreelance_code' => $d['agfreelance_code']
        );
    }
    else
    {
        $data[] = array(
            'agid' => $id,
            'agfvdatefrom' => '',
            'agfvdateto' =>  '',
            'agfneverend' => '0',
            'agfcommission' => 0,
            'agfcommission_type' => 0,
            'agfreelance_code' => ''
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Agent List
| -------------------------------------------------------------------------------------
*/
function get_agent_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_agent';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'agid' => $d['agid'],
            'chid' => $d['chid'],
            'agtype' => $d['agtype'],
            'agname' => $d['agname'],
            'agaddress' => $d['agaddress'],
            'agemail' => $d['agemail'],
            'agcp' => $d['agcp'],
            'agsp' => $d['agsp'],
            'agtopup' => $d['agtopup'],
            'agtopupval' => $d['agtopupval'],
            'agmobile' => $d['agmobile'],
            'agphone' => $d['agphone'],
            'agfax' => $d['agfax'],
            'agophone' => $d['agophone'],
            'agcontract' => $d['agcontract'],
            'agusername' => $d['agusername'],
            'agpassword' => $d['agpassword'],
            'agstatus' => $d['agstatus'],
            'agreateddate' => $d['agreateddate'],
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

function get_sub_agent_list( $parent = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_agent WHERE agparent = %d';
    $q = $db->prepare_query( $s, $parent );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'agid' => $d['agid'],
            'chid' => $d['chid'],
            'agtype' => $d['agtype'],
            'agname' => $d['agname'],
            'agaddress' => $d['agaddress'],
            'agemail' => $d['agemail'],
            'agcp' => $d['agcp'],
            'agsp' => $d['agsp'],
            'agtopup' => $d['agtopup'],
            'agtopupval' => $d['agtopupval'],
            'agmobile' => $d['agmobile'],
            'agphone' => $d['agphone'],
            'agfax' => $d['agfax'],
            'agophone' => $d['agophone'],
            'agcontract' => $d['agcontract'],
            'agusername' => $d['agusername'],
            'agpassword' => $d['agpassword'],
            'agstatus' => $d['agstatus'],
            'agreateddate' => $d['agreateddate'],
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Agent
| -------------------------------------------------------------------------------------
*/
function save_agent()
{
    global $db;

    $contract_file = isset( $_POST['agcontract'] ) && !empty( $_POST['agcontract'] ) ? json_encode( $_POST['agcontract'] ) : '';

    $s = 'INSERT INTO ticket_agent(
            chid,
            agtype,
            agname,
            agaddress,
            agemail,
            agcp,
            agposition,
            agsp,
            agtopup,
            agtopupval,
            agmobile,
            agphone,
            agfax,
            agophone,
            agcontract,
            agpayment_type,
            agusername,
            agpassword,
            agstatus,
            agcod,
            agreateddate,
            cagaddress,
            cagemail,
            cagcp,
            cagposition,
            cagmobile,
            cagphone,
            cagfax,
            cagophone,
            iagaddress,
            iagemail,
            iagcp,
            iagposition,
            iagmobile,
            iagphone,
            iagfax,
            iagophone,
            gagaddress,
            gagemail,
            gagcp,
            gagposition,
            gagmobile,
            gagphone,
            gagfax,
            gagophone,
            agpercentage,
            luser_id ) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s,
            $_POST['chid'],
            $_POST['agtype'],
            $_POST['agname'],
            $_POST['agaddress'],
            $_POST['agemail'],
            $_POST['agcp'],
            $_POST['agposition'],
            $_POST['agsp'],
            $_POST['agtopup'],
            $_POST['agtopupval'],
            $_POST['agmobile'],
            $_POST['agphone'],
            $_POST['agfax'],
            $_POST['agophone'],
            $contract_file,
            $_POST['agpayment_type'],
            $_POST['agusername'],
            md5( $_POST['agpassword'] ),
            $_POST['agstatus'],
            $_POST['agcod'],
            date( 'Y-m-d H:i:s' ),
            $_POST['cagaddress'],
            $_POST['cagemail'],
            $_POST['cagcp'],
            $_POST['cagposition'],
            $_POST['cagmobile'],
            $_POST['cagphone'],
            $_POST['cagfax'],
            $_POST['cagophone'],
            $_POST['iagaddress'],
            $_POST['iagemail'],
            $_POST['iagcp'],
            $_POST['iagposition'],
            $_POST['iagmobile'],
            $_POST['iagphone'],
            $_POST['iagfax'],
            $_POST['iagophone'],
            $_POST['gagaddress'],
            $_POST['gagemail'],
            $_POST['gagcp'],
            $_POST['gagposition'],
            $_POST['gagmobile'],
            $_POST['gagphone'],
            $_POST['gagfax'],
            $_POST['gagophone'],
            $_POST['agpercentage'],
            $_COOKIE['user_id'] );
    $r = $db->do_query( $q );
    
    if( !is_array( $r ) )
    {
        $id = $db->insert_id();

        update_agent_net_rate( $id );
        update_agent_freelance_code( $id );
        
        save_log( $id, 'agent', 'Add new agent - ' . $_POST['agname'] );

        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Agent
| -------------------------------------------------------------------------------------
*/
function update_agent()
{
    global $db;

    $curr_agent    = get_agent( $_POST['agid'] );
    $contract_file = isset( $_POST['agcontract'] ) && !empty( $_POST['agcontract'] ) ? json_encode( $_POST['agcontract'] ) : '';

    if( isset( $_POST['agpassword'] ) && !empty( $_POST['agpassword'] ) )
    {
        $s = 'UPDATE ticket_agent SET
                agid = %d,
                chid = %d,
                agtype = %s,
                agname = %s,
                agaddress = %s,
                agemail = %s,
                agcp = %s,
                agposition = %s,
                agsp = %s,
                agtopup = %s,
                agtopupval = %d,
                agmobile = %s,
                agphone = %s,
                agfax = %s,
                agophone = %s,
                agcontract = %s,
                agpayment_type = %s,
                agusername = %s,
                agpassword = %s,
                agstatus = %s,
                agcod = %d,
                cagaddress = %s,
                cagemail = %s,
                cagcp = %s,
                cagposition = %s,
                cagmobile = %s,
                cagphone = %s,
                cagfax = %s,
                cagophone = %s,
                iagaddress = %s,
                iagemail = %s,
                iagcp = %s,
                iagposition = %s,
                iagmobile = %s,
                iagphone = %s,
                iagfax = %s,
                iagophone = %s,
                gagaddress = %s,
                gagemail = %s,
                gagcp = %s,
                gagposition = %s,
                gagmobile = %s,
                gagphone = %s,
                gagfax = %s,
                gagophone = %s
              WHERE agid = %d';
        $q = $db->prepare_query( $s,
                $_POST['agid'],
                $_POST['chid'],
                $_POST['agtype'],
                $_POST['agname'],
                $_POST['agaddress'],
                $_POST['agemail'],
                $_POST['agcp'],
                $_POST['agposition'],
                $_POST['agsp'],
                $_POST['agtopup'],
                $_POST['agtopupval'],
                $_POST['agmobile'],
                $_POST['agphone'],
                $_POST['agfax'],
                $_POST['agophone'],
                $contract_file,
                $_POST['agpayment_type'],
                $_POST['agusername'],
                md5( $_POST['agpassword'] ),
                $_POST['agstatus'],
                $_POST['agcod'],
                $_POST['cagaddress'],
                $_POST['cagemail'],
                $_POST['cagcp'],
                $_POST['cagposition'],
                $_POST['cagmobile'],
                $_POST['cagphone'],
                $_POST['cagfax'],
                $_POST['cagophone'],
                $_POST['iagaddress'],
                $_POST['iagemail'],
                $_POST['iagcp'],
                $_POST['iagposition'],
                $_POST['iagmobile'],
                $_POST['iagphone'],
                $_POST['iagfax'],
                $_POST['iagophone'],
                $_POST['gagaddress'],
                $_POST['gagemail'],
                $_POST['gagcp'],
                $_POST['gagposition'],
                $_POST['gagmobile'],
                $_POST['gagphone'],
                $_POST['gagfax'],
                $_POST['gagophone'],
                $_POST['agid'] );
    }
    else
    {
        $s = 'UPDATE ticket_agent SET
                agid = %d,
                chid = %d,
                agtype = %s,
                agname = %s,
                agaddress = %s,
                agemail = %s,
                agcp = %s,
                agposition = %s,
                agsp = %s,
                agtopup = %s,
                agtopupval = %d,
                agmobile = %s,
                agphone = %s,
                agfax = %s,
                agophone = %s,
                agcontract = %s,
                agpayment_type = %s,
                agusername = %s,
                agstatus = %s,
                agcod = %d,
                cagaddress = %s,
                cagemail = %s,
                cagcp = %s,
                cagposition = %s,
                cagmobile = %s,
                cagphone = %s,
                cagfax = %s,
                cagophone = %s,
                iagaddress = %s,
                iagemail = %s,
                iagcp = %s,
                iagposition = %s,
                iagmobile = %s,
                iagphone = %s,
                iagfax = %s,
                iagophone = %s,
                gagaddress = %s,
                gagemail = %s,
                gagcp = %s,
                gagposition = %s,
                gagmobile = %s,
                gagphone = %s,
                gagfax = %s,
                gagophone = %s,
                agpercentage = %s
              WHERE agid = %d';
        $q = $db->prepare_query( $s,
                $_POST['agid'],
                $_POST['chid'],
                $_POST['agtype'],
                $_POST['agname'],
                $_POST['agaddress'],
                $_POST['agemail'],
                $_POST['agcp'],
                $_POST['agposition'],
                $_POST['agsp'],
                $_POST['agtopup'],
                $_POST['agtopupval'],
                $_POST['agmobile'],
                $_POST['agphone'],
                $_POST['agfax'],
                $_POST['agophone'],
                $contract_file,
                $_POST['agpayment_type'],
                $_POST['agusername'],
                $_POST['agstatus'],
                $_POST['agcod'],
                $_POST['cagaddress'],
                $_POST['cagemail'],
                $_POST['cagcp'],
                $_POST['cagposition'],
                $_POST['cagmobile'],
                $_POST['cagphone'],
                $_POST['cagfax'],
                $_POST['cagophone'],
                $_POST['iagaddress'],
                $_POST['iagemail'],
                $_POST['iagcp'],
                $_POST['iagposition'],
                $_POST['iagmobile'],
                $_POST['iagphone'],
                $_POST['iagfax'],
                $_POST['iagophone'],
                $_POST['gagaddress'],
                $_POST['gagemail'],
                $_POST['gagcp'],
                $_POST['gagposition'],
                $_POST['gagmobile'],
                $_POST['gagphone'],
                $_POST['gagfax'],
                $_POST['gagophone'],
                $_POST['agpercentage'],
                $_POST['agid'] );
    }

    if( $db->do_query( $q ) )
    {
        update_agent_net_rate( $_POST['agid'] );
        update_agent_freelance_code( $_POST['agid'] );

        if( $curr_agent['agname'] != $_POST['agname'] )
        {
            save_log( $_POST['agid'], 'agent', 'Update agent - ' . $curr_agent['agname'] . ' to ' . $_POST['agname'] );
        }
        else
        {
            save_log( $_POST['agid'], 'agent', 'Update agent - ' . $_POST['agname'] );
        }

        return $_POST['agid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Reset Password Agent
| -------------------------------------------------------------------------------------
*/
function reset_password_agent( $id, $new_pwd )
{
    global $db;

    $d = get_agent( $id );

    $s = 'UPDATE ticket_agent SET agpassword = %s WHERE agid = %d';
    $q = $db->prepare_query( $s, md5( $new_pwd ), $id );

    if( $db->do_query( $q ) )
    {
        save_log( $id, 'agent', 'Reset agent password - ' . $d['agname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Agent Net Rate
| -------------------------------------------------------------------------------------
*/
function update_agent_net_rate( $id )
{
    global $db;

    if( isset( $_POST['rate'] ) && $_POST['agtype'] == 'Net Rate' )
    {
        foreach( $_POST['agvdatefrom'] as $key => $date )
        {
            $agvdatefrom = date( 'Y-m-d', strtotime( $date ) );
            $agvdateto   = date( 'Y-m-d', strtotime( $_POST['agvdateto'][$key] ) );

            foreach( $_POST['rate'] as $point => $dt )
            {
                list( $lcid, $lcid_to ) = explode( '|', $point );

                $s = 'SELECT agnid FROM ticket_agent_net_rate WHERE agid = %d AND lcid = %d AND lcid_to = %d AND agvdatefrom = %s AND agvdateto = %s';
                $q = $db->prepare_query( $s, $id, $lcid, $lcid_to, $agvdatefrom, $agvdateto );
                $r = $db->do_query( $q );
                $d = $db->fetch_array( $r );

                if( $db->num_rows( $r ) > 0 )
                {
                    $s2 = 'UPDATE ticket_agent_net_rate SET
                             adult_rate_inc_trans = %s,
                             child_rate_inc_trans = %s,
                             infant_rate_inc_trans = %s,
                             adult_rate_exc_trans = %s,
                             child_rate_exc_trans = %s,
                             infant_rate_exc_trans = %s
                          WHERE agnid = %d';
                    $q2 = $db->prepare_query( $s2,
                             $dt['adult_rate_inc_trans'][$key],
                             $dt['child_rate_inc_trans'][$key],
                             $dt['infant_rate_inc_trans'][$key],
                             $dt['adult_rate_exc_trans'][$key],
                             $dt['child_rate_exc_trans'][$key],
                             $dt['infant_rate_exc_trans'][$key],
                             $d['agnid'] );
                    $r2 = $db->do_query( $q2 );
                }
                else
                {
                    $s2 = 'INSERT INTO ticket_agent_net_rate(
                             agid,
                             lcid,
                             lcid_to,
                             agvdatefrom,
                             agvdateto,
                             adult_rate_inc_trans,
                             child_rate_inc_trans,
                             infant_rate_inc_trans,
                             adult_rate_exc_trans,
                             child_rate_exc_trans,
                             infant_rate_exc_trans ) VALUES ( %d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s )';
                    $q2 = $db->prepare_query( $s2,
                             $id,
                             $lcid,
                             $lcid_to,
                             $agvdatefrom,
                             $agvdateto,
                             $dt['adult_rate_inc_trans'][$key],
                             $dt['child_rate_inc_trans'][$key],
                             $dt['infant_rate_inc_trans'][$key],
                             $dt['adult_rate_exc_trans'][$key],
                             $dt['child_rate_exc_trans'][$key],
                             $dt['infant_rate_exc_trans'][$key] );
                    $r2 = $db->do_query( $q2 );
                }
            }
        }
    }
}

function update_agent_freelance_code( $id )
{
    global $db;

    if( $_POST['agtype'] == 'Freelance Code' )
    {
        $s = 'SELECT agfid FROM ticket_agent_freelance_code WHERE agid = %d';
        $q = $db->prepare_query( $s, $id );
        $r = $db->do_query( $q );
        $d = $db->fetch_array( $r );

        $neverend = isset( $_POST['agfneverend'] ) ? $_POST['agfneverend'] : '0';

        if( $db->num_rows( $r ) > 0 )
        {
            $s2 = 'UPDATE ticket_agent_freelance_code SET
                     agid = %s,
                     agfvdatefrom = %s,
                     agfvdateto = %s,
                     agfneverend = %s,
                     agfcommission = %s,
                     agfcommission_type = %s,
                     agfreelance_code = %s
                  WHERE agfid = %d';
            $q2 = $db->prepare_query( $s2,
                     $id,
                     date( 'Y-m-d', strtotime( $_POST['agfvdatefrom'] ) ),
                     date( 'Y-m-d', strtotime( $_POST['agfvdateto'] ) ),
                     $neverend,
                     $_POST['agfcommission'],
                     $_POST['agfcommission_type'],
                     $_POST['agfreelance_code'],
                     $d['agfid'] );
            $r2 = $db->do_query( $q2 );
        }
        else
        {
            $s2 = 'INSERT INTO ticket_agent_freelance_code(
                     agid,
                     agfvdatefrom,
                     agfvdateto,
                     agfneverend,
                     agfcommission,
                     agfcommission_type,
                     agfreelance_code) VALUES ( %d, %s, %s, %s, %s, %s, %s)';
            $q2 = $db->prepare_query( $s2,
                     $id,
                     date( 'Y-m-d', strtotime( $_POST['agfvdatefrom'] ) ),
                     date( 'Y-m-d', strtotime( $_POST['agfvdateto'] ) ),
                     $neverend,
                     $_POST['agfcommission'],
                     $_POST['agfcommission_type'],
                     $_POST['agfreelance_code'] );
            $r2 = $db->do_query( $q2 );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Agent
| -------------------------------------------------------------------------------------
*/
function delete_agent( $id='' )
{
    global $db;

    $files = get_agent_contract( $id );
    $data  = get_agent( $id );

    $s = 'DELETE FROM ticket_agent WHERE agid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( !isset( $r['error_code'] ) )
    {
        if( !empty( $files ) )
        {
            foreach( $files as $key => $d )
            {
                if( file_exists( PLUGINS_PATH . '/ticket/uploads/contract/' . $d['filename'] ) && !empty( $d['filename'] ) )
                {
                    unlink( PLUGINS_PATH . '/ticket/uploads/contract/' . $d['filename'] );
                }
            }
        }

        save_log( $id, 'agent', 'Delete agent - ' . $data['agname'] );

        return true;
    }
}

function is_filter_agent()
{
    if( isset( $_POST['filter'] ) )
    {
        if( $_POST['chid'] == '' )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}

function ticket_filter_agent()
{
    $filter = array( 'chid' => '', 'agsp' => '' );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array(
            'chid' => isset( $chid ) ? $chid : '',
            'agsp' => isset( $agsp ) ? $agsp : ''
        );
    }

    return $filter;
}

/*
| -------------------------------------------------------------------------------------
| Get Net Rate
| -------------------------------------------------------------------------------------
*/
function get_net_rate_list( $id )
{
    $loc  = get_location_list();

    if( !empty( $loc ) )
    {
        $list   = '';
        $depart = array();
        $destin = array();
        $rate   = get_agent_net_rate( $id );

        foreach( $loc as $key => $d )
        {
            if( $d['lctype'] == '0' )
            {
                $depart[] = $d;
            }
            elseif( $d['lctype'] == '1' )
            {
                $destin[] = $d;
            }
            elseif( $d['lctype'] == '2' )
            {
                $depart[] = $d;
                $destin[] = $d;
            }
        }

        $num = 0;

        foreach( $rate[0]['agnetprice'] as $period => $obj )
        {
            list( $dtfrom, $dtto ) = explode( '|', $period );

            $list .= '
            <div class="net-price-block" data="' . $period . '">
                <h3>Period : ' . $dtfrom . ' to ' . $dtto . '</h3>
                <select class="select-rate-category" style="width:100%;" autocomplete="off">
                    ' . get_rate_category_option() . '
                </select>
                <div class="table-net-rate-wrapp">
                    <table class="table-striped net-rate-list" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th rowspan="3">From/To</th>
                                <th rowspan="3">To/From</th>
                                <th colspan="6">Rate</th>
                            </tr>
                            <tr>
                                <th colspan="3">Include Transport</th>
                                <th colspan="3">Exclude Transport</th>
                            </tr>
                            <tr>
                                <th>Adult</th>
                                <th>Child</th>
                                <th>Infant</th>
                                <th>Adult</th>
                                <th>Child</th>
                                <th>Infant</th>
                            </tr>
                        </thead>
                        <tbody>';

                            foreach( $depart as $d )
                            {
                                foreach( $destin as $ds )
                                {
                                    if( $d['lcid'] == $ds['lcid'] )
                                    {
                                        continue;
                                    }

                                    $index = $d['lcid'] . '|' . $ds['lcid'];

                                    $adult_rate_inc_trans  = isset( $obj[ $index ]['adult_rate_inc_trans'] ) ? $obj[ $index ]['adult_rate_inc_trans'] : 0;
                                    $child_rate_inc_trans  = isset( $obj[ $index ]['child_rate_inc_trans'] ) ? $obj[ $index ]['child_rate_inc_trans'] : 0;
                                    $infant_rate_inc_trans = isset( $obj[ $index ]['infant_rate_inc_trans'] ) ? $obj[ $index ]['infant_rate_inc_trans'] : 0;

                                    $adult_rate_exc_trans  = isset( $obj[ $index ]['adult_rate_exc_trans'] ) ? $obj[ $index ]['adult_rate_exc_trans'] : 0;
                                    $child_rate_exc_trans  = isset( $obj[ $index ]['child_rate_exc_trans'] ) ? $obj[ $index ]['child_rate_exc_trans'] : 0;
                                    $infant_rate_exc_trans = isset( $obj[ $index ]['infant_rate_exc_trans'] ) ? $obj[ $index ]['infant_rate_exc_trans'] : 0;

                                    $list .= '
                                    <tr>
                                        <td>' . $d['lcname'] . '</td>
                                        <td>' . $ds['lcname'] . '</td>
                                        <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][adult_rate_inc_trans][]" value="' . $adult_rate_inc_trans . '" type="text"></td>
                                        <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][child_rate_inc_trans][]" value="' . $child_rate_inc_trans . '" type="text"></td>
                                        <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][infant_rate_inc_trans][]" value="' . $infant_rate_inc_trans . '" type="text"></td>
                                        <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][adult_rate_exc_trans][]" value="' . $adult_rate_exc_trans . '" type="text"></td>
                                        <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][child_rate_exc_trans][]" value="' . $child_rate_exc_trans . '" type="text"></td>
                                        <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][infant_rate_exc_trans][]" value="' . $infant_rate_exc_trans . '" type="text"></td>
                                    </tr>';
                                }
                            }

                            $list .= '
                        </tbody>
                    </table>
                </div>
            </div>';

            $num++;
        }
    }
    else
    {
        $list .= '<td colspan="8">Sorry no location was found</td>';
    }

    return $list;
}

/*
| -------------------------------------------------------------------------------------
| Get Detail Rate
| -------------------------------------------------------------------------------------
*/
function get_detail_rate_list( $detail )
{
    if( $detail['agtype'] == 'Net Rate' )
    {
        $loc  = get_location_list();
        $list = '';

        if( !empty( $loc ) )
        {
            $depart = array();
            $destin = array();
            $rate   = get_agent_net_rate( $detail['agid'] );

            foreach( $loc as $key => $d )
            {
                if( $d['lctype'] == '0' )
                {
                    $depart[] = $d;
                }
                elseif( $d['lctype'] == '1' )
                {
                    $destin[] = $d;
                }
                elseif( $d['lctype'] == '2' )
                {
                    $depart[] = $d;
                    $destin[] = $d;
                }
            }

            foreach( $depart as $d )
            {
                foreach( $destin as $ds )
                {
                    if( $d['lcid'] == $ds['lcid'] )
                    {
                        continue;
                    }

                    $num  = 0;
                    $nrow = count( $rate[0]['agnetprice'] ) * 2;

                    foreach( $rate[0]['agnetprice'] as $period => $obj )
                    {
                        list( $dtfrom, $dtto ) = explode( '|', $period );

                        $index = $d['lcid'] . '|' . $ds['lcid'];

                        $adult_rate_inc_trans  = isset( $obj[ $index ]['adult_rate_inc_trans'] ) ? $obj[ $index ]['adult_rate_inc_trans'] : 0;
                        $child_rate_inc_trans  = isset( $obj[ $index ]['child_rate_inc_trans'] ) ? $obj[ $index ]['child_rate_inc_trans'] : 0;
                        $infant_rate_inc_trans = isset( $obj[ $index ]['infant_rate_inc_trans'] ) ? $obj[ $index ]['infant_rate_inc_trans'] : 0;

                        $adult_rate_exc_trans  = isset( $obj[ $index ]['adult_rate_exc_trans'] ) ? $obj[ $index ]['adult_rate_exc_trans'] : 0;
                        $child_rate_exc_trans  = isset( $obj[ $index ]['child_rate_exc_trans'] ) ? $obj[ $index ]['child_rate_exc_trans'] : 0;
                        $infant_rate_exc_trans = isset( $obj[ $index ]['infant_rate_exc_trans'] ) ? $obj[ $index ]['infant_rate_exc_trans'] : 0;

                        if( $num == 0 )
                        {
                            $list .= '
                            <tr>
                                <td rowspan="' . $nrow .  '">' . $d['lcname'] . '</td>
                                <td rowspan="' . $nrow .  '">' . $ds['lcname'] . '</td>
                                <td colspan="4" style="background:#F5F5F5;">Period : ' . $dtfrom . ' to ' . $dtto . '</td>
                            </tr>
                            <tr>
                                <td>
                                    <p>' . $adult_rate_inc_trans . '</p>
                                    <p>' . $adult_rate_exc_trans . '</p>
                                </td>
                                <td>
                                    <p>' . $child_rate_inc_trans . '</p>
                                    <p>' . $child_rate_exc_trans . '</p>
                                </td>
                                <td>
                                    <p>' . $infant_rate_inc_trans . '</p>
                                    <p>' . $infant_rate_exc_trans . '</p>
                                </td>
                                <td>
                                    <p>Yes</p>
                                    <p>No</p>
                                </td>
                            </tr>';
                        }
                        else
                        {
                            $list .= '
                            <tr>
                                <td colspan="4" style="background:#F5F5F5;">Period : ' . $dtfrom . ' to ' . $dtto . '</td>
                            </tr>
                            <tr>
                                <td>
                                    <p>' . $adult_rate_inc_trans . '</p>
                                    <p>' . $adult_rate_exc_trans . '</p>
                                </td>
                                <td>
                                    <p>' . $child_rate_inc_trans . '</p>
                                    <p>' . $child_rate_exc_trans . '</p>
                                </td>
                                <td>
                                    <p>' . $infant_rate_inc_trans . '</p>
                                    <p>' . $infant_rate_exc_trans . '</p>
                                </td>
                                <td>
                                    <p>Yes</p>
                                    <p>No</p>
                                </td>
                            </tr>';
                        }

                        $num++;
                    }
                }
            }

            if( !empty( $list ) )
            {
                $result = '
                <table class="table">
                    <tr>
                        <th>From/To</th>
                        <th>To/From</th>
                        <th>Adult</th>
                        <th>Child</th>
                        <th>Infant</th>
                        <th>Trans.</th>
                    </tr>
                    ' . $list . '
                </table>
                <div class="item wt-border">
                    <div class="row">
                        <div class="col-md-12">
                            <b>Contract Type</b>';

                            if( empty( $detail['agcontract'] ) )
                            {
                                $result .= '<p>No contract file was uploaded</p>';
                            }
                            else
                            {
                                $site_url = site_url();

                                foreach( $detail['agcontract'] as $d )
                                {
                                    $result .= '
                                    <div class="form-group clearfix">
                                        <figure class="contract-file">
                                            <img src="../l-plugins/ticket/images/contract.svg" alt="Contract">
                                        </figure>
                                        <div class="contract-action">
                                            <p>' . $d['type'] . '</p>
                                            <a class="view-contract" href="' . HTSERVER . $site_url . '/l-plugins/ticket/uploads/contract/' . $d['filename'] . '" target="_blank">View Contract</a>
                                            <a class="edit-contract" href="' . get_state_url( 'booking-source&sub=agent&prc=edit&id=' . $detail['agid'] ) .'">Edit Contract</a>
                                        </div>
                                    </div>';
                                }
                            }

                            $result .= '
                        </div>
                    </div>
                </div>';

                return $result;
            }
        }
    }
    else
    {
        $result = '';

        foreach( $detail['agfreelance'] as $key => $d )
        {
            $result .= '
            <div class="item">
                <div class="row">
                    <div class="col-md-3">
                        <b>Code Validity</b>
                        <p>' . ( $d['agfneverend'] == '1' ? 'Unspecified' : $d['agfvdatefrom'] . ' - ' . $d['agfvdateto'] ) . '</p>
                    </div>
                    <div class="col-md-3">
                        <b>Commission from Publish Rate</b>
                        <p>' . ( $d['agfcommission_type'] == '1' ? 'Rp. ' . number_format( $d['agfcommission'], 0, ',', '.' ) : $d['agfcommission'] . '%' ) . '</p>
                    </div>
                    <div class="col-md-3">
                        <b>Freelance Code</b>
                        <p>' . $d['agfreelance_code'] . '</p>
                    </div>
                </div>
            </div>';
        }

        $result .= '
        <div class="item wt-border">
            <div class="row">
                <div class="col-md-12">
                    <b>Contract Type</b>';

                    if( empty( $detail['agcontract_file'] ) )
                    {
                        $result .= '<p>No contract file was uploaded</p>';
                    }
                    else
                    {
                        $site_url = site_url();

                        foreach( $detail['agcontract'] as $d )
                        {
                            $result .= '
                            <div class="form-group clearfix">
                                <figure class="contract-file">
                                    <img src="../l-plugins/ticket/images/contract.svg" alt="Contract">
                                </figure>
                                <div class="contract-action">
                                    <p>' . $d['type'] . '</p>
                                    <a class="view-contract" href="' . HTSERVER . $site_url . '/l-plugins/ticket/uploads/contract/' . $d['filename'] . '" target="_blank">View Contract</a>
                                    <a class="edit-contract" href="' . get_state_url( 'booking-source&sub=agent&prc=edit&id=' . $detail['agid'] ) .'">Edit Contract</a>
                                </div>
                            </div>';
                        }
                    }

                    $result .= '
                </div>
            </div>
        </div>';

        return $result;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Option
| -------------------------------------------------------------------------------------
*/
function get_agent_option( $agid = '', $use_empty = true, $empty_text = 'Select Agent' )
{
    global $db;

    $arr = json_decode( $agid, true );

    if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
    {
        $agid = $arr;
    }

    $s = 'SELECT * FROM ticket_agent WHERE agparent = 0';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    while( $d = $db->fetch_array( $r ) )
    {
        if( is_array( $agid ) )
        {
            $option .= '<option value="' . $d['agid'] . '" ' . ( in_array( $d['agid'], $agid ) ? 'selected' : '' ) . ' >' . $d['agname'] . '</option>';
        }
        else
        {
            $option .= '<option value="' . $d['agid'] . '" ' . ( $d['agid'] == $agid ? 'selected' : '' ) . ' >' . $d['agname'] . '</option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Sub Agent Option
| -------------------------------------------------------------------------------------
*/
function get_sub_agent_option( $parent = '', $agid = '', $use_empty = true, $empty_text = 'Select Sub Agent' )
{
    $sagent = get_sub_agent_list( $parent );
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $agent ) )
    {
        foreach( $agent as $d )
        {
            $option .= '<option value="' . $d['agid'] . '" ' . ( $d['agid'] == $agid ? 'selected' : '' ) . ' >' . $d['agname'] . '</option>';
        }
    }

    return $option;
}

function get_agent_sales_person_option( $agsp = '', $use_empty = true, $empty_text = 'Select Sales Person' )
{
    global $db;

    $s = 'SELECT DISTINCT( agsp ) FROM ticket_agent ORDER BY agsp';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    while( $d = $db->fetch_array( $r ) )
    {
        $option .= '<option value="' . $d['agsp'] . '" ' . ( $d['agsp'] == $agsp ? 'selected' : '' ) . ' >' . $d['agsp'] . '</option>';
    }

    return $option;
}

function get_agent_rate_type_option( $type = '' )
{
    return '
    <option value="Net Rate" ' . ( $type == 'Net Rate' ? 'selected' : '' ) . ' >Net Rate</option>
    <option value="Freelance Code" ' . ( $type == 'Freelance Code' ? 'selected' : '' ) . ' >Freelance Code</option>';
}

function get_agent_contract_type_option( $type = '' )
{
    return '
    <option value="Ad Hoc" ' . ( $type == 'Ad Hoc' ? 'selected' : '' ) . ' >Ad Hoc</option>
    <option value="LOA" ' . ( $type == 'LOA' ? 'selected' : '' ) . ' >LOA</option>';
}

function get_agent_payment_type_option( $type = '' )
{
    return '
    <option value="Pre Paid" ' . ( $type == 'Pre Paid' ? 'selected' : '' ) . ' >Pre Paid</option>
    <option value="Credit" ' . ( $type == 'Credit' ? 'selected' : '' ) . ' >Credit</option>';
}

function get_agent_status_option( $status = '' )
{
    return '
    <option value="0" ' . ( $status == '0' ? 'selected' : '' ) . ' >Active</option>
    <option value="1" ' . ( $status == '1' ? 'selected' : '' ) . ' >Suspended</option>';
}

function get_agent_commission_type_option( $type = '' )
{
    return '
    <option value="0" ' . ( $type == '0' ? 'selected' : '' ) . ' >%</option>
    <option value="1" ' . ( $type == '1' ? 'selected' : '' ) . ' >Rp</option>';
}

function get_agent_topup_option( $agtopup = '' )
{
    return '
    <option value="0" ' . ( $agtopup == '0' ? 'selected' : '' ) . ' >No</option>
    <option value="1" ' . ( $agtopup == '1' ? 'selected' : '' ) . ' >Yes</option>';
}

function get_agcod_option( $agcod = '' )
{
    $option = '';

    for( $i = 0; $i < 31; $i++ )
    {
        $option .= '<option value="' . $i . '" ' . ( $agcod == $i ? 'selected' : '' ) . ' >' . $i . ' Days</option>';
    }

    return $option;
}

function get_agent_status_text( $status = '' )
{
    return $status == '0' ? 'Active' : 'Suspended';
}

function upload_contract_file()
{
    global $db;

    if( isset( $_FILES['file_name'] ) && $_FILES['file_name']['error'] == 0 )
    {
        $file_name   = $_FILES['file_name']['name'];
        $file_size   = $_FILES['file_name']['size'];
        $file_type   = $_FILES['file_name']['type'];
        $file_source = $_FILES['file_name']['tmp_name'];

        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'pdf' ) )
            {
                $file_ext    = file_name_filter( $file_name, true );
                $sef_url     = file_name_filter( $file_name ) . '-'. time();
                $file_name_n = $sef_url . $file_ext;

                if( !file_exists( PLUGINS_PATH . '/ticket/uploads/contract/' ) )
                {
                    mkdir( PLUGINS_PATH . '/ticket/uploads/contract/');
                }

                if( upload( $file_source, PLUGINS_PATH . '/ticket/uploads/contract/' . $file_name_n ) )
                {
                    $index = uniqid();

                    if( !empty( $_POST['agid'] ) )
                    {
                        $contract   = get_agent_contract( $_POST['agid'] );
                        $contract   = empty( $contract ) ? array() : $contract;

                        $contract[ $index ] = array( 'type' => 'Ad Hoc', 'filename' => $file_name_n );

                        //-- Update New Contract
                        $s = 'UPDATE ticket_agent SET agcontract = %s WHERE agid = %d';
                        $q = $db->prepare_query( $s, json_encode( $contract ), $_POST['agid'] );
                        $r = $db->do_query( $q );

                        if( isset( $r['error_code'] ) )
                        {
                            return array( 'result'=>'failed', 'message'=>'Failed to add new contract to database' );
                        }
                        else
                        {
                            return array( 'result' => 'success', 'filename' => $file_name_n, 'fileloc'=>HTSERVER . site_url() . '/l-plugins/ticket/uploads/contract/' . $file_name_n, 'index' => $index );
                        }
                    }
                    else
                    {
                        return array( 'result' => 'success', 'filename' => $file_name_n, 'fileloc'=>HTSERVER . site_url() . '/l-plugins/ticket/uploads/contract/' . $file_name_n, 'index' => $index );
                    }
                }
                else
                {
                    return array( 'result'=>'failed', 'message'=>'Failed to add new contract' );
                }
            }
            else
            {
                return array( 'result'=>'error', 'message'=>'Failed to add new contract, File type must be PDF' );
            }
        }
        else
        {
            return array( 'result'=>'error', 'message'=>'Failed to add new contract, The maximum file size is 2MB' );
        }
    }
    else
    {
        return array( 'result'=>'error', 'message'=>'Failed to add new contract, No PDF file was uploaded' );
    }
}

function delete_contract_file()
{
    global $db;

    if( isset( $_POST['idx'] ) || isset( $_POST['file'] )  )
    {
        if( isset( $_POST['agid'] ) && !empty( $_POST['agid'] ) )
        {
            $contract = get_agent_contract( $_POST['agid'] );
            $contract = empty( $contract ) ? array() : $contract;

            if( isset( $contract[ $_POST['idx'] ] ) )
            {
                $file = $contract[ $_POST['idx'] ]['filename'];

                if( file_exists( PLUGINS_PATH . '/ticket/uploads/contract/' . $file ) && !empty( $file ) )
                {
                    unset( $contract[ $_POST['idx'] ] );

                    $contract = empty( $contract ) ? '' : json_encode( $contract );

                    //-- Update New Contract
                    $s = 'UPDATE ticket_agent SET agcontract = %s WHERE agid = %d';
                    $q = $db->prepare_query( $s, $contract, $_POST['agid'] );
                    $r = $db->do_query( $q );

                    if( isset( $r['error_code'] ) )
                    {
                        return array( 'result'=>'failed', 'message'=>'Failed to delete this contract from database' );
                    }
                    else
                    {
                        unlink( PLUGINS_PATH . '/ticket/uploads/contract/' . $file );

                        return array( 'result' => 'success' );
                    }
                }
                else
                {
                    return array( 'result'=>'failed', 'message'=>'Failed to delete this contract' );
                }
            }
            else
            {
                return array( 'result'=>'failed', 'message'=>'Failed to delete this contract' );
            }
        }
        else
        {
            if( file_exists( PLUGINS_PATH . '/ticket/uploads/contract/' . $_POST['file'] ) && !empty( $_POST['file'] ) )
            {
                unlink( PLUGINS_PATH . '/ticket/uploads/contract/' . $_POST['file'] );

                return array( 'result' => 'success' );
            }
            else
            {
                return array( 'result'=>'failed', 'message'=>'Failed to delete this contract' );
            }
        }
    }
    else
    {
        return array( 'result'=>'failed', 'message'=>'Some of parameter have empty value' );
    }
}

function send_login_access_data( $agid )
{
    $data = get_agent( $agid  );

    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $data['agnewpwd'] = random_string();

            if( reset_password_agent( $data['agid'], $data['agnewpwd'] ) )
            {
                $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

                $mail = new Mandrill( $keys );

                $email_to   = $data['agemail'];
                $web_title  = get_meta_data('web_title');
                $email_from = get_meta_data('smtp_email_address');
                $email_data = get_login_access_notification( $data );

                $message  = array(
                    'subject'    => 'Authentication Data For ' . $web_title . ' Admnin Panel',
                    'from_name'  => $web_title,
                    'from_email' => $email_from,
                    'html'       => $email_data,
                    'to' => array(
                        array(
                            'email' => $email_to,
                            'name'  => '',
                            'type'  => 'to'
                        )
                    ),
                    'headers' => array( 'Reply-To' => $email_from )
                );

                $async   = false;
                $result  = $mail->messages->send( $message, $async );

                if( isset( $result[0]['status'] ) )
                {
                    if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                    {
                        return array( 'result' => 'error' );
                    }
                    else
                    {
                        return array( 'result' => 'success' );
                    }
                }
                else
                {
                    return array( 'result' => 'error' );
                }
            }
        }
        catch( Mandrill_Error $e )
        {
            return array( 'result' => 'success', 'message' => $e->getMessage() );
        }
    }
}

function get_login_access_notification( $data )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/email/authentication-data-notification-to-agent.html', 'email' );

    add_block( 'email-block', 'e-block', 'email' );

    add_variable( 'agname', $data['agname'] );
    add_variable( 'agemail', $data['agemail'] );
    add_variable( 'agusername', $data['agusername'] );
    add_variable( 'agnewpwd', $data['agnewpwd'] );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'login_link', HTSERVER . site_url() . '/agent/login' );

    parse_template( 'email-block', 'e-block' );

    return return_template( 'email' );
}

function get_agrate_validity( $agid = '' )
{
    global $db;

    $s = 'SELECT a.agid, a.agvdatefrom, a.agvdateto
          FROM ticket_agent_net_rate AS a
          WHERE a.agid = %d GROUP BY a.agvdatefrom, a.agvdateto';
    $q = $db->prepare_query( $s, $agid );
    $r = $db->do_query( $q );

    $list = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $list[] = $d;
        }
    }

    return $list;
}

function delete_rate_validity( $agid = '', $agvdatefrom = '', $agvdateto = '' )
{
    global $db;

    $s = 'DELETE FROM ticket_agent_net_rate WHERE agid = %d AND agvdatefrom = %s AND agvdateto = %s';
    $q = $db->prepare_query( $s, $agid, date( 'Y-m-d', strtotime( $agvdatefrom ) ), date( 'Y-m-d', strtotime( $agvdateto ) ) );
    $r = $db->do_query( $q );

    if( isset( $r['error_code'] ) )
    {
        return $r;
    }
    else
    {
        save_log( $agid, 'agent', 'Delete rate validity : ' . $agvdatefrom . ' to ' . $agvdateto );

        return true;
    }
}

function get_agrate_validity_content( $agdatevalidity )
{
    if( empty( $agdatevalidity ) )
    {
        return '
        <div class="form-inline">
            <div class="form-group">
                <input type="text" class="text text-date text-date-from text-medium form-control" name="agvdatefrom[]" value="" placeholder="Date" autocomplete="off" readonly>
                <span>&nbsp;&nbsp;to&nbsp;&nbsp;</span>
                <input type="text" class="text text-date text-date-to text-medium form-control" name="agvdateto[]" value="" placeholder="Date"" autocomplete="off" readonly>
                <a class="delete-date" data-from="" data-to="" data-id="" data-type="net">
                    <img src="../l-plugins/ticket/images/delete.svg">
                </a>
            </div>
        </div>';
    }
    else
    {
        $list = '';

        foreach( $agdatevalidity as $d )
        {
            $list .= '
            <div class="form-inline">
                <div class="form-group">
                    <input type="text" class="text text-date text-date-from text-medium form-control" name="agvdatefrom[]" value="' . date( 'd F Y', strtotime( $d['agvdatefrom'] ) ) . '" placeholder="Date" autocomplete="off" readonly>
                    <span>&nbsp;&nbsp;to&nbsp;&nbsp;</span>
                    <input type="text" class="text text-date text-date-to text-medium form-control" name="agvdateto[]" value="' . date( 'd F Y', strtotime( $d['agvdateto'] ) ) . '" placeholder="Date"" autocomplete="off" readonly>
                    <a class="delete-date" data-from="' . $d['agvdatefrom'] . '" data-to="' . $d['agvdateto'] . '" data-id="' . $d['agid'] . '" data-type="net">
                        <img src="../l-plugins/ticket/images/delete.svg">
                    </a>
                </div>
            </div>';
        }

        return $list;
    }
}

function add_rate_date_validity()
{
    return '
    <div class="form-inline">
        <div class="form-group">
            <input type="text" class="text text-date text-date-from text-medium form-control" name="agvdatefrom[]" value="" placeholder="Date" autocomplete="off" readonly>
            <span>&nbsp;&nbsp;to&nbsp;&nbsp;</span>
            <input type="text" class="text text-date text-date-to text-medium form-control" name="agvdateto[]" value="" placeholder="Date"" autocomplete="off" readonly>
            <a class="delete-date" data-from="" data-to="" data-id="" data-type="net">
                <img src="../l-plugins/ticket/images/delete.svg">
            </a>
        </div>
    </div>';
}

function add_rate_date_price()
{
    $loc  = get_location_list();

    if( !empty( $loc ) )
    {
        $list   = '';
        $depart = array();
        $destin = array();

        foreach( $loc as $key => $d )
        {
            if( $d['lctype'] == '0' )
            {
                $depart[] = $d;
            }
            elseif( $d['lctype'] == '1' )
            {
                $destin[] = $d;
            }
            elseif( $d['lctype'] == '2' )
            {
                $depart[] = $d;
                $destin[] = $d;
            }
        }

        $list .= '
        <div class="net-price-block">
            <h3></h3>
            <select class="select-rate-category" style="width:100%;" autocomplete="off">
                ' . get_rate_category_option() . '
            </select>
            <div class="table-net-rate-wrapp">
                <table class="table-striped net-rate-list" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th rowspan="3">From/To</th>
                            <th rowspan="3">To/From</th>
                            <th colspan="6">Rate</th>
                        </tr>
                        <tr>
                            <th colspan="3">Include Transport</th>
                            <th colspan="3">Exclude Transport</th>
                        </tr>
                        <tr>
                            <th>Adult</th>
                            <th>Child</th>
                            <th>Infant</th>
                            <th>Adult</th>
                            <th>Child</th>
                            <th>Infant</th>
                        </tr>
                    </thead>
                    <tbody>';

                        foreach( $depart as $d )
                        {
                            foreach( $destin as $ds )
                            {
                                if( $d['lcid'] == $ds['lcid'] )
                                {
                                    continue;
                                }

                                $index = $d['lcid'] . '|' . $ds['lcid'];
                                $list .= '
                                <tr>
                                    <td>' . $d['lcname'] . '</td>
                                    <td>' . $ds['lcname'] . '</td>
                                    <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][adult_rate_inc_trans][]" value="0" type="text"></td>
                                    <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][child_rate_inc_trans][]" value="0" type="text"></td>
                                    <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][infant_rate_inc_trans][]" value="0" type="text"></td>
                                    <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][adult_rate_exc_trans][]" value="0" type="text"></td>
                                    <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][child_rate_exc_trans][]" value="0" type="text"></td>
                                    <td><input class="text form-control" name="rate[' . $d['lcid'] . '|' . $ds['lcid'] . '][infant_rate_exc_trans][]" value="0" type="text"></td>
                                </tr>';
                            }
                        }

                        $list .= '
                    </tbody>
                </table>
            </div>
        </div>';
    }

    return $list;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_agent_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        extract( $_POST );

        $data = ticket_agent_table_query( $chid, $agsp );

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-agent' )
    {
        if( is_access( $_COOKIE['user_id'], $_POST['state'], $_POST['sub'] ) )
        {
            if( delete_agent( $_POST['agid'] ) )
            {
                echo '{"result":"success"}';
            }
            else
            {
                echo '{"result":"failed"}';
            }
        }
        else
        {
            echo '{"result":"failed"}';
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-contract-file' )
    {
        $data = upload_contract_file();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-contract-file' )
    {
        $data = delete_contract_file();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'send-login-access' )
    {
        $data = send_login_access_data( $_POST['agid'] );

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-rate-validity-and-price' )
    {
        $data[ 'period' ] = add_rate_date_validity();
        $data[ 'prices' ] = add_rate_date_price();

        echo json_encode( $data ); ;
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-rate-validity' )
    {
        $d = delete_rate_validity( $_POST['agid'], $_POST['agvdatefrom'], $_POST['agvdateto'] );

        if( $d === true )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( $d );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'set-rate-by-rate-category' )
    {
        $content = set_agent_rate_by_rate_category( $_POST['rctid'] );

        if( empty( $content ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'content' => $content ) );
        }
    }
}

function set_agent_rate_by_rate_category( $rctid = '' )
{
    $location = get_location_list();
    $rctdata  = get_rate_category( $rctid );
    $rctvalue = json_decode( $rctdata['rctvalue'], true );

    if( !empty( $location ) )
    {
        $depart = array();
        $destin = array();

        foreach( $location as $key => $d )
        {
            if( $d['lctype'] == '0' )
            {
                $depart[] = $d;
            }
            elseif( $d['lctype'] == '1' )
            {
                $destin[] = $d;
            }
            elseif( $d['lctype'] == '2' )
            {
                $depart[] = $d;
                $destin[] = $d;
            }
        }

        $list = '
        <div class="net-price-block">
            <table class="table-striped net-rate-list" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th rowspan="3">From/To</th>
                        <th rowspan="3">To/From</th>
                        <th colspan="6">Rate</th>
                    </tr>
                    <tr>
                        <th colspan="3">Include Transport</th>
                        <th colspan="3">Exclude Transport</th>
                    </tr>
                    <tr>
                        <th>Adult</th>
                        <th>Child</th>
                        <th>Infant</th>
                        <th>Adult</th>
                        <th>Child</th>
                        <th>Infant</th>
                    </tr>
                </thead>
                <tbody>';

                    foreach( $depart as $dt )
                    {
                        foreach( $destin as $ds )
                        {
                            if( $dt['lcid'] == $ds['lcid'] )
                            {
                                continue;
                            }

                            $index = $dt['lcid'] . '|' . $ds['lcid'];

                            $adult_rate_inc_trans  = isset( $rctvalue[ $index ]['adult_rate_inc_trans'] ) ? $rctvalue[ $index ]['adult_rate_inc_trans'] : 0;
                            $child_rate_inc_trans  = isset( $rctvalue[ $index ]['child_rate_inc_trans'] ) ? $rctvalue[ $index ]['child_rate_inc_trans'] : 0;
                            $infant_rate_inc_trans = isset( $rctvalue[ $index ]['infant_rate_inc_trans'] ) ? $rctvalue[ $index ]['infant_rate_inc_trans'] : 0;

                            $adult_rate_exc_trans  = isset( $rctvalue[ $index ]['adult_rate_exc_trans'] ) ? $rctvalue[ $index ]['adult_rate_exc_trans'] : 0;
                            $child_rate_exc_trans  = isset( $rctvalue[ $index ]['child_rate_exc_trans'] ) ? $rctvalue[ $index ]['child_rate_exc_trans'] : 0;
                            $infant_rate_exc_trans = isset( $rctvalue[ $index ]['infant_rate_exc_trans'] ) ? $rctvalue[ $index ]['infant_rate_exc_trans'] : 0;

                            $list .= '
                            <tr>
                                <td>' . $dt['lcname'] . '</td>
                                <td>' . $ds['lcname'] . '</td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][adult_rate_inc_trans][]" value="' . $adult_rate_inc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][child_rate_inc_trans][]" value="' . $child_rate_inc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][infant_rate_inc_trans][]" value="' . $infant_rate_inc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][adult_rate_exc_trans][]" value="' . $adult_rate_exc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][child_rate_exc_trans][]" value="' . $child_rate_exc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][infant_rate_exc_trans][]" value="' . $infant_rate_exc_trans . '" type="text"></td>
                            </tr>';
                        }
                    }

                    $list .= '
                </tbody>
            </table>
        </div>';

        return $list;
    }
}

/*
| -------------------------------------------------------------------------------------
| Export - Agent Export
| -------------------------------------------------------------------------------------
*/
function ticket_agent_export_csv()
{
    $content = get_agent_data_export_csv();

    if( !empty( $content ) )
    {
        require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

        $html = str_get_html( $content );

        header( 'Content-type: application/ms-excel' );
        header( 'Content-Disposition: attachment; filename=agent-data.csv' );

        $fp = fopen( 'php://output', 'w' );

        foreach( $html->find( 'tr' ) as $element )
        {
            $td = array();

            foreach( $element->find( 'th' ) as $row )
            {
                $td [] = $row->plaintext;
            }

            foreach( $element->find( 'td' ) as $row )
            {
                $td [] = $row->plaintext;
            }

            fputcsv( $fp, $td );
        }

        fclose( $fp );
    }

    exit;
}

function get_agent_data_export_csv()
{
    $filter  = json_decode( base64_decode( $_GET['filter'] ), true );
    $datas   = ticket_get_agent_table_export( $filter );
    $content = '';

    if( !empty( $datas ) )
    {
        $content =
        '<table width="100%" border="1">
            <tr>
                <td>Agent</td>
                <td>Chn</td>
                <td>Channel</td>
                <td>Contracting</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Booking/Cancellation</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Invoicing/Payment</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Group Handler</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>SP</td>
                <td>Payment</td>
                <td>COD</td>
                <td>Rate Valid to</td>
                <td>Status</td>
            </tr>
            <tr>
                <td>Agent</td>
                <td>Chn</td>
                <td>Channel</td>
                <td>Contact Person</td>
                <td>Position</td>
                <td>Email</td>
                <td>Mobile</td>
                <td>Phone</td>
                <td>Operation Phone</td>
                <td>Fax</td>
                <td>Address</td>
                <td>Contact Person</td>
                <td>Position</td>
                <td>Email</td>
                <td>Mobile</td>
                <td>Phone</td>
                <td>Operation Phone</td>
                <td>Fax</td>
                <td>Address</td>
                <td>Contact Person</td>
                <td>Position</td>
                <td>Email</td>
                <td>Mobile</td>
                <td>Phone</td>
                <td>Operation Phone</td>
                <td>Fax</td>
                <td>Address</td>
                <td>Contact Person</td>
                <td>Position</td>
                <td>Email</td>
                <td>Mobile</td>
                <td>Phone</td>
                <td>Operation Phone</td>
                <td>Fax</td>
                <td>Address</td>
                <td>SP</td>
                <td>Payment</td>
                <td>COD</td>
                <td>Rate Valid to</td>
                <td>Status</td>
            </tr>';

        foreach ( $datas as $data )
        {
            $content .=
            '<tr>
                <td>' . $data['agent'] . '</td>
                <td>' . $data['chn'] . '</td>
                <td>' . $data['channel'] . '</td>
                <td>' . $data['ccp'] . '</td>
                <td>' . $data['cposition'] . '</td>
                <td>' . $data['cemail'] . '</td>
                <td>' . $data['cmobile'] . '</td>
                <td>' . $data['cphone'] . '</td>
                <td>' . $data['cophone'] . '</td>
                <td>' . $data['cfax'] . '</td>
                <td>' . $data['caddress'] . '</td>
                <td>' . $data['cp'] . '</td>
                <td>' . $data['position'] . '</td>
                <td>' . $data['email'] . '</td>
                <td>' . $data['mobile'] . '</td>
                <td>' . $data['phone'] . '</td>
                <td>' . $data['ophone'] . '</td>
                <td>' . $data['fax'] . '</td>
                <td>' . $data['address'] . '</td>
                <td>' . $data['icp'] . '</td>
                <td>' . $data['iposition'] . '</td>
                <td>' . $data['iemail'] . '</td>
                <td>' . $data['imobile'] . '</td>
                <td>' . $data['iphone'] . '</td>
                <td>' . $data['iophone'] . '</td>
                <td>' . $data['ifax'] . '</td>
                <td>' . $data['iaddress'] . '</td>
                <td>' . $data['gcp'] . '</td>
                <td>' . $data['gposition'] . '</td>
                <td>' . $data['gemail'] . '</td>
                <td>' . $data['gmobile'] . '</td>
                <td>' . $data['gphone'] . '</td>
                <td>' . $data['gophone'] . '</td>
                <td>' . $data['gfax'] . '</td>
                <td>' . $data['gaddress'] . '</td>
                <td>' . $data['sp'] . '</td>
                <td>' . $data['payment'] . '</td>
                <td>' . $data['cod'] . '</td>
                <td>' . $data['rvalid'] . '</td>
                <td>' . $data['status'] . '</td>
            </tr>';
        }

        $content .=
        '</table>';

        return $content;
    }
}


?>
