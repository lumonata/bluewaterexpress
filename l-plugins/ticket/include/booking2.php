<?php

add_actions( 'booking', 'ticket_booking' );
add_actions( 'ticket-booking-ajax_page', 'ticket_booking_ajax' );
add_actions( 'ticket-boarding-pass_page', 'ticket_boarding_pass_report' );
add_actions( 'ticket-booking-send-email_page', 'ticket_booking_send_email' );

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\ExecutePayment;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;

function ticket_booking()
{
    if( ( is_view_booking() || is_popup_detail_booking() ) && isset( $_GET['id'] ) )
    {
        return ticket_booking_show_detail( $_GET['id'] );
    }
    elseif( ( is_edit_booking() || is_popup_edit_booking() ) && isset( $_GET['id'] ) )
    {
        return ticket_booking_edit_detail( $_GET['id'] );
    }
    elseif( ( is_revise_booking() || is_popup_revise_booking() ) && isset( $_GET['id'] ) )
    {
        return ticket_booking_revise_detail( $_GET['id'] );
    }
    elseif( ( is_cancel_reservation() || is_popup_cancel_reservation() ) && isset( $_GET['id'] ) )
    {
        return ticket_cancel_reservation_form( $_GET['id'] );
    }

    return ticket_booking_table_data();
}

/*
| -------------------------------------------------------------------------------------
| Booking Table List
| -------------------------------------------------------------------------------------
*/
function ticket_booking_table_data()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url('reservation&sub=booking') . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    $site_url = site_url();
    $filter   = ticket_filter_booking();

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/booking/list.html', 'booking' );

    add_block( 'list-block', 'bblock', 'booking' );

    add_variable( 'bdate', $bdate );
    add_variable( 'search', $search );
    add_variable( 'bbemail', $bbemail );
    add_variable( 'date_end', $date_end );
    add_variable( 'date_start', $date_start );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'action', get_state_url( 'reservation&sub=booking' ) );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-booking-ajax' );


    add_variable( 'rev_status', get_reservation_status( $rstatus, true,'All Reservation Status') );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'location_option', get_location_option( $lcid, true, 'All Departure') );
    add_variable( 'status_option', get_booking_status_option( $status, true, 'All Status') );
    add_variable( 'location2_option', get_location_option( $lcid2, true, 'All Arrival Point') );
    add_variable( 'bsource_option', get_booking_source_option( $chid, null, true, 'All Booking Source' ) );

    parse_template('list-block','bblock', false);

    add_actions( 'section_title', 'Reservation List' );
    add_actions( 'other_elements', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template('booking');
}

/*
| -------------------------------------------------------------------------------------
| Booking Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_booking_table_query( $chid = '', $lcid = '', $lcid2 = '', $rid = '', $status = '', $date_start = '', $date_end = '', $search = '', $bdate = '', $bbemail = '' , $rstatus = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        0  => 'a.bticket',
        1  => 'a.bdate',
        5  => 'c.bddate',
        7  => 'c.bddeparttime',
        8  => 'c.bdarrivetime',
        10 => 'a.bbrevstatus'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.bticket DESC, a.bdate DESC';
    }

    if( empty( $rdata['search']['value'] ) )
    {
        $w = '';

        if( !empty( $status ) )
        {
            if( is_array( $status ) )
            {
                $estatus = end( $status );

                $w .= '( ';

                foreach( $status as $st )
                {
                    if( $st == $estatus )
                    {
                        $w .= $db->prepare_query( 'c.bdpstatus = %s', $st );
                    }
                    else
                    {
                        $w .= $db->prepare_query( 'c.bdpstatus = %s OR ', $st );
                    }
                }

                $w .= ' )';
            }
            else
            {
                $w .= $db->prepare_query( 'c.bdpstatus = %s', $status );
            }
        }
        else
        {
            $w .= $db->prepare_query( 'c.bdpstatus NOT IN( %s )', 'ol' );
        }

        if( !empty( $search ) )
        {
            $cols[2] = '(
                SELECT GROUP_CONCAT( CONVERT( bpname USING utf8 ) SEPARATOR "<br/><br/>" )
                FROM ticket_booking_passenger AS a2
                WHERE a2.bdid = c.bdid
                ORDER BY a2.bpid ASC
            )';

            $esearch = end( $cols );

            $w .= ' AND ( ';

            foreach( $cols as $col )
            {
                if( $col == $esearch )
                {
                    $w .= $db->prepare_query( $col . ' LIKE %s', '%' . $search . '%' );
                }
                else
                {
                    $w .= $db->prepare_query( $col . ' LIKE %s OR ', '%' . $search . '%' );
                }
            }

            $w .= ' )';
        }

        if( !empty( $rstatus ) )
        {
            if( is_array( $rstatus ) )
            {
                $estatus = end( $rstatus );

                $w .= ' AND ( ';

                foreach( $rstatus as $st )
                {
                    if( $st == $estatus )
                    {
                        $w .= $db->prepare_query( 'c.bdrevstatus = %s', $st );
                    }
                    else
                    {
                        $w .= $db->prepare_query( 'c.bdrevstatus = %s OR ', $st );
                    }
                }

                $w .= ' )';
            }
            else
            {
                $w .= $db->prepare_query( ' AND c.bdrevstatus = %s', $rstatus );
            }
        }

        if( $chid != '' )
        {
            $arr = explode( '|', $chid );

            if( count( $arr ) == 2 )
            {
                $w .= $db->prepare_query( ' AND a.chid = %d', $arr[0] );
                $w .= $db->prepare_query( ' AND a.agid = %d', $arr[1] );
            }
            else
            {
                $w .= $db->prepare_query( ' AND a.chid = %d', $chid );
            }
        }

        if( $lcid != '' )
        {
            $w .= $db->prepare_query( ' AND c.bdfrom = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid );
        }

        if( $lcid2 != '' )
        {
            $w .= $db->prepare_query( ' AND c.bdto = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid2 );
        }

        if( $rid != '' )
        {
            $w .= $db->prepare_query( ' AND c.rid = %d', $rid );
        }

        if( $bdate != '' )
        {
            $w .= $db->prepare_query( ' AND a.bdate = %s', date( 'Y-m-d', strtotime( $bdate ) ) );
        }

        if( $bbemail != '' )
        {
            $w .= $db->prepare_query( ' AND a.bbemail LIKE %s', '%' . $bbemail . '%' );
        }

        if( $date_start != '' && $date_end != '' )
        {
            $w .= $db->prepare_query( ' AND c.bddate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $date_start ) ), date( 'Y-m-d', strtotime( $date_end ) ) );
        }

        $s = 'SELECT
                a.bid,
                a.bcode,
                a.agid,
                a.sagid,
                a.bticket,
                b.chcode,
                b.chname,
                a.bbname,
                a.bdate,
                c.bdpstatus,
                c.bdid,
                c.bdfrom,
                c.bdto,
                c.bddate,
                c.bdtype,
                c.bddeparttime,
                c.bdarrivetime,
                a.bbrevtype,
                c.bdrevstatus,
                a.bbrevagent,
                a.bcreason,
                a.bcmessage,
                a.bbphone,
                a.brcdate,
                a.bonhandtotal,
                a.btotal,
                a.bbooking_staf,
                a.bprintboardingstatus,
                a.bbemail,
                a.bremark,
                c.total,
                d.agname,
                c.num_adult,
                c.rid,
                c.num_child,
                c.num_infant,
                c.bdstatus,
                a.btype,
                a.pmcode,
                a.bdiscountnum,
                a.bdiscounttype,
                ( c.num_adult + c.num_child + c.num_infant ) AS num_pax,
                (
                    SELECT GROUP_CONCAT( IF( bpstatus = "cn", CONCAT("<strike>",CONVERT( bpname USING utf8 ),"</strike>"), CONVERT( bpname USING utf8 ) ) SEPARATOR "<br/><br/>" )
                    FROM ticket_booking_passenger AS a2
                    WHERE a2.bdid = c.bdid
                    ORDER BY a2.bpid ASC
                ) AS pass_name
              FROM ticket_booking AS a
              LEFT JOIN ticket_channel AS b ON a.chid = b.chid
              LEFT JOIN ticket_booking_detail AS c ON c.bid = a.bid
              LEFT JOIN ticket_agent AS d ON d.agid = a.agid
              WHERE ' . $w . ' AND a.bstt <> "ar" ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $bsearch = array();
        $cols[2] = '(
            SELECT GROUP_CONCAT( CONVERT( bpname USING utf8 ) SEPARATOR "<br/><br/>" )
            FROM ticket_booking_passenger AS a2
            WHERE a2.bdid = c.bdid
            ORDER BY a2.bpid ASC
        )';

        foreach( $cols as $col )
        {
            $bsearch[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                a.bid,
                a.bcode,
                a.agid,
                a.sagid,
                a.bticket,
                b.chcode,
                b.chname,
                a.bbname,
                a.bdate,
                c.bdpstatus,
                c.bdid,
                c.bdfrom,
                c.bdto,
                c.bddate,
                c.bdtype,
                c.bddeparttime,
                c.bdarrivetime,
                a.bbrevtype,
                c.bdrevstatus,
                a.bbrevagent,
                a.bcreason,
                a.bcmessage,
                a.bbphone,
                a.brcdate,
                a.bonhandtotal,
                a.btotal,
                a.bbooking_staf,
                a.bprintboardingstatus,
                a.bbemail,
                a.bremark,
                c.total,
                d.agname,
                c.num_adult,
                c.rid,
                c.num_child,
                c.num_infant,
                c.bdstatus,
                a.btype,
                a.pmcode,
                a.bdiscountnum,
                a.bdiscounttype,
                ( c.num_adult + c.num_child + c.num_infant ) AS num_pax,
                (
                    SELECT GROUP_CONCAT( IF( bpstatus = "cn", CONCAT("<strike>",CONVERT( bpname USING utf8 ),"</strike>"), CONVERT( bpname USING utf8 ) ) SEPARATOR "<br/><br/>" )
                    FROM ticket_booking_passenger AS a2
                    WHERE a2.bdid = c.bdid
                    ORDER BY a2.bpid ASC
                ) AS pass_name
              FROM ticket_booking AS a
              LEFT JOIN ticket_channel AS b ON a.chid = b.chid
              LEFT JOIN ticket_booking_detail AS c ON c.bid = a.bid
              LEFT JOIN ticket_agent AS d ON d.agid = a.agid
              WHERE ( ' . implode( ' OR ', $bsearch ) . ' ) AND a.bstt <> "ar" ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data   = array();
    $filter = array( 'rid' => $rid, 'lcid' => $lcid, 'lcid2' => $lcid2, 'chid' => $chid, 'bstatus' => $status, 'bdate' => $bdate, 'date_start' => $date_start, 'date_end' => $date_end, 'sbooking' => $search, 'bbemail' => $bbemail );

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $cnccss    = $d2['bdpstatus'] == 'cn' ? 'sr-only' : '-';
            $trans     = ticket_booking_transport_detail( $d2['bdid'] );
            $booked    = empty( $d2['agid'] ) ? $d2['bbname'] : $d2['bbooking_staf'];
            $brcdate   = $d2['brcdate'] == '0000-00-00' ? '' : date( 'd M Y', strtotime( $d2['brcdate'] ) );
            $pass_name = $d2['pass_name'] . '<br /><br />' . 'Pax : ' . $d2['num_pax'] . ' | Adult : ' . $d2['num_adult'] . ' | Child : ' . $d2['num_child'] . ' | Infant : ' . $d2['num_infant'] . '<span class="sr-only">' . $d2['num_pax'] . '|' .  $d2['num_adult'] . '|' . $d2['num_child'] . '|' . $d2['num_infant'] . '</span>';
            $pass_name = mb_convert_encoding( $pass_name, 'UTF-8', 'UTF-8' );
            $phone     = mb_convert_encoding( $d2['bbphone'], 'UTF-8', 'UTF-8' );

            if( $d2['pmcode'] != '' && $d2['bdiscountnum'] >= 0 )
            {
                if( $d2['bdiscounttype'] == '0' )
                {
                    $d2['total'] = $d2['total'] - ( ( $d2['total'] * $d2['bdiscountnum'] ) / 100  );
                }
                else
                {
                    $d2['total'] = $d2['total'] - $d2['bdiscountnum'];
                }
            }

            $via_by = get_via_by( $d2['rid'] , $d2['bdfrom'] , $d2['bdto'] );

            $data[] = array(
                'transport'   => $trans,
                'booked_by'   => $booked,
                'cancel_css'  => $cnccss,
                'brcdate'     => $brcdate,
                'guest_name'  => $pass_name,
                'id'          => $d2['bid'],
                'bdid'        => $d2['bdid'],
                'agid'        => $d2['agid'],
                'ref_code'    => $d2['bcode'],
                'phone'       => $phone,
                'no_ticket'   => $d2['bticket'],
                'bstatus'     => $d2['bdpstatus'],
                'bbemail'     => $d2['bbemail'],
                'bcreason'    => $d2['bcreason'],
                'bcmessage'   => $d2['bcmessage'],
                'rsv_type'    => $d2['bbrevtype'],
                'rsv_status'  => $d2['bdrevstatus'],
                'bonhandtotal'=> $d2['bonhandtotal'],
                'print_sts'   => $d2['bprintboardingstatus'],
                'remarks'     => $d2['bcmessage'] . '<br>' . $d2['bremark'],
                'route'       => $d2['bdfrom'] . ' - ' . $d2['bdto']. $via_by,
                'bdtype'      => ucwords( $d2['bdtype'] ),
                'ref_agent'   => $d2['bbrevagent'] != '' ? $d2['bbrevagent'] : '-',
                'status'      => $d2['bdstatus'] == 'cn' ? 'Cancelled' : ticket_booking_status( $d2['bdpstatus'] ),
                'total'       => number_format( $d2['total'], 0, ',', '.' ),
                'btotal'      => number_format( $d2['btotal'], 0, ',', '.' ),
                'rev_date'    => date( 'd M Y', strtotime( $d2['bdate'] ) ),
                'dept_date'   => date( 'd M Y', strtotime( $d2['bddate'] ) ),
                'dept_time'   => date( 'H:i', strtotime( $d2['bddeparttime'] ) ),
                'arrive_time' => date( 'H:i', strtotime( $d2['bdarrivetime'] ) ),
                'chanel'      => empty( $d2['agid'] ) ? $d2['chname'] : $d2['agname'],
                'ticket_date' => $d2['bticket'] . '<br/><span style="font-size:10px;">(' . date( 'd M Y', strtotime( $d2['bdate'] ) ) . ')</span>',
                'edit_link'   => HTSERVER . $surl . '/l-admin/?state=reservation&sub=booking&prc=edit&id=' . $d2['bid'] . '&filter=' . base64_encode( json_encode( $filter ) ),
                'detail_link' => HTSERVER . $surl . '/l-admin/?state=reservation&sub=booking&prc=popup-detail&id=' . $d2['bid'] . '&filter=' . base64_encode( json_encode( $filter ) ),
                'cancel_link' => HTSERVER . $surl . '/l-admin/?state=reservation&sub=booking&prc=cancel-reservation&id=' . $d2['bid'] . '&filter=' . base64_encode( json_encode( $filter ) )
            );
        }
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );
    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Driver Detail
| -------------------------------------------------------------------------------------
*/
function get_driver_detail($btid)
{
    global $db;

    $s = 'SELECT btdrivername, btdriverphone FROM ticket_booking_transport WHERE btid = %s';
    $q = $db->prepare_query( $s, $btid );
    $r = $db->do_query( $q );

    while( $d = $db->fetch_array( $r ) )
    {
		$result['btdrivername']  = !empty( $d['btdrivername'] ) || $d['btdrivername'] == ""  ? $d['btdrivername'] : '-';
		$result['btdriverphone'] = !empty( $d['btdriverphone'] ) || $d['btdrivername'] == "" ? $d['btdriverphone'] : '-';
    }

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Via By
| -------------------------------------------------------------------------------------
*/
function get_via_by( $rid, $bdfrom_id, $bdto_id )
{
    global $db;

    $s = 'SELECT lcname FROM ticket_location
          WHERE lcid = ( SELECT via_id FROM ticket_route WHERE rid = %d )';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $start = get_location( $bdfrom_id, 'lcname' );
    $end   = get_location( $bdto_id, 'lcname' );

    if( !empty( $d['lcname'] ) )
    {
        if( $start != $d['lcname'] && $end != $d['lcname'] )
        {
            $location_by = ' Via ' . $d['lcname'];
        }
        else
        {
            $location_by ='';
        }
    }
    else
    {
        $location_by ='';
    }

    return $location_by;
}

/*
| -------------------------------------------------------------------------------------
| Last Booking Count Query
| -------------------------------------------------------------------------------------
*/
function ticket_last_booking_count()
{
    global $db;

    $s = 'SELECT COUNT(bid) as num FROM ticket_booking WHERE bdate >= DATE_SUB( CURRENT_DATE, INTERVAL 1 MONTH )';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d['num'];
}

/*
| -------------------------------------------------------------------------------------
| 30 days Booking Chart Data
| -------------------------------------------------------------------------------------
*/
function ticket_last_30_days_chart()
{
    global $db;

    $s = 'SELECT
            a.bid,
            a.bdate,
            a.bticket,
            b.bdfrom,
            b.bdto,
            b.num_adult,
            b.num_child,
            b.num_infant,
            b.num_adult + b.num_child + b.num_infant AS amount,
            c.chid,
            c.chcode,
            c.chname,
            b.bdtype
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          WHERE a.bstt <> %s
          ORDER BY a.bdate, b.bdtype, b.bdfrom, b.bdto ASC';
    $q = $db->prepare_query( $s, 'ar' );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $data_arr = array();
        $date_arr = array();

        while( $d = $db->fetch_array( $r ) )
        {
            if( isset( $date_arr[ $d['bdate'] ] ) )
            {
                $date_arr[ $d['bdate'] ] += 1;
            }
            else
            {
                $date_arr[ $d['bdate'] ] = 1;
            }

            $data_arr[] = $d;
        }

        foreach( $data_arr as $dt )
        {
            $index = base64_encode( json_encode( array( $dt['bdate'], $date_arr[ $dt['bdate'] ] ) ) );

            $items[ $index ][ $dt['chname'] ][] = array(
                'bid' => $dt['bid'],
                'bdate' => $dt['bdate'],
                'bticket' => $dt['bticket'],
                'bdfrom' => $dt['bdfrom'],
                'bdto' => $dt['bdto'],
                'num_adult' => $dt['num_adult'],
                'num_child' => $dt['num_child'],
                'num_infant' => $dt['num_infant'],
                'chid' => $dt['chid'],
                'chcode' => $dt['chcode'],
                'chname' => $dt['chname'],
                'amount' => $dt['amount']
            );
        }

        if( !empty( $items ) )
        {
            $category = array();
            $series   = array();

            foreach( $items as $obj => $groups )
            {
                list( $rev_date, $total_rev ) = json_decode( base64_decode( $obj ) );

                $category[] = $rev_date;

                foreach( $groups as $channel => $d )
                {
                    $series[$channel][ $rev_date ][] = $total_rev;
                }
            }

            return get_chart_options( $category, $series );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Last Booking Table List
| -------------------------------------------------------------------------------------
*/
function ticket_last_booking()
{
    global $db;

    $s = 'SELECT
            a.bid,
            a.agid,
            a.bticket,
            c.chcode,
            a.bbname,
            a.bdate,
            a.bstatus,
            d.bdid,
            d.bdfrom,
            d.bdto,
            d.bddeparttime,
            e.lcid,
            d.bddate
          FROM ticket_booking AS a
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          LEFT JOIN ticket_booking_detail AS d ON d.bid = a.bid
          LEFT JOIN ticket_location AS e ON d.bdfrom = e.lcname
          WHERE a.bstatus NOT IN( %s ) AND a.bstt <> %s
          ORDER BY DATE( a.bdate ) DESC LIMIT 5';
    $q = $db->prepare_query( $s, 'ol', 'ar' );
    $r = $db->do_query( $q );

    set_template( PLUGINS_PATH . '/ticket/tpl/booking/table.html', 'booking' );

    add_block( 'table-block', 'tbblock', 'booking' );

    add_variable( 'list', ticket_booking_list_data( $r ) );

    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template('table-block','tbblock', false);

    return return_template('booking');
}

/*
| -------------------------------------------------------------------------------------
| Booking Table List Item
| -------------------------------------------------------------------------------------
*/
function ticket_booking_list_data( $r )
{
    global $db;

    $site_url = site_url();

    if( $db->num_rows( $r ) == 0 )
    {
        if( isset( $_POST['s'] ) && !empty( $_POST['s'] ) )
        {
            return '
            <tr>
                <td colspan="9">
                    <p class="text-center text-danger">No result found for <em>"' . $_POST['s'] . '"</em>. Check your spellling or try another terms</p>
                </td>
            </tr>';
        }
        else
        {
            return '
            <tr>
                <td colspan="9">
                    <p class="text-center text-danger">No data found</p>
                </td>
            </tr>';
        }
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/booking/loop.html', 'booking-loop' );

    add_block( 'loop-block', 'bloop', 'booking-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        $pass = ticket_passenger_list( $d['bdid'] );

        add_variable( 'id', $d['bid'] );
        add_variable( 'chanel', $d['chcode'] );
        add_variable( 'no_ticket', $d['bticket'] );
        add_variable( 'dept_time', $d['bddeparttime'] );
        add_variable( 'route', $d['bdfrom'] . ' - ' . $d['bdto'] );
        add_variable( 'rev_date', date('d M Y', strtotime( $d['bdate'] ) ) );
        add_variable( 'dept_date', date( 'd M Y', strtotime( $d['bddate'] ) ) );
        add_variable( 'booked_by', empty( $d['agid'] ) ? $d['bbname'] : get_agent( $d['agid'], 'agname' ) );

        add_variable( 'status', ticket_booking_status( $d['bstatus'] ) );
        add_variable( 'guest_name', ticket_passenger_list_name( $pass ) );
        add_variable( 'cancel_css', $d['bstatus'] == 'cn' ? 'sr-only' : '' );

        add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-booking-ajax/' );
        add_variable( 'detail_link', HTSERVER . $site_url . '/l-admin/?state=reservation&sub=booking&prc=popup-detail&id=' . $d['bid'] );
        add_variable( 'edit_link', HTSERVER . $site_url . '/l-admin/?state=reservation&sub=booking&prc=edit&id=' . $d['bid'] );

        parse_template('loop-block', 'bloop', true);
    }

    return return_template('booking-loop');
}

function ticket_booking_edit_detail( $bid )
{
    if( isset( $_POST['revise_booking'] ) )
    {
        header( 'Location:' . get_state_url( 'reservation&sub=booking&prc=revise&id=' . $bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );

        exit;
    }

    $site_url = site_url();
    $data     = ticket_booking_all_data( $bid );
    $update   = run_booking_update( false, $data );

    if( !empty( $data ) )
    {
        extract( $data['detail'] );

        $bstatus_message = ticket_booking_status( $data['bstatus'] );

        set_template( PLUGINS_PATH . '/ticket/tpl/booking/form.html', 'booking-form' );

        add_block( 'form-departure-loop-block', 'fmdl-block', 'booking-form' );
        add_block( 'form-return-loop-block', 'fmrl-block', 'booking-form' );
        add_block( 'form-departure-block', 'fmd-block', 'booking-form' );
        add_block( 'form-return-block', 'fmr-block', 'booking-form' );
        add_block( 'form-block', 'fm-block', 'booking-form' );

        add_variable( 'site_url', $site_url );
        add_variable( 'gemail', $data['gemail'] );
        add_variable( 'gmobile', $data['gmobile'] );
        add_variable( 'bcode', $data['bcode'] );
        add_variable( 'booking_id', $data['bid'] );
        add_variable( 'bstatus', $data['bstatus'] );
        add_variable( 'bticket', $data['bticket'] );
        add_variable( 'bagremark', $data['bagremark'] );
        add_variable( 'bfilter', isset( $_GET['filter'] ) ? $_GET['filter'] : '' );
        add_variable( 'bstatus_option', get_booking_status_option( $data['bstatus'], false ) );
        add_variable( 'bsource_option', get_booking_source_option( $data['chid'], $data['agid'], false ) );
        add_variable( 'bblockingtime', empty( $data['bblockingtime'] ) ? '' : date( 'd F Y H:i', $data['bblockingtime'] ) );

        add_variable( 'agid', $data['agid'] );
        add_variable( 'bbname', $data['bbname'] );
        add_variable( 'bbemail', $data['bbemail'] );
        add_variable( 'bbphone', $data['bbphone'] );

        if( empty( $data['agid'] ) )
        {
            add_variable( 'bsource', $data['chname'] );
        }
        else
        {
            $agid = empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'];

            add_variable( 'bsource', get_agent( $agid, 'agname' ) );
        }

        add_variable( 'bcreason', $data['bcreason'] );
        add_variable( 'bcmessage', $data['bcmessage'] );
        add_variable( 'brefund', $data['brefund'] );
        add_variable( 'btransactionfee', $data['btransactionfee'] );
        add_variable( 'bcancellationfee', $data['bcancellationfee'] );

        add_variable( 'bcdate', $data['bcdate'] == '0000-00-00' ? '' : date( 'd M Y', strtotime( $data['bcdate'] ) ) );
        add_variable( 'brcdate', $data['brcdate'] == '0000-00-00' ? '' : date( 'd M Y', strtotime( $data['brcdate'] ) ) );

        add_variable( 'bremark', $data['bremark'] );
        add_variable( 'bbrevagent', $data['bbrevagent'] );
        add_variable( 'bbooking_staf', $data['bbooking_staf'] );
        add_variable( 'bbrevtype_option', get_revtype_option( $data['bbrevtype'] ) );
        add_variable( 'bbrevstatus_option', get_revstatus_option( $data['bbrevstatus'] ) );
        add_variable( 'bbcountry_list_option', get_country_list_option( $data['bbcountry'] ) );

        add_variable( 'btype', ticket_booking_type( $data['btype'] ) );
        add_variable( 'bdate', date( 'd M Y', strtotime( $data['bdate'] ) ) );

        $div_blocked =
        '<div class="row-detail-item-block">
            <div class="row-detail-col-block">
                <h2 class="row-detail-block-font">Cancelled</h2>
            </div>
        </div>';

        //-- Go Trip
        if( isset( $departure ) )
        {
            foreach( $departure as $dp_trip )
            {
                extract( $dp_trip );

                $dep_trans = get_transport_edit_field( $dp_trip );

                add_variable( 'dp_id', $bdid );
                add_variable( 'dp_date', $bddate );
                add_variable( 'dep_item', ticket_item_list_edit_content( $data['agid'], $data['bdate'], $dp_trip, $data ) );
                add_variable( 'dep_bdrevstatus', get_revstatus_option( $bdrevstatus ) );
                add_variable( 'dep_bdpstatus', get_booking_status_option( $bdpstatus, false ) );
                add_variable( 'dep_passenger', ticket_passenger_list_edit_content( $passenger ) );

                add_variable( 'dep_transport', $dep_trans );
                add_variable( 'dep_bdtype', ucfirst( $bdtype ) );
                add_variable( 'dep_trans_css', empty( $dep_trans ) ? 'sr-only' : '' );

                add_variable( 'row-detail-booking-pos', $bdstatus == 'cn' ? 'row-detail-booking-pos' : '' );
                add_variable( 'div_blocked', $bdstatus == 'cn' ? $div_blocked : '' );
                add_variable( 'status_sub_heading', $bdstatus == 'cn' ? '<span style="color:red">( Cancelled )</span>' : '' );

                parse_template( 'form-departure-loop-block', 'fmdl-block', true );
            }

            parse_template( 'form-departure-block', 'fmd-block' );
        }

        //-- Back Trip
        if( isset( $return ) )
        {

            foreach( $return as $rt_trip )
            {
                extract( $rt_trip );

                $rtn_trans = get_transport_edit_field( $rt_trip );

                add_variable( 'rt_id', $bdid );
                add_variable( 'rt_date', $bddate );
                add_variable( 'rtn_item', ticket_item_list_edit_content( $data['agid'], $data['bdate'], $rt_trip, $data ) );
                add_variable( 'rtn_bdrevstatus', get_revstatus_option( $bdrevstatus ) );
                add_variable( 'rtn_bdpstatus', get_booking_status_option( $bdpstatus, false ) );
                add_variable( 'rtn_passenger', ticket_passenger_list_edit_content( $passenger ) );

                add_variable( 'rtn_transport', $rtn_trans );
                add_variable( 'rtn_bdtype', ucfirst( $bdtype ) );
                add_variable( 'rtn_trans_css', empty( $rtn_trans ) ? 'sr-only' : '' );

                add_variable( 'row-detail-booking-pos', $bdstatus == 'cn' ? 'row-detail-booking-pos' : '' );
                add_variable( 'div_blocked', $bdstatus == 'cn' ? $div_blocked : '' );
                add_variable( 'status_sub_heading', $bdstatus == 'cn' ? '<span style="color:red">( Cancelled )</span>' : '' );

                parse_template( 'form-return-loop-block', 'fmrl-block', true );
            }

            parse_template( 'form-return-block', 'fmr-block' );
        }

        $btotal       = $data['btotal'];
        $bsubtotal    = $data['bsubtotal'];
        $bdiscount    = $data['bdiscount'];
        $bonhandtotal = $data['bonhandtotal'];
        $remaintotal  = $btotal - $data['bonhandtotal'];

        if( empty( $data['pmcode'] ) )
        {
            add_variable( 'sub_css', 'sr-only' );
        }
        else
        {
            add_variable( 'pmcode', 'Code : ' . $data['pmcode'] );
        }

        add_variable( 'onhandtotal_display', $bonhandtotal );
        add_variable( 'remaintotal_display', $remaintotal );
        add_variable( 'subtotal_display', $bsubtotal );
        add_variable( 'discount_display', $bdiscount );
        add_variable( 'grandtotal_display', $btotal );

        add_variable( 'bhotelname', $data['bhotelname'] );
        add_variable( 'bhoteladdress', $data['bhoteladdress'] );
        add_variable( 'bhotelphone', $data['bhotelphone'] );
        add_variable( 'bhotelemail', $data['bhotelemail'] );

        add_variable( 'subtotal', $data['bsubtotal'] );
        add_variable( 'discount', $data['bdiscount'] );
        add_variable( 'onhandtotal', $data['bonhandtotal'] );

        add_variable( 'message', generate_message_block() );
        add_variable( 'state_url', get_state_url( 'reservation&sub=booking' ) );
        add_variable( 'payment_detail_content', get_payment_edit_field( $data ) );
        add_variable( 'popup_link', get_state_url( 'reservation&sub=booking&prc=popup-revise&id=' . $bid ) );
        add_variable( 'print_boarding_pas_link', HTSERVER . $site_url . '/ticket-boarding-pass/?id=' . $bid );

        add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-booking-ajax' );

        add_actions( 'section_title', 'Booking Detail' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
        add_actions( 'other_elements', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/numeral.min.js?v=' . TICKET_VERSION );
        add_actions( 'other_elements', 'get_custom_javascript', 'https://cdn.jsdelivr.net/npm/autonumeric@1.9.46' );

        parse_template( 'form-block', 'fm-block' );

        return return_template( 'booking-form' );
    }
}

function ticket_booking_revise_detail( $bid )
{
    $data = ticket_booking_all_data( $bid );

    if( empty( $data ) )
    {
        header( 'Location:' . get_state_url( 'reservation&sub=booking' . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );

        exit;
    }

    if( !empty( $data['detail'] ) )
    {
        if( isset( $_GET['revise'] ) && $_GET['revise'] == 'passenger' )
        {
            $content = ticket_booking_revise_passenger( $data, $bid );
        }
        elseif( isset( $_GET['revise'] ) && $_GET['revise'] == 'trip' )
        {
            $content = ticket_booking_revise_trip( $data, $bid );
        }
        else
        {
            $content = ticket_booking_revise_booking( $data, $bid );
        }

        return $content;
    }
}

function is_valid_for_revise_booking( $data, $bid )
{
    return true;
}

function ticket_booking_revise_booking( $data, $bid )
{
    $filter = isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '';

    if( isset( $_POST['revise_booking_data'] ) )
    {
        $new_bid = ticket_booking_revise_data( $_POST, $data, $bid );

        if( !empty( $new_bid ) )
        {
            header( 'Location:' . get_state_url( 'reservation&sub=booking&prc=edit&id=' . $new_bid . $filter ) );

            exit;
        }
    }
    else
    {
        extract( $data );

        set_template( PLUGINS_PATH . '/ticket/tpl/booking/revise-booking.html', 'revise' );

        add_block( 'no-departure-trip-loop-block', 'ndtrl-block', 'revise' );
        add_block( 'no-return-trip-loop-block', 'nrtrl-block', 'revise' );
        add_block( 'departure-trip-loop-block', 'dtrl-block', 'revise' );
        add_block( 'return-trip-loop-block', 'rtrl-block', 'revise' );
        add_block( 'departure-trip-block', 'dtr-block', 'revise' );
        add_block( 'return-trip-block', 'rtr-block', 'revise' );
        add_block( 'revise-block', 'rv-block', 'revise' );

        if( isset( $_POST['check_trip_availability'] ) )
        {
            extract( $_POST );

            $depart_start_point = isset( $depart_start_point ) ? $depart_start_point : '';
            $return_start_point = isset( $return_start_point ) ? $return_start_point : '';
            $depart_end_point   = isset( $depart_end_point ) ? $depart_end_point : '';
            $return_end_point   = isset( $return_end_point ) ? $return_end_point : '';
            $bsource            = isset( $booking_source ) ? $booking_source : '';
            $return_date        = isset( $return_date ) ? $return_date : '';
            $depart_date        = isset( $depart_date ) ? $depart_date : '';
            $depart_id          = isset( $depart_id ) ? $depart_id : '';
            $return_id          = isset( $return_id ) ? $return_id : '';

            $depart_end_point_name   = get_location( $depart_end_point, 'lcname' );
            $depart_start_point_name = get_location( $depart_start_point, 'lcname' );
            $return_end_point_name   = get_location( $return_end_point, 'lcname' );
            $return_start_point_name = get_location( $return_start_point, 'lcname' );

            $bpstatus_depart    = isset( $data['detail']['departure'][$depart_id]['bdpstatus'] ) ? $data['detail']['departure'][$depart_id]['bdpstatus'] : '';
            $bpstatus_return    = isset( $data['detail']['return'][$return_id]['bdpstatus'] ) ? $data['detail']['return'][$return_id]['bdpstatus'] : '';

            if( isset( $route_type ) && $route_type == '0' )
            {
                $return_point_css = 'disabled';
                $one_way_check    = 'checked';
                $return_css       = 'sr-only';
                $return_check     = '';
                $return_passenger = '';
                $depart_passenger = get_passenger_list_edit_content( $depart_id, $bpstatus_depart, 'departure', $adult, $child, $infant );
            }
            else
            {
                $return_point_css = '';
                $one_way_check    = '';
                $return_css       = '';
                $return_check     = 'checked';
                $return_passenger = get_passenger_list_edit_content( $return_id, $bpstatus_return, 'return', $adult, $child, $infant );
                $depart_passenger = get_passenger_list_edit_content( $depart_id, $bpstatus_depart, 'departure', $adult, $child, $infant );
            }

            $arr = explode( '|', $bsource );

            if( count( $arr ) == 2 )
            {
                $chid = $arr[0];
                $agid = $arr[1];
            }
            else
            {
                $chid = $arr[0];
                $agid = null;
            }

            $depart_end_point_list   = get_availibility_loc_option( null, $depart_end_point );
            $return_end_point_list   = get_availibility_loc_option( null, $return_end_point );
            $depart_start_point_list = get_availibility_loc_option( null, $depart_start_point );
            $return_start_point_list = get_availibility_loc_option( null, $return_start_point );
            $booking_source_list     = get_booking_source_option( $chid, $agid );

            add_variable( 'chid', $chid );
            add_variable( 'agid', $agid );

            add_variable( 'adult_num', $adult );
            add_variable( 'child_num', $child );
            add_variable( 'infant_num', $infant );

            add_variable( 'adult_num_val', $num_adult );
            add_variable( 'child_num_val', $num_child );
            add_variable( 'infant_num_val', $num_infant );

            add_variable( 'route_type', $route_type );
            add_variable( 'booking_source_list', $booking_source_list );

            add_variable( 'depart_id', $depart_id );
            add_variable( 'depart_date', $depart_date );
            add_variable( 'depart_passenger', $depart_passenger );
            add_variable( 'depart_end_point', $depart_end_point );
            add_variable( 'depart_start_point', $depart_start_point );
            add_variable( 'depart_end_point_name', $depart_end_point_name );
            add_variable( 'depart_start_point_name', $depart_start_point_name );
            add_variable( 'depart_routes', $depart_start_point_name . ' to ' . $depart_end_point_name );

            add_variable( 'return_id', $return_id );
            add_variable( 'return_css', $return_css );
            add_variable( 'return_date', $return_date );
            add_variable( 'return_passenger', $return_passenger );
            add_variable( 'return_end_point', $return_end_point );
            add_variable( 'return_start_point', $return_start_point );
            add_variable( 'return_end_point_name', $return_end_point_name );
            add_variable( 'return_start_point_name', $return_start_point_name );
            add_variable( 'return_routes', $return_start_point_name . ' to ' . $return_end_point_name );

            add_variable( 'return_check', $return_check );
            add_variable( 'one_way_check', $one_way_check );
            add_variable( 'return_point_css', $return_point_css );

            add_variable( 'depart_end_point_list', $depart_end_point_list );
            add_variable( 'return_end_point_list', $return_end_point_list );
            add_variable( 'depart_start_point_list', $depart_start_point_list );
            add_variable( 'return_start_point_list', $return_start_point_list );

            add_variable( 'depart_date_format', date( 'D j F y', strtotime( $depart_date ) ) );
            add_variable( 'return_date_format', date( 'D j F y', strtotime( $return_date ) ) );

            get_edit_trip_availability_result( $_POST, $data );
        }
        else
        {
            $result_class       = 'sr-only';
            $return_css         = 'sr-only';
            $depart_css         = 'sr-only';
            $depart_end_point   = '';
            $return_end_point   = '';
            $depart_start_point = '';
            $return_start_point = '';
            $return_date        = '';
            $depart_date        = '';
            $depart_id          = '';
            $return_id          = '';
            $return_css         = '';
            $num_adult          = 0;
            $num_child          = 0;
            $num_infant         = 0;

            foreach( $detail as $obj )
            {
                foreach( $obj as $d )
                {
                    if( $d['bdtype'] == 'departure' )
                    {
                        $depart_date        = date( 'd F Y', strtotime( $d['bddate'] ) );
                        $depart_start_point = $d['bdfrom_id'];
                        $depart_end_point   = $d['bdto_id'];
                        $depart_id          = $d['bdid'];
                        $depart_css         = '';
                    }
                    elseif( $d['bdtype'] == 'return' )
                    {
                        $return_date        = date( 'd F Y', strtotime( $d['bddate'] ) );
                        $return_start_point = $d['bdfrom_id'];
                        $return_end_point   = $d['bdto_id'];
                        $return_id          = $d['bdid'];
                        $return_css         = '';
                    }

                    $num_adult  = $d['num_adult'];
                    $num_child  = $d['num_child'];
                    $num_infant = $d['num_infant'];
                }
            }

            if( isset( $btype ) && $btype == '0' )
            {
                add_variable( 'return_check', '' );
                add_variable( 'one_way_check', 'checked' );
                add_variable( 'return_point_css', 'disabled' );
            }
            else
            {
                add_variable( 'return_check', 'checked' );
                add_variable( 'one_way_check', '' );
                add_variable( 'return_point_css', '' );
            }

            $depart_end_point_list   = get_availibility_loc_option( null, $depart_end_point );
            $return_end_point_list   = get_availibility_loc_option( null, $return_end_point );
            $depart_start_point_list = get_availibility_loc_option( null, $depart_start_point );
            $return_start_point_list = get_availibility_loc_option( null, $return_start_point );
            $booking_source_list     = get_booking_source_option( $chid, $agid );

            add_variable( 'chid', $chid );
            add_variable( 'agid', $agid );
            add_variable( 'route_type', $btype );
            add_variable( 'result_class', $result_class );
            add_variable( 'booking_source_list', $booking_source_list );

            add_variable( 'adult_num', $num_adult );
            add_variable( 'child_num', $num_child );
            add_variable( 'infant_num', $num_infant );

            add_variable( 'adult_num_val', $num_adult );
            add_variable( 'child_num_val', $num_child );
            add_variable( 'infant_num_val', $num_infant );

            add_variable( 'depart_id', $depart_id );
            add_variable( 'depart_css', $depart_css );
            add_variable( 'depart_date', $depart_date );
            add_variable( 'depart_end_point', $depart_end_point );
            add_variable( 'depart_start_point', $depart_start_point );
            add_variable( 'depart_end_point_list', $depart_end_point_list );
            add_variable( 'depart_start_point_list', $depart_start_point_list );

            add_variable( 'return_id', $return_id );
            add_variable( 'return_css', $return_css );
            add_variable( 'return_date', $return_date );
            add_variable( 'return_end_point', $return_end_point );
            add_variable( 'return_start_point', $return_start_point );
            add_variable( 'return_end_point_list', $return_end_point_list );
            add_variable( 'return_start_point_list', $return_start_point_list );
        }

        add_variable( 'bstt', $bstt );
        add_variable( 'sagid', $sagid );
        add_variable( 'bcode', $bcode );
        add_variable( 'bdate', $bdate );
        add_variable( 'bbname', $bbname );
        add_variable( 'pmcode', $pmcode );
        add_variable( 'btotal', $btotal );
        add_variable( 'bcdate', $bcdate );
        add_variable( 'brcode', $brcode );
        add_variable( 'bbemail', $bbemail );
        add_variable( 'bbphone', $bbphone );
        add_variable( 'bticket', $bticket );
        add_variable( 'bremark', $bremark );
        add_variable( 'brefund', $brefund );
        add_variable( 'brcdate', $brcdate );
        add_variable( 'bstatus', $bstatus );
        add_variable( 'bbphone2', $bbphone2 );
        add_variable( 'bcreason', $bcreason );
        add_variable( 'bcmessage', $bcmessage );
        add_variable( 'bagremark', $bagremark );
        add_variable( 'bsubtotal', $bsubtotal );
        add_variable( 'bdiscount', $bdiscount );
        add_variable( 'bfeedback', $bfeedback );
        add_variable( 'bbcountry', $bbcountry );
        add_variable( 'bbrevtype', $bbrevtype );
        add_variable( 'bbrevagent', $bbrevagent );
        add_variable( 'bpaymethod', $bpaymethod );
        add_variable( 'bhotelname', $bhotelname );
        add_variable( 'bhotelphone', $bhotelphone );
        add_variable( 'bhotelemail', $bhotelemail );
        add_variable( 'bpvaliddate', $bpvaliddate );
        add_variable( 'bbrevstatus', $bbrevstatus );
        add_variable( 'bonhandtotal', $bonhandtotal );
        add_variable( 'bhoteladdress', $bhoteladdress );
        add_variable( 'freelancecode', $freelancecode );
        add_variable( 'bblockingtime', $bblockingtime );
        add_variable( 'bbooking_staf', $bbooking_staf );
        add_variable( 'btransactionfee', $btransactionfee );
        add_variable( 'bcancellationfee', $bcancellationfee );
        add_variable( 'bblockingtime_sts_48', $bblockingtime_sts_48 );
        add_variable( 'bblockingtime_sts_24', $bblockingtime_sts_24 );
        add_variable( 'bprintboardingstatus', $bprintboardingstatus );

        add_variable( 'body_class', 'white' );
        add_variable( 'source_css', in_array( $_COOKIE['user_type'], array( 1, 6, 7, 8 ) ) ? '' : 'sr-only' );
        add_variable( 'cancel_link', get_state_url( 'reservation&sub=booking&prc=edit&id=' . $bid . $filter ) );
        add_variable( 'revise_trip_link', get_state_url( 'reservation&sub=booking&prc=revise&revise=trip&id=' . $bid . $filter ) );
        add_variable( 'revise_booking_link', get_state_url( 'reservation&sub=booking&prc=revise&revise=booking&id=' . $bid . $filter ) );
        add_variable( 'revise_passenger_link', get_state_url( 'reservation&sub=booking&prc=revise&revise=passenger&id=' . $bid . $filter ) );

        add_actions( 'section_title', 'Revise Booking' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/bootstrap-toggle.min.css' );

        add_actions( 'other_elements', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/bootstrap-toggle.min.js' );
        add_actions( 'other_elements', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/numeral.min.js?v=' . TICKET_VERSION );
        add_actions( 'other_elements', 'get_custom_javascript', 'https://cdn.jsdelivr.net/npm/autonumeric@1.9.46' );

        parse_template( 'revise-block', 'rv-block' );

        return return_template( 'revise' );
    }
}

function ticket_booking_revise_trip( $data, $bid )
{
    if( isset( $_POST['revise_trip_data'] ) )
    {
        $new_bid = ticket_booking_revise_trip_data( $_POST, $data, $bid );

        if( !empty( $new_bid ) )
        {
            header( 'Location:' . get_state_url( 'reservation&sub=booking&prc=edit&id=' . $new_bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );

            exit;
        }
    }
    else
    {
        extract( $data );

        set_template( PLUGINS_PATH . '/ticket/tpl/booking/revise-trip.html', 'revise' );

        add_block( 'no-departure-trip-loop-block', 'ndtrl-block', 'revise' );
        add_block( 'no-return-trip-loop-block', 'nrtrl-block', 'revise' );
        add_block( 'departure-trip-loop-block', 'dtrl-block', 'revise' );
        add_block( 'return-trip-loop-block', 'rtrl-block', 'revise' );
        add_block( 'departure-trip-block', 'dtr-block', 'revise' );
        add_block( 'return-trip-block', 'rtr-block', 'revise' );
        add_block( 'revise-block', 'rv-block', 'revise' );

        if( isset( $_POST['check_trip_availability'] ) )
        {
            extract( $_POST );

            $depart_start_point = isset( $depart_start_point ) ? $depart_start_point : '';
            $return_start_point = isset( $return_start_point ) ? $return_start_point : '';
            $depart_end_point   = isset( $depart_end_point ) ? $depart_end_point : '';
            $return_end_point   = isset( $return_end_point ) ? $return_end_point : '';
            $bsource            = isset( $booking_source ) ? $booking_source : '';
            $return_date        = isset( $return_date ) ? $return_date : '';
            $depart_date        = isset( $depart_date ) ? $depart_date : '';

            $depart_end_point_name   = get_location( $depart_end_point, 'lcname' );
            $depart_start_point_name = get_location( $depart_start_point, 'lcname' );
            $return_end_point_name   = get_location( $return_end_point, 'lcname' );
            $return_start_point_name = get_location( $return_start_point, 'lcname' );

            if( isset( $bdidval ) && !empty( $bdidval ) )
            {
                list( $bdid, $bdtype ) = explode( '|', $bdidval );

                $bpstatus_depart    = isset( $data['detail']['departure'][$bdid]['bdpstatus'] ) ? $data['detail']['departure'][$bdid]['bdpstatus'] : '';
                $bpstatus_return    = isset( $data['detail']['return'][$bdid]['bdpstatus'] ) ? $data['detail']['return'][$bdid]['bdpstatus'] : '';

                if( $bdtype == 'departure' )
                {
                    $return_point_css = 'disabled';
                    $return_css       = 'sr-only';
                    $return_passenger = '';

                    $depart_css       = '';
                    $depart_point_css = '';
                    $depart_passenger = get_passenger_list_edit_content( $bdid, $bpstatus_depart, 'departure', $adult, $child, $infant, 'trip' );
                }
                else
                {
                    $return_point_css = '';
                    $return_css       = '';
                    $return_passenger = get_passenger_list_edit_content( $bdid, $bpstatus_return, 'return', $adult, $child, $infant, 'trip' );

                    $depart_point_css = 'disabled';
                    $depart_css       = 'sr-only';
                    $depart_passenger = '';
                }
            }

            $arr = explode( '|', $bsource );

            if( count( $arr ) == 2 )
            {
                $chid = $arr[0];
                $agid = $arr[1];
            }
            else
            {
                $chid = $arr[0];
                $agid = null;
            }

            $depart_end_point_list   = get_availibility_loc_option( null, $depart_end_point );
            $return_end_point_list   = get_availibility_loc_option( null, $return_end_point );
            $depart_start_point_list = get_availibility_loc_option( null, $depart_start_point );
            $return_start_point_list = get_availibility_loc_option( null, $return_start_point );

            $booking_source_list     = get_booking_source_option( $chid, $agid );
            $trip_detail_list        = get_trip_detail_option( $detail, $bdidval );

            add_variable( 'chid', $chid );
            add_variable( 'agid', $agid );
            add_variable( 'bdidval', $bdidval );
            add_variable( 'route_type', $btype );

            add_variable( 'adult_num', $adult );
            add_variable( 'child_num', $child );
            add_variable( 'infant_num', $infant );

            add_variable( 'adult_num_val', $num_adult );
            add_variable( 'child_num_val', $num_child );
            add_variable( 'infant_num_val', $num_infant );

            add_variable( 'trip_detail_list', $trip_detail_list );
            add_variable( 'booking_source_list', $booking_source_list );

            add_variable( 'depart_css', $depart_css );
            add_variable( 'depart_date', $depart_date );
            add_variable( 'depart_point_css', $depart_point_css );
            add_variable( 'depart_passenger', $depart_passenger );
            add_variable( 'depart_end_point', $depart_end_point );
            add_variable( 'depart_start_point', $depart_start_point );
            add_variable( 'depart_end_point_name', $depart_end_point_name );
            add_variable( 'depart_start_point_name', $depart_start_point_name );
            add_variable( 'depart_routes', $depart_start_point_name . ' to ' . $depart_end_point_name );

            add_variable( 'return_css', $return_css );
            add_variable( 'return_date', $return_date );
            add_variable( 'return_point_css', $return_point_css );
            add_variable( 'return_passenger', $return_passenger );
            add_variable( 'return_end_point', $return_end_point );
            add_variable( 'return_start_point', $return_start_point );
            add_variable( 'return_end_point_name', $return_end_point_name );
            add_variable( 'return_start_point_name', $return_start_point_name );
            add_variable( 'return_routes', $return_start_point_name . ' to ' . $return_end_point_name );

            add_variable( 'depart_end_point_list', $depart_end_point_list );
            add_variable( 'return_end_point_list', $return_end_point_list );
            add_variable( 'depart_start_point_list', $depart_start_point_list );
            add_variable( 'return_start_point_list', $return_start_point_list );

            add_variable( 'depart_date_format', date( 'D j F y', strtotime( $depart_date ) ) );
            add_variable( 'return_date_format', date( 'D j F y', strtotime( $return_date ) ) );

            get_edit_trip_availability_result( $_POST, $data );
        }
        else
        {
            $result_class       = 'sr-only';
            $return_css         = 'sr-only';
            $depart_css         = 'sr-only';
            $depart_point_css   = 'disabled';
            $return_point_css   = 'disabled';
            $num_pass_css       = 'disabled';
            $bsource_css        = 'disabled';
            $button_css         = 'disabled';
            $depart_end_point   = '';
            $return_end_point   = '';
            $depart_start_point = '';
            $return_start_point = '';
            $return_date        = '';
            $depart_date        = '';
            $depart_id          = '';
            $return_id          = '';
            $return_css         = '';
            $num_adult          = 0;
            $num_child          = 0;
            $num_infant         = 0;

            $depart_end_point_list   = get_availibility_loc_option( null );
            $return_end_point_list   = get_availibility_loc_option( null );
            $depart_start_point_list = get_availibility_loc_option( null );
            $return_start_point_list = get_availibility_loc_option( null );

            $booking_source_list     = get_booking_source_option( $chid, $agid );
            $trip_detail_list        = get_trip_detail_option( $detail );

            add_variable( 'chid', $chid );
            add_variable( 'agid', $agid );
            add_variable( 'route_type', $btype );
            add_variable( 'result_class', $result_class );
            add_variable( 'trip_detail_list', $trip_detail_list );
            add_variable( 'booking_source_list', $booking_source_list );

            add_variable( 'adult_num', $num_adult );
            add_variable( 'child_num', $num_child );
            add_variable( 'infant_num', $num_infant );

            add_variable( 'adult_num_val', $num_adult );
            add_variable( 'child_num_val', $num_child );
            add_variable( 'infant_num_val', $num_infant );

            add_variable( 'button_css', $button_css );
            add_variable( 'bsource_css', $bsource_css );
            add_variable( 'num_pass_css', $num_pass_css );

            add_variable( 'depart_id', $depart_id );
            add_variable( 'depart_css', $depart_css );
            add_variable( 'depart_date', $depart_date );
            add_variable( 'depart_point_css', $depart_point_css );
            add_variable( 'depart_end_point', $depart_end_point );
            add_variable( 'depart_start_point', $depart_start_point );
            add_variable( 'depart_end_point_list', $depart_end_point_list );
            add_variable( 'depart_start_point_list', $depart_start_point_list );

            add_variable( 'return_id', $return_id );
            add_variable( 'return_css', $return_css );
            add_variable( 'return_date', $return_date );
            add_variable( 'return_point_css', $depart_point_css );
            add_variable( 'return_end_point', $return_end_point );
            add_variable( 'return_start_point', $return_start_point );
            add_variable( 'return_end_point_list', $return_end_point_list );
            add_variable( 'return_start_point_list', $return_start_point_list );
        }

        add_variable( 'bstt', $bstt );
        add_variable( 'sagid', $sagid );
        add_variable( 'bcode', $bcode );
        add_variable( 'bdate', $bdate );
        add_variable( 'bbname', $bbname );
        add_variable( 'pmcode', $pmcode );
        add_variable( 'btotal', $btotal );
        add_variable( 'bcdate', $bcdate );
        add_variable( 'brcode', $brcode );
        add_variable( 'bbemail', $bbemail );
        add_variable( 'bbphone', $bbphone );
        add_variable( 'bticket', $bticket );
        add_variable( 'bremark', $bremark );
        add_variable( 'brefund', $brefund );
        add_variable( 'brcdate', $brcdate );
        add_variable( 'bstatus', $bstatus );
        add_variable( 'bbphone2', $bbphone2 );
        add_variable( 'bcreason', $bcreason );
        add_variable( 'bcmessage', $bcmessage );
        add_variable( 'bagremark', $bagremark );
        add_variable( 'bsubtotal', $bsubtotal );
        add_variable( 'bdiscount', $bdiscount );
        add_variable( 'bfeedback', $bfeedback );
        add_variable( 'bbcountry', $bbcountry );
        add_variable( 'bbrevtype', $bbrevtype );
        add_variable( 'bbrevagent', $bbrevagent );
        add_variable( 'bpaymethod', $bpaymethod );
        add_variable( 'bhotelname', $bhotelname );
        add_variable( 'bhotelphone', $bhotelphone );
        add_variable( 'bhotelemail', $bhotelemail );
        add_variable( 'bpvaliddate', $bpvaliddate );
        add_variable( 'bbrevstatus', $bbrevstatus );
        add_variable( 'bonhandtotal', $bonhandtotal );
        add_variable( 'bhoteladdress', $bhoteladdress );
        add_variable( 'freelancecode', $freelancecode );
        add_variable( 'bblockingtime', $bblockingtime );
        add_variable( 'bbooking_staf', $bbooking_staf );
        add_variable( 'btransactionfee', $btransactionfee );
        add_variable( 'bcancellationfee', $bcancellationfee );
        add_variable( 'bblockingtime_sts_48', $bblockingtime_sts_48 );
        add_variable( 'bblockingtime_sts_24', $bblockingtime_sts_24 );
        add_variable( 'bprintboardingstatus', $bprintboardingstatus );

        add_variable( 'body_class', 'white' );
        add_variable( 'source_css', in_array( $_COOKIE['user_type'], array( 1, 6, 7, 8 ) ) ? '' : 'sr-only' );
        add_variable( 'cancel_link', get_state_url( 'reservation&sub=booking&prc=edit&id=' . $bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );
        add_variable( 'revise_trip_link', get_state_url( 'reservation&sub=booking&prc=revise&revise=trip&id=' . $bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );
        add_variable( 'revise_booking_link', get_state_url( 'reservation&sub=booking&prc=revise&revise=booking&id=' . $bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );
        add_variable( 'revise_passenger_link', get_state_url( 'reservation&sub=booking&prc=revise&revise=passenger&id=' . $bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );

        add_actions( 'section_title', 'Revise Trip' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        parse_template( 'revise-block', 'rv-block' );

        return return_template( 'revise' );
    }
}

function ticket_booking_revise_passenger( $data, $bid )
{
    if( isset( $_POST['revise_passenger_data'] ) )
    {
        if( ticket_booking_revise_passanger_data( $_POST, $data, $bid ) )
        {
            $prm = base64_encode(
                json_encode(
                    array(
                        'sbooking'   => $data['bticket'],
                        'bstatus'    => array( 'pa', 'ca', 'pp' ),
                        'date_start' => '',
                        'date_end'   => '',
                        'bbemail'    => '',
                        'lcid2'      => '',
                        'bdate'      => '',
                        'lcid'       => '',
                        'chid'       => '',
                        'rid'        => '',
                    )
                )
            );

            header( 'Location:' . get_state_url( 'reservation&sub=booking&prm=' . $prm ) );

            exit;
        }
    }
    else
    {
        extract( $data );

        set_template( PLUGINS_PATH . '/ticket/tpl/booking/revise-passenger.html', 'revise' );

        add_block( 'departure-trip-passenger-block', 'dtrp-block', 'revise' );
        add_block( 'return-trip-passenger-block', 'rtrp-block', 'revise' );
        add_block( 'no-departure-trip-loop-block', 'ndtrl-block', 'revise' );
        add_block( 'no-return-trip-loop-block', 'nrtrl-block', 'revise' );
        add_block( 'departure-trip-loop-block', 'dtrl-block', 'revise' );
        add_block( 'return-trip-loop-block', 'rtrl-block', 'revise' );
        add_block( 'departure-trip-block', 'dtr-block', 'revise' );
        add_block( 'return-trip-block', 'rtr-block', 'revise' );
        add_block( 'revise-block', 'rv-block', 'revise' );

        if( isset( $_POST['check_trip_availability'] ) )
        {
            extract( $_POST );

            //-- Explode Booking Source
            $arr = explode( '|', $booking_source );

            if( count( $arr ) == 2 )
            {
                $chid = $arr[0];
                $agid = $arr[1];
            }
            else
            {
                $chid = $arr[0];
                $agid = null;
            }

            //-- Get Revise Passenger Data
            $dp_point = 0;
            $rt_point = 0;

            $depart_id = '';
            $return_id = '';

            if( !empty( $bdid ) )
            {
                foreach( $bdid as $bdtype => $val )
                {
                    if( $bdtype == 'departure' )
                    {
                        $depart_id = $val;

                        $dp_point++;
                    }
                    else if( $bdtype == 'return' )
                    {
                        $return_id = $val;

                        $rt_point++;
                    }
                }
            }

            $result_class       = '';
            $return_css         = '';
            $depart_css         = '';
            $bsource_css        = '';
            $button_css         = '';
            $depart_end_point   = isset( $depart_end_point ) ? $depart_end_point : '';
            $return_end_point   = isset( $return_end_point ) ? $return_end_point : '';
            $depart_start_point = isset( $depart_start_point ) ? $depart_start_point : '';
            $return_start_point = isset( $return_start_point ) ? $return_start_point : '';
            $return_date        = isset( $return_date ) ? $return_date : '';
            $depart_date        = isset( $depart_date ) ? $depart_date : '';
            $depart_point_css   = $dp_point > 0 ? '' : 'disabled';
            $return_point_css   = $rt_point > 0 ? '' : 'disabled';

            $depart_bdid_list        = get_revise_trip_detail_option( $detail, 'departure', $bdid );
            $return_bdid_list        = get_revise_trip_detail_option( $detail, 'return', $bdid );
            $depart_bpid_list        = get_bpid_list( $detail, 'departure', $bpid );
            $return_bpid_list        = get_bpid_list( $detail, 'return', $bpid );
            $booking_source_list     = get_booking_source_option( $chid, $agid );
            $depart_end_point_list   = get_availibility_loc_option( null, $depart_end_point );
            $return_end_point_list   = get_availibility_loc_option( null, $return_end_point );
            $depart_start_point_list = get_availibility_loc_option( null, $depart_start_point );
            $return_start_point_list = get_availibility_loc_option( null, $return_start_point );

            $depart_end_point_name   = get_location( $depart_end_point, 'lcname' );
            $depart_start_point_name = get_location( $depart_start_point, 'lcname' );
            $return_end_point_name   = get_location( $return_end_point, 'lcname' );
            $return_start_point_name = get_location( $return_start_point, 'lcname' );

            add_variable( 'chid', $chid );
            add_variable( 'agid', $agid );
            add_variable( 'route_type', $btype );
            add_variable( 'result_class', $result_class );
            add_variable( 'booking_source_list', $booking_source_list );

            add_variable( 'button_css', $button_css );
            add_variable( 'bsource_css', $bsource_css );

            add_variable( 'depart_id', $depart_id );
            add_variable( 'depart_css', $depart_css );
            add_variable( 'depart_date', $depart_date );
            add_variable( 'depart_point_css', $depart_point_css );
            add_variable( 'depart_bpid_list', $depart_bpid_list );
            add_variable( 'depart_bdid_list', $depart_bdid_list );
            add_variable( 'depart_end_point', $depart_end_point );
            add_variable( 'depart_start_point', $depart_start_point );
            add_variable( 'depart_end_point_list', $depart_end_point_list );
            add_variable( 'depart_end_point_name', $depart_end_point_name );
            add_variable( 'depart_start_point_list', $depart_start_point_list );
            add_variable( 'depart_start_point_name', $depart_start_point_name );

            add_variable( 'return_id', $return_id );
            add_variable( 'return_css', $return_css );
            add_variable( 'return_date', $return_date );
            add_variable( 'return_point_css', $return_point_css );
            add_variable( 'return_bpid_list', $return_bpid_list );
            add_variable( 'return_bdid_list', $return_bdid_list );
            add_variable( 'return_end_point', $return_end_point );
            add_variable( 'return_start_point', $return_start_point );
            add_variable( 'return_end_point_list', $return_end_point_list );
            add_variable( 'return_end_point_name', $return_end_point_name );
            add_variable( 'return_start_point_list', $return_start_point_list );
            add_variable( 'return_start_point_name', $return_start_point_name );

            add_variable( 'depart_routes', $depart_start_point_name . ' to ' . $depart_end_point_name );
            add_variable( 'return_routes', $return_start_point_name . ' to ' . $return_end_point_name );

            add_variable( 'depart_date_format', date( 'D j F y', strtotime( $depart_date ) ) );
            add_variable( 'return_date_format', date( 'D j F y', strtotime( $return_date ) ) );

            get_edit_passenger_availability_result( $_POST, $data );
        }
        else
        {
            $result_class       = 'sr-only';
            $return_css         = 'sr-only';
            $depart_css         = 'sr-only';
            $bsource_css        = 'disabled';
            $button_css         = 'disabled';
            $depart_point_css   = 'disabled';
            $return_point_css   = 'disabled';
            $depart_end_point   = '';
            $return_end_point   = '';
            $depart_start_point = '';
            $return_start_point = '';
            $return_date        = '';
            $depart_date        = '';
            $num_adult          = 0;
            $num_child          = 0;
            $num_infant         = 0;

            $depart_bdid_list        = get_revise_trip_detail_option( $detail, 'departure' );
            $return_bdid_list        = get_revise_trip_detail_option( $detail, 'return' );
            $booking_source_list     = get_booking_source_option( $chid, $agid );
            $depart_bpid_list        = get_bpid_list( $detail, 'departure' );
            $return_bpid_list        = get_bpid_list( $detail, 'return' );
            $depart_end_point_list   = get_availibility_loc_option( null );
            $return_end_point_list   = get_availibility_loc_option( null );
            $depart_start_point_list = get_availibility_loc_option( null );
            $return_start_point_list = get_availibility_loc_option( null );

            add_variable( 'chid', $chid );
            add_variable( 'agid', $agid );
            add_variable( 'route_type', $btype );
            add_variable( 'result_class', $result_class );
            add_variable( 'booking_source_list', $booking_source_list );

            add_variable( 'adult_num', $num_adult );
            add_variable( 'child_num', $num_child );
            add_variable( 'infant_num', $num_infant );

            add_variable( 'button_css', $button_css );
            add_variable( 'bsource_css', $bsource_css );

            add_variable( 'depart_css', $depart_css );
            add_variable( 'depart_date', $depart_date );
            add_variable( 'depart_point_css', $depart_point_css );
            add_variable( 'depart_bpid_list', $depart_bpid_list );
            add_variable( 'depart_bdid_list', $depart_bdid_list );
            add_variable( 'depart_end_point', $depart_end_point );
            add_variable( 'depart_start_point', $depart_start_point );
            add_variable( 'depart_end_point_list', $depart_end_point_list );
            add_variable( 'depart_start_point_list', $depart_start_point_list );

            add_variable( 'return_css', $return_css );
            add_variable( 'return_date', $return_date );
            add_variable( 'return_point_css', $depart_point_css );
            add_variable( 'return_bpid_list', $return_bpid_list );
            add_variable( 'return_bdid_list', $return_bdid_list );
            add_variable( 'return_end_point', $return_end_point );
            add_variable( 'return_start_point', $return_start_point );
            add_variable( 'return_end_point_list', $return_end_point_list );
            add_variable( 'return_start_point_list', $return_start_point_list );
        }

        add_variable( 'bstt', $bstt );
        add_variable( 'sagid', $sagid );
        add_variable( 'bcode', $bcode );
        add_variable( 'bdate', $bdate );
        add_variable( 'bbname', $bbname );
        add_variable( 'pmcode', $pmcode );
        add_variable( 'btotal', $btotal );
        add_variable( 'bcdate', $bcdate );
        add_variable( 'brcode', $brcode );
        add_variable( 'bbemail', $bbemail );
        add_variable( 'bbphone', $bbphone );
        add_variable( 'bticket', $bticket );
        add_variable( 'bremark', $bremark );
        add_variable( 'brefund', $brefund );
        add_variable( 'brcdate', $brcdate );
        add_variable( 'bstatus', $bstatus );
        add_variable( 'bbphone2', $bbphone2 );
        add_variable( 'bcreason', $bcreason );
        add_variable( 'bcmessage', $bcmessage );
        add_variable( 'bagremark', $bagremark );
        add_variable( 'bsubtotal', $bsubtotal );
        add_variable( 'bdiscount', $bdiscount );
        add_variable( 'bfeedback', $bfeedback );
        add_variable( 'bbcountry', $bbcountry );
        add_variable( 'bbrevtype', $bbrevtype );
        add_variable( 'bbrevagent', $bbrevagent );
        add_variable( 'bpaymethod', $bpaymethod );
        add_variable( 'bhotelname', $bhotelname );
        add_variable( 'bhotelphone', $bhotelphone );
        add_variable( 'bhotelemail', $bhotelemail );
        add_variable( 'bpvaliddate', $bpvaliddate );
        add_variable( 'bbrevstatus', $bbrevstatus );
        add_variable( 'bonhandtotal', $bonhandtotal );
        add_variable( 'bhoteladdress', $bhoteladdress );
        add_variable( 'freelancecode', $freelancecode );
        add_variable( 'bblockingtime', $bblockingtime );
        add_variable( 'bbooking_staf', $bbooking_staf );
        add_variable( 'btransactionfee', $btransactionfee );
        add_variable( 'bcancellationfee', $bcancellationfee );
        add_variable( 'bblockingtime_sts_48', $bblockingtime_sts_48 );
        add_variable( 'bblockingtime_sts_24', $bblockingtime_sts_24 );
        add_variable( 'bprintboardingstatus', $bprintboardingstatus );

        add_variable( 'body_class', 'white' );
        add_variable( 'source_css', in_array( $_COOKIE['user_type'], array( 1, 6, 7, 8 ) ) ? '' : 'sr-only' );
        add_variable( 'cancel_link', get_state_url( 'reservation&sub=booking&prc=edit&id=' . $bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );
        add_variable( 'revise_trip_link', get_state_url( 'reservation&sub=booking&prc=revise&revise=trip&id=' . $bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );
        add_variable( 'revise_booking_link', get_state_url( 'reservation&sub=booking&prc=revise&revise=booking&id=' . $bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );
        add_variable( 'revise_passenger_link', get_state_url( 'reservation&sub=booking&prc=revise&revise=passenger&id=' . $bid . ( isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '' ) ) );

        add_actions( 'section_title', 'Revise Passenger' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        parse_template( 'revise-block', 'rv-block', false );

        return return_template( 'revise' );
    }
}

function get_bpid_list( $detail, $type, $bpid = array() )
{
    $option = '';
    $bpid   = isset( $bpid[ $type ] ) ? $bpid[ $type ] : $bpid;

    if( isset( $detail[ $type ] ) && !empty( $detail[ $type ] ) )
    {
        foreach( $detail[ $type ] as $dt )
        {
            $option .= '
            <optgroup label="' . $dt['rname'] . ' --- ' . date( 'd F Y', strtotime( $dt['bddate'] ) ) . '">';

                foreach( $dt['passenger'] as $bptype => $obj )
                {
                    foreach( $obj as $d )
                    {
                        $option .= '
                        <option data-bdid="' . $d['bdid'] . '" data-bdtype="' . $type . '" value="' . $d['bpid'] . '|' . $bptype . '" ' . ( in_array( $d['bpid'] . '|' . $bptype , $bpid ) ? 'selected' : '' ) . '>
                            ' . $d['bpname'] . '
                        </option>';
                    }
                }

                $option .= '
            </optgroup>';
        }
    }

    return $option;
}

function get_revise_passanger_id_and_type( $bpid = array() )
{
    $bpid_arr  = array();
    $passenger = array(
        'departure' => array( 'adult' => 0, 'child' => 0, 'infant' => 0 ),
        'return'    => array( 'adult' => 0, 'child' => 0, 'infant' => 0 )
    );

    if( !empty( $bpid ) )
    {
        foreach( $bpid as $bdtype => $obj )
        {
            foreach( $obj as $str )
            {
                list( $id, $bptype ) = explode( '|', $str );

                $bpid_arr[ $bdtype ][] = $id;

                if( isset( $passenger[ $bdtype ][ $bptype ] ) )
                {
                    $passenger[ $bdtype ][ $bptype ] += 1;
                }
                else
                {
                    $passenger[ $bdtype ][ $bptype ] = 1;
                }
            }
        }
    }

    return array( 'bpid' => $bpid_arr, 'passenger' => $passenger );
}

function get_passenger_list_revise_content( $bdid, $btype, $bpid )
{
    global $db;

    $content = '';

    if( isset( $bpid[ $btype ] ) && !empty( $bpid[ $btype ] ) )
    {
        $i = 0;
        $a = 0;
        $c = 0;

        foreach( $bpid[ $btype ] as $id )
        {
            //-- Passanger
            $s = 'SELECT * FROM ticket_booking_passenger AS a WHERE a.bpid = %d';
            $q = $db->prepare_query( $s, $id );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
                $d = $db->fetch_array( $r );

                $idx = $d['bptype'] == 'adult' ? $a : ( $d['bptype'] == 'child' ? $c : $i );

                $content .= '
                <tr>
                    <td>
                        <span class="small-title">' . ucfirst( $d['bptype'] ) . ' ' . ( $idx + 1 ) . '</span>
                        <input type="text" class="bpid text form-control sr-only" value="' . $d['bpid'] . '" name="passenger[' . $btype . '][' . $d['bptype'] . '][' . $idx . '][bpid]">
                        <input type="text" class="bpid text form-control sr-only" value="' . $d['bdid'] . '" name="passenger[' . $btype . '][' . $d['bptype'] . '][' . $idx . '][bdid]">
                        <input type="text" class="bpid text form-control sr-only" value="' . $d['bptype'] . '" name="passenger[' . $btype . '][' . $d['bptype'] . '][' . $idx . '][bptype]">
                        <input type="text" class="bpname text form-control" value="' . $d['bpname'] . '" name="passenger[' . $btype . '][' . $d['bptype'] . '][' . $idx . '][bpname]" required>
                    </td>
                    <td>
                        <span class="small-title">Gender</span>
                        <div>
                            <select class="bpgender select-option form-control" name="passenger[' . $btype . '][' . $d['bptype'] . '][' . $idx . '][bpgender]" autocomplete="off">
                                ' . get_gender_option( $d['bpgender'], false ) . '
                            </select>
                        </div>
                    </td>
                    <td>
                        <span class="small-title">Date Of Birth</span>
                        <input type="text" class="bpbirthdate text form-control txt-date-' . $d['bptype'] . '" value="' . ( $d['bpbirthdate'] == '0000-00-00' ? '' : date( 'd F Y', strtotime( $d['bpbirthdate'] ) ) ) . '" name="passenger[' . $btype . '][' . $d['bptype'] . '][' . $idx . '][bpbirthdate]" readonly>
                    </td>
                    <td width="150">
                        <span class="small-title">Nationality</span>
                        <div>
                            <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][' . $d['bptype'] . '][' . $idx . '][lcountry_id]" autocomplete="off">
                                ' . get_country_list_option( $d['lcountry_id'] ) . '
                            </select>
                        </div>
                    </td>
                </tr>';

                if( $d['bptype'] == 'adult' )
                {
                    $a++;
                }
                else if( $d['bptype'] == 'child' )
                {
                    $c++;
                }
                else
                {
                    $i++;
                }
            }
        }
    }

    return $content;
}

function get_passenger_list_edit_content( $bdid, $bdpstatus, $btype, $adult = 0, $child = 0, $infant = 0, $revise_type = 'booking' )
{
    $content   = '';
    $passenger = ticket_passenger_list( $bdid, true );
    $disabled  = $bdpstatus == 'pa' ? 'disabled' : '';

    if( !empty( $adult ) )
    {
        $n = 1;

        if ( isset( $passenger['adult'] ) )
        {
            foreach ( $passenger['adult'] as $i => $data )
            {
                if ( $passenger['adult'][$i]['bpstatus'] != 'cn' )
                {
                    $bpname      = isset( $passenger['adult'][$i]['bpname'] ) ? $passenger['adult'][$i]['bpname'] : '';
                    $bpgender    = isset( $passenger['adult'][$i]['bpgender'] ) ? $passenger['adult'][$i]['bpgender'] : 1;
                    $lcountry_id = isset( $passenger['adult'][$i]['lcountry_id'] ) ? $passenger['adult'][$i]['lcountry_id'] : '';
                    $bpbirthdate = isset( $passenger['adult'][$i]['bpbirthdate'] ) ? ( $passenger['adult'][$i]['bpbirthdate'] == '0000-00-00' ? '' : date( 'd F Y', strtotime( $passenger['adult'][$i]['bpbirthdate'] ) ) ) : '';

                    $bpstatus         = isset( $passenger['adult'][$i]['bpstatus'] ) ? $passenger['adult'][$i]['bpstatus'] : '';
                    $bprefund         = isset( $passenger['adult'][$i]['bprefund'] ) ? $passenger['adult'][$i]['bprefund'] : '';
                    $bpcdate          = isset( $passenger['adult'][$i]['bpcdate'] ) ? $passenger['adult'][$i]['bpcdate'] : '';
                    $bpcancelfee      = isset( $passenger['adult'][$i]['bpcancelfee'] ) ? $passenger['adult'][$i]['bpcancelfee'] : '';
                    $bppercentfee     = isset( $passenger['adult'][$i]['bppercentfee'] ) ? $passenger['adult'][$i]['bppercentfee'] : '';
                    $bptransactionfee = isset( $passenger['adult'][$i]['bptransactionfee'] ) ? $passenger['adult'][$i]['bptransactionfee'] : '';
                    $cheked_status    = $bpstatus == 'aa' ? 'checked' : '';

                    $content .= '
                    <tr>
                        <td>
                            <span class="small-title">Adult ' . ( $n ) . '</span>
                            <input type="text" class="bpname text form-control" value="' . $bpname . '" name="passenger[' . $btype . '][adult][' . ( $n ) . '][bpname]" required>
                            <div class="sr-only">
                                <input type="text" class="text form-control" value="' . $bpcdate . '" name="passenger[' . $btype . '][adult][' . ( $n ) . '][bpcdate]">
                                <input type="text" class="text form-control" value="' . $bpstatus . '" name="passenger[' . $btype . '][adult][' . ( $n ) . '][bpstatus]">
                                <input type="text" class="text form-control" value="' . $bprefund . '" name="passenger[' . $btype . '][adult][' . ( $n ) . '][bprefund]">
                                <input type="text" class="text form-control" value="' . $bpcancelfee . '" name="passenger[' . $btype . '][adult][' . ( $n ) . '][bpcancelfee]">
                                <input type="text" class="text form-control" value="' . $bppercentfee . '" name="passenger[' . $btype . '][adult][' . ( $n ) . '][bppercentfee]">
                                <input type="text" class="text form-control" value="' . $bptransactionfee . '" name="passenger[' . $btype . '][adult][' . ( $n ) . '][bptransactionfee]">
                            </div>
                        </td>
                        <td>
                            <span class="small-title">Gender</span>
                            <div>
                                <select class="bpgender select-option form-control" name="passenger[' . $btype . '][adult][' . ( $n ) . '][bpgender]" autocomplete="off">
                                    ' . get_gender_option( $bpgender, false ) . '
                                </select>
                            </div>
                        </td>
                        <td>
                            <span class="small-title">Date Of Birth</span>
                            <input type="text" class="bpbirthdate text form-control txt-date-adult" value="' . $bpbirthdate . '" name="passenger[' . $btype . '][adult][' . ( $n ) . '][bpbirthdate]" readonly>
                        </td>
                        <td width="150">
                            <span class="small-title">Nationality</span>
                            <div>
                                <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][adult][' . ( $n ) . '][lcountry_id]" autocomplete="off">
                                    ' . get_country_list_option( $lcountry_id ) . '
                                </select>
                            </div>
                        </td>';

                        if ( $revise_type == 'booking' )
                        {
                            $content .=
                            '<td width="150">
                                <span class="small-title">Status</span>
                                <input type="checkbox" class="status_passanger" data-btype="' . $btype . '" data-netprice="{inc_adult_price}" data-sellprice="{}"  data-bptype="adult" data-toggle="toggle" data-width="100%" data-onstyle="primary" data-offstyle="danger" data-on="Active" data-off="Removed" ' . $cheked_status . ' name="passenger[' . $btype . '][adult][' . ( $n ) . '][bpstatus]">
                            </td>';
                        }

                    $content .=
                    '</tr>';

                    $adult--;
                    $n++;
                }
            }
        }

        if ( $adult > 0 )
        {
            for( $i = 0; $i < $adult; $i++ )
            {
                $content .= '
                <tr>
                    <td>
                        <span class="small-title">Adult ' . $n . '</span>
                        <input type="text" class="bpname text form-control" value="" name="passenger[' . $btype . '][adult][' . $n . '][bpname]" required>
                        <div class="sr-only">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][adult][' . $n . '][bpcdate]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][adult][' . $n . '][bpstatus]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][adult][' . $n . '][bprefund]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][adult][' . $n . '][bpcancelfee]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][adult][' . $n . '][bppercentfee]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][adult][' . $n . '][bptransactionfee]">
                        </div>
                    </td>
                    <td>
                        <span class="small-title">Gender</span>
                        <div>
                            <select class="bpgender select-option form-control" name="passenger[' . $btype . '][adult][' . $n . '][bpgender]" autocomplete="off">
                                ' . get_gender_option( 1, false ) . '
                            </select>
                        </div>
                    </td>
                    <td>
                        <span class="small-title">Date Of Birth</span>
                        <input type="text" class="bpbirthdate text form-control txt-date-adult" value="" name="passenger[' . $btype . '][adult][' . $n . '][bpbirthdate]" readonly>
                    </td>
                    <td width="150">
                        <span class="small-title">Nationality</span>
                        <div>
                            <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][adult][' . $n . '][lcountry_id]" autocomplete="off">
                                ' . get_country_list_option( '' ) . '
                            </select>
                        </div>
                    </td>';

                    if ( $revise_type == 'booking' )
                    {
                        $content .=
                        '<td width="150">
                            <span class="small-title">Status</span>
                            <input type="checkbox" class="status_passanger" data-btype="' . $btype . '" data-price="{inc_adult_price}" data-bptype="adult" data-toggle="toggle" data-width="100%" data-onstyle="primary" data-offstyle="danger" data-on="Active" data-off="Removed" checked name="passenger[' . $btype . '][adult][' . $n . '][bpstatus]">
                        </td>';
                    }

                $content .=
                '</tr>';

                $n++;
            }
        }
    }

    if( !empty( $child ) )
    {
        $n = 1;

        if ( isset( $passenger['child'] ) )
        {
            foreach ( $passenger['child'] as $i => $data )
            {
                if ( $passenger['child'][$i]['bpstatus'] != 'cn' )
                {
                    $bpname      = isset( $passenger['child'][$i]['bpname'] ) ? $passenger['child'][$i]['bpname'] : '';
                    $bpgender    = isset( $passenger['child'][$i]['bpgender'] ) ? $passenger['child'][$i]['bpgender'] : 1;
                    $lcountry_id = isset( $passenger['child'][$i]['lcountry_id'] ) ? $passenger['child'][$i]['lcountry_id'] : '';
                    $bpbirthdate = isset( $passenger['child'][$i]['bpbirthdate'] ) ? ( $passenger['child'][$i]['bpbirthdate'] == '0000-00-00' ? '' : date( 'd F Y', strtotime( $passenger['child'][$i]['bpbirthdate'] ) ) ) : '';

                    $bpstatus         = isset( $passenger['child'][$i]['bpstatus'] ) ? $passenger['child'][$i]['bpstatus'] : '';
                    $bprefund         = isset( $passenger['child'][$i]['bprefund'] ) ? $passenger['child'][$i]['bprefund'] : '';
                    $bpcdate          = isset( $passenger['child'][$i]['bpcdate'] ) ? $passenger['child'][$i]['bpcdate'] : '';
                    $bpcancelfee      = isset( $passenger['child'][$i]['bpcancelfee'] ) ? $passenger['child'][$i]['bpcancelfee'] : '';
                    $bppercentfee     = isset( $passenger['child'][$i]['bppercentfee'] ) ? $passenger['child'][$i]['bppercentfee'] : '';
                    $bptransactionfee = isset( $passenger['child'][$i]['bptransactionfee'] ) ? $passenger['child'][$i]['bptransactionfee'] : '';
                    $cheked_status    = $bpstatus == 'aa' ? 'checked' : '';

                    $content .= '
                    <tr>
                        <td>
                            <span class="small-title">Child ' . ( $n ) . '</span>
                            <input type="text" class="bpname text form-control" value="' . $bpname . '" name="passenger[' . $btype . '][child][' . ( $n ) . '][bpname]" required>
                            <div class="sr-only">
                                <input type="text" class="text form-control" value="' . $bpcdate . '" name="passenger[' . $btype . '][child][' . ( $n ) . '][bpcdate]">
                                <input type="text" class="text form-control" value="' . $bpstatus . '" name="passenger[' . $btype . '][child][' . ( $n ) . '][bpstatus]">
                                <input type="text" class="text form-control" value="' . $bprefund . '" name="passenger[' . $btype . '][child][' . ( $n ) . '][bprefund]">
                                <input type="text" class="text form-control" value="' . $bpcancelfee . '" name="passenger[' . $btype . '][child][' . ( $n ) . '][bpcancelfee]">
                                <input type="text" class="text form-control" value="' . $bppercentfee . '" name="passenger[' . $btype . '][child][' . ( $n ) . '][bppercentfee]">
                                <input type="text" class="text form-control" value="' . $bptransactionfee . '" name="passenger[' . $btype . '][child][' . ( $n ) . '][bptransactionfee]">
                            </div>
                        </td>
                        <td>
                            <span class="small-title">Gender</span>
                            <div>
                                <select class="bpgender select-option form-control" name="passenger[' . $btype . '][child][' . ( $n ) . '][bpgender]" autocomplete="off">
                                    ' . get_gender_option( $bpgender, false ) . '
                                </select>
                            </div>
                        </td>
                        <td>
                            <span class="small-title">Date Of Birth</span>
                            <input type="text" class="bpbirthdate text form-control txt-date-child" value="' . $bpbirthdate . '" name="passenger[' . $btype . '][child][' . ( $n ) . '][bpbirthdate]" readonly>
                        </td>
                        <td width="150">
                            <span class="small-title">Nationality</span>
                            <div>
                                <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][child][' . ( $n ) . '][lcountry_id]" autocomplete="off">
                                    ' . get_country_list_option( $lcountry_id ) . '
                                </select>
                            </div>
                        </td>';

                        if ( $revise_type == 'booking' )
                        {
                            $content .=
                            '<td width="150">
                                <span class="small-title">Status</span>
                                <input type="checkbox" class="status_passanger" data-btype="' . $btype . '" data-bptype="child" data-toggle="toggle" data-width="100%" data-onstyle="primary" data-offstyle="danger" data-on="Active" data-off="Removed" ' . $cheked_status . ' name="passenger[' . $btype . '][child][' . ( $n ) . '][bpstatus]">
                            </td>';
                        }

                    $content .=
                    '</tr>';

                    $child--;
                    $n++;
                }
            }
        }


        if ( $child > 0 )
        {
            for( $i = 0; $i < $child; $i++ )
            {
                $content .= '
                <tr>
                    <td>
                        <span class="small-title">Child ' . $n . '</span>
                        <input type="text" class="bpname text form-control" value="" name="passenger[' . $btype . '][child][' . $n . '][bpname]" required>
                        <div class="sr-only">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][child][' . $n . '][bpcdate]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][child][' . $n . '][bpstatus]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][child][' . $n . '][bprefund]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][child][' . $n . '][bpcancelfee]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][child][' . $n . '][bppercentfee]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][child][' . $n . '][bptransactionfee]">
                        </div>
                    </td>
                    <td>
                        <span class="small-title">Gender</span>
                        <div>
                            <select class="bpgender select-option form-control" name="passenger[' . $btype . '][child][' . $n . '][bpgender]" autocomplete="off">
                                ' . get_gender_option( 1, false ) . '
                            </select>
                        </div>
                    </td>
                    <td>
                        <span class="small-title">Date Of Birth</span>
                        <input type="text" class="bpbirthdate text form-control txt-date-child" value="" name="passenger[' . $btype . '][child][' . $n . '][bpbirthdate]" readonly>
                    </td>
                    <td width="150">
                        <span class="small-title">Nationality</span>
                        <div>
                            <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][child][' . $n . '][lcountry_id]" autocomplete="off">
                                ' . get_country_list_option( '' ) . '
                            </select>
                        </div>
                    </td>';

                    if ( $revise_type == 'booking' )
                    {
                        $content .=
                        '<td width="150">
                            <span class="small-title">Status</span>
                            <input type="checkbox" class="status_passanger" data-btype="' . $btype . '" data-price="{inc_child_price}" data-bptype="child" data-toggle="toggle" data-width="100%" data-onstyle="primary" data-offstyle="danger" data-on="Active" data-off="Removed" checked name="passenger[' . $btype . '][child][' . $n . '][bpstatus]">
                        </td>';
                    }

                $content .=
                '</tr>';

                $n++;
            }
        }
    }

    if( !empty( $infant ) )
    {
        $n = 1;

        if ( isset( $passenger['infant'] ) )
        {
            foreach ( $passenger['infant'] as $i => $data )
            {
                if ( $passenger['infant'][$i]['bpstatus'] != 'cn' )
                {
                    $bpname      = isset( $passenger['infant'][$i]['bpname'] ) ? $passenger['infant'][$i]['bpname'] : '';
                    $bpgender    = isset( $passenger['infant'][$i]['bpgender'] ) ? $passenger['infant'][$i]['bpgender'] : 1;
                    $lcountry_id = isset( $passenger['infant'][$i]['lcountry_id'] ) ? $passenger['infant'][$i]['lcountry_id'] : '';
                    $bpbirthdate = isset( $passenger['infant'][$i]['bpbirthdate'] ) ? ( $passenger['infant'][$i]['bpbirthdate'] == '0000-00-00' ? '' : date( 'd F Y', strtotime( $passenger['infant'][$i]['bpbirthdate'] ) ) ) : '';

                    $bpstatus         = isset( $passenger['infant'][$i]['bpstatus'] ) ? $passenger['infant'][$i]['bpstatus'] : '';
                    $bprefund         = isset( $passenger['infant'][$i]['bprefund'] ) ? $passenger['infant'][$i]['bprefund'] : '';
                    $bpcdate          = isset( $passenger['infant'][$i]['bpcdate'] ) ? $passenger['infant'][$i]['bpcdate'] : '';
                    $bpcancelfee      = isset( $passenger['infant'][$i]['bpcancelfee'] ) ? $passenger['infant'][$i]['bpcancelfee'] : '';
                    $bppercentfee     = isset( $passenger['infant'][$i]['bppercentfee'] ) ? $passenger['infant'][$i]['bppercentfee'] : '';
                    $bptransactionfee = isset( $passenger['infant'][$i]['bptransactionfee'] ) ? $passenger['infant'][$i]['bptransactionfee'] : '';
                    $cheked_status    = $bpstatus == 'aa' ? 'checked' : '';

                    $content .= '
                    <tr>
                        <td>
                            <span class="small-title">Infant ' . ( $n ) . '</span>
                            <input type="text" class="bpname text form-control" value="' . $bpname . '" name="passenger[' . $btype . '][infant][' . ( $n ) . '][bpname]" required>
                            <div class="sr-only">
                                <input type="text" class="text form-control" value="' . $bpcdate . '" name="passenger[' . $btype . '][infant][' . ( $n ) . '][bpcdate]">
                                <input type="text" class="text form-control" value="' . $bpstatus . '" name="passenger[' . $btype . '][infant][' . ( $n ) . '][bpstatus]">
                                <input type="text" class="text form-control" value="' . $bprefund . '" name="passenger[' . $btype . '][infant][' . ( $n ) . '][bprefund]">
                                <input type="text" class="text form-control" value="' . $bpcancelfee . '" name="passenger[' . $btype . '][infant][' . ( $n ) . '][bpcancelfee]">
                                <input type="text" class="text form-control" value="' . $bppercentfee . '" name="passenger[' . $btype . '][infant][' . ( $n ) . '][bppercentfee]">
                                <input type="text" class="text form-control" value="' . $bptransactionfee . '" name="passenger[' . $btype . '][infant][' . ( $n ) . '][bptransactionfee]">
                            </div>
                        </td>
                        <td>
                            <span class="small-title">Gender</span>
                            <div>
                                <select class="bpgender select-option form-control" name="passenger[' . $btype . '][infant][' . ( $n ) . '][bpgender]" autocomplete="off">
                                    ' . get_gender_option( $bpgender, false ) . '
                                </select>
                            </div>
                        </td>
                        <td>
                            <span class="small-title">Date Of Birth</span>
                            <input type="text" class="bpbirthdate text form-control txt-date-infant" value="' . $bpbirthdate . '" name="passenger[' . $btype . '][infant][' . ( $n ) . '][bpbirthdate]" readonly>
                        </td>
                        <td width="150">
                            <span class="small-title">Nationality</span>
                            <div>
                                <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][infant][' . ( $n ) . '][lcountry_id]" autocomplete="off">
                                    ' . get_country_list_option( $lcountry_id ) . '
                                </select>
                            </div>
                        </td>';

                        if ( $revise_type == 'booking' )
                        {
                            $content .=
                            '<td width="150">
                                <span class="small-title">Status</span>
                                <input type="checkbox" class="status_passanger" data-btype="' . $btype . '" data-bptype="infant" data-toggle="toggle" data-width="100%" data-onstyle="primary" data-offstyle="danger" data-on="Active" data-off="Removed" ' . $cheked_status . ' name="passenger[' . $btype . '][infant][' . ( $n ) . '][bpstatus]">
                            </td>';
                        }

                    $content .=
                    '</tr>';

                    $infant--;
                    $n++;
                }
            }
        }

        if ( $infant > 0 )
        {
            for( $i = 0; $i < $infant; $i++ )
            {
                $content .= '
                <tr>
                    <td>
                        <span class="small-title">Infant ' . $n . '</span>
                        <input type="text" class="bpname text form-control" value="" name="passenger[' . $btype . '][infant][' . $n . '][bpname]" required>
                        <div class="sr-only">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][infant][' . $n . '][bpcdate]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][infant][' . $n . '][bpstatus]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][infant][' . $n . '][bprefund]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][infant][' . $n . '][bpcancelfee]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][infant][' . $n . '][bppercentfee]">
                            <input type="text" class="text form-control" value="" name="passenger[' . $btype . '][infant][' . $n . '][bptransactionfee]">
                        </div>
                    </td>
                    <td>
                        <span class="small-title">Gender</span>
                        <div>
                            <select class="bpgender select-option form-control" name="passenger[' . $btype . '][infant][' . $n . '][bpgender]" autocomplete="off">
                                ' . get_gender_option( 1, false ) . '
                            </select>
                        </div>
                    </td>
                    <td>
                        <span class="small-title">Date Of Birth</span>
                        <input type="text" class="bpbirthdate text form-control txt-date-infant" value="" name="passenger[' . $btype . '][infant][' . $n . '][bpbirthdate]" readonly>
                    </td>
                    <td width="150">
                        <span class="small-title">Nationality</span>
                        <div>
                            <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][infant][' . $n . '][lcountry_id]" autocomplete="off">
                                ' . get_country_list_option( '' ) . '
                            </select>
                        </div>
                    </td>';

                    if ( $revise_type == 'booking' )
                    {
                        $content .=
                        '<td width="150">
                            <span class="small-title">Status</span>
                            <input type="checkbox" class="status_passanger" data-btype="' . $btype . '" data-price="{inc_infant_price}" data-bptype="infant" data-toggle="toggle" data-width="100%" data-onstyle="primary" data-offstyle="danger" data-on="Active" data-off="Removed" checked name="passenger[' . $btype . '][infant][' . $n . '][bpstatus]">
                        </td>';
                    }

                $content .=
                '</tr>';

                $n++;
            }
        }
    }

    return $content;
}

function ticket_booking_show_detail( $bid )
{
    $message = '';
    $data    = ticket_booking_all_data( $bid  );

    if( !empty( $data ) )
    {
        if( isset( $_POST['booking_action'] ) )
        {
            if( $_POST['action'] == 0 )
            {
                if( validate_booking_status( $data ) )
                {
                    generate_booking_pdf_ticket( $data, 'I' );
                }
                else
                {
                    $message = array( 'type'=> 'error', 'content' => array( 'Sorry, unable to print this ticket, please finish your payment' ) );
                }
            }
            elseif( $_POST['action'] == 3 )
            {
                if( !send_booking_receipt_and_ticket_to_client( $data ) )
                {
                    $message = array( 'type'=> 'error', 'content' => array( 'Sorry, unable to send receipt and ticket, please finish your payment' ) );
                }
            }
            elseif( $_POST['action'] == 4 )
            {
                if( validate_booking_status( $data ) )
                {
                    generate_booking_pdf_ticket( $data, 'D' );
                }
                else
                {
                    $message = array( 'type'=> 'error', 'content' => array( 'Sorry, unable to download this ticket, please finish your payment' ) );
                }
            }
            elseif( $_POST['action'] == 5 )
            {
                generate_booking_pdf_receipt( $data, 'D' );
            }
            elseif( $_POST['action'] == 6 )
            {
                generate_reconfirmation_pdf( $data, 'I' );
            }
            elseif( $_POST['action'] == 8 )
            {
                generate_reconfirmation_pdf( $data, 'D' );
            }
            elseif( $_POST['action'] == 10 )
            {
                if( validate_booking_status( $data, array( 'pa', 'ca', 'pp' ) ) )
                {
                    generate_boarding_pass_pdf( $data, 'I' );
                }
                else
                {
                    $message = array( 'type'=> 'error', 'content' => array( 'Sorry, unable to download this reconfirmation, please finish your payment' ) );
                }
            }
            elseif( $_POST['action'] == 11 )
            {
                generate_reconfirmation_doc( $data );
            }
            elseif( $_POST['action'] == 13 )
            {
                generate_invoice_agent_pdf( $data, 'D' );
            }
        }

        extract( $data['detail'] );

        $site_url        = site_url();
        $bstatus_message = ticket_booking_status( $data['bstatus'] );
        $compliment      = array( 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 );

        set_template( PLUGINS_PATH . '/ticket/tpl/booking/detail.html', 'booking-detail' );

        add_block( 'detail-departure-loop-block', 'dtdl-block', 'booking-detail' );
        add_block( 'detail-departure-block', 'dtd-block', 'booking-detail' );
        add_block( 'detail-return-loop-block', 'dtrl-block', 'booking-detail' );
        add_block( 'detail-return-block', 'dtr-block', 'booking-detail' );
        add_block( 'detail-block', 'dt-block', 'booking-detail' );

        add_variable( 'prc', $_GET['prc'] );
        add_variable( 'site_url', $site_url );
        add_variable( 'bcode', $data['bcode'] );
        add_variable( 'booking_id', $data['bid'] );
        add_variable( 'bstatus', $data['bstatus'] );
        add_variable( 'bticket', $data['bticket'] );
        add_variable( 'bcreason', $data['bcreason'] );
        add_variable( 'bcmessage', $data['bcmessage'] );
        add_variable( 'bstatus_message', $bstatus_message );
        add_variable( 'bpaymethod', get_booking_payment_method( $data ) );
        add_variable( 'cancelation_css', in_array( $data['bstatus'], array( 'cr', 'cn' ) ) ? '' : 'sr-only' );

        add_variable( 'brefund', number_format( $data['brefund'], 0, ',', '.' ) );
        // add_variable( 'brefund', number_format( $data['brefund'], 0, ',', '.' ) );
        add_variable( 'btransactionfee', number_format( $data['btransactionfee'], 0, ',', '.' ) );
        add_variable( 'bcancellationfee', number_format( $data['bcancellationfee'], 0, ',', '.' ) );

        if( $data['bcdate'] != '0000-00-00' )
        {
            add_variable( 'cancelation_date_css', '' );
            add_variable( 'bcdate', date( 'd F Y', strtotime( $data['bcdate'] ) ) );
        }
        else
        {
            add_variable( 'cancelation_date_css', 'sr-only' );
            add_variable( 'bcdate', '' );
        }

        if( $data['brcdate'] != '0000-00-00' )
        {
            add_variable( 'req_cancelation_date_css', '' );
            add_variable( 'brcdate', date( 'd F Y', strtotime( $data['brcdate'] ) ) );
        }
        else
        {
            add_variable( 'req_cancelation_date_css', 'sr-only' );
            add_variable( 'brcdate', '' );
        }

        add_variable( 'bpaydetail', ticket_payment_detail_content( $data ) );
        add_variable( 'bbcountry', ticket_passenger_country( $data['bbcountry'] ) );
        add_variable( 'bbooking_staf', empty( $data['bbooking_staf'] ) ? '-' : $data['bbooking_staf'] );
        add_variable( 'bblockingtime', empty( $data['bblockingtime'] ) ? '-' : date( 'd F Y H:i', $data['bblockingtime'] ) );

        add_variable( 'bbname', $data['bbname'] );
        add_variable( 'bbemail', $data['bbemail'] );
        add_variable( 'bbphone', $data['bbphone'] );

        add_variable( 'bhotelname', $data['bhotelname'] );
        add_variable( 'bremark', $data['bcmessage'] . '<br/>' . $data['bremark'] );
        add_variable( 'baccommodationcss', empty( $data['bhotelname'] ) ? 'sr-only' : '' );
        add_variable( 'bremarkcss', empty( $data['bremark'] ) && empty( $data['bcmessage'] ) ? 'sr-only' : '' );
        add_variable( 'bhoteladdress', empty( $data['bhoteladdress'] ) ? '' : $data['bhoteladdress'] . '<br />' );
        add_variable( 'bhotelphone', empty( $data['bhotelphone'] ) ? '' : 'P. ' . $data['bhotelphone'] . '<br />' );
        add_variable( 'bhotelemail', empty( $data['bhotelemail'] ) ? '' : 'E. ' . $data['bhotelemail'] . '<br />' );

        add_variable( 'bbrevtype', empty( $data['bbrevtype'] ) ? '-' : $data['bbrevtype'] );
        add_variable( 'bbrevagent', empty( $data['bbrevagent'] ) ? '-' : $data['bbrevagent'] );
        add_variable( 'bbrevstatus', empty( $data['bbrevstatus'] ) ? '-' : $data['bbrevstatus'] );

        add_variable( 'message', generate_message_block( $message ) );
        add_variable( 'btype', ticket_booking_type( $data['btype'] ) );
        add_variable( 'bdate', date( 'd F Y', strtotime( $data['bdate'] ) ) );
        add_variable( 'class_booking_status', generateSefUrl( $bstatus_message ) );
        add_variable( 'detail_link', HTSERVER . $site_url . '/l-admin/?state=reservation&sub=booking&prc=edit&id=' . $data['bid'] );
        add_variable( 'print_boarding_pas_link', HTSERVER . $site_url . '/ticket-boarding-pass/?id=' . $data['bid'] );

        add_variable( 'bcancellationpercent', '(' . $data['bcancelationpercent'] . '%)' );

        if( empty( $data['agid'] ) )
        {
            add_variable( 'bchannel', $data['chname'] );
        }
        else
        {
            $agid = empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'];

            add_variable( 'bchannel', get_agent( $agid, 'agname' ) );
        }

        $div_blocked =
        '<div class="row-detail-item-block">
            <div class="row-detail-col-block">
                <h2 class="row-detail-block-font">Cancelled</h2>
            </div>
        </div>';

        //-- Go Trip
        if( isset( $departure ) )
        {
            foreach( $departure as $key => $dp_trip )
            {
                extract( $dp_trip );

                $var = get_variable_by_port_type( $bdfrom_id, $bdto_id );

                $dep_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                $dep_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                $dep_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                $dep_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';

                $dep_pa_after_discount = $num_adult == 0 ? '' : '&emsp;&nbsp;&nbsp;' . number_format( ( $price_per_adult - $disc_price_per_adult ), 0, ',', '.' );
                $dep_pc_after_discount = $num_child == 0 ? '' : '&emsp;&nbsp;&nbsp;' . number_format( ( $price_per_child - $disc_price_per_child ), 0, ',', '.' );
                $dep_pi_after_discount = $num_infant == 0 ? '' : '&emsp;&nbsp;&nbsp;' . number_format( ( $price_per_infant - $disc_price_per_infant ), 0, ',', '.' );

                add_variable( 'dep_bdto', $bdto );
                add_variable( 'dep_bdfrom', $bdfrom );
                add_variable( 'dep_num_adult', $num_adult );
                add_variable( 'dep_num_child', $num_child );
                add_variable( 'dep_num_infant', $num_infant );
                add_variable( 'dep_bddeparttime', $bddeparttime );
                add_variable( 'dep_bdarrivetime', $bdarrivetime );

                add_variable( 'dep_bdrevstatus', $bdrevstatus );
                add_variable( 'dep_bdpstatus', ticket_booking_status( $bdpstatus ) );

                add_variable( 'dep_discount', $discount );
                add_variable( 'dep_price_per_adult', $price_per_adult );
                add_variable( 'dep_price_per_child', $price_per_child );
                add_variable( 'dep_price_per_infant', $price_per_infant );

                add_variable( 'dep_pa_display', $dep_pa_display );
                add_variable( 'dep_pc_display', $dep_pc_display );
                add_variable( 'dep_pi_display', $dep_pi_display );
                add_variable( 'dep_passenger', ticket_passenger_list_content( $passenger ) );

                add_variable( 'dep_pa_after_discount', $dep_pa_after_discount );
                add_variable( 'dep_pc_after_discount', $dep_pc_after_discount );
                add_variable( 'dep_pi_after_discount', $dep_pi_after_discount );

                add_variable( 'dep_bdtype', ucfirst( $bdtype ) );
                add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'dep_transport_css', empty( $transport ) ? 'sr-only' : '' );
                add_variable( 'dep_transport', ticket_detail_transport_content( $transport, $bdfrom, $bdto ) );

                add_variable( 'baaccommodationarea', $var['drop_sts'] ? $bdfrom : $bdto );

                add_variable( 'dep_css', '' );
                add_variable( 'dep_trans_css', '' );
                add_variable( 'row-detail-booking-pos', $bdstatus == 'cn' ? 'row-detail-booking-pos' : '' );
                add_variable( 'div_blocked', $bdstatus == 'cn' ? $div_blocked : '' );
                add_variable( 'status_sub_heading', $bdstatus == 'cn' ? '<span style="color:red">( Cancelled )</span>' : '' );

                if( in_array( $data['bpaymethod'], $compliment ) )
                {
                    add_variable( 'dep_pt_display', 0 );
                }
                else
                {
                    add_variable( 'dep_pt_display', $dep_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );
                }

                parse_template( 'detail-departure-loop-block', 'dtdl-block', true );
            }

            parse_template( 'detail-departure-block', 'dtd-block' );
        }
        else
        {
            add_variable( 'dep_css', 'sr-only' );
            add_variable( 'dep_trans_css', 'sr-only' );
        }

        //-- Back Trip
        if( isset( $return ) )
        {
            foreach( $return as $key => $rt_trip )
            {
                extract( $rt_trip );

                $rtn_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                $rtn_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                $rtn_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                $rtn_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';

                $rtn_pa_after_discount = $num_adult == 0 ? '' : '&emsp;&nbsp;&nbsp;' . number_format( ( $price_per_adult - $disc_price_per_adult ), 0, ',', '.' );
                $rtn_pc_after_discount = $num_child == 0 ? '' : '&emsp;&nbsp;&nbsp;' . number_format( ( $price_per_child - $disc_price_per_child ), 0, ',', '.' );
                $rtn_pi_after_discount = $num_infant == 0 ? '' : '&emsp;&nbsp;&nbsp;' . number_format( ( $price_per_infant - $disc_price_per_infant ), 0, ',', '.' );

                add_variable( 'rtn_bdto', $bdto );
                add_variable( 'rtn_bdfrom', $bdfrom );
                add_variable( 'rtn_bddeparttime', $bddeparttime );
                add_variable( 'rtn_bdarrivetime', $bdarrivetime );

                add_variable( 'rtn_bdrevstatus', $bdrevstatus );
                add_variable( 'rtn_bdpstatus', ticket_booking_status( $bdpstatus ) );

                add_variable( 'rtn_discount', $discount );
                add_variable( 'rtn_price_per_adult', $price_per_adult );
                add_variable( 'rtn_price_per_child', $price_per_child );
                add_variable( 'rtn_price_per_infant', $price_per_infant );

                add_variable( 'rtn_pa_display', $rtn_pa_display );
                add_variable( 'rtn_pc_display', $rtn_pc_display );
                add_variable( 'rtn_pi_display', $rtn_pi_display );
                add_variable( 'rtn_passenger', ticket_passenger_list_content( $passenger ) );

                add_variable( 'rtn_pa_after_discount', $rtn_pa_after_discount );
                add_variable( 'rtn_pc_after_discount', $rtn_pc_after_discount );
                add_variable( 'rtn_pi_after_discount', $rtn_pi_after_discount );

                add_variable( 'rtn_bdtype', ucfirst( $bdtype ) );
                add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'rtn_transport_css', empty( $transport ) ? 'sr-only' : '' );
                add_variable( 'rtn_transport', ticket_detail_transport_content( $transport, $bdfrom, $bdto ) );

                add_variable( 'rtn_css', '' );
                add_variable( 'rtn_trans_css', '' );
                add_variable( 'row-detail-booking-pos', $bdstatus == 'cn' ? 'row-detail-booking-pos' : '' );
                add_variable( 'div_blocked', $bdstatus == 'cn' ? $div_blocked : '' );
                add_variable( 'status_sub_heading', $bdstatus == 'cn' ? '<span style="color:red">( Cancelled )</span>' : '' );

                if( in_array( $data['bpaymethod'], $compliment ) )
                {
                    add_variable( 'rtn_pt_display', 0 );
                }
                else
                {
                    add_variable( 'rtn_pt_display', $rtn_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );
                }

                parse_template( 'detail-return-loop-block', 'dtrl-block', true );
            }

            parse_template( 'detail-return-block', 'dtr-block' );
        }
        else
        {
            add_variable( 'rtn_css', 'sr-only' );
            add_variable( 'rtn_trans_css', 'sr-only' );
        }

        $btotal       = $data['btotal'];
        $bsubtotal    = $data['bsubtotal'];
        $bdiscount    = $data['bdiscount'];
        $bonhandtotal = $data['bonhandtotal'];
        $remaintotal  = $btotal - $data['bonhandtotal'];

        if( empty( $data['pmcode'] ) )
        {
            add_variable( 'sub_css', 'sr-only' );
            add_variable( 'subtotal_display', number_format( $bsubtotal, 0, ',', '.' ) );
            add_variable( 'discount_display', number_format( $bdiscount, 0, ',', '.' ) );
            add_variable( 'grandtotal_display', number_format( $btotal, 0, ',', '.' ) );
            add_variable( 'onhandtotal_display', number_format( $bonhandtotal, 0, ',', '.' ) );
            add_variable( 'remaintotal_display', number_format( $remaintotal, 0, ',', '.' ) );
        }
        else
        {
            add_variable( 'pmcode', 'Code : ' . $data['pmcode'] );
            add_variable( 'subtotal_display', number_format( $bsubtotal, 0, ',', '.' ) );
            add_variable( 'discount_display', number_format( $bdiscount, 0, ',', '.' ) );
            add_variable( 'grandtotal_display', number_format( $btotal, 0, ',', '.' ) );
            add_variable( 'onhandtotal_display', number_format( $bonhandtotal, 0, ',', '.' ) );
            add_variable( 'remaintotal_display', number_format( $remaintotal, 0, ',', '.' ) );
        }

        add_variable( 'img_url', get_theme_img() );
        add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-booking-ajax' );

        add_actions( 'section_title', 'Booking Detail' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        parse_template( 'detail-block', 'dt-block' );

        return return_template( 'booking-detail' );
    }
}

function ticket_payment_detail_content( $data, $is_email = false )
{
    extract( $data );

    if( $bstatus == 'pa' )
    {
        if( $is_email )
        {
            $css = 'style="font-size:11px; padding-top:5px; display:block;"';
        }
        else
        {
            $css = '';
        }

        if( !empty( $payment ) )
        {
            $payment = end( $payment );

            if( $bpaymethod == 3 )
            {
                $ppaycode = $payment['ppaycode'];

                if( empty( $payment['prefcode'] ) || empty( $payment['pvirtualacc'] ) )
                {
                    try
                    {
                        $setting = get_paypal_action_link();
                        $context = getApiContext( $setting['client_id'], $setting['client_secret'] );
                        $payment = Payment::get( $payment['ppaycode'], $context );
                        $payment = json_decode( $payment->toJSON( 128 ), true );

                        if( $payment === null && json_last_error() !== JSON_ERROR_NONE )
                        {
                            $prefcode    = '';
                            $pvirtualacc = '';
                        }
                        else
                        {
                            $prefcode    = '<br/>Invoice : ' . $payment['transactions'][0]['invoice_number'];
                            $pvirtualacc = '<br/>Trans. ID : ' . $payment['transactions'][0]['related_resources'][0]['sale']['id'];
                        }
                    }
                    catch( Exception $ex )
                    {
                        $prefcode    = '';
                        $pvirtualacc = '';
                    }
                }
                else
                {
                    $prefcode    = empty( $payment['prefcode'] ) ? '' : '<br/>Invoice : ' . $payment['prefcode'];
                    $pvirtualacc = empty( $payment['pvirtualacc'] ) ? '' : '<br/>Trans. ID : ' . $payment['pvirtualacc'];
                }

                if( $is_email )
                {
                    return '<small ' . $css . '>' . $ppaycode . $prefcode . '</small>';
                }
                else
                {
                    return '<small ' . $css . '>' . $ppaycode . $prefcode . $pvirtualacc . '</small>';
                }
            }
            else if( $bpaymethod == 5 )
            {
                if( isset( $payment ) )
                {
                    if( $payment['pmethod'] == 'Doku' )
                    {
                        return '<small ' . $css . '>Doku #' . $payment['prefcode'] . '</small>';
                    }
                    else
                    {
                        return '<small ' . $css . '>Nicepay #' . $payment['ppaycode'] . '</small>';
                    }
                }
            }
        }
    }
}

function ticket_detail_transport_content( $transport = array(), $from, $to )
{
    $content = '';

    foreach( $transport as $type => $obj )
    {
        foreach( $obj as $d )
        {
            if( $d['bttrans_type'] == '2' )
            {
                $trans_loc = $type == 'pickup' ? $from : $to;
                $content  .= '
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <span class="small-title">' . ucfirst( $type ) . ' - ' . $trans_loc . '</span>
                            <div class="form-group"
                                <strong class="strong-text">Own Transport</strong>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <span class="small-title">Driver Detail</span>
                            <div class="form-group">
                                <p class="regular-text">
                                    <b>Name: </b>' . $d['btdrivername'] . '<br />
                                    <b>Phone: </b>' . $d['btdriverphone'] . '
                                </p>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <span class="small-title">Transport Fee</span>
                            <div class="form-group"
                                <p class="regular-text">' . ( $d['bttrans_fee'] == 0 ? 'N/A' : number_format( $d['bttrans_fee'], 0, ',', '.' ) ) . '</p>
                            </div>
                        </div>
                    </div>
                </div>';
            }
            else
            {
                $tarea   = get_transport_area( $d['taid'] );
                $airport = $tarea['taairport'];
                $area    = $tarea['taname'];

                $trans_type = $d['bttrans_type'] == '0' ? 'Shared Transport' : 'Privated Transport';
                $trans_loc  = $type == 'pickup' ? $from : $to;

                if( empty( $area ) )
                {
                    $hdetail = 'No Accommodation Booked';
                }
                else
                {
                    $rpto   = $d['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $d['btrpto'] ) );
                    $rpfrom = $d['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $d['btrpfrom'] ) );

                    $hname  = empty( $d['bthotelname'] ) ? '' : $d['bthotelname'] . '<br />';
                    $haddr  = empty( $d['bthoteladdress'] ) ? '' : $d['bthoteladdress'] . '<br />';

                    $flabel = $type == 'pickup' ? '<span class="small-title">Flight Landing Time</span>' : '<span class="small-title">Flight Take Off Time</span>';
                    $ftime  = empty( $airport ) ? '' : sprintf( '<br/><br/><b>' . $flabel . '</b><br/>%s<br/>', date( 'd F Y, H:i', strtotime( $d['btflighttime'] ) ) );

                    if( empty( $d['hid'] ) )
                    {
                        if( empty( $rpfrom ) && empty( $rpto ) )
                        {
                            $hdetail = $area . $ftime;
                        }
                        else
                        {
                            if( $type == 'pickup' )
                            {
                                $hdetail = $area . '<br/>in between ' . $rpfrom . ' - ' . $rpto . $ftime;
                            }
                            else
                            {
                                $hdetail = $area;
                            }
                        }
                    }
                    elseif( $d['hid'] == 1 )
                    {
                        if( empty( $rpfrom ) && empty( $rpto ) )
                        {
                            if( empty( $hname ) && empty( $haddr ) )
                            {
                                $hdetail = $area . '<br/>Hotel to be advised' . $ftime;
                            }
                            else
                            {
                                $hdetail = $area . '<br/>' . $hname . $haddr . $ftime;
                            }
                        }
                        else
                        {
                            if( empty( $hname ) && empty( $haddr ) )
                            {
                                if( $type == 'pickup' )
                                {
                                    $hdetail = $area . '<br/>Hotel to be advised<br />in between ' . $rpfrom . ' - ' . $rpto . $ftime;
                                }
                                else
                                {
                                    $hdetail = $area . '<br/>Hotel to be advised';
                                }
                            }
                            else
                            {
                                if( $type == 'pickup' )
                                {
                                    $hdetail = $area . '<br/>' . $hname . $haddr . 'in between ' . $rpfrom . ' - ' . $rpto . $ftime;
                                }
                                else
                                {
                                    $hdetail = $area . '<br/>' . $hname . $haddr;
                                }
                            }
                        }
                    }
                    else
                    {
                        if( empty( $rpfrom ) && empty( $rpto ) )
                        {
                            $hdetail = $area . '<br/>' . $hname . $haddr . $ftime;
                        }
                        else
                        {
                            if( $type == 'pickup' )
                            {
                                $hdetail = $area . '<br/>' . $hname . $haddr . 'in between ' . $rpfrom . ' - ' . $rpto . $ftime;
                            }
                            else
                            {
                                $hdetail = $area . '<br/>' . $hname . $haddr;
                            }
                        }
                    }
                }

                $content .= '
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <span class="small-title">' . ucfirst( $type ) . ' - ' . $trans_loc . '</span>
                            <div class="form-group"
                                <strong class="strong-text">' . $trans_type . '</strong>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <span class="small-title">Transport Detail</span>
                            <div class="form-group">
                                <p class="regular-text">
                                    ' . $hdetail . '
                                </p>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <span class="small-title">Transport Fee</span>
                            <div class="form-group"
                                <p class="regular-text">' . ( $d['bttrans_fee'] == 0 ? 'Free' : number_format( $d['bttrans_fee'], 0, ',', '.' ) ) . '</p>
                            </div>
                        </div>
                    </div>
                </div>';
            }
        }
    }

    return $content;
}

function ticket_detail_transport_email_content( $transport = array(), $from, $to, $bdate, $bddate )
{
    $content = '
    <table cellspacing="0" cellpadding="0" style="width:100%; table-layout:fixed;">';

        foreach( $transport as $ttype => $trans )
        {
            foreach( $trans as $t )
            {
                if( $t['bttrans_type'] != '2' )
                {
                    if( empty( $t['taid'] ) )
                    {
                        if( round( ( strtotime( $bddate ) - strtotime( $bdate ) ) / 3600, 1 ) > 24 )
                        {
                            $hdetail = 'No Accommodation Booked <b>(Pls advice the address latest 24 hours prior trip if you need land transport arrangement)</b>';
                        }
                        else
                        {
                            $hdetail = 'No Accommodation Booked';
                        }
                    }
                    else
                    {
                        $tarea  = get_transport_area( $t['taid'] );
                        $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                        $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                        $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                        $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                        if( empty( $rpfrom ) && empty( $rpto ) )
                        {
                            $note = '';
                        }
                        else
                        {
                            if( $ttype == 'pickup' )
                            {
                                $note = '<br/>In between ' . $rpfrom . ' - ' . $rpto;
                            }
                            else
                            {
                                $note = '';
                            }
                        }

                        if( empty( $tarea['taairport'] ) )
                        {
                            $ftime = '';
                        }
                        else
                        {
                            $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                            $ftime = sprintf( '<br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                        }

                        if( empty( $t['hid'] ) )
                        {
                            $hdetail = $tarea['taname'] . $ftime . $note;
                        }
                        elseif( $t['hid'] == 1 )
                        {
                            if( empty( $hname ) && empty( $haddr ) )
                            {
                                if( round( ( strtotime( $bddate ) - strtotime( $bdate ) ) / 3600, 1 ) > 24 )
                                {
                                    $hdetail =  $tarea['taname'] . $ftime . '<br />Hotel to be advised <b>(Pls advice your hotel stay latest 24 hours prior trip)</b>' . $note;
                                }
                                else
                                {
                                    $hdetail = $tarea['taname'] . $ftime . '<br />Hotel to be advised' . $note;
                                }
                            }
                            else
                            {
                                $hdetail = $tarea['taname'] . $ftime . '<br />**Hotel Details From Guest**<br />' . $hname . $haddr . $note;
                            }
                        }
                        else
                        {
                            if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                            {
                                $hdetail = $tarea['taname'] . $ftime . $note;
                            }
                            else
                            {
                                $hdetail = $tarea['taname'] . $ftime . '<br />' . $hname . $haddr . $note;
                            }
                        }
                    }

                    if( $t['bttrans_type'] == '0' )
                    {
                        $trans_type = 'Shared Transport';
                    }
                    else
                    {
                        $trans_type = 'Privated Transport';
                    }

                    $trans_loc  = $ttype == 'pickup' ? $from : $to;
                    $content .= '
                    <tr>
                        <td align="left" valign="top" style="width:170px; padding:7px;">
                            <strong style="color:rgb(62, 117, 128); font-size:12px;">' . ucfirst( $ttype ) . ' - ' . $trans_loc . '</strong>
                            <p style="color:rgb(73, 73, 85); font-size:12px; margin:0;">
                                ' . $trans_type . '
                            </p>
                        </td>
                        <td align="left" valign="top" style="padding:7px;">
                            <strong style="color:rgb(62, 117, 128); font-size:12px;">Transport Detail</strong>
                            <p style="color:rgb(73, 73, 85); font-size:12px; margin:0;">
                                ' . $hdetail . '
                            </p>
                        </td>
                        <td align="right" valign="top" style="padding:7px;">
                            <strong style="color:rgb(62, 117, 128); font-size:12px;">Transport Fee</strong>
                            <p style="color:rgb(73, 73, 85); font-size:12px; margin:0;">
                                <b>' . ( $t['bttrans_fee'] == 0 ? '0' : number_format( $t['bttrans_fee'], 0, ',', '.' ) ) . '</b>
                                <br/><br/>
                            </p>
                        </td>
                    </tr>';
                }
                else
                {
                    $trans_loc = $ttype == 'pickup' ? $from : $to;
                    $content  .= '
                    <tr>
                        <td align="left" valign="top" style="width:170px; padding:7px;">
                            <strong style="color:rgb(62, 117, 128); font-size:12px;">' . ucfirst( $ttype ) . ' - ' . $trans_loc . '</strong>
                            <p style="color:rgb(73, 73, 85); font-size:12px; margin:0;">Own Transport</p>
                        </td>
                        <td align="left" valign="top" style="padding:7px;">
                            <strong style="color:rgb(62, 117, 128); font-size:12px;">Transport Detail</strong>
                            <p style="color:rgb(73, 73, 85); font-size:12px; margin:0;">
                                <b>Name: </b>' . $t['btdrivername'] . '<br />
                                <b>Phone: </b>' . $t['btdriverphone'] . '
                            </p>
                        </td>
                        <td align="right" valign="top" style="padding:7px;">
                            <strong style="color:rgb(62, 117, 128); font-size:12px;">Transport Fee</strong>
                            <p style="color:rgb(73, 73, 85); font-size:12px; margin:0;">
                                <b>' . ( $t['bttrans_fee'] == 0 ? 'N/A' : number_format( $t['bttrans_fee'], 0, ',', '.' ) ) . '</b>
                                <br/><br/>
                            </p>
                        </td>
                    </tr>';
                }
            }
        }

        $content .= '
    </table>';

    return $content;
}

function ticket_detail_transport_pdf_content( $transport = array(), $from, $to )
{
    $content = '
    <div class="trans clearfix">';

        foreach( $transport as $ttype => $trans )
        {
            foreach( $trans as $t )
            {
                if( $t['bttrans_type'] != '2' )
                {
                    if( empty( $t['taid'] ) )
                    {
                        $hdetail = 'No Accommodation Booked';
                    }
                    else
                    {
                        $tarea  = get_transport_area( $t['taid'] );
                        $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                        $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                        $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                        $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                        if( empty( $rpfrom ) && empty( $rpto ) )
                        {
                            $note = '';
                        }
                        else
                        {
                            if( $ttype == 'pickup' )
                            {
                                $note = '<br/>In between ' . $rpfrom . ' - ' . $rpto;
                            }
                            else
                            {
                                $note = '';
                            }
                        }

                        if( empty( $tarea['taairport'] ) )
                        {
                            $ftime = '';
                        }
                        else
                        {
                            $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                            $ftime = sprintf( '<br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                        }

                        if( empty( $t['hid'] ) )
                        {
                            $hdetail = $tarea['taname'] . $ftime . $note;
                        }
                        elseif( $t['hid'] == 1 )
                        {
                            if( empty( $hname ) && empty( $haddr ) )
                            {
                                $hdetail = $tarea['taname'] . $ftime . '<br />Hotel to be advised' . $note;
                            }
                            else
                            {
                                $hdetail = $tarea['taname'] . $ftime . '<br />**Hotel Details From Guest**<br />' . $hname . $haddr . $note;
                            }
                        }
                        else
                        {
                            if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                            {
                                $hdetail = $tarea['taname'] . $ftime . $note;
                            }
                            else
                            {
                                $hdetail = $tarea['taname'] . ', ' . $hname . $haddr . $ftime . $note;
                            }
                        }
                    }

                    if( $t['bttrans_type'] == '0' )
                    {
                        $trans_type = 'Shared Transport';
                    }
                    else
                    {
                        $trans_type = 'Privated Transport';
                    }

                    $format   = '<p>%s, %s<br/>%s</p>';
                    $content .= sprintf( $format, ucfirst( $ttype ), $trans_type, $hdetail );
                }
                else
                {
                    $notif = ( $ttype == 'pickup' ) ? 'Latest check in 30 minutes prior to departure' : '' ;

                    $format   = '<p>%s, Own Transport<br/><b>Driver : </b>%s ( %s )<br/><b>%s</b></p>';
                    $content .= sprintf( $format, ucfirst( $ttype ), $t['btdrivername'], $t['btdriverphone'], $notif );
                }
            }
        }

        $content .= '

    </div>';

    return $content;
}

function ticket_passenger_country( $id )
{
    global $db;

    $s = 'SELECT * FROM l_country WHERE lcountry_id = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return isset( $d['lcountry'] ) && empty( $d['lcountry'] ) === false ? $d['lcountry'] : '-';
}

function ticket_booking_detail_by_id( $bdid )
{
    global $db;

    $s = 'SELECT
            a.*,
            b.rname,
            b.rstatus,
            b.rdepart,
            b.rarrive,
            b.rcot,
            b.rtype,
            b.rhoppingpoint
          FROM ticket_booking_detail AS a
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          WHERE a.bdid = %d ORDER BY bddate';
    $q = $db->prepare_query( $s, $bdid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d;
}

function ticket_booking_detail( $bid, $type = '' )
{
    global $db;

    $data = array();

    if( empty( $type ) )
    {
        $s = 'SELECT
                a.*,
                b.rname,
                b.rstatus,
                b.rdepart,
                b.rarrive,
                b.rcot,
                b.rtype,
                b.rhoppingpoint
              FROM ticket_booking_detail AS a
              LEFT JOIN ticket_route AS b ON a.rid = b.rid
              WHERE a.bid = %d ORDER BY bddate';
        $q = $db->prepare_query( $s, $bid );
    }
    else
    {
        $s = 'SELECT
                a.*,
                b.rname,
                b.rstatus,
                b.rdepart,
                b.rarrive,
                b.rcot,
                b.rtype,
                b.rhoppingpoint
              FROM ticket_booking_detail AS a
              LEFT JOIN ticket_route AS b ON a.rid = b.rid
              WHERE a.bid = %d AND a.bdtype = %s ORDER BY bddate';
        $q = $db->prepare_query( $s, $bid, $type );
    }

    $r = $db->do_query( $q );

    while( $d = $db->fetch_array( $r ) )
    {
        $data[ $d['bdtype'] ][ $d['bdid'] ] = $d;

        //-- Passanger
        $s2 = 'SELECT * FROM ticket_booking_passenger WHERE bdid = %d ORDER BY bpid ASC';
        $q2 = $db->prepare_query( $s2, $d['bdid'] );
        $r2 = $db->do_query( $q2 );

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[ $d['bdtype'] ][ $d['bdid'] ]['passenger'][ $d2['bptype'] ][] = $d2;
        }

        //-- Transport
        $s2 = 'SELECT * FROM ticket_booking_transport AS a
               LEFT JOIN ticket_hotel AS b ON a.hid = b.hid
               LEFT JOIN ticket_transport_area AS c ON a.taid = c.taid
               WHERE a.bdid = %d';
        $q2 = $db->prepare_query( $s2, $d['bdid'] );
        $r2 = $db->do_query( $q2 );

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[ $d['bdtype'] ][ $d['bdid'] ]['transport'][ $d2['bttype'] ][] = $d2;
        }

        if( !isset( $data[ $d['bdtype'] ][ $d['bdid'] ]['transport'] ) )
        {
            $data[ $d['bdtype'] ][ $d['bdid'] ]['transport'] = array();
        }
    }

    return empty( $type ) ? $data : $data[$type];
}

function ticket_passenger_list( $bdid, $group = false )
{
    global $db;

    $s = 'SELECT * FROM ticket_booking_passenger WHERE bdid = %d ORDER BY bpid ASC';
    $q = $db->prepare_query( $s, $bdid );
    $r = $db->do_query( $q );

    $data = array();

    if( $group )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data[ $d['bptype'] ][] = $d;
        }
    }
    else
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data[] = $d;
        }
    }

    return $data;
}

function ticket_booking_payment_detail( $bid )
{
    global $db;

    $s = 'SELECT * FROM ticket_booking_payment AS a WHERE a.bid = %d ORDER BY a.ppaydate, a.pid';
    $q = $db->prepare_query( $s, $bid );
    $r = $db->do_query( $q );

    $dt = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $dt[] = $d;
    }

    return $dt;
}

function ticket_booking_payment_by_id( $pid )
{
    global $db;

    $s = 'SELECT * FROM ticket_booking_payment AS a WHERE a.pid = %d';
    $q = $db->prepare_query( $s, $pid );
    $r = $db->do_query( $q );

    return $db->fetch_array( $r );
}

function ticket_passenger_list_content( $data, $for_email = false )
{
    $content = '';

    foreach( $data as $obj )
    {
        foreach( $obj as $i => $d )
        {
            $bpbirthdate = $d['bpbirthdate'] == '0000-00-00' ? '-' : date( 'd M Y', strtotime( $d['bpbirthdate'] ) );
            $bpcountry   = ticket_passenger_country( $d['lcountry_id'] );
            $bpgender    = empty( $d['bpgender'] ) ? 'Female' : 'Male';

            if( $for_email )
            {
                $content .= '
                <tr>
                    <td align="left" valign="middle" style="border-bottom:1px solid rgb(198, 216, 220); padding:7px; width:40%;">
                        <p style="margin:5px 0 0; color:rgb(73, 73, 85); font-size:12px;">' . $d['bpname'] . '</p>
                    </td>
                    <td align="left" valign="middle" style="border-bottom:1px solid rgb(198, 216, 220); padding:7px; width:10%;">
                        <p style="margin:5px 0 0; color:rgb(73, 73, 85); font-size:12px;">' . ucfirst( $d['bptype'] ) . '</p>
                    </td>
                    <td align="left" valign="middle" style="border-bottom:1px solid rgb(198, 216, 220); padding:7px; width:10%;">
                        <p style="margin:5px 0 0; color:rgb(73, 73, 85); font-size:12px;">' . $bpgender . '</p>
                    </td>
                    <td align="left" valign="middle" style="border-bottom:1px solid rgb(198, 216, 220); padding:7px; width:20%;">
                        <p style="margin:5px 0 0; color:rgb(73, 73, 85); font-size:12px;">' . $bpbirthdate . '</p>
                    </td>
                    <td align="left" valign="middle" style="border-bottom:1px solid rgb(198, 216, 220); padding:7px; width:20%;">
                        <p style="margin:5px 0 0; color:rgb(73, 73, 85); font-size:12px;">' . $bpcountry . '</p>
                    </td>
                </tr>';
            }
            else
            {
                if ( $d['bpstatus'] == 'cn' )
                {
                    $content .= '
                    <ul class="row">
                        <li class="col-md-4">
                            <p class="regular-text">' . $d['bpname'] . '</p>
                        </li>
                        <li class="col-md-1">
                            <p class="regular-text">' . ucfirst( $d['bptype'] ) . '</p>
                        </li>
                        <li class="col-md-1">
                            <p class="regular-text">' . $bpgender . '</p>
                        </li>
                        <li class="col-md-2">
                            <p class="regular-text">' . $bpcountry . '</p>
                        </li>
                        <li class="col-md-2 text-right">
                            <p class="regular-text">' . $bpbirthdate . '</p>
                        </li>
                        <li class="col-md-2 text-right">
                            <p class="regular-text">Cancelled</p>
                        </li>
                    </ul>';
                }
                else
                {
                    $content .= '
                    <ul class="row">
                        <li class="col-md-4">
                            <p class="regular-text">' . $d['bpname'] . '</p>
                        </li>
                        <li class="col-md-1">
                            <p class="regular-text">' . ucfirst( $d['bptype'] ) . '</p>
                        </li>
                        <li class="col-md-1">
                            <p class="regular-text">' . $bpgender . '</p>
                        </li>
                        <li class="col-md-2">
                            <p class="regular-text">' . $bpcountry . '</p>
                        </li>
                        <li class="col-md-2 text-right">
                            <p class="regular-text">' . $bpbirthdate . '</p>
                        </li>
                        <li class="col-md-2 text-right">
                            <p class="regular-text">Active</p>
                        </li>
                    </ul>';
                }
            }
        }
    }

    return $content;
}

function ticket_passenger_list_edit_content( $data )
{
    $content = '';

    foreach( $data as $obj )
    {
        foreach( $obj as $i => $d )
        {
            $content .= '
            <tr>
                <td>
                    <span class="small-title">' . ucfirst( $d['bptype'] ) . ' ' . ( $i + 1 ) . '</span>
                    <input type="text" class="form-control" id="" value="' . $d['bpname'] . '" name="passenger[' . $d['bpid'] .'][bpname]">
                </td>
                <td>
                    <span class="small-title">Gender</span>
                    <select name="passenger[' . $d['bpid'] . '][bpgender]" class="form-control" autocomplete="off">
                        ' . get_gender_option( $d['bpgender'], false ) . '
                    </select>
                </td>
                <td>
                    <span class="small-title">Date Of Birth</span>
                    <input type="text" class="form-control txt-date-' . $d['bptype'] . '" id="" value="' . ( $d['bpbirthdate'] == '0000-00-00' ? '' : date( 'd F Y', strtotime( $d['bpbirthdate'] ) ) ) . '" name="passenger[' . $d['bpid'] . '][bpbirthdate]">
                </td>
                <td width="150">
                    <span class="small-title">Nationality</span>
                    <select name="passenger[' . $d['bpid'] . '][lcountry_id]" class="form-control" autocomplete="off">
                        ' . get_country_list_option( $d['lcountry_id'] ) . '
                    </select>
                </td>
                <td width="120">
                    <span class="small-title">Status</span>';

                    if ( $d['bpstatus'] == 'cn' )
                    {
                        $content .= '<input type="text" class="form-control text-center" value="Canceled" readonly>';
                    }
                    else
                    {
                        $content .= '<input type="text" class="form-control text-center" value="Active" readonly>';
                    }

                $content .= '
                </td>
            </tr>';
        }
    }

    return $content;
}

function ticket_passenger_list_for_pdf_content( $data, $for_email = false )
{
    $content = '
    <li class="head clearfix">
        <div class="pass-name">
            <strong>Passanger Name</strong>
        </div>
        <div class="pass-type">
            <strong>Type</strong>
        </div>
        <div class="pass-gender">
            <strong>Gender</strong>
        </div>
        <div class="pass-birthdate">
            <strong>Date Of Birth</strong>
        </div>
        <div class="pass-nationality">
            <strong>Nationality</strong>
        </div>
    </li>';

    foreach( $data as $obj )
    {
        $last = end( $obj );

        foreach( $obj as $i => $d )
        {
            if( $d['bpstatus'] == 'cn' )
            {
                continue;
            }

            $content .= '
            <li class="body clearfix ' . ( $last['bpid'] == $d['bpid'] ? 'last' : '' ) . '">
                <div class="pass-name">
                    <p class="strong-text">' . $d['bpname'] . '</p>
                </div>
                <div class="pass-type">
                    <p class="regular-text">' . ucfirst( $d['bptype'] ) . '</p>
                </div>
                <div class="pass-gender">
                    <p class="regular-text">' . ( empty( $d['bpgender'] ) ? 'Female' : 'Male' ) . '</p>
                </div>
                <div class="pass-birthdate">
                    <p class="regular-text">' . ( $d['bpbirthdate'] == '0000-00-00' ? '-' : date( 'd M Y', strtotime( $d['bpbirthdate'] ) ) ) . '</p>
                </div>
                <div class="pass-nationality">
                    <p class="regular-text">' . ticket_passenger_country( $d['lcountry_id'] ) . '</p>
                </div>
            </li>';
        }
    }

    return $content;
}

function ticket_filter_booking()
{
    $filter = array( 'rid' => '', 'lcid' => '', 'lcid2' => '', 'chid' => '', 'status' => array( 'pa', 'ca', 'pp' ), 'bdate' => '', 'date_start' => '', 'date_end' => '', 'rstatus' => array( 'Devinitive' => 'Devinitive', 'Tentative' => 'Tentative' ), 'search' => '', 'bbemail' => '' );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $status = isset( $bstatus ) ? $bstatus : '';
        $rstatus = isset( $rstatus ) ? $rstatus : '';
        $filter = array( 'rid' => $rid, 'lcid' => $lcid, 'lcid2' => $lcid2, 'chid' => $chid, 'status' => $status, 'bdate' => $bdate, 'date_start' => $date_start, 'date_end' => $date_end, 'rstatus' => $rstatus, 'search' => $sbooking, 'bbemail' => $bbemail  );
    }

    return $filter;
}

function update_session_on_promo_code_applied( $sess )
{
    global $db;

    extract( $sess );

    $sess['bcommission'] = 0;
    $sess['bcommissiontype'] = '0';
    $sess['badditionaldisc'] = '1';
    $sess['bcommissioncondition'] = '0';

    if( isset( $promo_code ) && empty( $promo_code ) === false )
    {
        $s = 'SELECT * FROM ticket_promo WHERE pmcode = %s';
        $q = $db->prepare_query( $s, $promo_code );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $d = $db->fetch_array( $r );

            if( empty( $d['agid'] ) === false )
            {
                $sess['agid'] = $d['agid'];
                $sess['chid'] = get_agent( $d['agid'], 'chid' );

                if( $d['pmcommissiontype'] == '0' )
                {
                    $sess['bcommission'] = floatval( $d['pmcommission'] );
                }
                else
                {
                    $sess['bcommission'] = floatval( $d['pmcommission'] ) / count( $trip );
                }

                $sess['bcommissiontype'] = $d['pmcommissiontype'];
                $sess['badditionaldisc'] = $d['pmadditionaldisc'];
                $sess['bcommissioncondition'] = $d['pmcommissioncondition'];
            }
        }
    }

    return $sess;
}

function save_transaction( $post, $sess )
{
    global $db;

    $retval = 1;

    $db->begin();

    $sess = update_session_on_promo_code_applied( $sess );
    $id   = save_booking( $post, $sess );

    if( empty( $id ) )
    {
        $retval = 0;
    }
    else
    {
        $dta = ticket_booking_all_data( $id );
        $did = save_booking_detail( $id, $post, $sess );
        $nid = save_booking_notification( $id, $dta['bticket'], $dta['bstatus'] );
        $tid = save_booking_agent_transaction( $id, $dta['bcode'], $post, $sess );

        extract( $post );
        extract( $sess );

        if( $payment == 1 )
        {
            $pid = save_booking_payment( $id, generate_payment_id(), '', time(), $grandtotal, 0, 'Cash', '1', $email );
        }
        elseif( in_array( $payment, array( 2, 3, 4, 5 ) ) === false )
        {
            $method = get_payment_method_by_id( $payment, 'mname' );

            if( empty( $method ) === false )
            {
                $pid = save_booking_payment( $id, generate_payment_id(), '', time(), $grandtotal, 0, $method, '1', $email );
            }
            else
            {
                $pid = 0;
            }
        }
        else
        {
            $pid = 1;
        }

        if( empty( $did ) || empty( $tid ) || empty( $pid ) || empty( $nid ) )
        {
            $retval = 0;
        }
    }

    if( $retval == 0 )
    {
        $db->rollback();

        return 0;
    }
    else
    {
        $db->commit();

        return $id;
    }
}

function save_booking( $post, $sess )
{
    global $db;

    extract( $post );
    extract( $sess );

    $bticket = generate_ticket_number();
    $bcode   = generate_code_number( $type_of_route );

    $status  = isset( $status ) ? $status : 'pp';
    $status  = $payment == 1 ? 'pa' : ( $payment == 8 ? 'ca' : $status );
    $ototal  = $payment == 1 ? $grandtotal : 0;
    $gemail  = isset( $gemail ) ? $gemail : '';
    $gmobile = isset( $gmobile ) ? $gmobile : '';
    $frcode  = isset( $freelance_code ) ? $freelance_code : '';
    $vdate   = in_array( $status, array( 'pp', 'ol' ) ) ? strtotime( date( 'Y-m-d' ) . ' +1 day' ) : 0;

    $compliment    = array( 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 );
    $blocking_time = empty( $blocking_time ) ? 0 : strtotime( $blocking_time );

    $promo_disc      = isset( $promo_disc ) ? $promo_disc : 0;
    $promo_disc_type = isset( $promo_disc_type ) ? $promo_disc_type : '0';

    if( in_array( $payment, $compliment ) )
    {
        $subtotal = 0;

        if( isset( $sess['trip']['departure']['pickup']['transport_fee'] ) )
        {
            $subtotal += $sess['trip']['departure']['pickup']['transport_fee'];
        }

        if( isset( $sess['trip']['departure']['drop-off']['transport_fee'] ) )
        {
            $subtotal += $sess['trip']['departure']['drop-off']['transport_fee'];
        }

        if( isset( $sess['trip']['return']['pickup']['transport_fee'] ) )
        {
            $subtotal += $sess['trip']['return']['pickup']['transport_fee'];
        }

        if( isset( $sess['trip']['return']['drop-off']['transport_fee'] ) )
        {
            $subtotal += $sess['trip']['return']['drop-off']['transport_fee'];
        }

        $grandtotal = $subtotal;
        $status     = 'pa';
        $discount   = 0;
        $ototal     = 0;
    }

    if( isset( $agid ) && !empty( $agid ) )
    {
        if( isset( $sagid ) && !empty( $sagid ) )
        {
            $s = 'INSERT INTO ticket_booking(
                    chid,
                    btype,
                    bcode,
                    bticket,
                    bhotelname,
                    bhoteladdress,
                    bhotelphone,
                    bhotelemail,
                    bremark,
                    pmcode,
                    freelancecode,
                    bsubtotal,
                    bdiscount,
                    btotal,
                    bonhandtotal,
                    bdate,
                    bstatus,
                    bpvaliddate,
                    bpaymethod,
                    bblockingtime,
                    agid,
                    sagid,
                    bbname,
                    bbphone,
                    bbphone2,
                    bbemail,
                    bbcountry,
                    bbrevtype,
                    bbrevstatus,
                    bbrevagent,
                    bbooking_staf,
                    bcommission,
                    bcommissiontype,
                    bcommissioncondition,
                    badditionaldisc,
                    bdiscountnum,
                    bdiscounttype,
                    gemail,
                    gmobile ) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
            $q = $db->prepare_query( $s,
                    $chid,
                    $type_of_route,
                    $bcode,
                    $bticket,
                    $hotel_name,
                    $hotel_address,
                    $hotel_phone,
                    $hotel_email,
                    $remark,
                    $promo_code,
                    $frcode,
                    $subtotal,
                    $discount,
                    $grandtotal,
                    $ototal,
                    date( 'Y-m-d' ),
                    $status,
                    $vdate,
                    $payment,
                    $blocking_time,
                    $agid,
                    $sagid,
                    $full_name,
                    $phone,
                    $phone2,
                    $email,
                    $bbcountry,
                    $rev_type,
                    $rev_status,
                    $agent_code,
                    $booking_staf,
                    $bcommission,
                    $bcommissiontype,
                    $bcommissioncondition,
                    $badditionaldisc,
                    $promo_disc,
                    $promo_disc_type,
                    $gemail,
                    $gmobile);
        }
        else
        {
            $s = 'INSERT INTO ticket_booking(
                    chid,
                    btype,
                    bcode,
                    bticket,
                    bhotelname,
                    bhoteladdress,
                    bhotelphone,
                    bhotelemail,
                    bremark,
                    pmcode,
                    freelancecode,
                    bsubtotal,
                    bdiscount,
                    btotal,
                    bonhandtotal,
                    bdate,
                    bstatus,
                    bpvaliddate,
                    bpaymethod,
                    bblockingtime,
                    agid,
                    bbname,
                    bbphone,
                    bbphone2,
                    bbemail,
                    bbcountry,
                    bbrevtype,
                    bbrevstatus,
                    bbrevagent,
                    bbooking_staf,
                    bcommission,
                    bcommissiontype,
                    bcommissioncondition,
                    badditionaldisc,
                    bdiscountnum,
                    bdiscounttype,
                    gemail,
                    gmobile ) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
            $q = $db->prepare_query( $s,
                    $chid,
                    $type_of_route,
                    $bcode,
                    $bticket,
                    $hotel_name,
                    $hotel_address,
                    $hotel_phone,
                    $hotel_email,
                    $remark,
                    $promo_code,
                    $frcode,
                    $subtotal,
                    $discount,
                    $grandtotal,
                    $ototal,
                    date( 'Y-m-d' ),
                    $status,
                    $vdate,
                    $payment,
                    $blocking_time,
                    $agid,
                    $full_name,
                    $phone,
                    $phone2,
                    $email,
                    $bbcountry,
                    $rev_type,
                    $rev_status,
                    $agent_code,
                    $booking_staf,
                    $bcommission,
                    $bcommissiontype,
                    $bcommissioncondition,
                    $badditionaldisc,
                    $promo_disc,
                    $promo_disc_type,
                    $gemail,
                    $gmobile );
        }
    }
    else
    {
        $s = 'INSERT INTO ticket_booking(
                chid,
                btype,
                bcode,
                bticket,
                bhotelname,
                bhoteladdress,
                bhotelphone,
                bhotelemail,
                bremark,
                pmcode,
                freelancecode,
                bsubtotal,
                bdiscount,
                btotal,
                bonhandtotal,
                bdate,
                bstatus,
                bpvaliddate,
                bpaymethod,
                bblockingtime,
                bbname,
                bbphone,
                bbphone2,
                bbemail,
                bbcountry,
                bbrevtype,
                bbrevstatus,
                bbrevagent,
                bbooking_staf,
                bcommission,
                bcommissiontype,
                bcommissioncondition,
                badditionaldisc,
                bdiscountnum,
                bdiscounttype,
                gemail,
                gmobile ) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
        $q = $db->prepare_query( $s,
                $chid,
                $type_of_route,
                $bcode,
                $bticket,
                $hotel_name,
                $hotel_address,
                $hotel_phone,
                $hotel_email,
                $remark,
                $promo_code,
                $frcode,
                $subtotal,
                $discount,
                $grandtotal,
                $ototal,
                date( 'Y-m-d' ),
                $status,
                $vdate,
                $payment,
                $blocking_time,
                $full_name,
                $phone,
                $phone2,
                $email,
                $bbcountry,
                $rev_type,
                $rev_status,
                $agent_code,
                $booking_staf,
                $bcommission,
                $bcommissiontype,
                $bcommissioncondition,
                $badditionaldisc,
                $promo_disc,
                $promo_disc_type,
                $gemail,
                $gmobile );
    }

    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return 0;
    }
    else
    {
        return $db->insert_id();
    }
}

function save_booking_notification( $id, $bticket, $status )
{
    if( $status == 'pp' )
    {
        $title   = 'Pending Payment #' . $bticket;
        $content = sprintf( '<p>you have a reservation with pending payment status, please click <a href="%s">this link</a> to see reservation details</p>', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $id );

        save_ticket_notification( $id, $title, $content, 0, 0, date( 'Y-m-d H:i:s' ) );
    }

    return 1;
}

function save_booking_detail( $bid, $post, $sess )
{
    global $db;

    extract( $post );
    extract( $sess );

    $error      = 0;
    $status     = isset( $status ) ? $status : 'pp';
    $status     = $payment == 1 ? 'pa' : ( $payment == 8 ? 'ca' : $status );
    $compliment = array( 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 );

    foreach( $trip as $type => $d )
    {
        if( $type == 'departure' )
        {
            $route  = get_route_detail_content( $d['id'], $depart_port, $destination_port );
            $bddate = date( 'Y-m-d', strtotime( $date_of_depart ) );

            $bdfrom_id = $depart_port;
            $bdto_id   = $destination_port;
        }
        elseif( $type == 'return' )
        {
            $route  = get_route_detail_content( $d['id'], $return_port, $destination_port_rtn );
            $bddate = date( 'Y-m-d', strtotime( $date_of_return ) );

            $bdfrom_id = $return_port;
            $bdto_id   = $destination_port_rtn;
        }

        $ptransport_fee = isset( $d['pickup']['transport_fee'] ) ? $d['pickup']['transport_fee'] : 0;
        $dtransport_fee = isset( $d['drop-off']['transport_fee'] ) ? $d['drop-off']['transport_fee'] : 0;
        $transport_fee  = $ptransport_fee + $dtransport_fee;
        $grandtotal     = ( $d['total'] + $transport_fee ) - $d['discount'];

        if( isset( $agid ) && !empty( $agid ) )
        {
            if( isset( $sagid ) && !empty( $sagid ) )
            {
                $agent = get_agent( $sagid );
            }
            else
            {
                $agent = get_agent( $agid );
            }

            if( $agent['agpayment_type'] == 'Pre Paid' )
            {
                if( $agent['agtype'] == 'Net Rate' )
                {
                    $commission = 0;
                    $commission_type = '0';
                }
                else
                {
                    if( isset( $agent['agfreelance'][0] ) )
                    {
                        $commission = $agent['agfreelance'][0]['agfcommission'];
                        $commission_type = $agent['agfreelance'][0]['agfcommission_type'];
                    }
                    else
                    {
                        $commission = 0;
                        $commission_type = '0';
                    }
                }
            }
            else
            {
                $commission = 0;
                $commission_type = '0';
            }
        }
        else
        {
            $commission = 0;
            $commission_type = '0';
        }

        if( in_array( $payment, $compliment ) )
        {
            $d['total']             = $transport_fee;
            $grandtotal             = $d['total'];
            $d['discount']          = 0;
            $d['adult_price']       = 0;
            $d['child_price']       = 0;
            $d['infant_price']      = 0;
            $d['adult_sell_price']  = 0;
            $d['child_sell_price']  = 0;
            $d['infant_sell_price'] = 0;
            $d['adult_disc']        = 0;
            $d['child_disc']        = 0;
            $d['infant_disc']       = 0;
        }

        $s = 'INSERT INTO ticket_booking_detail(
                bid,
                boid,
                rid,
                sid,
                bdtype,
                bdfrom,
                bdto,
                bdfrom_id,
                bdto_id,
                bddeparttime,
                bdarrivetime,
                bddate,
                bdtranstype,
                num_adult,
                num_child,
                num_infant,
                price_per_adult,
                price_per_child,
                price_per_infant,
                selling_price_per_adult,
                selling_price_per_child,
                selling_price_per_infant,
                net_price_per_adult,
                net_price_per_child,
                net_price_per_infant,
                disc_price_per_adult,
                disc_price_per_child,
                disc_price_per_infant,
                commission,
                commission_type,
                subtotal,
                transport_fee,
                discount,
                total,
                bdpstatus,
                bdrevstatus) VALUES( %d, %d, %d, %d, %s, %s, %s, %d, %d, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
        $q = $db->prepare_query( $s,
                $bid,
                $d['boid'],
                $d['id'],
                $d['sid'],
                $type,
                $route['depart'],
                $route['arrive'],
                $bdfrom_id,
                $bdto_id,
                $route['depart_time'],
                $route['arrive_time'],
                $bddate,
                $d['transport'],
                $adult_num,
                $child_num,
                $infant_num,
                $d['adult_price'],
                $d['child_price'],
                $d['infant_price'],
                $d['adult_sell_price'],
                $d['child_sell_price'],
                $d['infant_sell_price'],
                $d['adult_sell_price'],
                $d['child_sell_price'],
                $d['infant_sell_price'],
                $d['adult_disc'],
                $d['child_disc'],
                $d['infant_disc'],
                $commission,
                $commission_type,
                $d['total'],
                $transport_fee,
                $d['discount'],
                $grandtotal,
                $status,
                $rev_status);

        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            return 0;
        }
        else
        {
            $bdid = $db->insert_id();
            $bpid = save_booking_passenger( $bdid, $type, $post );
            $btid = save_booking_transport( $bdid, $type, $post, $d );

            if( empty( $bdid ) || empty( $bpid ) || empty( $btid ) )
            {
                $error++;
            }
        }
    }

    if( $error == 0 )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

function change_booking_source( $bid = '', $agid = '' )
{
    global $db;

    $s = 'UPDATE ticket_booking AS a SET a.chid = %d, a.agid = %d WHERE a.bid = %d';
    $q = $db->prepare_query( $s, $ag['chid'], $agid, $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function save_booking_transport( $bdid, $type, $post, $data )
{
    global $db;

    extract( $post );

    $btid = array();

    if( isset( $data['pickup'] ) )
    {
        $pickup_rpto            = isset( $data['pickup']['rpto'] ) ? $data['pickup']['rpto'] : '';
        $pickup_rpfrom          = isset( $data['pickup']['rpfrom'] ) ? $data['pickup']['rpfrom'] : '';
        $pickup_area_id         = isset( $data['pickup']['area_id'] ) ? $data['pickup']['area_id'] : NULL;
        $pickup_hotel_id        = isset( $data['pickup']['hotel_id'] ) ? $data['pickup']['hotel_id'] : NULL;
        $pickup_transport_fee   = isset( $data['pickup']['transport_fee'] ) ? $data['pickup']['transport_fee'] : 0;
        $pickup_transport_type  = isset( $data['pickup']['transport_type'] ) ? $data['pickup']['transport_type'] : 2;
        $pickup_flighttime      = isset( $data['pickup']['flight_time'] ) && !empty( $data['pickup']['flight_time'] ) ? date( 'Y-m-d H:i:s', strtotime( $data['pickup']['flight_time'] ) ) : '';

        if( $type == 'departure' )
        {
            $pickup_drivername   = isset( $dep_pickup_driver_name ) ? $dep_pickup_driver_name : '';
            $pickup_driverphone  = isset( $dep_pickup_driver_phone ) ? $dep_pickup_driver_phone : '';
            $pickup_hotelname    = isset( $dep_pickup_hotel_name ) ? $dep_pickup_hotel_name : '';
            $pickup_hotelphone   = isset( $dep_pickup_hotel_phone ) ? $dep_pickup_hotel_phone : '';
            $pickup_hoteladdress = isset( $dep_pickup_hotel_address ) ? $dep_pickup_hotel_address : '';
        }
        elseif( $type == 'return' )
        {
            $pickup_drivername   = isset( $rtn_pickup_driver_name ) ? $rtn_pickup_driver_name : '';
            $pickup_driverphone  = isset( $rtn_pickup_driver_phone ) ? $rtn_pickup_driver_phone : '';
            $pickup_hotelname    = isset( $rtn_pickup_hotel_name ) ? $rtn_pickup_hotel_name : '';
            $pickup_hotelphone   = isset( $rtn_pickup_hotel_phone ) ? $rtn_pickup_hotel_phone : '';
            $pickup_hoteladdress = isset( $rtn_pickup_hotel_address ) ? $rtn_pickup_hotel_address : '';
        }            
                        
        $tparam = array(            
            'bdid'           => $bdid,
            'bttype'         => 'pickup',
            'btrpto'         => $pickup_rpto,
            'btrpfrom'       => $pickup_rpfrom,
            'taid'           => $pickup_area_id,
            'hid'            => $pickup_hotel_id,
            'bthotelname'    => $pickup_hotelname,
            'bthotelphone'   => $pickup_hotelphone,
            'btdrivername'   => $pickup_drivername,
            'btflighttime'   => $pickup_flighttime,
            'btdriverphone'  => $pickup_driverphone,
            'bthoteladdress' => $pickup_hoteladdress,
            'bttrans_fee'    => $pickup_transport_fee,
            'bttrans_type'   => $pickup_transport_type
        );

        if( empty( $pickup_flighttime ) )
        {
            unset( $tparam['btflighttime'] );
        }

        if( $pickup_transport_type == 2 || empty( $pickup_hotel_id ) || empty( $pickup_area_id ) )
        {
            unset( $tparam['hid'] );
            unset( $tparam['taid'] );
        }

        $q = 'INSERT INTO ticket_booking_transport(' . implode( ',', array_keys( $tparam ) ) . ') VALUES ("' . implode( '" , "', $tparam ) . '")';
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            return 0;
        }
        else
        {
            $btid[] = $db->insert_id();
        }
    }

    if( isset( $data['drop-off'] ) )
    {
        $dropoff_rpto           = isset( $data['drop-off']['rpto'] ) ? $data['drop-off']['rpto'] : '';
        $dropoff_rpfrom         = isset( $data['drop-off']['rpfrom'] ) ? $data['drop-off']['rpfrom'] : '';
        $dropoff_area_id        = isset( $data['drop-off']['area_id'] ) ? $data['drop-off']['area_id'] : NULL;
        $dropoff_hotel_id       = isset( $data['drop-off']['hotel_id'] ) ? $data['drop-off']['hotel_id'] : NULL;
        $dropoff_transport_fee  = isset( $data['drop-off']['transport_fee'] ) ? $data['drop-off']['transport_fee'] : 0;
        $dropoff_transport_type = isset( $data['drop-off']['transport_type'] ) ? $data['drop-off']['transport_type'] : 2;
        $dropoff_flighttime     = isset( $data['drop-off']['flight_time'] ) && !empty( $data['drop-off']['flight_time'] ) ? date( 'Y-m-d H:i:s', strtotime( $data['drop-off']['flight_time'] ) ) : '';

        if( $type == 'departure' )
        {
            $dropoff_drivername   = isset( $dep_dropoff_driver_name ) ? $dep_dropoff_driver_name : '';
            $dropoff_driverphone  = isset( $dep_dropoff_driver_phone ) ? $dep_dropoff_driver_phone : '';
            $dropoff_hotelname    = isset( $dep_dropoff_hotel_name ) ? $dep_dropoff_hotel_name : '';
            $dropoff_hotelphone   = isset( $dep_dropoff_hotel_phone ) ? $dep_dropoff_hotel_phone : '';
            $dropoff_hoteladdress = isset( $dep_dropoff_hotel_address ) ? $dep_dropoff_hotel_address : '';
        }
        elseif( $type == 'return' )
        {
            $dropoff_drivername   = isset( $rtn_dropoff_driver_name ) ? $rtn_dropoff_driver_name : '';
            $dropoff_driverphone  = isset( $rtn_dropoff_driver_phone ) ? $rtn_dropoff_driver_phone : '';
            $dropoff_hotelname    = isset( $rtn_dropoff_hotel_name ) ? $rtn_dropoff_hotel_name : '';
            $dropoff_hotelphone   = isset( $rtn_dropoff_hotel_phone ) ? $rtn_dropoff_hotel_phone : '';
            $dropoff_hoteladdress = isset( $rtn_dropoff_hotel_address ) ? $rtn_dropoff_hotel_address : '';
        }            
                        
        $tparam = array(            
            'bdid'           => $bdid,
            'bttype'         => 'drop-off',
            'btrpto'         => $dropoff_rpto,
            'btrpfrom'       => $dropoff_rpfrom,
            'taid'           => $dropoff_area_id,
            'hid'            => $dropoff_hotel_id,
            'bthotelname'    => $dropoff_hotelname,
            'bthotelphone'   => $dropoff_hotelphone,
            'btdrivername'   => $dropoff_drivername,
            'btflighttime'   => $dropoff_flighttime,
            'btdriverphone'  => $dropoff_driverphone,
            'bthoteladdress' => $dropoff_hoteladdress,
            'bttrans_fee'    => $dropoff_transport_fee,
            'bttrans_type'   => $dropoff_transport_type
        );

        if( empty( $dropoff_flighttime ) )
        {
            unset( $tparam['btflighttime'] );
        }

        if( $dropoff_transport_type == 2 || empty( $dropoff_hotel_id ) || empty( $dropoff_area_id ) )
        {
            unset( $tparam['hid'] );
            unset( $tparam['taid'] );
        }

        $q = 'INSERT INTO ticket_booking_transport(' . implode( ',', array_keys( $tparam ) ) . ') VALUES ("' . implode( '" , "', $tparam ) . '")';
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            return 0;
        }
        else
        {
            $btid[] = $db->insert_id();
        }
    }

    return $btid;
}

function save_booking_passenger( $bdid, $type, $post )
{
    global $db;

    extract( $post );

    $bpid = array();

    foreach( $gfullname[$type] as $ptype => $names )
    {
        foreach( $names as $idx => $name )
        {
            $birthday = empty( $gbirth[$type][$ptype][$idx] ) ? '' : date( 'Y-m-d', strtotime( $gbirth[$type][$ptype][$idx] ) );

            $s = 'INSERT INTO ticket_booking_passenger(
                    bdid,
                    bptype,
                    bpname,
                    bpgender,
                    bpbirthdate,
                    lcountry_id ) VALUES( %d, %s, %s, %s, %s, %d )';
            $q = $db->prepare_query( $s,
                    $bdid,
                    $ptype,
                    $name,
                    $ggender[$type][$ptype][$idx],
                    $birthday,
                    $gcountry[$type][$ptype][$idx] );
            $r = $db->do_query( $q );

            if( is_array( $r ) )
            {
                return 0;
            }
            else
            {
                $bpid[] = $db->insert_id();
            }
        }
    }

    return $bpid;
}

function save_booking_agent_transaction( $id, $bcode, $post, $sess )
{
    global $db;

    extract( $post );
    extract( $sess );

    if( isset( $agid ) && !empty( $agid ) )
    {
        //-- Save Agent Transaction
        $atgname = array();

        foreach( $gfullname['departure'] as $ptype => $names )
        {
            foreach( $names as $idx => $name )
            {
                $atgname[] = $name;
            }
        }

        if( empty( $sagid ) )
        {
            $s = 'INSERT INTO ticket_agent_transaction( agid, bcode, bid, atdate, atgname, atdebet, atcredit, atstatus ) VALUES( %d, %s, %d, %s, %s, %s, %s, %d )';
            $q = $db->prepare_query( $s, $agid, $bcode, $id, date( 'Y-m-d' ), implode( ', ', $atgname ), $grandtotal, 0, 1 );
        }
        else
        {
            $s = 'INSERT INTO ticket_agent_transaction( agid, bcode, bid, atdate, atgname, atdebet, atcredit, atstatus ) VALUES( %d, %s, %d, %s, %s, %s, %s, %d )';
            $q = $db->prepare_query( $s, $sagid, $bcode, $id, date( 'Y-m-d' ), implode( ', ', $atgname ), $grandtotal, 0, 1 );
        }

        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            return 0;
        }
        else
        {
            //-- Save Notification For memo purpose
            $tid     = $db->insert_id();
            $booked  = ticket_booking_all_data( $id );
            $notpass = false;

            foreach( $booked['detail'] as $detail )
            {
                foreach( $detail as $t )
                {
                    $passenger = $t['num_adult'] + $t['num_child'] + $t['num_infant'];

                    if( empty( $sagid ) )
                    {
                        if( !is_available_allotment_agent( $t['bddate'], $t['sid'], $passenger, $agid ) )
                        {
                            $notpass = true;
                        }
                    }
                    else
                    {
                        if( !is_available_allotment_agent( $t['bddate'], $t['sid'], $passenger, $sagid ) )
                        {
                            $notpass = true;
                        }
                    }
                }
            }

            if( $notpass )
            {
                $agname   = get_agent( ( empty( $sagid ) ? $agid : $sagid ), 'agname' );
                $ncontent = sprintf( '<p>Agent <b>"%s"</b> is make the reservation exceeds the maximum limit of allotment that has been given. Please verify <a href="%s">this booking</a> now</p>', $agname, HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $id );

                save_ticket_notification( $id, 'Overload Allotment', $ncontent, 0, 0, date( 'Y-m-d H:i:s' ) );
            }

            return $tid;
        }
    }
    else
    {
        return 1;
    }
}

function save_booking_payment( $bid, $ppayid = '', $ppaycode = '', $ppaydate = '', $ptotal = 0, $ptotalusd = 0, $pmethod = '', $pstatus = '0', $ppayemail = '', $pnoaccount = '', $pbankname = '', $paccountname = '', $pnote = '', $prefcode='', $pvirtualacc = '', $pvirtualaccvaliddate = 0, $pvirtualaccvalidtime = 0, $papproval = '', $pchname = '', $pbrand = '', $pwords = '', $pmcn = '', $presult = '' )
{
    global $db;

    $s = 'INSERT INTO ticket_booking_payment(
            bid,
            ppayid,
            ppaycode,
            ppayemail,
            ppaydate,
            ptotal,
            ptotalusd,
            pnoaccount,
            pstatus,
            pmethod,
            pbankname,
            prefcode,
            pvirtualacc,
            pvirtualaccvaliddate,
            pvirtualaccvalidtime,
            paccountname,
            pnote,
            papproval,
            pchname,
            pbrand,
            pwords,
            pmcn,
            presult) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
    $q = $db->prepare_query( $s,
            $bid,
            $ppayid,
            $ppaycode,
            $ppayemail,
            $ppaydate,
            $ptotal,
            $ptotalusd,
            $pnoaccount,
            $pstatus,
            $pmethod,
            $pbankname,
            $prefcode,
            $pvirtualacc,
            $pvirtualaccvaliddate,
            $pvirtualaccvalidtime,
            $paccountname,
            $pnote,
            $papproval,
            $pchname,
            $pbrand,
            $pwords,
            $pmcn,
            $presult);
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        return $db->insert_id();
    }
}

function save_booking_payment_2()
{
    global $db;

    if( isset( $_POST ) )
    {
        $fields = array();
        $values = array();
        $flogs  = array();
        $fname  = array(
            'pvirtualacc' => 'Transaction ID / Virtual Account',
            'prefcode'    => 'Invoice Number/ Ref. Code',
            'ppaydate'    => 'Date of payment',
            'pmethod'     => 'Payment Method',
            'ppayemail'   => 'Email payment',
            'ppaycode'    => 'Payment Code',
            'ptotal'      => 'Total Paid'
        );

        foreach( $_POST as $field => $val )
        {
            if( !in_array( $field, array( 'pkey', 'updateonhand', 'bticket' ) ) && $val != '' )
            {
                $fields[] = $field;

                if( $field == 'ppaydate' )
                {
                    $values[] = strtotime( $val );
                    $flogs[]  = $fname[ $field ] . ' : ' . date( 'd F Y', strtotime( $val ) );
                }
                else if( $field == 'pmethod' )
                {
                    list( $id, $method ) = explode( '|', $val );

                    $values[] = $method;
                    $flogs[]  = $fname[ $field ] . ' : ' . $method;
                }
                else
                {
                    $values[] = $val;

                    if( !in_array( $field, array( 'bid', 'pstatus' ) ) )
                    {
                        $flogs[]  = $fname[ $field ] . ' : ' . $val;
                    }
                }
            }
        }

        if( !empty( $fields ) && !empty( $values ) )
        {
            $fields[] = 'ppayid';
            $values[] = generate_payment_id();

            $q = 'INSERT INTO ticket_booking_payment(' . implode( ',', $fields ) . ') VALUES ("' . implode( '" , "', $values ) . '")';
            $r = $db->do_query( $q );

            if( !is_array( $r ) )
            {
                if( $_POST['updateonhand'] == 1 )
                {
                    $p = ticket_booking_payment_detail( $_POST['bid'] );

                    if( empty( $p ) )
                    {
                        ticket_booking_update_on_hand_payment( $_POST['bid'], $_POST['ptotal'] );
                    }
                    else
                    {
                        ticket_booking_update_on_hand_payment( $_POST['bid'], array_sum( array_column( $p, 'ptotal' ) ) );
                    }
                }

                if( !empty( $flogs ) )
                {
                    save_log( $_POST[ 'bid' ], 'reservation', 'Add Booking Payment #' . $_POST['bticket'] . ' : <br/>' . implode( '<br/>', array_unique( $flogs ) ) );
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

function update_booking_payment( $pid, $bid, $ppaycode = '', $ppaydate = '', $ptotal = 0, $ptotalusd = 0, $pmethod = '', $pstatus = '0', $ppayemail = '', $pnoaccount = '', $pbankname = '', $paccountname = '', $pnote = '', $prefcode='', $pvirtualacc = '', $pvirtualaccvaliddate = 0, $pvirtualaccvalidtime = 0, $papproval = '', $pchname = '', $pbrand = '', $pwords = '', $pmcn = '', $presult = '' )
{
    global $db;

    $s = 'UPDATE ticket_booking_payment SET
            ppaycode = %s,
            ppayemail = %s,
            ppaydate = %s,
            ptotal = %s,
            ptotalusd = %s,
            pnoaccount = %s,
            pstatus = %s,
            pmethod = %s,
            pbankname = %s,
            prefcode = %s,
            pvirtualacc = %s,
            pvirtualaccvaliddate = %s,
            pvirtualaccvalidtime = %s,
            paccountname = %s,
            pnote = %s,
            papproval = %s,
            pchname = %s,
            pbrand = %s,
            pwords = %s,
            pmcn = %s,
            presult = %s
          WHERE pid = %d AND bid = %d';
    $q = $q = $db->prepare_query( $s,
            $ppaycode,
            $ppayemail,
            $ppaydate,
            $ptotal,
            $ptotalusd,
            $pnoaccount,
            $pstatus,
            $pmethod,
            $pbankname,
            $prefcode,
            $pvirtualacc,
            $pvirtualaccvaliddate,
            $pvirtualaccvalidtime,
            $paccountname,
            $pnote,
            $papproval,
            $pchname,
            $pbrand,
            $pwords,
            $pmcn,
            $presult,
            $pid,
            $bid);
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        return true;
    }
}

function update_booking_payment_2()
{
    global $db;

    if( isset( $_POST ) )
    {
        $odata  = ticket_booking_payment_by_id( $_POST[ 'pid' ] );
        $fields = array();
        $flogs  = array();
        $fname  = array(
            'pvirtualacc' => 'Transaction ID / Virtual Account',
            'prefcode'    => 'Invoice Number/ Ref. Code',
            'ppaydate'    => 'Date of payment',
            'pmethod'     => 'Payment Method',
            'ppayemail'   => 'Email payment',
            'ppaycode'    => 'Payment Code',
            'ptotal'      => 'Total Paid'
        );

        foreach( $_POST as $field => $val )
        {
            if( !in_array( $field, array( 'pid', 'pkey', 'bid', 'updateonhand', 'bticket' ) ) )
            {
                if( $field == 'ppaydate' )
                {
                    $fields[] = $db->prepare_query( 'ppaydate = %s', strtotime( $val ) );

                    if( $odata[ $field ] != $val )
                    {
                        $flogs[] = $fname[ $field ] . ' : ' . date( 'd F Y', $odata[ $field ] ) . ' --> ' . $val;
                    }
                }
                else if( $field == 'pmethod' )
                {
                    list( $id, $method ) = explode( '|', $val );

                    $fields[] = $db->prepare_query( $field . ' = %s', $method );

                    if( $odata[ $field ] != $val )
                    {
                        $flogs[] = $fname[ $field ] . ' : ' . $odata[ $field ] . ' --> ' . $method;
                    }
                }
                else if( $field == 'ptotal' )
                {
                    $fields[] = $db->prepare_query( $field . ' = %s', $val );

                    if( $odata[ $field ] != $val )
                    {
                        $flogs[] = $fname[ $field ] . ' : ' . number_format( $odata[ $field ], 0, ',', '.' ) . ' --> ' . number_format( $val, 0, ',', '.' );
                    }
                }
                else
                {
                    $fields[] = $db->prepare_query( $field . ' = %s', $val );

                    if( $odata[ $field ] != $val )
                    {
                        $flogs[] = $fname[ $field ] . ' : ' . ( empty( $odata[ $field ] ) ? 'Empty Value' : $odata[ $field ] ) . ' --> ' . ( empty( $val ) ? 'Empty Value' : $val );
                    }
                }
            }
        }

        if( !empty( $fields ) )
        {
            $q = 'UPDATE ticket_booking_payment SET ' . implode( ',', $fields ) . ' WHERE pid = '. $_POST[ 'pid' ];
            $r = $db->do_query( $q );

            if( !is_array( $r ) )
            {
                if( $_POST['updateonhand'] == 1 )
                {
                    $p = ticket_booking_payment_detail( $_POST['bid'] );

                    if( empty( $p ) )
                    {
                        ticket_booking_update_on_hand_payment( $_POST['bid'], $_POST['ptotal'] );
                    }
                    else
                    {
                        ticket_booking_update_on_hand_payment( $_POST['bid'], array_sum( array_column( $p, 'ptotal' ) ) );
                    }
                }

                if( !empty( $flogs ) )
                {
                    save_log( $_POST[ 'bid' ], 'reservation', 'Update Booking Payment #' . $_POST['bticket'] . ' : <br/>' . implode( '<br/>', array_unique( $flogs ) ) );
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}

function get_booking_notification_to_admin( $data )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/email/new-booking-notification-to-admin.html', 'admin-email' );

    add_block( 'email-block', 'ae-block', 'admin-email' );

    add_variable( 'bbname', $data['bbname'] );
    add_variable( 'bbemail', $data['bbemail'] );
    add_variable( 'bbphone', $data['bbphone'] );
    add_variable( 'bticket', $data['bticket'] );

    if( $data['agpayment_type'] == 'Credit' )
    {
        add_variable( 'payment_detail', '' );
        add_variable( 'payment_method', get_booking_payment_method( $data ) );
        add_variable( 'paid_amount', number_format( ( isset( $data['payment'] ) && !empty( $data['payment'] ) ? array_sum( array_column( $data['payment'], 'ptotal' ) ) : 0 ), 0, ',', '.' ) );
    }
    else
    {
        add_variable( 'payment_detail', 'display:none;' );
    }

    add_variable( 'bdate', date( 'd F Y', strtotime( $data['bdate'] ) ) );

    add_variable( 'over_capacity_note_css', $data['ntitle'] == 'Overload Allotment' ? '' : 'display:none;' );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'detail_link', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'] );

    parse_template( 'email-block', 'ae-block' );

    return return_template( 'admin-email' );
}

function get_payment_notification_to_admin( $data )
{
    $payment = isset( $data['payment'] ) && !empty( $data['payment'] ) ? end( $data['payment'] ) : '';

    if( ( !empty( $payment ) && $payment == '1' ) || $data['bstatus'] == 'ca' || ( $data['bonhandtotal'] == $data['btotal'] ) )
    {
        $email_template = 'admin-' . $data['bid'] . '-email';
        $email_block    = 'ae-' . $data['bid'] . '-block';

        set_template( PLUGINS_PATH . '/ticket/tpl/email/new-payment-notification-to-admin.html', $email_template );

        get_email_booking_detail_content( $data, $email_template );

        add_block( 'email-block', $email_block, $email_template );

        add_variable( 'other_accommodation_css', $data['chid'] == 15 ? '' : 'display:none;' );
        add_variable( 'detail_text', 'Here is the proof of transaction # : <strong>' . $data['bticket'] . '</strong>' );
        add_variable( 'detail_link', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'] );

        parse_template( 'email-block', $email_block );

        return return_template( $email_template );
    }
}

function get_failed_payment_notification_to_admin( $data )
{
    if( $data['bstatus'] == 'pf' )
    {
        $email_template = 'admin-' . $data['bid'] . '-email';
        $email_block    = 'ae-' . $data['bid'] . '-block';

        set_template( PLUGINS_PATH . '/ticket/tpl/email/new-payment-notification-to-admin.html', $email_template );

        get_email_booking_detail_content( $data, $email_template );

        add_block( 'email-block', $email_block, $email_template );

        add_variable( 'detail_text', 'Status : Payment Failed' );
        add_variable( 'detail_link', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'] );

        parse_template( 'email-block', $email_block );

        return return_template( $email_template );
    }
}

function get_no_payment_notification_to_admin( $data )
{
    if( $data['bstatus'] == 'ol' )
    {
        $email_template = 'admin-' . $data['bid'] . '-email';
        $email_block    = 'ae-' . $data['bid'] . '-block';

        set_template( PLUGINS_PATH . '/ticket/tpl/email/new-payment-notification-to-admin.html', $email_template );

        get_email_booking_detail_content( $data, $email_template );

        add_block( 'email-block', $email_block, $email_template );

        add_variable( 'detail_text', 'Status : Past the payout limit ( ' . date( 'd F Y H:i:s', $data['bpvaliddate'] ) . ' )' );
        add_variable( 'detail_link', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'] );

        parse_template( 'email-block', $email_block );

        return return_template( $email_template );
    }
}

function get_payment_notification_to_admin_subject( $data )
{
    if( $data['bstatus'] == 'pf' || $data['bstatus'] == 'ol' )
    {
        return 'Booking Failed #' . $data['bticket'] . ' - ' . $data['bbname'];
    }
    else
    {
        return 'Payment Confirmation and Ticket #' . $data['bticket'] . ' - ' . $data['bbname'];
    }
}

function get_booking_notification_to_client( $data )
{
    $email_template = 'client-' . $data['bid'] . '-email';
    $email_block    = 'ce-' . $data['bid'] . '-block';

    set_template( PLUGINS_PATH . '/ticket/tpl/email/new-booking-notification-to-client.html', $email_template );

    get_email_booking_detail_content( $data, $email_template );

    add_block( 'email-block', $email_block, $email_template );

    if( $data['bpaymethod'] == 2 )
    {
        $link   = HTSERVER . site_url() . '/booking/payment/?type=nicepay&id=' . $data['bid'];
        $format = '<p>If you have not paid, please continue the payment process by clicking <a href="%s" style="color:rgb(16, 163, 193);">this link</a></p>';

        add_variable( 'payment_link', sprintf( $format, $link ) );
    }
    elseif( $data['bpaymethod'] == 3 || get_booking_payment_method( $data ) == 'Paypal' )
    {
        $link   = HTSERVER . site_url() . '/booking/payment/?type=paypal&id=' . $data['bid'];
        $format = '<p>If you have not paid, please continue the payment process by clicking <a href="%s" style="color:rgb(16, 163, 193);">this link</a></p>';

        add_variable( 'payment_link', sprintf( $format, $link ) );
    }
    elseif( $data['bpaymethod'] == 5 )
    {
        $method = get_meta_data( 'cc_payment_gateway_opt' );

        $link   = HTSERVER . site_url() . '/booking/payment/?type=' . $method . '&id=' . $data['bid'];
        $format = '<p>If you have not paid, please continue the payment process by clicking <a href="%s" style="color:rgb(16, 163, 193);">this link</a></p>';

        add_variable( 'payment_link', sprintf( $format, $link ) );
    }
    else
    {
        add_variable( 'payment_link', '' );
    }

    parse_template( 'email-block', $email_block );

    return return_template( $email_template );
}

function get_email_booking_detail_content( $data, $email_template )
{
    extract( $data );

    $time = mt_rand( 10, 100 );

    add_block( 'email-departure-loop-block', 'edl-' . $bid . '-' . $time . '-block', $email_template );
    add_block( 'email-departure-block', 'ed-' . $bid . '-' . $time . '-block', $email_template );
    add_block( 'email-return-loop-block', 'erl-' . $bid . '-' . $time . '-block', $email_template );
    add_block( 'email-return-block', 'er-' . $bid . '-' . $time . '-block', $email_template );

    extract( $detail );

    $bstatus_message = ticket_booking_status( $bstatus );

    add_variable( 'site_url', site_url() );
    add_variable( 'web_title', web_title() );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'cemail', get_meta_data( 'email_reservation' ) );

    add_variable( 'booking_id', $bid );
    add_variable( 'bstatus', $bstatus );
    add_variable( 'bticket', $bticket );
    add_variable( 'bstatus_message', $bstatus_message );

    add_variable( 'bbname', $bbname );
    add_variable( 'bbemail', $bbemail );
    add_variable( 'bbphone', $bbphone );

    if( empty( $agid ) )
    {
        add_variable( 'bchannel', $chname );
    }
    else
    {
        $agid = empty( $sagid ) ? $agid : $sagid;

        add_variable( 'bchannel', get_agent( $agid, 'agname' ) );
    }

    add_variable( 'bbrevtype', $bbrevtype );
    add_variable( 'bbrevstatus', $bbrevstatus );
    add_variable( 'booking_staf', empty( $bbooking_staf ) ? '-' : $bbooking_staf );
    add_variable( 'booking_status_message', $bstatus_message );
    add_variable( 'bpaymethod', get_booking_payment_method( $data ) );

    add_variable( 'bremark', $bremark );
    add_variable( 'bhotelname', $bhotelname );
    add_variable( 'bpaydetail', ticket_payment_detail_content( $data, true ) );
    add_variable( 'bhoteladdress', empty( $bhoteladdress ) ? '' : $bhoteladdress . '<br />' );
    add_variable( 'bhotelphone', empty( $bhotelphone ) ? '' : 'P. <a href="tel:' . $bhotelphone . '" style="color:rgb(16, 163, 193);">' . $bhotelphone . '</a><br />' );
    add_variable( 'bhotelemail', empty( $bhotelemail ) ? '' : 'E. <a href="mailto:' . $bhotelemail . '" style="color:rgb(16, 163, 193);">' . $bhotelemail . '</a><br />' );

    add_variable( 'bremarkcss', empty( $bremark ) ? 'display:none;' : '' );
    add_variable( 'baccommodationcss', empty( $bhotelname ) ? 'display:none;' : '' );

    add_variable( 'btype', ticket_booking_type( $btype ) );
    add_variable( 'class_booking_status', generateSefUrl( $bstatus_message ) );
    add_variable( 'bdate', date( 'd M Y', strtotime( $bdate ) ) );
    add_variable( 'bbrevagent', empty( $bbrevagent ) ? '-' : $bbrevagent );

    //-- Go Trip
    if( isset( $departure ) )
    {
        foreach( $departure as $key => $dp_trip )
        {
            extract( $dp_trip );

            $var = get_variable_by_port_type( $bdfrom_id, $bdto_id );

            $dep_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
            $dep_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
            $dep_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
            $dep_pw_discount = $discount == 0 ? '' : '<small style="display:block; color:red; text-decoration:line-through; font-size:inherit;">' . number_format( $subtotal, 0, ',', '.' ) . '</small>';

            add_variable( 'dep_bdto', $bdto );
            add_variable( 'dep_bdfrom', $bdfrom );
            add_variable( 'dep_num_adult', $num_adult );
            add_variable( 'dep_num_child', $num_child );
            add_variable( 'dep_num_infant', $num_infant );
            add_variable( 'dep_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
            add_variable( 'dep_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );

            add_variable( 'dep_discount', $discount );
            add_variable( 'dep_price_per_adult', $price_per_adult );
            add_variable( 'dep_price_per_child', $price_per_child );
            add_variable( 'dep_price_per_infant', $price_per_infant );

            add_variable( 'dep_pa_display', $dep_pa_display );
            add_variable( 'dep_pc_display', $dep_pc_display );
            add_variable( 'dep_pi_display', $dep_pi_display );
            add_variable( 'dep_pt_display', $dep_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );
            add_variable( 'dep_passenger', ticket_passenger_list_content( $passenger, true ) );

            add_variable( 'dep_bdtype', ucfirst( $bdtype ) );
            add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
            add_variable( 'dep_trans_detail', ticket_detail_transport_email_content( $transport, $bdfrom, $bdto, $bdate, $bddate ) );

            add_variable( 'baaccommodationarea', $var['drop_sts'] ? $bdfrom : $bdto );

            if( $bdpstatus == 'cn' )
            {
                add_variable( 'dep_cancel_mention_css', '' );
            }
            else
            {
                add_variable( 'dep_cancel_mention_css', 'display:none;' );
            }

            add_variable( 'recomendation_villa', get_recommendation_villa( $bdto ) );

            parse_template( 'email-departure-loop-block', 'edl-' . $bid . '-' . $time . '-block', true );
        }

        parse_template( 'email-departure-block', 'ed-' . $bid . '-' . $time . '-block' );
    }

    //-- Back Trip
    if( isset( $return ) )
    {
        foreach( $return as $key => $rt_trip )
        {
            extract( $rt_trip );

            $rtn_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
            $rtn_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
            $rtn_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
            $rtn_pw_discount = $discount == 0 ? '' : '<small style="display:block; color:red; text-decoration:line-through; font-size:inherit;">' . number_format( $subtotal, 0, ',', '.' ) . '</small>';

            add_variable( 'rtn_bdto', $bdto );
            add_variable( 'rtn_bdfrom', $bdfrom );
            add_variable( 'rtn_num_adult', $num_adult );
            add_variable( 'rtn_num_child', $num_child );
            add_variable( 'rtn_num_infant', $num_infant );
            add_variable( 'rtn_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
            add_variable( 'rtn_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );

            add_variable( 'rtn_discount', $discount );
            add_variable( 'rtn_price_per_adult', $price_per_adult );
            add_variable( 'rtn_price_per_child', $price_per_child );
            add_variable( 'rtn_price_per_infant', $price_per_infant );

            add_variable( 'rtn_pa_display', $rtn_pa_display );
            add_variable( 'rtn_pc_display', $rtn_pc_display );
            add_variable( 'rtn_pi_display', $rtn_pi_display );
            add_variable( 'rtn_pt_display', $rtn_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );
            add_variable( 'rtn_passenger', ticket_passenger_list_content( $passenger, true ) );

            add_variable( 'rtn_bdtype', ucfirst( $bdtype ) );
            add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );
            add_variable( 'rtn_trans_detail', ticket_detail_transport_email_content( $transport, $bdfrom, $bdto, $bdate, $bddate ) );

            if( $bdpstatus == 'cn' )
            {
                add_variable( 'rtn_cancel_mention_css', '' );
            }
            else
            {
                add_variable( 'rtn_cancel_mention_css', 'display:none;' );
            }

            parse_template( 'email-return-loop-block', 'erl-' . $bid . '-' . $time . '-block', true );
        }

        parse_template( 'email-return-block', 'er-' . $bid . '-' . $time . '-block' );
    }

    if( empty( $pmcode ) )
    {
        add_variable( 'sub_css', 'display:none;' );
        add_variable( 'subtotal_display', number_format( $bsubtotal, 0, ',', '.' ) );
        add_variable( 'discount_display', number_format( $bdiscount, 0, ',', '.' ) );
        add_variable( 'grandtotal_display', number_format( $btotal, 0, ',', '.' ) );
        add_variable( 'onhandtotal_display', number_format( $bonhandtotal, 0, ',', '.' ) );
        add_variable( 'remaintotal_display', number_format( ( $btotal - $bonhandtotal ), 0, ',', '.' ) );
    }
    else
    {
        add_variable( 'pmcode', 'Code : ' . $pmcode );
        add_variable( 'subtotal_display', number_format( $bsubtotal, 0, ',', '.' ) );
        add_variable( 'discount_display', number_format( $bdiscount, 0, ',', '.' ) );
        add_variable( 'grandtotal_display', number_format( $btotal, 0, ',', '.' ) );
        add_variable( 'onhandtotal_display', number_format( $bonhandtotal, 0, ',', '.' ) );
        add_variable( 'remaintotal_display', number_format( ( $btotal - $bonhandtotal ), 0, ',', '.' ) );
    }
}

function get_recommendation_villa( $bdto = '' )
{
    if( $bdto == 'Gili Trawangan' )
    {
        return '
        <table border="0" cellpadding="10" cellspacing="0" width="800" id="accoDetail" style="background-color:rgb(255,255,255); {other_accommodation_css}">
          <tr>
              <td align="center" valign="top" style="border-bottom:1px solid rgb(198, 216, 220);">
                  <a href="https://www.kunovillas.com/" id="www.kunovillas.com">
                    <img style="width:100%;" src="' . HTSERVER . site_url() . '/l-content/themes/custom/images/Kuno.jpeg" alt="">
                  </a>
              </td>
          </tr>
      </table>';
    }
}

function generate_qrcode( $bticket )
{
    require_once ADMIN_PATH . '/includes/php-qrcode/qrlib.php';

    if( !file_exists( PLUGINS_PATH . '/ticket/uploads/qrcode/' ) )
    {
        mkdir( PLUGINS_PATH . '/ticket/uploads/qrcode/' );
    }

    QRcode::png( $bticket,  PLUGINS_PATH . '/ticket/uploads/qrcode/' . $bticket . '.png', 'H', 4, 0 );

    return HTSERVER . site_url() . '/l-plugins/ticket/uploads/qrcode/' . $bticket . '.png';
}

function ticket_boarding_pass_report()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/booking/boarding-pass.html', 'boarding-pass-pdf' );

    add_block( 'pdf-loop', 'p-loop', 'boarding-pass-pdf' );
    add_block( 'pdf-block', 'p-block', 'boarding-pass-pdf' );

    $surl = site_url();
    $data = ticket_booking_all_data( $_GET['id']  );

    foreach( $data['detail'] as $dtype => $detail )
    {
        foreach( $detail as $did => $dt )
        {
            foreach( $dt['passenger'] as $type => $pass )
            {
                foreach( $pass as $d )
                {
                    if( $dt == end( $data['detail'] ) && $pass == end( $dt['passenger'] ) && $d == end( $pass ) )
                    {
                        $pagebreak = '';
                    }
                    else
                    {
                        $pagebreak = '<div class="page-break"></div>';
                    }

                    $trpname = get_trip_name( $dt );
                    $country = get_country( $d['lcountry_id'] );
                    $bddate  = date( 'd/m/Y', strtotime( $dt['bddate'] ) );
                    $source  = get_booking_source( $data['chid'], $data['agid'] );
                    $price   = number_format( $dt['price_per_' . $type], 0, ',', '.' );
                    $payment = ucwords( str_replace( '-', ' ', get_booking_payment_method( $data ) ) );
                    $route   = strlen( $trpname ) > 25 ? substr( $trpname, 0, 25 ) . '...' : $trpname;
                    $pname   = strlen( $d['bpname'] ) > 25 ? substr( $d['bpname'], 0, 25 ) . '...' : $d['bpname'];

                    add_variable( 'name', $pname );
                    add_variable( 'route', $route );
                    add_variable( 'date', $bddate );
                    add_variable( 'price', $price );
                    add_variable( 'route2', $trpname );
                    add_variable( 'payment', $payment );
                    add_variable( 'pagebreak', $pagebreak );
                    add_variable( 'source', $source['name'] );
                    add_variable( 'nationality', $country['lcountry_code'] );

                    add_variable( 'name2', $d['bpname'] );
                    add_variable( 'nationality2', $country['lcountry'] );
                    add_variable( 'time', date( 'H:i', strtotime( $dt['bddeparttime'] ) ) );
                    add_variable( 'transport', get_boarding_pass_transport_content( $dt ) );

                    parse_template( 'pdf-loop', 'p-loop', true );
                }
            }
        }
    }

    add_actions( 'is_use_ajax', true );
    add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
    add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $surl . '/l-plugins/ticket/js/accounting.min.js?v=' . TICKET_VERSION );

    add_variable( 'admurl', get_admin_url() );
    add_variable( 'section_title', 'Boarding Pas' );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

    add_variable( 'include_js', attemp_actions( 'include_js' ) );
    add_variable( 'include_css', attemp_actions( 'include_css' ) );
    add_variable( 'style', HTSERVER . site_url() .'/l-plugins/ticket/css/boarding-pass.css?v=' . TICKET_VERSION );

    parse_template( 'pdf-block', 'p-block' );

    return return_template( 'boarding-pass-pdf' );
}

function generate_boarding_pass_pdf( $data, $type = 'S' )
{
    $content = get_boarding_pass_pdf( $data );

    if( !empty( $content ) )
    {
        require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

        if( $type == 'I'  )
        {
            $mpdf = new mPDF( 'utf-8', [216, 93], 0, '', 13, 0, 0, 0 );

            $mpdf->SetTitle( 'Boarding Pass #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->SetJS( 'this.print();' );
            $mpdf->Output( 'boarding-pass-' . $data['bticket'] . '.pdf', $type );

            ticket_booking_update_print_boarding_status( $data['bid'], 1 );
        }
        elseif( $type == 'D'  )
        {
            $mpdf = new mPDF( 'utf-8', [216, 93], 0, '', 0, 0, 0, 0 );
            $mpdf->SetTitle( 'Boarding Pass #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->Output( 'boarding-pass-' . $data['bticket'] . '.pdf', $type );
        }
        else
        {
            $mpdf = new mPDF( 'utf-8', [216, 93], 0, '', 0, 0, 0, 0 );
            $mpdf->SetTitle( 'Boarding Pass #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );

            $content = $mpdf->Output( '', 'S' );

            return $content;
        }
    }
}

function get_boarding_pass_pdf( $data )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/booking/boarding-pass.html', 'boarding-pass-pdf' );

    add_block( 'pdf-loop', 'p-loop', 'boarding-pass-pdf' );
    add_block( 'pdf-block', 'p-block', 'boarding-pass-pdf' );
    
    foreach( $data['detail'] as $dtype => $detail )
    {
        foreach( $detail as $bdid => $dt )
        {
            foreach( $dt['passenger'] as $type => $pass )
            {
                foreach( $pass as $d )
                {
                    if ( $d['bpstatus'] == 'aa' )
                    {
                        if( $detail == end( $data['detail'] ) && $pass == end( $dt['passenger'] ) && $d == end( $pass ) )
                        {
                            $pagebreak = '';
                        }
                        else
                        {
                            $pagebreak = '<pagebreak></pagebreak>';
                        }

                        $trpname = get_trip_name( $dt );
                        $bddate  = date( 'd/m/Y', strtotime( $dt['bddate'] ) );
                        $country = get_country( $d['lcountry_id'] );
                        $source  = get_booking_source( $data['chid'], $data['agid'] );
                        $price   = number_format( $dt['price_per_' . $type], 0, ',', '.' );
                        $via_by  = get_via_by( $dt['rid'] , $dt['bdfrom_id'] , $dt['bdto_id']);
                        $payment = ucwords( str_replace( '-', ' ', get_booking_payment_method( $data ) ) );
                        $route   = strlen( $trpname ) > 25 ? mb_substr( $trpname, 0, 25, 'UTF-8' ) . '...' : $trpname;
                        $pname   = strlen( $d['bpname'] ) > 25 ? mb_substr( $d['bpname'], 0, 25, 'UTF-8' ) . '...' : $d['bpname'];

                        add_variable( 'name', $pname );
                        add_variable( 'route', $route );
                        add_variable( 'date', $bddate );
                        add_variable( 'price', $price );
                        add_variable( 'route2', $trpname );
                        add_variable( 'payment', $payment );
                        add_variable( 'pagebreak', $pagebreak );
                        add_variable( 'source', $source['name'] );
                        add_variable( 'nationality', $country['lcountry_code'] );
                        add_variable( 'via_by', $via_by );
                        add_variable( 'name2', $d['bpname'] );
                        add_variable( 'nationality2', $country['lcountry'] );
                        add_variable( 'time', date( 'H:i', strtotime( $dt['bddeparttime'] ) ) );
                        add_variable( 'transport', get_boarding_pass_transport_content( $dt ) );

                        parse_template( 'pdf-loop', 'p-loop', true );
                    }
                }
            }
        }
    }

    add_variable( 'style', HTSERVER . site_url() .'/l-plugins/ticket/css/boarding-pass.css?v=' . TICKET_VERSION );

    parse_template( 'pdf-block', 'p-block' );

    return return_template( 'boarding-pass-pdf' );
}

function get_trip_name( $trip )
{
    global $db;

    if( !empty( $trip ) )
    {
        $start = get_location( $trip['bdfrom_id'], 'lcname' );
        $stime = date( 'H:i' , strtotime( $trip['bddeparttime'] ) );
        $end   = get_location( $trip['bdto_id'], 'lcname' );
        $etime = date( 'H:i' , strtotime( $trip['bdarrivetime'] ) );

        return $start . ' (' . $stime . ') - ' . $end . ' (' . $etime . ')';
    }
}

function generate_reconfirmation_doc( $data )
{
    if( !empty( $data ) )
    {
        extract( $data['detail'] );

        require_once ADMIN_PATH . '/includes/phpoffice/vendor/autoload.php';

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor( ADMIN_PATH . '/includes/phpoffice/resources/reconfirmation_doc.docx' );

        $route     = array();
        $dep_trans = array();
        $ret_trans = array();
        $dep_pdata = get_pass_name_and_num_of_pass( $data['detail'] );

        if ( isset( $departure ) )
        {
            foreach ( $departure as $bdid => $dep_trip )
            {
                extract( $dep_trip );

                if( $bdpstatus == 'cn' )
                {
                    continue;
                }

                $dep_bddate       = date( 'd M Y', strtotime( $bddate ) );
                $dep_bddeparttime = date( 'h:i A', strtotime( $bddeparttime ) );
                $dep_bdarrivetime = date( 'h:i A', strtotime( $bdarrivetime ) );

                $route[] = 'Out :';
                $route[] = $dep_bddate . ', ' . ucwords( $bdfrom ) . ' - ' . ucwords( $bdto ) . get_via_by( $rid , $bdfrom_id , $bdto_id ) . ', ETD ' . $dep_bddeparttime . ' / ETA ' . $dep_bdarrivetime;

                if ( $bdtranstype == '1' )
                {
                    foreach( $transport as $ttype => $trans )
                    {
                        foreach( $trans as $t )
                        {
                            if( $t['bttrans_type'] != '2' )
                            {
                                if( empty( $t['taid'] ) )
                                {
                                    $hdetail = 'No Accommodation Booked';
                                }
                                else
                                {
                                    $tarea  = get_transport_area( $t['taid'] );
                                    $hname  = empty( $t['bthotelname'] ) ? '' : htmlentities( $t['bthotelname'] );
                                    $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                                    $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                    $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                    if( empty( $rpfrom ) && empty( $rpto ) )
                                    {
                                        $note = '';
                                    }
                                    else
                                    {
                                        if( $ttype == 'pickup' )
                                        {
                                            $note = '<w:br/>In between ' . $rpfrom . ' - ' . $rpto;
                                        }
                                        else
                                        {
                                            $note = '';
                                        }
                                    }

                                    if( empty( $tarea['taairport'] ) )
                                    {
                                        $ftime = '';
                                    }
                                    else
                                    {
                                        $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                        $ftime = sprintf( '<w:br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                    }

                                    if( empty( $t['hid'] ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . $note;
                                    }
                                    elseif( $t['hid'] == 1 )
                                    {
                                        if( empty( $hname ) && empty( $haddr ) )
                                        {
                                            $hdetail = $tarea['taname'] . $ftime . ', Hotel to be advised' . $note;
                                        }
                                        else
                                        {
                                            $hdetail = $tarea['taname'] . $ftime . ', ' . $hname . $haddr . $note;
                                        }
                                    }
                                    else
                                    {
                                        if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                        {
                                            $hdetail = $tarea['taname'] . $ftime . $note;
                                        }
                                        else
                                        {
                                            $hdetail = $tarea['taname'] . $ftime . ', ' . $hname . $haddr . $note;
                                        }
                                    }
                                }

                                if( $t['bttrans_type'] == '0' )
                                {
                                    $dep_trans[] = ucfirst( $ttype ) . ', Shared Transport<w:br/>' . $hdetail;
                                }
                                else
                                {
                                    $dep_trans[] = ucfirst( $ttype ) . ', Privated Transport<w:br/>' . $hdetail;
                                }
                            }
                            else
                            {
                                $drv_detail  = get_driver_detail( $transport[ $ttype ][ 0 ][ 'btid' ] );
                                $dep_trans[] = ucfirst( $ttype ) . ', Own Transport / ' . $drv_detail['btdrivername'] . ' / ' . $drv_detail['btdriverphone'];
                            }
                        }
                    }
                }
                else
                {
                    $drv_detail  = get_driver_detail( $transport[ $ttype ][ 0 ][ 'btid' ] );
                    $dep_trans[] = 'Own Transport / ' . $drv_detail['btdrivername'] . ' / ' . $drv_detail['btdriverphone'];
                }
            }
        }

        if ( isset( $return ) )
        {
            foreach ( $return as $bdid => $ret_trip )
            {
                extract( $ret_trip );

                if( $bdpstatus == 'cn' )
                {
                    continue;
                }

                $ret_bddate       = date( 'd M Y', strtotime( $bddate ) );
                $ret_bddeparttime = date( 'h:i A', strtotime( $bddeparttime ) );
                $ret_bdarrivetime = date( 'h:i A', strtotime( $bdarrivetime ) );

                $route[] = 'Return :';
                $route[] = $ret_bddate . ', ' . ucwords( $bdfrom ) . ' - ' . ucwords( $bdto ) . get_via_by( $rid , $bdfrom_id , $bdto_id ) . ', ETD ' . $ret_bddeparttime . ' / ETA ' . $ret_bdarrivetime;

                if ( $bdtranstype == '1' )
                {
                    foreach( $transport as $ttype => $trans )
                    {
                        foreach( $trans as $t )
                        {
                            if( $t['bttrans_type'] != '2' )
                            {
                                if( empty( $t['taid'] ) )
                                {
                                    $hdetail = 'No Accommodation Booked';
                                }
                                else
                                {
                                    $tarea  = get_transport_area( $t['taid'] );
                                    $hname  = empty( $t['bthotelname'] ) ? '' : htmlentities( $t['bthotelname'] );
                                    $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                                    $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                    $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                    if( empty( $rpfrom ) && empty( $rpto ) )
                                    {
                                        $note = '';
                                    }
                                    else
                                    {
                                        if( $ttype == 'pickup' )
                                        {
                                            $note = '<w:br/>In between ' . $rpfrom . ' - ' . $rpto;
                                        }
                                        else
                                        {
                                            $note = '';
                                        }
                                    }

                                    if( empty( $tarea['taairport'] ) )
                                    {
                                        $ftime = '';
                                    }
                                    else
                                    {
                                        $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                        $ftime = sprintf( '<w:br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                    }

                                    if( empty( $t['hid'] ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . $note;
                                    }
                                    elseif( $t['hid'] == 1 )
                                    {
                                        if( empty( $hname ) && empty( $haddr ) )
                                        {
                                            $hdetail = $tarea['taname'] . $ftime . ', Hotel to be advised' . $note;
                                        }
                                        else
                                        {
                                            $hdetail = $tarea['taname'] . $ftime . ', ' . $hname . $haddr . $note;
                                        }
                                    }
                                    else
                                    {
                                        if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                        {
                                            $hdetail = $tarea['taname'] . $ftime . $note;
                                        }
                                        else
                                        {
                                            $hdetail = $tarea['taname'] . $ftime . ', ' . $hname . $haddr . $note;
                                        }
                                    }
                                }

                                if( $t['bttrans_type'] == '0' )
                                {
                                    $ret_trans[] = ucfirst( $ttype ) . ', Shared Transport<w:br/>' . $hdetail;
                                }
                                else
                                {
                                    $ret_trans[] = ucfirst( $ttype ) . ', Privated Transport<w:br/>' . $hdetail;
                                }
                            }
                            else
                            {
                                $drv_detail  = get_driver_detail( $transport[ $ttype ][0]['btid'] );
                                $ret_trans[] = ucfirst( $ttype ) . ', Own Transport / ' . $drv_detail['btdrivername'] . ' / ' . $drv_detail['btdriverphone'];
                            }
                        }
                    }
                }
                else
                {
                    $drv_detail  = get_driver_detail( $transport[ $ttype ][0]['btid'] );
                    $ret_trans[] = 'Own Transport / ' . $driver_detail['btdrivername'] . ' / ' . $driver_detail['btdriverphone'];
                }
            }
        }

        $templateProcessor->setValue( 'bbname', $dep_pdata['pass_name'][0] );
        $templateProcessor->setValue( 'passanger', implode( ', ', $dep_pdata['pass_name'] ) );
        $templateProcessor->setValue( 'num_of_persons', implode( ', ', $dep_pdata['num_of_person'] ) );
        $templateProcessor->setValue( 'routetime', implode( '<w:br/>', $route ) . '<w:br/>' );
        $templateProcessor->setValue( 'out_transport', 'Out Transport' );
        $templateProcessor->setValue( 'data_out_transport', implode( '<w:br/><w:br/>', $dep_trans ) . '<w:br/>' );
        $templateProcessor->setValue( 'return_transport', !empty( $ret_trans ) ? 'Return Transport' : '' );
        $templateProcessor->setValue( 'd', !empty( $ret_trans ) ? ':' : '' );
        $templateProcessor->setValue( 'data_ret_transport', ( !empty( $ret_trans ) ? implode( '<w:br/><w:br/>', $ret_trans ) : '' ) . '<w:br/>' );
        $templateProcessor->setValue( 'users', get_display_name( $_COOKIE['user_id'] ) );

        header( 'Content-Disposition: attachment; filename="reconfirmation_' . $data['bticket'] . '.docx"' );

        $templateProcessor->saveAs( 'php://output' );
    }
}

function generate_reconfirmation_pdf( $data, $type = 'S' )
{
    $content = get_reconfirmation_pdf( $data );

    if( !empty( $content ) )
    {
        require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

        if( $type == 'I'  )
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 0, 0 );
            $mpdf->SetTitle( 'Reconfirmation #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->SetJS( 'this.print();' );
            $mpdf->Output( 'reconfirmation-' . $data['bticket'] . '.pdf', $type );
        }
        elseif( $type == 'D'  )
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 0, 0 );
            $mpdf->SetTitle( 'Reconfirmation #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->Output( 'reconfirmation-' . $data['bticket'] . '.pdf', $type );
        }
        else
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 0, 0 );
            $mpdf->SetTitle( 'Reconfirmation #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );

            $content = $mpdf->Output( '', 'S' );

            return $content;
        }
    }
}

function get_pass_name_and_num_of_pass( $detail )
{
    $adult  = array();
    $child  = array();
    $infant = array();

    $num_of_person = array();
    $pass_name     = array();

    foreach( $detail as $bdtype => $obj )
    {
        foreach( $obj as $bdid => $d )
        {
            foreach( $d['passenger'] as $bptype => $passenger )
            {
                foreach( $passenger as $p )
                {
                    if( $bptype == 'adult' && !in_array( $p['bpname'], $adult ) )
                    {
                        $adult[] = $p['bpname'];
                    }
                    else if( $bptype == 'child' && !in_array( $p['bpname'], $child ) )
                    {
                        $child[] = $p['bpname'];
                    }
                    else if( $bptype == 'infant' && !in_array( $p['bpname'], $infant ) )
                    {
                        $infant[] = $p['bpname'];
                    }

                    if( !in_array( $p['bpname'], $pass_name ) )
                    {
                        $pass_name[] = $p['bpname'];
                    }
                }
            }
        }
    }

    $adults  = count( $adult );
    $childs  = count( $child );
    $infants = count( $infant );

    $num_of_person[] = $adults  . ( $adults > 1  ? ' adults'  : ' adult' );
    $num_of_person[] = $childs  . ( $childs > 1  ? ' childs'  : ' child' );
    $num_of_person[] = $infants . ( $infants > 1 ? ' infants' : ' infant' );

    return array( 'pass_name' => $pass_name, 'num_of_person' => $num_of_person );
}

function get_reconfirmation_pdf( $data )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/booking/reconfirmation.html', 'reconfirmation-pdf' );

    add_block( 'pdf-block', 'p-block', 'reconfirmation-pdf' );

    extract( $data['detail'] );

    //-- Go Trip
    if( isset( $departure ) )
    {
        $dep_route = array();
        $dep_trans = array();

        foreach( $departure as $key => $dp_trip )
        {
            extract( $dp_trip );

            if( $bdpstatus == 'cn' )
            {
                continue;
            }

            $dep_bdto         = $bdto;
            $dep_bdfrom       = $bdfrom;
            $dep_bddate       = date( 'd M Y', strtotime( $bddate ) );
            $dep_bddeparttime = date( 'h:i A', strtotime( $bddeparttime ) );
            $dep_bdarrivetime = date( 'h:i A', strtotime( $bdarrivetime ) );

            if( $bdtranstype == '1' )
            {
                $dep_trans_detail = array();

                foreach( $transport as $ttype => $trans )
                {
                    foreach( $trans as $t )
                    {
                        if( $t['bttrans_type'] != '2' )
                        {
                            if( empty( $t['taid'] ) )
                            {
                                $hdetail = 'No Accommodation Booked';
                            }
                            else
                            {
                                $tarea  = get_transport_area( $t['taid'] );
                                $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                                $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                                $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                if( empty( $rpfrom ) && empty( $rpto ) )
                                {
                                    $note = '';
                                }
                                else
                                {
                                    if( $ttype == 'pickup' )
                                    {
                                        $note = '<br/>In between ' . $rpfrom . ' - ' . $rpto;
                                    }
                                    else
                                    {
                                        $note = '';
                                    }
                                }

                                if( empty( $tarea['taairport'] ) )
                                {
                                    $ftime = '';
                                }
                                else
                                {
                                    $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                    $ftime = sprintf( '<br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                }

                                if( empty( $t['hid'] ) )
                                {
                                    $hdetail = $tarea['taname'] . $ftime . $note;
                                }
                                elseif( $t['hid'] == 1 )
                                {
                                    if( empty( $hname ) && empty( $haddr ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . ', Hotel to be advised' . $note;
                                    }
                                    else
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . ', ' . $hname . $haddr . $note;
                                    }
                                }
                                else
                                {
                                    if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . $note;
                                    }
                                    else
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . ', ' . $hname . $haddr . $note;
                                    }
                                }
                            }

                            if( $t['bttrans_type'] == '0' )
                            {
                                $dep_trans_detail[] = ucfirst( $ttype ) . ', Shared Transport<br/>' . $hdetail;
                            }
                            else
                            {
                                $dep_trans_detail[] = ucfirst( $ttype ) . ', Privated Transport<br/>' . $hdetail;
                            }
                        }
                        else
                        {
                            $driver_detail      = get_driver_detail( $transport[ $ttype ][0]['btid'] );
                            $dep_trans_detail[] = ucfirst( $ttype ) . ', Own Transport / ' . $driver_detail['btdrivername'] . ' / ' . $driver_detail['btdriverphone'];
                        }
                    }
                }

                $dep_trans[] = empty( $dep_trans_detail ) ? '' : implode( '<br/><br/>' , $dep_trans_detail );
            }
            else
            {
                $driver_detail = get_driver_detail( $transport[ $ttype ][0]['btid'] );
                $dep_trans[]   = 'Own Transport / ' . $driver_detail['btdrivername'] . ' / ' . $driver_detail['btdriverphone'];
            }

            $dep_route[] = $dep_bddate . ', ' . $dep_bdfrom . ' - ' . $dep_bdto . ', ' . ' ETD ' . $dep_bddeparttime . ' / ETA ' . $dep_bdarrivetime;
        }

        add_variable( 'dep_trans_detail', empty( $dep_trans ) ? '' : implode( '<br/>', $dep_trans ) );
        add_variable( 'dep_route', empty( $dep_route ) ? '' : '<b>Out :</b><br/>' . implode( '<br/>', $dep_route ) . get_via_by( $dp_trip['rid'] , $dp_trip['bdfrom_id'] , $dp_trip['bdto_id'] ) );
    }

    //-- Back Trip
    if( isset( $return ) && $data['btype'] == '1' )
    {
        $rtn_route = array();
        $rtn_trans = array();

        foreach( $return as $key => $rt_trip )
        {
            extract( $rt_trip );

            if( $bdpstatus == 'cn' )
            {
                continue;
            }

            $rtn_bdto         = $bdto;
            $rtn_bdfrom       = $bdfrom;
            $rtn_bddate       = date( 'd M Y', strtotime( $bddate ) );
            $rtn_bddeparttime = date( 'h:i A', strtotime( $bddeparttime ) );
            $rtn_bdarrivetime = date( 'h:i A', strtotime( $bdarrivetime ) );

            if( $bdtranstype == '1' )
            {
                $rtn_trans_detail = array();

                foreach( $transport as $ttype => $trans )
                {
                    foreach( $trans as $t )
                    {
                        if( $t['bttrans_type'] != '2' )
                        {
                            if( empty( $t['taid'] ) )
                            {
                                $hdetail = 'No Accommodation Booked';
                            }
                            else
                            {
                                $tarea  = get_transport_area( $t['taid'] );
                                $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                                $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                                $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                if( empty( $rpfrom ) && empty( $rpto ) )
                                {
                                    $note = '';
                                }
                                else
                                {
                                    if( $ttype == 'pickup' )
                                    {
                                        $note = '<br/>In between ' . $rpfrom . ' - ' . $rpto;
                                    }
                                    else
                                    {
                                        $note = '';
                                    }
                                }

                                if( empty( $tarea['taairport'] ) )
                                {
                                    $ftime = '';
                                }
                                else
                                {
                                    $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                    $ftime = sprintf( '<br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                }

                                if( empty( $t['hid'] ) )
                                {
                                    $hdetail = $tarea['taname'] . $ftime . $note;
                                }
                                elseif( $t['hid'] == 1 )
                                {
                                    if( empty( $hname ) && empty( $haddr ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . ', Hotel to be advised' . $note;
                                    }
                                    else
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . ', ' . $hname . $haddr . $note;
                                    }
                                }
                                else
                                {
                                    if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . $note;
                                    }
                                    else
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . ', ' . $hname . $haddr . $note;
                                    }
                                }
                            }

                            if( $t['bttrans_type'] == '0' )
                            {
                                $rtn_trans_detail[] = ucfirst( $ttype ) . ', Shared Transport<br/>' . $hdetail;
                            }
                            else
                            {
                                $rtn_trans_detail[] = ucfirst( $ttype ) . ', Privated Transport<br/>' . $hdetail;
                            }
                        }
                        else
                        {
                            $driver_detail      = get_driver_detail( $transport[ $ttype ][0]['btid'] );
                            $rtn_trans_detail[] = ucfirst( $ttype ) . ', Own Transport / ' . $driver_detail['btdrivername'] . ' / ' . $driver_detail['btdriverphone'];
                        }
                    }
                }

                $rtn_trans[] = empty( $rtn_trans_detail ) ? '' : implode( '<br/><br/>' , $rtn_trans_detail );
            }
            else
            {
                $driver_detail = get_driver_detail( $transport[ $ttype ][0]['btid'] );
                $rtn_trans[]   = 'Own Transport / ' . $driver_detail['btdrivername'] . ' / ' . $driver_detail['btdriverphone'];
            }

            $rtn_route[] = $rtn_bddate . ', ' . $rtn_bdfrom . ' - ' . $rtn_bdto . ', ' . ' ETD ' . $rtn_bddeparttime . ' / ETA ' . $rtn_bdarrivetime;
        }

        add_variable( 'rtn_css', empty( $rtn_route ) ? 'display:none; padding:0;' : '' );
        add_variable( 'rtn_trans_detail', empty( $rtn_trans ) ? '' : implode( '<br/>', $rtn_trans ) );
        add_variable( 'rtn_route', empty( $rtn_route ) ? '' : '<b>Return : </b><br/>' . implode( '<br/>', $rtn_route ) . get_via_by( $rt_trip['rid'] , $rt_trip['bdfrom_id'] , $rt_trip['bdto_id'] ) );
    }
    else
    {
        add_variable( 'rtn_css', 'display:none; padding:0;' );
        add_variable( 'rtn_trans_txt', '' );
        add_variable( 'rtn_route', '' );
    }

    $dep_pdata = get_pass_name_and_num_of_pass( $data['detail'] );

    add_variable( 'bbname', $dep_pdata['pass_name'][0] );
    add_variable( 'pass_name', implode( ', ', $dep_pdata['pass_name'] ) );
    add_variable( 'num_of_person', implode( ', ', $dep_pdata['num_of_person'] ) );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'users', get_display_name( $_COOKIE['user_id'] ) );
    add_variable( 'style', HTSERVER . site_url() .'/l-plugins/ticket/css/reconfirmation.css' );

    parse_template( 'pdf-block', 'p-block' );

    return return_template( 'reconfirmation-pdf' );
}

function generate_booking_pdf_ticket( $data, $type = 'S' )
{
    $content = get_booking_pdf_ticket( $data );

    if( !empty( $content ) )
    {
        require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

        if( $type == 'I'  )
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 7, 7, 7, 7 );
            $mpdf->SetTitle( 'Tiket #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->SetJS( 'this.print();' );
            $content = $mpdf->Output( 'ticket-' . $data['bticket'] . '.pdf', $type );

            if( file_exists( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' ) )
            {
                unlink( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' );
            }

            return $content;
        }
        elseif( $type == 'D'  )
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 7, 7, 7, 7 );
            $mpdf->SetTitle( 'Tiket #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->Output( 'ticket-' . $data['bticket'] . '.pdf', $type );

            if( file_exists( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' ) )
            {
                unlink( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' );
            }
        }
        else
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 7, 7, 7, 7 );
            $mpdf->SetTitle( 'Tiket #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );

            $content = $mpdf->Output( '', 'S' );

            return $content;
        }
    }
}

function get_booking_pdf_ticket( $data )
{
    $macrotime = time();

    set_template( PLUGINS_PATH . '/ticket/tpl/booking/ticket.html', 'ticket-' . $macrotime . '-pdf' );

    add_block( 'pdf-departure-loop-block', 'pdl-' . $macrotime . '-block', 'ticket-' . $macrotime . '-pdf' );
    add_block( 'pdf-departure-block', 'pd-' . $macrotime . '-block', 'ticket-' . $macrotime . '-pdf' );
    add_block( 'pdf-return-loop-block', 'prl-' . $macrotime . '-block', 'ticket-' . $macrotime . '-pdf' );
    add_block( 'pdf-return-block', 'pr-' . $macrotime . '-block', 'ticket-' . $macrotime . '-pdf' );
    add_block( 'pdf-block', 'p-' . $macrotime . '-block', 'ticket-' . $macrotime . '-pdf' );

    add_variable( 'bbname', $data['bbname'] );
    add_variable( 'bbemail', $data['bbemail'] );
    add_variable( 'bbphone', $data['bbphone'] );

    add_variable( 'web_title',  web_title() );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'booking_id', $data['bcode'] );
    add_variable( 'booking_ticket',  $data['bticket'] );
    add_variable( 'participans', ticket_detail_participans( $data['detail'] ) );
    add_variable( 'booking_ticket_qrcode',  generate_qrcode( $data['bticket'] ) );

    extract( $data['detail'] );

    $dep_passenger = array();
    $rtn_passenger = array();

    if( isset( $departure ) )
    {
        $num = 0;

        foreach( $departure as $key => $dp_trip )
        {
            extract( $dp_trip );

            if( $bdstatus != 'cn' )
            {
                $pass = ticket_passenger_list_for_pdf_content( $passenger );

                if( !empty( $pass ) )
                {
                    $dep_passenger[] = $pass;
                }

                add_variable( 'dep_bdto', $bdto );
                add_variable( 'dep_bdfrom', $bdfrom );
                add_variable( 'dep_boname', get_boat( $boid, 'boname' ) );
                add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'dep_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                add_variable( 'dep_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );
                add_variable( 'dep_route', get_route_detail_list_content( $rid, $bdfrom_id, $bdto_id ) );
                add_variable( 'dep_trans_detail', ticket_detail_transport_pdf_content( $transport, $bdfrom, $bdto ) );

                parse_template( 'pdf-departure-loop-block', 'pdl-' . $macrotime . '-block', true );

                $num++;
            }
        }

        if( $num > 0 )
        {
            parse_template( 'pdf-departure-block', 'pd-' . $macrotime . '-block' );
        }
    }

    if( isset( $return ) && $data['btype'] == '1' )
    {
        $num = 0;

        foreach( $return as $key => $rt_trip )
        {
            extract( $rt_trip );

            if ( $bdstatus != 'cn' )
            {
                $pass = ticket_passenger_list_for_pdf_content( $passenger );

                if( !empty( $pass ) )
                {
                    $rtn_passenger[] = $pass;
                }

                add_variable( 'rtn_bdto', $bdto );
                add_variable( 'rtn_bdfrom', $bdfrom );
                add_variable( 'rtn_boname', get_boat( $boid, 'boname' ) );
                add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'rtn_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                add_variable( 'rtn_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );
                add_variable( 'rtn_route', get_route_detail_list_content( $rid, $bdfrom_id, $bdto_id ) );
                add_variable( 'rtn_trans_detail', ticket_detail_transport_pdf_content( $transport, $bdfrom, $bdto ) );

                parse_template( 'pdf-return-loop-block', 'prl-' . $macrotime . '-block', true );

                $num++;
            }
        }

        if( $num > 0 )
        {
            parse_template( 'pdf-return-block', 'pr-' . $macrotime . '-block' );
        }

    }

    add_variable( 'dep_passenger', implode( '', $dep_passenger ) );
    add_variable( 'dprt_css', empty( $dep_passenger ) ? 'display:none;' : '' );

    add_variable( 'rtn_passenger', implode( '', $rtn_passenger ) );
    add_variable( 'rtn_css', empty( $rtn_passenger ) ? 'display:none;' : '' );

    add_variable( 'footer_images', set_footer_images() );
    add_variable( 'email', get_meta_data( 'email_reservation' ) );
    add_variable( 'term_and_condition', get_meta_data( 'term_and_condition_content', 'ticket_setting' ) );
    add_variable( 'style', HTSERVER . site_url() . '/l-plugins/ticket/css/ticket.css' );

    parse_template( 'pdf-block', 'p-' . $macrotime . '-block' );

    return return_template( 'ticket-' . $macrotime . '-pdf' );
}

function ticket_detail_participans( $detail )
{
    $dep_participants = array();
    $rtn_participants = array();

    foreach( $detail as $bdtype => $obj )
    {
        $adult  = array();
        $child  = array();
        $infant = array();

        foreach( $obj as $bdid => $d )
        {
          	if( $d['bdstatus'] != 'cn' )
			{
	            foreach( $d['passenger'] as $bptype => $passenger )
	            {
	                foreach( $passenger as $p )
	                {
	                    if( $bdtype == 'departure' )
	                    {
	                    	$dep_participants[ $bptype ][] = $p['bpname'];
	                    }
	                    else
	                    {
	                        $rtn_participants[ $bptype ][] = $p['bpname'];
	                    }
	                }
	            }
          	}
        }
    }

    $content = array();

    if( !empty( $dep_participants ) )
    {
        $adults  = isset( $dep_participants['adult'] ) ? count( $dep_participants['adult'] ) : 0;
        $childs  = isset( $dep_participants['child'] ) ? count( $dep_participants['child'] ) : 0;
        $infants = isset( $dep_participants['infant'] ) ? count( $dep_participants['infant'] ) : 0;

        $dpass[] = $adults  . ( $adults > 1  ? ' adults'  : ' adult' );
        $dpass[] = $childs  . ( $childs > 1  ? ' childs'  : ' child' );
        $dpass[] = $infants . ( $infants > 1 ? ' infants' : ' infant' );

        $content[] = 'Departure Trip : ' . implode( ', ', $dpass );
    }

    if( !empty( $rtn_participants ) )
    {
        $adults  = isset( $rtn_participants['adult'] ) ? count( $rtn_participants['adult'] ) : 0;
        $childs  = isset( $rtn_participants['child'] ) ? count( $rtn_participants['child'] ) : 0;
        $infants = isset( $rtn_participants['infant'] ) ? count( $rtn_participants['infant'] ) : 0;

        $rpass[] = $adults  . ( $adults > 1  ? ' adults'  : ' adult' );
        $rpass[] = $childs  . ( $childs > 1  ? ' childs'  : ' child' );
        $rpass[] = $infants . ( $infants > 1 ? ' infants' : ' infant' );

        $content[] = 'Return Trip : ' . implode( ', ', $rpass );
    }

    return implode( '<br/>', $content );
}

function set_footer_images()
{
    $meta_value = get_meta_data( 'fimage', 'ticket_setting' );

    if( !empty( $meta_value ) )
    {
        $arr = json_decode( $meta_value, true );
        $url = site_url();

        if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
        {
            $content = '
            <div class="footer-img">';
                foreach( $arr as $d )
                {
                    $content .= '<img src="' . HTSERVER . $url . '/l-plugins/ticket/uploads/footer/' . $d . '" />&nbsp;&nbsp;';
                }

                $content .= '
            </div>';

            return $content;
        }
    }
}

function generate_booking_pdf_receipt( $data, $type = 'S' )
{
    $content = get_booking_pdf_receipt( $data );

    if( !empty( $content ) )
    {
        require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

        if( $type == 'I'  )
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 0, 0 );

            $mpdf->setAutoTopMargin    = true;
            $mpdf->setAutoBottomMargin = true;

            $mpdf->SetTitle( 'Tiket #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->SetJS( 'this.print();' );
            $mpdf->Output( 'receipt-' . $data['bticket'] . '.pdf', $type );
        }
        elseif( $type == 'D'  )
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 0, 0 );

            $mpdf->setAutoTopMargin    = true;
            $mpdf->setAutoBottomMargin = true;

            $mpdf->SetTitle( 'Tiket #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->Output( 'receipt-' . $data['bticket'] . '.pdf', $type );
        }
        else
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 0, 0 );

            $mpdf->setAutoTopMargin    = true;
            $mpdf->setAutoBottomMargin = true;

            $mpdf->SetTitle( 'Tiket #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );

            $content = $mpdf->Output( '', 'S' );

            return $content;
        }
    }
}

function get_booking_pdf_receipt( $data )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/booking/receipt.html', 'receipt-pdf' );

    add_block( 'pdf-departure-loop-block', 'pdl-block', 'receipt-pdf' );
    add_block( 'pdf-departure-block', 'pd-block', 'receipt-pdf' );
    add_block( 'pdf-return-loop-block', 'prl-block', 'receipt-pdf' );
    add_block( 'pdf-return-block', 'pr-block', 'receipt-pdf' );

    add_block( 'pdf-block', 'p-block', 'receipt-pdf' );

    extract( $data['detail'] );

    add_variable( 'bbname', $data['bbname'] );
    add_variable( 'bbemail', $data['bbemail'] );
    add_variable( 'bbphone', $data['bbphone'] );

    if( empty( $data['agid'] ) )
    {
        add_variable( 'bchannel', $data['chname'] );
    }
    else
    {
        $agid = empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'];

        add_variable( 'bchannel', get_agent( $agid, 'agname' ) );
    }

    add_variable( 'web_title',  web_title() );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'booking_id',  '' );
    add_variable( 'booking_ticket',  $data['bticket'] );
    add_variable( 'btype', ticket_booking_type( $data['btype'] ) );
    add_variable( 'bdate', date( 'd M Y', strtotime( $data['bdate'] ) ) );

    add_variable( 'bbrevtype', empty( $data['bbrevtype'] ) ? '-' : $data['bbrevtype'] );
    add_variable( 'bbrevagent', empty( $data['bbrevagent'] ) ? '-' : $data['bbrevagent'] );
    add_variable( 'bbrevstatus', empty( $data['bbrevstatus'] ) ? '-' : $data['bbrevstatus'] );
    add_variable( 'booking_staf', empty( $data['bbooking_staf'] ) ? '-' : $data['bbooking_staf'] );

    add_variable( 'bhotelname', $data['bhotelname'] );
    add_variable( 'bhoteladdress', empty( $data['bhoteladdress'] ) ? '' : $data['bhoteladdress'] . '<br />' );
    add_variable( 'bhotelphone', empty( $data['bhotelphone'] ) ? '' : 'P. <a href="tel:' . $data['bhotelphone'] . '" style="color:rgb(16, 163, 193);">' . $data['bhotelphone'] . '</a><br />' );
    add_variable( 'bhotelemail', empty( $data['bhotelemail'] ) ? '' : 'E. <a href="mailto:' . $data['bhotelemail'] . '" style="color:rgb(16, 163, 193);">' . $data['bhotelemail'] . '</a><br />' );
    add_variable( 'baccommodationcss', empty( $data['bhotelname'] ) ? 'display:none;' : '' );

    //-- Go Trip
    if( isset( $departure ) )
    {
        $last = end( $departure );

        foreach( $departure as $key => $dp_trip )
        {
            extract( $dp_trip );

            $var = get_variable_by_port_type( $bdfrom_id, $bdto_id );

            $dep_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
            $dep_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
            $dep_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
            $dep_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';

            add_variable( 'dep_bdto', $bdto );
            add_variable( 'dep_bdfrom', $bdfrom );
            add_variable( 'dep_num_adult', $num_adult );
            add_variable( 'dep_num_child', $num_child );
            add_variable( 'dep_num_infant', $num_infant );
            add_variable( 'dep_item_class', $last['bdid'] == $bdid ? 'last' : '' );
            add_variable( 'dep_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
            add_variable( 'dep_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );

            add_variable( 'dep_discount', $discount );
            add_variable( 'dep_price_per_adult', $price_per_adult );
            add_variable( 'dep_price_per_child', $price_per_child );
            add_variable( 'dep_price_per_infant', $price_per_infant );

            add_variable( 'dep_pa_display', $dep_pa_display );
            add_variable( 'dep_pc_display', $dep_pc_display );
            add_variable( 'dep_pi_display', $dep_pi_display );
            add_variable( 'dep_pt_display', $dep_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );
            add_variable( 'dep_passenger', ticket_passenger_list_for_pdf_content( $passenger ) );

            add_variable( 'dep_bdtype', ucfirst( $bdtype ) );
            add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );

            $dep_trans_detail = '';

            if( !empty( $transport ) )
            {
                foreach( $transport as $ttype => $trans )
                {
                    foreach( $trans as $t )
                    {
                        if( $t['bttrans_type'] != '2' )
                        {
                            if( empty( $t['taid'] ) )
                            {
                                $hdetail = '<p><b>No Accommodation Booked</b></p>';
                            }
                            else
                            {
                                $tarea  = get_transport_area( $t['taid'] );
                                $hname  = empty( $t['bthotelname'] ) ? '' : sprintf( '<p>%s</p>', $t['bthotelname'] );
                                $haddr  = empty( $t['bthoteladdress'] ) ? '' : sprintf( '<p>%s</p>', $t['bthoteladdress'] );
                                $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                if( empty( $rpfrom ) && empty( $rpto ) )
                                {
                                    $note = '';
                                }
                                else
                                {
                                    if( $ttype == 'pickup' )
                                    {
                                        $note = '<p>In between ' . $rpfrom . ' - ' . $rpto . '</p>';
                                    }
                                    else
                                    {
                                        $note = '';
                                    }
                                }

                                if( empty( $tarea['taairport'] ) )
                                {
                                    $ftime = '';
                                }
                                else
                                {
                                    $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                    $ftime = sprintf( '<p>' . $flbl . ' %s</p>', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                }

                                if( empty( $t['hid'] ) )
                                {
                                    $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $note;
                                }
                                elseif( $t['hid'] == 1 )
                                {
                                    if( empty( $hname ) && empty( $haddr ) )
                                    {
                                        $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . 'Hotel to be advised' . $note;
                                    }
                                    else
                                    {
                                        $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $hname . $haddr . $note;
                                    }
                                }
                                else
                                {
                                    if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                    {
                                        $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $note;
                                    }
                                    else
                                    {
                                        $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $hname . $haddr . $note;
                                    }
                                }
                            }

                            if( $t['bttrans_type'] == '0' )
                            {
                                $trans_type = 'Shared Transport';
                            }
                            else
                            {
                                $trans_type = 'Privated Transport';
                            }

                            $trans_fee = $t['bttrans_fee'] == 0 ? 'Free' : number_format( $t['bttrans_fee'], 0, ',', '.' );

                            $dep_trans_detail .= '
                            <div class="clearfix">
                                <div class="item trip">
                                    <strong>' . ucfirst( $ttype ) . '</strong>
                                    <p><b>' . $trans_type . '</b></p>
                                </div>
                                <div class="item transport">
                                    <strong>Transport Detail</strong>
                                    ' . $hdetail . '
                                </div>
                                <div class="item price">
                                    <strong>Transport Fee</strong>
                                    <p><b>'. $trans_fee .'</b></p>
                                </div>
                            </div>';
                        }
                        else
                        {
                            $dep_trans_detail .= '
                            <div class="clearfix">
                                <div class="item trip">
                                    <strong>' . ucfirst( $ttype ) . '</strong>
                                    <p><b>Own Transport</b></p>
                                </div>
                                <div class="item transport"></div>
                                <div class="item price">
                                    <strong>Transport Fee</strong>
                                    <p><b>N/A</b></p>
                                </div>
                            </div>';
                        }
                    }
                }
            }

            if( $bdpstatus == 'cn' )
            {
                add_variable( 'dep_cancel_mention_css', 'style="color:red;"' );
            }
            else
            {
                add_variable( 'dep_cancel_mention_css', 'style="display:none; color:red;"' );
            }

            add_variable( 'dep_trans_detail', $dep_trans_detail );

            parse_template( 'pdf-departure-loop-block', 'pdl-block', true );
        }

        parse_template( 'pdf-departure-block', 'pd-block' );
    }

    //-- Back Trip
    if( isset( $return ) && $data['btype'] == '1' )
    {
        $last = end( $return );

        foreach( $return as $key => $rt_trip )
        {
            extract( $rt_trip );

            $rtn_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
            $rtn_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
            $rtn_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
            $rtn_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small><br/>';

            add_variable( 'rtn_bdto', $bdto );
            add_variable( 'rtn_bdfrom', $bdfrom );
            add_variable( 'rtn_item_class', $last['bdid'] == $bdid ? 'last' : '' );
            add_variable( 'rtn_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
            add_variable( 'rtn_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );

            add_variable( 'rtn_discount', $discount );
            add_variable( 'rtn_price_per_adult', $price_per_adult );
            add_variable( 'rtn_price_per_child', $price_per_child );
            add_variable( 'rtn_price_per_infant', $price_per_infant );

            add_variable( 'rtn_pa_display', $rtn_pa_display );
            add_variable( 'rtn_pc_display', $rtn_pc_display );
            add_variable( 'rtn_pi_display', $rtn_pi_display );
            add_variable( 'rtn_pt_display', $rtn_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );
            add_variable( 'rtn_passenger', ticket_passenger_list_for_pdf_content( $passenger ) );

            add_variable( 'rtn_bdtype', ucfirst( $bdtype ) );
            add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );

            $rtn_trans_detail = '';

            if( !empty( $transport ) )
            {
                foreach( $transport as $ttype => $trans )
                {
                    foreach( $trans as $t )
                    {
                        if( $t['bttrans_type'] != '2' )
                        {
                            if( empty( $t['taid'] ) )
                            {
                                $hdetail = '<b>No Accommodation Booked</b>';
                            }
                            else
                            {
                                $tarea  = get_transport_area( $t['taid'] );
                                $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                                $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                                $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                if( empty( $rpfrom ) && empty( $rpto ) )
                                {
                                    $note = '';
                                }
                                else
                                {
                                    if( $ttype == 'pickup' )
                                    {
                                        $note = '<br/>In between ' . $rpfrom . ' - ' . $rpto;
                                    }
                                    else
                                    {
                                        $note = '';
                                    }
                                }

                                if( empty( $tarea['taairport'] ) )
                                {
                                    $ftime = '';
                                }
                                else
                                {
                                    $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                    $ftime = sprintf( '<br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                }

                                if( empty( $t['hid'] ) )
                                {
                                    $hdetail = '<b>' . $tarea['taname'] . '</b>' . $ftime . $note;
                                }
                                elseif( $t['hid'] == 1 )
                                {
                                    if( empty( $hname ) && empty( $haddr ) )
                                    {
                                        $hdetail = '<b>' . $tarea['taname'] . '</b>' . $ftime . 'Hotel to be advised' . $note;
                                    }
                                    else
                                    {
                                        $hdetail = '<b>' . $tarea['taname'] . '</b>' . $ftime . $hname . $haddr . $note;
                                    }
                                }
                                else
                                {
                                    if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                    {
                                        $hdetail = '<b>' . $tarea['taname'] . '</b>' . $ftime . $note;
                                    }
                                    else
                                    {
                                        $hdetail = '<b>' . $tarea['taname'] . '</b>' . $ftime . $hname . $haddr . $note;
                                    }
                                }
                            }

                            if( $t['bttrans_type'] == '0' )
                            {
                                $trans_type = 'Shared Transport';
                            }
                            else
                            {
                                $trans_type = 'Privated Transport';
                            }

                            $trans_fee = $t['bttrans_fee'] == 0 ? 'Free' : number_format( $t['bttrans_fee'], 0, ',', '.' );

                            $rtn_trans_detail .= '
                            <div class="clearfix">
                                <div class="item trip">
                                    <strong>' . ucfirst( $ttype ) . '</strong>
                                    <p><b>' . $trans_type . '</b></p>
                                </div>
                                <div class="item transport">
                                    <strong>Transport Detail</strong>
                                    <p>' . $hdetail . '</p>
                                </div>
                                <div class="item price">
                                    <strong>Transport Fee</strong>
                                    <p><b>'. $trans_fee .'</b></p>
                                </div>
                            </div>';
                        }
                        else
                        {
                            $rtn_trans_detail .= '
                            <div class="clearfix">
                                <div class="item trip">
                                    <strong>' . ucfirst( $ttype ) . '</strong>
                                    <p><b>Own Transport</b></p>
                                </div>
                                <div class="item transport"></div>
                                <div class="item price">
                                    <strong>Transport Fee</strong>
                                    <p><b>N/A</b></p>
                                </div>
                            </div>';
                        }
                    }
                }
            }

            if( $bdpstatus == 'cn' )
            {
                add_variable( 'rtn_cancel_mention_css', 'style="color:red;"' );
            }
            else
            {
                add_variable( 'rtn_cancel_mention_css', 'style="display:none; color:red;"' );
            }

            add_variable( 'rtn_trans_detail', $rtn_trans_detail );

            parse_template( 'pdf-return-loop-block', 'prl-block', true );
        }

        parse_template( 'pdf-return-block', 'pr-block' );
    }

    if( empty( $data['pmcode'] ) )
    {
        add_variable( 'sub_css', 'sr-only' );
        add_variable( 'subtotal_display', number_format( $data['bsubtotal'], 0, ',', '.' ) );
        add_variable( 'discount_display', number_format( $data['bdiscount'], 0, ',', '.' ) );
        add_variable( 'grandtotal_display', number_format( $data['btotal'], 0, ',', '.' ) );
        add_variable( 'onhandtotal_display', number_format( $data['bonhandtotal'], 0, ',', '.' ) );
        add_variable( 'remaintotal_display', number_format( ( $data['btotal'] - $data['bonhandtotal'] ), 0, ',', '.' ) );
    }
    else
    {
        add_variable( 'pmcode', 'Code : ' . $data['pmcode'] );
        add_variable( 'subtotal_display', number_format( $data['bsubtotal'], 0, ',', '.' ) );
        add_variable( 'discount_display', number_format( $data['bdiscount'], 0, ',', '.' ) );
        add_variable( 'grandtotal_display', number_format( $data['btotal'], 0, ',', '.' ) );
        add_variable( 'onhandtotal_display', number_format( $data['bonhandtotal'], 0, ',', '.' ) );
        add_variable( 'remaintotal_display', number_format( ( $data['btotal'] - $data['bonhandtotal'] ), 0, ',', '.' ) );
    }

    add_variable( 'style', HTSERVER . site_url() .'/l-plugins/ticket/css/receipt.css' );

    parse_template( 'pdf-block', 'p-block' );

    return return_template( 'receipt-pdf' );
}

function get_payment_notification_to_client( $data )
{
    $payment = isset( $data['payment'] ) && !empty( $data['payment'] ) ? end( $data['payment'] ) : '';

    if( ( isset( $payment['pstatus'] ) && $payment['pstatus'] == '1' ) || $data['bstatus'] == 'ca' )
    {
        $email_template = 'client-' . $data['bid'] . '-email';
        $email_block    = 'ce-' . $data['bid'] . '-block';

        set_template( PLUGINS_PATH . '/ticket/tpl/email/new-payment-notification-to-client.html', $email_template );

        get_email_booking_detail_content( $data, $email_template );

        add_block( 'email-block', $email_block, $email_template );

        add_variable( 'other_accommodation_css', $data['chid'] == 15 ? '' : 'display:none;' );
        add_variable( 'detail_text', 'Here is the proof of transaction # : <strong>' . $data['bticket'] . '</strong>' );

        parse_template( 'email-block', $email_block );

        return return_template( $email_template );
    }
}

function get_payment_failed_notification_to_client( $data )
{
    if( $data['bstatus'] == 'pf' )
    {
        $email_template = 'client-' . $data['bid'] . '-email';
        $email_block    = 'ce-' . $data['bid'] . '-block';

        set_template( PLUGINS_PATH . '/ticket/tpl/email/payment-failed-notification-to-client.html', $email_template );

        get_email_booking_detail_content( $data, $email_template );

        add_block( 'email-block', $email_block, $email_template );

        add_variable( 'detail_text', 'Status : Payment Failed' );
        add_variable( 'rsv_email', get_meta_data( 'email_reservation' ) );

        parse_template( 'email-block', $email_block );

        return return_template( $email_template );
    }
}

function get_no_payment_notification_to_client( $data )
{
    if( $data['bstatus'] == 'ol' )
    {
        $email_template = 'client-' . $data['bid'] . '-email';
        $email_block    = 'ce-' . $data['bid'] . '-block';

        set_template( PLUGINS_PATH . '/ticket/tpl/email/payment-failed-notification-to-client.html', $email_template );

        get_email_booking_detail_content( $data, $email_template );

        add_block( 'email-block', $email_block, $email_template );

        add_variable( 'detail_text', 'Status : Past the payout limit ( ' . date( 'd F Y H:i:s', $data['bpvaliddate'] ) . ' )' );
        add_variable( 'rsv_email', get_meta_data( 'email_reservation' ) );

        parse_template( 'email-block', $email_block );

        return return_template( $email_template );
    }
}

function get_booking_reconfirmation_to_client( $data )
{
    $email_data = array();

    set_template( PLUGINS_PATH . '/ticket/tpl/email/booking-reconfirmation-to-client.html', 'reconfirmation-email' );
    add_block( 'email-block', 'rc-block', 'reconfirmation-email' );

    extract( $data['detail'] );

    //-- Go Trip
    if( isset( $departure ) )
    {
        $departure_route = array();
        $departure_trans = array();

        foreach( $departure as $key =>$dp_trip )
        {
            extract( $dp_trip );

            if( $bdpstatus == 'cn' )
            {
                continue;
            }

            $dep_bdto         = $bdto;
            $dep_bdfrom       = $bdfrom;
            $dep_bddate       = date( 'd M Y', strtotime( $bddate ) );
            $dep_bddeparttime = date( 'h:i A', strtotime( $bddeparttime ) );
            $dep_bdarrivetime = date( 'h:i A', strtotime( $bdarrivetime ) );
            $dep_trans_detail = '';

            if( !empty( $transport ) )
            {
                foreach( $transport as $ttype => $trans )
                {
                    foreach( $trans as $t )
                    {
                        if( $t['bttrans_type'] != '2' )
                        {
                            if( empty( $t['taid'] ) )
                            {
                                $hdetail = 'No Accommodation Booked';
                            }
                            else
                            {
                                $tarea  = get_transport_area( $t['taid'] );
                                $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                                $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                                $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                if( empty( $rpfrom ) && empty( $rpto ) )
                                {
                                    $note = '';
                                }
                                else
                                {
                                    if( $ttype == 'pickup' )
                                    {
                                        $note = '<br/>In between ' . $rpfrom . ' - ' . $rpto;
                                    }
                                    else
                                    {
                                        $note = '';
                                    }
                                }

                                if( empty( $tarea['taairport'] ) )
                                {
                                    $ftime = '';
                                }
                                else
                                {
                                    $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                    $ftime = sprintf( '<br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                }

                                if( empty( $t['hid'] ) )
                                {
                                    $hdetail = $tarea['taname'] . $ftime . $note;
                                }
                                elseif( $t['hid'] == 1 )
                                {
                                    if( empty( $hname ) && empty( $haddr ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . '<br />Hotel to be advised' . $note;
                                    }
                                    else
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . '<br />' . $hname . $haddr . $note;
                                    }
                                }
                                else
                                {
                                    if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . $note;
                                    }
                                    else
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . '<br />' . $hname . $haddr . $note;
                                    }
                                }
                            }

                            if( $t['bttrans_type'] == '0' )
                            {
                                $trans_type = 'Shared Transport';
                            }
                            else
                            {
                                $trans_type = 'Privated Transport';
                            }
                        }
                        else
                        {
                            $trans_type = 'Own Transport';
                            $hdetail    = '';
                        }

                        $dep_trans_detail = '<li style="padding-bottom:5px;">' . ucfirst( $ttype ) . ', ' . $trans_type . '<br/>' . $hdetail . '</li>';
                    }
                }
            }

            $departure_route[] = $dep_bddate . ', ' . $dep_bdfrom . ' - ' . $dep_bdto . ', ' . ' ETD ' . $dep_bddeparttime . ' / ETA ' . $dep_bdarrivetime;
            $departure_trans[] = $dep_trans_detail;
        }

        $email_data['departure_route']            = empty( $departure_route ) ? '' : '<strong>Depart Trip</strong><br/>' . implode( '<br/>', $departure_route );
        $email_data['departure_transport_detail'] = empty( $departure_trans ) ? '' : '<strong>Depart Trip</strong><br/>' . '<ul style="margin:0; list-style:square; padding-left:15px;">' . implode( '', $departure_trans ) . '</ul>';
    }

    //-- Back Trip
    if( isset( $return ) && $data['btype'] == '1' )
    {
        $return_route = array();
        $return_trans = array();

        foreach( $return as $key => $rt_trip )
        {
            extract( $rt_trip );

            if( $bdpstatus == 'cn' )
            {
                continue;
            }

            $rtn_bdto         = $bdto;
            $rtn_bdfrom       = $bdfrom;
            $rtn_bddate       = date( 'd M Y', strtotime( $bddate ) );
            $rtn_bddeparttime = date( 'h:i A', strtotime( $bddeparttime ) );
            $rtn_bdarrivetime = date( 'h:i A', strtotime( $bdarrivetime ) );
            $rtn_trans_detail = '';

            if( !empty( $transport ) )
            {
                foreach( $transport as $ttype => $trans )
                {
                    foreach( $trans as $t )
                    {
                        if( $t['bttrans_type'] != '2' )
                        {
                            if( empty( $t['taid'] ) )
                            {
                                $hdetail = 'No Accommodation Booked';
                            }
                            else
                            {
                                $tarea  = get_transport_area( $t['taid'] );
                                $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                                $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                                $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                if( empty( $rpfrom ) && empty( $rpto ) )
                                {
                                    $note = '';
                                }
                                else
                                {
                                    if( $ttype == 'pickup' )
                                    {
                                        $note = '<br/>In between ' . $rpfrom . ' - ' . $rpto;
                                    }
                                    else
                                    {
                                        $note = '';
                                    }
                                }

                                if( empty( $tarea['taairport'] ) )
                                {
                                    $ftime = '';
                                }
                                else
                                {
                                    $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                    $ftime = sprintf( '<br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                }

                                if( empty( $t['hid'] ) )
                                {
                                    $hdetail = $tarea['taname'] . $ftime . $note;
                                }
                                elseif( $t['hid'] == 1 )
                                {
                                    if( empty( $hname ) && empty( $haddr ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . '<br />Hotel to be advised' . $note;
                                    }
                                    else
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . '<br />' . $hname . $haddr . $note;
                                    }
                                }
                                else
                                {
                                    if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . $note;
                                    }
                                    else
                                    {
                                        $hdetail = $tarea['taname'] . $ftime . '<br />' . $hname . $haddr . $note;
                                    }
                                }
                            }

                            if( $t['bttrans_type'] == '0' )
                            {
                                $trans_type = 'Shared Transport';
                            }
                            else
                            {
                                $trans_type = 'Privated Transport';
                            }
                        }
                        else
                        {
                            $trans_type = 'Own Transport';
                            $hdetail    = '';
                        }

                        $rtn_trans_detail .= '<li style="padding-bottom:5px;">' . ucfirst( $ttype ) . ', ' . $trans_type . '<br/>' . $hdetail . '</li>';
                    }
                }
            }

            $return_route[] = $rtn_bddate . ', ' . $rtn_bdfrom . ' - ' . $rtn_bdto . ', ' . ' ETD ' . $rtn_bddeparttime . ' / ETA ' . $rtn_bdarrivetime;
            $return_trans[] = $rtn_trans_detail;
        }

        $email_data['return_route']            = empty( $return_route ) ? '' : '<strong>Return Trip</strong><br/>' . implode( '<br/>', $return_route );
        $email_data['return_transport_detail'] = empty( $return_trans ) ? '' : '<strong>Return Transport</strong><br/>' . '<ul style="margin:0; list-style:square; padding-left:15px;">' . implode( '', $return_trans ) . '</ul>';
        $email_data['return_css']              = empty( $return_route ) ? 'display:none; padding:0;' : '';
    }
    else
    {
        $email_data['return_css']              = 'display:none; padding:0;';
        $email_data['return_route']            = '';
        $email_data['return_transport_detail'] = '';
    }

    $dep_pdata = get_pass_name_and_num_of_pass( $data['detail'] );

    $email_data['customer_name']  = $dep_pdata['pass_name'][0];
    $email_data['passanger_name'] = implode( ', ', $dep_pdata['pass_name'] );
    $email_data['num_of_person']  = implode( ', ', $dep_pdata['num_of_person'] );
    $email_data['users']          = get_display_name( $_COOKIE['user_id'] );
    $email_data['payment_status'] = ticket_booking_status( $data['bstatus'] );

    add_variable( 'email_content' , return_email_setting( 'reconfirmation_email_content', $email_data ) );

    parse_template( 'email-block', 'rc-block' );

    return return_template( 'reconfirmation-email' );
}

function create_booking_payment_with_paypal( $data )
{
    $route   = array();
    $baseurl = site_url();
    $kurs    = grab_currency( 'USD', 'IDR', 1 );
    $rate    = round( $data['btotal'] / $kurs, 2 );
    $adds    = round( ( 5 / 100 ) * $rate, 2 );
    $btotal  = $rate + $adds;

    foreach( $data['detail'] as $dt )
    {
        foreach( $dt as $d )
        {
            $route[] = ucfirst( $d['bdtype'] ) . ', ' . $d['bdfrom'] . ' to ' . $d['bdto'];
        }
    }

    $name = '#' . $data['bticket'] . ' : ' . implode( ' - ', $route );

    $item = new Item();
    $item->setName( $name )->setCurrency( 'USD' )->setQuantity( 1 )->setPrice( $btotal );

    $items[] = $item;

    $payer = new Payer();
    $payer->setPaymentMethod( 'paypal' );

    $itemList = new ItemList();
    $itemList->setItems( $items );

    $details = new Details();
    $details->setSubtotal( $btotal );

    $amount = new Amount();
    $amount->setCurrency( 'USD' )->setTotal( $btotal )->setDetails( $details );

    $transaction = new Transaction();
    $transaction->setAmount( $amount )->setItemList( $itemList )->setDescription( 'Payment For Booking ID #' . $data['bticket'] )->setInvoiceNumber( uniqid() );

    $redirecturls = new RedirectUrls();
    $redirecturls->setReturnUrl( HTSERVER . $baseurl . '/booking/payment/?type=paypal&id=' . $data['bid'] . '&btotal=' . $btotal )->setCancelUrl( HTSERVER . $baseurl . '/booking/canceled/?id=' . $data['bid'] );

    $payment = new Payment();
    $payment->setIntent( 'sale' )->setPayer( $payer )->setRedirectUrls( $redirecturls )->setTransactions( array( $transaction ) );

    $request = clone $payment;

    try
    {
        $setting = get_paypal_action_link();
        $context = getApiContext( $setting['client_id'], $setting['client_secret'] );

        $payment->create( $context );

        return $payment->getApprovalLink();
    }
    catch( PayPalConnectionException $ex )
    {
        return '';
    }
    catch( Exception $ex )
    {
        return '';
    }
}

function execute_booking_payment_with_paypal( $payment_id, $payer_id, $bid, $btotal = 0 )
{
    $data    = ticket_booking_all_data( $bid );
    $setting = get_paypal_action_link();
    $context = getApiContext( $setting['client_id'], $setting['client_secret'] );
    $payment = Payment::get( $payment_id, $context );

    $execution = new PaymentExecution();
    $execution->setPayerId( $payer_id );

    $details = new Details();
    $details->setSubtotal( $btotal );

    $amount = new Amount();
    $amount->setCurrency( 'USD' )->setTotal( $btotal )->setDetails( $details );

    $transaction = new Transaction();
    $transaction->setAmount( $amount );

    $execution->addTransaction( $transaction );

    try
    {
        $result = $payment->execute( $execution, $context );
        $object = json_decode( $result, true );

        if( $object !== null && json_last_error() === JSON_ERROR_NONE )
        {
            extract( $object );

            if( $state == 'approved' )
            {
                $account_name   = $payer['payer_info']['first_name'] . ' ' . $payer['payer_info']['last_name'];
                $paypal_payment = isset( $data['payment'] ) && !empty( $data['payment'] ) ? end( $data['payment'] ) : '';

                if( isset( $paypal_payment['pid'] ) && !empty( $paypal_payment['pid'] ) )
                {
                    if( update_booking_payment( $paypal_payment['pid'], $bid, $payment->getId(), strtotime( $update_time ), $data['btotal'], $btotal, ucwords( $payer['payment_method'] ), '1', $payer['payer_info']['email'], $payer['payer_info']['payer_id'], null, $account_name, $transactions[0]['description'], $transactions[0]['invoice_number'], $transactions[0]['related_resources'][0]['sale']['id'] ) )
                    {
                        if( ticket_booking_update_status( $bid, 'pa' ) )
                        {
                            $ndata    = ticket_booking_all_data( $bid  );
                            $pdf_data = generate_booking_pdf_ticket( $ndata );

                            ticket_booking_update_agent_transaction( $ndata );
                            ticket_booking_update_on_hand_payment( $bid, $ndata['btotal'] );

                            send_booking_ticket_to_client( $ndata, $pdf_data );
                            send_payment_notification_to_admin( $bid, $pdf_data );
                        }

                        return true;
                    }
                }
                else
                {
                    if( save_booking_payment( $bid, generate_payment_id(), $payment->getId(), strtotime( $update_time ), $data['btotal'], $btotal, ucwords( $payer['payment_method'] ), '1', $payer['payer_info']['email'], $payer['payer_info']['payer_id'], null, $account_name, $transactions[0]['description'], $transactions[0]['invoice_number'], $transactions[0]['related_resources'][0]['sale']['id'] ) )
                    {
                        if( ticket_booking_update_status( $bid, 'pa' ) )
                        {
                            $ndata    = ticket_booking_all_data( $bid  );
                            $pdf_data = generate_booking_pdf_ticket( $ndata );

                            ticket_booking_update_agent_transaction( $ndata );
                            ticket_booking_update_on_hand_payment( $bid, $ndata['btotal'] );

                            send_booking_ticket_to_client( $ndata, $pdf_data );
                            send_payment_notification_to_admin( $bid, $pdf_data );
                        }

                        return true;
                    }
                }
            }
        }

        try
        {
            $payment = Payment::get( $payment_id, $context );
        }
        catch( Exception $ex )
        {
            return false;
        }
    }
    catch( PayPalConnectionException $ex )
    {
        return false;
    }
    catch( Exception $ex )
    {
        return false;
    }
}

function create_booking_payment_with_nicepay( $post )
{
    include_once PLUGINS_PATH . '/ticket/include/nicepay/lib/NicepayLib.php';

    $nicepay = new NicepayLib();
    $data    = ticket_booking_all_data( $post['bid'] );

    if( isset( $post['payMethod'] ) && $post['payMethod'] == '01' && isset( $post['amt'] ) && $post['amt'] )
    {
        $amt = $post['amt'];
        $con = $data['bbcountry'] == 248 || empty( $data['bbcountry'] ) ? '-' : get_country( $data['bbcountry'], 'lcountry' );

        //-- Populate Mandatory parameters to send
        $nicepay->set( 'amt', $amt );
        $nicepay->set( 'payMethod', '01' );
        $nicepay->set( 'currency', 'IDR' );
        $nicepay->set( 'referenceNo', generate_reference_code() );
        $nicepay->set( 'description', 'Payment of Reference Code #' . $nicepay->get( 'referenceNo' ) );

        //-- Customer Data
        $nicepay->set( 'billingNm', $data['bbname'] );
        $nicepay->set( 'billingEmail', $data['bbemail'] );
        $nicepay->set( 'billingPhone', empty( $data['bbphone'] ) ? ( empty( $data['bbphone2'] ) ? '-' : $data['bbphone2'] ) : $data['bbphone'] );

        $nicepay->set( 'billingAddr', '-' );
        $nicepay->set( 'billingCity', '-' );
        $nicepay->set( 'billingState', '-' );
        $nicepay->set( 'billingPostCd', '-' );
        $nicepay->set( 'billingCountry', $con );

        $nicepay->set( 'deliveryNm', $data['bbname'] );
        $nicepay->set( 'deliveryEmail', $data['bbemail'] );
        $nicepay->set( 'deliveryPhone', empty( $data['bbphone'] ) ? ( empty( $data['bbphone2'] ) ? '-' : $data['bbphone2'] ) : $data['bbphone'] );

        $nicepay->set( 'deliveryAddr', '-' );
        $nicepay->set( 'deliveryCity', '-' );
        $nicepay->set( 'deliveryState', '-' );
        $nicepay->set( 'deliveryPostCd', '-' );
        $nicepay->set( 'deliveryCountry', $con );

        //-- Send Data
        $response = $nicepay->chargeCard();

        //-- Response from NICEPAY
        if( isset( $response->data->resultCd ) && $response->data->resultCd == '0000' )
        {
            $bid         = $post['bid'];
            $pid         = $post['pid'];
            $ppaycode    = $response->tXid;
            $ppayid      = generate_payment_id();

            if( empty( $pid ) )
            {
                if( save_booking_payment( $bid, $ppayid, $ppaycode, 0, 0, 0, 'Nicepay Credit Card', '0', $data['bbemail'] ) )
                {
                    header( 'Location: ' . $response->data->requestURL . '?tXid=' . $response->tXid );
                }
            }
            else
            {
                if( update_booking_payment( $pid, $bid, $ppaycode, 0, 0, 0, 'Nicepay Credit Card', '0', $data['bbemail'] ) )
                {
                    header( 'Location: ' . $response->data->requestURL . '?tXid=' . $response->tXid );
                }
            }
        }
        else
        {
            $cctype = get_meta_data( 'cc_payment_gateway_opt');

            if( $cctype == 'nicepay' )
            {
                header( 'Location: ' . HTSERVER . site_url() . '/booking/redirect-secondary-payment/?type=doku&id=' . $data['bid'] );
            }
            else
            {
                header( 'Location: ' . HTSERVER . site_url() . '/booking/failed-payment/?id=' . $data['bid'] );
            }
        }
    }
    else if( isset( $post['payMethod'] ) && $post['payMethod'] == '02' && isset( $post['amt'] ) && $post['amt'] )
    {
        $bankCd       = $post['bankCd'];
        $amt          = $post['amt'];
        $dateNow      = date( 'Ymd' );
        $vaExpiryDate = date( 'Ymd', strtotime( $dateNow . ' +1 day' ) );
        $con          = $data['bbcountry'] == 248 || empty( $data['bbcountry'] ) ? '-' : get_country( $data['bbcountry'], 'lcountry' );

        $nicepay->set( 'amt', $amt );
        $nicepay->set( 'bankCd', $bankCd );
        $nicepay->set( 'payMethod', '02' );
        $nicepay->set( 'currency', 'IDR' );
        $nicepay->set( 'referenceNo', generate_reference_code() );
        $nicepay->set( 'description', 'Payment of Reference Code #' . $nicepay->get( 'referenceNo' ) );

        //-- Customer Data
        $nicepay->set( 'billingNm', $data['bbname'] );
        $nicepay->set( 'billingEmail', $data['bbemail'] );
        $nicepay->set( 'billingPhone', empty( $data['bbphone'] ) ? ( empty( $data['bbphone2'] ) ? '-' : $data['bbphone2'] ) : $data['bbphone'] );

        $nicepay->set( 'billingAddr', '-' );
        $nicepay->set( 'billingCity', '-' );
        $nicepay->set( 'billingState', '-' );
        $nicepay->set( 'billingPostCd', '-' );
        $nicepay->set( 'billingCountry', $con );

        $nicepay->set( 'deliveryNm', $data['bbname'] );
        $nicepay->set( 'deliveryEmail', $data['bbemail'] );
        $nicepay->set( 'deliveryPhone', empty( $data['bbphone'] ) ? ( empty( $data['bbphone2'] ) ? '-' : $data['bbphone2'] ) : $data['bbphone'] );

        $nicepay->set( 'deliveryAddr', '-' );
        $nicepay->set( 'deliveryCity', '-' );
        $nicepay->set( 'deliveryState', '-' );
        $nicepay->set( 'deliveryPostCd', '-' );
        $nicepay->set( 'deliveryCountry', empty( $data['agid'] ) ? get_country( $data['bbcountry'], 'lcountry' ) : '-' );

        $nicepay->set( 'vacctValidDt', $vaExpiryDate );
        $nicepay->set( 'vacctValidTm', date( 'His' ) );

        //-- Send Data
        $response = $nicepay->requestVA();

        //-- Response from NICEPAY
        if( isset( $response->resultCd ) && $response->resultCd == '0000' )
        {
            //-- Listen for parameters passed
            $bid          = $post['bid'];
            $ppaycode     = $response->tXid;
            $pnote        = $response->description;
            $pvirtualacc  = $response->bankVacctNo;
            $prefcode     = $response->referenceNo;
            $paccountname = $response->billingNm;
            $pvaccvdate   = $response->vacctValidDt;
            $pvaccvtime   = $response->vacctValidTm;
            $ppayid       = generate_payment_id();
            $ppaydate     = strtotime( $response->transDt );
            $pbankname    = get_bank_cd_name( $response->bankCd );
            $ppayemail    = $data['bbemail'];

            if( empty( $post['pid'] ) )
            {
                if( save_booking_payment( $bid, $ppayid, $ppaycode, $ppaydate, 0, 0, 'Nicepay Transfer', '0', $ppayemail, null, $pbankname, $paccountname, $pnote, $prefcode, $pvirtualacc, $pvaccvdate, $pvaccvtime ) )
                {
                    header( 'Location: ' . $response->callbackUrl . '&tXid=' . $response->tXid );
                }
            }
            else
            {
                if( update_booking_payment( $post['pid'], $bid, $ppaycode, $ppaydate, 0, 0, 'Nicepay Transfer', '0', $ppayemail, null, $pbankname, $paccountname, $pnote, $prefcode, $pvirtualacc, $pvaccvdate, $pvaccvtime ) )
                {
                    header( 'Location: ' . $response->callbackUrl . '&tXid=' . $response->tXid );
                }
            }
        }
        elseif( isset( $response->resultCd ) )
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}

function check_valid_transfer_date_with_nicepay( $pvaccvdate, $pvaccvtime )
{
    $vdate = date( 'Y-m-d', strtotime( $pvaccvdate ) );
    $vtime = date( 'H:i:s', strtotime( $pvaccvtime ) );

    $ntime = strtotime( date( 'Y-m-d H:i:s' ) );
    $vtime = strtotime( $vdate . ' ' . $vtime );

    if( $ntime > $vtime )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function execute_check_payment_status_with_nicepay( $tXid, $referenceNo, $amt )
{
    include_once PLUGINS_PATH . '/ticket/include/nicepay/lib/NicepayLib.php';

    $nicepay = new NicepayLib();

    $nicepay->set( 'amt', $amt );
    $nicepay->set( 'tXid', $tXid );
    $nicepay->set( 'referenceNo', $referenceNo );
    $nicepay->set( 'iMid', get_meta_data( 'nicepay_imid', 'ticket_setting' ) );

    $merchantToken = $nicepay->merchantTokenC();

    $nicepay->set( 'merchantToken', $merchantToken );

    $paymentStatus = $nicepay->checkPaymentStatus( $tXid, $referenceNo, $amt );
}

function execute_payment_notify_by_nicepay()
{
    global $db;

    include_once PLUGINS_PATH . '/ticket/include/nicepay/lib/NicepayLib.php';

    $nicepay = new NicepayLib();

    $push_param = array( 'tXid', 'referenceNo', 'merchantToken', 'amt' );

    $nicepay->extractNotification( $push_param );

    $pushedid    = $nicepay->iMid;
    $ptotal      = $nicepay->getNotification( 'amt' );
    $ppaycode    = $nicepay->getNotification( 'tXid' );
    $prefcode    = $nicepay->getNotification( 'referenceNo' );
    $pushedtoken = $nicepay->getNotification( 'merchantToken' );

    $s = 'SELECT * FROM ticket_booking_payment AS a LEFT JOIN ticket_booking AS b ON a.bid = b.bid WHERE a.ppaycode = %s AND b.bstt <> %s';
    $q = $db->prepare_query( $s, $ppaycode, 'ar' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        if( $d['bpaymethod'] == 2 )
        {
            if( isset( $d['pstatus'] ) && $d['pstatus'] == '0' )
            {
                $nicepay->set( 'amt', $ptotal );
                $nicepay->set( 'tXid', $ppaycode );
                $nicepay->set( 'iMid', $pushedid );
                $nicepay->set( 'referenceNo', $prefcode );

                $merchanttoken = $nicepay->merchantTokenC();

                $nicepay->set( 'merchantToken', $merchanttoken );

                $paymentStatus = $nicepay->checkPaymentStatus( $ppaycode, $prefcode, $ptotal );

                if( $pushedtoken == $merchanttoken )
                {
                    if( isset( $paymentStatus->status ) && $paymentStatus->status == '0' )
                    {
                        extract( ( array ) $paymentStatus );

                        if( update_booking_payment( $d['pid'], $d['bid'], $tXid, $d['ppaydate'], $amt, 0, 'Nicepay Transfer', '1', $d['ppayemail'], null, get_bank_cd_name( $bankCd ), $billingNm, $goodsNm, $referenceNo, $vacctNo, $vacctValidDt, $vacctValidTm ) )
                        {
                            if( ticket_booking_update_status( $d['bid'], 'pa' ) )
                            {
                                $ndata    = ticket_booking_all_data( $d['bid'] );
                                $pdf_data = generate_booking_pdf_ticket( $ndata );

                                ticket_booking_update_agent_transaction( $ndata );
                                ticket_booking_update_on_hand_payment( $d['bid'], $ndata['btotal'] );

                                send_booking_ticket_to_client( $ndata, $pdf_data );
                                send_payment_notification_to_admin( $d['bid'], $pdf_data );

                                return array( 'bid' => $d['bid'], 'result' => 'success' );
                            }
                        }
                    }
                    else
                    {
                        return array( 'bid' => $d['bid'], 'result' => 'failed' );
                    }
                }
            }
        }
        else
        {
            $nicepay->set( 'amt', $ptotal );
            $nicepay->set( 'tXid', $ppaycode );
            $nicepay->set( 'iMid', $pushedid );
            $nicepay->set( 'referenceNo', $prefcode );

            $merchanttoken = $nicepay->merchantTokenC();

            $nicepay->set( 'merchantToken', $merchanttoken );

            $paymentStatus = $nicepay->checkPaymentStatus( $ppaycode, $prefcode, $ptotal );

            if( $pushedtoken == $merchanttoken )
            {
                if( isset( $paymentStatus->status ) && $paymentStatus->status == '0' )
                {
                    return array( 'bid' => $d['bid'], 'result' => 'success' );
                }
                else
                {
                    return array( 'bid' => $d['bid'], 'result' => 'failed' );
                }
            }
        }
    }
}

function execute_booking_payment_with_nicepay()
{
    global $db;

    if( isset( $_GET['tXid'] ) )
    {
        $result = array();

        $s = 'SELECT pid, bid FROM ticket_booking_payment WHERE ppaycode = %s';
        $q = $db->prepare_query( $s, $_GET['tXid'] );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $d = $db->fetch_array( $r );

            if( isset( $_GET['resultCd'] ) )
            {
                if( $_GET['resultCd'] == '0000' && isset( $_GET['cardNo'] ) )
                {
                    $s2 = 'UPDATE ticket_booking_payment SET
                            ppaydate = %s,
                            ptotal = %s,
                            prefcode = %s,
                            pnote = %s,
                            pstatus = %s
                           WHERE pid = %d';
                    $q2 = $db->prepare_query( $s2, strtotime( $_GET['transDt'] ), $_GET['amount'], $_GET['referenceNo'], $_GET['description'], '1', $d['pid'] );

                    if( $db->do_query( $q2 ) )
                    {
                        if( ticket_booking_update_status( $d['bid'], 'pa' ) )
                        {
                            $ndata    = ticket_booking_all_data( $d['bid'] );
                            $pdf_data = generate_booking_pdf_ticket( $ndata );

                            ticket_booking_update_agent_transaction( $ndata );
                            ticket_booking_update_on_hand_payment( $d['bid'], $ndata['btotal'] );

                            send_booking_ticket_to_client( $ndata, $pdf_data );
                            send_payment_notification_to_admin( $d['bid'], $pdf_data );

                            return array( 'result' => 'paid', 'bid' => $d['bid'] );
                        }
                    }
                }
                else
                {
                    return array( 'result' => 'failed', 'bid' => $d['bid'] );
                }
            }

            return array( 'result' => 'success', 'bid' => $d['bid'] );
        }

        return array();
    }
}

function execute_booking_payment_with_doku()
{
    global $db;

    if( $_GET['process'] == 'notify' )
    {
        if( $_POST['RESULTMSG'] == 'SUCCESS' )
        {
            list( $var, $bcode, $num ) = explode( '-', $_POST['TRANSIDMERCHANT'] );

            $bid  = ticket_booking_data_by_code( $bcode, 'bid' );
            $data = ticket_booking_all_data( $bid );

            extract( $data );

            $ptotal      = $_POST['AMOUNT'];
            $prefcode    = $_POST['TRANSIDMERCHANT'];
            $papproval   = $_POST['APPROVALCODE'];
            $pbankname   = $_POST['BANK'];
            $pchname     = $_POST['CHNAME'];
            $pbrand      = $_POST['BRAND'];
            $pwords      = $_POST['WORDS'];
            $pmcn        = $_POST['MCN'];
            $presult     = $_POST['RESULTMSG'];
            $ppaydate    = strtotime( $_POST['PAYMENTDATETIME'] );
            $pnote       = 'Payment of Reference Code #' . $_POST['TRANSIDMERCHANT'];

            if( isset( $payment['pid'] ) )
            {
                if( update_booking_payment( $payment['pid'], $bid, null, $ppaydate, $ptotal, 0, 'Doku', '1', $bbemail, null, $pbankname, null, $pnote, $prefcode, null, 0, 0, $papproval, $pchname, $pbrand, $pwords, $pmcn, $presult ) )
                {
                    if( ticket_booking_update_status( $bid, 'pa' ) )
                    {
                        $ndata    = ticket_booking_all_data( $bid );
                        $pdf_data = generate_booking_pdf_ticket( $ndata );

                        ticket_booking_update_agent_transaction( $ndata );
                        ticket_booking_update_on_hand_payment( $bid, $ndata['btotal'] );

                        send_booking_ticket_to_client( $ndata, $pdf_data );
                        send_payment_notification_to_admin( $bid, $pdf_data );
                    }

                    echo 'CONTINUE';
                }
                else
                {
                    echo 'STOP1';
                }
            }
            else
            {
                $ppayid = generate_payment_id();

                if( save_booking_payment( $bid, $ppayid, null, $ppaydate, $ptotal, 0, 'Doku', '1', $bbemail, null, $pbankname, null, null, $prefcode, null, 0, 0, $papproval, $pchname, $pbrand, $pwords, $pmcn, $presult ) )
                {
                    if( ticket_booking_update_status( $bid, 'pa' ) )
                    {
                        $ndata    = ticket_booking_all_data( $bid );
                        $pdf_data = generate_booking_pdf_ticket( $ndata );

                        ticket_booking_update_agent_transaction( $ndata );
                        ticket_booking_update_on_hand_payment( $bid, $ndata['btotal'] );

                        send_booking_ticket_to_client( $ndata, $pdf_data );
                        send_payment_notification_to_admin( $bid, $pdf_data );
                    }

                    echo 'CONTINUE';
                }
                else
                {
                    echo 'STOP1';
                }
            }
        }
        else
        {
            echo 'STOP2';
        }

        exit;
    }
    elseif( $_GET['process'] == 'redirect' )
    {
        if( isset( $_POST['STATUSCODE'] ) && $_POST['STATUSCODE'] == 0000 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

function get_booking_pending_data()
{
    global $db;

    $s = 'SELECT * FROM ticket_booking WHERE bstatus = %s AND bdate <= %s AND bpvaliddate <> 0';
    $q = $db->prepare_query( $s, 'ol', date( 'Y-m-d' ) );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = $d;
    }

    return $data;
}

function get_booking_blocking_time_data()
{
    global $db;

    $s = 'SELECT * FROM ticket_booking WHERE bblockingtime <> %d';
    $q = $db->prepare_query( $s, 0 );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = $d;
    }

    return $data;
}

function ticket_booking_status_by_id( $bid )
{
    global $db;

    $s = 'SELECT bstatus FROM ticket_booking WHERE bid = %d';
    $q = $db->prepare_query( $s, $bid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return isset( $d['bstatus'] ) ? $d['bstatus'] : '';
}

function generate_doku_reference_code( $bcode )
{
    $micro_date = microtime();
    $date_array = explode( ' ', $micro_date );
    $date       = date( 'Ymd', $date_array[1] );
    $date_data  = preg_replace( '/[^\p{L}\p{N}\s]/u', '', $date_array[0] );

    return 'BWE-' . $bcode . '-' . $date_data . rand( 10, 99 );
}

function generate_reference_code()
{
    $micro_date = microtime();
    $date_array = explode( ' ', $micro_date );
    $date       = date( 'YmdHis', $date_array[1] );
    $date_data  = preg_replace( '/[^\p{L}\p{N}\s]/u', '', $date_array[0] );

    return 'REF'.$date.$date_data.rand(100,999);
}

function get_bank_cd_name( $code = '' )
{
    $bank_list = array(
        'CENA' => 'BCA',
        'BNIN' => 'BNI',
        'BMRI' => 'Mandiri',
        'BBBA' => 'Permata',
        'IBBK' => 'BII Maybank',
        'BRIN' => 'BRI',
        'BNIA' => 'CIMB Niaga',
        'HNBN' => 'KEB Hana Bank',
        'BDIN' => 'Danamon'
    );

    return empty( $code ) ? $bank_list : $bank_list[ $code ];
}

function get_paypal_action_link()
{
    return array(
        'client_id' => get_meta_data( 'paypal_client_id', 'ticket_setting' ),
        'client_secret' => get_meta_data( 'paypal_client_secret', 'ticket_setting' )
    );
}

function grab_currency( $from_Currency, $to_Currency, $amount )
{
    $data = file_get_contents( 'http://free.currencyconverterapi.com/api/v3/convert?q=' . $from_Currency . '_' . $to_Currency . '&compact=ultra&apiKey=408061195e560cbca518' );
    $json = json_decode( $data, true );

    preg_match( '/<span class=bld>(.*)<\/span>/', $data, $converted );

    if( isset( $json[ $from_Currency . '_' . $to_Currency ] ) )
    {
        return $json[ $from_Currency . '_' . $to_Currency ];
    }
    else
    {
        return 0;
    }
}

function generate_code_number( $type = 0 )
{
    global $db;

    //-- No ticket : [type][year][month][date][no-urut]
    //-- ex : [0][2013][01][20][9999] = 0201301209999
    $p = $type . date('Y') . date('m') . date('d');
    $q = $db->prepare_query( 'SELECT MAX( substring( bcode, 10, 4 ) ) as cnumber FROM ticket_booking WHERE substring( bcode, 1, 9 ) = %s', $p );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $number = $d['cnumber'] == NULL ? 1 : intval( $d['cnumber'] ) + 1;

    if( $number > 9999 )
    {
        $number = 1;
    }
    elseif( $number < 10 )
    {
        $number = '000' . $number;
    }
    elseif( $number < 100 )
    {
        $number = '00' . $number;
    }
    elseif( $number < 1000 )
    {
        $number = '0' . $number;
    }
    else
    {
        $number = $number;
    }

    $ticket_number = $p . $number;

    return $ticket_number;
}

function generate_ticket_number()
{
    global $db;

    $p = 'BWE' . date('y') . date('m');
    $q = $db->prepare_query( 'SELECT MAX( substring( bticket, 8, 4 ) ) as tnumber FROM ticket_booking WHERE substring( bticket, 1, 7 ) = %s', $p );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $number = $d['tnumber'] == NULL ? 1 : intval( $d['tnumber'] ) + 1;

    if( $number > 9999 )
    {
        $number = 1;
    }
    elseif( $number < 10 )
    {
        $number = '000' . $number;
    }
    elseif( $number < 100 )
    {
        $number = '00' . $number;
    }
    elseif( $number < 1000 )
    {
        $number = '0' . $number;
    }
    else
    {
        $number = $number;
    }

    $ticket_number = $p . $number;

    return $ticket_number;
}

function generate_payment_id()
{
    global $db;

    $p = 'PAY' . date('y') . date('m');
    $q = $db->prepare_query( 'SELECT MAX( substring( ppayid, 8, 4 ) ) as pnumber FROM ticket_booking_payment WHERE substring( ppayid, 1, 7 ) = %s', $p );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $number = $d['pnumber'] == NULL ? 1 : intval( $d['pnumber'] ) + 1;

    if( $number > 9999 )
    {
        $number = 1;
    }
    elseif( $number < 10 )
    {
        $number = '000' . $number;
    }
    elseif( $number < 100 )
    {
        $number = '00' . $number;
    }
    elseif( $number < 1000 )
    {
        $number = '0' . $number;
    }
    else
    {
        $number = $number;
    }

    $ticket_number = $p . $number;

    return $ticket_number;
}

function is_search_booking()
{
    if( isset( $_POST['sbooking'] ) && $_POST['sbooking'] != '' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_view_booking()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'view' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_edit_booking()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'edit' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_revise_booking()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'revise' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_cancel_reservation()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'cancel-reservation' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_popup_detail_booking()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'popup-detail' )
    {
        add_actions( 'is_use_ajax', true );

        return true;
    }
    else
    {
        return false;
    }
}

function is_popup_edit_booking()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'popup-edit' )
    {
        add_actions( 'is_use_ajax', true );

        return true;
    }
    else
    {
        return false;
    }
}

function is_popup_revise_booking()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'popup-revise' )
    {
        add_actions( 'is_use_ajax', true );

        return true;
    }
    else
    {
        return false;
    }
}

function is_popup_cancel_reservation()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking' && isset( $_GET['prc'] ) && $_GET['prc'] == 'popup-cancel-reservation' )
    {
        add_actions( 'is_use_ajax', true );

        return true;
    }
    else
    {
        return false;
    }
}

function ticket_booking_is_filter_view()
{
    if( isset( $_GET['state'] ) && $_GET['state']=='reservation' && isset( $_GET['prm'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function ticket_booking_status( $status = '' )
{
    $arr = array(
        'pa' => 'Paid',
        'ca' => 'Credit',
        'pp' => 'Pending Payment',
        'cr' => 'Request Cancelation',
        'cn' => 'Cancelled',
        'rf' => 'Refunded',
        'bc' => 'Blocked',
        'pv' => 'Pending Verification',
        'pf' => 'Payment Failed'
    );

    $status = $status == 'ol' ? 'pp' : $status;

    if( !empty( $status ) &&  isset( $arr[$status] ) )
    {
        return $arr[$status];
    }
    else
    {
        return $arr;
    }
}

function ticket_booking_type( $type )
{
    $type_array = array(
        0 => 'One Way Trip',
        1 => 'Return Trip'
    );

    if( isset( $type_array[$type] ) )
    {
        return $type_array[$type];
    }
    else
    {
        return $type_array;
    }
}

function ticket_booking_data( $id )
{
    global $db;

    $s = 'SELECT * FROM ticket_booking AS a
          LEFT JOIN ticket_agent AS c ON a.agid = c.agid
          LEFT JOIN ticket_notification AS d ON d.bid = a.bid
          WHERE a.bid = %d AND a.bstt <> %s';
    $q = $db->prepare_query( $s, $id, 'ar' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return $db->fetch_array( $r );
    }
    else
    {
        return null;
    }
}

function ticket_booking_data_by_code( $bcode, $field = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_booking AS a WHERE a.bcode = %s AND a.bstt <> %s';
    $q = $db->prepare_query( $s, $bcode, 'ar' );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( empty( $field ) )
    {
        return $d;
    }
    else
    {
        return $d[ $field ];
    }
}

function ticket_booking_all_data( $id )
{
    global $db;

    $s = 'SELECT
            a.*,
            d.ntitle,
            b.agname,
            b.agemail,
            b.agphone,
            b.agpayment_type,
            c.chname,
            e.bdid,
            e.rid
          FROM ticket_booking AS a
          LEFT JOIN ticket_agent AS b ON a.agid = b.agid
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          LEFT JOIN ticket_notification AS d ON d.bid = a.bid
          LEFT JOIN ticket_booking_detail AS e ON a.bid = e.bid
          WHERE a.bid = %d AND a.bstt <> %s';
    $q = $db->prepare_query( $s, $id, 'ar' );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) )
    {
        $data = $db->fetch_array( $r );

        if( !empty( $data['agid'] ) )
        {
            $data['bonhandtotal'] = get_agent_invoice_paid_off( $data['agid'], $data['bid'] );
        }

        $data['detail']  = ticket_booking_detail( $id );
        $data['payment'] = ticket_booking_payment_detail( $id );
        $data['agent_transaction'] = ticket_booking_agent_transaction( $id );
    }

    return $data;
}

function ticket_booking_cancel_request( $post )
{
    global $db;

    $retval = 1;

    $db->begin();

    if( $post['bcreason'] == 'Other' )
    {
        $post['bcreason'] = $post['bcoreason'];
    }

    $s = 'UPDATE ticket_booking SET bcreason = %s, bcmessage = %s, bstatus = %s, brcdate = %s WHERE bid = %d';
    $q = $db->prepare_query( $s, $post['bcreason'], $post['bcmessage'], 'cr', date( 'Y-m-d' ), $post['bid'] );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        $retval = 0;
    }
    else
    {
        $s = 'UPDATE ticket_booking_detail SET bdpstatus = %s WHERE bid = %d';
        $q = $db->prepare_query( $s, 'cr', $post['bid'] );
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            $retval = 0;
        }
    }

    if( $retval == 0 )
    {
        $db->rollback();

        return false;
    }
    else
    {
        $db->commit();

        send_booking_cancelation_request_to_admin( $post['bid'] );

        return true;
    }
}

function ticket_booking_canceled_data( $post )
{
    global $db;

    if( $post['bcreason'] == 'Other' )
    {
        $post['bcreason'] = $post['bcoreason'];
    }

    $s = 'UPDATE ticket_booking SET bcreason = %s, bcmessage = %s, bstatus = %s, brcdate = %s WHERE bid = %d';
    $q = $db->prepare_query( $s, $post['bcreason'], $post['bcmessage'], 'cr', date( 'Y-m-d' ), $post['bid'] );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $s = 'UPDATE ticket_booking_detail SET bdpstatus = %s WHERE bid = %d';
        $q = $db->prepare_query( $s, 'cr', $post['bid'] );
        $r = $db->do_query( $q );

        send_booking_cancelation_request_to_admin( $post['bid'] );

        return true;
    }
}

function ticket_booking_update_data_per_field( $bid, $field, $value )
{
    global $db;

    $s = 'UPDATE ticket_booking SET ' . $field . ' = %s WHERE bid = %d';
    $q = $db->prepare_query( $s, $value, $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ticket_booking_update_status( $bid, $status )
{
    global $db;

    $s = 'UPDATE ticket_booking SET bstatus = %s WHERE bid = %d';
    $q = $db->prepare_query( $s, $status, $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        $s = 'UPDATE ticket_booking_detail SET bdpstatus = %s WHERE bid = %d';
        $q = $db->prepare_query( $s, $status, $bid );
        $r = $db->do_query( $q );

        return true;
    }
}

function ticket_booking_update_print_boarding_status( $bid, $status )
{
    global $db;

    $s = 'UPDATE ticket_booking SET bprintboardingstatus = %s WHERE bid = %d';
    $q = $db->prepare_query( $s, $status, $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ticket_booking_update_feedback_status( $bid, $status )
{
    global $db;

    $s = 'UPDATE ticket_booking SET bfeedback = %d WHERE bid = %d';
    $q = $db->prepare_query( $s, $status, $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ticket_booking_update_on_hand_payment( $bid, $bonhandtotal = 0 )
{
    global $db;

    $s = 'UPDATE ticket_booking SET bonhandtotal = %s WHERE bid = %d';
    $q = $db->prepare_query( $s, $bonhandtotal, $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ticket_booking_update_agent_transaction( $data = array() )
{
    global $db;

    if( empty( $data ) === false )
    {
        extract( $data );

        $atgname = array();

        foreach( $detail as $obj )
        {
            foreach( $obj as $dt )
            {
                foreach( $dt[ 'passenger' ] as $pass )
                {
                    foreach( $pass as $g )
                    {
                        if( $g[ 'bpstatus' ] != 'cn' )
                        {
                            $atgname[] = $g[ 'bpname' ];
                        }
                    }
                }
            }
        }

        if( empty( $atgname ) )
        {
            $atgname = '';
        }
        else
        {
            $atgname = array_unique( $atgname );
            $atgname = implode( ',', $atgname );
        }

        if( empty( $pmcode ) === false && empty( $agid ) === false )
        {
            $s = 'INSERT INTO ticket_agent_transaction( agid, bcode, bid, atdate, atgname, atdebet, atcredit, atstatus ) VALUES( %d, %s, %d, %s, %s, %s, %s, %d )';
            $q = $db->prepare_query( $s, $agid, $bcode, $bid, date( 'Y-m-d' ), $atgname, 0, $btotal, 2 );
            $r = $db->do_query( $q );

            if( is_array( $r ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}

function ticket_booking_update_data( $post, $data, $from_agent = false )
{
    global $db;

    $retval = 1;
    $db->begin();

    extract( $post );
    extract( $data );

    $update_booked    = false;
    $update_booked_by = false;
    $update_detail    = false;
    $update_transport = false;
    $update_passenger = false;
    $update_payment   = false;
    $fields_log       = array();

    $n = array(
        /* Named for booked */
        'bstatus' => 'Booking Status',
        'bremark' => 'Remark',
        'bagremark' => 'Agent Remark',
        'bcreason' => 'Cancel Reason',
        'bcancellationfee' => 'Cancellation fee',
        'btransactionfee' => 'Transaction fee',
        'brefund' => 'Refund',
        'bcmessage' => 'Cancellation Message',
        'bhotelname' => 'Accommodation - Hotel Name',
        'bhoteladdress' => 'Accommodation - Hotel Address',
        'bhotelphone' => 'Accommodation - Hotel Phone',
        'bhotelemail' => 'Accommodation - Hotel Email',
        /* Named for Booked By */
        'bbname' => 'Booked Name',
        'bbemail' => 'Booked Email',
        'bbphone' => 'Booked Phone',
        'bbcountry' => 'Booked Country',
        'bbrevtype' => 'Reservation Type',
        'bbooking_staf' => 'Booking Staf',
        'bbrevagent' => 'Reference (Agent Code)',
        /* Named for Booked Detail */
        'price_per_adult' => 'Price per Adult',
        'price_per_child' => 'Price per Child',
        'price_per_infant' => 'Price per Infant',
        'net_price_per_adult' => 'Net Price per Adult',
        'net_price_per_child' => 'Net Price per Child',
        'net_price_per_infant' => 'Net Price per Infant',
        'selling_price_per_adult' => 'Selling Price per Adult',
        'selling_price_per_child' => 'Selling Price per Child',
        'selling_price_per_infant' => 'Selling Price per Infant',
        'discount' => 'Discount',
        'disc_price_per_adult' => 'Adult Disc.',
        'disc_price_per_child' => 'Child Disc.',
        'disc_price_per_infant' => 'Infant Disc.',
        'subtotal' => 'Sub Total',
        'total' => 'Total Trip',
        'bdrevstatus' => 'Reservation Status',
        'bdpstatus' => 'Booking Status',
        /* Named for Transport */
        'gemail' => 'Guest Email',
        'gmobile' => 'Guest Mobile',
        /* Named for Transport */
        'bttrans_type' => 'Transport Type',
        'bttrans_fee' => 'Transport Fee',
        'btdrivername' => 'Driver Name',
        'btdriverphone' => 'Driver Phone',
        'bthotelroomnumber' => 'Hotel/Villa Room Number',
        'bthoteladdress' => 'Hotel/Villa Address',
        'bthotelphone' => 'Hotel/Villa Phone',
        'bthotelemail' => 'Hotel/Villa Email',
        'bthotelname' => 'Hotel Name',
        'bpname' => 'Passanger Name',
        'bpgender' => 'Passanger Gender',
        'lcountry_id' => 'Passanger Country' );

    if( isset( $booked ) && !empty( $booked ) )
    {
        foreach( $booked as $bid => $obj )
        {
            $fields = array();

            foreach( $obj as $field => $val )
            {
                if( $field == 'bblockingtime' )
                {
                    $fields[] = $db->prepare_query( 'bblockingtime_sts_48 = %s', '0' );
                    $fields[] = $db->prepare_query( 'bblockingtime_sts_24 = %s', '0' );
                    $fields[] = $db->prepare_query( 'bblockingtime = %s', ( $val == '' ? 0 : strtotime( $val ) ) );

                    /* CREATE LOG EDIT bblockingtime */
                    if( $val != '' )
                    {
                        if( isset( $data[ 'bblockingtime' ] ) && $obj[ 'bblockingtime' ] != $data[ 'bblockingtime' ] )
                        {
                            $fields_log[] = 'Blocking time : ' . $data[ $field ] . ' --> ' . strtotime( $val );
                        }
                    }
                }
                elseif( in_array( $field, array( 'brcdate', 'bcdate' ) ) && !empty( $val ) )
                {
                    $fields[] = $db->prepare_query( $field . ' = %s', date( 'Y-m-d', strtotime( $val ) ) );

                    /* CREATE LOG EDIT brcdate AND bcdate */
                    if( $val != '' )
                    {
                        if( isset( $data['brcdate'] ) && $field == 'brcdate' && date( 'Y-m-d', strtotime( $data['brcdate'] ) ) != date( 'Y-m-d', strtotime( $val ) ) )
                        {
                            $fields_log[] = 'Add Request Cancel Date : ' . $val;
                        }
                        elseif( isset( $data['bcdate'] ) && $field == 'bcdate' && date( 'Y-m-d', strtotime( $data['bcdate'] ) ) != date( 'Y-m-d', strtotime( $val ) ) )
                        {
                            $fields_log[] = 'Add Cancel Date : ' . $val;
                        }
                    }
                }
                else
                {
                    $fields[] = $db->prepare_query( $field . ' = %s', $val );

                    /* CREATE LOG EDIT field */
                    if( $field != 'bblockingtime' && $field != 'brcdate' && $field != 'bcdate' )
                    {
                        if( isset( $data[ $field ] ) && $data[ $field ] != $obj[ $field ] )
                        {
                            $fields_log[] = $n[ $field ] . ' : ' . $data[ $field ] . ' --> ' . $val;
                        }
                    }
                }
            }

            if( !empty( $fields ) )
            {
                $q = 'UPDATE ticket_booking SET ' . implode( ',', $fields ) . ' WHERE bid = '. $bid;
                $r = $db->do_query( $q );

                if( is_array( $r ) )
                {
                    $retval = 0;
                }
            }
        }
    }

    if( isset( $booked_by ) && !empty( $booked_by ) )
    {
        foreach( $booked_by as $bid => $obj )
        {
            $fields = array();

            foreach( $obj as $field => $val )
            {
                $fields[] = $field . '="' . $val . '"';

                /* CREATE LOG EDIT BOOKED BY */
                if( isset( $data[$field] ) && $val != $data[$field] )
                {
                    $val_old = $field == 'bbcountry' ? get_country( $data[ $field ], 'lcountry' ) : $data[ $field ];
                    $val_new = $field == 'bbcountry' ? get_country( $val, 'lcountry' ) : $val;

                    $fields_log[] = $n[ $field ] . ' : ' . $val_old . ' --> ' . $val_new;
                }
            }

            if( !empty( $fields ) )
            {
                $q = 'UPDATE ticket_booking SET ' . implode( ',', $fields ) . ' WHERE bid = '. $bid;
                $r = $db->do_query( $q );

                if( is_array( $r ) )
                {
                    $retval = 0;
                }
            }
        }
    }

    if( isset( $passenger ) && !empty( $passenger ) )
    {
        foreach( $passenger as $bpid => $obj )
        {
            $fields = array();

            foreach( $obj as $field => $val )
            {
                $val_old = get_value_before_update( 'ticket_booking_passenger', $field, 'bpid', $bpid );

                if( $field == 'bpbirthdate' )
                {
                    if( $val == '' )
                    {
                        /* CREATE LOG EDIT PASSANGER DOB */
                        if ( $val != '' )
                        {
                            $fields_log[] = ' Add DOB : --> ' . $val;
                        }
                    }
                    else
                    {
                        $fields[] = $field . '="' . date( 'Y-m-d', strtotime( $val ) ) . '"';

                        /* CREATE LOG EDIT PASSANGER DOB */
                        if ( $val_old != date( 'Y-m-d', strtotime( $val ) ) )
                        {
                            $fields_log[] = ' Change DOB : ' . date( 'd F Y', strtotime( $val_old ) ) . ' --> ' . $val;
                        }
                    }
                }
                else
                {
                    $fields[] = $field . '="' . $val . '"';

                    /* CREATE LOG EDIT PASSANGER */
                    if ( $field == 'bpgender' )
                    {
                        $val_old = $val_old == '1' ? 'Male' : 'Female';
                        $val     = $val == '1' ? 'Male' : 'Female';
                    }

                    if ( $field == 'lcountry_id' )
                    {
                        $val_old = get_country( $val_old, 'lcountry' );
                        $val     = get_country( $val, 'lcountry' );
                    }

                    if ( $val_old != $val )
                    {
                        $bdid = get_value_before_update( 'ticket_booking_passenger', 'bdid', 'bpid', $bpid );
                        $from = get_value_before_update( 'ticket_booking_detail', 'bdfrom', 'bdid', $bdid );
                        $to   = get_value_before_update( 'ticket_booking_detail', 'bdto', 'bdid', $bdid );

                        $fields_log[] = $n[ $field ]. ' ( ' . $from . ' - ' . $to . ' ) : ' . $val_old . ' --> ' . $val;
                    }
                }
            }

            if( !empty( $fields ) )
            {
                $q = 'UPDATE ticket_booking_passenger SET ' . implode( ',', $fields ) . ' WHERE bpid = '. $bpid;
                $r = $db->do_query( $q );

                if( is_array( $r ) )
                {
                    $retval = 0;
                }
            }
        }
    }

    if( isset( $empty_transport ) && !empty( $empty_transport ) )
    {
        foreach( $empty_transport as $bdid => $empty_trans )
        {
            foreach( $empty_trans as $bttype => $obj )
            {
                $fields = array( 'bdid', 'bttype' );
                $values = array( $bdid, $bttype );

                //-- If No Accommodation
                if( !isset( $obj['taid'] ) )
                {
                    array_push( $fields, 'bthotelname', 'bthotelphone', 'bthotelemail', 'bthoteladdress', 'bthotelroomnumber' );
                    array_push( $values, '', '', '', '', '' );
                }

                //-- If Own Transport
                if( isset( $obj['bttrans_type'] ) && $obj['bttrans_type'] <> '2' )
                {
                    array_push( $fields, 'btdrivername', 'btdriverphone' );
                    array_push( $values, '', '' );
                }

                //-- If With Transport
                if( isset( $obj['bttrans_type'] ) && $obj['bttrans_type'] == '2' )
                {
                    array_push( $fields, 'btflighttime', 'btrpfrom', 'btrpto' );
                    array_push( $values, '', '', '' );
                }

                foreach( $obj as $field => $val )
                {
                    if( $val != '' )
                    {
                        if( $field == 'taid' )
                        {
                            list( $taid, $btrpfrom, $btrpto ) = explode( '|', $val );

                            if( isset( $taid ) && !empty( $taid ) )
                            {
                                array_push( $fields, 'taid' );
                                array_push( $values, $taid );
                            }

                            if( isset( $btrpfrom ) )
                            {
                                array_push( $fields, 'btrpfrom' );
                                array_push( $values, $btrpfrom );
                            }

                            if( isset( $btrpto ) )
                            {
                                array_push( $fields, 'btrpto' );
                                array_push( $values, $btrpto );
                            }
                        }
                        else
                        {
                            array_push( $fields, $field );
                            array_push( $values, $val );
                        }
                    }
                }

                if( !empty( $fields ) && !empty( $values ) )
                {
                    $q = 'INSERT INTO ticket_booking_transport(' . implode( ',', $fields ) . ') VALUES ("' . implode( '" , "', $values ) . '")';
                    $r = $db->do_query( $q );

                    if( is_array( $r ) )
                    {
                        $fields_log[] = 'Add Transport : <br/> Add' . ( isset( $n[ $fields ] ) ? $n[ $fields ] : $fields ) . ' : ' . $values;

                        $retval = 0;
                    }
                }
            }
        }
    }

    if( isset( $transport ) && !empty( $transport ) )
    {
        foreach( $transport as $bdid => $trans )
        {
            foreach( $trans as $btid => $obj )
            {
                $fields = array();

                //-- If No Accommodation
                if( !isset( $obj['taid'] ) )
                {
                    $fields[] = 'hid = NULL';
                    $fields[] = 'taid = NULL';
                    $fields[] = 'bthotelname = ""';
                    $fields[] = 'bthotelphone = ""';
                    $fields[] = 'bthotelemail = ""';
                    $fields[] = 'bthoteladdress = ""';
                    $fields[] = 'bthotelroomnumber = ""';
                }

                //-- If Own Transport
                if( isset( $obj['bttrans_type'] ) && $obj['bttrans_type'] <> '2' )
                {
                    $fields[] = 'btdrivername = ""';
                    $fields[] = 'btdriverphone = ""';
                }

                //-- If With Transport
                if( isset( $obj['bttrans_type'] ) && $obj['bttrans_type'] == '2' )
                {
                    $fields[] = 'btrpfrom = ""';
                    $fields[] = 'btrpto = ""';
                }

                foreach( $obj as $field => $val )
                {
                    $ttrip   = get_value_before_update( 'ticket_booking_detail', 'bdtype', 'bdid', $bdid );
                    $bttype  = get_value_before_update( 'ticket_booking_transport', 'bttype', 'btid', $btid );
                    $val_old = isset( $detail) ? $detail[$ttrip][$bdid]['transport'][$bttype][0][$field] : '';

                    if( $field == 'taid' )
                    {
                        list( $taid, $btrpfrom, $btrpto ) = explode( '|', $val );

                        if( isset( $taid ) )
                        {
                            if( $taid == 0 )
                            {
                                $fields[] = 'hid = NULL';
                                $fields[] = 'taid = NULL';
                            }
                            else
                            {
                                $fields[] = 'taid = "' . $taid . '"';
                            }

                            /* CREATE LOG EDIT TRANSPORT */
                            if( $taid != $val_old )
                            {
                                $fields_log[] =  'Transport Area : ' . ( $val_old == 0 ? 'Transport area' : get_transport_area( $val_old, 'taname' ) ) . ' --> ' . ( $taid == 0 ? 'Transport area' : get_transport_area( $taid, 'taname' ) );
                            }
                        }

                        if( isset( $btrpfrom ) )
                        {
                            $fields[] = 'btrpfrom = "' . $btrpfrom . '"';
                        }

                        if( isset( $btrpto ) )
                        {
                            $fields[] = 'btrpto = "' . $btrpto . '"';
                        }
                    }
                    elseif( $field == 'hid' )
                    {
                        if( $val == '' )
                        {
                            $fields[] = 'hid = NULL';
                        }
                        else
                        {
                            $fields[] = 'hid = "' . $val . '"';
                        }
                    }
                    elseif( $field == 'btflighttime' )
                    {
                        if( $val != '' )
                        {
                            $fields[] = 'btflighttime = "' . date( 'Y-m-d H:i:s', strtotime( $val ) ) . '"';
                        }
                    }
                    else
                    {
                        $fields[] = $field . ' = "' . $val . '"';

                        /* CREATE LOG EDIT TRANSPORT */
                        if( $field != 'hid' && $field != 'btflighttime' )
                        {
                            if( $val != $val_old )
                            {
                                $fields_log[] =  $n[$field] .' : ' . ( $field == 'bttrans_type' ? get_transport_type( $val_old ) : $val_old ) . ' --> ' . ( $field == 'bttrans_type' ? get_transport_type( $val ) : $val );
                            }
                        }
                    }
                }

                if( empty( $fields ) === false )
                {
                    $q = 'UPDATE ticket_booking_transport SET ' . implode( ',', $fields ) . ' WHERE btid = '. $btid;
                    $r = $db->do_query( $q );

                    if( is_array( $r ) )
                    {
                        $retval = 0;
                    }
                }
            }
        }
    }

    if( isset( $booked_detail ) )
    {
        $total = array();

        foreach( $booked_detail as $bdid => $obj )
        {
            $dsubtotal  = isset( $obj['subtotal'] ) ?  $obj['subtotal'] : 0;
            $ddiscount  = isset( $obj['discount'] ) ? $obj['discount'] : 0;
            $num_adult  = isset( $obj['num_adult'] ) ? $obj['num_adult'] : 0;
            $num_child  = isset( $obj['num_child'] ) ? $obj['num_child'] : 0;
            $num_infant = isset( $obj['num_infant'] ) ? $obj['num_infant'] : 0;
            $prc_adult  = isset( $obj['price_per_adult'] ) ? $obj['price_per_adult'] : 0;
            $prc_child  = isset( $obj['price_per_child'] ) ? $obj['price_per_child'] : 0;
            $prc_infant = isset( $obj['price_per_infant'] ) ? $obj['price_per_infant'] : 0;
            $disc_price_per_adult  = isset( $obj['disc_price_per_adult'] ) ? $obj['disc_price_per_adult'] : 0;
            $disc_price_per_child  = isset( $obj['disc_price_per_child'] ) ? $obj['disc_price_per_child'] : 0;
            $disc_price_per_infant = isset( $obj['disc_price_per_infant'] ) ? $obj['disc_price_per_infant'] : 0;
            $selling_price_per_adult  = isset( $obj['selling_price_per_adult'] ) ? $obj['selling_price_per_adult'] : 0;
            $selling_price_per_child  = isset( $obj['selling_price_per_child'] ) ? $obj['selling_price_per_child'] : 0;
            $selling_price_per_infant = isset( $obj['selling_price_per_infant'] ) ? $obj['selling_price_per_infant'] : 0;

            $fields = array();

            foreach( $obj as $field => $val )
            {
                $val_old    = get_value_before_update( 'ticket_booking_detail', $field, 'bdid', $bdid );

                if( $val != '' )
                {
                    if( $field == 'total' )
                    {
                        if( isset( $empty_transport[ $bdid ] ) )
                        {
                            $transfee = array_sum( array_column( $empty_transport[ $bdid ], 'bttrans_fee' ) );
                        }
                        elseif( isset( $transport[ $bdid ] ) )
                        {
                            $transfee = array_sum( array_column( $transport[ $bdid ], 'bttrans_fee' ) );
                        }
                        else
                        {
                            $transfee = 0;
                        }

                        $dtotal   = $transfee + ( $dsubtotal - $ddiscount );
                        $total[]  = $dtotal;

                        $fields[] = $db->prepare_query( 'transport_fee = %s', $transfee );
                        $fields[] = $db->prepare_query( 'total = %s', $dtotal );
                    }
                    else if( $field == 'bdflighttime' )
                    {
                        $fields[] = $db->prepare_query( $field . ' = %s', date( 'Y-m-d H:i:s', strtotime( $val ) ) );
                    }
                    else
                    {
                        $fields[] = $db->prepare_query( $field . ' = %s', $val );

                        if( $val != $val_old )
                        {
                            $from = get_value_before_update( 'ticket_booking_detail', 'bdfrom', 'bdid', $bdid );
                            $to   = get_value_before_update( 'ticket_booking_detail', 'bdto', 'bdid', $bdid );
                            $trip = get_value_before_update( 'ticket_booking_detail', 'bdtype', 'bdid', $bdid );

                            $fields_log[] = $n[ $field ]. ' ( '. ucwords( $trip ) . ' Trip : ' . $from . ' - ' . $to . ' ) : ' . ( $field == 'bdpstatus' ? ticket_booking_status( $val_old ) : $val_old ) . ' --> ' . ( $field == 'bdpstatus' ? ticket_booking_status( $val ) : $val );
                        }
                    }
                }
            }

            if( empty( $fields ) === false )
            {
                $q = 'UPDATE ticket_booking_detail SET ' . implode( ',', $fields ) . ' WHERE bdid = '. $bdid;
                $r = $db->do_query( $q );

                if( is_array( $r ) )
                {
                    $retval = 0;
                }
            }
        }

        $bsubtotal = empty($total) ? $subtotal : array_sum( $total );
        $btotal    = $bsubtotal - $discount;

        $s = 'UPDATE ticket_booking SET bsubtotal = %s, btotal = %s WHERE bid = %d';
        $q = $db->prepare_query( $s, $bsubtotal, $btotal, $bid );
        $r = $db->do_query( $q );

        if( is_array( $r ) === false )
        {
            if( isset( $agid ) && !empty( $agid ) )
            {
                $s2 = 'UPDATE ticket_agent_transaction SET agid = %d, atdebet = %s, atcredit = %s WHERE bcode = %s AND atstatus = %d';
                $q2 = $db->prepare_query( $s2, $agid, $btotal, 0, $bcode, 1 );
                $r2 = $db->do_query( $q2 );

                if( is_array( $r2 ) )
                {
                    $retval = 0;
                }
            }
        }
        else
        {
            $retval = 0;
        }
    }

    if( $retval == 0 )
    {
        $db->rollback();

        return false;
    }
    else
    {
        $db->commit();

        if( empty( $fields_log ) === false )
        {
            if( $from_agent === false )
            {
                save_log( $bid, 'reservation', 'Edit reservation #' . $bticket . '<br/>------- detail edit --------<br/>' . implode( '<br/>', $fields_log ) );

                $db->do_query( 'UPDATE ticket_booking SET bstact="1" WHERE bid = '. $bid );
            }
        }

        return true;
    }
}

function ticket_booking_send_feedback( $post )
{
    if( save_feedback( $post ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function get_booking_updated_notif_to_admin( $data, $from_agent = false )
{
    $time = time();

    set_template( PLUGINS_PATH . '/ticket/tpl/email/new-booking-update-notif-to-admin.html', 'admin-' . $time . '-email' );

    add_block( 'email-block', 'ae-' . $time . '-block', 'admin-' . $time . '-email' );

    add_variable( 'bbname', $data['bbname'] );
    add_variable( 'bbemail', $data['bbemail'] );
    add_variable( 'bbphone', $data['bbphone'] );

    add_variable( 'bticket', $data['bticket'] );
    add_variable( 'bdate', date( 'd F Y', strtotime( $data['bdate'] ) ) );
    add_variable( 'wording', 'You get a new update, with the following details :' );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'detail_link', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'] );

    parse_template( 'email-block', 'ae-' . $time . '-block' );

    return return_template( 'admin-' . $time . '-email' );
}

function get_booking_cancelation_request_to_admin( $data )
{
    $time = time();

    set_template( PLUGINS_PATH . '/ticket/tpl/email/new-cancelation-request-to-admin.html', 'admin-' . $time . '-email' );

    add_block( 'email-block', 'ae-' . $time . '-block', 'admin-' . $time . '-email' );

    add_variable( 'bbname', $data['bbname'] );
    add_variable( 'bbemail', $data['bbemail'] );
    add_variable( 'bbphone', $data['bbphone'] );

    add_variable( 'bticket', $data['bticket'] );
    add_variable( 'bcreason', $data['bcreason'] );
    add_variable( 'bcmessage', nl2br( $data['bcmessage'] ) );
    add_variable( 'bdate', date( 'd F Y', strtotime( $data['bdate'] ) ) );

    if( $data['bstatus'] == 'cr' )
    {
        add_variable( 'wording', 'You get a new cancelation request, with the following details :' );

        add_variable( 'brefund_css', 'display:none;' );
        add_variable( 'btransactionfee', 0 );
        add_variable( 'breceivedbycust', 0 );
        add_variable( 'bcancellationfee', 0 );
    }
    elseif( $data['bstatus'] == 'cn' )
    {
        add_variable( 'wording', 'You get a new cancelation, with the following details :' );

        add_variable( 'brefund_css', 'display:table-row;' );
        add_variable( 'breceivedbycust', number_format( ( $data['brefund'] ), 0, ',', '.' ) );
        add_variable( 'btransactionfee', number_format( $data['btransactionfee'], 0, ',', '.' ) );
        add_variable( 'bcancellationfee', number_format( $data['bcancellationfee'], 0, ',', '.' ) );
    }

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'detail_link', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'] );

    parse_template( 'email-block', 'ae-' . $time . '-block' );

    return return_template( 'admin-' . $time . '-email' );
}

function get_booking_cancelation_to_admin( $data, $ctype )
{
    $time = time();

    set_template( PLUGINS_PATH . '/ticket/tpl/email/new-cancelation-to-admin.html', 'admincn-' . $time . '-email' );

    add_block( 'email-cancel-block', 'ace-' . $time . '-block', 'admincn-' . $time . '-email' );

    add_variable( 'bbname', $data['bbname'] );
    add_variable( 'bbemail', $data['bbemail'] );
    add_variable( 'bbphone', $data['bbphone'] );

    add_variable( 'bticket', $data['bticket'] );
    add_variable( 'bcreason', $data['bcreason'] );
    add_variable( 'bcmessage', nl2br( $data['bcmessage'] ) );
    add_variable( 'bdate', date( 'd F Y', strtotime( $data['bdate'] ) ) );

    add_variable( 'wording', 'You get a new cancellation <b>'. ucwords( $ctype ) .'</b>, with the following details :' );

    add_variable( 'brefund_css', 'display:table-row;' );
    add_variable( 'breceivedbycust', number_format( $data['brefund'], 0, ',', '.' ) );
    add_variable( 'btransactionfee', number_format( $data['btransactionfee'], 0, ',', '.' ) );
    add_variable( 'bcancellationfee', number_format( $data['bcancellationfee'], 0, ',', '.' ) );
    add_variable( 'cancellationfeepercent', $data['bcancelationpercent'] == '' || $data['bcancelationpercent'] == 0 ? '' : '('.$data['bcancelationpercent'].'%)' );

    extract( $data['detail'] );

    $trip = '';

    if ( isset( $departure ) )
    {
        foreach ( $departure as $bdid => $d )
        {
            $status = $d['bdstatus'] == 'cn' ? 'Cancelled' : '';

            $trip .=
            '<tr>
                <td colspan="2" style="border-left:1px solid rgb(198, 216, 220);border-top:1px solid rgb(198, 216, 220);"><strong style="color:rgb(62, 117, 128);">Departure Trip</strong></td>
                <td align="right" style="border-right:1px solid rgb(198, 216, 220);border-top:1px solid rgb(198, 216, 220);"><strong style="color:red;">' . $status . '</strong></td>
            </tr>
            <tr>
                <td colspan="3" style="border:1px solid rgb(198, 216, 220);">
                    <table width="100%">
                        <tr>
                            <td><b>Departure</b></td>
                            <td><b>Arrival</b></td>
                            <td><b>Adult (IDR)</b></td>
                            <td><b>Child (IDR)</b></td>
                            <td><b>Infant (IDR)</b></td>
                            <td align="right"><b>Sub Total (IDR)</b></td>
                        </tr>
                        <tr>
                            <td>' . $d['bdfrom'] . '</td>
                            <td>' . $d['bdto'] . '</td>
                            <td>' . $d['num_adult'] . ' x ' . number_format( $d['price_per_adult'], 0, ',', '.' ) . '</td>
                            <td>' . $d['num_child'] . ' x ' . number_format( $d['price_per_child'], 0, ',', '.' ) . '</td>
                            <td>' . $d['num_infant'] . ' x ' . number_format( $d['price_per_infant'], 0, ',', '.' ) . '</td>
                            <td align="right">' . number_format( $d['total'], 0, ',', '.' ) . '</td>
                        </tr>
                        <tr>
                            <td>' . date( 'd F Y', strtotime( $d['bddate'] ) ) . ', ' . $d['bddeparttime'] . '</td>
                            <td>' . date( 'd F Y', strtotime( $d['bddate'] ) ) . ', ' . $d['bdarrivetime'] . '</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="right"></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-top: 10px;">
                                <table width="100%">
                                    <tr>
                                        <td style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Passanger Name</b></td>
                                        <td align="center" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Type</b></td>
                                        <td align="center" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Date of Birth</b></td>
                                        <td align="right" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Status</b></td>
                                    </tr>';

                                    foreach ( $d['passenger'] as $pass )
                                    {
                                        foreach ( $pass as $p )
                                        {
                                            $stpass = $p['bpstatus'] == 'cn' ? 'Cancelled' : 'Active';

                                            $trip .=
                                            '<tr>
                                                <td style="border-bottom:1px solid rgb(198, 216, 220);">' . $p['bpname'] . '</td>
                                                <td align="center" style="border-bottom:1px solid rgb(198, 216, 220);">' . ucwords( $p['bptype'] ) . '</td>
                                                <td align="center" style="border-bottom:1px solid rgb(198, 216, 220);">' . date( 'd F Y', strtotime( $p['bpbirthdate'] ) ) . '</td>
                                                <td align="right" style="border-bottom:1px solid rgb(198, 216, 220);">'. $stpass .'</td>
                                            </tr>';
                                        }
                                    }

                                $trip .=
                                '</table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>';
        }
    }

    if ( isset( $return ) )
    {
        foreach ( $return as $bdid => $r )
        {
            $status = $r['bdstatus'] == 'cn' ? 'Cancelled' : '';

            $trip .=
            '<tr>
                <td colspan="2" style="border-left:1px solid rgb(198, 216, 220);"><strong style="color:rgb(62, 117, 128);">Return Trip</strong></td>
                <td align="right" style="border-right:1px solid rgb(198, 216, 220);"><strong style="color:red;">' . $status . '</strong></td>
            </tr>
            <tr>
                <td colspan="3" style="border:1px solid rgb(198, 216, 220);">
                    <table width="100%">
                        <tr>
                            <td><b>Departure</b></td>
                            <td><b>Arrival</b></td>
                            <td><b>Adult (IDR)</b></td>
                            <td><b>Child (IDR)</b></td>
                            <td><b>Infant (IDR)</b></td>
                            <td align="right"><b>Sub Total (IDR)</b></td>
                        </tr>
                        <tr>
                            <td>' . $r['bdfrom'] . '</td>
                            <td>' . $r['bdto'] . '</td>
                            <td>' . $r['num_adult'] . ' x ' . number_format( $r['price_per_adult'], 0, ',', '.' ) . '</td>
                            <td>' . $r['num_child'] . ' x ' . number_format( $r['price_per_child'], 0, ',', '.' ) . '</td>
                            <td>' . $r['num_infant'] . ' x ' . number_format( $r['price_per_infant'], 0, ',', '.' ) . '</td>
                            <td align="right">' . number_format( $r['total'], 0, ',', '.' ) . '</td>
                        </tr>
                        <tr>
                            <td>' . date( 'd F Y', strtotime( $r['bddate'] ) ) . ', ' . $r['bddeparttime'] . '</td>
                            <td>' . date( 'd F Y', strtotime( $r['bddate'] ) ) . ', ' . $r['bdarrivetime'] . '</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="right"></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-top: 10px;">
                                <table width="100%">
                                    <tr>
                                        <td style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Passanger Name</b></td>
                                        <td align="center" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Type</b></td>
                                        <td align="center" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Date of Birth</b></td>
                                        <td align="right" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Status</b></td>
                                    </tr>';

                                    foreach ( $r['passenger'] as $pass )
                                    {
                                        foreach ( $pass as $p )
                                        {
                                            $stpass = $p['bpstatus'] == 'cn' ? 'Cancelled' : 'Active';

                                            $trip .=
                                            '<tr>
                                                <td style="border-bottom:1px solid rgb(198, 216, 220);">' . $p['bpname'] . '</td>
                                                <td align="center" style="border-bottom:1px solid rgb(198, 216, 220);">' . ucwords( $p['bptype'] ) . '</td>
                                                <td align="center" style="border-bottom:1px solid rgb(198, 216, 220);">' . date( 'd F Y', strtotime( $p['bpbirthdate'] ) ) . '</td>
                                                <td align="right" style="border-bottom:1px solid rgb(198, 216, 220);">'. $stpass .'</td>
                                            </tr>';
                                        }
                                    }

                                $trip .=
                                '</table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>';
        }
    }

    add_variable( 'trip', $trip );
    add_variable( 'btotal', number_format( $data['btotal'], 0, ',', '.' ) );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'detail_link', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'] );

    parse_template( 'email-cancel-block', 'ace-' . $time . '-block' );

    return return_template( 'admincn-' . $time . '-email' );
}

function get_booking_cancelation_notif_to_client( $data )
{
    $time = time();

    set_template( PLUGINS_PATH . '/ticket/tpl/email/new-cancelation-notif-to-client.html', 'client-' . $time . '-email' );

    add_block( 'email-block', 'cl-' . $time . '-block', 'client-' . $time . '-email' );

    add_variable( 'bbname', $data['bbname'] );
    add_variable( 'bbemail', $data['bbemail'] );
    add_variable( 'bbphone', $data['bbphone'] );

    add_variable( 'bticket', $data['bticket'] );
    add_variable( 'bcreason', $data['bcreason'] );
    add_variable( 'bcmessage', nl2br( $data['bcmessage'] ) );
    add_variable( 'bdate', date( 'd F Y', strtotime( $data['bdate'] ) ) );

    if( $data['bstatus'] == 'cr' )
    {
        add_variable( 'wording', 'You get a new cancelation request, with the following details :' );

        add_variable( 'brefund_css', 'display:none;' );
        add_variable( 'btransactionfee', 0 );
        add_variable( 'breceivedbycust', 0 );
        add_variable( 'bcancellationfee', 0 );
    }
    elseif( $data['bstatus'] == 'cn' )
    {
        add_variable( 'wording', 'Your booking has been cancelled :' );

        add_variable( 'brefund_css', 'display:table-row;' );
        add_variable( 'breceivedbycust', number_format( ( $data['brefund'] ), 0, ',', '.' ) );
        add_variable( 'btransactionfee', number_format( $data['btransactionfee'], 0, ',', '.' ) );
        add_variable( 'bcancellationfee', number_format( $data['bcancellationfee'], 0, ',', '.' ) );
    }

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'detail_link', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'] );

    parse_template( 'email-block', 'cl-' . $time . '-block' );

    return return_template( 'client-' . $time . '-email' );
}

function get_cancelation_notif_to_client( $data, $ctype )
{
    $time = time();

    set_template( PLUGINS_PATH . '/ticket/tpl/email/new-cancelation-booking-notif-to-client.html', 'clientcn-' . $time . '-email' );

    add_block( 'email-block-client-cancelation', 'clcn-' . $time . '-block', 'clientcn-' . $time . '-email' );

    add_variable( 'bbname', $data['bbname'] );
    add_variable( 'bbemail', $data['bbemail'] );
    add_variable( 'bbphone', $data['bbphone'] );

    add_variable( 'bticket', $data['bticket'] );
    add_variable( 'bcreason', $data['bcreason'] );
    add_variable( 'bcmessage', nl2br( $data['bcmessage'] ) );
    add_variable( 'bdate', date( 'd F Y', strtotime( $data['bdate'] ) ) );

    add_variable( 'wording', 'Your booking has been cancelled, <b>' . ucwords( $ctype ) . '</b> :' );

    add_variable( 'brefund_css', 'display:table-row;' );
    add_variable( 'breceivedbycust', number_format( ( $data['brefund'] ), 0, ',', '.' ) );
    add_variable( 'btransactionfee', number_format( $data['btransactionfee'], 0, ',', '.' ) );
    add_variable( 'bcancellationfee', number_format( $data['bcancellationfee'], 0, ',', '.' ) );
    add_variable( 'cancellationfeepercent', $data['bcancelationpercent'] == '' || $data['bcancelationpercent'] == 0 ? '' : '('.$data['bcancelationpercent'].'%)' );

    extract( $data['detail'] );

    $trip = '';

    if ( isset( $departure ) )
    {
        foreach ( $departure as $bdid => $d )
        {
            $status = $d['bdstatus'] == 'cn' ? 'Cancelled' : '';

            $trip .=
            '<tr>
                <td colspan="2" style="border-left:1px solid rgb(198, 216, 220);border-top:1px solid rgb(198, 216, 220);"><strong style="color:rgb(62, 117, 128);">Departure Trip</strong></td>
                <td align="right" style="border-right:1px solid rgb(198, 216, 220);border-top:1px solid rgb(198, 216, 220);"><strong style="color:red;">' . $status . '</strong></td>
            </tr>
            <tr>
                <td colspan="3" style="border:1px solid rgb(198, 216, 220);">
                    <table width="100%">
                        <tr>
                            <td><b>Departure</b></td>
                            <td><b>Arrival</b></td>
                            <td><b>Adult (IDR)</b></td>
                            <td><b>Child (IDR)</b></td>
                            <td><b>Infant (IDR)</b></td>
                            <td align="right"><b>Sub Total (IDR)</b></td>
                        </tr>
                        <tr>
                            <td>' . $d['bdfrom'] . '</td>
                            <td>' . $d['bdto'] . '</td>
                            <td>' . $d['num_adult'] . ' x ' . number_format( $d['price_per_adult'], 0, ',', '.' ) . '</td>
                            <td>' . $d['num_child'] . ' x ' . number_format( $d['price_per_child'], 0, ',', '.' ) . '</td>
                            <td>' . $d['num_infant'] . ' x ' . number_format( $d['price_per_infant'], 0, ',', '.' ) . '</td>
                            <td align="right">' . number_format( $d['total'], 0, ',', '.' ) . '</td>
                        </tr>
                        <tr>
                            <td>' . date( 'd F Y', strtotime( $d['bddate'] ) ) . ', ' . $d['bddeparttime'] . '</td>
                            <td>' . date( 'd F Y', strtotime( $d['bddate'] ) ) . ', ' . $d['bdarrivetime'] . '</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="right"></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-top: 10px;">
                                <table width="100%">
                                    <tr>
                                        <td style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Passanger Name</b></td>
                                        <td align="center" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Type</b></td>
                                        <td align="center" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Date of Birth</b></td>
                                        <td align="right" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Status</b></td>
                                    </tr>';

                                    foreach ( $d['passenger'] as $pass )
                                    {
                                        foreach ( $pass as $p )
                                        {
                                            $stpass = $p['bpstatus'] == 'cn' ? 'Cancelled' : 'Active';

                                            $trip .=
                                            '<tr>
                                                <td style="border-bottom:1px solid rgb(198, 216, 220);">' . $p['bpname'] . '</td>
                                                <td align="center" style="border-bottom:1px solid rgb(198, 216, 220);">' . ucwords( $p['bptype'] ) . '</td>
                                                <td align="center" style="border-bottom:1px solid rgb(198, 216, 220);">' . date( 'd F Y', strtotime( $p['bpbirthdate'] ) ) . '</td>
                                                <td align="right" style="border-bottom:1px solid rgb(198, 216, 220);">'. $stpass .'</td>
                                            </tr>';
                                        }
                                    }

                                $trip .=
                                '</table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>';
        }
    }

    if ( isset( $return ) )
    {
        foreach ( $return as $bdid => $r )
        {
            $status = $r['bdstatus'] == 'cn' ? 'Cancelled' : '';

            $trip .=
            '<tr>
                <td colspan="2" style="border-left:1px solid rgb(198, 216, 220);"><strong style="color:rgb(62, 117, 128);">Return Trip</strong></td>
                <td align="right" style="border-right:1px solid rgb(198, 216, 220);"><strong style="color:red;">' . $status . '</strong></td>
            </tr>
            <tr>
                <td colspan="3" style="border:1px solid rgb(198, 216, 220);">
                    <table width="100%">
                        <tr>
                            <td><b>Departure</b></td>
                            <td><b>Arrival</b></td>
                            <td><b>Adult (IDR)</b></td>
                            <td><b>Child (IDR)</b></td>
                            <td><b>Infant (IDR)</b></td>
                            <td align="right"><b>Sub Total (IDR)</b></td>
                        </tr>
                        <tr>
                            <td>' . $r['bdfrom'] . '</td>
                            <td>' . $r['bdto'] . '</td>
                            <td>' . $r['num_adult'] . ' x ' . number_format( $r['price_per_adult'], 0, ',', '.' ) . '</td>
                            <td>' . $r['num_child'] . ' x ' . number_format( $r['price_per_child'], 0, ',', '.' ) . '</td>
                            <td>' . $r['num_infant'] . ' x ' . number_format( $r['price_per_infant'], 0, ',', '.' ) . '</td>
                            <td align="right">' . number_format( $r['total'], 0, ',', '.' ) . '</td>
                        </tr>
                        <tr>
                            <td>' . date( 'd F Y', strtotime( $r['bddate'] ) ) . ', ' . $r['bddeparttime'] . '</td>
                            <td>' . date( 'd F Y', strtotime( $r['bddate'] ) ) . ', ' . $r['bdarrivetime'] . '</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="right"></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-top: 10px;">
                                <table width="100%">
                                    <tr>
                                        <td style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Passanger Name</b></td>
                                        <td align="center" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Type</b></td>
                                        <td align="center" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Date of Birth</b></td>
                                        <td align="right" style="border-bottom:3px solid rgb(198, 216, 220);border-top:3px solid rgb(198, 216, 220);"><b>Status</b></td>
                                    </tr>';

                                    foreach ( $r['passenger'] as $pass )
                                    {
                                        foreach ( $pass as $p )
                                        {
                                            $stpass = $p['bpstatus'] == 'cn' ? 'Cancelled' : 'Active';

                                            $trip .=
                                            '<tr>
                                                <td style="border-bottom:1px solid rgb(198, 216, 220);">' . $p['bpname'] . '</td>
                                                <td align="center" style="border-bottom:1px solid rgb(198, 216, 220);">' . ucwords( $p['bptype'] ) . '</td>
                                                <td align="center" style="border-bottom:1px solid rgb(198, 216, 220);">' . date( 'd F Y', strtotime( $p['bpbirthdate'] ) ) . '</td>
                                                <td align="right" style="border-bottom:1px solid rgb(198, 216, 220);">'. $stpass .'</td>
                                            </tr>';
                                        }
                                    }

                                $trip .=
                                '</table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>';
        }
    }

    add_variable( 'trip', $trip );
    add_variable( 'btotal', number_format( $data['btotal'], 0, ',', '.' ) );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'detail_link', HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'] );

    parse_template( 'email-block-client-cancelation', 'clcn-' . $time . '-block' );

    return return_template( 'clientcn-' . $time . '-email' );
}

function get_booking_blocking_time_notif_to_admin( $data, $type, $link )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/email/notif-booking-blocking-time-to-admin.html', 'admin-email-' . $data['bid'] );

    add_block( 'email-block', 'ae-' . $data['bid'] . '-block', 'admin-email-' . $data['bid'] );

    add_variable( 'bticket', $data['bticket'] );
    add_variable( 'btime', $type == 2 ? '48' : '24' );

    add_variable( 'detail_link', $link );
    add_variable( 'img_url', get_theme_img() );

    parse_template( 'email-block', 'ae-' . $data['bid'] . '-block' );

    return return_template( 'admin-email-' . $data['bid'] );
}

function ticket_check_is_booking_already_edited( $bdata )
{
    global $db;

    $s = 'SELECT COUNT( a.lapp_id ) AS num FROM l_log AS a WHERE a.lapp_id = %d AND a.lapp_type = %s AND a.ldescription LIKE %s';
    $q = $db->prepare_query( $s, $bdata['bid'], 'reservation', '%Edit reservation #' . $bdata['bticket'] . '%' );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        $d = $db->fetch_array( $r );

        if( $d['num'] > 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

function ticket_item_list_edit_content( $agent, $bdate, $data = array(), $bdata = array(), $from_agent = false )
{
    extract( $data );

    $readonly = $from_agent ? 'readonly' : '';
    $disable  = $bdstatus == 'cn' ? 'disabled' : 'readonly';

    $net_price_adlt = 0;
    $net_price_chld = 0;
    $net_price_infn = 0;

    $booking_already_edited = ticket_check_is_booking_already_edited( $bdata );

    if( $net_price_per_adult != 0 || $net_price_per_child != 0 || $net_price_per_infant != 0 )
    {
        if( $booking_already_edited )
        {
            $net_price_adlt = $net_price_per_adult;
            $net_price_chld = $net_price_per_child;
            $net_price_infn = $net_price_per_infant;
        }
        else
        {
            $net_price_adlt  = ( $price_per_adult != $net_price_per_adult ) ? $net_price_per_adult : ( $net_price_per_adult - $disc_price_per_adult );
            $net_price_chld  = ( $price_per_child != $net_price_per_child ) ? $net_price_per_child : ( $net_price_per_child - $disc_price_per_child );
            $net_price_infn  = ( $price_per_infant != $net_price_per_infant ) ? $net_price_per_infant : ( $net_price_per_infant - $disc_price_per_infant );
        }
    }
    else
    {
        $net_price_adlt  = $price_per_adult - $disc_price_per_adult;
        $net_price_chld  = $price_per_child - $disc_price_per_child;
        $net_price_infn  = $price_per_infant - $disc_price_per_infant;

        if( !empty( $agent ) )
        {
            //-- Check Agent Rate
            $agdata = get_agent( $agent );

            if( $agdata['agtype'] == 'Net Rate' )
            {
                $rate = get_agent_net_rate( $agent, $bdfrom_id, $bdto_id );

                if ( !empty( $rate ) )
                {
                    if ( !empty( $rate[0]['agdatevalidity'] ) )
                    {
                        foreach ( $rate[0]['agnetprice'] as $date => $dt )
                        {
                            $darray = explode( '|' , $date );

                            foreach ( $dt as $lc )
                            {
                                if ( check_in_range_2( date( 'd-m-Y', strtotime( $darray[0] ) ), date( 'd-m-Y', strtotime( $darray[1] ) ), date( 'd-m-Y', strtotime( $bdate) ) ) )
                                {
                                    if( empty( $bdata['pmcode'] ) )
                                    {
                                        if( $price_per_adult == 0 && $net_price_per_adult == 0 )
                                        {
                                            $net_price_adlt = $net_price_per_adult - $disc_price_per_adult;
                                        }
                                        else
                                        {
                                            if( $net_price_per_adult != 0 )
                                            {
                                                $net_price_adlt = $net_price_per_adult - $disc_price_per_adult;
                                            }
                                            else
                                            {
                                                $net_price_adlt = $lc['adult_rate_inc_trans'] - $disc_price_per_adult;
                                            }
                                        }

                                        if( $price_per_child == 0 && $net_price_per_child == 0 )
                                        {
                                            $net_price_chld = $net_price_per_child - $disc_price_per_child;
                                        }
                                        else
                                        {
                                            if( $net_price_per_child != 0 )
                                            {
                                                $net_price_chld = $net_price_per_child - $disc_price_per_child;
                                            }
                                            else
                                            {
                                                $net_price_chld = $lc['child_rate_inc_trans'] - $disc_price_per_child;
                                            }
                                        }

                                        if( $price_per_infant == 0 && $net_price_per_infant == 0 )
                                        {
                                            $net_price_infn = $net_price_per_infant - $disc_price_per_infant;
                                        }
                                        else
                                        {
                                            if( $net_price_per_infant != 0 )
                                            {
                                                $net_price_infn = $net_price_per_infant - $disc_price_per_infant;
                                            }
                                            else
                                            {
                                                $net_price_infn = $lc['infant_rate_inc_trans'] - $disc_price_per_infant;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if( $booking_already_edited === false )
    {
        //-- Check discount per trip
        //-- 0 = %
        //-- 1 = Rp.
        if( $bdata['bdiscounttype'] == '0' )
        {
            $d_disc_adult = ( $net_price_adlt * $bdata['bdiscountnum'] ) / 100;
            $d_disc_child = ( $net_price_chld * $bdata['bdiscountnum'] ) / 100;
            $d_disc_infnt = ( $net_price_infn * $bdata['bdiscountnum'] ) / 100;
        }
        else
        {
            $d_disc_adult = $bdata['bdiscountnum'];
            $d_disc_child = $bdata['bdiscountnum'];
            $d_disc_infnt = $bdata['bdiscountnum'];
        }

        //-- Check again commission condition
        //-- If use discount => total - bdiscount
        if( $bdata['bcommissioncondition'] == '0' )
        {
            $d_price_adult = $net_price_adlt - $d_disc_adult;
            $d_price_child = $net_price_chld - $d_disc_child;
            $d_price_infnt = $net_price_infn - $d_disc_infnt;
        }
        else
        {
            $d_price_adult = $net_price_adlt;
            $d_price_child = $net_price_chld;
            $d_price_infnt = $net_price_infn;
        }

        //-- Check Commission Type
        //-- 0 = %
        //-- 1 = Rp.
        if( $bdata['bcommissiontype'] == '0' )
        {
            $d_commission_adult = $d_price_adult * ( $bdata['bcommission'] / 100 );
            $d_commission_child = $d_price_child * ( $bdata['bcommission'] / 100 );
            $d_commission_infnt = $d_price_infnt * ( $bdata['bcommission'] / 100 );
        }
        else
        {
            $d_commission_adult = $d_price_adult * ( $bdata['bcommission'] / $d_price_adult );
            $d_commission_child = $d_price_child * ( $bdata['bcommission'] / $d_price_child );
            $d_commission_infnt = $d_price_infnt * ( $bdata['bcommission'] / $d_price_infnt );
        }

        $net_price_adlt = $net_price_adlt - $d_disc_adult - $d_commission_adult;
        $net_price_chld = $net_price_chld - $d_disc_child - $d_commission_child;
        $net_price_infn = $net_price_infn - $d_disc_infnt - $d_commission_infnt;
    }

    $content  = '
    <div class="item-list">
        <div class="row">
            <div class="col-md-2">
                <span class="small-title">Departure</span><br/>
                <strong class="strong-text">' . $bdfrom . '</strong>
                <p class="regular-text">
                    ' . date( 'd M Y', strtotime( $bddate ) ) . ', ' . $bddeparttime . '
                </p>
            </div>
            <div class="col-md-2">
                <span class="small-title">Arrival</span><br/>
                <strong class="strong-text">' . $bdto . '</strong>
                <p class="regular-text">' . date( 'd M Y', strtotime( $bddate ) ) . ', ' . $bdarrivetime . '</p>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3 text-right">
                        <span class="small-title">Adult (IDR)</span>';
                        if( $num_adult == 0 )
                        {
                            $content .= '<p class="regular-text">-</p>';
                        }
                        else
                        {
                            $content .= '
                            <div class="input-group adult-group">
                                <span class="input-group-addon">' . $num_adult . ' x</span>
                                <input type="hidden" class="text form-control numpax" value="' . $num_adult . '" name="booked_detail[' . $bdid . '][num_adult]" ' . $readonly . ' autocomplete="off">
                                <input type="text" data-typecheck="'. $bdid .'_adult" id="'. $bdid .'_adult-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control numprice" value="' . $price_per_adult . '" name="booked_detail[' . $bdid . '][price_per_adult]" ' . $readonly . ' autocomplete="off">
                            </div>';

                            $content .= '
                            <div class="input-group adult-group">
                                <span class="input-group-addon">Disc.</span>
                                <input type="hidden" class="text form-control numpax" readonly autocomplete="off">
                                <input type="text" data-typecheck="'. $bdid .'_adult" id="'. $bdid .'_adult-disc" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-disc text-right form-control numdisc" value="' . $disc_price_per_adult . '" name="booked_detail[' . $bdid . '][disc_price_per_adult]" autocomplete="off">
                            </div>';

                            $content .= '
                            <div class="input-group adult-group">
                                <span class="input-group-addon">' . $num_adult . ' x</span>
                                <input type="hidden" class="text form-control numpax" value="' . $num_adult . '" readonly autocomplete="off">
                                <input type="text" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-disc-pax text-right form-control numdiscpax" value="' . ($price_per_adult - $disc_price_per_adult) . '" readonly>
                            </div>';
                        }

                        $content .= '
                    </div>
                    <div class="col-md-3 text-right">
                        <span class="small-title">Child (IDR)</span>';
                        if( $num_child == 0 )
                        {
                            $content .= '<p class="regular-text">-</p>';
                        }
                        else
                        {
                            $content .= '
                            <div class="input-group child-group">
                                <span class="input-group-addon">' . $num_child . ' x</span>
                                <input type="hidden" class="text form-control numpax" value="' . $num_child . '" name="booked_detail[' . $bdid . '][num_child]" ' . $readonly . ' autocomplete="off">
                                <input type="text" data-typecheck="'. $bdid .'_child" id="'. $bdid .'_child-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control numprice" value="' . $price_per_child . '" name="booked_detail[' . $bdid . '][price_per_child]" ' . $readonly . ' autocomplete="off">
                            </div>';

                            $content .= '
                            <div class="input-group child-group">
                                <span class="input-group-addon">Disc.</span>
                                <input type="hidden" class="text form-control numpax" readonly>
                                <input type="text" data-typecheck="'. $bdid .'_child" id="'. $bdid .'_child-disc" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-disc text-right form-control numdisc" value="' . $disc_price_per_child . '" name="booked_detail[' . $bdid . '][disc_price_per_child]" autocomplete="off">
                            </div>';

                            $content .= '
                            <div class="input-group child-group">
                                <span class="input-group-addon">' . $num_child . ' x</span>
                                <input type="hidden" class="text form-control numpax" value="' . $num_child . '" readonly autocomplete="off">
                                <input type="text" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-disc-pax text-right form-control numdiscpax" value="' . ( $price_per_child - $disc_price_per_child ) . '" readonly>
                            </div>';
                        }

                        $content .= '
                    </div>
                    <div class="col-md-3 text-right">
                        <span class="small-title">Infant (IDR)</span>';
                        if( $num_infant == 0 )
                        {
                            $content .= '<p class="regular-text">-</p>';
                        }
                        else
                        {
                            $content .= '
                            <div class="input-group infant-group">
                                <span class="input-group-addon">' . $num_infant . ' x</span>
                                <input type="hidden" class="text form-control numpax" value="' . $num_infant . '" name="booked_detail[' . $bdid . '][num_infant]" ' . $readonly . ' autocomplete="off">
                                <input type="text" data-typecheck="'. $bdid .'_infant" id="'. $bdid .'_infant-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control numprice" value="' . $price_per_infant . '" name="booked_detail[' . $bdid . '][price_per_infant]" ' . $readonly . ' autocomplete="off">
                            </div>';

                            $content .= '
                            <div class="input-group infant-group">
                                <span class="input-group-addon">Disc.</span>
                                <input type="hidden" class="text form-control numpax" value="' . $disc_price_per_infant . '" readonly autocomplete="off">
                                <input type="text" data-typecheck="'. $bdid .'_infant" id="'. $bdid .'_infant-disc" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-disc text-right form-control numdisc" value="' . $disc_price_per_infant . '" name="booked_detail[' . $bdid . '][disc_price_per_infant]" autocomplete="off">
                            </div>';

                            $content .= '
                            <div class="input-group infant-group">
                                <span class="input-group-addon">' . $num_infant . ' x</span>
                                <input type="hidden" class="text form-control numpax" value="' . $num_infant . '" readonly autocomplete="off">
                                <input type="text" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-disc-pax text-right form-control numdiscpax" value="' . ( $price_per_infant - $disc_price_per_infant ) . '" readonly>
                            </div>';
                        }

                        $content .= '
                    </div>
                    <div class="col-md-3 text-right">
                        <span class="small-title">Sub Total Discount (IDR)</span>
                        <div class="input-group discount-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="text" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control disc" value="' . $discount . '" name="booked_detail[' . $bdid . '][discount]" readonly autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-right">
                <span class="small-title">Sub Total (IDR)</span>
                <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="hidden" class="text form-control d-subtotal" data-status="'. $bdstatus .'" value="' . $subtotal . '" name="booked_detail[' . $bdid . '][subtotal]" ' . $readonly . ' autocomplete="off">
                    <input type="text" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control d-total" value="' . ( $total - $transport_fee ) . '" name="booked_detail[' . $bdid . '][total]" autocomplete="off" '. $disable .'>
                </div>
            </div>
        </div>
        <div class="row" style="border-top: 1px solid rgb(230, 238, 239);padding:11px 0 20px 0;margin: 15px 2px 0 2px;border-bottom:1px solid rgb(230, 238, 239);">
            <div class="col-md-4"></div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3 text-right">
                        <span class="small-title">Adult (Net Rate - IDR)</span>';
                        if( $num_adult == 0 )
                        {
                            $content .= '<p class="regular-text">-</p>';
                        }
                        else
                        {
                            $content .= '
                            <div class="input-group adult-group">
                                <span class="input-group-addon">' . $num_adult . ' x</span>
                                <input type="hidden" class="text form-control numpax" value="' . $num_adult . '" name="booked_detail[' . $bdid . '][num_adult]" ' . $readonly . ' autocomplete="off">
                                <input type="text" data-typecheck="'. $bdid .'_adult" id="'. $bdid .'_adult-net" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control netprice" value="' . $net_price_adlt . '" name="booked_detail[' . $bdid . '][net_price_per_adult]" ' . $readonly . ' autocomplete="off">
                            </div>';
                        }

                        $content .= '
                    </div>
                    <div class="col-md-3 text-right">
                        <span class="small-title">Child (Net Rate - IDR)</span>';
                        if( $num_child == 0 )
                        {
                            $content .= '<p class="regular-text">-</p>';
                        }
                        else
                        {
                            $content .= '
                            <div class="input-group child-group">
                                <span class="input-group-addon">' . $num_child . ' x</span>
                                <input type="hidden" class="text form-control numpax" value="' . $num_child . '" name="booked_detail[' . $bdid . '][num_child]" ' . $readonly . ' autocomplete="off">
                                <input type="text" data-typecheck="'. $bdid .'_child" id="'. $bdid .'_child-net" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control netprice" value="' . $net_price_chld . '" name="booked_detail[' . $bdid . '][net_price_per_child]" ' . $readonly . ' autocomplete="off">
                            </div>';
                        }

                        $content .= '
                    </div>
                    <div class="col-md-3 text-right">
                        <span class="small-title">Infant (Net Rate - IDR)</span>';
                        if( $num_infant == 0 )
                        {
                            $content .= '<p class="regular-text">-</p>';
                        }
                        else
                        {
                            $content .= '
                            <div class="input-group infant-group">
                                <span class="input-group-addon">' . $num_infant . ' x</span>
                                <input type="hidden" class="text form-control numpax" value="' . $num_infant . '" name="booked_detail[' . $bdid . '][num_infant]" ' . $readonly . ' autocomplete="off">
                                <input type="text" data-typecheck="'. $bdid .'_infant" id="'. $bdid .'_infant-net" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control netprice" value="' . $net_price_infn . '" name="booked_detail[' . $bdid . '][net_price_per_infant]" ' . $readonly . ' autocomplete="off">
                            </div>';
                        }

                        $content .= '
                    </div>
                    <div class="col-md-3 text-right">
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-right"></div>
        </div>
    </div>';

    return $content;
}

function get_transport_edit_field( $data = array(), $from_agent = false )
{
    global $db;

    if( !empty( $data ) )
    {
        extract( $data );

        $count     = ceil( ( $num_adult + $num_child + $num_infant ) / 4 );
        $disabled  = $bdtranstype == '0' ? 'disabled' : '';
        $content   = '';

        if( empty( $transport ) )
        {
            $dtrans = get_pickup_drop_list_data( $rid, $bdfrom_id );
            $atrans = get_pickup_drop_list_data( $rid, $bdto_id );

            if( isset( $dtrans['pickup'] ) )
            {
                if( empty( $transport_type ) )
                {
                    $transdata = get_pickup_drop_list_data( $rid, $bdfrom_id, 'pickup' );
                    $content  .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="item-list">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="small-title">Pickup - ' . $bdfrom .'</span>
                                            <select class="select-modal-edit form-control transport-type" name="empty_transport[' . $bdid . '][pickup][bttrans_type]">
                                                <option value="0">Shared Transport</option>
                                                <option value="1">Private Transport</option>
                                                <option value="2">Own Transport</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="hidden-block">
                                            <div class="form-group">
                                                <span class="small-title">Area</span>
                                                <select class="select-modal-edit form-control hotels-area" name="empty_transport[' . $bdid . '][pickup][taid]">';
                                                    if( $from_agent )
                                                    {
                                                        $content .= get_transport_area_option_by_trip( null, $transdata, $count );
                                                    }
                                                    else
                                                    {
                                                        $content .= get_transport_area_option_by_all( null, $transdata, $count );
                                                    }
                                                    $content .= '
                                                </select>
                                            </div>
                                            <div class="form-group flight-time sr-only">
                                                <span class="small-title">Flight Landing Time</span>
                                                <input type="text" value="" name="empty_transport[' . $bdid . '][pickup][btflighttime]" readonly />
                                            </div>
                                        </div>
                                        <div class="hidden-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Name</span>
                                                <select class="select-modal-edit form-control hotels" name="empty_transport[' . $bdid . '][pickup][hid]" disabled>';
                                                    if( $from_agent )
                                                    {
                                                        $content .= get_transport_hotel_option_by_trip( null, $transdata, $count );
                                                    }
                                                    else
                                                    {
                                                        $content .= get_transport_hotel_option_by_all( null, $transdata, $count );
                                                    }
                                                    $content .= '
                                                </select>
                                            </div>
                                        </div>
                                        <div class="hotel-block hotel-name-block">
                                            <div class="form-group">
                                                <input class="text text-full form-control h-name" type="text" name="empty_transport[' . $bdid . '][pickup][bthotelname]" disabled />
                                            </div>
                                        </div>
                                        <div class="hotel-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Room Number</span>
                                                <input class="text text-full form-control h-room" type="text" name="empty_transport[' . $bdid . '][pickup][bthotelroomnumber]" disabled />
                                            </div>
                                        </div>
                                        <div class="hotel-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Address</span>
                                                <input class="text text-full form-control h-address" type="text" name="empty_transport[' . $bdid . '][pickup][bthoteladdress]" disabled />
                                            </div>
                                        </div>
                                        <div class="hotel-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Phone</span>
                                                <input class="text text-full form-control h-phone" type="text" name="empty_transport[' . $bdid . '][pickup][bthotelphone]" disabled />
                                            </div>
                                        </div>
                                        <div class="hotel-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Email</span>
                                                <input class="text text-full form-control h-email" type="email" name="empty_transport[' . $bdid . '][pickup][bthotelemail]" disabled />
                                            </div>
                                        </div>
                                        <div class="driver-block sr-only">
                                            <div class="form-group">
                                                <span class="small-title">Driver Name</span>
                                                <input class="text text-full form-control" type="text" value="" name="empty_transport[' . $bdid . '][pickup][btdrivername]" disabled />
                                            </div>
                                        </div>
                                        <div class="driver-block sr-only">
                                            <div class="form-group">
                                                <span class="small-title">Driver Phone</span>
                                                <input class="text text-full form-control" type="text" value="" name="empty_transport[' . $bdid . '][pickup][btdriverphone]" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <div class="form-group">
                                            <span class="small-title">Transport Fee</span>
                                            <input type="text" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control h-transfee" name="empty_transport[' . $bdid . '][pickup][bttrans_fee]" value="0" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
                }
            }

            if( isset( $atrans['drop-off'] ) )
            {
                if( empty( $transport_type ) )
                {
                    $transdata = get_pickup_drop_list_data( $rid, $bdto_id, 'drop-off' );
                    $content  .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="item-list">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="small-title">Drop-off - ' . $bdto .'</span>
                                            <select class="select-modal-edit form-control transport-type" name="empty_transport[' . $bdid . '][drop-off][bttrans_type]">
                                                <option value="0">Shared Transport</option>
                                                <option value="1">Private Transport</option>
                                                <option value="2">Own Transport</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="hidden-block">
                                            <div class="form-group">
                                                <span class="small-title">Area</span>
                                                <select class="select-modal-edit form-control hotels-area" name="empty_transport[' . $bdid . '][drop-off][taid]" ' . $disabled . '>';
                                                    if( $from_agent )
                                                    {
                                                        $content .= get_transport_area_option_by_trip( null, $transdata, $count );
                                                    }
                                                    else
                                                    {
                                                        $content .= get_transport_area_option_by_all( null, $transdata, $count );
                                                    }
                                                    $content .= '
                                                </select>
                                            </div>
                                            <div class="form-group flight-time sr-only">
                                                <span class="small-title">Flight Landing Time</span>
                                                <input type="text" value="" name="empty_transport[' . $bdid . '][drop-off][btflighttime]" readonly />
                                            </div>
                                        </div>
                                        <div class="hidden-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Name</span>
                                                <select class="select-modal-edit form-control hotels" name="empty_transport[' . $bdid . '][drop-off][hid]" disabled>';
                                                    if( $from_agent )
                                                    {
                                                        $content .= get_transport_hotel_option_by_trip( null, $transdata, $count );
                                                    }
                                                    else
                                                    {
                                                        $content .= get_transport_hotel_option_by_all( null, $transdata, $count );
                                                    }
                                                    $content .= '
                                                </select>
                                            </div>
                                        </div>
                                        <div class="hotel-block hotel-name-block">
                                            <div class="form-group">
                                                <input class="text text-full form-control h-name" type="text" name="empty_transport[' . $bdid . '][drop-off][bthotelname]" disabled />
                                            </div>
                                        </div>
                                        <div class="hotel-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Room Number</span>
                                                <input class="text text-full form-control h-room" type="text" name="empty_transport[' . $bdid . '][drop-off][bthotelroomnumber]" disabled />
                                            </div>
                                        </div>
                                        <div class="hotel-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Address</span>
                                                <input class="text text-full form-control h-address" type="text" name="empty_transport[' . $bdid . '][drop-off][bthoteladdress]" disabled />
                                            </div>
                                        </div>
                                        <div class="hotel-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Phone</span>
                                                <input class="text text-full form-control h-phone" type="text" name="empty_transport[' . $bdid . '][drop-off][bthotelphone]" disabled />
                                            </div>
                                        </div>
                                        <div class="hotel-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Email</span>
                                                <input class="text text-full form-control h-email" type="email" name="empty_transport[' . $bdid . '][drop-off][bthotelemail]" disabled />
                                            </div>
                                        </div>
                                        <div class="driver-block sr-only">
                                            <div class="form-group">
                                                <span class="small-title">Driver Name</span>
                                                <input class="text text-full form-control" type="text" value="" name="empty_transport[' . $bdid . '][drop-off][btdrivername]" disabled />
                                            </div>
                                        </div>
                                        <div class="driver-block sr-only">
                                            <div class="form-group">
                                                <span class="small-title">Driver Phone</span>
                                                <input class="text text-full form-control" type="text" value="" name="empty_transport[' . $bdid . '][drop-off][btdriverphone]" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <div class="form-group">
                                            <span class="small-title">Transport Fee</span>
                                            <input type="text" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control h-transfee" name="empty_transport[' . $bdid . '][drop-off][bttrans_fee]" value="0" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
                }
            }
        }
        else
        {
            foreach( $transport as $ttype => $obj )
            {
                foreach( $obj as $d )
                {
                    if( $ttype == 'pickup' )
                    {
                        $location    = $bdfrom;
                        $port_id     = $bdfrom_id;
                        $flight_txt  = 'Flight Landing Time';
                    }
                    else
                    {
                        $location    = $bdto;
                        $port_id     = $bdto_id;
                        $flight_txt  = 'Flight Take Off Time';
                    }

                    $transdata = get_pickup_drop_list_data( $rid, $port_id, $ttype );

                    $drv_css   = $d['bttrans_type'] == '2' ? '' : 'sr-only';
                    $drv_req   = $d['bttrans_type'] == '2' ? 'required' : '';
                    $taid_css  = $d['bttrans_type'] == '2' ? 'disabled' : '';
                    $fee_css   = $d['bttrans_type'] == '1' ? '' : 'readonly';
                    $hid_css   = $d['bttrans_type'] == '2' ? 'disabled' : ( empty( $d['taid'] ) ? 'disabled' : '' );
                    $htl_css   = $d['bttrans_type'] == '2' ? 'sr-only' : '';
                    $content  .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="item-list">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="small-title">' . ucfirst( $ttype ) . ' - ' . $location .'</span>
                                            <select class="select-modal-edit form-control transport-type" name="transport[' . $d['bdid'] . '][' . $d['btid'] . '][bttrans_type]">
                                                <option value="0" ' . ( $d['bttrans_type'] == '0' ? 'selected' : '' ) . '>Shared Transport</option>
                                                <option value="1" ' . ( $d['bttrans_type'] == '1' ? 'selected' : '' ) . '>Private Transport</option>
                                                <option value="2" ' . ( $d['bttrans_type'] == '2' ? 'selected' : '' ) . '>Own Transport</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="hidden-block">
                                            <div class="form-group">
                                                <span class="small-title">Area</span>
                                                <select class="select-modal-edit form-control hotels-area" name="transport[' . $d['bdid'] . '][' . $d['btid'] . '][taid]" ' . $taid_css . '>';
                                                    if( $from_agent )
                                                    {
                                                        $content .= get_transport_area_option_by_trip( $d['taid'], $transdata, $count );
                                                    }
                                                    else
                                                    {
                                                        $content .= get_transport_area_option_by_all( $d['taid'], $transdata, $count );
                                                    }
                                                    $content .= '
                                                </select>
                                            </div>
                                            <div class="form-group flight-time ' . ( $d['taairport'] == '1' ? '' : 'sr-only' ) . '">
                                                <span class="small-title">' . $flight_txt .'</span>
                                                <input type="text" value="' . ( $d['btflighttime'] == '0000-00-00 00:00:00' ? '' : date( 'd F Y H:i', strtotime( $d['btflighttime'] ) ) ) . '" name="transport[' . $d['bdid'] . '][' . $d['btid'] . '][btflighttime]" readonly />
                                            </div>
                                        </div>
                                        <div class="hidden-block">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Name</span>
                                                <select class="select-modal-edit form-control hotels" name="transport[' . $d['bdid'] . '][' . $d['btid'] . '][hid]" ' . $hid_css . '>';
                                                    if( $from_agent )
                                                    {
                                                        $content .= get_transport_hotel_option_by_trip( $d['hid'], $transdata, $count );
                                                    }
                                                    else
                                                    {
                                                        $content .= get_transport_hotel_option_by_all( $d['hid'], $transdata, $count );
                                                    }
                                                    $content .= '
                                                </select>
                                            </div>
                                        </div>
                                        <div class="hotel-block hotel-name-block ' . $htl_css . ' ' . ( $d['hid'] == 1 ? '' : 'sr-only' ) . '">
                                            <div class="form-group">
                                                <input class="text text-full form-control h-name" type="text" value="' . $d['bthotelname'] . '" name="transport[' .  $d['bdid'] . '][' . $d['btid'] . '][bthotelname]" ' . $hid_css . ' />
                                            </div>
                                        </div>
                                        <div class="hotel-block ' . $htl_css . '">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Room Number</span>
                                                <input class="text text-full form-control h-room" type="text" value="' . $d['bthotelroomnumber'] . '" name="transport[' .  $d['bdid'] . '][' . $d['btid'] . '][bthotelroomnumber]" ' . $hid_css . ' />
                                            </div>
                                        </div>
                                        <div class="hotel-block ' . $htl_css . '">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Address</span>
                                                <input class="text text-full form-control h-address" type="text" value="' . $d['bthoteladdress'] . '" name="transport[' .  $d['bdid'] . '][' . $d['btid'] . '][bthoteladdress]" ' . $hid_css . ' />
                                            </div>
                                        </div>
                                        <div class="hotel-block ' . $htl_css . '">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Phone</span>
                                                <input class="text text-full form-control h-phone" type="text" value="' . $d['bthotelphone'] . '" name="transport[' .  $d['bdid'] . '][' . $d['btid'] . '][bthotelphone]" ' . $hid_css . ' />
                                            </div>
                                        </div>
                                        <div class="hotel-block ' . $htl_css . '">
                                            <div class="form-group">
                                                <span class="small-title">Hotel/Villa Email</span>
                                                <input class="text text-full form-control h-email" type="email" value="' . $d['bthotelemail'] . '" name="transport[' .  $d['bdid'] . '][' . $d['btid'] . '][bthotelemail]" ' . $hid_css . ' />
                                            </div>
                                        </div>
                                        <div class="driver-block ' . $drv_css . '">
                                            <div class="form-group">
                                                <span class="small-title">Driver Name</span>
                                                <input class="text text-full form-control" type="text" value="' . $d['btdrivername'] . '" name="transport[' .  $d['bdid'] . '][' . $d['btid'] . '][btdrivername]" ' . $drv_req . ' />
                                            </div>
                                        </div>
                                        <div class="driver-block ' . $drv_css . '">
                                            <div class="form-group">
                                                <span class="small-title">Driver Phone</span>
                                                <input class="text text-full form-control" type="text" value="' . $d['btdriverphone'] . '" name="transport[' .  $d['bdid'] . '][' . $d['btid'] . '][btdriverphone]" ' . $drv_req . ' />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <div class="form-group">
                                            <span class="small-title">Transport Fee</span>
                                            <input type="text" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" class="text text-number text-right form-control h-transfee" name="transport[' . $d['bdid'] . '][' . $d['btid'] . '][bttrans_fee]" value="' . $d['bttrans_fee'] . '" ' . $fee_css . '>
                                            <p class="fee regular-text sr-only">' . ( $d['bttrans_fee'] == 0 ? 'Free' : number_format( $d['bttrans_fee'], 0, ',', '.' ) ) . '</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
                }
            }
        }

        return $content;
    }
}

function get_payment_edit_field( $data )
{
    if( empty( $data['agid'] ) )
    {
        $content = '
        <h2>Payment History</h2>
        <div class="row">
            <div class="col-md-12">
                <table class="table-striped" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><p>Method</p></th>
                            <th><p>Payment Code</p></th>
                            <th><p>Invoice Number/ Ref. Code</p></th>
                            <th><p>Transaction ID / Virtual Account</p></th>
                            <th><p>Email payment</p></th>
                            <th><p>Date of payment</p></th>
                            <th><p class="text-right">Total Paid</p></th>
                            <th><p class="text-center">Action</p></th>
                        </tr>
                    </thead>
                    <tbody>';

                        if( empty( $data['payment'] ) )
                        {
                            $content .= '
                            <tr valign="top">
                                <td colspan="8" class="text-center"><p>No payment was found</p></td>
                            </tr>';
                        }
                        else
                        {
                            foreach( $data['payment'] as $p )
                            {
                                $vacc_css = $p['pmethod'] == 'Paypal' ? '' : 'sr-only';
                                $vacc_lbl = $p['pmethod'] == 'Paypal' ? 'Transaction ID' : 'Virtual Account';
                                $refc_lbl = $p['pmethod'] == 'Paypal' ? 'Invoice Number' : 'Ref. Code';

                                if( $p['pmethod'] == 'Paypal' )
                                {
                                    if( empty( $p['prefcode'] ) || empty( $p['pvirtualacc'] ) )
                                    {
                                        try
                                        {
                                            $setting  = get_paypal_action_link();
                                            $context  = getApiContext( $setting['client_id'], $setting['client_secret'] );
                                            $ppayment = Payment::get( $p['ppaycode'], $context );
                                            $ppayment = json_decode( $ppayment->toJSON( 128 ), true );

                                            if( $ppayment === null && json_last_error() !== JSON_ERROR_NONE )
                                            {
                                                $prefcode    = '-';
                                                $pvirtualacc = '-';
                                            }
                                            else
                                            {
                                                $prefcode    = isset( $ppayment['transactions'][0]['invoice_number'] ) ? $ppayment['transactions'][0]['invoice_number'] : '';
                                                $pvirtualacc = isset( $ppayment['transactions'][0]['related_resources'][0]['sale']['id'] ) ? $ppayment['transactions'][0]['related_resources'][0]['sale']['id'] : '';
                                            }
                                        }
                                        catch( Exception $ex )
                                        {
                                            $prefcode    = '-';
                                            $pvirtualacc = '-';
                                        }
                                    }
                                    else
                                    {
                                        $prefcode    = $p['prefcode'];
                                        $pvirtualacc = $p['pvirtualacc'];
                                    }

                                    $ptotalusd = $p['ptotalusd'];
                                    $ptotal    = $p['ptotal'];
                                }
                                else
                                {
                                    $ptotal      = $p['ptotal'];
                                    $prefcode    = empty( $p['prefcode'] ) ? '-' : $p['prefcode'];
                                    $pvirtualacc = '-';
                                }

                                $content .= '
                                <tr valign="top">
                                    <td>
                                        <p>' . $p['pmethod'] . '</p>
                                        <div class="edit-field">
                                            <select name="pmethod" autocomplete="off">
                                                ' . get_booking_payment_method_option( $data['bpaymethod'], $p['pmethod'] ) .  '
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <p>' . ( empty( $p['ppaycode'] ) ? '-' : $p['ppaycode'] ) . '</p>
                                        <div class="edit-field">
                                            <input class="text form-control" name="ppaycode" value="' . $p['ppaycode'] . '" type="text" autocomplete="off">
                                        </div>
                                    </td>
                                    <td>
                                        <p>' . $prefcode . '</p>
                                        <div class="edit-field">
                                            <input class="text form-control" name="prefcode" value="' . $prefcode . '" type="text" autocomplete="off">
                                        </div>
                                    </td>
                                    <td>
                                        <p>' . $pvirtualacc . '</p>
                                        <div class="edit-field">
                                            <input class="text form-control" name="pvirtualacc" value="' . $pvirtualacc . '" type="text" autocomplete="off">
                                        </div>
                                    </td>
                                    <td>
                                        <p>' . $p['ppayemail'] . '</p>
                                        <div class="edit-field">
                                            <input class="text form-control" name="ppayemail" value="' . $p['ppayemail'] . '" type="email" autocomplete="off">
                                        </div>
                                    </td>
                                    <td>
                                        <p>' . ( empty( $p['ppaydate'] ) ? '' : date( 'd F Y', $p['ppaydate'] ) ) . '</p>
                                        <div class="edit-field">
                                            <input class="text txt-date form-control" name="ppaydate" value="' . ( empty( $p['ppaydate'] ) ? '' : date( 'd F Y', $p['ppaydate'] ) ) . '" type="text" autocomplete="off">
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-right">' . number_format( $ptotal, 0, ',', '.' ) . ( isset( $ptotalusd ) ? '<br/><span style="color:red;">in USD ' . $ptotalusd . '</span>' : '' ) . '</p>
                                        <div class="edit-field">
                                            <input data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="2" class="text text-number text-right form-control ptotal ptotalusd" name="ptotal" value="' . ( isset( $ptotalusd ) ? $ptotalusd : $ptotal ) . '" type="text" autocomplete="off">
                                            <input data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="2" class="sr-only text text-number text-right form-control ptotal" value="' . $ptotal . '" type="text" autocomplete="off">
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-center">
                                            <a class="edit-link" href="javascript:;">Edit</a>&nbsp;|&nbsp;
                                            <a class="delete-link" data-id="' . $p['pid'] . '" href="javascript:;">Delete</a>
                                        </p>
                                        <div class="edit-field text-cnter">
                                            <a class="update-link" data-id="' . $p['pid'] . '" href="javascript:;">Save</a>&nbsp;|&nbsp;
                                            <a class="cancel-edit" href="javascript:;">Cancel</a>
                                        </div>
                                    </td>
                                </tr>';
                            }
                        }

                        $content .= '
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <div class="add-new-payment-field add-new-payment-field-2">
                    <div class="inner sr-only">
                        <fieldset>
                            <div class="row">
                                <div class="col-md-3">
                                    <p class="text-right">Payment Date :</p>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="text txt-date form-control" name="nppaydate" value="' . date( 'd F Y' ) . '" data-date="' . date( 'd F Y' ) . '" autocomplete="off" />
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-3">
                                    <p class="text-right">Payment Method :</p>
                                </div>
                                <div class="col-md-9">
                                    <select class="select-option" name="npmethod" autocomplete="off">
                                        ' . get_booking_payment_method_option() .  '
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-3">
                                    <p class="text-right">Invoice Number/ Ref. Code</p>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="text form-control" name="nprefcode" value="" autocomplete="off" />
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-3">
                                    <p class="text-right">Transaction ID / Virtual Account</p>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="text form-control" name="npvirtualacc" value="" autocomplete="off" />
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-3">
                                    <p class="text-right">Email Payment</p>
                                </div>
                                <div class="col-md-9">
                                    <input type="email" class="text form-control" name="nppayemail" value="" autocomplete="off" />
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-3">
                                    <p class="text-right">Paid Amount :</p>
                                </div>
                                <div class="col-md-9">
                                    <input data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="2" type="text" class="text text-number text-right form-control" name="nptotal" value="0" autocomplete="off" />
                                </div>
                            </div>
                        </fieldset>

                        <p class="text-right">
                            <a class="save-new-payment-2" data-id="' . $data['bid'] . '">Save New Payment</a>&nbsp;|&nbsp;<a class="cancel-new-payment-2" data-id="' . $data['bid'] . '">Cancel</a>
                        </p>
                    </div>

                    <p class="text-right">
                        <a class="add-new-payment-2 pull-right">Add Payment</a>
                    </p>
                </div>
            </div>
        </div>';
    }
    else
    {
        $ag_payment_type = get_agent( $data['agid'], 'agpayment_type' );

        if( $ag_payment_type == 'Pre Paid' )
        {
            $content = '
            <h2>Payment History</h2>
            <div class="row">
                <div class="col-md-12">
                    <table class="table-striped" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th><p>Guest Name</p></th>
                                <th><p>Status</p></th>
                                <th><p>Date Of Payment</p></th>
                                <th><p>Method</p></th>
                                <th><p class="text-right">Debet</p></th>
                                <th><p class="text-right">Credit</p></th>
                                <th><p class="text-right">Remain</p></th>
                                <th width="150"><p class="text-center">Action</p></th>
                            </tr>
                        </thead>
                        <tbody>';

                            $dt = get_booking_payment_history( $data['bid'] );

                            if( empty( $dt ) )
                            {
                                $content .= '
                                <tr>
                                    <td colspan="8" class="text-center"><p>No payment was found</p></td>
                                </tr>';
                            }
                            else
                            {
                                foreach( $dt as $i => $d )
                                {
                                    $content .= '
                                    <tr valign="top">
                                        <td>
                                            <p>' . $d['atgname'] . '</p>
                                            <div class="edit-field">
                                                <input class="atgname text form-control" name="atgname[' . $i . ']" value="' . $d['atgname'] . '" type="text" autocomplete="off">
                                            </div>
                                        </td>
                                        <td>
                                            <p>' . $d['atstatus'] . '</p>
                                            <div class="edit-field">
                                                <input class="atstatus text form-control" name="atstatus[' . $i . ']" value="' . $d['atstatus'] . '" type="text" autocomplete="off" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <p>' . date( 'd F Y', strtotime( $d['atdate'] ) ) . '</p>
                                            <div class="edit-field">
                                                <input class="atdate text txt-date form-control" name="atdate[' . $i . ']" value="' . date( 'd F Y', strtotime( $d['atdate'] ) ) . '" type="text" autocomplete="off" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <p>' . $d['atmethod'] . '</p>
                                            <div class="edit-field">
                                                <select class="atmethod" name="atmethod[' . $i . ']" autocomplete="off">
                                                    ' . get_booking_payment_method_option( $d['atmid'] ) .  '
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-right">' . number_format( $d['atdebet'], 0, '', '.' ) . '</p>
                                            <div class="edit-field">
                                                <input class="atdebet text text-number text-right form-control" name="atdebet[' . $i . ']" value="' . $d['atdebet'] . '" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" type="text" autocomplete="off" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-right">' . number_format( $d['atcredit'], 0, '', '.' ) . '</p>
                                            <div class="edit-field">
                                                <input class="atcredit text text-number text-right form-control" name="atcredit[' . $i . ']" value="' . $d['atcredit'] . '" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" type="text" autocomplete="off">
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-right">' . number_format( $d['atoutstanding'], 0, '', '.' ) . '</p>
                                            <div class="edit-field">
                                                <input class="atoutstanding text text-number text-right form-control" name="atoutstanding[' . $i . ']" value="' . $d['atoutstanding'] . '" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0" type="text" autocomplete="off" readonly>
                                            </div>
                                        </td>
                                        <td>';

                                            if( $d['atstatus'] != 'Confirmed' )
                                            {
                                                $content .= '
                                                <p class="text-center">
                                                    <a class="edit-outstanding-payment" href="javascript:;">Edit</a>&nbsp;|&nbsp;
                                                    <a class="delete-outstanding-payment" data-id="' . $d['atid'] . '" href="javascript:;">Delete</a>
                                                </p>
                                                <div class="edit-field text-center">
                                                    <a class="update-outstanding-payment" data-id="' . $d['atid'] . '" href="javascript:;">Save</a>&nbsp;|&nbsp;
                                                    <a class="cancel-edit-outstanding-payment" href="javascript:;">Cancel</a>
                                                </div>';
                                            }

                                            $content .= '
                                        </td>
                                    </tr>';
                                }
                            }

                            $content .= '
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="add-new-payment-field">
                        <div class="inner sr-only">
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="text-right">Payment Date :</p>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="text txt-date form-control" name="atdate" value="' . date( 'd F Y' ) . '" data-date="' . date( 'd F Y' ) . '" autocomplete="off" />
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="text-right">Payment Method :</p>
                                    </div>
                                    <div class="col-md-9">
                                        <select class="select-option" name="atmethod" autocomplete="off">
                                            ' . get_booking_payment_method_option() .  '
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="text-right">Paid Amount :</p>
                                    </div>
                                    <div class="col-md-9">
                                        <input data-v-min="0" data-v-max="' . ( $data['btotal'] - $data['bonhandtotal'] ) . '" data-a-sep="." data-a-dec="," data-m-dec="2" type="text" class="text text-number text-right form-control" name="atcredit" value="0" autocomplete="off" />
                                    </div>
                                </div>
                            </fieldset>

                            <p class="text-right">
                                <a class="save-new-payment" data-id="' . $data['agid'] . '">Save New Payment</a>&nbsp;|&nbsp;<a class="cancel-new-payment" data-id="' . $data['bid'] . '">Cancel</a>
                            </p>
                        </div>

                        <p class="text-right">
                            <a class="add-new-payment pull-right">Add Payment</a>
                        </p>
                    </div>
                </div>
            </div>';
        }
        else
        {
            $content = '
            <h2>Payment History</h2>
            <div class="row">
                <div class="col-md-12">
                    <table class="table-striped" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th><p>Guest Name</p></th>
                                <th><p>Status</p></th>
                                <th><p>Date Of Payment</p></th>
                                <th><p>Method</p></th>
                                <th><p class="text-right">Debet</p></th>
                                <th><p class="text-right">Credit</p></th>
                                <th><p class="text-right">Remain</p></th>
                            </tr>
                        </thead>
                        <tbody>';

                            $dt = get_booking_payment_history( $data['bid'] );

                            if( empty( $dt ) )
                            {
                                $content .= '
                                <tr>
                                    <td colspan="7" class="text-center"><p>No payment was found</p></td>
                                </tr>';
                            }
                            else
                            {
                                foreach( $dt as $d )
                                {
                                    $content .= '
                                    <tr valign="top">
                                        <td><p>' . $d['atgname'] . '</p></td>
                                        <td><p>' . $d['atstatus'] . '</p></td>
                                        <td><p>' . date( 'd F Y', strtotime( $d['atdate'] ) ) . '</p></td>
                                        <td><p>' . $d['atmethod'] . '</p></td>
                                        <td><p class="text-right">' . number_format( $d['atdebet'], 0, '', '.' ) . '</p></td>
                                        <td><p class="text-right">' . number_format( $d['atcredit'], 0, '', '.' ) . '</p></td>
                                        <td><p class="text-right">' . number_format( $d['atoutstanding'], 0, '', '.' ) . '</p></td>
                                    </tr>';
                                }
                            }

                            $content .= '
                        </tbody>
                    </table>
                </div>
            </div>';
        }
    }

    return $content;
}

function get_booking_payment_history( $bid )
{
    global $db;

    $s = 'SELECT
            a.bid,
            b.atid,
            b.agid,
            b.atmid,
            b.atdate,
            b.atgname,
            b.atdebet,
            b.atcredit,
            b.atstatus,
            b.atmethod
          FROM ticket_booking AS a
          RIGHT JOIN ticket_agent_transaction AS b ON b.bid = a.bid
          WHERE a.bid = %d AND a.bstt <> %s ORDER BY b.atdate ASC';
    $q = $db->prepare_query( $s, $bid, 'ar' );
    $r = $db->do_query( $q );

    $data = array();

    $atoutstanding = 0;

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            if( $d['atstatus'] == 1 )
            {
                $d['atstatus'] = 'Confirmed';
            }
            elseif( $d['atstatus'] == 2 )
            {
                $d['atstatus'] = 'Paid Balance';
            }
            elseif( $d['atstatus'] == 3 )
            {
                $d['atstatus'] = 'Canceled, cancelation fee applied';
            }

            $atoutstanding = $atoutstanding + ( $d['atdebet'] - $d['atcredit'] );

            $d['atoutstanding'] = $atoutstanding;

            $data[] = $d;
        }

        krsort( $data );
    }

    return $data;
}

function get_transport_area_option_by_trip( $taid, $trans, $count = 0 )
{
    $options   = '
    <option value="0|00:00:00|00:00:00" data-area-id="0" data-airport="0" data-fee="0" ' . ( is_null( $taid ) ? 'selected' : '' ) . '>No Accommodation Booked</option>';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        foreach( $trans as $d )
        {
            $fee      = $d['tafee'] * $count;
            $selected = $taid == $d['taid'] ? 'selected' : '';

            $options .= '
            <option value="' . $d['taid'] . '|' . $d['rpfrom'] . '|' . $d['rpto'] . '"
                data-area-id="' . $d['taid'] . '"
                data-airport="' . $d['taairport'] . '"
                data-fee="' . $fee . '" ' . $selected . '>
                ' . $d['taname'] . '
            </option>';
        }

        return $options;
    }
}

function get_transport_area_option_by_all( $taid, $trans, $count = 0 )
{
    global $db;

    $options = '
    <option value="0|00:00:00|00:00:00" data-area-id="0" data-airport="0" data-fee="0" ' . ( is_null( $taid ) ? 'selected' : '' ) . '>No Accommodation Booked</option>';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        $rpfrom = array();
        $rpto   = array();
        $lcid   = '';

        if( !empty( $trans ) )
        {
            foreach( $trans as $t )
            {
                $rpfrom[ $t['taid'] ] = $t['rpfrom'];
                $rpto[ $t['taid'] ]   = $t['rpto'];
                $lcid                 = $t['lcid'];
            }
        }

        $s = 'SELECT * FROM ticket_transport_area AS a
              LEFT JOIN ticket_transport_area_fee AS b ON b.taid = a.taid
              WHERE b.lcid = %s ORDER BY a.taname';
        $q = $db->prepare_query( $s, $lcid );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            $fee      = $d['tafee'] * $count;
            $selected = $taid == $d['taid'] ? 'selected' : '';
            $from     = isset( $rpfrom[ $d['taid'] ] ) ? $rpfrom[ $d['taid'] ] : '';
            $to       = isset( $rpto[ $d['taid'] ] ) ? $rpto[ $d['taid'] ] : '';

            $options .= '
            <option value="' . $d['taid'] . '|' . $from . '|' . $to . '"
                data-area-id="' . $d['taid'] . '"
                data-airport="' . $d['taairport'] . '"
                data-fee="' . $fee . '" ' . $selected . '>
                ' . $d['taname'] . '
            </option>';
        }

        return $options;
    }
}

function get_transport_hotel_option_by_trip( $hid, $trans, $count = 0 )
{
    global $db;

    $options   = '
    <option value="1" data-address="" data-phone="" data-email="" data-airport="" data-area-id="" data-fee="0" ' . ( $hid == 1  ? 'selected' : '' ) . '>Not in list/To be Advised</option>';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        $area   = array();
        $lcid   = '';

        foreach( $trans as $t )
        {
            $area[] = $t['taid'];
            $lcid   = $t['lcid'];
        }

        $s = 'SELECT * FROM ticket_hotel AS a
              LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
              LEFT JOIN ticket_transport_area_fee AS c ON c.taid = b.taid
              WHERE a.hstatus = %d AND c.lcid = %s AND a.taid IN ( ' . implode( ',', $area ) . ' ) ORDER BY a.hname';
        $q = $db->prepare_query( $s, 1, $lcid );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            $fee      = $d['tafee'] * $count;
            $selected = $hid == $d['hid'] ? 'selected' : '';
            $hname    = empty( $d['hname'] ) ? '' : mb_convert_encoding( $d['hname'], 'UTF-8', 'auto' );
            $haddress = empty( $d['haddress'] ) ? '' : mb_convert_encoding( $d['haddress'], 'UTF-8', 'auto' );

            $options .= '
            <option value="' . $d['hid'] . '"
                data-area-id="' . $d['taid'] . '"
                data-airport="' . $d['taairport'] . '"
                data-address="' . $haddress . '"
                data-phone="' . $d['hphone'] . '"
                data-email="' . $d['hemail'] . '"
                data-fee="' . $fee . '" ' . $selected . '>
                ' . $hname . '
            </option>';
        }

        return $options;
    }
}

function get_transport_hotel_option_by_all( $hid, $trans, $count = 0 )
{
    global $db;

    $options   = '
    <option value="1" data-address="" data-phone="" data-email="" data-airport="" data-area-id="" data-fee="0" ' . ( $hid == 1  ? 'selected' : '' ) . '>Not in list/To be Advised</option>';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        $area   = array();
        $lcid   = '';

        foreach( $trans as $t )
        {
            $area[] = $t['taid'];
            $lcid   = $t['lcid'];
        }

        $s = 'SELECT * FROM ticket_hotel AS a
              LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
              LEFT JOIN ticket_transport_area_fee AS c ON c.taid = b.taid
              WHERE a.hstatus = %d AND c.lcid = %s ORDER BY a.hname';
        $q = $db->prepare_query( $s, 1, $lcid );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            $fee      = $d['tafee'] * $count;
            $selected = $hid == $d['hid'] ? 'selected' : '';
            $hname    = empty( $d['hname'] ) ? '' : mb_convert_encoding( $d['hname'], 'UTF-8', 'auto' );
            $haddress = empty( $d['haddress'] ) ? '' : mb_convert_encoding( $d['haddress'], 'UTF-8', 'auto' );

            $options .= '
            <option value="' . $d['hid'] . '"
                data-area-id="' . $d['taid'] . '"
                data-airport="' . $d['taairport'] . '"
                data-address="' . $haddress . '"
                data-phone="' . $d['hphone'] . '"
                data-email="' . $d['hemail'] . '"
                data-fee="' . $fee . '" ' . $selected . '>
                ' . $hname . '
            </option>';
        }

        return $options;
    }
}

function ticket_booking_revise_passanger_data( $post, $data, $bid )
{
    global $db;

    $retval = 1;

    $db->begin();

    //-- Redefined old data ( number of pass, subtotal, discount, total )
    //-- Base on revise passenger
    if( !empty( $post['depart_id'] ) )
    {
        if( isset( $post['passenger'][ 'departure' ] ) )
        {
            foreach( $post['passenger'][ 'departure' ] as $bptype => $pobj )
            {
                foreach( $pobj as $p )
                {
                    foreach( $data['detail'][ 'departure' ][ $post['depart_id'] ]['passenger'][ $bptype ] as $key => $val )
                    {
                        if( $val['bpid'] == $p['bpid'] )
                        {
                            $dsc_pass = $data['detail'][ 'departure' ][ $post['depart_id'] ]['disc_price_per_' . $bptype];
                            $prc_pass = $data['detail'][ 'departure' ][ $post['depart_id'] ]['price_per_' . $bptype];
                            $num_pass = $data['detail'][ 'departure' ][ $post['depart_id'] ]['num_' . $bptype];
                            $subtotal = $data['detail'][ 'departure' ][ $post['depart_id'] ]['subtotal'];
                            $discount = $data['detail'][ 'departure' ][ $post['depart_id'] ]['discount'];
                            $total    = $data['detail'][ 'departure' ][ $post['depart_id'] ]['total'];

                            unset( $data['detail'][ 'departure' ][ $post['depart_id'] ]['passenger'][ $bptype ][ $key ] );

                            if( count( $data['detail'][ 'departure' ][ $post['depart_id'] ]['passenger'][ $bptype ] ) == 0 )
                            {
                                unset( $data['detail'][ 'departure' ][ $post['depart_id'] ]['passenger'][ $bptype ] );
                            }

                            if( $num_pass > 0 )
                            {
                                $data['detail'][ 'departure' ][ $post['depart_id'] ]['num_' . $bptype] = $num_pass - 1;
                            }

                            if( $subtotal > 0 )
                            {
                                $data['detail'][ 'departure' ][ $post['depart_id'] ]['subtotal'] = $subtotal - $prc_pass;
                            }

                            if( $discount > 0 )
                            {
                                $data['detail'][ 'departure' ][ $post['depart_id'] ]['discount'] = $discount - $dsc_pass;
                            }

                            if( $total > 0 )
                            {
                                $ntransfee = $data['detail'][ 'departure' ][ $post['depart_id'] ]['transport_fee'];
                                $nsubtotal = $data['detail'][ 'departure' ][ $post['depart_id'] ]['subtotal'];
                                $ndiscount = $data['detail'][ 'departure' ][ $post['depart_id'] ]['discount'];

                                $data['detail'][ 'departure' ][ $post['depart_id'] ]['total'] =  $nsubtotal + $ntransfee - $ndiscount;
                            }
                            else
                            {
                                $data['detail'][ 'departure' ][ $post['depart_id'] ]['total'] = 0;
                            }
                        }
                    }
                }
            }
        }
    }

    if( !empty( $post['return_id'] ) )
    {
        if( isset( $post['passenger'][ 'return' ] ) )
        {
            foreach( $post['passenger'][ 'return' ] as $bptype => $pobj )
            {
                foreach( $pobj as $p )
                {
                    foreach( $data['detail'][ 'return' ][ $post['return_id'] ]['passenger'][ $bptype ] as $key => $val )
                    {
                        if( $val['bpid'] == $p['bpid'] )
                        {
                            $dsc_pass = $data['detail'][ 'return' ][ $post['return_id'] ]['disc_price_per_' . $bptype];
                            $prc_pass = $data['detail'][ 'return' ][ $post['return_id'] ]['price_per_' . $bptype];
                            $num_pass = $data['detail'][ 'return' ][ $post['return_id'] ]['num_' . $bptype];
                            $subtotal = $data['detail'][ 'return' ][ $post['return_id'] ]['subtotal'];
                            $discount = $data['detail'][ 'return' ][ $post['return_id'] ]['discount'];
                            $total    = $data['detail'][ 'return' ][ $post['return_id'] ]['total'];

                            unset( $data['detail'][ 'return' ][ $post['return_id'] ]['passenger'][ $bptype ][ $key ] );

                            if( count( $data['detail'][ 'return' ][ $post['return_id'] ]['passenger'][ $bptype ] ) == 0 )
                            {
                                unset( $data['detail'][ 'return' ][ $post['return_id'] ]['passenger'][ $bptype ] );
                            }

                            if( $num_pass > 0 )
                            {
                                $data['detail'][ 'return' ][ $post['return_id'] ]['num_' . $bptype] = $num_pass - 1;
                            }

                            if( $subtotal > 0 )
                            {
                                $data['detail'][ 'return' ][ $post['return_id'] ]['subtotal'] = $subtotal - $prc_pass;
                            }

                            if( $discount > 0 )
                            {
                                $data['detail'][ 'return' ][ $post['return_id'] ]['discount'] = $discount - $dsc_pass;
                            }

                            if( $total > 0 )
                            {
                                $ntransfee = $data['detail'][ 'return' ][ $post['return_id'] ]['transport_fee'];
                                $nsubtotal = $data['detail'][ 'return' ][ $post['return_id'] ]['subtotal'];
                                $ndiscount = $data['detail'][ 'return' ][ $post['return_id'] ]['discount'];

                                $data['detail'][ 'return' ][ $post['return_id'] ]['total'] =  $nsubtotal + $ntransfee - $ndiscount;
                            }
                            else
                            {
                                $data['detail'][ 'return' ][ $post['return_id'] ]['total'] = 0;
                            }
                        }
                    }
                }
            }
        }
    }

    //-- Insert Old Data Booking Minus Revise Passenger
    $nbsubtotal = 0;
    $bparam     = array();
    $old_bcode  = $data[ 'bcode' ];
    $new_bcode  = generate_code_number( $data[ 'btype' ] );

    $pass_exc   = array( 'bpid' );
    $booked_exc = array( 'bid', 'ntitle', 'agname', 'agemail', 'agphone', 'agpayment_type', 'chname', 'detail', 'payment', 'agent_transaction', 'booking_detail_id' );
    $detail_exc = array( 'bdid', 'rname', 'rstatus', 'rdepart', 'rarrive', 'rcot', 'rtype', 'rhoppingpoint', 'passenger', 'transport' );
    $trans_exc  = array( 'btid', 'hname', 'haddress', 'hphone', 'hphone2', 'hfax', 'hemail', 'hstatus', 'hcreateddate', 'luser_id', 'taname', 'tacreateddate', 'taairport', 'tatypeforagent', 'tatypeforonline' );

    foreach( $data as $field => $val )
    {
        if( $val != '' && !in_array( $field, $booked_exc ) )
        {
            if( $field == 'bcode' )
            {
                $bparam[ $field ] = $new_bcode;
            }
            else
            {
                $bparam[ $field ] = $val;
            }
        }
    }

    if( !empty( $bparam ) )
    {
        //-- Insert New Booking Data
        //-- Base On Old Booking Data
        $q = 'INSERT INTO ticket_booking(' . implode( ',', array_keys( $bparam ) ) . ') VALUES ("' . implode( '" , "', $bparam ) . '")';
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            $retval = 0;
        }
        else
        {
            $new_bid = $db->insert_id();

            foreach( $data[ 'detail' ] as $bdtype => $details )
            {
                foreach( $details as $bdid => $detail )
                {
                    $bdparam = array();

                    $detail[ 'bid' ] = $new_bid;

                    foreach( $detail as $field => $val )
                    {
                        if( $val != '' && $val != '0000-00-00 00:00:00' && !in_array( $field, $detail_exc ) )
                        {
                            $bdparam[ $field ] = $val;
                        }
                    }

                    if( isset( $bdparam[ 'total' ] ) )
                    {
                        $nbsubtotal += $bdparam[ 'total' ];
                    }

                    if( !empty( $bdparam ) && !empty( $data[ 'detail' ][ $bdtype ][ $bdid ][ 'passenger' ] ) )
                    {
                        //-- Insert New Booking Detail Data
                        //-- Base On Old Booking Data
                        $q = 'INSERT INTO ticket_booking_detail(' . implode( ',', array_keys( $bdparam ) ) . ') VALUES ("' . implode( '" , "', $bdparam ) . '")';
                        $r = $db->do_query( $q );

                        if( is_array( $r ) )
                        {
                            $retval = 0;
                        }
                        else
                        {
                            $new_bdid = $db->insert_id();
                            $new_pass = array();

                            foreach( $data[ 'detail' ][ $bdtype ][ $bdid ][ 'passenger' ] as $bptype => $passenger )
                            {
                                foreach( $passenger as $obj )
                                {
                                    $obj[ 'bdid' ]   = $new_bdid;
                                    $obj[ 'bptype' ] = $bptype;

                                    $bpparam = array();

                                    foreach( $obj as $field => $val )
                                    {
                                        if( $val != '' && !in_array( $field, $pass_exc ) )
                                        {
                                            $bpparam[ $field ] = $val;
                                        }
                                    }

                                    if( !empty( $bpparam ) )
                                    {
                                        //-- Insert New Booking Passenger Data
                                        //-- Base On Old Booking Data
                                        $q = 'INSERT INTO ticket_booking_passenger(' . implode( ',', array_keys( $bpparam ) ) . ') VALUES ("' . implode( '" , "', $bpparam ) . '")';
                                        $r = $db->do_query( $q );

                                        if( is_array( $r ) )
                                        {
                                            $retval = 0;
                                        }
                                        else
                                        {
                                            $bpparam[ 'bpid' ] = $db->insert_id();

                                            $new_pass[] = $bpparam;
                                        }
                                    }
                                }
                            }

                            $new_trans = array();

                            foreach( $data[ 'detail' ][ $bdtype ][ $bdid ][ 'transport' ] as $bttype => $transport )
                            {
                                foreach( $transport as $obj )
                                {
                                    $obj[ 'bdid' ] = $new_bdid;

                                    $btparam = array();

                                    foreach( $obj as $field => $val )
                                    {
                                        if( $val != '' && !in_array( $field, $trans_exc ) )
                                        {
                                            $btparam[ $field ] = $val;
                                        }
                                    }

                                    //-- Insert New Booking Transport Data
                                    //-- Base On Old Booking Data
                                    $q = 'INSERT INTO ticket_booking_transport(' . implode( ',', array_keys( $btparam ) ) . ') VALUES ("' . implode( '" , "', $btparam ) . '")';
                                    $r = $db->do_query( $q );

                                    if( is_array( $r ) )
                                    {
                                        $retval = 0;
                                    }
                                    else
                                    {
                                        $taid = isset( $obj['taid'] ) ? $obj['taid'] : '';

                                        $new_trans[] = array(
                                            'btid' => $db->insert_id(),
                                            'bttype' => $bttype,
                                            'taid' => $taid
                                        );
                                    }
                                }
                            }

                            //-- Check Port Clearance Detail
                            $s = 'SELECT a.pcid, c.bddate, c.rid
                                  FROM ticket_port_clearance_detail AS a
                                  LEFT JOIN ticket_booking_passenger AS b ON a.bpid = b.bpid
                                  LEFT JOIN ticket_booking_detail AS c ON b.bdid = c.bdid
                                  WHERE c.bid = %d AND c.bdtype = %s  GROUP BY c.bdid';
                            $q = $db->prepare_query( $s, $bid, $bdtype );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $d[ 'rid' ] == $bdparam[ 'rid' ] )
                                {
                                    foreach( $new_pass as $np )
                                    {
                                        $guest_age     = empty( $np['bpbirthdate'] ) ? 0 : date_diff( date_create( $np['bpbirthdate'] ), date_create( date( 'Y-m-d' ) ) )->format( '%y' );
                                        $guest_country = get_nationality( $np['lcountry_id'], 'lcountry_code' );
                                        $guest_gender  = $np['bpgender'] == 1 ? 'Male' : 'Female';

                                        $s2 = 'INSERT INTO ticket_port_clearance_detail( pcid, bpid, lcountry_id, guest_name, guest_type, guest_country, guest_gender, guest_age ) VALUES( %d, %d, %d, %s, %s, %s, %s, %s )';
                                        $q2 = $db->prepare_query( $s2, $d['pcid'], $np['bpid'], $np['lcountry_id'], $np['bpname'], $np['bptype'], $guest_country, $guest_gender, $guest_age );
                                        $r2 = $db->do_query( $q2 );

                                        if( is_array( $r2 ) )
                                        {
                                            $retval = 0;
                                        }
                                    }
                                }
                            }

                            //-- Check Trip Transport Detail
                            $s = 'SELECT * FROM ticket_trip_transport AS a
                                  LEFT JOIN ticket_trip_transport_detail AS b ON b.ttid = a.ttid
                                  LEFT JOIN ticket_booking_transport AS c ON b.btid = c.btid
                                  LEFT JOIN ticket_booking_detail AS d ON c.bdid = d.bdid
                                  WHERE d.bid = %d AND d.bdtype = %s  GROUP BY d.bdid';
                            $q = $db->prepare_query( $s, $bid, $bdtype );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                $ttlocation = $d['ttdtype'] == 'pickup' ? $bdparam['bdfrom'] : $bdparam['bdto'];
                                $tttime     = strtotime( $bdparam[ 'bddeparttime' ] );
                                $bddeptime  = strtotime( $d[ 'tttime' ] );

                                if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $tttime == $bddeptime && $d[ 'ttlocation' ] == $ttlocation )
                                {
                                    foreach( $new_trans as $nt )
                                    {
                                        if( $d['taid'] == $nt['taid'] )
                                        {
                                            $s2 = 'INSERT INTO ticket_trip_transport_detail( ttid, btid, ttdtype ) VALUES( %d, %d, %s )';
                                            $q2 = $db->prepare_query( $s2, $d['ttid'], $nt['btid'], $nt['bttype'] );
                                            $r2 = $db->do_query( $q2 );

                                            if( is_array( $r2 ) )
                                            {
                                                $retval = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            foreach( $post[ 'booked_detail' ] as $bdtype => $detail )
            {
                $bdparam  = array();
                $data_key = $post[ $bdtype . '_trip_transport_val' ];

                $detail[ $data_key ][ 'bid' ]           = $new_bid;
                $detail[ $data_key ][ 'transport_fee' ] = 0; //array_sum( array_column( $post[ 'transport' ][ $bdtype ][ $data_key ], 'bttrans_fee' ) );
                $detail[ $data_key ][ 'total' ]         = $detail[ $data_key ][ 'subtotal' ] + $detail[ $data_key ][ 'transport_fee' ] - $detail[ $data_key ][ 'discount' ];
                $detail[ $data_key ][ 'num_adult' ]     = isset( $post[ 'passenger' ][ $bdtype ][ 'adult' ] ) ? count( $post[ 'passenger' ][ $bdtype ][ 'adult' ] ) : 0;
                $detail[ $data_key ][ 'num_child' ]     = isset( $post[ 'passenger' ][ $bdtype ][ 'child' ] ) ? count( $post[ 'passenger' ][ $bdtype ][ 'child' ] ) : 0;
                $detail[ $data_key ][ 'num_infant' ]    = isset( $post[ 'passenger' ][ $bdtype ][ 'infant' ] ) ? count( $post[ 'passenger' ][ $bdtype ][ 'infant' ] ) : 0;

                foreach( $detail[ $data_key ] as $field => $val )
                {
                    if( $val != '' )
                    {
                        $bdparam[ $field ] = $val;
                    }
                }

                if( isset( $bdparam[ 'total' ] ) )
                {
                    $nbsubtotal += $bdparam[ 'total' ];
                }

                if( !empty( $bdparam ) )
                {
                    //-- Insert New Booking Detail Data
                    //-- Base On Post Data
                    $q = 'INSERT INTO ticket_booking_detail(' . implode( ',', array_keys( $bdparam ) ) . ') VALUES ("' . implode( '" , "', $bdparam ) . '")';
                    $r = $db->do_query( $q );

                    if( is_array( $r ) )
                    {
                        $retval = 0;
                    }
                    else
                    {
                        $new_bdid = $db->insert_id();
                        $new_pass = array();

                        foreach( $post[ 'passenger' ][ $bdtype ] as $bptype => $passenger )
                        {
                            foreach( $passenger as $obj )
                            {
                                $obj[ 'bdid' ]   = $new_bdid;
                                $obj[ 'bptype' ] = $bptype;

                                unset( $obj[ 'bpid' ] );

                                $bpparam = array();

                                foreach( $obj as $field => $val )
                                {
                                    if( $val != '' )
                                    {
                                        if( $field == 'bpbirthdate' )
                                        {
                                            $val = date( 'Y-m-d', strtotime( $val ) );
                                        }

                                        $bpparam[ $field ] = $val;
                                    }
                                }

                                if( !empty( $bpparam ) )
                                {
                                    //-- Insert New Booking Passenger Data
                                    //-- Base On Post Data
                                    $q = 'INSERT INTO ticket_booking_passenger(' . implode( ',', array_keys( $bpparam ) ) . ') VALUES ("' . implode( '" , "', $bpparam ) . '")';
                                    $r = $db->do_query( $q );

                                    if( is_array( $r ) )
                                    {
                                        $retval = 0;
                                    }
                                    else
                                    {
                                        $bpparam[ 'bpid' ] = $db->insert_id();

                                        $new_pass[] = $bpparam;
                                    }
                                }
                            }
                        }

                        $new_trans = array();

                        foreach( $post[ 'transport' ][ $bdtype ][ $data_key ] as $bttype => $obj )
                        {
                            if( $obj['bttrans_type'] == 2 )
                            {
                                $s = 'INSERT INTO ticket_booking_transport( bdid, bttype, bttrans_type, btdrivername, btdriverphone, bttrans_fee ) VALUES( %d, %s, %s, %s, %s, %s )';
                                $q = $db->prepare_query( $s, $new_bdid, $bttype, $obj['bttrans_type'], $obj['btdrivername'], $obj['btdriverphone'], $obj['bttrans_fee'] );
                            }
                            else
                            {
                                if( empty( $obj['taid'] ) || empty( $obj['hid'] ) )
                                {
                                    $s = 'INSERT INTO ticket_booking_transport( bdid, bttype, bttrans_type, bttrans_fee ) VALUES( %d, %s, %s, %s )';
                                    $q = $db->prepare_query( $s, $new_bdid, $bttype, $obj['bttrans_type'], $obj['bttrans_fee'] );
                                }
                                else
                                {
                                    if( $obj['hid'] == 1 )
                                    {
                                        $bthotelname       = isset( $obj['bthotelname'] ) ? $obj['bthotelname'] : '';
                                        $bthotelphone      = isset( $obj['bthotelphone'] ) ? $obj['bthotelphone'] : '';
                                        $bthotelemail      = isset( $obj['bthotelemail'] ) ? $obj['bthotelemail'] : '';
                                        $bthoteladdress    = isset( $obj['bthoteladdress'] ) ? $obj['bthoteladdress'] : '';
                                        $bthotelroomnumber = isset( $obj['bthotelroomnumber'] ) ? $obj['bthotelroomnumber'] : '';
                                    }
                                    else
                                    {
                                        $h = get_hotel( $obj['hid'] );

                                        $bthotelname       = $h['hname'];
                                        $bthotelphone      = $h['hphone'];
                                        $bthotelemail      = $h['hemail'];
                                        $bthoteladdress    = $h['haddress'];
                                        $bthotelroomnumber = '';
                                    }

                                    list( $taid, $btrpfrom, $btrpto ) = explode( '|', $obj['taid'] );

                                    $btflighttime = isset( $obj['btflighttime'] ) ? ( empty( $obj['btflighttime'] ) ? '' : date( 'Y-m-d H:i:s', strtotime( $obj['btflighttime'] ) ) ) : '';

                                    //-- Insert New Booking Transport Data
                                    //-- Base On Post Data
                                    $btrans = array(
                                        'bdid' => $new_bdid,
                                        'hid' => $obj['hid'],
                                        'taid' => $taid,
                                        'bttype' => $bttype,
                                        'bttrans_type' => $obj['bttrans_type'],
                                        'bthotelname' => $bthotelname,
                                        'bthoteladdress' => $bthoteladdress,
                                        'bthotelphone' => $bthotelphone,
                                        'bthotelemail' => $bthotelemail,
                                        'bthotelroomnumber' => $bthotelroomnumber,
                                        'btflighttime' => $btflighttime,
                                        'btrpfrom' => $btrpfrom,
                                        'btrpto' => $btrpto,
                                        'bttrans_fee' => 0
                                    );

                                    if( empty( $btflighttime ) )
                                    {
                                        unset( $btrans['btflighttime'] );
                                    }

                                    if( empty( $taid ) )
                                    {
                                        unset( $btrans['taid'] );
                                    }

                                    $q = 'INSERT INTO ticket_booking_transport(' . implode( ',', array_keys( $btrans ) ) . ') VALUES ("' . implode( '" , "', $btrans ) . '")';
                                }
                            }

                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $taid = isset( $taid ) ? $taid : '';

                                $new_trans[] = array(
                                    'btid' => $db->insert_id(),
                                    'bttype' => $bttype,
                                    'taid' => $taid
                                );
                            }
                        }

                        //-- Check Port Clearance Detail
                        $s = 'SELECT a.pcid, c.bddate, c.rid
                              FROM ticket_port_clearance_detail AS a
                              LEFT JOIN ticket_booking_passenger AS b ON a.bpid = b.bpid
                              LEFT JOIN ticket_booking_detail AS c ON b.bdid = c.bdid
                              WHERE c.bid = %d AND c.bdtype = %s  GROUP BY c.bdid';
                        $q = $db->prepare_query( $s, $bid, $bdtype );
                        $r = $db->do_query( $q );

                        if( is_array( $r ) )
                        {
                            $retval = 0;
                        }
                        else
                        {
                            $d = $db->fetch_array( $r );

                            if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $d[ 'rid' ] == $bdparam[ 'rid' ] )
                            {
                                foreach( $new_pass as $np )
                                {
                                    $guest_age     = empty( $np['bpbirthdate'] ) ? 0 : date_diff( date_create( $np['bpbirthdate'] ), date_create( date( 'Y-m-d' ) ) )->format( '%y' );
                                    $guest_country = get_nationality( $np['lcountry_id'], 'lcountry_code' );
                                    $guest_gender  = $np['bpgender'] == 1 ? 'Male' : 'Female';

                                    $s2 = 'INSERT INTO ticket_port_clearance_detail( pcid, bpid, lcountry_id, guest_name, guest_type, guest_country, guest_gender, guest_age ) VALUES( %d, %d, %d, %s, %s, %s, %s, %s )';
                                    $q2 = $db->prepare_query( $s2, $d['pcid'], $np['bpid'], $np['lcountry_id'], $np['bpname'], $np['bptype'], $guest_country, $guest_gender, $guest_age );
                                    $r2 = $db->do_query( $q2 );

                                    if( is_array( $r2 ) )
                                    {
                                        $retval = 0;
                                    }
                                }
                            }
                        }

                        //-- Check Trip Transport Detail
                        $s = 'SELECT * FROM ticket_trip_transport AS a
                              LEFT JOIN ticket_trip_transport_detail AS b ON b.ttid = a.ttid
                              LEFT JOIN ticket_booking_transport AS c ON b.btid = c.btid
                              LEFT JOIN ticket_booking_detail AS d ON c.bdid = d.bdid
                              WHERE d.bid = %d AND d.bdtype = %s  GROUP BY d.bdid';
                        $q = $db->prepare_query( $s, $bid, $bdtype );
                        $r = $db->do_query( $q );

                        if( is_array( $r ) )
                        {
                            $retval = 0;
                        }
                        else
                        {
                            $d = $db->fetch_array( $r );

                            $ttlocation = $d['ttdtype'] == 'pickup' ? $bdparam['bdfrom'] : $bdparam['bdto'];
                            $tttime     = strtotime( $bdparam[ 'bddeparttime' ] );
                            $bddeptime  = strtotime( $d[ 'tttime' ] );

                            if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $tttime == $bddeptime && $d[ 'ttlocation' ] == $ttlocation )
                            {
                                foreach( $new_trans as $nt )
                                {
                                    if( $d['taid'] == $nt['taid'] )
                                    {
                                        $s2 = 'INSERT INTO ticket_trip_transport_detail( ttid, btid, ttdtype ) VALUES( %d, %d, %s )';
                                        $q2 = $db->prepare_query( $s2, $d['ttid'], $nt['btid'], $nt['bttype'] );
                                        $r2 = $db->do_query( $q2 );

                                        if( is_array( $r2 ) )
                                        {
                                            $retval = 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //-- Update Booking Table Set Old Booking To "ar" Status
    $s = 'UPDATE ticket_booking SET bstt = %s WHERE bid = %d';
    $q = $db->prepare_query( $s, 'ar', $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        $retval = 0;
    }
    else
    {
        //-- Update New Booking Data
        $nbtotal = $nbsubtotal - $data[ 'bdiscount' ];

        $s2 = 'UPDATE ticket_booking SET brcode = %d, bsubtotal = %s, btotal = %s WHERE bid = %d';
        $q2 = $db->prepare_query( $s2, $bid, $nbsubtotal, $nbtotal, $new_bid );
        $r2 = $db->do_query( $q2 );

        if( is_array( $r2 ) )
        {
            $retval = 0;
        }
        else
        {
            //-- Select Boking Payment Data
            $s = 'SELECT * FROM ticket_booking_payment WHERE bid = %d';
            $q = $db->prepare_query( $s, $bid );
            $r = $db->do_query( $q );

            if( !is_array( $r ) )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    unset( $d[ 'pid' ] );

                    $d[ 'bid' ] = $new_bid;

                    //-- Insert New Payment Data
                    $q2 = 'INSERT INTO ticket_booking_payment(' . implode( ',', array_keys( $d ) ) . ') VALUES ("' . implode( '" , "', $d ) . '")';
                    $r2 = $db->do_query( $q2 );

                    if( is_array( $r2 ) )
                    {
                        $retval = 0;
                    }
                }
            }

            //-- Select Agent Transaction Data
            $s = 'SELECT * FROM ticket_agent_transaction WHERE bcode = %s';
            $q = $db->prepare_query( $s, $old_bcode );
            $r = $db->do_query( $q );

            if( !is_array( $r ) )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    unset( $d[ 'atid' ] );

                    //-- Update Outstanding Value
                    if( $d['atstatus'] == 1 )
                    {
                        $d[ 'atdebet' ] = $nbtotal;
                    }

                    $d[ 'bcode' ] = $new_bcode;
                    $d[ 'bid' ]   = $new_bid;

                    $q2 = 'INSERT INTO ticket_agent_transaction(' . implode( ',', array_keys( $d ) ) . ') VALUES ("' . implode( '" , "', $d ) . '")';
                    $r2 = $db->do_query( $q2 );

                    if( is_array( $r2 ) )
                    {
                        $retval = 0;
                    }
                }
            }
        }
    }

    if( $retval == 0 )
    {
        $db->rollback();

        return false;
    }
    else
    {
        $db->commit();

        return true;
    }
}

function ticket_booking_revise_trip_data( $post, $data, $bid )
{
    global $db;

    $retval = 1;

    $db->begin();

    if( isset( $post[ 'booked' ] ) )
    {
        $bparam = array();

        $nbsubtotal = 0;
        $old_bcode  = $post[ 'booked' ][ 'bcode' ];
        $new_bcode  = generate_code_number( $post[ 'booked' ][ 'btype' ] );

        $pass_exc   = array( 'bpid' );
        $detail_exc = array( 'bdid', 'rname', 'rstatus', 'rdepart', 'rarrive', 'rcot', 'rtype', 'rhoppingpoint', 'passenger', 'transport' );
        $trans_exc  = array( 'btid', 'hname', 'haddress', 'hphone', 'hphone2', 'hfax', 'hemail', 'hstatus', 'hcreateddate', 'luser_id', 'taname', 'tacreateddate', 'taairport', 'tatypeforagent', 'tatypeforonline' );

        foreach( $post[ 'booked' ] as $field => $val )
        {
            if( $val != '' && $val != '0000-00-00 00:00:00' )
            {
                if( $field == 'bcode' )
                {
                    $bparam[ $field ] = $new_bcode;
                }
                else
                {
                    $bparam[ $field ] = $val;
                }
            }
        }

        if( !empty( $bparam ) )
        {
            //-- Insert New Booking Data
            $q = 'INSERT INTO ticket_booking(' . implode( ',', array_keys( $bparam ) ) . ') VALUES ("' . implode( '" , "', $bparam ) . '")';
            $r = $db->do_query( $q );

            if( is_array( $r ) )
            {
                $retval = 0;
            }
            else
            {
                $new_bid = $db->insert_id();

                //-- Prepare Insert Old Detail Data <> Post Data
                list( $bdid, $bdtype ) = explode( '|', $post['bdidval'] );

                foreach( $data['detail'] as $dt_bdtype => $detail )
                {
                    foreach( $detail as $dt_bdid => $dt )
                    {
                        if( $bdid != $dt_bdid )
                        {
                            $bdparam = array();

                            $dt[ 'bid' ] = $new_bid;

                            foreach( $dt as $field => $val )
                            {
                                if( $val != '' && !in_array( $field, $detail_exc ) )
                                {
                                    $bdparam[ $field ] = $val;
                                }
                            }

                            if( isset( $bdparam[ 'total' ] ) )
                            {
                                $nbsubtotal += $bdparam[ 'total' ];
                            }

                            if( !empty( $bdparam ) )
                            {
                                //-- Insert New Booking Detail Data
                                //-- Base On Old Booking Detail Data
                                $q = 'INSERT INTO ticket_booking_detail(' . implode( ',', array_keys( $bdparam ) ) . ') VALUES ("' . implode( '" , "', $bdparam ) . '")';
                                $r = $db->do_query( $q );

                                if( is_array( $r ) )
                                {
                                    $retval = 0;
                                }
                                else
                                {
                                    $new_bdid = $db->insert_id();
                                    $new_pass = array();

                                    foreach( $data[ 'detail' ][ $dt_bdtype ][ $dt_bdid ][ 'passenger' ] as $bptype => $passenger )
                                    {
                                        foreach( $passenger as $obj )
                                        {
                                            $obj[ 'bdid' ]   = $new_bdid;
                                            $obj[ 'bptype' ] = $bptype;

                                            $bpparam = array();

                                            foreach( $obj as $field => $val )
                                            {
                                                if( $val != '' && $val != '0000-00-00' && !in_array( $field, $pass_exc ) )
                                                {
                                                    $bpparam[ $field ] = $val;
                                                }
                                            }

                                            if( !empty( $bpparam ) )
                                            {
                                                //-- Insert New Booking Passenger Data
                                                //-- Base On Old Booking Data
                                                $q = 'INSERT INTO ticket_booking_passenger(' . implode( ',', array_keys( $bpparam ) ) . ') VALUES ("' . implode( '" , "', $bpparam ) . '")';
                                                $r = $db->do_query( $q );

                                                if( is_array( $r ) )
                                                {
                                                    $retval = 0;
                                                }
                                                else
                                                {
                                                    $bpparam[ 'bpid' ] = $db->insert_id();

                                                    $new_pass[] = $bpparam;
                                                }
                                            }
                                        }
                                    }

                                    $new_trans = array();

                                    foreach( $data[ 'detail' ][ $dt_bdtype ][ $dt_bdid ][ 'transport' ] as $bttype => $transport )
                                    {
                                        foreach( $transport as $obj )
                                        {
                                            $obj[ 'bdid' ] = $new_bdid;

                                            $btparam = array();

                                            foreach( $obj as $field => $val )
                                            {
                                                if( $val != '' && $val != '0000-00-00 00:00:00' && !in_array( $field, $trans_exc ) )
                                                {
                                                    $btparam[ $field ] = $val;
                                                }
                                            }

                                            //-- Insert New Booking Transport Data
                                            //-- Base On Old Booking Data
                                            $q = 'INSERT INTO ticket_booking_transport(' . implode( ',', array_keys( $btparam ) ) . ') VALUES ("' . implode( '" , "', $btparam ) . '")';
                                            $r = $db->do_query( $q );

                                            if( is_array( $r ) )
                                            {
                                                $retval = 0;
                                            }
                                            else
                                            {
                                                $taid = isset( $obj['taid'] ) ? $obj['taid'] : '';

                                                $new_trans[] = array(
                                                    'btid' => $db->insert_id(),
                                                    'bttype' => $bttype,
                                                    'taid' => $taid
                                                );
                                            }
                                        }
                                    }

                                    //-- Check Port Clearance Detail
                                    $s = 'SELECT a.pcid, c.bddate, c.rid
                                          FROM ticket_port_clearance_detail AS a
                                          LEFT JOIN ticket_booking_passenger AS b ON a.bpid = b.bpid
                                          LEFT JOIN ticket_booking_detail AS c ON b.bdid = c.bdid
                                          WHERE c.bid = %d AND c.bdtype = %s  GROUP BY c.bdid';
                                    $q = $db->prepare_query( $s, $bid, $bdtype );
                                    $r = $db->do_query( $q );

                                    if( is_array( $r ) )
                                    {
                                        $retval = 0;
                                    }
                                    else
                                    {
                                        $d = $db->fetch_array( $r );

                                        if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $d[ 'rid' ] == $bdparam[ 'rid' ] )
                                        {
                                            foreach( $new_pass as $np )
                                            {
                                                $guest_age     = empty( $np['bpbirthdate'] ) ? 0 : date_diff( date_create( $np['bpbirthdate'] ), date_create( date( 'Y-m-d' ) ) )->format( '%y' );
                                                $guest_country = get_nationality( $np['lcountry_id'], 'lcountry_code' );
                                                $guest_gender  = $np['bpgender'] == 1 ? 'Male' : 'Female';

                                                $s2 = 'INSERT INTO ticket_port_clearance_detail( pcid, bpid, lcountry_id, guest_name, guest_type, guest_country, guest_gender, guest_age ) VALUES( %d, %d, %d, %s, %s, %s, %s, %s )';
                                                $q2 = $db->prepare_query( $s2, $d['pcid'], $np['bpid'], $np['lcountry_id'], $np['bpname'], $np['bptype'], $guest_country, $guest_gender, $guest_age );
                                                $r2 = $db->do_query( $q2 );

                                                if( is_array( $r2 ) )
                                                {
                                                    $retval = 0;
                                                }
                                            }
                                        }
                                    }

                                    //-- Check Trip Transport Detail
                                    $s = 'SELECT * FROM ticket_trip_transport AS a
                                          LEFT JOIN ticket_trip_transport_detail AS b ON b.ttid = a.ttid
                                          LEFT JOIN ticket_booking_transport AS c ON b.btid = c.btid
                                          LEFT JOIN ticket_booking_detail AS d ON c.bdid = d.bdid
                                          WHERE d.bid = %d AND d.bdtype = %s  GROUP BY d.bdid';
                                    $q = $db->prepare_query( $s, $bid, $bdtype );
                                    $r = $db->do_query( $q );

                                    if( is_array( $r ) )
                                    {
                                        $retval = 0;
                                    }
                                    else
                                    {
                                        $d = $db->fetch_array( $r );

                                        $ttlocation = $d['ttdtype'] == 'pickup' ? $bdparam['bdfrom'] : $bdparam['bdto'];
                                        $tttime     = strtotime( $bdparam[ 'bddeparttime' ] );
                                        $bddeptime  = strtotime( $d[ 'tttime' ] );

                                        if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $tttime == $bddeptime && $d[ 'ttlocation' ] == $ttlocation )
                                        {
                                            foreach( $new_trans as $nt )
                                            {
                                                if( $d['taid'] == $nt['taid'] )
                                                {
                                                    $s2 = 'INSERT INTO ticket_trip_transport_detail( ttid, btid, ttdtype ) VALUES( %d, %d, %s )';
                                                    $q2 = $db->prepare_query( $s2, $d['ttid'], $nt['btid'], $nt['bttype'] );
                                                    $r2 = $db->do_query( $q2 );

                                                    if( is_array( $r2 ) )
                                                    {
                                                        $retval = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //-- Prepare Insert New Detail Data
                //-- Base On Post Data
                foreach( $post[ 'booked_detail' ] as $bdtype => $detail )
                {
                    $bdparam  = array();
                    $data_key = $post[ $bdtype . '_trip_transport_val' ];

                    $detail[ $data_key ][ 'bid' ]           = $new_bid;
                    $detail[ $data_key ][ 'transport_fee' ] = array_sum( array_column( $post[ 'transport' ][ $bdtype ][ $data_key ], 'bttrans_fee' ) );
                    $detail[ $data_key ][ 'total' ]         = $detail[ $data_key ][ 'subtotal' ] + $detail[ $data_key ][ 'transport_fee' ] - $detail[ $data_key ][ 'discount' ];

                    foreach( $detail[ $data_key ] as $field => $val )
                    {
                        if( $val != '' )
                        {
                            $bdparam[ $field ] = $val;
                        }
                    }

                    if( isset( $bdparam[ 'total' ] ) )
                    {
                        $nbsubtotal += $bdparam[ 'total' ];
                    }

                    if( !empty( $bdparam ) )
                    {
                        //-- Insert New Booking Detail Data
                        //-- Base On Post Data
                        $q = 'INSERT INTO ticket_booking_detail(' . implode( ',', array_keys( $bdparam ) ) . ') VALUES ("' . implode( '" , "', $bdparam ) . '")';
                        $r = $db->do_query( $q );

                        if( is_array( $r ) )
                        {
                            $retval = 0;
                        }
                        else
                        {
                            $new_bdid = $db->insert_id();
                            $new_pass = array();

                            foreach( $post[ 'passenger' ][ $bdtype ] as $bptype => $passenger )
                            {
                                foreach( $passenger as $obj )
                                {
                                    $obj[ 'bdid' ]   = $new_bdid;
                                    $obj[ 'bptype' ] = $bptype;

                                    unset( $obj[ 'bpid' ] );

                                    $bpparam = array();

                                    foreach( $obj as $field => $val )
                                    {
                                        if( $val != '' )
                                        {
                                            if( $field == 'bpbirthdate' )
                                            {
                                                $val = date( 'Y-m-d', strtotime( $val ) );
                                            }

                                            $bpparam[ $field ] = $val;
                                        }
                                    }

                                    if( !empty( $bpparam ) )
                                    {
                                        //-- Insert New Booking Passenger Data
                                        //-- Base On Post Data
                                        $q = 'INSERT INTO ticket_booking_passenger(' . implode( ',', array_keys( $bpparam ) ) . ') VALUES ("' . implode( '" , "', $bpparam ) . '")';
                                        $r = $db->do_query( $q );

                                        if( is_array( $r ) )
                                        {
                                            $retval = 0;
                                        }
                                        else
                                        {
                                            $bpparam[ 'bpid' ] = $db->insert_id();

                                            $new_pass[] = $bpparam;
                                        }
                                    }
                                }
                            }

                            $new_trans = array();

                            foreach( $post[ 'transport' ][ $bdtype ][ $data_key ] as $bttype => $obj )
                            {
                                if( $obj['bttrans_type'] == 2 )
                                {
                                    $s = 'INSERT INTO ticket_booking_transport( bdid, bttype, bttrans_type, btdrivername, btdriverphone, bttrans_fee ) VALUES( %d, %s, %s, %s, %s, %s )';
                                    $q = $db->prepare_query( $s, $new_bdid, $bttype, $obj['bttrans_type'], $obj['btdrivername'], $obj['btdriverphone'], $obj['bttrans_fee'] );
                                }
                                else
                                {
                                    if( empty( $obj['taid'] ) || empty( $obj['hid'] ) )
                                    {
                                        $s = 'INSERT INTO ticket_booking_transport( bdid, bttype, bttrans_type, bttrans_fee ) VALUES( %d, %s, %s, %s )';
                                        $q = $db->prepare_query( $s, $new_bdid, $bttype, $obj['bttrans_type'], $obj['bttrans_fee'] );
                                    }
                                    else
                                    {
                                        if( $obj['hid'] == 1 )
                                        {
                                            $bthotelname       = isset( $obj['bthotelname'] ) ? $obj['bthotelname'] : '';
                                            $bthotelphone      = isset( $obj['bthotelphone'] ) ? $obj['bthotelphone'] : '';
                                            $bthotelemail      = isset( $obj['bthotelemail'] ) ? $obj['bthotelemail'] : '';
                                            $bthoteladdress    = isset( $obj['bthoteladdress'] ) ? $obj['bthoteladdress'] : '';
                                            $bthotelroomnumber = isset( $obj['bthotelroomnumber'] ) ? $obj['bthotelroomnumber'] : '';
                                        }
                                        else
                                        {
                                            $h = get_hotel( $obj['hid'] );

                                            $bthotelname       = $h['hname'];
                                            $bthotelphone      = $h['hphone'];
                                            $bthotelemail      = $h['hemail'];
                                            $bthoteladdress    = $h['haddress'];
                                            $bthotelroomnumber = '';
                                        }

                                        list( $taid, $btrpfrom, $btrpto ) = explode( '|', $obj['taid'] );

                                        $btflighttime = isset( $obj['btflighttime'] ) ? ( empty( $obj['btflighttime'] ) ? '' : date( 'Y-m-d H:i:s', strtotime( $obj['btflighttime'] ) ) ) : '';

                                        //-- Insert New Booking Transport Data
                                        //-- Base On Post Data
                                        $btrans = array(
                                            'bdid' => $new_bdid,
                                            'hid' => $obj['hid'],
                                            'taid' => $taid,
                                            'bttype' => $bttype,
                                            'bttrans_type' => $obj['bttrans_type'],
                                            'bthotelname' => $bthotelname,
                                            'bthoteladdress' => $bthoteladdress,
                                            'bthotelphone' => $bthotelphone,
                                            'bthotelemail' => $bthotelemail,
                                            'bthotelroomnumber' => $bthotelroomnumber,
                                            'btflighttime' => $btflighttime,
                                            'btrpfrom' => $btrpfrom,
                                            'btrpto' => $btrpto,
                                            'bttrans_fee' => $obj['bttrans_fee']
                                        );

                                        if( empty( $btflighttime ) )
                                        {
                                            unset( $btrans['btflighttime'] );
                                        }

                                        if( empty( $taid ) )
                                        {
                                            unset( $btrans['taid'] );
                                        }

                                        $q = 'INSERT INTO ticket_booking_transport(' . implode( ',', array_keys( $btrans ) ) . ') VALUES ("' . implode( '" , "', $btrans ) . '")';
                                    }
                                }

                                $r = $db->do_query( $q );

                                if( is_array( $r ) )
                                {
                                    $retval = 0;
                                }
                                else
                                {
                                    $taid = isset( $taid ) ? $taid : '';

                                    $new_trans[] = array(
                                        'btid' => $db->insert_id(),
                                        'bttype' => $bttype,
                                        'taid' => $taid
                                    );
                                }
                            }

                            //-- Check Port Clearance Detail
                            $s = 'SELECT a.pcid, c.bddate, c.rid
                                  FROM ticket_port_clearance_detail AS a
                                  LEFT JOIN ticket_booking_passenger AS b ON a.bpid = b.bpid
                                  LEFT JOIN ticket_booking_detail AS c ON b.bdid = c.bdid
                                  WHERE c.bid = %d AND c.bdtype = %s  GROUP BY c.bdid';
                            $q = $db->prepare_query( $s, $bid, $bdtype );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $d[ 'rid' ] == $bdparam[ 'rid' ] )
                                {
                                    foreach( $new_pass as $np )
                                    {
                                        $guest_age     = empty( $np['bpbirthdate'] ) ? 0 : date_diff( date_create( $np['bpbirthdate'] ), date_create( date( 'Y-m-d' ) ) )->format( '%y' );
                                        $guest_country = get_nationality( $np['lcountry_id'], 'lcountry_code' );
                                        $guest_gender  = $np['bpgender'] == 1 ? 'Male' : 'Female';

                                        $s2 = 'INSERT INTO ticket_port_clearance_detail( pcid, bpid, lcountry_id, guest_name, guest_type, guest_country, guest_gender, guest_age ) VALUES( %d, %d, %d, %s, %s, %s, %s, %s )';
                                        $q2 = $db->prepare_query( $s2, $d['pcid'], $np['bpid'], $np['lcountry_id'], $np['bpname'], $np['bptype'], $guest_country, $guest_gender, $guest_age );
                                        $r2 = $db->do_query( $q2 );

                                        if( is_array( $r2 ) )
                                        {
                                            $retval = 0;
                                        }
                                    }
                                }
                            }

                            //-- Check Trip Transport Detail
                            $s = 'SELECT * FROM ticket_trip_transport AS a
                                  LEFT JOIN ticket_trip_transport_detail AS b ON b.ttid = a.ttid
                                  LEFT JOIN ticket_booking_transport AS c ON b.btid = c.btid
                                  LEFT JOIN ticket_booking_detail AS d ON c.bdid = d.bdid
                                  WHERE d.bid = %d AND d.bdtype = %s  GROUP BY d.bdid';
                            $q = $db->prepare_query( $s, $bid, $bdtype );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                $ttlocation = $d['ttdtype'] == 'pickup' ? $bdparam['bdfrom'] : $bdparam['bdto'];
                                $tttime     = strtotime( $bdparam[ 'bddeparttime' ] );
                                $bddeptime  = strtotime( $d[ 'tttime' ] );

                                if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $tttime == $bddeptime && $d[ 'ttlocation' ] == $ttlocation )
                                {
                                    foreach( $new_trans as $nt )
                                    {
                                        if( $d['taid'] == $nt['taid'] )
                                        {
                                            $s2 = 'INSERT INTO ticket_trip_transport_detail( ttid, btid, ttdtype ) VALUES( %d, %d, %s )';
                                            $q2 = $db->prepare_query( $s2, $d['ttid'], $nt['btid'], $nt['bttype'] );
                                            $r2 = $db->do_query( $q2 );

                                            if( is_array( $r2 ) )
                                            {
                                                $retval = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //-- Update Booking Table Set Old Booking To "ar" Status
    $s = 'UPDATE ticket_booking SET bstt = %s WHERE bid = %d';
    $q = $db->prepare_query( $s, 'ar', $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        $retval = 0;
    }
    else
    {
        //-- Update New Booking Data
        $nbtotal = $nbsubtotal - $data[ 'bdiscount' ];

        $s2 = 'UPDATE ticket_booking SET brcode = %d, bsubtotal = %s, btotal = %s WHERE bid = %d';
        $q2 = $db->prepare_query( $s2, $bid, $nbsubtotal, $nbtotal, $new_bid );
        $r2 = $db->do_query( $q2 );

        if( is_array( $r2 ) )
        {
            $retval = 0;
        }
        else
        {
            //-- Select Boking Payment Data
            $s = 'SELECT * FROM ticket_booking_payment WHERE bid = %d';
            $q = $db->prepare_query( $s, $bid );
            $r = $db->do_query( $q );

            if( !is_array( $r ) )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    unset( $d[ 'pid' ] );

                    $d[ 'bid' ] = $new_bid;

                    //-- Insert New Payment Data
                    $q2 = 'INSERT INTO ticket_booking_payment(' . implode( ',', array_keys( $d ) ) . ') VALUES ("' . implode( '" , "', $d ) . '")';
                    $r2 = $db->do_query( $q2 );

                    if( is_array( $r2 ) )
                    {
                        $retval = 0;
                    }
                }
            }

            //-- Select Agent Transaction Data
            $s = 'SELECT * FROM ticket_agent_transaction WHERE bcode = %s';
            $q = $db->prepare_query( $s, $old_bcode );
            $r = $db->do_query( $q );

            if( !is_array( $r ) )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    unset( $d[ 'atid' ] );

                    //-- Update Outstanding Value
                    if( $d['atstatus'] == 1 )
                    {
                        $d[ 'atdebet' ] = $nbtotal;
                    }

                    $d[ 'bcode' ] = $new_bcode;
                    $d[ 'bid' ]   = $new_bid;

                    $q2 = 'INSERT INTO ticket_agent_transaction(' . implode( ',', array_keys( $d ) ) . ') VALUES ("' . implode( '" , "', $d ) . '")';
                    $r2 = $db->do_query( $q2 );

                    if( is_array( $r2 ) )
                    {
                        $retval = 0;
                    }
                }
            }
        }
    }

    if( $retval == 0 )
    {
        $db->rollback();

        return '';
    }
    else
    {
        $db->commit();

        return $new_bid;
    }
}

function ticket_booking_revise_data( $post, $data, $bid )
{
    global $db;

    $retval = 1;

    $db->begin();

    if( isset( $post[ 'booked' ] ) )
    {
        $bparam = array();

        $nbsubtotal = 0;
        $old_bcode  = $post[ 'booked' ][ 'bcode' ];
        $new_bcode  = generate_code_number( $post[ 'booked' ][ 'btype' ] );

        foreach( $post[ 'booked' ] as $field => $val )
        {
            if( $val != '' )
            {
                if( in_array( $field, array( 'brcdate', 'bcdate' ) ) && $val == '0000-00-00 00:00:00' )
                {
                    continue;
                }

                if( $field == 'bcode' )
                {
                    $bparam[ $field ] = $new_bcode;
                }
                else
                {
                    $bparam[ $field ] = $val;
                }
            }
        }

        if( !empty( $bparam ) )
        {
            //-- Insert New Booking Data
            $q = 'INSERT INTO ticket_booking(' . implode( ',', array_keys( $bparam ) ) . ') VALUES ("' . implode( '" , "', $bparam ) . '")';
            $r = $db->do_query( $q );

            if( is_array( $r ) )
            {
                $retval = 0;
            }
            else
            {
                $new_bid = $db->insert_id();

                foreach( $post[ 'booked_detail' ] as $bdtype => $detail )
                {
                    $bdparam  = array();
                    $data_key = $post[ $bdtype . '_trip_transport_val' ];

                    $detail[ $data_key ][ 'bid' ]           = $new_bid;
                    $detail[ $data_key ][ 'transport_fee' ] = array_sum( array_column( $post[ 'transport' ][ $bdtype ][ $data_key ], 'bttrans_fee' ) );
                    $detail[ $data_key ][ 'total' ]         = $detail[ $data_key ][ 'subtotal' ] + $detail[ $data_key ][ 'transport_fee' ] - $detail[ $data_key ][ 'discount' ];

                    foreach( $detail[ $data_key ] as $field => $val )
                    {
                        if( $val != '' )
                        {
                            $bdparam[ $field ] = $val;
                        }
                    }

                    if( isset( $bdparam[ 'total' ] ) )
                    {
                        $nbsubtotal += $bdparam[ 'total' ];
                    }

                    if( !empty( $bdparam ) )
                    {
                        //-- Insert New Booking Detail Data
                        $q = 'INSERT INTO ticket_booking_detail(' . implode( ',', array_keys( $bdparam ) ) . ') VALUES ("' . implode( '" , "', $bdparam ) . '")';
                        $r = $db->do_query( $q );

                        if( is_array( $r ) )
                        {
                            $retval = 0;
                        }
                        else
                        {
                            $new_bdid = $db->insert_id();
                            $new_pass = array();

                            foreach( $post[ 'passenger' ][ $bdtype ] as $bptype => $passenger )
                            {
                                foreach( $passenger as $obj )
                                {
                                    if ( isset( $obj['bpstatus'] ) && $obj['bpstatus'] == 'on' )
                                    {
                                        $obj[ 'bdid' ]   = $new_bdid;
                                        $obj[ 'bptype' ] = $bptype;

                                        $bpparam = array();

                                        foreach( $obj as $field => $val )
                                        {
                                            if( $val != '' )
                                            {
                                                if ( $field == 'bpbirthdate' )
                                                {
                                                    $val = date( 'Y-m-d', strtotime( $val ) );
                                                }
                                                elseif ( $field == 'bpstatus' )
                                                {
                                                    $val = ( $val == 'on' ) ? 'aa' : $val;
                                                }
                                                else
                                                {
                                                    $val = $val;
                                                }

                                                $bpparam[ $field ] = $val;
                                            }
                                        }

                                        if( !empty( $bpparam ) )
                                        {
                                            //-- Insert New Booking Passenger Data
                                            $q = 'INSERT INTO ticket_booking_passenger(' . implode( ',', array_keys( $bpparam ) ) . ') VALUES ("' . implode( '" , "', $bpparam ) . '")';
                                            $r = $db->do_query( $q );

                                            if( is_array( $r ) )
                                            {
                                                $retval = 0;
                                            }
                                            else
                                            {
                                                $bpparam[ 'bpid' ] = $db->insert_id();

                                                $new_pass[] = $bpparam;
                                            }
                                        }
                                    }
                                }
                            }

                            $new_trans = array();

                            foreach( $post[ 'transport' ][ $bdtype ][ $data_key ] as $bttype => $obj )
                            {
                                if( $obj['bttrans_type'] == 2 )
                                {
                                    $s = 'INSERT INTO ticket_booking_transport(
                                            bdid,
                                            bttype,
                                            bttrans_type,
                                            btdrivername,
                                            btdriverphone,
                                            bttrans_fee ) VALUES( %d, %s, %s, %s, %s, %s )';
                                    $q = $db->prepare_query( $s, $new_bdid, $bttype, $obj['bttrans_type'], $obj['btdrivername'], $obj['btdriverphone'], $obj['bttrans_fee'] );
                                }
                                else
                                {
                                    if( empty( $obj['taid'] ) || empty( $obj['hid'] ) )
                                    {
                                        $s = 'INSERT INTO ticket_booking_transport(
                                                bdid,
                                                bttype,
                                                bttrans_type,
                                                bttrans_fee ) VALUES( %d, %s, %s, %s )';
                                        $q = $db->prepare_query( $s, $new_bdid, $bttype, $obj['bttrans_type'], $obj['bttrans_fee'] );
                                    }
                                    else
                                    {
                                        if( $obj['hid'] == 1 )
                                        {
                                            $bthotelname       = isset( $obj['bthotelname'] ) ? $obj['bthotelname'] : '';
                                            $bthotelphone      = isset( $obj['bthotelphone'] ) ? $obj['bthotelphone'] : '';
                                            $bthotelemail      = isset( $obj['bthotelemail'] ) ? $obj['bthotelemail'] : '';
                                            $bthoteladdress    = isset( $obj['bthoteladdress'] ) ? $obj['bthoteladdress'] : '';
                                            $bthotelroomnumber = isset( $obj['bthotelroomnumber'] ) ? $obj['bthotelroomnumber'] : '';
                                        }
                                        else
                                        {
                                            $h = get_hotel( $obj['hid'] );

                                            $bthotelname       = $h['hname'];
                                            $bthotelphone      = $h['hphone'];
                                            $bthotelemail      = $h['hemail'];
                                            $bthoteladdress    = $h['haddress'];
                                            $bthotelroomnumber = '';
                                        }

                                        list( $taid, $btrpfrom, $btrpto ) = explode( '|', $obj['taid'] );

                                        $btflighttime = isset( $obj['btflighttime'] ) ? ( empty( $obj['btflighttime'] ) ? '' : date( 'Y-m-d H:i:s', strtotime( $obj['btflighttime'] ) ) ) : '';

                                        //-- Insert New Booking Transport Data
                                        //-- Base On Post Data
                                        $btrans = array(
                                            'bdid' => $new_bdid,
                                            'hid' => $obj['hid'],
                                            'taid' => $taid,
                                            'bttype' => $bttype,
                                            'bttrans_type' => $obj['bttrans_type'],
                                            'bthotelname' => $bthotelname,
                                            'bthoteladdress' => $bthoteladdress,
                                            'bthotelphone' => $bthotelphone,
                                            'bthotelemail' => $bthotelemail,
                                            'bthotelroomnumber' => $bthotelroomnumber,
                                            'btflighttime' => $btflighttime,
                                            'btrpfrom' => $btrpfrom,
                                            'btrpto' => $btrpto,
                                            'bttrans_fee' => $obj['bttrans_fee']
                                        );

                                        if( empty( $btflighttime ) )
                                        {
                                            unset( $btrans['btflighttime'] );
                                        }

                                        if( empty( $taid ) )
                                        {
                                            unset( $btrans['taid'] );
                                        }

                                        $q = 'INSERT INTO ticket_booking_transport(' . implode( ',', array_keys( $btrans ) ) . ') VALUES ("' . implode( '" , "', $btrans ) . '")';
                                    }
                                }

                                $r = $db->do_query( $q );

                                if( is_array( $r ) )
                                {
                                    $retval = 0;
                                }
                                else
                                {
                                    $taid = isset( $taid ) ? $taid : '';

                                    $new_trans[] = array(
                                        'btid' => $db->insert_id(),
                                        'bttype' => $bttype,
                                        'taid' => $taid
                                    );
                                }
                            }

                            //-- Check Port Clearance Detail
                            $s = 'SELECT a.pcid, c.bddate, c.rid
                                  FROM ticket_port_clearance_detail AS a
                                  LEFT JOIN ticket_booking_passenger AS b ON a.bpid = b.bpid
                                  LEFT JOIN ticket_booking_detail AS c ON b.bdid = c.bdid
                                  WHERE c.bid = %d AND c.bdtype = %s  GROUP BY c.bdid, a.pcid';
                            $q = $db->prepare_query( $s, $bid, $bdtype );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $d[ 'rid' ] == $bdparam[ 'rid' ] )
                                {
                                    foreach( $new_pass as $np )
                                    {
                                        $guest_age     = empty( $np['bpbirthdate'] ) ? 0 : date_diff( date_create( $np['bpbirthdate'] ), date_create( date( 'Y-m-d' ) ) )->format( '%y' );
                                        $guest_country = get_nationality( $np['lcountry_id'], 'lcountry_code' );
                                        $guest_gender  = $np['bpgender'] == 1 ? 'Male' : 'Female';

                                        $s2 = 'INSERT INTO ticket_port_clearance_detail( pcid, bpid, lcountry_id, guest_name, guest_type, guest_country, guest_gender, guest_age ) VALUES( %d, %d, %d, %s, %s, %s, %s, %s )';
                                        $q2 = $db->prepare_query( $s2, $d['pcid'], $np['bpid'], $np['lcountry_id'], $np['bpname'], $np['bptype'], $guest_country, $guest_gender, $guest_age );
                                        $r2 = $db->do_query( $q2 );

                                        if( is_array( $r2 ) )
                                        {
                                            $retval = 0;
                                        }
                                    }
                                }
                            }

                            //-- Check Trip Transport Detail
                            $s = 'SELECT * FROM ticket_trip_transport AS a
                                  LEFT JOIN ticket_trip_transport_detail AS b ON b.ttid = a.ttid
                                  LEFT JOIN ticket_booking_transport AS c ON b.btid = c.btid
                                  LEFT JOIN ticket_booking_detail AS d ON c.bdid = d.bdid
                                  WHERE d.bid = %d AND d.bdtype = %s  GROUP BY d.bdid, a.ttid, b.ttdid';
                            $q = $db->prepare_query( $s, $bid, $bdtype );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                $ttlocation = $d['ttdtype'] == 'pickup' ? $bdparam['bdfrom'] : $bdparam['bdto'];
                                $tttime     = strtotime( $bdparam[ 'bddeparttime' ] );
                                $bddeptime  = strtotime( $d[ 'tttime' ] );

                                if( $d[ 'bddate' ] == $bdparam[ 'bddate' ] && $tttime == $bddeptime && $d[ 'ttlocation' ] == $ttlocation )
                                {
                                    foreach( $new_trans as $nt )
                                    {
                                        if( $d['taid'] == $nt['taid'] )
                                        {
                                            $s2 = 'INSERT INTO ticket_trip_transport_detail( ttid, btid, ttdtype ) VALUES( %d, %d, %s )';
                                            $q2 = $db->prepare_query( $s2, $d['ttid'], $nt['btid'], $nt['bttype'] );
                                            $r2 = $db->do_query( $q2 );

                                            if( is_array( $r2 ) )
                                            {
                                                $retval = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //-- Update Booking Table Set Old Booking To "ar" Status
        $s = 'UPDATE ticket_booking SET bstt = %s WHERE bid = %d';
        $q = $db->prepare_query( $s, 'ar', $bid );
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            $retval = 0;
        }
        else
        {
            $nbtotal = $nbsubtotal - $post[ 'booked' ][ 'bdiscount' ];

            //-- Update brcode, bsubtotal, btotal Of New Booking Data
            $s2 = 'UPDATE ticket_booking SET brcode = %d, bsubtotal = %s, btotal = %s WHERE bid = %d';
            $q2 = $db->prepare_query( $s2, $bid, $nbsubtotal, $nbtotal, $new_bid );
            $r2 = $db->do_query( $q2 );

            if( is_array( $r2 ) )
            {
                $retval = 0;
            }
            else
            {
                //-- Select Boking Payment Data
                $s = 'SELECT * FROM ticket_booking_payment WHERE bid = %d';
                $q = $db->prepare_query( $s, $bid );
                $r = $db->do_query( $q );

                if( !is_array( $r ) )
                {
                    while( $d = $db->fetch_array( $r ) )
                    {
                        unset( $d[ 'pid' ] );

                        $d[ 'bid' ] = $new_bid;

                        $q2 = 'INSERT INTO ticket_booking_payment(' . implode( ',', array_keys( $d ) ) . ') VALUES ("' . implode( '" , "', $d ) . '")';
                        $r2 = $db->do_query( $q2 );

                        if( is_array( $r2 ) )
                        {
                            $retval = 0;
                        }
                    }
                }

                //-- Select Agent Transaction Data
                $s = 'SELECT * FROM ticket_agent_transaction WHERE bcode = %s';
                $q = $db->prepare_query( $s, $old_bcode );
                $r = $db->do_query( $q );

                if( !is_array( $r ) )
                {
                    while( $d = $db->fetch_array( $r ) )
                    {
                        unset( $d[ 'atid' ] );

                        //-- Update Outstanding Value
                        if( $d['atstatus'] == 1 )
                        {
                            $d[ 'atdebet' ] = $nbtotal;
                        }

                        $d[ 'bcode' ] = $new_bcode;
                        $d[ 'bid' ]   = $new_bid;

                        $q2 = 'INSERT INTO ticket_agent_transaction(' . implode( ',', array_keys( $d ) ) . ') VALUES ("' . implode( '" , "', $d ) . '")';
                        $r2 = $db->do_query( $q2 );

                        if( is_array( $r2 ) )
                        {
                            $retval = 0;
                        }
                    }
                }
            }
        }
    }

    if( $retval == 0 )
    {
        $db->rollback();

        return '';
    }
    else
    {
        $db->commit();

        return $new_bid;
    }
}

function replace_booking_transport( $transport, $bdid, $bdtype, $key )
{
    global $db;

    if( isset( $transport[ $bdtype ][ $key ] ) )
    {
        foreach( $transport[ $bdtype ][ $key ] as $bttype => $t )
        {
            if( $t['bttrans_type'] == 2 )
            {
                $s = 'INSERT INTO ticket_booking_transport( bdid, bttype, bttrans_type, btdrivername, btdriverphone, bttrans_fee ) VALUES( %d, %s, %s, %s, %s, %s )';
                $q = $db->prepare_query( $s, $bdid, $bttype, $t['bttrans_type'], $t['btdrivername'], $t['btdriverphone'], $t['bttrans_fee'] );
            }
            else
            {
                if( empty( $t['taid'] ) || empty( $t['hid'] ) )
                {
                    $s = 'INSERT INTO ticket_booking_transport( bdid, bttype, bttrans_type, bttrans_fee ) VALUES( %d, %s, %s, %s )';
                    $q = $db->prepare_query( $s, $bdid, $bttype, $t['bttrans_type'], $t['bttrans_fee'] );
                }
                else
                {
                    if( $t['hid'] == 1 )
                    {
                        $bthotelname       = isset( $t['bthotelname'] ) ? $t['bthotelname'] : '';
                        $bthotelphone      = isset( $t['bthotelphone'] ) ? $t['bthotelphone'] : '';
                        $bthotelemail      = isset( $t['bthotelemail'] ) ? $t['bthotelemail'] : '';
                        $bthoteladdress    = isset( $t['bthoteladdress'] ) ? $t['bthoteladdress'] : '';
                        $bthotelroomnumber = isset( $t['bthotelroomnumber'] ) ? $t['bthotelroomnumber'] : '';
                    }
                    else
                    {
                        $h = get_hotel( $t['hid'] );

                        $bthotelname       = $h['hname'];
                        $bthotelphone      = $h['hphone'];
                        $bthotelemail      = $h['hemail'];
                        $bthoteladdress    = $h['haddress'];
                        $bthotelroomnumber = '';
                    }

                    list( $taid, $btrpfrom, $btrpto ) = explode( '|', $t['taid'] );

                    $btflighttime = isset( $t['btflighttime'] ) ? ( empty( $t['btflighttime'] ) ? '' : date( 'Y-m-d H:i:s', strtotime( $t['btflighttime'] ) ) ) : '';

                    if( !empty( $taid ) )
                    {
                        $s = 'INSERT INTO ticket_booking_transport(
                                bdid,
                                hid,
                                taid,
                                bttype,
                                bttrans_type,
                                bthotelname,
                                bthoteladdress,
                                bthotelphone,
                                bthotelemail,
                                bthotelroomnumber,
                                btflighttime,
                                btrpfrom,
                                btrpto,
                                bttrans_fee ) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
                        $q = $db->prepare_query( $s,
                                $bdid,
                                $t['hid'],
                                $taid,
                                $bttype,
                                $t['bttrans_type'],
                                $bthotelname,
                                $bthoteladdress,
                                $bthotelphone,
                                $bthotelemail,
                                $bthotelroomnumber,
                                $btflighttime,
                                $btrpfrom,
                                $btrpto,
                                $t['bttrans_fee'] );
                    }
                    else
                    {
                        $s = 'INSERT INTO ticket_booking_transport(
                                bdid,
                                hid,
                                bttype,
                                bttrans_type,
                                bthotelname,
                                bthoteladdress,
                                bthotelphone,
                                bthotelemail,
                                bthotelroomnumber,
                                btflighttime,
                                btrpfrom,
                                btrpto,
                                bttrans_fee ) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
                        $q = $db->prepare_query( $s,
                                $bdid,
                                $t['hid'],
                                $bttype,
                                $t['bttrans_type'],
                                $bthotelname,
                                $bthoteladdress,
                                $bthotelphone,
                                $bthotelemail,
                                $bthotelroomnumber,
                                $btflighttime,
                                $btrpfrom,
                                $btrpto,
                                $t['bttrans_fee'] );
                    }
                }
            }

            $r = $db->do_query( $q );
        }
    }
}

function replace_booking_passenger( $passenger, $bdid, $bdtype )
{
    global $db;

    if( isset( $passenger[ $bdtype ] ) )
    {
        foreach( $passenger[ $bdtype ] as $bptype => $obj )
        {
            foreach( $obj as $p )
            {
                $s = 'INSERT INTO ticket_booking_passenger(
                        bdid,
                        bptype,
                        bpname,
                        bpgender,
                        lcountry_id,
                        bpbirthdate) VALUES( %d, %s, %s, %s, %d, %s )';
                $q = $db->prepare_query( $s,
                        $bdid,
                        $bptype,
                        $p['bpname'],
                        $p['bpgender'],
                        $p['lcountry_id'],
                        empty( $p['bpbirthdate'] ) ? '' : date( 'Y-m-d', strtotime( $p['bpbirthdate'] ) ) );
                $r = $db->do_query( $q );
            }
        }
    }
}

function get_transport_fee_new_total( $transport, $bdtype, $key )
{
    return isset( $transport[ $bdtype ][ $key ] ) ? array_sum( array_column( $transport[ $bdtype ][ $key ], 'bttrans_fee' ) ) : 0;
}

function get_transport_fee_total( $bdid )
{
    global $db;

    $s = 'SELECT SUM( bttrans_fee ) AS transfee FROM ticket_booking_transport AS a WHERE a.bdid = %d';
    $q = $db->prepare_query( $s, $bdid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        return $d['transfee'];
    }
    else
    {
        return 0;
    }
}

function get_booking_subtotal_price( $bid )
{
    global $db;

    $s = 'SELECT SUM( total ) AS dtotal FROM ticket_booking_detail AS a WHERE a.bid = %d';
    $q = $db->prepare_query( $s, $bid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        return $d['dtotal'];
    }
    else
    {
        return 0;
    }
}

function get_edit_passenger_availability_result( $post, $data )
{
    global $db;

    extract( $post );
    extract( $data );

    $arr  = explode( '|', $booking_source );
    $pass = get_revise_passanger_id_and_type( $bpid );

    if( count( $arr ) == 2 )
    {
        $chid = $arr[0];
        $agid = $arr[1];
    }
    else
    {
        $chid = $arr[0];
        $agid = null;
    }

    $compliment = array( 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 );

    foreach( $bdid as $type => $id )
    {
        if( $type == 'departure' )
        {
            extract( $pass['passenger']['departure'] );

            $pass_num = $adult + $child + $infant;

            //-- Departure Trip
            if( isset( $depart_start_point ) && isset( $depart_end_point ) && isset( $depart_date ) )
            {
                $depart_date = date( 'Y-m-d', strtotime( $depart_date ) );
                $depart_tt   = $detail[ 'departure' ][ $id ][ 'bdtranstype' ];
                $depart_id   = $detail[ 'departure' ][ $id ][ 'bdid' ];
                $depart_sid  = $detail[ 'departure' ][ $id ][ 'sid' ];
                $depart_dt   = $detail[ 'departure' ][ $id ];

                $s = 'SELECT * FROM ticket_schedule AS a
                      LEFT JOIN ticket_route AS b ON a.rid = b.rid
                      LEFT JOIN ticket_boat AS c ON a.boid = c.boid
                      WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
                $q = $db->prepare_query( $s, $depart_date, $depart_date, 'publish', 'publish', '1' );
                $r = $db->do_query( $q );

                if( $db->num_rows( $r ) > 0 )
                {
                    $i = 0;

                    while( $d = $db->fetch_array( $r ) )
                    {
                        $available_depart  = is_available_depart_on_list( $d['rid'], $depart_start_point, $depart_date, true );
                        $available_arrival = is_available_arrival_on_list( $d['rid'], $depart_end_point );

                        if( strtotime( $depart_date ) < time() )
                        {
                            $not_closed_date        = true;
                            $available_date         = true;
                            $is_available_allotment = true;
                        }
                        else
                        {
                            $not_closed_date        = is_date_not_in_close_allotment_list( $agid, $d['sid'], $depart_date );
                            $available_date         = is_available_date_on_list( $d['sid'], $depart_date, true );
                            $is_available_allotment = is_available_allotment( $depart_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $depart_start_point, $depart_end_point, $agid, false, $depart_id );
                        }

                        if( $available_depart && $available_arrival && $available_date )
                        {
                            if( $is_available_allotment )
                            {
                                $p  = get_result_price( $d['sid'], $d['rid'], $depart_date, $depart_start_point, $depart_end_point, $adult, $child, $infant, $agid, $depart_dt );
                                $rd = get_route_detail_content( $d['rid'], $depart_start_point, $depart_end_point );

                                $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                                $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                                $trip_inc_transport_val = base64_encode( json_encode( array( $d['sid'], 1 ) ) );
                                $trip_inc_transport_id  = str_replace( '=', '', $trip_inc_transport_val );

                                $trip_exc_transport_val = base64_encode( json_encode( array( $d['sid'], 0 ) ) );
                                $trip_exc_transport_id  = str_replace( '=', '', $trip_exc_transport_val );

                                $current_transport_val  = base64_encode( json_encode( array( $depart_sid, intval( $depart_tt ) ) ) );

                                if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                                {
                                    add_variable( 'sid', $d['sid'] );
                                    add_variable( 'rid', $d['rid'] );
                                    add_variable( 'boid', $d['boid'] );
                                    add_variable( 'bdid', $depart_id );
                                    add_variable( 'boname', $d['boname'] );
                                    add_variable( 'bddate', $depart_date );

                                    add_variable( 'bddeparttime', $rd['depart_time'] );
                                    add_variable( 'bdarrivetime', $rd['arrive_time'] );

                                    add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                    add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                                    add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                                    add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                                    add_variable( 'trip_inc_transport_id', $trip_inc_transport_id );
                                    add_variable( 'trip_exc_transport_id', $trip_exc_transport_id );

                                    add_variable( 'trip_inc_transport_check', $trip_inc_transport_val == $current_transport_val ? 'checked' : '' );
                                    add_variable( 'trip_exc_transport_check', $trip_exc_transport_val == $current_transport_val ? 'checked' : '' );

                                    if( in_array( $data['bpaymethod'], $compliment ) )
                                    {
                                        add_variable( 'inc_adult_price', 0 );
                                        add_variable( 'inc_child_price', 0 );
                                        add_variable( 'inc_infant_price', 0 );

                                        add_variable( 'exc_adult_price', 0 );
                                        add_variable( 'exc_child_price', 0 );
                                        add_variable( 'exc_infant_price', 0 );

                                        add_variable( 'discount_notif', '' );

                                        add_variable( 'inc_disc_price', 0 );
                                        add_variable( 'exc_disc_price', 0 );

                                        add_variable( 'inc_disc_adult', 0 );
                                        add_variable( 'inc_disc_child', 0 );
                                        add_variable( 'inc_disc_infant', 0 );

                                        add_variable( 'exc_disc_adult', 0 );
                                        add_variable( 'exc_disc_child', 0 );
                                        add_variable( 'exc_disc_infant', 0 );

                                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                        add_variable( 'include_price_number', 'Free' );
                                        add_variable( 'exclude_price_number', 'Free' );

                                        add_variable( 'include_price_disc_number', '' );
                                        add_variable( 'exclude_price_disc_number', '' );

                                        add_variable( 'inc_total_price', 0 );
                                        add_variable( 'exc_total_price', 0 );

                                        add_variable( 'include_css', '' );
                                        add_variable( 'exclude_css', '' );
                                    }
                                    else
                                    {
                                        add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                        add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                        add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                        add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                        add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                        add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                        if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) || !empty( $p['fix_discount'] ) )
                                        {
                                            if( !empty( $p['early_discount'] ) )
                                            {
                                                if( $p['early_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                                    $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                                }

                                                add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                            }

                                            if( !empty( $p['seat_discount'] ) )
                                            {
                                                if( $p['seat_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                                    $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                                }

                                                add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                            }

                                            if( !empty( $p['fix_discount'] ) )
                                            {
                                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['fix_discount']['disc_per_adult'] : 0;
                                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['fix_discount']['disc_per_child'] : 0;
                                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['fix_discount']['disc_per_infant'] : 0;

                                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['fix_discount']['disc_per_adult'] : 0;
                                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['fix_discount']['disc_per_child'] : 0;
                                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['fix_discount']['disc_per_infant'] : 0;

                                                $inc_disc_price  = ( $inc_disc_adult * $adult ) + ( $inc_disc_child * $child )  + ( $inc_disc_infant * $infant );
                                                $exc_disc_price  = ( $exc_disc_adult * $adult ) + ( $exc_disc_child * $child )  + ( $exc_disc_infant * $infant );
                                            }

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', $inc_disc_price > 0 ? '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' : '' );
                                            add_variable( 'exclude_price_disc_number', $exc_disc_price > 0 ? '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' : '' );
                                        }
                                        else
                                        {
                                            add_variable( 'discount_notif', '' );

                                            add_variable( 'inc_disc_price', 0 );
                                            add_variable( 'exc_disc_price', 0 );

                                            add_variable( 'inc_disc_adult', 0 );
                                            add_variable( 'inc_disc_child', 0 );
                                            add_variable( 'inc_disc_infant', 0 );

                                            add_variable( 'exc_disc_adult', 0 );
                                            add_variable( 'exc_disc_child', 0 );
                                            add_variable( 'exc_disc_infant', 0 );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '' );
                                            add_variable( 'exclude_price_disc_number', '' );
                                        }

                                        add_variable( 'inc_total_price', $inc_total_price );
                                        add_variable( 'exc_total_price', $exc_total_price );

                                        add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                        add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );
                                    }

                                    add_variable( 'rtdid', $p['rtdid'] );
                                    add_variable( 'bdcdate', $depart_dt['bdcdate'] );
                                    add_variable( 'bdstatus', $depart_dt['bdstatus'] );
                                    add_variable( 'bdpstatus', $depart_dt['bdpstatus'] );
                                    add_variable( 'bdcreason', $depart_dt['bdcreason'] );
                                    add_variable( 'bdcmessage', $depart_dt['bdcmessage'] );
                                    add_variable( 'bdrevstatus', $depart_dt['bdrevstatus'] );
                                    add_variable( 'bdcancelfee', $depart_dt['bdcancelfee'] );
                                    add_variable( 'bdtransactionfee', $depart_dt['bdtransactionfee'] );
                                    add_variable( 'bdcancelpercentfee', $depart_dt['bdcancelpercentfee'] );

                                    add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                                    add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $depart_start_point, $depart_end_point ) );

                                    add_variable( 'include_transport', get_edit_transport_option( $d, $depart_start_point, $depart_end_point, 'departure', $pass_num, $depart_dt ) );
                                    add_variable( 'exclude_transport', get_edit_transport_option( $d, $depart_start_point, $depart_end_point, 'departure', $pass_num, $depart_dt, 1 ) );

                                    parse_template( 'departure-trip-loop-block', 'dtrl-block', true );

                                    $i++;
                                }
                                else
                                {
                                    if( in_array( $data['bpaymethod'], $compliment ) )
                                    {
                                        add_variable( 'sid', $d['sid'] );
                                        add_variable( 'rid', $d['rid'] );
                                        add_variable( 'boid', $d['boid'] );
                                        add_variable( 'bdid', $depart_id );
                                        add_variable( 'boname', $d['boname'] );
                                        add_variable( 'bddate', $depart_date );

                                        add_variable( 'bddeparttime', $rd['depart_time'] );
                                        add_variable( 'bdarrivetime', $rd['arrive_time'] );

                                        add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                        add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                                        add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                                        add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                                        add_variable( 'trip_inc_transport_check', $trip_inc_transport_val == $current_transport_val ? 'checked' : '' );
                                        add_variable( 'trip_exc_transport_check', $trip_exc_transport_val == $current_transport_val ? 'checked' : '' );

                                        add_variable( 'inc_adult_price', 0 );
                                        add_variable( 'inc_child_price', 0 );
                                        add_variable( 'inc_infant_price', 0 );

                                        add_variable( 'exc_adult_price', 0 );
                                        add_variable( 'exc_child_price', 0 );
                                        add_variable( 'exc_infant_price', 0 );

                                        add_variable( 'discount_notif', '' );

                                        add_variable( 'inc_disc_price', 0 );
                                        add_variable( 'exc_disc_price', 0 );

                                        add_variable( 'inc_disc_adult', 0 );
                                        add_variable( 'inc_disc_child', 0 );
                                        add_variable( 'inc_disc_infant', 0 );

                                        add_variable( 'exc_disc_adult', 0 );
                                        add_variable( 'exc_disc_child', 0 );
                                        add_variable( 'exc_disc_infant', 0 );

                                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                        add_variable( 'include_price_number', 'Free' );
                                        add_variable( 'exclude_price_number', 'Free' );

                                        add_variable( 'include_price_disc_number', '' );
                                        add_variable( 'exclude_price_disc_number', '' );

                                        add_variable( 'inc_total_price', 0 );
                                        add_variable( 'exc_total_price', 0 );

                                        add_variable( 'include_css', '' );
                                        add_variable( 'exclude_css', '' );

                                        add_variable( 'rtdid', $p['rtdid'] );
                                        add_variable( 'bdcdate', $depart_dt['bdcdate'] );
                                        add_variable( 'bdstatus', $depart_dt['bdstatus'] );
                                        add_variable( 'bdpstatus', $depart_dt['bdpstatus'] );
                                        add_variable( 'bdcreason', $depart_dt['bdcreason'] );
                                        add_variable( 'bdcmessage', $depart_dt['bdcmessage'] );
                                        add_variable( 'bdrevstatus', $depart_dt['bdrevstatus'] );
                                        add_variable( 'bdcancelfee', $depart_dt['bdcancelfee'] );
                                        add_variable( 'bdtransactionfee', $depart_dt['bdtransactionfee'] );
                                        add_variable( 'bdcancelpercentfee', $depart_dt['bdcancelpercentfee'] );

                                        add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                                        add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $depart_start_point, $depart_end_point ) );

                                        add_variable( 'include_transport', get_edit_transport_option( $d, $depart_start_point, $depart_end_point, 'departure', $pass_num, $depart_dt ) );
                                        add_variable( 'exclude_transport', get_edit_transport_option( $d, $depart_start_point, $depart_end_point, 'departure', $pass_num, $depart_dt, 1 ) );

                                        parse_template( 'departure-trip-loop-block', 'dtrl-block', true );

                                        $i++;
                                    }
                                }
                            }
                        }
                    }

                    if( empty( $i )  )
                    {
                        parse_template( 'no-departure-trip-loop-block', 'ndtrl-block', true );
                    }
                    else
                    {
                        add_variable( 'depart_passenger', get_passenger_list_revise_content( $id, 'departure', $pass['bpid'] ) );

                        parse_template( 'departure-trip-passenger-block', 'dtrp-block', false );
                    }
                }
                else
                {
                    parse_template( 'no-departure-trip-loop-block', 'ndtrl-block', true );
                }
            }
        }
        elseif( $type == 'return' )
        {
            extract( $pass['passenger']['return'] );

            $pass_num = $adult + $child + $infant;

            //-- Return Trip
            if( isset( $return_start_point ) && isset( $return_end_point ) && isset( $return_date ) )
            {
                $return_date = date( 'Y-m-d', strtotime( $return_date ) );
                $return_tt   = $detail[ 'return' ][ $id ][ 'bdtranstype' ];
                $return_id   = $detail[ 'return' ][ $id ][ 'bdid' ];
                $return_sid  = $detail[ 'return' ][ $id ][ 'sid' ];
                $return_dt   = $detail[ 'return' ][ $id ];

                $s = 'SELECT * FROM ticket_schedule AS a
                      LEFT JOIN ticket_route AS b ON a.rid = b.rid
                      LEFT JOIN ticket_boat AS c ON a.boid = c.boid
                      WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
                $q = $db->prepare_query( $s, $return_date, $return_date, 'publish', 'publish', '1' );
                $r = $db->do_query( $q );

                if( $db->num_rows( $r ) > 0 )
                {
                    $i = 0;

                    while( $d = $db->fetch_array( $r ) )
                    {
                        $available_depart  = is_available_depart_on_list( $d['rid'], $return_start_point, $return_date, true );
                        $available_arrival = is_available_arrival_on_list( $d['rid'], $return_end_point );

                        if( strtotime( $return_date ) < time() )
                        {
                            $not_closed_date        = true;
                            $available_date         = true;
                            $is_available_allotment = true;
                        }
                        else
                        {
                            $not_closed_date        = is_date_not_in_close_allotment_list( $agid, $d['sid'], $return_date );
                            $available_date         = is_available_date_on_list( $d['sid'], $return_date, true );
                            $is_available_allotment = is_available_allotment( $return_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $return_start_point, $return_end_point, $agid, false, $return_id );
                        }

                        if( $available_depart && $available_arrival && $available_date )
                        {
                            if( $is_available_allotment )
                            {
                                $p  = get_result_price( $d['sid'], $d['rid'], $return_date, $return_start_point, $return_end_point, $adult, $child, $infant, $agid, $return_dt );
                                $rd = get_route_detail_content( $d['rid'], $return_start_point, $return_end_point );

                                $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                                $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                                $trip_inc_transport_val = base64_encode( json_encode( array( $d['sid'], 1 ) ) );
                                $trip_exc_transport_val = base64_encode( json_encode( array( $d['sid'], 0 ) ) );

                                $trip_inc_transport_id = str_replace( '=', '', $trip_inc_transport_val );
                                $trip_exc_transport_id = str_replace( '=', '', $trip_exc_transport_val );

                                $current_transport_val  = base64_encode( json_encode( array( $return_sid, intval( $return_tt ) ) ) );

                                if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                                {
                                    add_variable( 'sid', $d['sid'] );
                                    add_variable( 'rid', $d['rid'] );
                                    add_variable( 'boid', $d['boid'] );
                                    add_variable( 'bdid', $return_id );
                                    add_variable( 'boname', $d['boname'] );
                                    add_variable( 'bddate', $return_date );

                                    add_variable( 'bddeparttime', $rd['depart_time'] );
                                    add_variable( 'bdarrivetime', $rd['arrive_time'] );

                                    add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                    add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                                    add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                                    add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                                    add_variable( 'trip_inc_transport_id', $trip_inc_transport_id );
                                    add_variable( 'trip_exc_transport_id', $trip_exc_transport_id );

                                    add_variable( 'trip_inc_transport_check', $trip_inc_transport_val == $current_transport_val ? 'checked' : '' );
                                    add_variable( 'trip_exc_transport_check', $trip_exc_transport_val == $current_transport_val ? 'checked' : '' );

                                    if( in_array( $data['bpaymethod'], $compliment ) )
                                    {
                                        add_variable( 'inc_adult_price', 0 );
                                        add_variable( 'inc_child_price', 0 );
                                        add_variable( 'inc_infant_price', 0 );

                                        add_variable( 'exc_adult_price', 0 );
                                        add_variable( 'exc_child_price', 0 );
                                        add_variable( 'exc_infant_price', 0 );

                                        add_variable( 'discount_notif', '' );

                                        add_variable( 'inc_disc_price', 0 );
                                        add_variable( 'exc_disc_price', 0 );

                                        add_variable( 'inc_disc_adult', 0 );
                                        add_variable( 'inc_disc_child', 0 );
                                        add_variable( 'inc_disc_infant', 0 );

                                        add_variable( 'exc_disc_adult', 0 );
                                        add_variable( 'exc_disc_child', 0 );
                                        add_variable( 'exc_disc_infant', 0 );

                                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                        add_variable( 'include_price_number', 'Free' );
                                        add_variable( 'exclude_price_number', 'Free' );

                                        add_variable( 'include_price_disc_number', '' );
                                        add_variable( 'exclude_price_disc_number', '' );

                                        add_variable( 'inc_total_price', 0 );
                                        add_variable( 'exc_total_price', 0 );

                                        add_variable( 'include_css', '' );
                                        add_variable( 'exclude_css', '' );
                                    }
                                    else
                                    {
                                        add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                        add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                        add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                        add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                        add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                        add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                        if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) || !empty( $p['fix_discount'] ) )
                                        {
                                            if( !empty( $p['early_discount'] ) )
                                            {
                                                if( $p['early_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                                    $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                                }

                                                add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                            }

                                            if( !empty( $p['seat_discount'] ) )
                                            {
                                                if( $p['seat_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                                    $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                                }

                                                add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                            }

                                            if( !empty( $p['fix_discount'] ) )
                                            {
                                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['fix_discount']['disc_per_adult'] : 0;
                                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['fix_discount']['disc_per_child'] : 0;
                                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['fix_discount']['disc_per_infant'] : 0;

                                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['fix_discount']['disc_per_adult'] : 0;
                                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['fix_discount']['disc_per_child'] : 0;
                                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['fix_discount']['disc_per_infant'] : 0;

                                                $inc_disc_price  = ( $inc_disc_adult * $adult ) + ( $inc_disc_child * $child )  + ( $inc_disc_infant * $infant );
                                                $exc_disc_price  = ( $exc_disc_adult * $adult ) + ( $exc_disc_child * $child )  + ( $exc_disc_infant * $infant );
                                            }

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', $inc_disc_price > 0 ? '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' : '' );
                                            add_variable( 'exclude_price_disc_number', $exc_disc_price > 0 ? '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' : '' );
                                        }
                                        else
                                        {
                                            add_variable( 'discount_notif', '' );

                                            add_variable( 'inc_disc_price', 0 );
                                            add_variable( 'exc_disc_price', 0 );

                                            add_variable( 'inc_disc_adult', 0 );
                                            add_variable( 'inc_disc_child', 0 );
                                            add_variable( 'inc_disc_infant', 0 );

                                            add_variable( 'exc_disc_adult', 0 );
                                            add_variable( 'exc_disc_child', 0 );
                                            add_variable( 'exc_disc_infant', 0 );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '' );
                                            add_variable( 'exclude_price_disc_number', '' );
                                        }

                                        add_variable( 'inc_total_price', $inc_total_price );
                                        add_variable( 'exc_total_price', $exc_total_price );

                                        add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                        add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );
                                    }

                                    add_variable( 'rtdid', $p['rtdid'] );
                                    add_variable( 'bdcdate', $return_dt['bdcdate'] );
                                    add_variable( 'bdstatus', $return_dt['bdstatus'] );
                                    add_variable( 'bdpstatus', $return_dt['bdpstatus'] );
                                    add_variable( 'bdcreason', $return_dt['bdcreason'] );
                                    add_variable( 'bdcmessage', $return_dt['bdcmessage'] );
                                    add_variable( 'bdrevstatus', $return_dt['bdrevstatus'] );
                                    add_variable( 'bdcancelfee', $return_dt['bdcancelfee'] );
                                    add_variable( 'bdtransactionfee', $return_dt['bdtransactionfee'] );
                                    add_variable( 'bdcancelpercentfee', $return_dt['bdcancelpercentfee'] );

                                    add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                                    add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $return_start_point, $return_end_point ) );

                                    add_variable( 'include_transport', get_edit_transport_option( $d, $return_start_point, $return_end_point, 'return', $pass_num, $return_dt ) );
                                    add_variable( 'exclude_transport', get_edit_transport_option( $d, $return_start_point, $return_end_point, 'return', $pass_num, $return_dt, true ) );

                                    parse_template( 'return-trip-loop-block', 'rtrl-block', true );

                                    $i++;
                                }
                                else
                                {
                                    if( in_array( $data['bpaymethod'], $compliment ) )
                                    {
                                        add_variable( 'sid', $d['sid'] );
                                        add_variable( 'rid', $d['rid'] );
                                        add_variable( 'boid', $d['boid'] );
                                        add_variable( 'bdid', $return_id );
                                        add_variable( 'boname', $d['boname'] );
                                        add_variable( 'bddate', $return_date );

                                        add_variable( 'bddeparttime', $rd['depart_time'] );
                                        add_variable( 'bdarrivetime', $rd['arrive_time'] );

                                        add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                        add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                                        add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                                        add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                                        add_variable( 'trip_inc_transport_check', $trip_inc_transport_val == $current_transport_val ? 'checked' : '' );
                                        add_variable( 'trip_exc_transport_check', $trip_exc_transport_val == $current_transport_val ? 'checked' : '' );

                                        add_variable( 'inc_adult_price', 0 );
                                        add_variable( 'inc_child_price', 0 );
                                        add_variable( 'inc_infant_price', 0 );

                                        add_variable( 'exc_adult_price', 0 );
                                        add_variable( 'exc_child_price', 0 );
                                        add_variable( 'exc_infant_price', 0 );

                                        add_variable( 'discount_notif', '' );

                                        add_variable( 'inc_disc_price', 0 );
                                        add_variable( 'exc_disc_price', 0 );

                                        add_variable( 'inc_disc_adult', 0 );
                                        add_variable( 'inc_disc_child', 0 );
                                        add_variable( 'inc_disc_infant', 0 );

                                        add_variable( 'exc_disc_adult', 0 );
                                        add_variable( 'exc_disc_child', 0 );
                                        add_variable( 'exc_disc_infant', 0 );

                                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                        add_variable( 'include_price_number', 'Free' );
                                        add_variable( 'exclude_price_number', 'Free' );

                                        add_variable( 'include_price_disc_number', '' );
                                        add_variable( 'exclude_price_disc_number', '' );

                                        add_variable( 'inc_total_price', 0 );
                                        add_variable( 'exc_total_price', 0 );

                                        add_variable( 'include_css', '' );
                                        add_variable( 'exclude_css', '' );

                                        add_variable( 'rtdid', $p['rtdid'] );
                                        add_variable( 'bdcdate', $return_dt['bdcdate'] );
                                        add_variable( 'bdstatus', $return_dt['bdstatus'] );
                                        add_variable( 'bdpstatus', $return_dt['bdpstatus'] );
                                        add_variable( 'bdcreason', $return_dt['bdcreason'] );
                                        add_variable( 'bdcmessage', $return_dt['bdcmessage'] );
                                        add_variable( 'bdrevstatus', $return_dt['bdrevstatus'] );
                                        add_variable( 'bdcancelfee', $return_dt['bdcancelfee'] );
                                        add_variable( 'bdtransactionfee', $return_dt['bdtransactionfee'] );
                                        add_variable( 'bdcancelpercentfee', $return_dt['bdcancelpercentfee'] );

                                        add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                                        add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $return_start_point, $return_end_point ) );

                                        add_variable( 'include_transport', get_edit_transport_option( $d, $return_start_point, $return_end_point, 'return', $pass_num, $return_dt ) );
                                        add_variable( 'exclude_transport', get_edit_transport_option( $d, $return_start_point, $return_end_point, 'return', $pass_num, $return_dt, true ) );

                                        parse_template( 'return-trip-loop-block', 'rtrl-block', true );

                                        $i++;
                                    }
                                }
                            }
                        }
                    }

                    if( empty( $i )  )
                    {
                        parse_template( 'no-return-trip-loop-block', 'nrtrl-block', true );
                    }
                    else
                    {
                        add_variable( 'return_passenger', get_passenger_list_revise_content( $id, 'return', $pass['bpid'] ) );

                        parse_template( 'return-trip-passenger-block', 'rtrp-block', false );
                    }
                }
                else
                {
                    parse_template( 'no-return-trip-loop-block', 'nrtrl-block', true );
                }
            }
        }
    }

    if( isset( $bdid['departure'] ) && !empty( $bdid['departure'] ) )
    {
        parse_template( 'departure-trip-block', 'dtr-block' );
    }

    if( isset( $bdid['return'] ) && !empty( $bdid['return'] ) )
    {
        parse_template( 'return-trip-block', 'rtr-block' );
    }
}

function get_edit_trip_availability_result( $post, $data )
{
    global $db;

    extract( $post );
    extract( $data );

    $arr = explode( '|', $booking_source );

    if( count( $arr ) == 2 )
    {
        $chid = $arr[0];
        $agid = $arr[1];
    }
    else
    {
        $chid = $arr[0];
        $agid = null;
    }

    $depart_sid = null;
    $return_sid = null;
    $depart_id  = null;
    $return_id  = null;
    $depart_tt  = '';
    $return_tt  = '';
    $depart_dt  = '';
    $return_dt  = '';

    foreach( $data['detail'] as $bdtype => $obj )
    {
        foreach( $obj as $dt )
        {
            if( $bdtype == 'departure' )
            {
                $depart_tt  = $dt['bdtranstype'];
                $depart_id  = $dt['bdid'];
                $depart_sid = $dt['sid'];
                $depart_dt  = $dt;
            }
            else
            {
                $return_tt  = $dt['bdtranstype'];
                $return_id  = $dt['bdid'];
                $return_sid = $dt['sid'];
                $return_dt  = $dt;
            }
        }
    }

    $pass_num   = $adult + $child + $infant;
    $compliment = array( 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 );

    //-- Departure Trip
    if( isset( $depart_start_point ) && isset( $depart_end_point ) && isset( $depart_date ) )
    {
        $depart_date = date( 'Y-m-d', strtotime( $depart_date ) );

        $s = 'SELECT * FROM ticket_schedule AS a
              LEFT JOIN ticket_route AS b ON a.rid = b.rid
              LEFT JOIN ticket_boat AS c ON a.boid = c.boid
              WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
        $q = $db->prepare_query( $s, $depart_date, $depart_date, 'publish', 'publish', '1' );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $i = 0;

            while( $d = $db->fetch_array( $r ) )
            {
                $available_depart  = is_available_depart_on_list( $d['rid'], $depart_start_point, $depart_date, true );
                $available_arrival = is_available_arrival_on_list( $d['rid'], $depart_end_point );

                if( strtotime( $depart_date ) < time() )
                {
                    $not_closed_date        = true;
                    $available_date         = true;
                    $is_available_allotment = true;
                }
                else
                {
                    $not_closed_date        = is_date_not_in_close_allotment_list( $agid, $d['sid'], $depart_date );
                    $available_date         = is_available_date_on_list( $d['sid'], $depart_date, true );
                    $is_available_allotment = is_available_allotment( $depart_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $depart_start_point, $depart_end_point, $agid, false, $depart_id );
                }

                if( $available_depart && $available_arrival && $available_date )
                {
                    if( $is_available_allotment )
                    {
                        $p  = get_result_price( $d['sid'], $d['rid'], $depart_date, $depart_start_point, $depart_end_point, $adult, $child, $infant, $agid, $depart_dt );
                        $rd = get_route_detail_content( $d['rid'], $depart_start_point, $depart_end_point );

                        $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                        $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                        $trip_inc_transport_val = base64_encode( json_encode( array( $d['sid'], 1 ) ) );
                        $trip_exc_transport_val = base64_encode( json_encode( array( $d['sid'], 0 ) ) );

                        $trip_inc_transport_id  = str_replace( '=', '', $trip_inc_transport_val );
                        $trip_exc_transport_id  = str_replace( '=', '', $trip_exc_transport_val );

                        $current_transport_val  = base64_encode( json_encode( array( $depart_sid, intval( $depart_tt ) ) ) );

                        if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                        {
                            add_variable( 'sid', $d['sid'] );
                            add_variable( 'rid', $d['rid'] );
                            add_variable( 'boid', $d['boid'] );
                            add_variable( 'bdid', $depart_id );
                            add_variable( 'boname', $d['boname'] );
                            add_variable( 'bddate', $depart_date );

                            add_variable( 'bddeparttime', $rd['depart_time'] );
                            add_variable( 'bdarrivetime', $rd['arrive_time'] );

                            add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                            add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                            add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                            add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                            add_variable( 'trip_inc_transport_id', $trip_inc_transport_id );
                            add_variable( 'trip_exc_transport_id', $trip_exc_transport_id );

                            add_variable( 'trip_inc_transport_check', $trip_inc_transport_val == $current_transport_val ? 'checked' : '' );
                            add_variable( 'trip_exc_transport_check', $trip_exc_transport_val == $current_transport_val ? 'checked' : '' );

                            if( in_array( $data['bpaymethod'], $compliment ) )
                            {
                                add_variable( 'inc_adult_price', 0 );
                                add_variable( 'inc_child_price', 0 );
                                add_variable( 'inc_infant_price', 0 );

                                add_variable( 'exc_adult_price', 0 );
                                add_variable( 'exc_child_price', 0 );
                                add_variable( 'exc_infant_price', 0 );

                                add_variable( 'discount_notif', '' );

                                add_variable( 'inc_disc_price', 0 );
                                add_variable( 'exc_disc_price', 0 );

                                add_variable( 'inc_disc_adult', 0 );
                                add_variable( 'inc_disc_child', 0 );
                                add_variable( 'inc_disc_infant', 0 );

                                add_variable( 'exc_disc_adult', 0 );
                                add_variable( 'exc_disc_child', 0 );
                                add_variable( 'exc_disc_infant', 0 );

                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                add_variable( 'include_price_number', 'Free' );
                                add_variable( 'exclude_price_number', 'Free' );

                                add_variable( 'include_price_disc_number', '' );
                                add_variable( 'exclude_price_disc_number', '' );

                                add_variable( 'inc_total_price', 0 );
                                add_variable( 'exc_total_price', 0 );

                                add_variable( 'include_css', '' );
                                add_variable( 'exclude_css', '' );
                            }
                            else
                            {
                                add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) || !empty( $p['fix_discount'] ) )
                                {
                                    if( !empty( $p['early_discount'] ) )
                                    {
                                        if( $p['early_discount']['type'] == '0' )
                                        {
                                            $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                            $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                            $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                            $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                            $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                            $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                            $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                            $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                        }
                                        else
                                        {
                                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                            $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                            $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                        }

                                        add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                    }

                                    if( !empty( $p['seat_discount'] ) )
                                    {
                                        if( $p['seat_discount']['type'] == '0' )
                                        {
                                            $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                            $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                            $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                            $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                            $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                            $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                            $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                            $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                        }
                                        else
                                        {
                                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                            $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                            $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                        }

                                        add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                    }

                                    if( !empty( $p['fix_discount'] ) )
                                    {
                                        $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['fix_discount']['disc_per_adult'] : 0;
                                        $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['fix_discount']['disc_per_child'] : 0;
                                        $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['fix_discount']['disc_per_infant'] : 0;

                                        $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['fix_discount']['disc_per_adult'] : 0;
                                        $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['fix_discount']['disc_per_child'] : 0;
                                        $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['fix_discount']['disc_per_infant'] : 0;

                                        $inc_disc_price  = ( $inc_disc_adult * $adult ) + ( $inc_disc_child * $child ) + ( $inc_disc_infant * $infant );
                                        $exc_disc_price  = ( $exc_disc_adult * $adult ) + ( $exc_disc_child * $child ) + ( $exc_disc_infant * $infant );
                                    }

                                    add_variable( 'inc_disc_price', $inc_disc_price );
                                    add_variable( 'exc_disc_price', $exc_disc_price );

                                    add_variable( 'inc_disc_adult', $inc_disc_adult );
                                    add_variable( 'inc_disc_child', $inc_disc_child );
                                    add_variable( 'inc_disc_infant', $inc_disc_infant );

                                    add_variable( 'exc_disc_adult', $exc_disc_adult );
                                    add_variable( 'exc_disc_child', $exc_disc_child );
                                    add_variable( 'exc_disc_infant', $exc_disc_infant );

                                    add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                    add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                    add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                    add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                    add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                    add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                    add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                    add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                                    add_variable( 'include_price_disc_number', '<span class="disc-price display-'. $trip_inc_transport_id .' dis-not-'. $trip_inc_transport_id .'" data-a-sep="." data-a-dec="," data-m-dec="0">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                    add_variable( 'exclude_price_disc_number', '<span class="disc-price display-'. $trip_exc_transport_id .' dis-not-'. $trip_exc_transport_id .'" data-a-sep="." data-a-dec="," data-m-dec="0">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                }
                                else
                                {
                                    add_variable( 'discount_notif', '' );

                                    add_variable( 'inc_disc_price', 0 );
                                    add_variable( 'exc_disc_price', 0 );

                                    add_variable( 'inc_disc_adult', 0 );
                                    add_variable( 'inc_disc_child', 0 );
                                    add_variable( 'inc_disc_infant', 0 );

                                    add_variable( 'exc_disc_adult', 0 );
                                    add_variable( 'exc_disc_child', 0 );
                                    add_variable( 'exc_disc_infant', 0 );

                                    add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                    add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                    add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                    add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                    add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                    add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                    add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                    add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                    add_variable( 'include_price_disc_number', '' );
                                    add_variable( 'exclude_price_disc_number', '' );
                                }

                                add_variable( 'inc_total_price', $inc_total_price );
                                add_variable( 'exc_total_price', $exc_total_price );

                                add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );
                            }

                            add_variable( 'rtdid', $p['rtdid'] );
                            add_variable( 'bdcdate', $depart_dt['bdcdate'] );
                            add_variable( 'bdstatus', $depart_dt['bdstatus'] );
                            add_variable( 'bdpstatus', $depart_dt['bdpstatus'] );
                            add_variable( 'bdcreason', $depart_dt['bdcreason'] );
                            add_variable( 'bdcmessage', $depart_dt['bdcmessage'] );
                            add_variable( 'bdrevstatus', $depart_dt['bdrevstatus'] );
                            add_variable( 'bdcancelfee', $depart_dt['bdcancelfee'] );
                            add_variable( 'bdtransactionfee', $depart_dt['bdtransactionfee'] );
                            add_variable( 'bdcancelpercentfee', $depart_dt['bdcancelpercentfee'] );

                            add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                            add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $depart_start_point, $depart_end_point ) );

                            add_variable( 'include_transport', get_edit_transport_option( $d, $depart_start_point, $depart_end_point, 'departure', $pass_num, $depart_dt ) );
                            add_variable( 'exclude_transport', get_edit_transport_option( $d, $depart_start_point, $depart_end_point, 'departure', $pass_num, $depart_dt, 1 ) );

                            parse_template( 'departure-trip-loop-block', 'dtrl-block', true );

                            $i++;
                        }
                        else
                        {
                            if( in_array( $data['bpaymethod'], $compliment ) || !empty( $depart_dt ) )
                            {
                                add_variable( 'sid', $d['sid'] );
                                add_variable( 'rid', $d['rid'] );
                                add_variable( 'boid', $d['boid'] );
                                add_variable( 'bdid', $depart_id );
                                add_variable( 'boname', $d['boname'] );
                                add_variable( 'bddate', $depart_date );

                                add_variable( 'bddeparttime', $rd['depart_time'] );
                                add_variable( 'bdarrivetime', $rd['arrive_time'] );

                                add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                                add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                                add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                                add_variable( 'trip_inc_transport_check', $trip_inc_transport_val == $current_transport_val ? 'checked' : '' );
                                add_variable( 'trip_exc_transport_check', $trip_exc_transport_val == $current_transport_val ? 'checked' : '' );

                                add_variable( 'inc_adult_price', 0 );
                                add_variable( 'inc_child_price', 0 );
                                add_variable( 'inc_infant_price', 0 );

                                add_variable( 'exc_adult_price', 0 );
                                add_variable( 'exc_child_price', 0 );
                                add_variable( 'exc_infant_price', 0 );

                                add_variable( 'discount_notif', '' );

                                add_variable( 'inc_disc_price', 0 );
                                add_variable( 'exc_disc_price', 0 );

                                add_variable( 'inc_disc_adult', 0 );
                                add_variable( 'inc_disc_child', 0 );
                                add_variable( 'inc_disc_infant', 0 );

                                add_variable( 'exc_disc_adult', 0 );
                                add_variable( 'exc_disc_child', 0 );
                                add_variable( 'exc_disc_infant', 0 );

                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                add_variable( 'include_price_number', 'Free' );
                                add_variable( 'exclude_price_number', 'Free' );

                                add_variable( 'include_price_disc_number', '' );
                                add_variable( 'exclude_price_disc_number', '' );

                                add_variable( 'inc_total_price', 0 );
                                add_variable( 'exc_total_price', 0 );

                                add_variable( 'include_css', '' );
                                add_variable( 'exclude_css', '' );

                                add_variable( 'rtdid', $p['rtdid'] );
                                add_variable( 'bdcdate', $depart_dt['bdcdate'] );
                                add_variable( 'bdstatus', $depart_dt['bdstatus'] );
                                add_variable( 'bdpstatus', $depart_dt['bdpstatus'] );
                                add_variable( 'bdcreason', $depart_dt['bdcreason'] );
                                add_variable( 'bdcmessage', $depart_dt['bdcmessage'] );
                                add_variable( 'bdrevstatus', $depart_dt['bdrevstatus'] );
                                add_variable( 'bdcancelfee', $depart_dt['bdcancelfee'] );
                                add_variable( 'bdtransactionfee', $depart_dt['bdtransactionfee'] );
                                add_variable( 'bdcancelpercentfee', $depart_dt['bdcancelpercentfee'] );

                                add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                                add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $depart_start_point, $depart_end_point ) );

                                add_variable( 'include_transport', get_edit_transport_option( $d, $depart_start_point, $depart_end_point, 'departure', $pass_num, $depart_dt ) );
                                add_variable( 'exclude_transport', get_edit_transport_option( $d, $depart_start_point, $depart_end_point, 'departure', $pass_num, $depart_dt, 1 ) );

                                parse_template( 'departure-trip-loop-block', 'dtrl-block', true );

                                $i++;
                            }
                        }
                    }
                }
            }

            if( empty( $i )  )
            {
                parse_template( 'no-departure-trip-loop-block', 'ndtrl-block' );
            }
        }
        else
        {
            parse_template( 'no-departure-trip-loop-block', 'ndtrl-block' );
        }

        parse_template( 'departure-trip-block', 'dtr-block' );
    }

    //-- Return Trip
    if( isset( $return_start_point ) && isset( $return_end_point ) && isset( $return_date ) )
    {
        $return_date = date( 'Y-m-d', strtotime( $return_date ) );

        $s = 'SELECT * FROM ticket_schedule AS a
              LEFT JOIN ticket_route AS b ON a.rid = b.rid
              LEFT JOIN ticket_boat AS c ON a.boid = c.boid
              WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
        $q = $db->prepare_query( $s, $return_date, $return_date, 'publish', 'publish', '1' );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $i = 0;

            while( $d = $db->fetch_array( $r ) )
            {
                $available_depart  = is_available_depart_on_list( $d['rid'], $return_start_point, $return_date, true );
                $available_arrival = is_available_arrival_on_list( $d['rid'], $return_end_point );

                if( strtotime( $return_date ) < time() )
                {
                    $not_closed_date        = true;
                    $available_date         = true;
                    $is_available_allotment = true;
                }
                else
                {
                    $not_closed_date        = is_date_not_in_close_allotment_list( $agid, $d['sid'], $return_date );
                    $available_date         = is_available_date_on_list( $d['sid'], $return_date, true );
                    $is_available_allotment = is_available_allotment( $return_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $return_start_point, $return_end_point, $agid, false, $return_id );
                }

                if( $available_depart && $available_arrival && $available_date )
                {
                    if( $is_available_allotment )
                    {
                        $p  = get_result_price( $d['sid'], $d['rid'], $return_date, $return_start_point, $return_end_point, $adult, $child, $infant, $agid, $return_dt );
                        $rd = get_route_detail_content( $d['rid'], $return_start_point, $return_end_point );

                        $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                        $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                        $trip_inc_transport_val = base64_encode( json_encode( array( $d['sid'], 1 ) ) );
                        $trip_exc_transport_val = base64_encode( json_encode( array( $d['sid'], 0 ) ) );

                        $trip_inc_transport_id  = str_replace( '=', '', $trip_inc_transport_val );
                        $trip_exc_transport_id  = str_replace( '=', '', $trip_exc_transport_val );

                        $current_transport_val  = base64_encode( json_encode( array( $return_sid, intval( $return_tt ) ) ) );

                        if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                        {
                            add_variable( 'sid', $d['sid'] );
                            add_variable( 'rid', $d['rid'] );
                            add_variable( 'boid', $d['boid'] );
                            add_variable( 'bdid', $return_id );
                            add_variable( 'boname', $d['boname'] );
                            add_variable( 'bddate', $return_date );

                            add_variable( 'bddeparttime', $rd['depart_time'] );
                            add_variable( 'bdarrivetime', $rd['arrive_time'] );

                            add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                            add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                            add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                            add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                            add_variable( 'trip_inc_transport_id', $trip_inc_transport_id );
                            add_variable( 'trip_exc_transport_id', $trip_exc_transport_id );

                            add_variable( 'trip_inc_transport_check', $trip_inc_transport_val == $current_transport_val ? 'checked' : '' );
                            add_variable( 'trip_exc_transport_check', $trip_exc_transport_val == $current_transport_val ? 'checked' : '' );

                            if( in_array( $data['bpaymethod'], $compliment ) )
                            {
                                add_variable( 'inc_adult_price', 0 );
                                add_variable( 'inc_child_price', 0 );
                                add_variable( 'inc_infant_price', 0 );

                                add_variable( 'exc_adult_price', 0 );
                                add_variable( 'exc_child_price', 0 );
                                add_variable( 'exc_infant_price', 0 );

                                add_variable( 'discount_notif', '' );

                                add_variable( 'inc_disc_price', 0 );
                                add_variable( 'exc_disc_price', 0 );

                                add_variable( 'inc_disc_adult', 0 );
                                add_variable( 'inc_disc_child', 0 );
                                add_variable( 'inc_disc_infant', 0 );

                                add_variable( 'exc_disc_adult', 0 );
                                add_variable( 'exc_disc_child', 0 );
                                add_variable( 'exc_disc_infant', 0 );

                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                add_variable( 'include_price_number', 'Free' );
                                add_variable( 'exclude_price_number', 'Free' );

                                add_variable( 'include_price_disc_number', '' );
                                add_variable( 'exclude_price_disc_number', '' );

                                add_variable( 'inc_total_price', 0 );
                                add_variable( 'exc_total_price', 0 );

                                add_variable( 'include_css', '' );
                                add_variable( 'exclude_css', '' );
                            }
                            else
                            {
                                add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) || !empty( $p['fix_discount'] ) )
                                {
                                    if( !empty( $p['early_discount'] ) )
                                    {
                                        if( $p['early_discount']['type'] == '0' )
                                        {
                                            $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                            $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                            $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                            $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                            $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                            $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                            $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                            $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                        }
                                        else
                                        {
                                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                            $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                            $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                        }

                                        add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                    }

                                    if( !empty( $p['seat_discount'] ) )
                                    {
                                        if( $p['seat_discount']['type'] == '0' )
                                        {
                                            $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                            $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                            $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                            $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                            $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                            $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                            $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                            $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                        }
                                        else
                                        {
                                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                            $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                            $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                        }

                                        add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                    }

                                    if( !empty( $p['fix_discount'] ) )
                                    {
                                        $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['fix_discount']['disc_per_adult'] : 0;
                                        $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['fix_discount']['disc_per_child'] : 0;
                                        $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['fix_discount']['disc_per_infant'] : 0;

                                        $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['fix_discount']['disc_per_adult'] : 0;
                                        $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['fix_discount']['disc_per_child'] : 0;
                                        $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['fix_discount']['disc_per_infant'] : 0;

                                        $inc_disc_price  = ( $inc_disc_adult * $adult ) + ( $inc_disc_child * $child ) + ( $inc_disc_infant * $infant );
                                        $exc_disc_price  = ( $exc_disc_adult * $adult ) + ( $exc_disc_child * $child ) + ( $exc_disc_infant * $infant );
                                    }

                                    add_variable( 'inc_disc_price', $inc_disc_price );
                                    add_variable( 'exc_disc_price', $exc_disc_price );

                                    add_variable( 'inc_disc_adult', $inc_disc_adult );
                                    add_variable( 'inc_disc_child', $inc_disc_child );
                                    add_variable( 'inc_disc_infant', $inc_disc_infant );

                                    add_variable( 'exc_disc_adult', $exc_disc_adult );
                                    add_variable( 'exc_disc_child', $exc_disc_child );
                                    add_variable( 'exc_disc_infant', $exc_disc_infant );

                                    add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                    add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                    add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                    add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                    add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                    add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                    add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                    add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                                    add_variable( 'include_price_disc_number', '<span class="disc-price display-'. $trip_inc_transport_id .' dis-not-'. $trip_inc_transport_id .'" data-a-sep="." data-a-dec="," data-m-dec="0">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                    add_variable( 'exclude_price_disc_number', '<span class="disc-price display-'. $trip_exc_transport_id .' dis-not-'. $trip_exc_transport_id .'" data-a-sep="." data-a-dec="," data-m-dec="0">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                }
                                else
                                {
                                    add_variable( 'discount_notif', '' );

                                    add_variable( 'inc_disc_price', 0 );
                                    add_variable( 'exc_disc_price', 0 );

                                    add_variable( 'inc_disc_adult', 0 );
                                    add_variable( 'inc_disc_child', 0 );
                                    add_variable( 'inc_disc_infant', 0 );

                                    add_variable( 'exc_disc_adult', 0 );
                                    add_variable( 'exc_disc_child', 0 );
                                    add_variable( 'exc_disc_infant', 0 );

                                    add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                    add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                    add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                    add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                    add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                    add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                    add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                    add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                    add_variable( 'include_price_disc_number', '' );
                                    add_variable( 'exclude_price_disc_number', '' );
                                }

                                add_variable( 'inc_total_price', $inc_total_price );
                                add_variable( 'exc_total_price', $exc_total_price );

                                add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );
                            }

                            add_variable( 'rtdid', $p['rtdid'] );
                            add_variable( 'bdcdate', isset( $return_dt['bdcdate'] ) ? $return_dt['bdcdate'] : '' );
                            add_variable( 'bdstatus', isset( $return_dt['bdstatus'] ) ? $return_dt['bdstatus'] : 'aa' );
                            add_variable( 'bdpstatus', isset( $return_dt['bdpstatus'] ) ? $return_dt['bdpstatus'] : '' );
                            add_variable( 'bdcreason', isset( $return_dt['bdcreason'] ) ? $return_dt['bdcreason'] : '' );
                            add_variable( 'bdcmessage', isset( $return_dt['bdcmessage'] ) ? $return_dt['bdcmessage'] : '' );
                            add_variable( 'bdcancelfee', isset( $return_dt['bdcancelfee'] ) ? $return_dt['bdcancelfee'] : 0 );
                            add_variable( 'bdrevstatus', isset( $return_dt['bdrevstatus'] ) ? $return_dt['bdrevstatus'] : 'Tentative' );
                            add_variable( 'bdtransactionfee', isset( $return_dt['bdtransactionfee'] ) ? $return_dt['bdtransactionfee'] : '' );
                            add_variable( 'bdcancelpercentfee', isset( $return_dt['bdcancelpercentfee'] ) ? $return_dt['bdcancelpercentfee'] : '' );

                            add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                            add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $return_start_point, $return_end_point ) );

                            add_variable( 'include_transport', get_edit_transport_option( $d, $return_start_point, $return_end_point, 'return', $pass_num, $return_dt ) );
                            add_variable( 'exclude_transport', get_edit_transport_option( $d, $return_start_point, $return_end_point, 'return', $pass_num, $return_dt, true ) );

                            parse_template( 'return-trip-loop-block', 'rtrl-block', true );

                            $i++;
                        }
                        else
                        {
                            if( in_array( $data['bpaymethod'], $compliment ) || !empty( $return_dt ) )
                            {
                                add_variable( 'sid', $d['sid'] );
                                add_variable( 'rid', $d['rid'] );
                                add_variable( 'boid', $d['boid'] );
                                add_variable( 'bdid', $return_id );
                                add_variable( 'boname', $d['boname'] );
                                add_variable( 'bddate', $return_date );

                                add_variable( 'bddeparttime', $rd['depart_time'] );
                                add_variable( 'bdarrivetime', $rd['arrive_time'] );

                                add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                                add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                                add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                                add_variable( 'trip_inc_transport_check', $trip_inc_transport_val == $current_transport_val ? 'checked' : '' );
                                add_variable( 'trip_exc_transport_check', $trip_exc_transport_val == $current_transport_val ? 'checked' : '' );

                                add_variable( 'inc_adult_price', 0 );
                                add_variable( 'inc_child_price', 0 );
                                add_variable( 'inc_infant_price', 0 );

                                add_variable( 'exc_adult_price', 0 );
                                add_variable( 'exc_child_price', 0 );
                                add_variable( 'exc_infant_price', 0 );

                                add_variable( 'discount_notif', '' );

                                add_variable( 'inc_disc_price', 0 );
                                add_variable( 'exc_disc_price', 0 );

                                add_variable( 'inc_disc_adult', 0 );
                                add_variable( 'inc_disc_child', 0 );
                                add_variable( 'inc_disc_infant', 0 );

                                add_variable( 'exc_disc_adult', 0 );
                                add_variable( 'exc_disc_child', 0 );
                                add_variable( 'exc_disc_infant', 0 );

                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                add_variable( 'include_price_number', 'Free' );
                                add_variable( 'exclude_price_number', 'Free' );

                                add_variable( 'include_price_disc_number', '' );
                                add_variable( 'exclude_price_disc_number', '' );

                                add_variable( 'inc_total_price', 0 );
                                add_variable( 'exc_total_price', 0 );

                                add_variable( 'include_css', '' );
                                add_variable( 'exclude_css', '' );

                                add_variable( 'rtdid', $p['rtdid'] );
                                add_variable( 'bdcdate', isset( $return_dt['bdcdate'] ) ? $return_dt['bdcdate'] : '' );
                                add_variable( 'bdstatus', isset( $return_dt['bdstatus'] ) ? $return_dt['bdstatus'] : 'aa' );
                                add_variable( 'bdpstatus', isset( $return_dt['bdpstatus'] ) ? $return_dt['bdpstatus'] : '' );
                                add_variable( 'bdcreason', isset( $return_dt['bdcreason'] ) ? $return_dt['bdcreason'] : '' );
                                add_variable( 'bdcmessage', isset( $return_dt['bdcmessage'] ) ? $return_dt['bdcmessage'] : '' );
                                add_variable( 'bdcancelfee', isset( $return_dt['bdcancelfee'] ) ? $return_dt['bdcancelfee'] : 0 );
                                add_variable( 'bdrevstatus', isset( $return_dt['bdrevstatus'] ) ? $return_dt['bdrevstatus'] : 'Tentative' );
                                add_variable( 'bdtransactionfee', isset( $return_dt['bdtransactionfee'] ) ? $return_dt['bdtransactionfee'] : '' );
                                add_variable( 'bdcancelpercentfee', isset( $return_dt['bdcancelpercentfee'] ) ? $return_dt['bdcancelpercentfee'] : '' );

                                add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                                add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $return_start_point, $return_end_point ) );

                                add_variable( 'include_transport', get_edit_transport_option( $d, $return_start_point, $return_end_point, 'return', $pass_num, $return_dt ) );
                                add_variable( 'exclude_transport', get_edit_transport_option( $d, $return_start_point, $return_end_point, 'return', $pass_num, $return_dt, true ) );

                                parse_template( 'return-trip-loop-block', 'rtrl-block', true );

                                $i++;
                            }
                        }
                    }
                }
            }

            if( empty( $i )  )
            {
                parse_template( 'no-return-trip-loop-block', 'nrtrl-block' );
            }
        }
        else
        {
            parse_template( 'no-return-trip-loop-block', 'nrtrl-block' );
        }

        parse_template( 'return-trip-block', 'rtr-block' );
    }
}

function get_edit_transport_option( $dt, $from, $to, $trip_type, $passanger, $detail, $exclude = false )
{
    $content = '';
    $dtrans  = get_pickup_drop_list_data( $dt['rid'], $from );
    $atrans  = get_pickup_drop_list_data( $dt['rid'], $to );
    $count   = ceil( $passanger / 4 );

    $trip_inc_transport_val = base64_encode( json_encode( array( $dt['sid'], 1 ) ) );
    $trip_exc_transport_val = base64_encode( json_encode( array( $dt['sid'], 0 ) ) );

    if( isset( $dtrans['pickup'] ) )
    {
        if( isset( $detail[ 'transport' ][ 'pickup' ][ 0 ] ) )
        {
            extract( $detail[ 'transport' ][ 'pickup' ][ 0 ] );
        }
        else
        {
            $taid              = null;
            $hid               = null;
            $bttrans_type      = '';
            $btflighttime      = '';
            $btdrivername      = '';
            $btdriverphone     = '';
            $bthotelname       = '';
            $bthoteladdress    = '';
            $bthotelphone      = '';
            $bthotelemail      = '';
            $bthotelroomnumber = '';
            $taairport         = 0;
            $bttrans_fee       = 0;
        }

        if( $exclude )
        {
            $content .= '
            <div class="trans-opt-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="cols col-md-4">
                            <div class="form-group">
                                <b>Pickup : </b>
                                <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][pickup][bttrans_type]" autocomplete="off">
                                    <option value="2">Own Transport</option>
                                </select>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group driver-block">
                                <div class="fg-inner">
                                    <b>Driver Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][pickup][btdrivername]" value="' . $btdrivername . '" />
                                </div>
                                <div class="fg-inner">
                                    <b>Driver Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][pickup][btdriverphone]" value="' . $btdriverphone . '" />
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group trans-fee-block">
                                <b>Transport Fee : </b>
                                <div class="input-group">
                                    <div class="input-group-addon">IDR</div>
                                    <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][pickup][bttrans_fee]" value="0" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }
        else
        {
            if( $bttrans_type == '2' )
            {
                $with_trans_field_sts = 'disabled';
                $no_trans_field_sts   = '';

                $with_trans_css = 'sr-only';
                $no_trans_css   = '';
            }
            else
            {
                $with_trans_field_sts = '';
                $no_trans_field_sts   = 'disabled';

                $with_trans_css = '';
                $no_trans_css   = 'sr-only';
            }

            $content .= '
            <div class="trans-opt-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="cols col-md-4">
                            <div class="form-group">
                                <b>Pickup : </b>
                                <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bttrans_type]" autocomplete="off">
                                    <option value="0" ' . ( $bttrans_type == '0' ? 'selected' : '' ) . '>Shared Transport</option>
                                    <option value="1" ' . ( $bttrans_type == '1' ? 'selected' : '' ) . '>Private Transport</option>
                                    <option value="2" ' . ( $bttrans_type == '2' ? 'selected' : '' ) . '>Own Transport</option>
                                </select>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group hotel-block ' . $with_trans_css . '">
                                <div class="fg-inner">
                                    <b>Accommodation : </b>
                                    <select class="select-option hotels-area required" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][taid]" autocomplete="off" data-error="Area can\'t be empty" ' . $with_trans_field_sts . '>
                                        ' . get_transport_area_option_by_all( $taid, $dtrans['pickup'], $count ) . '
                                    </select>
                                </div>
                                <div class="fg-inner">
                                    <select class="select-option hotels required" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" ' . ( is_null( $hid ) ? 'disabled' : $with_trans_field_sts ) . '>
                                        ' . get_transport_hotel_option_by_all( $hid, $dtrans['pickup'], $count ) . '
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hotel-detail-block ' . ( $hid == 1 ? $with_trans_css : 'sr-only' ) . '">
                                <div class="fg-inner">
                                    <b>Hotel Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthotelname]" value="' . $bthotelname . '" ' . $with_trans_field_sts . '/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Address : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthoteladdress]" value="' . $bthoteladdress . '" ' . $with_trans_field_sts . '/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthotelphone]" value="' . $bthotelphone . '" ' . $with_trans_field_sts . '/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Email : </b>
                                    <input type="email" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthotelemail]" value="' . $bthotelemail . '" ' . $with_trans_field_sts . '/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Room Number : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthotelroomnumber]" value="' . $bthotelroomnumber . '" ' . $with_trans_field_sts . '/>
                                </div>
                            </div>
                            <div class="form-group flight-time ' . ( $taairport == 1 ? $with_trans_css : 'sr-only' ) . '">
                                <b>Flight Landing Time : </b>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                    <input type="text" class="trans-ftime text form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][btflighttime]" value="' . ( !empty( $btflighttime ) && $btflighttime != '0000-00-00 00:00:00' ? date( 'D F Y, H:i:s' ) : '' ) . '" readonly="readonly" ' . $with_trans_field_sts . '/>
                                </div>
                            </div>
                            <div class="form-group driver-block ' . $no_trans_css . '">
                                <div class="fg-inner">
                                    <b>Driver Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][btdrivername]" value="' . $btdrivername . '" ' . $no_trans_field_sts . '/>
                                </div>
                                <div class="fg-inner">
                                    <b>Driver Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][btdriverphone]" value="' . $btdriverphone . '" ' . $no_trans_field_sts . '/>
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group trans-fee-block">
                                <b>Transport Fee : </b>
                                <div class="input-group">
                                    <div class="input-group-addon">IDR</div>
                                    <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bttrans_fee]" value="' . $bttrans_fee . '" ' . ( $bttrans_type == '1' ? '' : 'readonly="readonly"' ) . ' />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }
    }

    if( isset( $atrans['drop-off'] ) )
    {
        if( isset( $detail[ 'transport' ][ 'drop-off' ][ 0 ] ) )
        {
            extract( $detail[ 'transport' ][ 'drop-off' ][ 0 ] );
        }
        else
        {
            $taid              = null;
            $hid               = null;
            $bttrans_type      = '';
            $btflighttime      = '';
            $btdrivername      = '';
            $btdriverphone     = '';
            $bthotelname       = '';
            $bthoteladdress    = '';
            $bthotelphone      = '';
            $bthotelemail      = '';
            $bthotelroomnumber = '';
            $taairport         = 0;
            $bttrans_fee       = 0;
        }

        if( $exclude )
        {
            $content .= '
            <div class="trans-opt-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="cols col-md-4">
                            <div class="form-group">
                                <b>Drop-off : </b>
                                <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][drop-off][bttrans_type]" autocomplete="off">
                                    <option value="2">Own Transport</option>
                                </select>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group driver-block">
                                <div class="fg-inner">
                                    <b>Driver Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][drop-off][btdrivername]" value="' . $btdrivername . '" />
                                </div>
                                <div class="fg-inner">
                                    <b>Driver Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][drop-off][btdriverphone]" value="' . $btdriverphone . '" />
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group trans-fee-block">
                                <b>Transport Fee : </b>
                                <div class="input-group">
                                    <div class="input-group-addon">IDR</div>
                                    <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][drop-off][bttrans_fee]" value="0" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }
        else
        {
            if( $bttrans_type == '2' )
            {
                $with_trans_field_sts = 'disabled';
                $no_trans_field_sts   = '';

                $with_trans_css = 'sr-only';
                $no_trans_css   = '';
            }
            else
            {
                $with_trans_field_sts = '';
                $no_trans_field_sts   = 'disabled';

                $with_trans_css = '';
                $no_trans_css   = 'sr-only';
            }

            $content .= '
            <div class="trans-opt-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="cols col-md-4">
                            <div class="form-group">
                                <b>Drop-off : </b>
                                <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bttrans_type]" autocomplete="off">
                                    <option value="0" ' . ( $bttrans_type == '0' ? 'selected' : '' ) . '>Shared Transport</option>
                                    <option value="1" ' . ( $bttrans_type == '1' ? 'selected' : '' ) . '>Private Transport</option>
                                    <option value="2" ' . ( $bttrans_type == '2' ? 'selected' : '' ) . '>Own Transport</option>
                                </select>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group hotel-block ' . $with_trans_css . '">
                                <div class="fg-inner">
                                    <b>Accommodation : </b>
                                    <select class="select-option hotels-area required" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][taid]" autocomplete="off" data-error="Area can\'t be empty" ' . $with_trans_field_sts . '>
                                        ' . get_transport_area_option_by_all( $taid, $atrans['drop-off'], $count ) . '
                                    </select>
                                </div>
                                <div class="fg-inner">
                                    <select class="select-option hotels required" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" ' . ( is_null( $hid ) ? 'disabled' : '' ) . '>
                                        ' . get_transport_hotel_option_by_all( $hid, $atrans['drop-off'], $count ) . '
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hotel-detail-block ' . ( $hid == 1 ? $with_trans_css : 'sr-only' ) . '">
                                <div class="fg-inner">
                                    <b>Hotel Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthotelname]" value="' . $bthotelname . '" ' . $with_trans_field_sts . '/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Address : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthoteladdress]" value="' . $bthoteladdress . '" ' . $with_trans_field_sts . '/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthotelphone]" value="' . $bthotelphone . '"' . $with_trans_field_sts . ' />
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Email : </b>
                                    <input type="email" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthotelemail]" value="' . $bthotelemail . '" ' . $with_trans_field_sts . '/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Room Number : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthotelroomnumber]" value="' . $bthotelroomnumber . '" ' . $with_trans_field_sts . '/>
                                </div>
                            </div>
                            <div class="form-group flight-time ' . ( $taairport == 1 ? $with_trans_css : 'sr-only' ) . '">
                                <b>Flight Landing Time : </b>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                    <input type="text" class="trans-ftime text form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][btflighttime]" value="' . ( !empty( $btflighttime ) && $btflighttime != '0000-00-00 00:00:00' ? date( 'D F Y, H:i:s' ) : '' ) . '" readonly="readonly" ' . $with_trans_field_sts . '/>
                                </div>
                            </div>
                            <div class="form-group driver-block ' . $no_trans_css . '">
                                <div class="fg-inner">
                                    <b>Driver Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][btdrivername]" value="' . $btdrivername . '" ' . $no_trans_field_sts . '/>
                                </div>
                                <div class="fg-inner">
                                    <b>Driver Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][btdriverphone]" value="' . $btdriverphone . '" ' . $no_trans_field_sts . '/>
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group trans-fee-block">
                                <b>Transport Fee : </b>
                                <div class="input-group">
                                    <div class="input-group-addon">IDR</div>
                                    <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bttrans_fee]" value="' . $bttrans_fee . '" ' . ( $bttrans_type == '1' ? '' : 'readonly="readonly"' ) . ' />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }
    }

    return $content;
}

function get_transport_option( $rid, $from, $to, $trip_type, $passanger )
{
    $content = '';
    $dtrans  = get_pickup_drop_list_data( $rid, $from );
    $atrans  = get_pickup_drop_list_data( $rid, $to );
    $count   = ceil( $passanger / 4 );

    if( isset( $dtrans['pickup'] ) )
    {
        $content .= '
        <div class="trans-opt-block">
            <div class="form-inline inner">
                <div class="form-group">
                    <b>Pickup : </b>
                    <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $rid . '][pickup][bttrans_type]" autocomplete="off">
                        <option value="0">Shared Transport</option>
                        <option value="1">Private Transport</option>
                        <option value="2">Own Transport</option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="select-option hotels-area required" name="transport[' . $trip_type . '][' . $rid . '][pickup][taid]" autocomplete="off" data-error="Area can\'t be empty">
                        ' . get_transport_area_option_by_all( null, $dtrans['pickup'], $count ) . '
                    </select>
                    <div class="flight-time sr-only">
                        <b>Flight Landing Time : </b>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                            <input type="text" class="trans-ftime text form-control" name="transport[' . $trip_type . '][' . $rid . '][pickup][btflighttime]" value="" readonly="readonly" />
                        </div>
                    </div>
                    <div class="driver-block sr-only">
                        <b>Driver Name : </b>
                        <input type="text" class="text text-full form-control" value="" name="transport[' . $trip_type . '][' . $rid . '][pickup][btdrivername]" />
                    </div>
                    <div class="driver-block sr-only">
                        <b>Driver Phone : </b>
                        <input type="text" class="text text-full form-control" value="" name="transport[' . $trip_type . '][' . $rid . '][pickup][btdriverphone]" />
                    </div>
                    <div class="trans-fee-block">
                        <b>Transport Fee : </b>
                        <div class="input-group">
                            <div class="input-group-addon">IDR</div>
                            <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $rid . '][pickup][bttrans_fee]" value="0" readonly="readonly" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <select class="select-option hotels required" name="transport[' . $trip_type . '][' . $rid . '][pickup][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" disabled>
                        ' . get_transport_hotel_option_by_all( null, $dtrans['pickup'], $count ) . '
                    </select>
                </div>
            </div>
        </div>';
    }

    if( isset( $atrans['drop-off'] ) )
    {
        $content .= '
        <div class="trans-opt-block">
            <div class="form-inline inner">
                <div class="form-group">
                    <b>Drop-off : </b>
                    <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $rid . '][drop-off][bttrans_type]" autocomplete="off">
                        <option value="0">Shared Transport</option>
                        <option value="1">Private Transport</option>
                        <option value="2">Own Transport</option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="select-option hotels-area required" name="transport[' . $trip_type . '][' . $rid . '][drop-off][taid]" autocomplete="off" data-error="Area can\'t be empty">
                        ' . get_transport_area_option_by_all( null, $atrans['drop-off'], $count ) . '
                    </select>
                    <div class="flight-time sr-only">
                        <b>Flight Take Off Time : </b>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                            <input type="text" class="trans-ftime text form-control" name="transport[' . $trip_type . '][' . $rid . '][drop-off][btflighttime]" value="" readonly="readonly" />
                        </div>
                    </div>
                    <div class="driver-block sr-only">
                        <b>Driver Name : </b>
                        <input type="text" class="text text-full form-control" value="" name="transport[' . $trip_type . '][' . $rid . '][drop-off][btdrivername]" />
                    </div>
                    <div class="driver-block sr-only">
                        <b>Driver Phone : </b>
                        <input type="text" class="text text-full form-control" value="" name="transport[' . $trip_type . '][' . $rid . '][drop-off][btdriverphone]" />
                    </div>
                    <div class="trans-fee-block">
                        <b>Transport Fee : </b>
                        <div class="input-group">
                            <div class="input-group-addon">IDR</div>
                            <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $rid . '][drop-off][bttrans_fee]" value="0"readonly="readonly" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <select class="select-option hotels required" name="transport[' . $trip_type . '][' . $rid . '][drop-off][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" disabled>
                        ' . get_transport_hotel_option_by_all( null, $atrans['drop-off'], $count ) . '
                    </select>
                </div>
            </div>
        </div>';
    }

    return $content;
}

function run_booking_update( $from_agent = false, $data=array() )
{
    global $flash;

    if( isset( $_POST['update_booking'] ) )
    {
        $filter = isset( $_GET['filter'] ) ? '&filter=' . $_GET['filter'] : '';

        if( ticket_booking_update_data( $_POST, $data, $from_agent ) )
        {
            if( $from_agent )
            {
                send_updated_booking_ticket_to_agent( $_GET['id'] );
                send_booking_updated_notif_to_admin( $_GET['id'], true );

                $flash->add( array( 'type'=> 'success', 'content' => array( 'Your reservation data has been updated' ) ) );

                header( 'Location:' . get_agent_admin_url( 'reservation&sub=booking&prc=edit&id=' . $_GET['id'] . $filter ) );

                exit;
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'Your reservation data has been updated' ) ) );

                header( 'Location:' . get_state_url( 'reservation&sub=booking&prc=edit&id=' . $_GET['id'] . $filter ) );

                exit;
            }
        }
        else
        {
            if( $from_agent )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to update your reservation data' ) ) );

                header( 'Location:' . get_agent_admin_url( 'reservation&sub=booking&prc=edit&id=' . $_GET['id'] . $filter ) );

                exit;
            }
            else
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to update your reservation data' ) ) );

                header( 'Location:' . get_state_url( 'reservation&sub=booking&prc=edit&id=' . $_GET['id'] . $filter ) );

                exit;
            }
        }
    }
    elseif( isset( $_POST['cancel_booking'] ) )
    {
        if( ticket_booking_canceled_data( $_POST ) )
        {
            $_SESSION['booking_edit_status'] = 'success';

            return array( 'type'=> 'success', 'content' => array( 'Your cancelation request has been sent, please wait for confirmation' ) );
        }
        else
        {
            $_SESSION['booking_edit_status'] = 'error';

            return array( 'type'=> 'error', 'content' => array( 'Your cancelation request has been failed to send' ) );
        }
    }
    elseif( isset( $_POST['send_feedback'] ) )
    {
        if( ticket_booking_send_feedback( $_POST ) )
        {
            if ( send_email_feedback_submit( $_POST ) )
            {
                $_SESSION['booking_edit_status'] = 'success';

                return array( 'type'=> 'success', 'content' => array( 'Thank you, Your feedback has been sent' ) );
            }
        }
        else
        {
            $_SESSION['booking_edit_status'] = 'error';

            return array( 'type'=> 'error', 'content' => array( 'Sorry, Your feedback has been failed to send' ) );
        }
    }
}

function get_message_after_submit()
{
    if( isset( $_SESSION['booking_edit_status'] ) )
    {
        if( $_SESSION['booking_edit_status'] == 'success' )
        {
            $message = array( 'type'=> 'success', 'content' => array( 'Reservation data successfully edited' ) );
        }
        elseif( $_SESSION['booking_edit_status'] == 'error' )
        {
            $message = array( 'type'=> 'error', 'content' => array( 'Reservation data failed to edited' ) );
        }

        unset( $_SESSION['booking_edit_status'] );
    }
    else
    {
        $message = '';
    }

    return $message;
}

function get_transport_type( $type = 0, $initial = false )
{
    if( empty( $type ) )
    {
        return $initial ? 'S' : 'Shared Transport';
    }
    elseif( $type == 1 )
    {
        return $initial ? 'P' : 'Private Transport';
    }
    elseif( $type == 2 )
    {
        return $initial ? 'O' : 'Own Transport';
    }
}

function get_nationality( $id, $field = '' )
{
    global $db;

    $s = 'SELECT * FROM l_country WHERE lcountry_id = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( empty( $field ) )
    {
        return $d;
    }
    else
    {
        return $d[ $field ];
    }
}

function get_booking_payment_method( $data )
{
    global $db;

    if( empty( $data['agid'] ) )
    {
        $s = 'SELECT pmethod FROM ticket_booking_payment AS a WHERE a.bid = %d ORDER BY a.ppaydate DESC, a.pid DESC LIMIT 1 ';
        $q = $db->prepare_query( $s, $data['bid'] );
        $r = $db->do_query( $q );
        $d = $db->fetch_array( $r );

        if( empty( $d['pmethod'] ) )
        {
            return get_payment_method_by_id( $data['bpaymethod'] );
        }
        else
        {
            return $d['pmethod'];
        }
    }
    else
    {
        $s = 'SELECT atmethod FROM ticket_agent_transaction AS a WHERE a.agid = %d AND a.bid = %d AND a.atstatus = %d ORDER BY a.atdate DESC, a.atid DESC LIMIT 1 ';
        $q = $db->prepare_query( $s, $data['agid'], $data['bid'], 2 );
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            return get_payment_method_by_id( $data['bpaymethod'] );
        }
        else
        {
            $d = $db->fetch_array( $r );

            if( empty( $d['atmethod'] ) )
            {
                return get_payment_method_by_id( $data['bpaymethod'] );
            }
            else
            {
                return $d['atmethod'];
            }
        }
    }
}

function get_boarding_pass_transport_content( $data )
{
    if( isset( $data['transport'] ) )
    {
        $content = '';

        foreach( $data['transport'] as $ttype => $obj )
        {
            foreach( $obj as $d )
            {
                if( is_null( $d['taid'] ) )
                {
                    $dttrans  = 'Own Transport / ' . $d['btdrivername'] . ' / ' . $d['btdriverphone'];
                    $dttrans2 = strlen( $dttrans ) > 25 ? substr( $dttrans, 0, 25 ) . '...' : $dttrans;
                    $content .= '
                    <tr>
                        <td>' . strtoupper( $ttype ) . ' :</td>
                        <td>' . strtoupper( $ttype ) . ' :</td>
                        <td>' . strtoupper( $ttype ) . ' :</td>
                    </tr>
                    <tr>
                      <td>' . $dttrans2 . '</td>
                      <td>' . $dttrans2 . '</td>
                      <td>' . $dttrans . '</td>
                    </tr>';
                }
                else
                {
                    if( $d['bthotelname'] == '' )
                    {
                        $hotelname = $d['hname'];
                        $hname     = $d['hname'];
                    }
                    else
                    {
                        $hotelname = strlen( $d['bthotelname'] ) > 20 ? mb_substr( $d['bthotelname'], 0, 19 ) . '...' : $d['bthotelname'];
                        $hname     = $d['bthotelname'];
                    }

                    $content  .= '
                    <tr>
                        <td>' . strtoupper( $ttype ) . ' :</td>
                        <td>' . strtoupper( $ttype ) . ' :</td>
                        <td>' . strtoupper( $ttype ) . ' :</td>
                    </tr>
                    <tr>
                      <td>' . $hotelname . '</td>
                      <td>' . $hotelname . '</td>
                      <td>' . $hname . '</td>
                    </tr>';
                }
            }
        }

        return $content;
    }
}

function get_booking_status_option( $sts = array( 'pa', 'ca', 'pp' ), $use_empty = true, $empty_text = 'Select Status' )
{
    $status = ticket_booking_status();
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $status ) )
    {
        foreach( $status as $st => $stat )
        {
            if( is_array( $sts ) )
            {
                $option .= '<option value="' . $st . '" ' . ( in_array( $st, $sts ) ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
            else
            {
                $sts = $sts == 'ol' ? 'pp' : $sts;

                $option .= '<option value="' . $st . '" ' . ( $st == $sts ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
        }
    }

    return $option;
}

// bayu edit nambahin fungsi booking method option
function get_booking_method_option( $sts = '', $use_empty = true, $empty_text = 'Select Booking Method' )
{
    $status = array(
        'fe' => 'Direct Online',
        'ag' => 'Direct Panel',
        'ad' => 'Admin'
    );
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $status ) )
    {
        foreach( $status as $st => $stat )
        {
            if( is_array( $sts ) )
            {
                $option .= '<option value="' . $st . '" ' . ( in_array( $st, $sts ) ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
            else
            {
                $sts = $sts == 'ol' ? 'pp' : $sts;

                $option .= '<option value="' . $st . '" ' . ( $st == $sts ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
        }
    }

    return $option;
}

function get_gender_option( $gender = '', $use_empty = true, $empty_text = 'Select Gender' )
{
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="1" ' . ( $gender == '1' ? 'selected' : '' ) . ' >Male</option>
    <option value="0" ' . ( $gender == '0' ? 'selected' : '' ) . ' >Female</option>';

    return $option;
}

function get_revtype_option( $revtype = '', $use_empty = true, $empty_text = 'Select Reservation Type' )
{
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="FIT" ' . ( $revtype == 'FIT' ? 'selected' : '' ) . ' >FIT</option>
    <option value="Group" ' . ( $revtype == 'Group' ? 'selected' : '' ) . ' >Group</option>
    <option value="Charter" ' . ( $revtype == 'Charter' ? 'selected' : '' ) . ' >Charter</option>';

    return $option;
}

function get_revstatus_option( $revstatus = '', $use_empty = true, $empty_text = 'Select Reservation Status' )
{
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="Devinitive" ' . ( $revstatus == 'Devinitive' ? 'selected' : '' ) . ' >Devinitive</option>
    <option value="Tentative" ' . ( $revstatus == 'Tentative' ? 'selected' : '' ) . ' >Tentative</option>
    <option value="Blocking" ' . ( $revstatus == 'Blocking' ? 'selected' : '' ) . ' >Blocking</option>
    <option value="No-Show" ' . ( $revstatus == 'No-Show' ? 'selected' : '' ) . ' >No-Show</option>
    <option value="Other Boat" ' . ( $revstatus == 'Other Boat' ? 'selected' : '' ) . ' >Other Boat</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Trip Option
| -------------------------------------------------------------------------------------
*/
function get_trip_option( $tid = '', $use_empty = true, $empty_text = 'Select Trip' )
{
    global $db;

    $s = 'SELECT a.bdfrom, a.bdto FROM ticket_booking_detail as a';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $trip = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $trip[] = $d['bdfrom'] . ' - ' . $d['bdto'];
    }

    $data   = array_unique( $trip );
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $data ) )
    {
        if ( is_array( $tid ) )
        {
            foreach( $data as $d )
            {
                $option .= '<option value="' . $d . '" ' . ( in_array( $d, $tid ) ? 'selected' : '' ) . '>' . $d . '</option>';
            }
        }
        else
        {
            foreach( $data as $d )
            {
                $option .= '<option value="' . $d . '" ' . ( $d == $tid ? 'selected' : '' ) . '>' . $d . '</option>';
            }
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Payment Method Option
| -------------------------------------------------------------------------------------
*/
function get_booking_payment_method_option( $mid = '', $pmethod = '' )
{
    $option  = '<option value="">Select Payment Method</option>';
    $method  = get_payment_method();

    foreach( $method as $idx => $d )
    {
        if( $d['mid'] == 5 )
        {
            $option .= '
            <option value="' . $d['mid'] . '|Nicepay Credit Card" ' . ( $pmethod == 'Nicepay Credit Card' ? 'selected' : '' ) . '>Nicepay Credit Card</option>
            <option value="' . $d['mid'] . '|Doku" ' . ( $pmethod == 'Doku' ? 'selected' : '' ) . '>Doku</option>';
        }
        elseif( $d['mid'] == 2 )
        {
            $option .= '<option value="' . $d['mid'] . '|Nicepay Transfer" ' . ( $d['mid'] == $mid ? 'selected' : '' ) . '>Nicepay Transfer</option>';
        }
        else
        {
            $option .= '<option value="' . $d['mid'] . '|' . $d['mname'] . '" ' . ( $d['mid'] == $mid ? 'selected' : '' ) . '>' . $d['mname'] . '</option>';
        }
    }

    return $option;
}

function get_booking_payment_method_option_2( $mname , $use_empty = true, $empty_text = 'Select Payment Method')
{
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $method  = get_payment_method();

    foreach( $method as $idx => $d )
    {
        $option .= '<option value="' . $d['mname'] . '" ' . ( $d['mname'] == $mname ? 'selected' : '' ) . '>' . $d['mname'] . '</option>';
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Payment Gateway Option
| -------------------------------------------------------------------------------------
*/
function get_booking_payment_gateway_option( $method = '', $use_empty = true, $empty_text = 'Select Payment Gateway' )
{
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="Nicepay Credit Card" ' . ( $method == 'Nicepay Credit Card' ? 'selected' : '' ) . '>Nicepay Credit Card</option>
    <option value="Doku" ' . ( $method == 'Doku' ? 'selected' : '' ) . '>Doku</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Payment Status Option
| -------------------------------------------------------------------------------------
*/
function get_booking_payment_status_option( $status = 0 )
{
    $option = '
    <option value="0" ' . ( $status == '0' ? 'selected' : '' ) . '>Not paid yet</option>
    <option value="1" ' . ( $status == '1' ? 'selected' : '' ) . '>Already Paid</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Email Function
| -------------------------------------------------------------------------------------
*/
function ticket_booking_send_email()
{
    extract( $_GET );

    if( $type == 1 )
    {
        send_booking_notification_to_admin( $id );
    }
    elseif( $type == 2 )
    {
        send_payment_notification_to_admin( $id );
    }
    elseif( $type == 3 )
    {
        send_booking_cancelation_request_to_admin( $id );
    }
    elseif( $type == 4 )
    {
        $data = ticket_booking_all_data( $id  );

        send_booking_receipt_to_client( $data );
    }
    elseif( $type == 5 )
    {
        $data = ticket_booking_all_data( $id  );

        send_booking_ticket_to_client( $data );
    }
    elseif( $type == 6 )
    {
        $data = ticket_booking_all_data( $id  );

        send_booking_receipt_and_ticket_to_client( $data );
    }
    elseif( $type == 7 )
    {
        $data = ticket_booking_all_data( $id  );

        send_reconfirmation_to_client( $data );
    }

    exit;
}

function validate_booking_status( $data, $statur_arr = array( 'pa', 'ca' ) )
{
    $status = array();

    foreach( $data['detail'] as $bdtype => $detail )
    {
        foreach( $detail as $bdid => $d )
        {
            $status[] = $d['bdpstatus'];
        }
    }

    if( count( array_intersect( $statur_arr, $status ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function send_booking_notification_to_admin( $bid )
{
    $data = ticket_booking_all_data( $bid  );

    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = get_meta_data('email_reservation');
            $email_to_2 = get_meta_data('email_reservation_2');
            $email_from = get_meta_data('smtp_email_address');

            if( empty( $email_to_2 )  )
            {
                $message  = array(
                    'subject'    => 'New Booking Inquiry #' . $data['bticket'],
                    'html'       => get_booking_notification_to_admin( $data ),
                    'from_name'  => get_meta_data('web_title'),
                    'from_email' => $email_from,
                    'to' => array(
                        array(
                            'email' => $email_to,
                            'name'  => '',
                            'type'  => 'to'
                        )
                    ),
                    'headers' => array( 'Reply-To' => $email_from )
                );
            }
            else
            {
                $message  = array(
                    'subject'    => 'New Booking Inquiry #' . $data['bticket'],
                    'html'       => get_booking_notification_to_admin( $data ),
                    'from_name'  => get_meta_data('web_title'),
                    'from_email' => $email_from,
                    'to' => array(
                        array(
                            'email' => $email_to,
                            'name'  => '',
                            'type'  => 'to'
                        ),
                        array(
                            'email' => $email_to_2,
                            'name'  => '',
                            'type'  => 'to'
                        )
                    ),
                    'headers' => array( 'Reply-To' => $email_from )
                );
            }

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

function send_payment_notification_to_admin( $bid, $email_pdf = '' )
{
    $data = ticket_booking_all_data( $bid  );

    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = get_meta_data('email_reservation');
            $email_to_2 = get_meta_data('email_reservation_2');
            $email_from = get_meta_data('smtp_email_address');
            $email_pdf  = empty( $email_pdf ) ? generate_booking_pdf_ticket( $data ) : $email_pdf;
            $email_pdf  = base64_encode( $email_pdf );

            //-- Delete Barcode After Encode
            if( file_exists( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' ) )
            {
                unlink( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' );
            }

            if( $data['bstatus'] == 'pf' )
            {
                $email_data = get_failed_payment_notification_to_admin( $data );
            }
            elseif( $data['bstatus'] == 'ol' )
            {
                $email_data = get_no_payment_notification_to_admin( $data );
            }
            elseif( $data['bstatus'] == 'pa' )
            {
                $email_data = get_payment_notification_to_admin( $data );
            }

            if( empty( $email_to_2 )  )
            {
                if( $data['bstatus'] == 'pa' )
                {
                    $message  = array(
                        'subject'    => get_payment_notification_to_admin_subject( $data ),
                        'from_name'  => get_meta_data('web_title'),
                        'from_email' => $email_from,
                        'html'       => $email_data,
                        'to' => array(
                            array(
                                'email' => $email_to,
                                'name'  => '',
                                'type'  => 'to'
                            )
                        ),
                        'attachments' => array(
                            array(
                                'type'    => 'application/pdf',
                                'name'    => 'ticket-' . $data['bticket'] . '.pdf',
                                'content' => $email_pdf
                            )
                        ),
                        'headers' => array( 'Reply-To' => $email_from )
                    );
                }
                else
                {
                    $message  = array(
                        'subject'    => get_payment_notification_to_admin_subject( $data ),
                        'from_name'  => get_meta_data('web_title'),
                        'from_email' => $email_from,
                        'html'       => $email_data,
                        'to' => array(
                            array(
                                'email' => $email_to,
                                'name'  => '',
                                'type'  => 'to'
                            )
                        ),
                        'headers' => array( 'Reply-To' => $email_from )
                    );
                }
            }
            else
            {
                if( $data['bstatus'] == 'pa' )
                {
                    $message  = array(
                        'subject'    => get_payment_notification_to_admin_subject( $data ),
                        'from_name'  => get_meta_data('web_title'),
                        'from_email' => $email_from,
                        'html'       => $email_data,
                        'to' => array(
                            array(
                                'email' => $email_to,
                                'name'  => '',
                                'type'  => 'to'
                            ),
                            array(
                                'email' => $email_to_2,
                                'name'  => '',
                                'type'  => 'to'
                            )
                        ),
                        'attachments' => array(
                            array(
                                'type'    => 'application/pdf',
                                'name'    => 'ticket-' . $data['bticket'] . '.pdf',
                                'content' => $email_pdf
                            )
                        ),
                        'headers' => array( 'Reply-To' => $email_from )
                    );
                }
                else
                {
                    $message  = array(
                        'subject'    => get_payment_notification_to_admin_subject( $data ),
                        'from_name'  => get_meta_data('web_title'),
                        'from_email' => $email_from,
                        'html'       => $email_data,
                        'to' => array(
                            array(
                                'email' => $email_to,
                                'name'  => '',
                                'type'  => 'to'
                            ),
                            array(
                                'email' => $email_to_2,
                                'name'  => '',
                                'type'  => 'to'
                            )
                        ),
                        'headers' => array( 'Reply-To' => $email_from )
                    );
                }
            }

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    save_failed_payment_notification( $bid, $data );

                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                save_failed_payment_notification( $bid, $data );

                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            save_failed_payment_notification( $bid, $data );

            return false;
        }
    }
}

function send_booking_updated_notif_to_admin( $bid, $from_agent = false )
{
    $data = ticket_booking_all_data( $bid  );

    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = get_meta_data( 'email_reservation' );
            $email_to_2 = get_meta_data( 'email_reservation_2' );
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = get_booking_updated_notif_to_admin( $data, $from_agent );

            if( empty( $email_to_2 )  )
            {
                $message  = array(
                    'subject'    => 'New Booking Updated #' . $data['bticket'],
                    'from_name'  => get_meta_data('web_title'),
                    'from_email' => $email_from,
                    'html'       => $email_data,
                    'to' => array(
                        array(
                            'email' => $email_to,
                            'name'  => '',
                            'type'  => 'to'
                        )
                    ),
                    'headers' => array( 'Reply-To' => $email_from )
                );
            }
            else
            {
                $message  = array(
                    'subject'    => 'New Booking Updated #' . $data['bticket'],
                    'from_name'  => get_meta_data('web_title'),
                    'from_email' => $email_from,
                    'html'       => $email_data,
                    'to' => array(
                        array(
                            'email' => $email_to,
                            'name'  => '',
                            'type'  => 'to'
                        ),
                        array(
                            'email' => $email_to_2,
                            'name'  => '',
                            'type'  => 'to'
                        )
                    ),
                    'headers' => array( 'Reply-To' => $email_from )
                );
            }

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

function send_booking_blocking_time_notif_to_admin( $bid, $type, $link )
{
    $data = ticket_booking_data( $bid  );

    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = get_meta_data( 'email_reservation' );
            $email_to_2 = get_meta_data( 'email_reservation_2' );
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = get_booking_blocking_time_notif_to_admin( $data, $type, $link );

            if( empty( $email_to_2 )  )
            {
                $message  = array(
                    'subject'    => 'Booking Blocking Time #' . $data['bticket'],
                    'from_name'  => get_meta_data('web_title'),
                    'from_email' => $email_from,
                    'html'       => $email_data,
                    'to' => array(
                        array(
                            'email' => $email_to,
                            'name'  => '',
                            'type'  => 'to'
                        )
                    ),
                    'headers' => array( 'Reply-To' => $email_from )
                );
            }
            else
            {
                $message  = array(
                    'subject'    => 'Booking Blocking Time #' . $data['bticket'],
                    'from_name'  => get_meta_data('web_title'),
                    'from_email' => $email_from,
                    'html'       => $email_data,
                    'to' => array(
                        array(
                            'email' => $email_to,
                            'name'  => '',
                            'type'  => 'to'
                        ),
                        array(
                            'email' => $email_to_2,
                            'name'  => '',
                            'type'  => 'to'
                        )
                    ),
                    'headers' => array( 'Reply-To' => $email_from )
                );
            }

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

function send_booking_receipt_to_client( $data )
{
    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = $data['bbemail'];
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = get_booking_notification_to_client( $data );

            $message  = array(
                'subject'    => 'New Booking Inquiry #' . $data['bticket'],
                'from_name'  => get_meta_data('web_title'),
                'from_email' => $email_from,
                'html'       => $email_data,
                'to' => array(
                    array(
                        'email' => $email_to,
                        'name'  => '',
                        'type'  => 'to'
                    )
                ),
                'headers' => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

function send_updated_booking_ticket_to_agent( $id )
{
    $data = ticket_booking_all_data( $id );

    if( validate_booking_status( $data ) || ( $data['bonhandtotal'] == $data['btotal'] ) )
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = $data['bbemail'];
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = get_payment_notification_to_client( $data );
            $email_pdf  = generate_booking_pdf_ticket( $data );
            $email_pdf  = base64_encode( $email_pdf );

            //-- Delete Barcode After Encode
            if( file_exists( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' ) )
            {
                unlink( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' );
            }

            $message  = array(
                'subject'    => 'Updated confirmation and Ticket #' . $data['bticket'],
                'from_name'  => get_meta_data('web_title'),
                'from_email' => $email_from,
                'html'       => $email_data,
                'to' => array(
                    array(
                        'email' => $email_to,
                        'name'  => '',
                        'type'  => 'to'
                    )
                ),
                'attachments' => array(
                    array(
                        'type'    => 'application/pdf',
                        'name'    => 'ticket-' . $data['bticket'] . '.pdf',
                        'content' => $email_pdf
                    )
                ),
                'headers' => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return array( 'result' => false, 'message' => 'Failed to send booking ticket' );
                }
                else
                {
                    return array( 'result' => true, 'message' => 'Booking ticket successfully sent' );
                }
            }
            else
            {
                return array( 'result' => false, 'message' => 'Failed to send booking ticket' );
            }
        }
        catch( Mandrill_Error $e )
        {
            return array( 'result' => false, 'message' => $e->getMessage() );
        }
    }
    else
    {
        return array( 'result' => false, 'message' => 'Failed to send booking ticket' );
    }
}

function send_booking_ticket_to_client( $data, $email_pdf = '' )
{
    if( validate_booking_status( $data ) )
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = $data['bbemail'];
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = get_payment_notification_to_client( $data );
            $email_pdf  = empty( $email_pdf ) ? generate_booking_pdf_ticket( $data ) : $email_pdf;
            $email_pdf  = base64_encode( $email_pdf );

            //-- Delete Barcode After Encode
            if( file_exists( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' ) )
            {
                unlink( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' );
            }

            $message  = array(
                'subject'    => get_payment_notification_to_admin_subject( $data ),
                'from_name'  => get_meta_data('web_title'),
                'from_email' => $email_from,
                'html'       => $email_data,
                'to' => array(
                    array(
                        'email' => $email_to,
                        'name'  => '',
                        'type'  => 'to'
                    )
                ),
                'attachments' => array(
                    array(
                        'type'    => 'application/pdf',
                        'name'    => 'ticket-' . $data['bticket'] . '.pdf',
                        'content' => $email_pdf
                    )
                ),
                'headers' => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    save_failed_payment_notification( $data['bid'], $data, true );

                    return array( 'result' => false, 'message' => 'Failed to send booking ticket' );
                }
                else
                {
                    return array( 'result' => true, 'message' => 'Booking ticket successfully sent' );
                }
            }
            else
            {
                save_failed_payment_notification( $data['bid'], $data, true );

                return array( 'result' => false, 'message' => 'Failed to send booking ticket' );
            }
        }
        catch( Mandrill_Error $e )
        {
            save_failed_payment_notification( $data['bid'], $data, true );

            return array( 'result' => false, 'message' => $e->getMessage() );
        }
    }
    else
    {
        save_failed_payment_notification( $data['bid'], $data, true );

        return array( 'result' => false, 'message' => 'Failed to send booking ticket' );
    }
}

function send_booking_receipt_and_ticket_to_client( $data )
{
    if( validate_booking_status( $data ) || ( $data['bonhandtotal'] == $data['btotal'] ) )
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = $data['bbemail'];
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = get_booking_notification_to_client( $data );
            $email_pdf  = generate_booking_pdf_ticket( $data );
            $email_pdf  = base64_encode( $email_pdf );

            //-- Delete Barcode After Encode
            if( file_exists( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' ) )
            {
                unlink( PLUGINS_PATH . '/ticket/uploads/qrcode/' . $data['bticket'] . '.png' );
            }

            $message  = array(
                'subject'    => 'New Booking Inquiry #' . $data['bticket'],
                'from_name'  => get_meta_data('web_title'),
                'from_email' => $email_from,
                'html'       => $email_data,
                'to' => array(
                    array(
                        'email' => $email_to,
                        'name'  => '',
                        'type'  => 'to'
                    )
                ),
                'attachments' => array(
                    array(
                        'type'    => 'application/pdf',
                        'name'    => 'ticket-' . $data['bticket'] . '.pdf',
                        'content' => $email_pdf
                    )
                ),
                'headers' => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function send_reconfirmation_to_client( $data )
{
    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = $data['bbemail'];
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = get_booking_reconfirmation_to_client( $data );

            $message  = array(
                'subject'    => 'Booking Reconfirmation #' . $data['bticket'],
                'from_name'  => get_meta_data('web_title'),
                'from_email' => $email_from,
                'html'       => $email_data,
                'to' => array(
                    array(
                        'email' => $email_to,
                        'name'  => '',
                        'type'  => 'to'
                    )
                ),
                'headers' => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

function send_payment_notification_to_client( $bid )
{
    $data = ticket_booking_all_data( $bid  );

    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = $data['bbemail'];
            $email_from = get_meta_data( 'smtp_email_address' );

            if( $data['bstatus'] == 'pf' )
            {
                $email_data = get_payment_failed_notification_to_client( $data );
            }
            elseif( $data['bstatus'] == 'ol' )
            {
                $email_data = get_no_payment_notification_to_client( $data );
            }

            $message  = array(
                'subject'    => 'Booking Failed #' . $data['bticket'],
                'from_name'  => get_meta_data('web_title'),
                'from_email' => $email_from,
                'html'       => $email_data,
                'to' => array(
                    array(
                        'email' => $email_to,
                        'name'  => '',
                        'type'  => 'to'
                    )
                ),
                'headers' => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    save_failed_payment_notification( $bid, $data, true );

                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                save_failed_payment_notification( $bid, $data, true );

                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            save_failed_payment_notification( $bid, $data, true );

            return false;
        }
    }
}

function save_failed_payment_notification( $bid, $data, $is_client = false )
{
    if( $is_client )
    {
        $title   = 'Client Payment Notification and Ticket #' . $data['bticket'];
        $link    = HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $data['bid'];
        $content = sprintf( '<p>Hello, it seems the system failed to send email to your client for reservation with code #%s. For more details please visit the following <a href="%s">link</a></p>', $data['bticket'], $link );
    }
    else
    {
        $title   = 'Admin Payment Notification and Ticket #' . $data['bticket'];
        $link    = HTSERVER . site_url() . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $bid;
        $content = sprintf( '<p>Hello, it seems the system failed to send email to admin for reservation with code #%s. For more details please visit the following <a href="%s">link</a></p>', $data['bticket'], $link );
    }

    save_ticket_notification( $bid, $title, $content, 0, 0, date( 'Y-m-d H:i:s', time() ) );
}

function ticket_passenger_list_name( $passenger, $use_br = false, $show_pax = false, $show_recap = true )
{
    if( !empty( $passenger ) )
    {
        $name = array();
        $type = array();

        foreach( $passenger as $d )
        {
            $bpname = html_entity_decode( $d['bpname'], ENT_QUOTES, 'UTF-8' );

            $type[ $d['bptype'] ][] = $bpname;

            $name[] = $bpname;
        }

        if( $show_pax )
        {
            $adult  = isset( $type['adult'] ) ? count( $type['adult'] ) : 0;
            $child  = isset( $type['child'] ) ? count( $type['child'] ) : 0;
            $infant = isset( $type['infant'] ) ? count( $type['infant'] ) : 0;
            $numpax = $adult + $child + $infant;
            $adds   = $show_recap ? '<span class="sr-only">' . $numpax . '|' . $adult . '|' . $child . '|' . $infant . '</span>' : '';

            return implode( ( $use_br ? '<br />' : ', ' ), $name ) . '<br /><br />' . 'Pax : ' . $numpax . ' | Adult : ' . $adult . ' | Child : ' . $child . ' | Infant : ' . $infant . $adds;
        }
        else
        {
            return implode( ( $use_br ? '<br />' : ', ' ), $name );
        }
    }
}

function ticket_booking_transport_detail( $bdid, $use_comma = false )
{
    global $db;

    $html = '';

    $s = 'SELECT
            b.bttype,
            b.bttrans_type,
            b.bthotelname,
            b.bthotelphone,
            b.btdrivername,
            b.btdriverphone,
            c.taname,
            a.bdid,
            a.bdtype
          FROM ticket_booking_detail AS a
          LEFT JOIN ticket_booking_transport AS b ON b.bdid = a.bdid
          LEFT JOIN ticket_transport_area AS c ON b.taid = c.taid
          WHERE a.bdid = %d';
    $q = $db->prepare_query( $s, $bdid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        if( $use_comma )
        {
            $html = array();

            while( $d = $db->fetch_array( $r ) )
            {
                $name  = 'Hotel Name : ' . ( empty( $d['bthotelname'] ) ? '-' : $d['bthotelname'] );
                $phone = 'Phone : ' . ( empty( $d['bthotelphone'] ) ? '-' : $d['bthotelphone'] );

                if( $d['bttrans_type'] == '0' )
                {
                    $bttrans_type = 'Shared';
                }
                elseif( $d['bttrans_type'] == '1' )
                {
                    $bttrans_type = 'Private';
                }
                else
                {
                    $bttrans_type = 'Own Transport';

                    $name  = 'Driver : ' . ( empty( $d['btdrivername'] ) ? '-' : $d['btdrivername'] );
                    $phone = 'Phone : ' . ( empty( $d['btdriverphone'] ) ? '-' : $d['btdriverphone'] );
                }

                $html[] = ucfirst( $d['bttype'] ) . ', Type : ' . $bttrans_type . ', ' . $name . ', '. $phone . ', Area : ' . ( empty( $d['taname'] ) ? '-' : $d['taname'] );
            }

            $html = implode( $html, '<br/>' );
        }
        else
        {
            $html = '
            <ul>';

                while( $d = $db->fetch_array( $r ) )
                {
                    $name  = 'Hotel Name : ' . ( empty( $d['bthotelname'] ) ? '-' : $d['bthotelname'] );
                    $phone = 'Phone : ' . ( empty( $d['bthotelphone'] ) ? '-' : $d['bthotelphone'] );

                    if( $d['bttrans_type'] == '0' )
                    {
                        $bttrans_type = 'Shared';
                    }
                    elseif( $d['bttrans_type'] == '1' )
                    {
                        $bttrans_type = 'Private';
                    }
                    else
                    {
                        $bttrans_type = 'Own Transport';

                        $name  = 'Driver : ' . ( empty( $d['btdrivername'] ) ? '-' : $d['btdrivername'] );
                        $phone = 'Phone : ' . ( empty( $d['btdriverphone'] ) ? '-' : $d['btdriverphone'] );
                    }

                    $html .= '
                    <li>
                        <p>' . ucfirst( $d['bttype'] ) . '</p>
                        <ul>
                            <li>Type : ' . $bttrans_type . '</li>
                            <li>'. $name .'</li>
                            <li>'. $phone .'</li>
                            <li>Area : ' . ( empty( $d['taname'] ) ? '-' : $d['taname'] ) . '</li>
                        </ul>
                    <li>';
                }

                $html .= '
            </ul>';
        }
    }

    return $html;
}

function get_revise_trip_detail_option( $detail, $type, $bdid = '' )
{
    $option = '<option data-bdtype="' . $type . '" data-adult="0" data-child="0" data-infant="0" data-from="" data-to="" data-date="" value="">Select Trip</option>';
    $bdid   = isset( $bdid[ $type ] ) ? $bdid[ $type ] : $bdid;

    if( isset( $detail[ $type ] ) && !empty( $detail[ $type ] ) )
    {
        foreach( $detail[ $type ] as $d )
        {
            $option .= '
            <option data-bdtype="' . $type . '" data-adult="' . $d['num_adult'] . '" data-child="' . $d['num_child'] . '" data-infant="' . $d['num_infant'] . '" data-from="' . $d['bdfrom_id'] . '" data-to="' . $d['bdto_id'] . '" data-date="' . $d['bddate'] . '" value="' . $d['bdid'] . '" ' . ( $d['bdid'] == $bdid ? 'selected' : '' ) . '>
                ' . $d['bdfrom'] . ' - ' . $d['bdto'] . ' ( ' . date( 'd F Y', strtotime( $d['bddate'] ) ) . ' )
            </option>';

        }
    }

    return $option;
}

function get_trip_detail_option( $detail, $bdidval = '' )
{
    $option = '<option data-bdtype="" data-adult="0" data-child="0" data-infant="0" data-from="" data-to="" data-date="" value=""></option>';

    if( !empty( $detail ) )
    {
        foreach( $detail as $bdtype => $obj )
        {
            $option .= '
            <optgroup label="' . ucfirst( $bdtype ) . '">';

                foreach( $obj as $bdid => $d )
                {
                    $option .= '
                    <option data-bdtype="' . $bdtype . '" data-adult="' . $d['num_adult'] . '" data-child="' . $d['num_child'] . '" data-infant="' . $d['num_infant'] . '" data-from="' . $d['bdfrom_id'] . '" data-to="' . $d['bdto_id'] . '" data-date="' . $d['bddate'] . '" value="' . $bdid . '|' . $bdtype . '" ' . ( $bdid . '|' . $bdtype == $bdidval ? 'selected' : '' ) . '>
                        ' . $d['bdfrom'] . ' - ' . $d['bdto'] . ' ( ' . $d['rname'] . ' )
                    </option>';
                }

                $option .= '
            </optgroup>';

        }
    }

    return $option;
}

function ticket_booking_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        extract( $_POST );

        $data = ticket_booking_table_query( $chid, $lcid, $lcid2, $rid, $status, $date_start, $date_end, $sbooking, $bdate, $bbemail, $rstatus);

        echo json_encode($data);
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'show-detail' )
    {
        echo ticket_booking_show_detail( $_GET['id'] );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'booking-cancelation' )
    {
        $data = ticket_booking_all_data( $_POST['bid'] );
        $res  = 0;

        if ( $_POST['ctype'] == 'ticket' )
        {
            $res = booking_cancel_ticket( $_POST );
        }
        else if( $_POST['ctype'] == 'trip' )
        {
            $res = booking_cancel_trip( $_POST );
        }
        else if( $_POST['ctype'] == 'passanger' )
        {
            $res = booking_cancel_passager( $_POST );
        }

        if ( $res != 0 )
        {
            send_booking_cancelation_request_to_admin( $res, 'booking-cancel', $_POST['ctype'] );
            send_booking_cancelation_notif_to_client( $res, 'booking-cancel', $_POST['ctype'] );

            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'execute-reservation-action' )
    {
        if( isset( $_POST['action'] ) )
        {
            $data = ticket_booking_all_data( $_POST['bid'] );

            if( $_POST['action'] == 1 )
            {
                $res = send_booking_ticket_to_client( $data );

                if( $res['result'] === false )
                {
                    echo json_encode( array( 'result'=> 'error', 'message' => 'Sorry, unable to send this ticket' ) );
                }
                else
                {
                    echo json_encode( array( 'result'=> 'success', 'message' => 'Ticket is successfully sent to client' ) );
                }
            }
            elseif( $_POST['action'] == 2 )
            {
                if( send_booking_receipt_to_client( $data ) )
                {
                    echo json_encode( array( 'result'=> 'success', 'message' => 'Receipt is successfully sent to client' ) );
                }
                else
                {
                    echo json_encode( array( 'result'=> 'error', 'message' => 'Sorry, unable to send this receipt' ) );
                }
            }
            elseif( $_POST['action'] == 7 )
            {
                if( send_reconfirmation_to_client( $data ) )
                {
                    echo json_encode( array( 'result'=> 'success', 'message' => 'Reconfirmation is successfully sent to client' ) );
                }
                else
                {
                    echo json_encode( array( 'result'=> 'error', 'message' => 'Sorry, unable to send this reconfirmation' ) );
                }
            }
            elseif( $_POST['action'] == 12 )
            {
                if( $data['agid'] != '' )
                {
                    if( send_invoice_to_client( $data ) )
                    {
                        echo json_encode( array( 'result'=> 'success', 'message' => 'Invoice is successfully sent to client' ) );
                    }
                    else
                    {
                        echo json_encode( array( 'result'=> 'error', 'message' => 'Sorry, unable to send this invoice' ) );
                    }
                }
                else
                {
                    echo json_encode( array( 'result'=> 'error', 'message' => 'Sorry, unable to send this invoice' ) );
                }
            }
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'update-booking-payment' )
    {
        if( update_booking_payment_2() )
        {
            echo json_encode( array( 'result'=> 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result'=> 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'save-booking-payment' )
    {
        if( save_booking_payment_2() )
        {
            echo json_encode( array( 'result'=> 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result'=> 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'delete-payment' )
    {
        if( delete_payment() )
        {
            echo json_encode( array( 'result'=> 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result'=> 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'save-outstanding-payment' )
    {
        if( save_outstanding_payment() )
        {
            echo json_encode( array( 'result'=> 'success' ) );
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'update-outstanding-payment' )
    {
        if( update_outstanding_payment() )
        {
            echo json_encode( array( 'result'=> 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result'=> 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'delete-outstanding-payment' )
    {
        if( delete_outstanding_payment() )
        {
            echo json_encode( array( 'result'=> 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result'=> 'failed' ) );
        }
    }

    exit;
}

/*
| -------------------------------------------------------------------------------------
| Booking Cancel | Booking Cancelation Get Data
| -------------------------------------------------------------------------------------
*/
function get_date_cancel_booking_ticket( $bid, $bdid=array() )
{
    global $db;

    $res  = array();
    $where = '';

    if ( $bid != '' )
    {
        $where .= $db->prepare_query( ' AND a.bid=%d ', $bid );
    }

    if ( !empty( $bdid ) )
    {
        $where .= $db->prepare_query( 'AND d.bdid IN( %s )', implode( ',', $bdid ) );
    }

    $s = 'SELECT
            d.bddate,
            d.bddeparttime
          FROM ticket_booking AS a JOIN ticket_booking_detail AS d ON a.bid = d.bid
          WHERE a.bstt NOT LIKE "ar"' . $where . ' ORDER BY d.bddate ASC LIMIT 1';
    $q = $db->prepare_query( $s, $bid );
    $r = $db->do_query( $q );

    while ( $d = $db->fetch_array( $r ) )
    {
        $res = $d;
    }

    return $res;
}

function ticket_passenger_list_cancel_content( $trip, $data, $dt )
{
    $content = '';
    $n=0;

    foreach( $data as $obj )
    {
        foreach( $obj as $i => $d )
        {
            $n++;
            $gender = ( $d['bpgender'] == 1 ) ? 'Male' : 'Female';
            if ( $d['bptype'] == 'adult' )
            {
                $pr       = $dt['price_per_adult'];
                $discount = $dt['disc_price_per_adult'];
                $price    = $pr - $discount;
            }
            else if ( $d['bptype'] == 'child' )
            {
                $pr       = $dt['price_per_child'];
                $discount = $dt['disc_price_per_child'];
                $price    = $pr - $discount;
            }
            else if ( $d['bptype'] == 'infant' )
            {
                $pr       = $dt['price_per_infant'];
                $discount = $dt['disc_price_per_infant'];
                $price    = $pr - $discount;
            }

            $content .= '
            <tr>
                <td width="400">
                    <span class="small-title">' . ucfirst( $d['bptype'] ) . ' ' . ( $i + 1 ) . '</span>
                    <input type="text" class="form-control" id="'.$d['bpid'].'" value="' . $d['bpname'] . '" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][name]">
                </td>
                <td width="100">
                    <span class="small-title">Gender</span>
                    <input type="text" class="form-control" id="'.$d['bpid'].'" value="' . $gender . '" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][gender]">
                </td>
                <td width="100">
                    <span class="small-title">Status</span>
                    <select id="status_'.$d['bpid'].'_'.$trip.'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][status]" class="form-control" autocomplete="off">';
                        if ( $d['bpstatus'] != 'cn' )
                        {
                            $content .=
                            '<option value="aa">Active</option>
                            <option value="cn">Cancel</option>';
                        }
                        else
                        {
                            $content .=
                            '<option value="aa" disabled>Active</option>
                            <option value="cn" selected>Cancel</option>';
                        }
                    $content .=
                    '</select>
                </td>
                <td width="150">
                    <span class="small-title">Price per passanger</span>
                    <input type="text" class="text text-number form-control text-right" id="pricepax_'.$d['bpid'].'_'.$trip.'" value="'. $price .'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][price]" readonly>
                </td>
                <td width="150">
                    <span class="small-title">Cancellation percent fee</span>
                    <input type="text" class="form-control" id="percentfee_'.$d['bpid'].'_'.$trip.'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][cancelpercent]" readonly>
                </td>
                <td width="150">
                    <span class="small-title">Cancellation fee</span>
                    <input type="text" class="text text-number form-control text-right" id="cancelationfee_'.$d['bpid'].'_'.$trip.'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][cancelationfee]" readonly>
                </td>
                <td width="150">
                    <span class="small-title">Transaction fee</span>
                    <input type="text" class="text text-number form-control text-right" id="transactionfee_'.$d['bpid'].'_'.$trip.'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][transactionfee]" readonly>
                </td>
                <td width="150">
                    <span class="small-title">Refund</span>
                    <input type="text" class="text text-number form-control text-right" id="refund'.$d['bpid'].'_'.$trip.'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][refund]" readonly>
                </td>

                <td style="display:none;">
                    <input type="text" class="text text-number form-control text-right" id="dispax_'.$d['bpid'].'_'.$trip.'" value="'.$discount.'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][price_disc]" readonly>
                </td>
                <td style="display:none;">
                    <input type="text" class="text text-number form-control text-right" id="prpax'.$d['bpid'].'_'.$trip.'" value="'.$pr.'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][pr]" readonly>
                </td>
                <td style="display:none;">
                    <input type="text" class="text text-number form-control text-right" id="bptype'.$d['bpid'].'_'.$trip.'" value="'.$d['bptype'].'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][bptype]" readonly>
                </td>
                <td style="display:none;">
                    <input type="text" class="text text-number form-control text-right" id="bpgender'.$d['bpid'].'_'.$trip.'" value="'.$d['bpgender'].'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][bpgender]" readonly>
                </td>
                <td style="display:none;">
                    <input type="text" class="text text-number form-control text-right" id="bpbirthdate'.$d['bpid'].'_'.$trip.'" value="'.$d['bpbirthdate'].'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][bpbirthdate]" readonly>
                </td>
                <td style="display:none;">
                    <input type="text" class="text text-number form-control text-right" id="lcountry_id'.$d['bpid'].'_'.$trip.'" value="'.$d['lcountry_id'].'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][lcountry_id]" readonly>
                </td>
                <td style="display:none;">
                    <input type="text" class="text text-number form-control text-right" id="bpid'.$d['bpid'].'_'.$trip.'" value="'.$d['bpid'].'" name="passenger['.$trip.']['.$dt['bdid'].']['.$d['bpid'].'][bpid]" readonly>
                </td>
            </tr>';

            $content .= '
            <script>
                jQuery("#percentfee_'.$d['bpid'].'_'.$trip.'").unbind();
                jQuery("#percentfee_'.$d['bpid'].'_'.$trip.'").on("keyup", function(){
                    var $this            = jQuery(this);
                                           $this.val( $this.val().replace( /[^\d.]/g, "" ) );
                    var price = jQuery("#pricepax_'.$d['bpid'].'_'.$trip.'").val() != "" ? jQuery("#pricepax_'.$d['bpid'].'_'.$trip.'").val() : 0;
                    var transactionfee = jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").val() != "" ? jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").val() : 0;
                    var result = price * (jQuery(this).val() / 100);

                    jQuery("#cancelationfee_'.$d['bpid'].'_'.$trip.'").val(result.toFixed(0));
                    jQuery("#refund'.$d['bpid'].'_'.$trip.'").val( parseInt(price) - (parseInt(result) + parseInt(transactionfee)) );
                });

                jQuery("#cancelationfee_'.$d['bpid'].'_'.$trip.'").unbind();
                jQuery("#cancelationfee_'.$d['bpid'].'_'.$trip.'").on("keyup", function(){
                    var $this            = jQuery(this);
                                           $this.val( $this.val().replace( /[^\d.]/g, "" ) );
                    var price = jQuery("#pricepax_'.$d['bpid'].'_'.$trip.'").val() != "" ? jQuery("#pricepax_'.$d['bpid'].'_'.$trip.'").val() : 0;
                    var transactionfee = jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").val() != "" ? jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").val() : 0;
                    var cancelationfee = jQuery(this).val() != "" ? jQuery(this).val() : 0;
                    var result = (jQuery(this).val() / price) * 100;

                    jQuery("#percentfee_'.$d['bpid'].'_'.$trip.'").val( result.toFixed(2) );
                    jQuery("#refund'.$d['bpid'].'_'.$trip.'").val( parseInt(price) - (parseInt(cancelationfee) + parseInt(transactionfee)) );
                });

                jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").unbind();
                jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").on("keyup", function(){
                    var $this            = jQuery(this);
                                           $this.val( $this.val().replace( /[^\d.]/g, "" ) );
                    var price = jQuery("#pricepax_'.$d['bpid'].'_'.$trip.'").val() != "" ? jQuery("#pricepax_'.$d['bpid'].'_'.$trip.'").val() : 0;
                    var transactionfee = jQuery(this).val() != "" ? jQuery(this).val() : 0;
                    var cancelationfee = jQuery("#cancelationfee_'.$d['bpid'].'_'.$trip.'").val() != "" ? jQuery("#cancelationfee_'.$d['bpid'].'_'.$trip.'").val() : 0;

                    jQuery("#refund'.$d['bpid'].'_'.$trip.'").val(parseInt(price) - (parseInt(cancelationfee) + parseInt(transactionfee)));
                });

                jQuery("#status_'.$d['bpid'].'_'.$trip.'").unbind();
                jQuery("#status_'.$d['bpid'].'_'.$trip.'").on("change", function(){
                    if (jQuery("#status_'.$d['bpid'].'_'.$trip.'").val() == "cn") {
                        jQuery("#percentfee_'.$d['bpid'].'_'.$trip.'").attr("readonly", false);
                        jQuery("#cancelationfee_'.$d['bpid'].'_'.$trip.'").attr("readonly", false);
                        jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").attr("readonly", false);
                        jQuery("#refund'.$d['bpid'].'_'.$trip.'").attr("readonly", false);

                        jQuery("#percentfee_'.$d['bpid'].'_'.$trip.'").val("0");
                        jQuery("#cancelationfee_'.$d['bpid'].'_'.$trip.'").val("0");
                        jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").val("0");
                        jQuery("#refund'.$d['bpid'].'_'.$trip.'").val(jQuery("#pricepax_'.$d['bpid'].'_'.$trip.'").val());
                    } else {
                        jQuery("#percentfee_'.$d['bpid'].'_'.$trip.'").attr("readonly", true);
                        jQuery("#cancelationfee_'.$d['bpid'].'_'.$trip.'").attr("readonly", true);
                        jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").attr("readonly", true);
                        jQuery("#refund'.$d['bpid'].'_'.$trip.'").attr("readonly", true);

                        jQuery("#percentfee_'.$d['bpid'].'_'.$trip.'").val("");
                        jQuery("#cancelationfee_'.$d['bpid'].'_'.$trip.'").val("");
                        jQuery("#transactionfee_'.$d['bpid'].'_'.$trip.'").val("");
                        jQuery("#refund'.$d['bpid'].'_'.$trip.'").val("");
                    }
                });
            </script>';
        }
    }

    return $content;
}

function ticket_cancel_reservation_form( $bid )
{
    $data       = ticket_booking_all_data( $bid );
    $site_url   = site_url();

    if( !empty( $data['detail'] ) )
    {
        extract( $data['detail'] );

        set_template( PLUGINS_PATH . '/ticket/tpl/booking/cancel-rev.html', 'cancel-rv' );
        add_block( 'dept-data-loop-block', 'dept-dt-block', 'cancel-rv' );
        add_block( 'ret-data-loop-block', 'ret-dt-block', 'cancel-rv' );
        add_block( 'cancel-block', 'cn-block', 'cancel-rv' );

        /* Booking Detail */
        add_variable( 'bid', $bid );
        add_variable( 'bemail', $data['bbemail'] );
        add_variable( 'bremark', $data['bremark'] );
        add_variable( 'no_ticket', $data['bticket'] );
        add_variable( 'bcmessage', $data['bcmessage'] );
        add_variable( 'bstatus', ticket_booking_status( $data['bstatus'] ) );
        add_variable( 'bdate', date( 'd M Y', strtotime( $data['bdate'] ) ) );
        add_variable( 'agtr', check_agen_transaction_booking( $data['bid'] ) );

        if( empty( $data['agid'] ) )
        {
            add_variable( 'bsource', $data['chname'] );
        }
        else
        {
            $agid = empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'];

            add_variable( 'bsource', get_agent( $agid, 'agname' ) );
        }

        /* cancelation */
        add_variable( 'btotal', $data['btotal'] );
        add_variable( 'bcdate', date( 'd M Y h:i:s' ) );
        add_variable( 'bonhandtotal', $data['bonhandtotal'] );
        add_variable( 'brcdate', $data['brcdate'] == '0000-00-00 00:00:00' ? date( 'd M Y h:i:s' ) : date( 'd M Y h:i:s', strtotime( $data['brcdate'] ) ) );

        /* Data Cancelation Trip */
        $option_trip = '<option value="">Select Trip</option>';

        if( isset( $departure ) )
        {
            foreach( $departure as $kdbid => $depart )
            {
                extract( $depart );

                $dep_pw_discount  = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';
                $dep_pa_display   = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                $dep_pc_display   = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                $dep_pi_display   = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );

                $dis_status_dep   = $bdstatus == 'cn' ? 'disabled' : '';
                $val_status_dep   = $bdstatus == 'cn' ? '>> Cancelled' : '';
                $option_trip     .= '<option value="'. $bdtype .'" data-id="'. $bdid .'" data-price="'. $total . '" '. $dis_status_dep .'>Departure ( ' . $bdfrom . ' to ' . $bdto . ' ) ' . $val_status_dep . '</option>';

                add_variable( 'dep_bdto', $bdto );
                add_variable( 'dep_bdfrom', $bdfrom );
                add_variable( 'dep_num_adult', $num_adult );
                add_variable( 'dep_num_child', $num_child );
                add_variable( 'dep_num_infant', $num_infant );
                add_variable( 'dep_bddeparttime', $bddeparttime );
                add_variable( 'dep_bdarrivetime', $bdarrivetime );

                add_variable( 'dep_discount', $discount );
                add_variable( 'dep_price_per_adult', $price_per_adult );
                add_variable( 'dep_price_per_child', $price_per_child );
                add_variable( 'dep_price_per_infant', $price_per_infant );

                add_variable( 'dep_pa_display', $dep_pa_display );
                add_variable( 'dep_pc_display', $dep_pc_display );
                add_variable( 'dep_pi_display', $dep_pi_display );
                add_variable( 'dep_pt_display', number_format( $total, 0, ',', '.' ) );

                add_variable( 'dep_bdtype', ucfirst( $bdtype ) );
                add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'dep_status', ticket_booking_status( $bdpstatus ) );

                add_variable( 'dep_css', '' );

                parse_template( 'dept-data-loop-block', 'dept-dt-block', true );
            }
        }
        else
        {
            add_variable( 'dep_css', 'sr-only' );
        }

        if( isset( $return ) )
        {
            foreach( $return as $kdbid => $ret )
            {
                extract( $ret );

                $rtn_pw_discount = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';
                $rtn_pa_display  = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                $rtn_pc_display  = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                $rtn_pi_display  = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );

                $status_ret_trip  = $bdstatus == 'cn' ? 'cn' : 'aa';
                $dis_status_ret   = $bdstatus == 'cn' ? 'disabled' : '';
                $val_status_ret   = $bdstatus == 'cn' ? '>> Cancelled' : '';
                $option_trip     .= '<option value="'. $bdtype .'" data-id="'. $bdid .'" data-price="'. $total . '" '. $dis_status_ret .'>Return ( ' . $bdfrom . ' to ' . $bdto . ' ) ' . $val_status_ret . '</option>';

                add_variable( 'rtn_bdto', $bdto );
                add_variable( 'rtn_bdfrom', $bdfrom );
                add_variable( 'rtn_bddeparttime', $bddeparttime );
                add_variable( 'rtn_bdarrivetime', $bdarrivetime );

                add_variable( 'rtn_discount', $discount );
                add_variable( 'rtn_price_per_adult', $price_per_adult );
                add_variable( 'rtn_price_per_child', $price_per_child );
                add_variable( 'rtn_price_per_infant', $price_per_infant );

                add_variable( 'rtn_pa_display', $rtn_pa_display );
                add_variable( 'rtn_pc_display', $rtn_pc_display );
                add_variable( 'rtn_pi_display', $rtn_pi_display );
                add_variable( 'rtn_pt_display', number_format( $total, 0, ',', '.' ) );

                add_variable( 'rtn_bdtype', ucfirst( $bdtype ) );
                add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'rtn_status', ticket_booking_status( $bdpstatus ) );

                add_variable( 'rtn_css', '' );

                parse_template( 'ret-data-loop-block', 'ret-dt-block', true );
            }
        }
        else
        {
            add_variable( 'rtn_css', 'sr-only' );
        }

        add_variable( 'btype', ticket_booking_type( $data['btype'] ) );
        add_variable( 'bdtype', ( $data['btype'] == '0' ) ? 'One Way' : 'Return' );
        add_variable( 'trip', $option_trip );
        /* End Data Cancelation Trip */

        /* Data Cancelation Passanger */
        if ( isset( $departure ) )
        {
            $dep_name = '';

            foreach ( $departure as $k => $depart )
            {
                extract( $depart );

                $dep_name .=
                '<tr>
                    <td colspan="12">
                        <h4>Passenger Departure</h4>
                        <div class="col-md-2">
                            <strong class="strong-text">'. $bdfrom .'</strong>
                            <p class="regular-text">'. date( 'd M Y', strtotime( $bddate ) ) .', ' . date( 'H:i:s', strtotime( $bddeparttime ) ) . '</p>
                        </div>
                        <div class="col-md-2">
                            <strong class="strong-text">'. $bdto .'</strong>
                            <p class="regular-text">'. date( 'd M Y', strtotime( $bddate ) ) .', ' . date( 'H:i:s', strtotime( $bdarrivetime ) ) . '</p>
                        </div>
                        <div class="col-md-1">
                            <strong class="strong-text">Adult</strong>
                            <p class="regular-text">'. $num_adult . ' x '. number_format( ($price_per_adult - $disc_price_per_adult), 0, ',', '.' ) .'</p>
                        </div>
                        <div class="col-md-1">
                            <strong class="strong-text">Child</strong>
                            <p class="regular-text">'. $num_child . ' x '. number_format( ($price_per_child - $disc_price_per_child), 0, ',', '.' ) .'</p>
                        </div>
                        <div class="col-md-1">
                            <strong class="strong-text">Infant</strong>
                            <p class="regular-text">'. $num_infant . ' x '. number_format( ($price_per_infant - $disc_price_per_infant), 0, ',', '.' ) .'</p>
                        </div>
                        <div class="col-md-3 text-right">
                            <strong class="strong-text">Subtotal(IDR)</strong>
                            <p class="regular-text">'. number_format( $total, 0, ',', '.' ) . '</p>
                        </div>
                        <div class="col-md-2 text-right">
                            <strong class="strong-text">Status</strong>
                            <p class="strong-text">'. ticket_booking_status( $bdpstatus ) . '</p>
                        </div>
                    </td>
                </tr>';

                $dep_name .= ticket_passenger_list_cancel_content( 'departure', $passenger, $data['detail']['departure'][$k] );

                add_variable( 'dep_name', $dep_name );
            }
        }

        if ( isset( $return ) )
        {
            $ret_name = '';

            foreach ( $return as $k => $ret )
            {
                extract( $ret );

                $ret_name .=
                '<tr>
                    <td colspan="12">
                        <h4>Passenger Return</h4>
                        <div class="col-md-2">
                            <strong class="strong-text">'. $bdfrom .'</strong>
                            <p class="regular-text">'. date( 'd M Y', strtotime( $bddate ) ) .', ' . date( 'H:i:s', strtotime( $bddeparttime ) ) . '</p>
                        </div>
                        <div class="col-md-2">
                            <strong class="strong-text">'. $bdto .'</strong>
                            <p class="regular-text">'. date( 'd M Y', strtotime( $bddate ) ) .', ' . date( 'H:i:s', strtotime( $bdarrivetime ) ) . '</p>
                        </div>
                        <div class="col-md-1">
                        <strong class="strong-text">Adult</strong>
                        <p class="regular-text">'. $num_adult . ' x '. number_format( ($price_per_adult - $disc_price_per_adult), 0, ',', '.' ) .'</p>
                        </div>
                        <div class="col-md-1">
                            <strong class="strong-text">Child</strong>
                            <p class="regular-text">'. $num_child . ' x '. number_format( ($price_per_child - $disc_price_per_child), 0, ',', '.' ) .'</p>
                        </div>
                        <div class="col-md-1">
                            <strong class="strong-text">Infant</strong>
                            <p class="regular-text">'. $num_infant . ' x '. number_format( ($price_per_infant - $disc_price_per_infant), 0, ',', '.' ) .'</p>
                        </div>
                        <div class="col-md-3 text-right">
                            <strong class="strong-text">Subtotal(IDR)</strong>
                            <p class="regular-text">'. number_format( $total, 0, ',', '.' ) . '</p>
                        </div>
                        <div class="col-md-2 text-right">
                            <strong class="strong-text">Status</strong>
                            <p class="strong-text">'. ticket_booking_status( $bdpstatus ) . '</p>
                        </div>
                    </td>
                </tr>';
                $ret_name .= ticket_passenger_list_cancel_content( 'return', $passenger, $data['detail']['return'][$k] );

                add_variable( 'ret_name', $ret_name );
            }
        }
        /* End Data Cancelation Passanger */

        add_variable( 'state_url', get_state_url( 'reservation&sub=booking' ) );
        add_variable( 'filter', $_GET['filter'] );
        add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-booking-ajax' );

        add_actions( 'section_title', 'Cancel Reservation' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        parse_template( 'cancel-block', 'cn-block' );

        return return_template( 'cancel-rv' );
    }
}

function check_agen_transaction_booking( $bid )
{
    global $db;

    $s = 'SELECT
            COUNT(atstatus) AS stt
          FROM ticket_booking AS b
          LEFT JOIN ticket_agent_transaction AS t ON b.bcode = t.bcode
          WHERE b.bid=%d AND atstatus = 2 AND b.bstt <> %s';
    $q = $db->prepare_query( $s, $bid, 'ar' );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if ( $d['stt'] == 0 )
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

function get_ticket_port_clereance_detail( $bpid )
{
    global $db;

    $data = array();

    if ( $bpid != '' )
    {
        $s = 'SELECT * FROM ticket_port_clearance_detail WHERE bpid=%d ORDER BY pcdid DESC LIMIT 1';
        $q = $db->prepare_query( $s, $bpid );
        $r = $db->do_query( $q );

        if ( $db->num_rows( $r ) )
        {
            while ( $d = $db->fetch_array( $r ) )
            {
                $data = $d;
            }
        }
    }

    return $data;
}

function get_ticket_trip_transport_detail( $btid )
{
    global $db;

    $data = array();

    if ( $btid != '' )
    {
        $s = 'SELECT * FROM ticket_trip_transport_detail WHERE btid=%d ORDER BY btid DESC LIMIT 1';
        $q = $db->prepare_query( $s, $btid );
        $r = $db->do_query( $q );

        if ( $db->num_rows( $r ) )
        {
            while ( $d = $db->fetch_array( $r ) )
            {
                $data = $d;
            }
        }
    }

    return $data;
}

function get_btotal_early( $bticket )
{
    global $db;

    $s = 'SELECT btotal FROM ticket_booking WHERE bticket=%s ORDER BY bid ASC LIMIT 1';
    $q = $db->prepare_query( $s, $bticket );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d['btotal'];
}

function get_bcode_agent_transaction( $bid )
{
    global $db;

    $s = 'SELECT bcode FROM ticket_booking WHERE bid=%d';
    $q = $db->prepare_query( $s, $bid );
    $r = $db->do_query( $q );

    if( $d = $db->fetch_array( $r ) )
    {
        return $d['bcode'];
    }
}

function get_passanger_new( $bdid )
{
    global $db;

    $s = 'SELECT bpname FROM ticket_booking_passenger WHERE bdid=%d';
    $q = $db->prepare_query( $s, $bdid );
    $r = $db->do_query( $q );
    $dt = array();

    if ( $db->num_rows($r) > 0 )
    {
        while ( $d = $db->fetch_array( $r ) )
        {
            $dt = $d;
        }
    }

    return implode( ',', $dt );
}

function delete_payment()
{
    global $db;

    if( $_POST[ 'status' ] == 1 )
    {
        $b = ticket_booking_all_data( $_POST[ 'bid' ] );

        $s = 'DELETE FROM ticket_booking_payment WHERE pid = %d';
        $q = $db->prepare_query( $s, $_POST[ 'pid' ] );
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            $p = ticket_booking_payment_detail( $b['bid'] );

            if( empty( $p ) )
            {
                ticket_booking_update_on_hand_payment( $b['bid'], 0 );
            }
            else
            {
                ticket_booking_update_on_hand_payment( $b['bid'], array_sum( array_column( $p, 'ptotal' ) ) );
            }

            save_log( $b['bid'], 'reservation', 'Delete Payment #' . $b['bticket'] );

            if( is_array( $r ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    else
    {
        $s = 'DELETE FROM ticket_booking_payment WHERE pid = %d';
        $q = $db->prepare_query( $s, $_POST[ 'pid' ] );
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Booking Cancel | Booking Cancelation Action
| -------------------------------------------------------------------------------------
*/
function booking_cancel_ticket( $post )
{
    global $db;

    $result = 0;

    extract( $post );

    $bonhandtotal   = isset( $tconhand ) ? $tconhand : 0;
    $btransfee      = isset( $tctransactionfee ) ? $tctransactionfee : 0;
    $bcancelfee     = isset( $tccancelfee ) ? $tccancelfee : 0;
    $bcancelpercent = isset( $tccancelpercent ) ? $tccancelpercent : 0;
    $brefund        = isset( $tcbrefund ) ? $tcbrefund : 0;
    $tcreason       = isset( $tcreason ) ? $tcreason : '';
    $tcoreason      = isset( $tcoreason ) ? $tcoreason : '';
    $tcmessage      = isset( $tcmessage ) ? $tcmessage : '';
    $tccdate        = isset( $tccdate ) ? $tccdate : '0000-00-00';
    $tcrcdate       = isset( $tcrcdate ) ? $tcrcdate : '0000-00-00';

    if( $tcreason == 'Other'  )
    {
        $tcreason = $tcoreason;
    }

    $retval = 1;

    $db->begin();

    $s = 'UPDATE ticket_booking SET bcreason=%s, bcmessage=%s, bstatus=%s, bcdate=%s, brcdate=%s, bcancellationfee=%s, btransactionfee=%s, brefund=%s, bcancelationpercent=%s, bstact="2" WHERE bid=%d';
    $q = $db->prepare_query( $s, $tcreason, $tcmessage, 'cn', date( 'Y-m-d h:i:s', strtotime( $tccdate ) ), date( 'Y-m-d h:i:s', strtotime( $tcrcdate ) ), $bcancelfee, $btransfee, $brefund, $bcancelpercent, $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        $retval = 0;
    }
    else
    {
        $p = 'UPDATE ticket_booking_detail SET bdstatus=%s, bdpstatus=%s WHERE bid=%d';
        $n = $db->prepare_query( $p, 'cn', 'cn', $bid );
        $i = $db->do_query( $n );

        if ( is_array( $i ) )
        {
            $retval = 0;
        }
        else
        {
            $f = 'SELECT bdid FROM ticket_booking_detail WHERE bid=%d';
            $g = $db->prepare_query( $f, $bid );
            $t = $db->do_query( $g );
            $c = 0;

            while ( $d = $db->fetch_array( $t ) )
            {
                $h = $db->do_query( 'UPDATE ticket_booking_passenger SET bpstatus="cn" WHERE bdid='.$d['bdid'] );

                if ( is_array( $h ) )
                {
                    $c++;
                }
            }

            if ( $c != 0 || $c > 0 )
            {
                $retval = 0;
            }
        }
    }

    if( $retval == 0 )
    {
        $db->rollback();

        return 0;
    }
    else
    {
        $data = ticket_booking_all_data( $bid );

        save_log( $bid, 'reservation', 'Cancelation Ticket #' . $data['bticket'] );

        $db->commit();

        return $bid;
    }
}

function booking_cancel_trip( $post )
{
    global $db;

    extract( $post );

    if ( $trbtype == 'One Way' )
    {
        $dts = array(
            'bid'               => $bid ,
            'tconhand'          => $tronhand,
            'tctransactionfee'  => $trtransactionfee,
            'tccancelfee'       => $trcancelfee,
            'tccancelpercent'   => $trcancelpercent,
            'tcbrefund'         => $trrefund,
            'tcreason'          => $trreason,
            'tcoreason'         => $troreason,
            'tcmessage'         => $trmessage,
            'tccdate'           => $trdate,
            'tcrcdate'          => $trrcdate
        );

        return booking_cancel_ticket( $dts );
    }
    elseif( $trbtype == 'Return' )
    {
        $d = ticket_booking_all_data( $bid );

        if ( count( $d['detail'] ) == 1 )
        {
            return booking_cancel_ticket( $post );
        }
        else
        {
            $idbooking = 0;
            $retval    = 1;
            $trip_is_cancel = '';

            $db->begin();

            if ( !empty( $d ) && !empty( $post ) )
            {
                if( $trreason == 'Other' )
                {
                    $trreason = $troreason;
                }

                // New In Booking //
                $btotal_new = $d['bsubtotal'];
                $brcode     = $d['brcode'] != '' || $d['brcode'] != 0 ? $d['brcode'] : $bid;
                $bcode_new  = generate_code_number( $d['btype'] );
                $agen       = '';
                $val        = '';

                if ( $d['agid'] != '' && $d['sagid'] != '' )
                {
                    $agen = 'agid,sagid,';
                    $val  = $d['agid'] . ',' . $d['sagid'] . ',';
                }
                elseif( $d['agid'] != '' && $d['sagid'] == '' )
                {
                    $agen = 'agid,';
                    $val  = $d['agid'] . ',';
                }

                /* INSERT BOOKING NEW */
                $sbooking = 'INSERT INTO ticket_booking(
                                chid,
                                btype,
                                bcode,
                                bticket,
                                bhotelname,
                                bhoteladdress,
                                bhotelphone,
                                bhotelemail,
                                bremark,
                                bagremark,
                                pmcode,
                                freelancecode,
                                bsubtotal,
                                bdiscount,
                                btotal,
                                bonhandtotal,
                                bcancellationfee,
                                bcancelationpercent,
                                btransactionfee,
                                brefund,
                                bdate,
                                bpaymethod,
                                bstatus,
                                bpvaliddate,
                                bcreason,
                                bcmessage,
                                bblockingtime,
                                bblockingtime_sts_48,
                                bblockingtime_sts_24,
                                bfeedback,'
                                .$agen.
                                'bbname,
                                bbphone,
                                bbphone2,
                                bbemail,
                                bbcountry,
                                bbrevtype,
                                bbrevstatus,
                                bbrevagent,
                                bbooking_staf,
                                bprintboardingstatus,
                                brcode,
                                bcdate,
                                brcdate,
                                bstact ) VALUES( %d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'.$val.'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,"3" )';
                $qbooking = $db->prepare_query( $sbooking,
                                $d['chid'],
                                $d['btype'],
                                $bcode_new,
                                $d['bticket'],
                                $d['bhotelname'],
                                $d['bhoteladdress'],
                                $d['bhotelphone'],
                                $d['bhotelemail'],
                                $d['bremark'],
                                $d['bagremark'],
                                $d['pmcode'],
                                $d['freelancecode'],
                                $d['bsubtotal'],
                                //$trprice,
                                $d['bdiscount'],
                                //$btotal_new,
                                $d['btotal'],
                                $d['bonhandtotal'],
                                ( $trcancelfee + $d['bcancellationfee'] ),
                                ( ( ( $d['bcancellationfee'] + $trcancelfee ) / $d['btotal'] ) * 100 ),
                                ( $trtransactionfee + $d['btransactionfee'] ),
                                ( $trrefund + $d['brefund'] ),
                                $d['bdate'],
                                $d['bpaymethod'],
                                $d['bstatus'],
                                $d['bpvaliddate'],
                                $trreason,
                                $trmessage,
                                $d['bblockingtime'],
                                $d['bblockingtime_sts_48'],
                                $d['bblockingtime_sts_24'],
                                $d['bfeedback'],
                                $d['bbname'],
                                $d['bbphone'],
                                $d['bbphone2'],
                                $d['bbemail'],
                                $d['bbcountry'],
                                $d['bbrevtype'],
                                $d['bbrevstatus'],
                                $d['bbrevagent'],
                                $d['bbooking_staf'],
                                $d['bprintboardingstatus'],
                                $brcode,
                                date( 'Y-m-d h:i:s', strtotime( $trdate ) ),
                                date( 'Y-m-d h:i:s', strtotime( $trrcdate ) ) );
                $do_booking = $db->do_query( $qbooking );

                if( is_array( $do_booking ) )
                {
                    $retval = 0;
                }
                else
                {
                    $idbooking  = $db->insert_id();

                    foreach ( $d['detail'] as $det_key => $det_val )
                    {
                        foreach ( $det_val as $kdet => $dt )
                        {
                            if ( $bdid == $kdet )
                            {
                                $trip_is_cancel = ucwords( $dt['bdtype'] ) . '<br/>' . $dt['bdfrom'] . ' to ' . $dt['bdto'];
                            }

                            $set_status      = isset( $dt['bdpstatus'] ) && $dt['bdpstatus'] != '' ? $dt['bdpstatus'] : $d['bstatus'];
                            $btripstatus     = $bdid == $kdet ? 'cn' : 'aa';
                            $bdpstatus       = $bdid == $kdet ? 'cn' : $set_status;

                            $bdcdate         = $bdid == $kdet ? date( 'Y-m-d' ) : '';
                            $treason         = $bdid == $kdet ? $trreason : '';
                            $tmessage        = $bdid == $kdet ? $trmessage : '';
                            $btransfee       = isset( $post['trtransactionfee'] ) && $bdid == $kdet && $trtransactionfee != '' ? $trtransactionfee : 0;
                            $bcancelfee      = isset( $post['trcancelfee'] ) && $bdid == $kdet && $trcancelfee != '' ? $trcancelfee : 0;
                            $bcancelpercent  = isset( $post['trcancelpercent'] ) && $bdid == $kdet && $trcancelpercent != '' ? $trcancelpercent : 0;
                            $trefund         = isset( $post['trrefund'] ) && $bdid == $kdet && $trrefund != '' ? $trrefund : 0;
                            $commission_type = empty( $dt['commission_type'] ) ? 0 : $dt['commission_type'];

                            /* INSERT TICKET BOOKING DETAIl */
                            $bdparam = array(
                                'bid'                      => $idbooking,
                                'rid'                      => $dt['rid'],
                                'sid'                      => $dt['sid'],
                                'boid'                     => $dt['boid'],
                                'bdto'                     => $dt['bdto'],
                                'total'                    => $dt['total'],
                                'bdtype'                   => $dt['bdtype'],
                                'bdfrom'                   => $dt['bdfrom'],
                                'bddate'                   => $dt['bddate'],
                                'bdto_id'                  => $dt['bdto_id'],
                                'subtotal'                 => $dt['subtotal'],
                                'discount'                 => $dt['discount'],
                                'bdfrom_id'                => $dt['bdfrom_id'],
                                'num_adult'                => $dt['num_adult'],
                                'num_child'                => $dt['num_child'],
                                'num_infant'               => $dt['num_infant'],
                                'commission'               => $dt['commission'],
                                'bdtranstype'              => $dt['bdtranstype'],
                                'bddeparttime'             => $dt['bddeparttime'],
                                'bdarrivetime'             => $dt['bdarrivetime'],
                                'transport_fee'            => $dt['transport_fee'],
                                'commission_type'          => $commission_type,
                                'price_per_adult'          => $dt['price_per_adult'],
                                'price_per_child'          => $dt['price_per_child'],
                                'price_per_infant'         => $dt['price_per_infant'],
                                'net_price_per_adult'      => $dt['net_price_per_adult'],
                                'net_price_per_child'      => $dt['net_price_per_child'],
                                'net_price_per_infant'     => $dt['net_price_per_infant'],
                                'disc_price_per_adult'     => $dt['disc_price_per_adult'],
                                'disc_price_per_child'     => $dt['disc_price_per_child'],
                                'disc_price_per_infant'    => $dt['disc_price_per_infant'],
                                'selling_price_per_adult'  => $dt['selling_price_per_adult'],
                                'selling_price_per_child'  => $dt['selling_price_per_child'],
                                'selling_price_per_infant' => $dt['selling_price_per_infant'],
                                'bdrevstatus'              => $dt['bdrevstatus'],
                                'bdcancelpercentfee'       => $bcancelpercent,
                                'bdstatus'                 => $btripstatus,
                                'bdcancelfee'              => $bcancelfee,
                                'bdtransactionfee'         => $btransfee,
                                'bdpstatus'                => $bdpstatus,
                                'bdcmessage'               => $tmessage,
                                'bdrefund'                 => $trefund,
                                'bdcdate'                  => $bdcdate,
                                'bdcreason'                => $treason
                            );

                            if( empty( $bdparam['bdcdate'] ) )
                            {
                                unset( $bdparam['bdcdate'] );
                            }

                            $qdetail  = 'INSERT INTO ticket_booking_detail(' . implode( ',', array_keys( $bdparam ) ) . ') VALUES ("' . implode( '" , "', $bdparam ) . '")';
                            $dodetail = $db->do_query( $qdetail );

                            if ( is_array( $dodetail ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $detail_id  = $db->insert_id();

                                /* INSERT BOOKING PASSANGER AND PORT CLEARANCE */
                                foreach ( $dt['passenger'] as $pass )
                                {
                                    foreach ( $pass as $vpass )
                                    {
                                        $bpparam = array(
                                            'bdid'        => $detail_id,
                                            'bptype'      => $vpass['bptype'],
                                            'bpname'      => $vpass['bpname'],
                                            'bpgender'    => $vpass['bpgender'],
                                            'bpbirthdate' => $vpass['bpbirthdate'],
                                            'lcountry_id' => $vpass['lcountry_id'],
                                            'bpstatus'    => ( $bdid == $kdet ? 'cn' : $vpass['bpstatus'] )
                                        );

                                        if( $vpass['bpbirthdate'] == '0000-00-00' )
                                        {
                                            unset( $bpparam['bpbirthdate'] );
                                        }

                                        $qpass  = 'INSERT INTO ticket_booking_passenger(' . implode( ',', array_keys( $bpparam ) ) . ') VALUES ("' . implode( '" , "', $bpparam ) . '")';
                                        $dopass = $db->do_query( $qpass );

                                        if ( is_array( $dopass ) )
                                        {
                                            $retval = 0;
                                        }
                                        else
                                        {
                                            $bpid_new = $db->insert_id();

                                            $port_clereance_detail = get_ticket_port_clereance_detail( $vpass['bpid'] );

                                            if ( !empty( $port_clereance_detail ) )
                                            {
                                                $sportclereancedetail = 'INSERT INTO ticket_port_clearance_detail(
                                                                            pcid,
                                                                            bpid,
                                                                            lcountry_id,
                                                                            guest_name,
                                                                            guest_type,
                                                                            guest_country,
                                                                            guest_gender,
                                                                            guest_age) VALUES ( %d,%d,%s,%s,%s,%s,%s,%s )';
                                                $qportclereancedetail = $db->prepare_query( $sportclereancedetail,
                                                                            $port_clereance_detail['pcid'],
                                                                            $bpid_new,
                                                                            $port_clereance_detail['lcountry_id'],
                                                                            $port_clereance_detail['guest_name'],
                                                                            $port_clereance_detail['guest_type'],
                                                                            $port_clereance_detail['guest_country'],
                                                                            $port_clereance_detail['guest_gender'],
                                                                            $port_clereance_detail['guest_age'] );
                                                $rportclereance = $db->do_query( $qportclereancedetail );

                                                if( is_array( $rportclereance ) )
                                                {
                                                    $retval = 0;
                                                }
                                            }
                                        }
                                    }
                                }

                                /* INSERT BOOKING TRANSPORT */
                                if ( $retval == 1 )
                                {
                                    foreach ( $dt['transport'] as $trans )
                                    {
                                        foreach ( $trans as $tr )
                                        {
                                            $tfield = '';
                                            $tval   = '';

                                            if ( $tr['hid'] != '' && $tr['taid'] != '' )
                                            {
                                                $tfield = 'hid,taid,';
                                                $tval   = $tr['hid'] . ',' . $tr['taid'] . ',';
                                            }
                                            elseif( $tr['hid'] != '' && $tr['taid'] == '' )
                                            {
                                                $tfield = 'hid,';
                                                $tval   = $tr['hid'] . ',';
                                            }
                                            elseif( $tr['hid'] == '' && $tr['taid'] != '' )
                                            {
                                                $tfield = 'taid,';
                                                $tval   = $tr['taid'] . ',';
                                            }

                                            $strans = 'INSERT INTO ticket_booking_transport(
                                                        bdid,'
                                                        .$tfield.
                                                        'bttype,
                                                        bttrans_type,
                                                        bthotelname,
                                                        bthoteladdress,
                                                        bthotelphone,
                                                        bthotelemail,
                                                        bthotelroomnumber,
                                                        btdrivername,
                                                        btdriverphone,
                                                        btflighttime,
                                                        btrpfrom,
                                                        btrpto,
                                                        bttrans_fee,
                                                        btstatus) VALUES ( %d,'.$tval.'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s )';
                                            $qtrans = $db->prepare_query( $strans,
                                                       $detail_id,
                                                       $tr['bttype'],
                                                       $tr['bttrans_type'],
                                                       $tr['bthotelname'],
                                                       $tr['bthoteladdress'],
                                                       $tr['bthotelphone'],
                                                       $tr['bthotelemail'],
                                                       $tr['bthotelroomnumber'],
                                                       $tr['btdrivername'],
                                                       $tr['btdriverphone'],
                                                       $tr['btflighttime'],
                                                       $tr['btrpfrom'],
                                                       $tr['btrpto'],
                                                       $tr['bttrans_fee'],
                                                       $btripstatus);
                                            $res_trans  = $db->do_query( $qtrans );

                                            if( is_array( $res_trans ) )
                                            {
                                                $retval = 0;
                                            }
                                            else
                                            {
                                                $trans_id   = $db->insert_id();
                                                $detail_trip_transport = get_ticket_trip_transport_detail( $tr['btid'] );

                                                if ( !empty( $detail_trip_transport ) )
                                                {
                                                    $striptransdet = 'INSERT INTO ticket_trip_transport_detail(ttid, btid,ttdtype) VALUES(%d,%d,%s)';
                                                    $qtriptransdet = $db->prepare_query( $striptransdet, $detail_trip_transport['ttid'], $trans_id, $detail_trip_transport['ttdtype'] );
                                                    $res_trans_det = $db->do_query( $qtriptransdet );

                                                    if( is_array( $res_trans_det ) )
                                                    {
                                                        $retval = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if ( $dt['bdpstatus'] != 'pa' )
                            {
                                if ( $btripstatus == 'cn' )
                                {
                                    $btotal_new = $btotal_new - $trprice;
                                }
                            }
                        }
                    }

                    if ( $retval == 1 )
                    {
                        if ( $d['agid'] != '' )
                        {
                            foreach ( $d['agent_transaction'] as $id => $val )
                            {
                                $sagentransaction = 'INSERT INTO ticket_agent_transaction(
                                                        agid,
                                                        bcode,
                                                        bid,
                                                        atdate,
                                                        atmid,
                                                        atmethod,
                                                        atgname,
                                                        atdebet,
                                                        atcredit,
                                                        atstatus) VALUES(%d,%s,%d,%s,%s,%s,%s,%d,%d,%s)';
                                $qagentransaction = $db->prepare_query( $sagentransaction,
                                                        $val['agid'],
                                                        $bcode_new,
                                                        $idbooking,
                                                        $val['atdate'],
                                                        $val['atmid'],
                                                        $val['atmethod'],
                                                        $val['atgname'],
                                                        $val['atdebet'],
                                                        $val['atcredit'],
                                                        $val['atstatus'] );
                                $doagentransaction = $db->do_query( $qagentransaction );

                                if( is_array( $doagentransaction ) )
                                {
                                    $retval = 0;
                                }
                            }
                        }
                    }
                }

                /* UPDATE STATUS TICKET BOOKING OLD AND INSERT PAYMENT BOOKING */
                if ( $retval == 1 )
                {
                    $update_booking = 'UPDATE ticket_booking SET bstatus=%s, bstt=%s WHERE bid=%d';
                    $p_update       = $db->prepare_query( $update_booking, 'cn', 'ar', $bid );
                    $do_update      = $db->do_query( $p_update );

                    $update_booking_new    = $db->prepare_query( 'UPDATE ticket_booking SET bsubtotal=%s, btotal=%s WHERE bid=%d', ( $btotal_new - $d['bdiscount'] ), $btotal_new, $idbooking );
                    $do_update_booking_new = $db->do_query( $update_booking_new );

                    if( is_array( $do_update ) && is_array( $do_update_booking_new ) )
                    {
                        $retval = 0;
                    }
                    else
                    {
                        if ( isset( $d['payment'] ) && !empty( $d['payment'] ) )
                        {
                            foreach( $d['payment'] as $p )
                            {
                                $spayment = 'INSERT INTO ticket_booking_payment(
                                                bid,
                                                ppayid,
                                                ppaycode,
                                                prefcode,
                                                ppayemail,
                                                ppaydate,
                                                ptotal,
                                                pnoaccount,
                                                paccountname,
                                                pbankname,
                                                pvirtualacc,
                                                pvirtualaccvaliddate,
                                                pvirtualaccvalidtime,
                                                pmethod,
                                                pstatus,
                                                pnote,
                                                papproval,
                                                pchname,
                                                pbrand,
                                                pwords,
                                                pmcn,
                                                presult) VALUES (%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s)';
                                $qpayment = $db->prepare_query( $spayment,
                                                $idbooking,
                                                $p['ppayid'],
                                                $p['ppaycode'],
                                                $p['prefcode'],
                                                $p['ppayemail'],
                                                $p['ppaydate'],
                                                $p['ptotal'],
                                                $p['pnoaccount'],
                                                $p['paccountname'],
                                                $p['pbankname'],
                                                $p['pvirtualacc'],
                                                $p['pvirtualaccvaliddate'],
                                                $p['pvirtualaccvalidtime'],
                                                $p['pmethod'],
                                                $p['pstatus'],
                                                $p['pnote'],
                                                $p['papproval'],
                                                $p['pchname'],
                                                $p['pbrand'],
                                                $p['pwords'],
                                                $p['pmcn'],
                                                $p['presult']);
                                $do_payment = $db->do_query( $qpayment );

                                if( is_array( $do_payment ) )
                                {
                                    $retval = 0;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                $retval = 0;
            }

            if( $retval == 0 )
            {
                $db->rollback();

                return 0;
            }
            else
            {
                save_log( $bid, 'reservation', 'Cancellation Trip #' . $d['bticket'] . ' : <br/>' . $trip_is_cancel );

                $db->commit();

                return $idbooking;
            }
        }
    }
}

function booking_cancel_passager( $post )
{
    global $db;

    extract( $post );

    $d = ticket_booking_all_data( $bid );

    $idbooking = 0;
    $passnameagent = array();
    $pass_list_cn_log = array();

    if ( !empty( $d ) )
    {
        if( $pcreason == 'Other' )
        {
            $pcreason = $poreason;
        }

        $retval    = 1;
        $val       = '';
        $agen      = '';
        $brcodes   = ( $d['brcode'] == '' ) ? $bid : $d['brcode'];
        $bcode_new = generate_code_number( $d['btype'] );

        $db->begin();

        if ( $d['agid'] != '' && $d['sagid'] != '' )
        {
            $agen = 'agid,sagid,';
            $val  = $d['agid'] . ',' . $d['sagid'] . ',';
        }
        elseif( $d['agid'] != '' && $d['sagid'] == '' )
        {
            $agen = 'agid,';
            $val  = $d['agid'] . ',';
        }

        $sbooking =
        'INSERT INTO ticket_booking(
            chid,
            btype,
            bcode,
            bticket,
            bhotelname,
            bhoteladdress,
            bhotelphone,
            bhotelemail,
            bremark,
            bagremark,
            pmcode,
            freelancecode,
            bsubtotal,
            bdiscount,
            btotal,
            bonhandtotal,
            bcancellationfee,
            btransactionfee,
            brefund,
            bdate,
            bpaymethod,
            bstatus,
            bpvaliddate,
            bcreason,
            bcmessage,
            bblockingtime,
            bblockingtime_sts_48,
            bblockingtime_sts_24,
            bfeedback,'
            .$agen.
            'bbname,
            bbphone,
            bbphone2,
            bbemail,
            bbcountry,
            bbrevtype,
            bbrevstatus,
            bbrevagent,
            bbooking_staf,
            bprintboardingstatus,
            brcode,
            bcdate,
            brcdate,
            bstact ) VALUES( %d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'.$val.'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,"4" )';
        $qbooking = $db->prepare_query( $sbooking,
            $d['chid'],
            $d['btype'],
            $bcode_new,
            $d['bticket'],
            $d['bhotelname'],
            $d['bhoteladdress'],
            $d['bhotelphone'],
            $d['bhotelemail'],
            $d['bremark'],
            $d['bagremark'],
            $d['pmcode'],
            $d['freelancecode'],
            $d['bsubtotal'],
            $d['bdiscount'],
            $d['btotal'],
            $d['bonhandtotal'],
            $d['bcancellationfee'],
            $d['btransactionfee'],
            $d['brefund'],
            $d['bdate'],
            $d['bpaymethod'],
            $d['bstatus'],
            $d['bpvaliddate'],
            $pcreason,
            $pmessage,
            $d['bblockingtime'],
            $d['bblockingtime_sts_48'],
            $d['bblockingtime_sts_24'],
            $d['bfeedback'],
            $d['bbname'],
            $d['bbphone'],
            $d['bbphone2'],
            $d['bbemail'],
            $d['bbcountry'],
            $d['bbrevtype'],
            $d['bbrevstatus'],
            $d['bbrevagent'],
            $d['bbooking_staf'],
            $d['bprintboardingstatus'],
            $brcodes,
            date( 'Y-m-d h:i:s', strtotime( $pcdate ) ),
            date( 'Y-m-d h:i:s', strtotime( $prcdate ) ) );
        $do_booking = $db->do_query( $qbooking );

        if( is_array( $do_booking ) )
        {
            $retval = 0;
        }
        else
        {
            $total_new = 0;
            $idbooking = $db->insert_id();

            $total_cancel_fee      = 0;
            $total_transaction_fee = 0;
            $total_refunds         = 0;
            $price_passangers      = 0;

            foreach ( $d['detail'] as $trip => $datas )
            {
                foreach ( $datas as $bdid => $dt )
                {
                    $trip_pr_aktive    = 0;
                    $trip_price_aktive = 0;
                    $trip_disc_aktive  = 0;

                    $adult  = 0;
                    $child  = 0;
                    $infant = 0;

                    foreach ( $passenger[$trip][$bdid] as $t )
                    {
                        $trip_price_aktive = $t['status'] != 'cn' ? $trip_price_aktive + $t['price'] : $trip_price_aktive; // HITUNG PRICE SETELAH DISCOUNT
                        $trip_pr_aktive    = $t['status'] != 'cn' ? $trip_pr_aktive + $t['pr'] : $trip_pr_aktive; // HITUNG PRICE SEBELUM DISCOUNT
                        $trip_disc_aktive  = $t['status'] != 'cn' ? $trip_disc_aktive + $t['price_disc'] : $trip_disc_aktive; // HITUNG DISCOUNT PER PASSANGER

                        if ( $t['bptype'] == 'adult' && $t['status'] == 'aa' )
                        {
                            $adult++;
                        }

                        if ( $t['bptype'] == 'child' && $t['status'] == 'aa' )
                        {
                            $child++;
                        }

                        if ( $t['bptype'] == 'infant' && $t['status'] == 'aa' )
                        {
                            $infant++;
                        }
                    }
                    
                    $commission_type = empty( $dt['commission_type'] ) ? 0 : $dt['commission_type'];

                    $sdetail  = 'INSERT INTO ticket_booking_detail(
                                    bid,
                                    boid,
                                    rid,
                                    sid,
                                    bdtype,
                                    bdfrom,
                                    bdto,
                                    bdfrom_id,
                                    bdto_id,
                                    bddeparttime,
                                    bdarrivetime,
                                    bddate,
                                    bdtranstype,
                                    num_adult,
                                    num_child,
                                    num_infant,
                                    price_per_adult,
                                    price_per_child,
                                    price_per_infant,
                                    selling_price_per_adult,
                                    selling_price_per_child,
                                    selling_price_per_infant,
                                    net_price_per_adult,
                                    net_price_per_child,
                                    net_price_per_infant,
                                    disc_price_per_adult,
                                    disc_price_per_child,
                                    disc_price_per_infant,
                                    commission,
                                    commission_type,
                                    subtotal,
                                    transport_fee,
                                    discount,
                                    total,
                                    bdstatus,
                                    bdpstatus,
                                    bdrevstatus ) VALUES( %d,%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s )';
                    $qdetail  = $db->prepare_query( $sdetail,
                                    $idbooking,
                                    $dt['boid'],
                                    $dt['rid'],
                                    $dt['sid'],
                                    $dt['bdtype'],
                                    $dt['bdfrom'],
                                    $dt['bdto'],
                                    $dt['bdfrom_id'],
                                    $dt['bdto_id'],
                                    $dt['bddeparttime'],
                                    $dt['bdarrivetime'],
                                    $dt['bddate'],
                                    $dt['bdtranstype'],
                                    $adult,
                                    $child,
                                    $infant,
                                    $dt['price_per_adult'],
                                    $dt['price_per_child'],
                                    $dt['price_per_infant'],
                                    $dt['selling_price_per_adult'],
                                    $dt['selling_price_per_child'],
                                    $dt['selling_price_per_infant'],
                                    $dt['net_price_per_adult'],
                                    $dt['net_price_per_child'],
                                    $dt['net_price_per_infant'],
                                    $dt['disc_price_per_adult'],
                                    $dt['disc_price_per_child'],
                                    $dt['disc_price_per_infant'],
                                    $dt['commission'],
                                    $commission_type,
                                    $trip_pr_aktive,
                                    $dt['transport_fee'],
                                    $trip_disc_aktive,
                                    $trip_price_aktive,
                                    $dt['bdstatus'],
                                    $dt['bdpstatus'] == '' ? $d['bstatus'] : $dt['bdpstatus'],
                                    $dt['bdrevstatus'] );
                    $dodetail = $db->do_query( $qdetail );

                    if ( is_array( $dodetail ) )
                    {
                        $retval = 0;
                    }
                    else
                    {
                        $detail_id = $db->insert_id();

                        foreach ( $passenger[$trip][$bdid] as $g )
                        {
                            $total_cancel_fee      = $total_cancel_fee + $g['cancelationfee'];
                            $total_transaction_fee = $total_transaction_fee + $g['transactionfee'];
                            $total_refunds         = $total_refunds + $g['refund'];
                            $price_passangers      = $g['status'] == 'cn' ? $price_passangers + $g['price'] : $price_passangers;

                            if ( $g['status'] != 'cn' )
                            {
                                $passnameagent[] = $g['name'];
                            }
                            else
                            {
                                $pass_list_cn_log[] = $g['name'];
                            }

                            $bprefund         = empty( $g['refund'] ) ? 0 : $g['refund'];
                            $bppercentfee     = empty( $g['cancelpercent'] ) ? 0 : $g['cancelpercent'];
                            $bpcancelfee      = empty( $g['cancelationfee'] ) ? 0 : $g['cancelationfee'];
                            $bptransactionfee = empty( $g['transactionfee'] ) ? 0 : $g['transactionfee'];
                            $bpbirthdate      = $g['bpbirthdate'] == '0000-00-00' ? '' : $g['bpbirthdate'];
                            $bpcdate          = $g['status'] == 'cn' ? date( 'Y-m-d', strtotime( $pcdate ) ) : '';

                            $pparam = array(
                                'bdid'             => $detail_id,
                                'bpname'           => $g['name'],
                                'bpstatus'         => $g['status'],
                                'bptype'           => $g['bptype'],
                                'bpgender'         => $g['bpgender'],
                                'lcountry_id'      => $g['lcountry_id'],
                                'bptransactionfee' => $bptransactionfee,
                                'bppercentfee'     => $bppercentfee,
                                'bpcancelfee'      => $bpcancelfee,
                                'bpbirthdate'      => $bpbirthdate,
                                'bprefund'         => $bprefund,
                                'bpcdate'          => $bpcdate
                            );

                            if( empty( $pparam['bpbirthdate'] ) )
                            {
                                unset( $pparam['bpbirthdate'] );
                            }

                            if( empty( $pparam['bpcdate'] ) )
                            {
                                unset( $pparam['bpcdate'] );
                            }

                            $qpass    = 'INSERT INTO ticket_booking_passenger(' . implode( ',', array_keys( $pparam ) ) . ') VALUES ("' . implode( '" , "', $pparam ) . '")';
                            $res_pass = $db->do_query( $qpass );

                            if( is_array( $res_pass ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $bpid_new              = $db->insert_id();
                                $port_clereance_detail = get_ticket_port_clereance_detail( $g['bpid'] );

                                if ( $g['status'] != 'cn' )
                                {
                                    if ( !empty( $port_clereance_detail ) )
                                    {
                                        $sportclereancedetail = 'INSERT INTO ticket_port_clearance_detail(
                                                                    pcid,
                                                                    bpid,
                                                                    lcountry_id,
                                                                    guest_name,
                                                                    guest_type,
                                                                    guest_country,
                                                                    guest_gender,
                                                                    guest_age) VALUES ( %d,%d,%s,%s,%s,%s,%s,%s )';
                                        $qportclereancedetail = $db->prepare_query( $sportclereancedetail,
                                                                    $port_clereance_detail['pcid'],
                                                                    $bpid_new,
                                                                    $port_clereance_detail['lcountry_id'],
                                                                    $port_clereance_detail['guest_name'],
                                                                    $port_clereance_detail['guest_type'],
                                                                    $port_clereance_detail['guest_country'],
                                                                    $port_clereance_detail['guest_gender'],
                                                                    $port_clereance_detail['guest_age'] );
                                        $rportclereance = $db->do_query( $qportclereancedetail );

                                        if( is_array( $rportclereance ) )
                                        {
                                            $retval = 0;
                                        }
                                    }
                                }
                            }
                        }

                        if( $retval == 1 )
                        {
                            foreach ( $dt['transport'] as $trans )
                            {
                                foreach ( $trans as $tr )
                                {
                                    $hid          = isset( $tr['hid'] ) ? $tr['hid'] : NULL;
                                    $taid         = isset( $tr['taid'] ) ? $tr['taid'] : NULL;
                                    $btflighttime = isset( $tr['btflighttime'] ) && $tr['btflighttime'] == '0000-00-00 00:00:00' ? '' : $tr['btflighttime'];

                                    $tparam = array(
                                        'hid'               => $hid,
                                        'taid'              => $taid,
                                        'bdid'              => $detail_id,
                                        'btflighttime'      => $btflighttime,
                                        'bttype'            => $tr['bttype'],
                                        'btrpto'            => $tr['btrpto'],
                                        'btrpfrom'          => $tr['btrpfrom'],
                                        'bttrans_fee'       => $tr['bttrans_fee'],
                                        'bthotelname'       => $tr['bthotelname'],
                                        'bttrans_type'      => $tr['bttrans_type'],
                                        'bthotelphone'      => $tr['bthotelphone'],
                                        'bthotelemail'      => $tr['bthotelemail'],
                                        'btdrivername'      => $tr['btdrivername'],
                                        'btdriverphone'     => $tr['btdriverphone'],
                                        'bthoteladdress'    => $tr['bthoteladdress'],
                                        'bthotelroomnumber' => $tr['bthotelroomnumber']
                                    );

                                    if( empty( $btflighttime ) )
                                    {
                                        unset( $tparam['btflighttime'] );
                                    }

                                    if( $tr['bttrans_type'] == 2 || empty( $hid ) || empty( $taid ) )
                                    {
                                        unset( $tparam['hid'] );
                                        unset( $tparam['taid'] );
                                    }

                                    $qtrans    = 'INSERT INTO ticket_booking_transport(' . implode( ',', array_keys( $tparam ) ) . ') VALUES ("' . implode( '" , "', $tparam ) . '")';
                                    $res_trans = $db->do_query( $qtrans );

                                    if( is_array( $res_trans ) )
                                    {
                                        $retval = 0;
                                    }
                                    else
                                    {
                                        $trans_id              = $db->insert_id();
                                        $detail_trip_transport = get_ticket_trip_transport_detail( $tr['btid'] );

                                        if ( !empty( $detail_trip_transport ) )
                                        {
                                            $striptransdet = 'INSERT INTO ticket_trip_transport_detail(ttid, btid,ttdtype) VALUES(%d,%d,%s)';
                                            $qtriptransdet = $db->prepare_query( $striptransdet, $detail_trip_transport['ttid'], $trans_id, $detail_trip_transport['ttdtype'] );
                                            $res_transdet = $db->do_query( $qtriptransdet );

                                            if( is_array( $res_transdet ) )
                                            {
                                                $retval = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ( $dt['bdpstatus'] == 'pa' )
                    {
                        $total_new = $total_new + $dt['total'];
                    }
                    else
                    {
                        $total_new = $total_new + $trip_price_aktive;
                    }
                }
            }

            if ( $retval == 1 )
            {
                if ( $d['agid'] != '' )
                {
                    $parray   = array_unique( $passnameagent );
                    $pax_list = implode( ',', $parray );

                    foreach ( $d['agent_transaction'] as $val )
                    {
                        $sagentransaction = 'INSERT INTO ticket_agent_transaction(
                                                agid,
                                                bcode,
                                                bid,
                                                atdate,
                                                atmid,
                                                atmethod,
                                                atgname,
                                                atdebet,
                                                atcredit,
                                                atstatus) VALUES(%d,%s,%d,%s,%s,%s,%s,%d,%d,%s)';
                        $qagentransaction = $db->prepare_query( $sagentransaction,
                                                $val['agid'],
                                                $bcode_new,
                                                $idbooking,
                                                $val['atdate'],
                                                $val['atmid'],
                                                $val['atmethod'],
                                                $pax_list,
                                                $val['atdebet'],
                                                $val['atcredit'],
                                                $val['atstatus'] );
                        $doagentransaction = $db->do_query( $qagentransaction );

                        if( is_array( $doagentransaction ) )
                        {
                            $retval = 0;
                        }
                    }
                }
            }

            if ( $retval == 1 )
            {
                if ( isset( $d['payment'] ) && !empty( $d['payment'] ) )
                {
                    foreach( $d['payment'] as $p )
                    {
                        $spayment = 'INSERT INTO ticket_booking_payment(
                                        bid,
                                        ppayid,
                                        ppaycode,
                                        prefcode,
                                        ppayemail,
                                        ppaydate,
                                        ptotal,
                                        pnoaccount,
                                        paccountname,
                                        pbankname,
                                        pvirtualacc,
                                        pvirtualaccvaliddate,
                                        pvirtualaccvalidtime,
                                        pmethod,
                                        pstatus,
                                        pnote,
                                        papproval,
                                        pchname,
                                        pbrand,
                                        pwords,
                                        pmcn,
                                        presult) VALUES (%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s)';
                        $qpayment = $db->prepare_query( $spayment,
                                        $idbooking,
                                        $p['ppayid'],
                                        $p['ppaycode'],
                                        $p['prefcode'],
                                        $p['ppayemail'],
                                        $p['ppaydate'],
                                        $p['ptotal'],
                                        $p['pnoaccount'],
                                        $p['paccountname'],
                                        $p['pbankname'],
                                        $p['pvirtualacc'],
                                        $p['pvirtualaccvaliddate'],
                                        $p['pvirtualaccvalidtime'],
                                        $p['pmethod'],
                                        $p['pstatus'],
                                        $p['pnote'],
                                        $p['papproval'],
                                        $p['pchname'],
                                        $p['pbrand'],
                                        $p['pwords'],
                                        $p['pmcn'],
                                        $p['presult'] );
                        $do_payment = $db->do_query( $qpayment );

                        if( is_array( $do_payment ) )
                        {
                            $retval = 0;
                        }
                    }

                    if( $retval == 1 )
                    {
                        // UPDATE BOOKING OLD
                        $update_booking     = 'UPDATE ticket_booking SET bstatus=%s, bstt=%s WHERE bid=%d';
                        $p_update           = $db->prepare_query( $update_booking, 'cn', 'ar', $bid );
                        $do_update_b        = $db->do_query( $p_update );
                        // UPDATE BOOKING NEW
                        $update_booking_new = 'UPDATE ticket_booking SET
                                                    bcancellationfee=%s,
                                                    bcancelationpercent=%s,
                                                    btransactionfee=%s,
                                                    brefund=%s,
                                                    bsubtotal=%s,
                                                    btotal=%s
                                                WHERE bid=%d';
                        $p_update_new       = $db->prepare_query( $update_booking_new,
                                                    ( $total_cancel_fee + $d['bcancellationfee'] ),
                                                    ( ( ( $d['bcancellationfee'] + $total_cancel_fee ) / $d['btotal'] ) * 100 ),
                                                    ( $total_transaction_fee + $d['btransactionfee'] ),
                                                    ( $total_refunds + $d['brefund'] ),
                                                    $total_new,
                                                    ( $total_new - $d['bdiscount'] ),
                                                $idbooking );

                        $do_update_b_new    = $db->do_query( $p_update_new );

                        if( is_array( $do_update_b ) && is_array( $do_update_b_new ) )
                        {
                            $retval = 0;
                        }
                    }
                }
            }
        }
    }
    else
    {
        $retval = 0;
    }

    if( $retval == 0 )
    {
        $db->rollback();

        return 0;
    }
    else
    {
        save_log( $bid, 'reservation', 'Cancellation Passanger #' . $d['bticket'] . ' : <br/>' . implode( '<br/>', array_unique( $pass_list_cn_log ) ) );

        $db->commit();

        return $idbooking;
    }
}

/*
| -------------------------------------------------------------------------------------
| Booking Cancel | Send Email
| -------------------------------------------------------------------------------------
*/
function send_booking_cancelation_request_to_admin( $bid, $type='', $ctype='' )
{
    $data = ticket_booking_all_data( $bid  );

    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = get_meta_data( 'email_reservation' );
            $email_to_2 = get_meta_data( 'email_reservation_2' );
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = $type == 'booking-cancel' ? get_booking_cancelation_to_admin( $data, $ctype ) : get_booking_cancelation_request_to_admin( $data );

            $email_arr  = explode( ';', $email_to_2 );

            if( $data['bstatus'] == 'cr' )
            {
                $subject = 'New Booking Cancelled Request #' . $data['bticket'];
            }
            else
            {
                $subject = 'Booking Cancelled #' . $data['bticket'];
            }

            if( empty( $email_to_2 )  )
            {
                $message  = array(
                    'from_name'  => get_meta_data('web_title'),
                    'from_email' => $email_from,
                    'html'       => $email_data,
                    'subject'    => $subject,
                    'to' => array(
                        array(
                            'email' => $email_to,
                            'name'  => '',
                            'type'  => 'to'
                        )
                    ),
                    'headers' => array( 'Reply-To' => $email_from )
                );
            }
            else
            {
                $send_to = array();

                $send_to[] = array(
                    'email' => $email_to,
                    'name'  => '',
                    'type'  => 'to'
                );

                foreach ( $email_arr as $em )
                {
                    $send_to[] = array(
                        'email' => $em,
                        'name'  => '',
                        'type'  => 'to'
                    );
                }

                $message  = array(
                    'from_name'  => get_meta_data( 'web_title' ),
                    'from_email' => $email_from,
                    'html'       => $email_data,
                    'subject'    => $subject,
                    'to'         => $send_to,
                    'headers'    => array( 'Reply-To' => $email_from )
                );
            }

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

function send_booking_cancelation_notif_to_client( $bid, $type='', $ctype='' )
{
    $data = ticket_booking_all_data( $bid );

    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = $data['bbemail'];
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = $type == 'booking-cancel' ? get_cancelation_notif_to_client( $data, $ctype ) : get_booking_cancelation_notif_to_client( $data );

            if( $data['bstatus'] == 'cr' )
            {
                $subject = 'New Booking Cancelled Request #' . $data['bticket'];
            }
            else
            {
                $subject = 'Booking Cancelled #' . $data['bticket'];
            }

            $message  = array(
                'from_name'  => get_meta_data('web_title'),
                'from_email' => $email_from,
                'html'       => $email_data,
                'subject'    => $subject,
                'to' => array(
                    array(
                        'email' => $email_to,
                        'name'  => '',
                        'type'  => 'to'
                    )
                ),
                'headers' => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Function | Log
| -------------------------------------------------------------------------------------
*/
function get_value_before_update( $table, $f, $field_search, $val_id )
{
    global $db;

    $s = 'SELECT '.$f.' FROM '.$table.' WHERE '.$field_search.' = %d';
    $q = $db->prepare_query( $s, $val_id );
    $r = $db->do_query( $q );
    $dta = $db->fetch_array( $r );

    return $dta[ $f ];
}

function ticket_booking_agent_transaction( $bid )
{
    global $db;

    $s = 'SELECT * FROM ticket_agent_transaction AS a WHERE a.bid = %d';
    $q = $db->prepare_query( $s, $bid );
    $r = $db->do_query( $q );

    $data = array();

    if ( $db->num_rows( $r ) > 0 )
    {
        while ( $d = $db->fetch_array( $r ) )
        {
            $data[ $d['atid'] ] = array(
                'agid' => $d['agid'],
                'bcode' => $d['bcode'],
                'bid' => $d['bid'],
                'atdate' => $d['atdate'],
                'atmid' => $d['atmid'],
                'atmethod' => $d['atmethod'],
                'atgname' => $d['atgname'],
                'atdebet' => $d['atdebet'],
                'atcredit' => $d['atcredit'],
                'atstatus' => $d['atstatus'],
            );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Function | Generate Invoice for Agent
| -------------------------------------------------------------------------------------
*/
function generate_invoice_agent_pdf( $data, $type = 'S' )
{
    $content = get_invoice_agent_data( $data, false );

    if( !empty( $content ) )
    {
        require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

        if( $type == 'I'  )
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 0, 0 );

            $mpdf->setAutoTopMargin    = true;
            $mpdf->setAutoBottomMargin = true;

            $mpdf->SetTitle( 'Invoice #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->SetJS( 'this.print();' );
            $mpdf->Output( 'invoice-' . $data['bticket'] . '.pdf', $type );
        }
        elseif( $type == 'D'  )
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 0, 0 );
            $mpdf->setAutoTopMargin    = true;
            $mpdf->setAutoBottomMargin = true;
            $mpdf->SetTitle( 'Invoice #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );
            $mpdf->Output( 'invoice-' . $data['bticket'] . '.pdf', $type );
        }
        else
        {
            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 0, 0 );

            $mpdf->setAutoTopMargin    = true;
            $mpdf->setAutoBottomMargin = true;

            $mpdf->SetTitle( 'Invoice #' . $data['bticket'] );
            $mpdf->WriteHTML( $content );

            $content = $mpdf->Output( '', 'S' );

            return $content;
        }
    }
}

function get_invoice_agent_data( $data, $email=false )
{
    extract( $data['detail'] );

    $template = $email ? 'email' : 'pdf';
    $tplhtml  = $email ? '/ticket/tpl/email/notif-invoice-agent.html' : '/ticket/tpl/booking/invoice-agent.html';
    $agent    = get_agent( $data['agid'], 'agpayment_type' );

    set_template( PLUGINS_PATH . $tplhtml, 'invoice-agent-' . $template );

    add_block( $template . '-invoice-detail-departure-loop-block', 'inv-dep-loop-block', 'invoice-agent-' . $template );
    add_block( $template . '-invoice-detail-departure-block', 'inv-dep-block', 'invoice-agent-' . $template );
    add_block( $template . '-invoice-detail-return-loop-block', 'inv-ret-loop-block', 'invoice-agent-' . $template );
    add_block( $template . '-invoice-detail-return-block', 'inv-ret-block', 'invoice-agent-' . $template );
    add_block( $template . '-invoice-block', 'inv-block', 'invoice-agent-' . $template );

    //-- Go Trip
    if( isset( $departure ) )
    {
        $last = end( $departure );
        $first = reset( $departure );

        foreach( $departure as $key => $dp_trip )
        {
            extract( $dp_trip );

            if ( $bdstatus != 'cn' || $bdpstatus != 'cn' )
            {
                $var = get_variable_by_port_type( $bdfrom_id, $bdto_id );

                $dep_pa_display   = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                $dep_pc_display   = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                $dep_pi_display   = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                $dep_pw_discount  = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small>';
                $dep_trans_detail = '';

                if( !empty( $transport ) )
                {
                    foreach( $transport as $ttype => $trans )
                    {
                        foreach( $trans as $t )
                        {
                            if( $t['bttrans_type'] != '2' )
                            {
                                if( empty( $t['taid'] ) )
                                {
                                    $hdetail = '<p><b>No Accommodation Booked</b></p>';
                                }
                                else
                                {
                                    $tarea  = get_transport_area( $t['taid'] );
                                    $hname  = empty( $t['bthotelname'] ) ? '' : sprintf( '<p>%s</p>', $t['bthotelname'] );
                                    $haddr  = empty( $t['bthoteladdress'] ) ? '' : sprintf( '<p>%s</p>', $t['bthoteladdress'] );
                                    $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                    $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                    if( empty( $rpfrom ) && empty( $rpto ) )
                                    {
                                        $note = '';
                                    }
                                    else
                                    {
                                        if( $ttype == 'pickup' )
                                        {
                                            $note = '<p>In between ' . $rpfrom . ' - ' . $rpto . '</p>';
                                        }
                                        else
                                        {
                                            $note = '';
                                        }
                                    }

                                    if( empty( $tarea['taairport'] ) )
                                    {
                                        $ftime = '';
                                    }
                                    else
                                    {
                                        $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                        $ftime = sprintf( '<p>' . $flbl . ' %s</p>', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                    }

                                    if( empty( $t['hid'] ) )
                                    {
                                        $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $note;
                                    }
                                    elseif( $t['hid'] == 1 )
                                    {
                                        if( empty( $hname ) && empty( $haddr ) )
                                        {
                                            $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . 'Hotel to be advised' . $note;
                                        }
                                        else
                                        {
                                            $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $hname . $haddr . $note;
                                        }
                                    }
                                    else
                                    {
                                        if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                        {
                                            $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $note;
                                        }
                                        else
                                        {
                                            $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $hname . $haddr . $note;
                                        }
                                    }
                                }

                                if( $t['bttrans_type'] == '0' )
                                {
                                    $trans_type = 'Shared Transport';
                                }
                                else
                                {
                                    $trans_type = 'Privated Transport';
                                }

                                $dep_trans_detail .= '
                                <div class="clearfix">
                                    <div class="item trip">
                                        <strong>' . ucfirst( $ttype ) . '</strong>
                                        <p><b>' . $trans_type . '</b></p>
                                    </div>
                                    <div class="item transport">
                                        <strong>Transport Detail</strong>
                                        ' . $hdetail . '
                                    </div>
                                    <div class="item price">
                                        <strong>Transport Fee</strong>
                                        <p><b>' . ( $t['bttrans_fee'] == 0 ? 'FREE' : number_format( $t['bttrans_fee'], 0, ',', '.' ) ) . '</b></p>
                                    </div>
                                </div>';
                            }
                            else
                            {
                                $dep_trans_detail .= '
                                <div class="clearfix">
                                    <div class="item trip">
                                        <strong>' . ucfirst( $ttype ) . '</strong>
                                        <p><b>Own Transport</b></p>
                                    </div>
                                    <div class="item transport"></div>
                                    <div class="item price">
                                        <strong>Transport Fee</strong>
                                        <p><b>N/A</b></p>
                                    </div>
                                </div>';
                            }
                        }
                    }
                }

                add_variable( 'trip_dep_css', '' );
                add_variable( 'dep_bdto', $bdto );
                add_variable( 'dep_bdfrom', $bdfrom );
                add_variable( 'dep_pa_display', $dep_pa_display );
                add_variable( 'dep_pc_display', $dep_pc_display );
                add_variable( 'dep_pi_display', $dep_pi_display );
                add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'dep_item_class', $last['bdid'] == $bdid ? 'last' : '' );
                add_variable( 'dep_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                add_variable( 'dep_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );
                add_variable( 'dep_passenger', $email ? ticket_passenger_list_content( $passenger, true ) : ticket_passenger_list_for_pdf_content( $passenger ) );
                add_variable( 'dep_trans_detail', $email ? ticket_detail_transport_email_content( $transport, $bdfrom, $bdto, $data['bdate'], $bddate ) : $dep_trans_detail );
                add_variable( 'dep_pt_display', $dep_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );

                parse_template( $template . '-invoice-detail-departure-loop-block', 'inv-dep-loop-block', true );
            }
            else
            {
                add_variable( 'trip_dep_css', 'sr-only' );
                add_variable( 'trip_email_dep_css', 'display:none;' );
            }
        }

        parse_template( $template . '-invoice-detail-departure-block', 'inv-dep-block' );
    }

    //-- Back Trip
    if( isset( $return ) && $data['btype'] == '1' )
    {
        $last = end( $return );

        foreach( $return as $key => $rt_trip )
        {
            extract( $rt_trip );

            if ( $bdstatus != 'cn' || $bdpstatus != 'cn' )
            {
                $rtn_pa_display   = $num_adult == 0 ? '-' : $num_adult . ' x ' . number_format( $price_per_adult, 0, ',', '.' );
                $rtn_pc_display   = $num_child == 0 ? '-' : $num_child . ' x ' . number_format( $price_per_child, 0, ',', '.' );
                $rtn_pi_display   = $num_infant == 0 ? '-' : $num_infant . ' x ' . number_format( $price_per_infant, 0, ',', '.' );
                $rtn_pw_discount  = $discount == 0 ? '' : '<small>' . number_format( $subtotal, 0, ',', '.' ) . '</small><br/>';
                $rtn_trans_detail = '';

                if( !empty( $transport ) )
                {
                    foreach( $transport as $ttype => $trans )
                    {
                        foreach( $trans as $t )
                        {
                            if( $t['bttrans_type'] != '2' )
                            {
                                if( empty( $t['taid'] ) )
                                {
                                    $hdetail = '<b>No Accommodation Booked</b>';
                                }
                                else
                                {
                                    $tarea  = get_transport_area( $t['taid'] );
                                    $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                                    $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                                    $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                    $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                    if( empty( $rpfrom ) && empty( $rpto ) )
                                    {
                                        $note = '';
                                    }
                                    else
                                    {
                                        if( $ttype == 'pickup' )
                                        {
                                            $note = '<br/>In between ' . $rpfrom . ' - ' . $rpto;
                                        }
                                        else
                                        {
                                            $note = '';
                                        }
                                    }

                                    if( empty( $tarea['taairport'] ) )
                                    {
                                        $ftime = '';
                                    }
                                    else
                                    {
                                        $flbl  = $ttype == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
                                        $ftime = sprintf( '<br/>' . $flbl . ' %s', date( 'd F Y, H:i', strtotime( $t['btflighttime'] ) ) );
                                    }

                                    if( empty( $t['hid'] ) )
                                    {
                                        $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $note;
                                    }
                                    elseif( $t['hid'] == 1 )
                                    {
                                        if( empty( $hname ) && empty( $haddr ) )
                                        {
                                            $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . 'Hotel to be advised' . $note;
                                        }
                                        else
                                        {
                                            $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $hname . $haddr . $note;
                                        }
                                    }
                                    else
                                    {
                                        if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                        {
                                            $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $note;
                                        }
                                        else
                                        {
                                            $hdetail = '<p><b>' . $tarea['taname'] . '</b></p>' . $ftime . $hname . $haddr . $note;
                                        }
                                    }
                                }

                                if( $t['bttrans_type'] == '0' )
                                {
                                    $trans_type = 'Shared Transport';
                                }
                                else
                                {
                                    $trans_type = 'Privated Transport';
                                }

                                $rtn_trans_detail .= '
                                <div class="clearfix">
                                    <div class="item trip">
                                        <strong>' . ucfirst( $ttype ) . '</strong>
                                        <p><b>' . $trans_type . '</b></p>
                                    </div>
                                    <div class="item transport">
                                        <strong>Transport Detail</strong>
                                        <p>' . $hdetail . '</p>
                                    </div>
                                    <div class="item price">
                                        <strong>Transport Fee</strong>
                                        <p><b>' . ( $t['bttrans_fee'] == 0 ? 'FREE' : number_format( $t['bttrans_fee'], 0, ',', '.' ) ) . '</b></p>
                                    </div>
                                </div>';
                            }
                            else
                            {
                                $rtn_trans_detail .= '
                                <div class="clearfix">
                                    <div class="item trip">
                                        <strong>' . ucfirst( $ttype ) . '</strong>
                                        <p><b>Own Transport</b></p>
                                    </div>
                                    <div class="item transport"></div>
                                    <div class="item price">
                                        <strong>Transport Fee</strong>
                                        <p><b>N/A</b></p>
                                    </div>
                                </div>';
                            }
                        }
                    }
                }

                add_variable( 'trip_rtn_css', '' );
                add_variable( 'rtn_bdto', $bdto );
                add_variable( 'rtn_bdfrom', $bdfrom );
                add_variable( 'rtn_pa_display', $rtn_pa_display );
                add_variable( 'rtn_pc_display', $rtn_pc_display );
                add_variable( 'rtn_pi_display', $rtn_pi_display );
                add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'rtn_item_class', $last['bdid'] == $bdid ? 'last' : '' );
                add_variable( 'rtn_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                add_variable( 'rtn_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );
                add_variable( 'rtn_passenger', $email ? ticket_passenger_list_content( $passenger, true ) : ticket_passenger_list_for_pdf_content( $passenger ) );
                add_variable( 'rtn_trans_detail', $email ? ticket_detail_transport_email_content( $transport, $bdfrom, $bdto, $data['bdate'], $bddate ) : $rtn_trans_detail );
                add_variable( 'rtn_pt_display', $rtn_pw_discount . number_format( ( $subtotal - $discount ), 0, ',', '.' ) );

                parse_template( $template . '-invoice-detail-return-loop-block', 'inv-ret-loop-block', true );
            }
            else
            {
                add_variable( 'trip_rtn_css', 'sr-only' );
                add_variable( 'trip_email_rtn_css', 'display:none;' );
            }
        }

        parse_template( $template . '-invoice-detail-return-block', 'inv-ret-block' );
    }

    if( empty( $data['pmcode'] ) )
    {
        $pstatus = ( $data['btotal'] - $data['bonhandtotal'] ) <= 0 ? 'Paid' : 'Unpaid';

        add_variable( 'pstatus', $pstatus );
        add_variable( 'sub_css', $email ? 'display:none;' : 'sr-only' );
        add_variable( 'subtotal_display', number_format( $data['bsubtotal'], 0, ',', '.' ) );
        add_variable( 'discount_display', number_format( $data['bdiscount'], 0, ',', '.' ) );
        add_variable( 'grandtotal_display', number_format( $data['btotal'], 0, ',', '.' ) );
        add_variable( 'onhandtotal_display', number_format( $data['bonhandtotal'], 0, ',', '.' ) );
        add_variable( 'remaintotal_display', number_format( ( $data['btotal'] - $data['bonhandtotal'] ), 0, ',', '.' ) );
    }
    else
    {
        $pstatus = ( $data['btotal'] - $data['bonhandtotal'] ) <= 0 ? 'Paid' : 'Unpaid';

        add_variable( 'pstatus', $pstatus );
        add_variable( 'pmcode', 'Code : ' . $data['pmcode'] );
        add_variable( 'subtotal_display', number_format( $data['bsubtotal'], 0, ',', '.' ) );
        add_variable( 'discount_display', number_format( $data['bdiscount'], 0, ',', '.' ) );
        add_variable( 'grandtotal_display', number_format( $data['btotal'], 0, ',', '.' ) );
        add_variable( 'onhandtotal_display', number_format( $data['bonhandtotal'], 0, ',', '.' ) );
        add_variable( 'remaintotal_display', number_format( ( $data['btotal'] - $data['bonhandtotal'] ), 0, ',', '.' ) );
    }

    if ( $agent == 'Pre Paid' || $data['agid'] == '' )
    {
        $ftrip    = $first['bddate'];
        $due_date = get_due_date( $data['bdate'], $ftrip );
    }
    else
    {
        $ftrip    = $first['bddate'];
        $agcod    = get_agent( $data['agid'], 'agcod' );
        $due_date = date( 'd F Y', strtotime( $ftrip . ' +'. $agcod . 'days' ) );
    }

    add_variable( 'ddate', $due_date );
    add_variable( 'web_title', web_title() );
    add_variable( 'bbname', $data['bbname'] );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'bticket', $data['bticket'] );
    add_variable( 'bbemail', $data['bbemail'] );
    add_variable( 'bbphone', $data['bbphone'] );
    add_variable( 'agtype', $agent == 'Pre Paid' ? 'Proforma ' : '' );
    add_variable( 'bdate', date( 'd F Y', strtotime( $data['bdate'] ) ) );
    add_variable( 'bpaymethod', get_payment_method_by_id( $data['bpaymethod'] ) );
    add_variable( 'bbrevagent', empty( $data['bbrevagent'] ) ? '-' : $data['bbrevagent'] );
    add_variable( 'bbooking_staf', empty( $data['bbooking_staf'] ) ? '-' : $data['bbooking_staf'] );
    add_variable( 'bchannel', get_agent( empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'], 'agname' ) );
    add_variable( 'agphone', get_agent( empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'], 'agphone' ) );
    add_variable( 'agaddress', get_agent( empty( $data['sagid'] ) ? $data['agid'] : $data['sagid'], 'agaddress' ) );
    add_variable( 'style', HTSERVER . site_url() . '/l-plugins/ticket/css/receipt.css' );

    parse_template( $template . '-invoice-block', 'inv-block' );

    return return_template( 'invoice-agent-' . $template );
}

function send_invoice_to_client( $data )
{
    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_to   = $data['bbemail'];
            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = get_invoice_agent_data( $data, true );

            $message  = array(
                'subject'    => 'Invoice #' . $data['bticket'],
                'from_name'  => get_meta_data('web_title'),
                'from_email' => $email_from,
                'html'       => $email_data,
                'to' => array(
                    array(
                        'email' => $email_to,
                        'name'  => '',
                        'type'  => 'to'
                    )
                ),
                'headers' => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

function check_in_range( $sdate, $edate, $bddate )
{
    $sdate  = strtotime( $sdate );
    $edate  = strtotime( $edate );
    $bddate = strtotime( $bddate );

    return ( ( $bddate >= $sdate ) && ( $bddate <= $edate ) );
}

function check_in_range_2( $sdate, $edate, $bddate )
{
    $sdate  = strtotime( $sdate );
    $edate  = strtotime( $edate );
    $bddate = strtotime( $bddate );

    return ( ( $bddate >= $sdate ) && ( $bddate <= $edate ) );
}

function get_due_date( $bdate, $bddate )
{
    $y = date( 'Y', strtotime( $bddate ) );
    $i = date_diff( date_create( $bdate ), date_create( $bddate ) );

    if ( intval( $i->format( '%a' ) ) < 7 )
    {
        return date( 'd F Y', strtotime( $bddate . ' -1days' ) );
    }
    else
    {
        if( check_in_range( '01-01-' . $y, '30-04-'. $y, $bddate ) || check_in_range( '01-11-' . $y, '31-12-'. $y, $bddate ) )
        {
            return date( 'd F Y', strtotime( $bddate . ' -7days' ) );
        }
        elseif( check_in_range( '01-05-' . $y, '30-06-'. $y, $bddate ) || check_in_range( '01-10-' . $y, '31-10-'. $y, $bddate ) )
        {
            return date( 'd F Y', strtotime( $bddate . ' -30days' ) ) . ' s/d ' . date( 'd F Y', strtotime( $bddate . ' -14days' ) );
        }
        elseif( check_in_range( '01-07-' . $y, '31-09-'. $y, $bddate ) )
        {
            return date( 'd F Y', strtotime( $bddate . ' -30days' ) );
        }
    }
}

function get_recomendation_villa()
{
    extract( $_GET );

    global $db;

    $s = 'SELECT
            a.*,
            b.bdto
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON a.bid = b.bid
          Where a.bid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    $recomendation_vila = '';

    if( $db->num_rows( $r ) )
    {
        $d = $db->fetch_array( $r );

        if( $d['bdto'] == 'Gili Trawangan' )
        {
            $site_url = site_url();

            $recomendation_vila = '
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
                <div class="container">
                    <br>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <a href="https://www.kunovillas.com/" target="blank_">
                                <img src="//' . $site_url . '/l-content/themes/custom/images/Kuno.jpeg" max-width="460" max-height="345">
                            </a>
                        </div>
                        <div class="item">
                            <a href="https://www.kunovillas.com/" target="blank_">
                                <img src="//' . $site_url . '/l-content/themes/custom/images/kuno_2.jpeg" max-width="460" max-height="345">
                            </a>
                        </div>
                        <div class="item">
                            <a href="https://www.kunovillas.com/" target="blank_">
                                <img src="//' . $site_url . '/l-content/themes/custom/images/kuno_3.jpg"  max-width="460" max-height="345">
                            </a>
                        </div>
                        <div class="item">
                            <a href="https://www.kunovillas.com/" target="blank_">
                                <img src="//' . $site_url . '/l-content/themes/custom/images/kuno_4.jpg" max-width="460" max-height="345">
                            </a>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <style>
                .carousel-inner > .item > img,
                .carousel-inner > .item > a > img{ width:100%; max-height:450px; object-fit:cover; margin:auto; }
                .container{ max-width:980px !important; max-height:450px!important; }
            </style>';
        }
    }

    return $recomendation_vila;
}

?>
