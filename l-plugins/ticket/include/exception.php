<?php

add_actions( 'exception', 'ticket_exception' );
add_actions( 'ticket-exception-ajax_page', 'ticket_exception_ajax' );

function ticket_exception()
{
    if( is_num_exception() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'schedules&sub=exception&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_exception();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_exception( $_GET['id'] ) )
        {
            return ticket_edit_exception();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_exception();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_exception( $val );
        }
    }

    return ticket_exception_table();
}



/*
| -------------------------------------------------------------------------------------
| Exception Table List
| -------------------------------------------------------------------------------------
*/
function ticket_exception_table()
{
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/exception/list.html', 'exception' );

    add_block( 'list-block', 'lcblock', 'exception' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'schedules&sub=exception' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-exception-ajax/' );
    add_variable( 'edit_link', get_state_url( 'schedules&sub=exception&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=exception&prc=add_new' ) );
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Exception List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'exception' );
}

function get_exception_schedules( $esid = array() )
{
	global $db;

	$schedule = json_decode( $esid, true );

	if( is_array( $schedule ) && json_last_error() === JSON_ERROR_NONE )
	{
		$item = '';

		foreach( $schedule as $sid )
		{
		    $s = 'SELECT * FROM ticket_schedule AS a LEFT JOIN ticket_route AS b ON a.rid = b.rid WHERE a.sid = %d';
		    $q = $db->prepare_query( $s, $sid );
		    $r = $db->do_query( $q );
		    $d = $db->fetch_array( $r );

		    $item .= $d['rname'] . '<br />' ;
		}

		return $item;
	}
}

/*
| -------------------------------------------------------------------------------------
| Add New Exception
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_exception()
{
	run_save_exception();

    $site_url = site_url();
    $data     = get_exception();

	set_template( PLUGINS_PATH . '/ticket/tpl/exception/form.html', 'exception' );
    add_block( 'form-block', 'lcblock', 'exception' );

    add_variable( 'eid', $data['eid'] );
    add_variable( 'etitle', $data['etitle'] );
    add_variable( 'emessage', $data['emessage'] );
    add_variable( 'esid_option', get_schedule_option( $data['esid'] ) );
    add_variable( 'apllyin_option', get_applyin_status( $data['eapplication'], false ) );
    add_variable( 'exception_date', get_exception_date_list( $data['exception_date'] ) );
    
    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=exception' ) );
    add_variable( 'action', get_state_url( 'schedules&sub=exception&prc=add_new' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'New Exception' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'exception' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Exception
| -------------------------------------------------------------------------------------
*/
function ticket_edit_exception()
{
    run_update_exception();

    $site_url = site_url();
    $data     = get_exception( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/exception/form.html', 'exception' );
    add_block( 'form-block', 'lcblock', 'exception' );

    add_variable( 'eid', $data['eid'] );
    add_variable( 'etitle', $data['etitle'] );
    add_variable( 'emessage', $data['emessage'] );
    add_variable( 'esid_option', get_schedule_option( $data['esid'] ) );
    add_variable( 'apllyin_option', get_applyin_status( $data['eapplication'], false ) );
    add_variable( 'exception_date', get_exception_date_list( $data['exception_date'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=exception' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-exception-ajax/' );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'exception' ) );
    add_variable( 'action', get_state_url( 'schedules&sub=exception&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Edit Exception' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'exception' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Exception
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_exception()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/exception/batch-delete.html', 'exception' );
    add_block( 'loop-block', 'lclblock', 'exception' );
    add_block( 'delete-block', 'lcblock', 'exception' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_exception( $val );

        add_variable( 'etitle',  $d['etitle'] );
        add_variable( 'eid', $d['eid'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' exception? :' );
    add_variable( 'action', get_state_url( 'schedules&sub=exception' ) );

    parse_template( 'delete-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Delete exception' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'exception' );
}

function run_save_exception()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_exception_data();

        if( empty( $error ) )
        {
            $data = save_exception();

            if( isset( $data['error_message'] ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( $data['error_message'] ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New exception successfully saved' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=exception&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_exception()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_exception_data();

        if( empty( $error ) )
        {
            $data = update_exception();

            if( isset( $data['error_message'] ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( $data['error_message'] ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This exception successfully edited' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=exception&prc=edit&id=' . $data ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_exception_data()
{
    $error = array();

    if( isset( $_POST['etitle'] ) && empty( $_POST['etitle'] ) )
    {
        $error[] = 'Title can\'t be empty';
    }

    if( isset( $_POST['esid'] ) && empty( $_POST['esid'] ) )
    {
        $error[] = 'Schedule by can\'t be empty';
    }

    if( isset( $_POST['exception_date'] ) && !empty( $_POST['exception_date'] ) )
    {
        $date_error = 0;

        foreach( $_POST['exception_date'] as $i => $d )
        {
            if( empty( $d['edatefrom'] ) )
            {
                $date_error++;
            }

            if( empty( $d['edateto'] ) )
            {
                $date_error++;
            }
        }
        
        if( $date_error > 0 )
        {
            $error[] = 'Please check your exception date data';
        }
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Exception List Count
| -------------------------------------------------------------------------------------
*/
function is_num_exception()
{
    global $db;

    $s = 'SELECT * FROM ticket_exception';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Exception Table List Query
| -------------------------------------------------------------------------------------
*/
function ticket_exception_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array( 
        1  => 'a.etitle',
        3  => 'a.emessage'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.eid DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT * FROM ticket_exception AS a ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT * FROM ticket_exception AS a WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'eid'        => $d2['eid'],
                'etitle'     => $d2['etitle'],
                'emessage'   => $d2['emessage'],
                'eschedules' => get_exception_schedules( $d2['esid'] ),
                'edit_link'  => get_state_url( 'schedules&sub=exception&prc=edit&id=' . $d2['eid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Exception By ID
| -------------------------------------------------------------------------------------
*/
function get_exception( $id = '', $field = '' )
{
    global $db;

    $exception_date = array();

    if( isset( $_POST['exception_date'] ) )
    {
        foreach( $_POST['exception_date'] as $i => $date )
        {
            $exception_date[ $i ] = $date;
        }
    }

    $data = array( 
        'eid'            => ( isset( $_POST['eid'] ) ? $_POST['eid'] : null ), 
        'esid'           => ( isset( $_POST['esid'] ) ? $_POST['esid'] : array() ), 
        'etitle'         => ( isset( $_POST['etitle'] ) ? $_POST['etitle'] : '' ), 
        'emessage'       => ( isset( $_POST['emessage'] ) ? $_POST['emessage'] : '' ), 
        'ecreateddate'   => ( isset( $_POST['ecreateddate'] ) ? $_POST['ecreateddate'] : '' ), 
        'eapplication'   => ( isset( $_POST['eapplication'] ) ? $_POST['eapplication'] : array( 0, 1, 2 ) ), 
        'luser_id'       => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' ),
        'exception_date' => $exception_date
    );

    $s = 'SELECT * FROM ticket_exception WHERE eid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
        	if( empty( $d['esid'] ) === false )
        	{
        		$d['esid'] = json_decode( $d['esid'], true );
        	}

            if( empty( $d['eapplication'] ) )
            {
                $d['eapplication'] = json_encode( array( 0, 1, 2 ) );
            }

            $data = array( 
                'eid' => $d['eid'], 
                'esid' => $d['esid'], 
                'etitle' => $d['etitle'], 
                'emessage' => $d['emessage'],
                'ecreateddate' => $d['ecreateddate'],
                'eapplication' => json_decode( $d['eapplication'], true ),
                'luser_id' => $d['luser_id'],
                'exception_date' => get_exception_detail( $d['eid'] )
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

function get_exception_detail( $eid )
{
    global $db;

    $s = 'SELECT * FROM ticket_exception_detail WHERE eid = %d';
    $q = $db->prepare_query( $s, $eid );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data[] = $d;
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Exception
| -------------------------------------------------------------------------------------
*/
function save_exception()
{
    global $db;

    $esid = isset( $_POST['esid'] ) ? json_encode( $_POST['esid'] ) : '';
    $eapp = isset( $_POST['eapplication'] ) ? json_encode( $_POST['eapplication'] ) : '';
    
    $s = 'INSERT INTO ticket_exception(
            etitle,
            emessage,
    		esid,
            eapplication,
            ecreateddate,
            luser_id)
          VALUES( %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s, $_POST['etitle'], $_POST['emessage'], $esid, $eapp, date( 'Y-m-d H:i:s' ), $_COOKIE['user_id'] );
    $r = $db->do_query( $q );
       
    if( isset( $r['error_code'] ) )
    {
        return array( 'error_message' => $r['message'] );
    }
    else
    {
        $id = $db->insert_id();
        
        save_exception_detail( $id );

        save_log( $id, 'exception', 'Add new exception date - ' . $_POST['etitle'] );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Exception Detail
| -------------------------------------------------------------------------------------
*/
function save_exception_detail( $eid )
{
    global $db;
    
    $error = 0;

    foreach( $_POST['exception_date'] as $i => $exc )
    {
        $s = 'INSERT INTO ticket_exception_detail(
                eid,
                etype,
                edatefrom,
                edateto)
              VALUES( %d, %d, %s, %s )';
        $q = $db->prepare_query( $s,
                $eid,
                $exc['etype'],
                date( 'Y-m-d', strtotime( $exc['edatefrom'] ) ),
                date( 'Y-m-d', strtotime( $exc['edateto'] ) ) );
        $r = $db->do_query( $q );

        if( isset( $r['error_code'] ) )
        {
            $error++;
        }
    }

    if( empty( $error ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Exception
| -------------------------------------------------------------------------------------
*/
function update_exception()
{
    global $db;

    $d = get_exception( $_POST['eid'] );

    $esid = isset( $_POST['esid'] ) ? json_encode( $_POST['esid'] ) : '';
    $eapp = isset( $_POST['eapplication'] ) ? json_encode( $_POST['eapplication'] ) : '';

    $s = 'UPDATE ticket_exception SET 
            etitle = %s,
            emessage = %s,
            esid = %s,
            eapplication = %s
          WHERE eid = %d';     
    $q = $db->prepare_query( $s, $_POST['etitle'], $_POST['emessage'], $esid, $eapp, $_POST['eid'] );
    $r = $db->do_query( $q );

    $logs   = array();
    $logs[] = $d['etitle'] !=  $_POST['etitle'] ? 'Change Exception Title : ' . $d['etitle'] . ' to ' . $_POST['etitle'] : '';
    $logs[] = $d['emessage'] != $_POST['emessage'] ? 'Change Exception Message : ' . $d['emessage'] . ' to ' . $_POST['emessage'] : '';
    $logs[] = json_encode( $d['esid'] ) != $esid ? 'Change Exception Schedule : ' . json_encode( $d['esid'] ) . ' to ' . $esid : '';
    $logs[] = json_encode( $d['eapplication'] ) != $eapp ? 'Change Exception Apply In : ' . json_encode( $d['eapplication'] ) . ' to ' . $eapp : '';

    if( isset( $r['error_code'] ) )
    {
        return array( 'error_message' => $r['message'] );
    }   
    else
    {
        $log_detail = update_exception_detail( $_POST['eid'], $d );

        if( empty( $logs ) === false ) 
        {
            foreach( $logs as $k => $log ) 
            {
                if( $log == '' ) 
                {
                    unset( $logs[ $k ] );
                }
            }
        }

        if( empty( $logs ) === false ) 
        {
            save_log( $_POST['eid'], 'exception', 'Update Manage Exception ID#' . $_POST['eid'] . '<br/><b>Detail edit</b> ----><br/>' . implode( '<br/>', $logs ) . '<br/>' . implode( '<br/>', $log_detail ) );
        }
        else
        {
            save_log( $_POST['eid'], 'exception', 'Update Manage Exception ID#' . $_POST['eid'] . '<br/>Exception Period has been deleted' );
        }

        return $_POST['eid'];
    }
}

function update_exception_detail( $eid, $log )
{
    global $db;
    
    $error = 0;
    $log_detail = array();

    foreach( $_POST['exception_date'] as $i => $exc )
    {
        if( empty( $exc['edid'] ) )
        {
            $s = 'INSERT INTO ticket_exception_detail(
                    eid,
                    etype,
                    edatefrom,
                    edateto)
                  VALUES( %d, %d, %s, %s )';
            $q = $db->prepare_query( $s,
                    $eid,
                    $exc['etype'],
                    date( 'Y-m-d', strtotime( $exc['edatefrom'] ) ),
                    date( 'Y-m-d', strtotime( $exc['edateto'] ) ) );
            $r = $db->do_query( $q );

            $log_detail[] = 'Add Exception Period : ' . date( 'd F Y', strtotime( $exc['edatefrom'] ) ) . ' until ' . date( 'd F Y', strtotime( $exc['edateto'] ) );
        }
        else
        {
            $s = 'UPDATE ticket_exception_detail SET
                    etype = %s,
                    edatefrom = %s,
                    edateto = %s
                  WHERE edid = %d';
            $q = $db->prepare_query( $s,
                    $exc['etype'],
                    date( 'Y-m-d', strtotime( $exc['edatefrom'] ) ),
                    date( 'Y-m-d', strtotime( $exc['edateto'] ) ),
                    $exc['edid'] );
            $r = $db->do_query( $q );

            if ( isset( $log['exception_date'][$i] ) ) 
            {
                $from_old = $log['exception_date'][$i]['edatefrom'];
                $to_old   = $log['exception_date'][$i]['edateto'];
                $from     = date( 'Y-m-d H:i:s', strtotime( $exc['edatefrom'] ) );
                $to       = date( 'Y-m-d H:i:s', strtotime( $exc['edateto'] ) );

                if ( $from_old.$to_old != $from.$to ) 
                {
                    $log_detail[] = 'Change Exception Period : ' . date( 'd F Y', strtotime( $from_old ) ) . ' until ' . date( 'd F Y', strtotime( $to_old ) ) . ' ---> ' . date( 'd F Y', strtotime( $from ) ) . ' until ' . date( 'd F Y', strtotime( $to ) );
                }
            }
            
        }

        if( isset( $r['error_code'] ) )
        {
            $error++;
        }
    }

    if( empty( $error ) )
    {
        return $log_detail;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Exception
| -------------------------------------------------------------------------------------
*/
function delete_exception( $id = '', $is_ajax = false )
{
    global $db;

    $d = get_exception( $id );

    $s = 'DELETE FROM ticket_exception WHERE eid = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'schedules&sub=exception&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'exception', 'Delete exception date - ' . $d['etitle'] );

        return true;
    }
}

function delete_exception_detail( $id = '', $is_ajax = false )
{
    global $db;

    $s = 'DELETE FROM ticket_exception_detail WHERE edid = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return true;
    }
}

function get_exception_date_list( $exception_date )
{
    if( !empty( $exception_date ) )
    {
        $content = '';

        foreach( $exception_date as $i => $d )
        {
            if( empty( $d['etype'] ) ) 
            {
                $dt_class = 'sr-only';
            }
            else
            {
                $dt_class = '';
            }

            $content .= '
            <div class="date-list">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="text text-date-from text-medium form-control" name="exception_date[' . $i . '][edatefrom]" value="' . date( 'd F Y', strtotime( $d['edatefrom'] ) ) . '" placeholder="Date">
                        </div>
                        <div class="form-group ' . $dt_class . '">
                            <input type="text" class="text text-date-to text-medium form-control" name="exception_date[' . $i . '][edateto]" value="' . date( 'd F Y', strtotime( $d['edateto'] ) ) . '" placeholder="Date">
                        </div>
                        <div class="form-group sr-only">
                            <input type="text" class="text-medium form-control" name="exception_date[' . $i . '][etype]" value="' . $d['etype'] . '">   
                            <input type="text" class="text-medium form-control" name="exception_date[' . $i . '][edid]" value="' . ( isset( $d['edid'] ) ? $d['edid'] : '' ) . '">                        
                        </div>
                        <div class="form-group">
                            <a class="delete-date" rel="' . ( isset( $d['edid'] ) ? $d['edid'] : '0' ) . '" ><span class="glyphicon glyphicon-trash"></span></a>
                        </div>
                    </div>
                </div>
            </div>';
        }

        return $content;
    }
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_exception_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_exception_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-exception' )
    {
        $d = delete_exception( $_POST['eid'], true );

        if( $d === true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-exception-detail' )
    {
        $d = delete_exception_detail( $_POST['edid'], true );

        if( $d === true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}

function get_applyin_status( $sts = '', $use_empty = true, $empty_text = 'Apply In' )
{
    $status = array(
        'Front End',
        'Agent Panel',
        'Admin'
    );

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( empty( $status ) === false )
    {
        foreach( $status as $st => $stat )
        {
            if( is_array( $sts ) )
            {
                $option .= '<option value="' . $st . '" ' . ( in_array( $st, $sts ) ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
            else
            {
                $option .= '<option value="' . $st . '" ' . ( $st == $sts ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
        }
    }

    return $option;
}

?>