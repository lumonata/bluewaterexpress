<?php

add_actions( 'reschedule', 'ticket_reschedule' );
add_actions( 'ticket-reschedule-ajax_page', 'ticket_reschedule_ajax' );

function ticket_reschedule()
{
    if( is_num_schedule() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'schedules&sub=reschedule&prc=add_new' ) );
    }
	if( is_add_new() )
    {
        return ticket_add_new_reschedule();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) )
        {
            $ticket = get_ticket_multi_change_sample( $_GET['id'], $_GET['sid'] );

            if( empty( $ticket ) )
            {
                return not_match_schedule_template();
            }
            
            return ticket_edit_reschedule( $ticket );
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_reschedule();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_reschedule( $val );
        }
    }
    elseif( is_reschedule() )
    {
        return ticket_reschedule_table();
    }
    return ticket_booking_table_data_for_reschedule();
}

/*
| -------------------------------------------------------------------------------------
| Ticket Table for rescheudle List
| -------------------------------------------------------------------------------------
*/
function ticket_booking_table_data_for_reschedule()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url('reservation&sub=reschedule') . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    $site_url = site_url();
    $filter   = ticket_filter_booking();

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/reschedule/list-ticket.html', 'booking' );

    add_block( 'list-block', 'bblock', 'booking' );

    add_variable( 'bdate', $bdate );
    add_variable( 'search', $search );
    add_variable( 'bbemail', $bbemail );
    add_variable( 'date_end', $date_end );
    add_variable( 'date_start', $date_start );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'action', get_state_url( 'reservation&sub=reschedule' ) );
    add_variable( 'raction', get_state_url( 'reservation&sub=reschedule&prc=reschedule' ) );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-booking-ajax' );


    add_variable( 'rev_status', get_reservation_status( $rstatus, true,'All Reservation Status') );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'location_option', get_location_option( $lcid, true, 'All Departure', 'from') );
    add_variable( 'status_option', get_booking_status_option( $status, true, 'All Status') );
    add_variable( 'location2_option', get_location_option( $lcid2, true, 'All Arrival Point', 'to') );
    add_variable( 'bsource_option', get_booking_source_option( $chid, null, true, 'All Booking Source', true ) );

    parse_template('list-block','bblock', false);

    add_actions( 'section_title', 'Reschedule' );
    add_actions( 'other_elements', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template('booking');
}

/*
| -------------------------------------------------------------------------------------
| Reschedule Table List
| -------------------------------------------------------------------------------------
*/
function ticket_reschedule_table()
{    
    global $db;

    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url('reservation&sub=reschedule&prc=reschedule').'&id=' .$_POST['get_id']. '&prm=' . base64_encode( json_encode( $_POST ) ) );

    }

    $site_url = site_url();
    $filter   = ticket_filter_reschedule();

    $r = get_schedule_filtered( $filter );

    extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/reschedule/list.html', 'rate' );

    add_block( 'list-block', 'rtblock', 'rate' );
    add_variable( 'ajax_url', '' );
    add_variable( 'site_url', $site_url );
    add_variable( 'get_id', $_GET['id'] );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', ticket_reschedule_table_data( $r ) );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'boat_option', get_boat_option( $boid, true, 'Select Boat' ) );
    add_variable( 'route_option', get_route_option( $rid, true, 'Select Route' ) );


    add_variable( 'action', get_state_url( 'reservation&sub=reschedule&prc=reschedule' ) );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=reschedule&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'reservation&sub=reschedule&prc=resechedule' ) );

    add_actions( 'section_title', 'Reschedule' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'list-block', 'rtblock', false );

    return return_template( 'rate' );
}

/*
| -------------------------------------------------------------------------------------
| Rate Period Table List Item
| -------------------------------------------------------------------------------------
*/
function ticket_reschedule_table_data( $r )
{
    global $db;

    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="4">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/reschedule/loop.html', 'rate-loop' );

    add_block( 'loop-block', 'rtloop', 'rate-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        $route_detail = get_route($d['rid']);
        $boat_detail  = get_boat($d['boid']);

        add_variable( 'id', $d['sid'] );
        add_variable( 'status', $d['sstatus'] );
        add_variable( 'route_name', $route_detail['rname'] );
        add_variable( 'boname', $boat_detail['boname'] );
        add_variable( 'period', date('d F Y', strtotime( $d['sfrom'] ) ).' - '. date('d F Y', strtotime( $d['sto'] ) ));
        add_variable( 'sfrom', date('d F Y', strtotime( $d['sfrom'] ) ) );
        add_variable( 'sto', date('d F Y', strtotime( $d['sto'] ) ) );
        add_variable( 'edit_link', get_state_url( 'reservation&sub=reschedule&prc=edit&id=' . $_GET['id'].'&sid='. $d['sid'] ) );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-rate-period-ajax/' );

        parse_template( 'loop-block', 'rtloop', true );
    }

    return return_template( 'rate-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Bticket on Schedule Selected
| -------------------------------------------------------------------------------------
*/
function ticket_schedule_selected_table_data( $r )
{
    global $db;

    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="4">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/reschedule/ticket-loop.html', 'rate-loop' );

    add_block( 'loop-block', 'rtloop', 'rate-loop' );

    while( $d = $db->fetch_array( $r ) )
    {

        add_variable( 'id', $d['bdid'] );
        add_variable( 'bticket', $d['bticket'] );
        add_variable( 'type', $d['bdtype'] );
        add_variable( 'bdfrom', $d['bdfrom'] );
        add_variable( 'bdto', $d['bdto'] );
        add_variable( 'depart_time', $d['bddeparttime']);
        add_variable( 'arrive_time', $d['bdarrivetime']);
        // add_variable( 'edit_link', get_state_url( 'schedules&sub=reschedule&prc=edit&id=' . $d['sid'] ) );
        // add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-rate-period-ajax/' );

        parse_template( 'loop-block', 'rtloop', true );
    }

    return return_template( 'rate-loop' );
}


/*
| -------------------------------------------------------------------------------------
| Add New Rate Period
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_reschedule()
{
	  $message = run_save_reschedule();
    $data    = get_reschedule();
	  set_template( PLUGINS_PATH . '/ticket/tpl/reschedule/form.html', 'rate' );
    add_block( 'form-block', 'rtblock', 'rate' );

    add_variable( 'sid', $data['sid'] );
    add_variable( 'rpto', empty( $data['rpto'] ) ? '' : date( 'd F Y', strtotime( $data['rpto'] ) ) );
    add_variable( 'rpfrom', empty( $data['rpfrom'] ) ? '' : date( 'd F Y', strtotime( $data['rpfrom'] ) ) );
    add_variable( 'route_option', get_route_option( $data['rid'],true,'Select Route'));
    add_variable( 'boat_option', get_boat_option( $data['boid'],true,'Select Route'));

    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'schedules&sub=reschedule&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=reschedule' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'rtblock', false );

    add_actions( 'section_title', 'Reschedule' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Rate Period
| -------------------------------------------------------------------------------------
*/
function ticket_edit_reschedule( $ticket = array() )
{
    if( empty( $ticket ) )
    {
        return not_match_schedule_template();
    }

    run_update_reschedule();

    set_template( PLUGINS_PATH . '/ticket/tpl/reschedule/form.html', 'rate' );

    add_block( 'reschedule-affected-block', 'rablock', 'rate' );
    add_block( 'reschedule-from-block', 'rfblock', 'rate' );
    add_block( 'reschedule-to-block', 'rtblock', 'rate' );
    add_block( 'form-block', 'rfblock', 'rate' );

    $old_arr = array();
    $new_arr = array();

    foreach( $ticket as $data )
    {
        foreach( array( 'old_data', 'new_data', 'old_data' ) as $idx => $type )
        {
            $schedule = get_schedule_list( 'a.sid = ' . $data[ $type ][ 'sid' ]  );
            $index    = sprintf( '%s-%s-%s', $data[ $type ][ 'sid' ], $data[ $type ][ 'depart_time' ], $data[ $type ][ 'arrive_time' ] );

            if( $idx != 2 )
            {
                if( $type == 'old_data' )
                {
                    if( empty( $schedule ) || in_array( $index,  $old_arr ) )
                    {
                        continue;
                    }

                    array_push( $old_arr, $index );
                }
                else
                {
                    if( empty( $schedule ) || in_array( $index,  $new_arr ) )
                    {
                        continue;
                    }

                    array_push( $new_arr, $index );
                }
            }
            
            foreach( $schedule as $sc )
            {
                add_variable( 'to', $data[ 'to' ] );
                add_variable( 'route', $sc['rname'] );
                add_variable( 'from', $data[ 'from' ] );
                add_variable( 'ticket', $data[ 'ticket' ] );
                add_variable( 'type', ucfirst( $data[ 'type' ] ) );
                add_variable( 'boat', get_boat( $data[ $type ][ 'boid' ], 'boname' ) );
                add_variable( 'depart', date( 'H:i', strtotime( $data[ $type ][ 'depart_time' ] ) ) );
                add_variable( 'arrive', date( 'H:i', strtotime( $data[ $type ][ 'arrive_time' ] ) ) );
                add_variable( 'period', sprintf( '%s - %s', date( 'd F Y', strtotime( $sc['sfrom'] ) ), date( 'd F Y', strtotime( $sc['sto'] ) ) ) );
                
                if( $type == 'old_data' )
                {
                    if( $idx == 2 )
                    {
                        parse_template( 'reschedule-affected-block', 'rablock', true );
                    }
                    else
                    {
                        parse_template( 'reschedule-from-block', 'rfblock', true );
                    }
                }
                else
                {
                    parse_template( 'reschedule-to-block', 'rtblock', true );
                }
            }
        }
    }
    
    add_variable( 'bdid', $_GET[ 'id' ] );
    add_variable( 'sid', $_GET[ 'sid' ] );
    add_variable( 'num_rows', count( $ticket ) );
    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=reschedule' ) );

    add_actions( 'section_title', 'Edit Rate Period' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'form-block', 'rfblock', false );

    return return_template( 'rate' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Rate Period
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_reschedule()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/reschedule/batch-delete.html', 'rate' );
    add_block( 'loop-block', 'rtlblock', 'rate' );
    add_block( 'delete-block', 'rtblock', 'rate' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_reschedule( $val );

        add_variable('rpfrom', date( 'd F Y', strtotime( $d['rpfrom'] ) ) );
        add_variable('rpto', date( 'd F Y', strtotime( $d['rpto'] ) ) );
        add_variable('rpid', $d['rpid'] );

        parse_template('loop-block', 'rtlblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' period? :' );
    add_variable('action', get_state_url('schedules&sub=reschedule'));

    parse_template( 'delete-block', 'rtblock', false );

    add_actions( 'section_title', 'Delete Rate Period' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate' );
}

function run_save_reschedule()
{
    if( is_save_draft() || is_publish() )
    {
        $error = validate_reschedule_data();

        if( empty($error) )
        {
            $post_id = save_reschedule();

            header( 'location:'.get_state_url( 'schedules&sub=reschedule&prc=add_new&result=1' ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'New rate period successfully saved' ) );
    }
}

function run_update_reschedule()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        if( update_reschedule() )
        {
            $flash->add( array( 'type'=> 'success', 'content' => array( 'Reservation has been reschedule' ) ) );

            header( 'Location:' . get_state_url( 'reservation&sub=reschedule&prc=edit&id=' . $_POST[ 'bdid' ] . '&sid=' . $_POST[ 'sid' ] ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );

            header( 'Location:' . get_state_url( 'reservation&sub=reschedule&prc=edit&id=' . $_POST[ 'bdid' ] . '&sid=' . $_POST[ 'sid' ] ) );

            exit;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Period By ID
| -------------------------------------------------------------------------------------
*/
function get_reschedule( $id = '', $field = '' )
{
    global $db;

    $data = array(
        'sid'           => ( isset( $_POST['sid'] ) ? $_POST['sid'] : null ),
        'rid'           => ( isset( $_POST['rid'] ) ? $_POST['rid'] : '' ),
        'boid'          => ( isset( $_POST['boid'] ) ? $_POST['boid'] : '' ),
        'sfrom'         => ( isset( $_POST['sfrom'] ) ? $_POST['sfrom'] : '' ),
        'sto'           => ( isset( $_POST['sto'] ) ? $_POST['sto'] : '' ),
        'sstatus'       => ( isset( $_POST['sstatus'] ) ? $_POST['sstatus'] : '' ),
        'screateddate'  => ( isset( $_POST['screateddate'] ) ? $_POST['screateddate'] : '' ),
        'luser_id'      => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' )
    );

    $s = 'SELECT * FROM ticket_schedule WHERE sid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'sid' => $d['sid'],
                'rid' => $d['rid'],
                'boid' => $d['boid'],
                'sfrom' => $d['sfrom'],
                'sto' => $d['sto'],
                'sstatus' => $d['sstatus'],
                'screateddate' => $d['screateddate'],
                'luser_id' => $d['luser_id']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Period List
| -------------------------------------------------------------------------------------
*/
function get_reschedule_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_schedule';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'sid' => $d['sid'],
            'rid' => $d['rid'],
            'boid' => $d['boid'],
            'sfrom' => $d['sfrom'],
            'sto' => $d['sto'],
            'sstatus' => $d['sstatus'],
            'screateddate' => $d['screateddate'],
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Rate Period
| -------------------------------------------------------------------------------------
*/
function save_reschedule()
{
    global $db;

    $status = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );

    $s = 'INSERT INTO ticket_schedule(
            rpfrom,
            rpto,
            rpstatus,
            rpcreateddate,
            luser_id)
          VALUES( %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s,
              date( 'Y-m-d', strtotime( $_POST['rpfrom'] ) ),
              date( 'Y-m-d', strtotime( $_POST['rpto'] ) ),
              $status,
              date( 'Y-m-d H:i:s' ),
              $_COOKIE['user_id'] );

    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();

        $from = date( 'd/m/Y', strtotime( $_POST['rpfrom'] ) );
        $to   = date( 'd/m/Y', strtotime( $_POST['rpto'] ) );

        save_log( $id, 'rate-period', 'Add new rate period from ' . $from . ' until ' . $to );

        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update ticketby reschedule data
| -------------------------------------------------------------------------------------
*/

function save_reschedule_data($current)
{
  // set to ticket reschedule backup
      global $db;
      $today = date("Y-m-d H:i:s");
      $s = 'INSERT INTO ticket_reschedule(
              sid,
              rid,
              boid,
              rsfrom,
              rsto,
              rsstatus,
              rscreatedate,
              luser_id
              )
            VALUES( %d, %d, %d, %s, %s, %s, %s, %d )';

      $q = $db->prepare_query( $s,
                $current['sid'],
                $current['rid'],
                $current['boid'],
                $current['sfrom'],
                $current['sto'],
                $current['sstatus'],
                $today,
                $current['luser_id']);
      if( $db->do_query( $q ) )
      {
        return TRUE;
      }
      else{
        return FALSE;
      }
}

/*
| -------------------------------------------------------------------------------------
| Update schedule by reschedule data
| -------------------------------------------------------------------------------------
*/

function update_reschedule_data($current ,$new_data)
{
  // updatre new schedule
      global $db;
      $route_detail = get_route($new_data['rrid']);
      $sfrom  = $new_data['sfrom'] != "" ? date( 'Y-m-d', strtotime( $new_data['sfrom'] ) ): '' ;
      $sto    = $new_data['sto']   != "" ? date( 'Y-m-d', strtotime( $new_data['sto'] ) )  : '' ;

      $s = 'UPDATE ticket_schedule SET
              rid = %s,
              boid = %s,
              sfrom = %s,
              sto = %s
            WHERE sid = %d';
      $q = $db->prepare_query( $s,
                $new_data['rrid'] ,
                $new_data['rsboid'],
                $sfrom,
                $sto,
                $current['sid']
              );
      if( $db->do_query( $q ) )
      {
        return TRUE;
      }
      else{
        return FALSE;
      }
}

/*
| -------------------------------------------------------------------------------------
| Update ticket by reschedule data
| -------------------------------------------------------------------------------------
*/
function update_reschedule_ticket($old_sid ,$new_sid, $bddate ,$bid){
    global $db;
    $new_schedule = get_schedule($new_sid);
    $route_detail = get_route($new_schedule['rid']);

    $s = 'SELECT
          a.bdid,
          a.bdfrom_id,
          a.bdto_id,
          a.bdfrom,
          a.bdto,
          b.bticket
          FROM ticket_booking_detail as a
          LEFT JOIN ticket_booking as b
          on a.bid = b.bid
          WHERE sid = %s AND a.bddate = %s';
    $q = $db->prepare_query( $s, $old_sid, $bddate);
    $r = $db->do_query( $q );
    $count = 0;
    // cek compatibel route to ticket
    while( $d = $db->fetch_array( $r ) )
    {
        $route_data = get_route_depart_and_arrive_time( $new_schedule['rid'], $d['bdfrom_id'], $d['bdto_id'] );
        if($route_data['error']!= ""){
          $status_checking = $d['bticket'].' '.$d['bdfrom'].' - '.$d['bdto'].' <br> '.$route_data['error'];
          break;
        }
        else{
          $status_checking ='';
        }
    }

    if($status_checking == ""){

      $r = $db->do_query( $q );
      while( $d = $db->fetch_array( $r ) )
      {
          $route_data = get_route_depart_and_arrive_time( $new_schedule['rid'], $d['bdfrom_id'], $d['bdto_id'] );

          $s2 = 'UPDATE ticket_booking_detail SET
                  sid= %s,
                  rid = %s,
                  boid = %s,
                  bddeparttime = %s,
                  bdarrivetime = %s
                WHERE bdid = %d';
          $q2 = $db->prepare_query( $s2,
                    $new_schedule['sid'],
                    $new_schedule['rid'],
                    $new_schedule['boid'],
                    $route_data['depart_time'],
                    $route_data['arrive_time'],
                    $d['bdid']
                  );
          if( $r2 = $db->do_query( $q2 ) )
          {
            $count = $count +1;
          }
          else{
            return FALSE;
          }
      }
    }

    if($status_checking != '') {
      $data['error'] = $status_checking;
      $data['bid'] = $bid;
      return $data;
    }
    else{
      $data['error'] = $status_checking;
      $data['new_sid'] = $new_schedule['sid'];
      $data['bid'] = $bid;
      return $data;
    }
}
/*
| -------------------------------------------------------------------------------------
| Update reschedule
| -------------------------------------------------------------------------------------
*/
function update_reschedule()
{
    global $db;

    $ticket = get_ticket_multi_change_sample( $_POST[ 'bdid' ], $_POST[ 'sid' ] );

    if( empty( $ticket ) )
    {
        return false;
    }
    else
    {
        $retval = 1;

        $db->begin();

        foreach( $ticket as $d )
        {
            //-- UPDATE booking detail
            $r = $db->update( 'ticket_booking_detail', array(
                'bddeparttime' => $d[ 'new_data' ][ 'depart_time' ],
                'bdarrivetime' => $d[ 'new_data' ][ 'arrive_time' ],
                'boid'         => $d[ 'new_data' ][ 'boid' ],
                'sid'          => $d[ 'new_data' ][ 'sid' ],
                'rid'          => $d[ 'new_data' ][ 'rid' ],
            ), array( 'bdid' => $d[ 'id' ] ) );

            if( is_array( $r ) )
            {
                $retval = 0;
            }
            else
            {
                //-- GET transport area
                $s1 = 'SELECT * FROM ticket_booking_transport AS a WHERE a.bdid = %d';
                $q1 = $db->prepare_query( $s1, $d[ 'id' ] );
                $r1 = $db->do_query( $q1 );
                $d1 = $db->fetch_array( $r1 );

                if( $d1[ 'taid' ] != '' )
                {
                    if( $d1[ 'bttype' ] == 'pickup' )
                    {
                        $lcid = $d[ 'from_id' ];
                    }
                    else
                    {
                        $lcid = $d[ 'to_id' ];    
                    }

                    //-- GET new pickup/drop-off time
                    $s2 = 'SELECT
                            b.rpfrom,
                            b.rpto
                          FROM ticket_route_detail AS a
                          LEFT JOIN ticket_route_pickup_drop AS b ON b.rdid = a.rdid
                          WHERE a.rid = %d AND a.lcid = %d AND b.taid = %d';
                    $q2 = $db->prepare_query( $s2, $d[ 'new_data' ][ 'rid' ], $lcid, $d1[ 'taid' ] );
                    $r2 = $db->do_query( $q2 );
                    $d2 = $db->fetch_array( $r2 );

                    //-- Update Booking Transport
                    $r = $db->update( 'ticket_booking_transport', array(
                        'btrpfrom' => $d2[ 'rpfrom' ],
                        'btrpto'   => $d2[ 'rpto' ]
                    ), array( 'btid' => $d1[ 'btid' ] ) );

                    if( is_array( $r ) )
                    {
                        $retval = 0;
                    }
                }
            }
        }

        if( $retval == 0 )
        {
            $db->rollback();

            return false;
        }
        else
        {
            $db->commit();

            return true;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Rate Period
| -------------------------------------------------------------------------------------
*/
function delete_reschedule( $rpid = '', $is_ajax = false )
{
    global $db;

    $d = get_reschedule( $rpid );

    $s = 'DELETE FROM ticket_schedule WHERE rpid = %d';
    $q = $db->prepare_query( $s, $rpid );
    $r = $db->do_query( $q );

    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'schedules&sub=reschedule&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        $from = date( 'd/m/Y', strtotime( $d['rpfrom'] ) );
        $to   = date( 'd/m/Y', strtotime( $d['rpto'] ) );

        save_log( $rpid, 'rate-period', 'Delete rate period from ' . $from . ' until ' . $to );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Period Option
| -------------------------------------------------------------------------------------
*/
function get_reschedule_option( $rpid = '', $empty_value = true )
{
    $rperiod = get_reschedule_list();
    $option  = $empty_value ? '<option value="">Select Rate Period</option>' : '';

    if( !empty( $rperiod ) )
    {
        foreach( $rperiod as $d )
        {
            $option .= '
            <option value="' . $d['rpid'] . '" ' . ( $d['rpid'] == $rpid ? 'selected' : '' ) . ' >
                ' . date( 'd F Y', strtotime( $d['rpfrom'] ) ) . ' - ' . date( 'd F Y', strtotime( $d['rpto'] ) ) .'
            </option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_reschedule_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-rate-period' )
    {
        $d = delete_reschedule( $_POST['rpid'], true );

        if( $d===true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}
function ticket_filter_reschedule()
{
    $filter = array( 'rid' => '', 'sfrom' => '', 'sto' => '', 'bticket' => '' , 'boid' => '');

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array( 'rid' => $rid, 'sfrom' => $sfrom, 'sto' => $sto, 'bticket' => $bticket, 'boid' => $boid );
    }

    return $filter;
}

function get_ticket_by_schedule($sid , $bddate){

  global $db;
  $s = 'SELECT a.bticket ,
               b.bdid,
               b.sid,
               b.bdfrom,
               b.bdto,
               b.bddeparttime,
               b.bdarrivetime,
               b.bdtype FROM ticket_booking AS a
               LEFT JOIN ticket_booking_detail AS b
               ON a.bid = b.bid WHERE b.sid = %s
               AND b.bddate = %s
               ORDER by b.bdid desc
               ';
  $q = $db->prepare_query( $s,$sid, $bddate );
  $r = $db->do_query( $q );
  return $r;
}

function get_bid_by_bdid($bdid){

  global $db;
  $s = 'SELECT bid FROM ticket_booking_detail WHERE bdid = %s';
  $q = $db->prepare_query( $s,$bdid );
  $r = $db->do_query( $q );
  $data = array();

  if( $db->num_rows( $r ) )
  {
      $data = $db->fetch_array( $r );
  }
  return $data;

}

function get_bddate_by_bdid($bdid){

  global $db;
  $s = 'SELECT bddate FROM ticket_booking_detail WHERE bdid = %s';
  $q = $db->prepare_query( $s,$bdid );
  $r = $db->do_query( $q );
  $data = array();

  if( $db->num_rows( $r ) )
  {
      $data = $db->fetch_array( $r );
  }
  return $data;

}

function get_ticket_multi_change_sample( $bdid, $sid )
{    
    global $db;

    if( intval( $bdid ) == 0 )
    {           
        $ids = json_decode( base64_decode( $bdid ), true );
    }
    else
    {
        $ids = array( $bdid );
    }

    $q = 'SELECT
            a.bticket, 
            b.rid,
            b.sid,
            b.bdid,
            b.boid,
            b.bdfrom, 
            b.bdfrom_id, 
            b.bdto,
            b.bdto_id,
            b.bddeparttime,
            b.bdarrivetime,
            b.bdfrom_id, 
            b.bdto_id,
            b.bdtype
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          WHERE b.bdid IN(' . ( implode( ',', $ids ) ) . ')';
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        $dta = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $rda = get_route_depart_and_arrive_time( $d, $sid );

            if( isset( $rda[ 'error' ] ) || empty( $rda ) )
            {
                return array();

                break;
            }
            else
            {
                $dta[] = $rda;
            }
        }

        return $dta;
    }
}

function get_ticket_change_sample( $bdid, $sid )
{
    global $db;

    $s = 'SELECT * FROM ticket_booking_detail AS a WHERE a.bdid = %d';
    $q = $db->prepare_query( $s, $bdid );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) )
    {
        $rid  = get_schedule( $sid, 'rid' );
        $dta  = $db->fetch_array( $r );
        $data = array( 
            'old_data' => $dta,
            'new_data' => get_route_depart_and_arrive_time( $rid, $dta['bdfrom_id'], $dta['bdto_id'] )
        );
    }

    return $data;
}
function get_sid_by_bdid($bdid){

  global $db;
  $s = 'SELECT sid FROM ticket_booking_detail WHERE bdid = %s';
  $q = $db->prepare_query( $s,$bdid );
  $r = $db->do_query( $q );

  $data = array();

  if( $db->num_rows( $r ) )
  {
      $data = $db->fetch_array( $r );
  }
  return $data;
}


function get_schedule_filtered($filter){
  extract($filter);

  $w = 'sstatus != "draft"';
  if($rid != ''){
    $w.=' AND rid = '.$rid;
  }
  if($sfrom != ''){
    $w.=" AND sfrom = '".date( 'Y-m-d', strtotime( $sfrom ) )."'";
  }
  if($sto != ''){
    $w.=" AND sto = '".date( 'Y-m-d', strtotime( $sto ) )."'";
  }
  if($boid != ''){
    $w.=" AND boid = '".$boid."'";
  }
  if($bticket != ''){
    $data = get_sid_by_bticket($bticket);

    if( !empty( $data ) )
    {
        if( is_array( $data ) )
        {
            $edata = end( $data );

            $w .= "and (";

            foreach( $data as $dt )
            {
                if( $dt == $edata )
                {
                    $w .=  ' sid = '.$dt;
                }
                else
                {
                    $w .= 'sid = ' .$dt. ' OR ' ;
                }
            }

            $w .= ' )';
        }
        else
        {
            $w .=' AND rid = '.$data;
        }
    }


  }
  global $db;
  $s = 'SELECT * FROM ticket_schedule WHERE '.$w .' ORDER BY sid DESC';
  $q = $db->prepare_query( $s );
  $r = $db->do_query( $q );
  // print_r($q);
  // exit();
  return $r;
}

// get booking sid by bticket
function get_sid_by_bticket( $bticket )
{
    global $db;

    $s = 'SELECT
            e.sid
          FROM ticket_booking AS a
          LEFT JOIN ticket_agent AS b ON a.agid = b.agid
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          LEFT JOIN ticket_notification AS d ON d.bid = a.bid
          LEFT JOIN ticket_booking_detail AS e ON a.bid = e.bid
          WHERE a.bticket = %d AND a.bstt <> %s';
    $q = $db->prepare_query( $s, $bticket, 'ar' );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) )
    {
        $data = $db->fetch_array( $r );
    }
    return $data;
}

function get_route_depart_and_arrive_time( $detail, $new_sid )
{
	global $db;

    extract( $detail );

    //-- Get Route ID from New Schedule
    $new_scd = get_schedule( $new_sid );

    //-- Get Old Depart/Arrival Time
    $old_dep_time = get_route_time( $detail[ 'rid' ], $detail[ 'bdfrom_id' ], 'depart' );
    $old_arv_time = get_route_time( $detail[ 'rid' ], $detail[ 'bdto_id' ], 'arrival' );

    //-- Get New Depart/Arrival Time
    $new_dep_time = get_route_time( $new_scd[ 'rid' ], $bdfrom_id, 'depart' );
    $new_arv_time = get_route_time( $new_scd[ 'rid' ], $bdto_id, 'arrival' );

    $dta = array();

    if( empty( $new_dep_time ) || empty( $new_arv_time ) )
    {
        $dta[ 'error' ] = 'Location Depart or Arrive are not in route';
    }
    else
    {
        if( strtotime( $new_dep_time ) > strtotime( $new_arv_time ) )
        {
            $dta[ 'error' ] = 'One of your ticket not compatible to your route';
        }
        else
        {
            $dta = array(
                'ticket'  => $detail[ 'bticket' ],
                'type'    => $detail[ 'bdtype' ],
                'from'    => $detail[ 'bdfrom' ],
                'to'      => $detail[ 'bdto' ],
                'from_id' => $detail[ 'bdfrom_id' ],
                'to_id'   => $detail[ 'bdto_id' ],
                'id'      => $detail[ 'bdid' ],
                'old_data' => array(
                    'depart_time' => $old_dep_time,
                    'arrive_time' => $old_arv_time,
                    'boid'        => $detail[ 'boid' ],
                    'rid'         => $detail[ 'rid' ],
                    'sid'         => $detail[ 'sid' ],
                ),
                'new_data' => array(
                    'depart_time' => $new_dep_time,
                    'arrive_time' => $new_arv_time,
                    'boid'        => $new_scd[ 'boid' ],
                    'rid'         => $new_scd[ 'rid' ],
                    'sid'         => $new_sid,
                )
            );
        }
    }

    return $dta;
}

?>
