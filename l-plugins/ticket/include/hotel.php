<?php

add_actions( 'hotel', 'ticket_hotel' );
add_actions( 'ticket-hotel-ajax_page', 'ticket_hotel_ajax' );

function ticket_hotel()
{
    if( is_num_hotel() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'transport&sub=hotel&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_hotel();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_hotel( $_GET['id'] ) )
        {
            return ticket_edit_hotel();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_hotel();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_hotel( $val );
        }
    }

    return ticket_hotel_table();
}

/*
| -------------------------------------------------------------------------------------
| Hotel Table List
| -------------------------------------------------------------------------------------
*/
function ticket_hotel_table()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url( 'transport&sub=hotel' ) . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    $site_url = site_url();
    $filter   = ticket_filter_hotel();

    extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/hotel/list.html', 'hotel' );

    add_block( 'list-block', 'lcblock', 'hotel' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );
    add_variable( 'area_option', get_transport_area_option( $taid, true, 'All Area' ) );

    add_variable( 'action', get_state_url( 'transport&sub=hotel' ) );
    add_variable( 'add_new_link', get_state_url( 'transport&sub=hotel&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'transport&sub=hotel&prc=edit' ) );

    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-hotel-ajax/' );

    parse_template( 'list-block', 'lcblock', false );

    add_actions( 'section_title', 'Hotel & Airport List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'hotel' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Hotel
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_hotel()
{
	run_save_hotel();

    $site_url = site_url();
    $data     = get_hotel();

	set_template( PLUGINS_PATH . '/ticket/tpl/hotel/form.html', 'hotel' );
    add_block( 'form-block', 'lcblock', 'hotel' );

    add_variable( 'hid', $data['hid'] );
    add_variable( 'hfax', $data['hfax'] );
    add_variable( 'hname', $data['hname'] );
    add_variable( 'hemail', $data['hemail'] );
    add_variable( 'hphone', $data['hphone'] );
    add_variable( 'hphone2', $data['hphone2'] );
    add_variable( 'haddress', $data['haddress'] );
    add_variable( 'taid', get_transport_area_option( $data['taid'] ) );
    add_variable( 'hstatus', get_hotel_status_option( $data['hstatus'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=hotel' ) );
    add_variable( 'action', get_state_url( 'transport&sub=hotel&prc=add_new' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'New Hotel & Airport' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'hotel' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Hotel
| -------------------------------------------------------------------------------------
*/
function ticket_edit_hotel()
{
    run_update_hotel();

    $site_url = site_url();
    $data     = get_hotel( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/hotel/form.html', 'hotel' );
    add_block( 'form-block', 'lcblock', 'hotel' );

    add_variable( 'hid', $data['hid'] );
    add_variable( 'hfax', $data['hfax'] );
    add_variable( 'hname', $data['hname'] );
    add_variable( 'hemail', $data['hemail'] );
    add_variable( 'hphone', $data['hphone'] );
    add_variable( 'hphone2', $data['hphone2'] );
    add_variable( 'haddress', $data['haddress'] );
    add_variable( 'taid', get_transport_area_option( $data['taid'] ) );
    add_variable( 'hstatus', get_hotel_status_option( $data['hstatus'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=hotel' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'hotel' ) );
    add_variable( 'action', get_state_url( 'transport&sub=hotel&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'delete_class', '' );

    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-hotel-ajax/' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'Edit Hotel & Airport' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'hotel' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Hotel
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_hotel()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/hotel/batch-delete.html', 'hotel' );
    add_block( 'loop-block', 'lclblock', 'hotel' );
    add_block( 'delete-block', 'lcblock', 'hotel' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_hotel( $val );

        add_variable('hname',  $d['hname'] );
        add_variable('hid', $d['hid'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' hotel? :' );
    add_variable('action', get_state_url('transport&sub=hotel'));

    parse_template( 'delete-block', 'lcblock', false );

    add_actions( 'section_title', 'Delete hotel & airport' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'hotel' );
}

function run_save_hotel()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_hotel_data();

        if( empty($error) )
        {
            $post_id = save_hotel();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new hotel' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New hotel successfully saved' ) ) );

                header( 'location:' . get_state_url( 'transport&sub=hotel&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_hotel()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_hotel_data();

        if( empty($error) )
        {
            $post_id = update_hotel();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this hotel' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This hotel successfully edited' ) ) );

                header( 'location:' . get_state_url( 'transport&sub=hotel&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_hotel_data()
{
    $error = array();

    if( isset($_POST['hname']) && empty( $_POST['hname'] ) )
    {
        $error[] = 'Hotel or airport name can\'t be empty';
    }

    if( isset($_POST['taid']) && empty( $_POST['taid'] ) )
    {
        $error[] = 'Area can\'t be empty';
    }

    if( isset($_POST['haddress']) && empty( $_POST['haddress'] ) )
    {
        $error[] = 'Hotel address can\'t be empty';
    }

    if( isset($_POST['hphone']) && empty( $_POST['hphone'] ) )
    {
        $error[] = 'Phone number can\'t be empty';
    }

    if( isset($_POST['hemail']) && empty( $_POST['hemail'] ) )
    {
        $error[] = 'Email address can\'t be empty';
    }

    return $error;
}

function ticket_filter_hotel()
{
    $filter = array( 'taid' => '' );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array( 'taid' => $taid );
    }

    return $filter;
}

/*
| -------------------------------------------------------------------------------------
| Get Hotel List Count
| -------------------------------------------------------------------------------------
*/
function is_num_hotel()
{
    global $db;

    $s = 'SELECT * FROM ticket_hotel WHERE hid NOT IN( 1, 2 )';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Hotel List Data Query
| -------------------------------------------------------------------------------------
*/
function get_hotel_table_query( $taid = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        0 => 'a.hid',
        1 => 'a.hname',
        2 => 'b.taname',
        3 => 'a.haddress',
        4 => 'a.hphone',
        5 => 'a.hemail',
        6 => 'a.hstatus'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.hid DESC';
    }

    $w = '';

    if( $taid != '' )
    {
        $w .= $db->prepare_query( ' AND a.taid = %d', $taid );
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT * FROM ticket_hotel AS a
              LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
              WHERE a.hid NOT IN( 1, 2 ) ' . $w . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT * FROM ticket_hotel AS a
              LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
              WHERE a.hid ' . $w . ' AND ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'hid'       => $d2['hid'],
                'hname'     => $d2['hname'],
                'taname'    => $d2['taname'],
                'haddress'  => $d2['haddress'],
                'hphone'    => $d2['hphone'],
                'hemail'    => $d2['hemail'],
                'hstatus'   => $d2['hstatus'],
                'edit_link' => get_state_url( 'transport&sub=hotel&prc=edit&id=' . $d2['hid'] ),
                'ajax_link' => HTSERVER . site_url() . '/ticket-hotel-ajax/'
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Hotel By ID
| -------------------------------------------------------------------------------------
*/
function get_hotel( $id = '', $field = '' )
{
    global $db;

    $data = array(
        'hid'          => ( isset( $_POST['hid'] ) ? $_POST['hid'] : null ),
        'taid'         => ( isset( $_POST['taid'] ) ? $_POST['taid'] : '' ),
        'taname'       => ( isset( $_POST['taname'] ) ? $_POST['taname'] : '' ),
        'taairport'    => ( isset( $_POST['taairport'] ) ? $_POST['taairport'] : '' ),
        'hname'        => ( isset( $_POST['hname'] ) ? $_POST['hname'] : '' ),
        'haddress'     => ( isset( $_POST['haddress'] ) ? $_POST['haddress'] : '' ),
        'hphone'       => ( isset( $_POST['hphone'] ) ? $_POST['hphone'] : '' ),
        'hphone2'      => ( isset( $_POST['hphone2'] ) ? $_POST['hphone2'] : '' ),
        'hfax'         => ( isset( $_POST['hfax'] ) ? $_POST['hfax'] : '' ),
        'hemail'       => ( isset( $_POST['hemail'] ) ? $_POST['hemail'] : '' ),
        'hstatus'      => ( isset( $_POST['hstatus'] ) ? $_POST['hstatus'] : '' ),
        'hcreateddate' => ( isset( $_POST['hcreateddate'] ) ? $_POST['hcreateddate'] : '' ),
        'luser_id'     => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' )
    );

    $s = 'SELECT * FROM ticket_hotel AS a LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid WHERE a.hid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'hid' => $d['hid'],
                'taid' => $d['taid'],
                'taname' => $d['taname'],
                'taairport' => $d['taairport'],
                'hname' => $d['hname'],
                'haddress' => $d['haddress'],
                'hphone' => $d['hphone'],
                'hphone2' => $d['hphone2'],
                'hfax' => $d['hfax'],
                'hemail' => $d['hemail'],
                'hstatus' => $d['hstatus'],
                'hcreateddate' => $d['hcreateddate'],
                'luser_id' => $d['luser_id']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Hotel List
| -------------------------------------------------------------------------------------
*/
function get_hotel_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_hotel WHERE hid NOT IN( 1, 2 )';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'hid' => $d['hid'],
            'taid' => $d['taid'],
            'hname' => $d['hname'],
            'haddress' => $d['haddress'],
            'hphone' => $d['hphone'],
            'hphone2' => $d['hphone2'],
            'hfax' => $d['hfax'],
            'hemail' => $d['hemail'],
            'hstatus' => $d['hstatus'],
            'hcreateddate' => $d['hcreateddate'],
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Hotel
| -------------------------------------------------------------------------------------
*/
function save_hotel()
{
    global $db;

    $s = 'INSERT INTO ticket_hotel(
            taid,
            hname,
            haddress,
            hphone,
            hphone2,
            hfax,
            hemail,
            hstatus,
            hcreateddate,
            luser_id ) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s,
            $_POST['taid'],
            $_POST['hname'],
            $_POST['haddress'],
            $_POST['hphone'],
            $_POST['hphone2'],
            $_POST['hfax'],
            $_POST['hemail'],
            $_POST['hstatus'],
            date( 'Y-m-d H:i:s' ),
            $_COOKIE['user_id'] );

    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();

        save_log( $id, 'hotel', 'Add new hotel/aiport - ' . $_POST['hname'] );

        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Hotel
| -------------------------------------------------------------------------------------
*/
function update_hotel()
{
    global $db;

    $d = get_hotel( $_POST['hid'] );

    $s = 'UPDATE ticket_hotel SET
            taid = %d,
            hname = %s,
            haddress = %s,
            hphone = %s,
            hphone2 = %s,
            hfax = %s,
            hemail = %s,
            hstatus = %s
          WHERE hid = %d';
    $q = $db->prepare_query( $s,
            $_POST['taid'],
            $_POST['hname'],
            $_POST['haddress'],
            $_POST['hphone'],
            $_POST['hphone2'],
            $_POST['hfax'],
            $_POST['hemail'],
            $_POST['hstatus'],
            $_POST['hid'] );

    if( $db->do_query( $q ) )
    {
        $s      = array( '', 'Open', 'Closed' );
        $logs   = array();
        $logs[] = $d['taid'] != $_POST['taid'] ? 'Change Hotel Area : ' . get_data_by_id( 'taname', 'ticket_transport_area', 'taid', $d['taid'] ) . ' ---> ' . get_data_by_id( 'taname', 'ticket_transport_area', 'taid', $_POST['taid'] ) : '';
        $logs[] = $d['hname'] != $_POST['hname'] ? 'Change Hotel Name : ' . $d['hname'] . ' ---> ' . $_POST['hname'] : '';
        $logs[] = $d['haddress'] != $_POST['haddress'] ? 'Change Hotel Address : ' . $d['haddress'] . ' ---> ' . $_POST['haddress'] : '';
        $logs[] = $d['hphone'] != $_POST['hphone'] ? 'Change Phone 1 : ' . $d['hphone'] . ' ---> ' . $_POST['hphone'] : '';
        $logs[] = $d['hphone2'] != $_POST['hphone2'] ? 'Change Phone 2 : ' . $d['hphone2'] . ' ---> ' . $_POST['hphone2'] : '';
        $logs[] = $d['hemail'] != $_POST['hemail'] ? 'Change Email : ' . $d['hemail'] . ' ---> ' . $_POST['hemail'] : '';
        $logs[] = $d['hstatus'] != $_POST['hstatus'] ? 'Change Hotel Status : ' . $s[ $d['hstatus'] ] . ' ---> ' . $s[ $_POST['hstatus'] ] : '';

        if ( !empty( $logs ) )
        {
            foreach ( $logs as $i => $log )
            {
                if ( $log == '' )
                {
                    unset( $logs[$i] );
                }
            }

            save_log( $_POST['taid'], 'hotel', 'Update Hotel & Airport - ' . $d['hname'] . '<br/>' . implode( '<br/>',  $logs ) );
        }

        return $_POST['hid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Hotel
| -------------------------------------------------------------------------------------
*/
function delete_hotel( $id='', $is_ajax = false )
{
    global $db;

    $d = get_hotel( $id );

    $s = 'DELETE FROM ticket_hotel WHERE hid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'transport&sub=hotel&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'hotel', 'Delete hotel/aiport - ' . $d['hname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Hotel Status Option
| -------------------------------------------------------------------------------------
*/
function get_hotel_status_option( $status='' )
{
    $option = '
    <option value="1" ' . ( $status=='1' ? 'selected' : '' ) . '>Open</option>
    <option value="2" ' . ( $status=='2' ? 'selected' : '' ) . '>Closed</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Hotel Option
| -------------------------------------------------------------------------------------
*/
function get_hotel_option( $hid = '' )
{
    $hotel  = get_hotel_list();
    $option = '<option value="">Select Hotel</option>';

    if( !empty( $hotel ) )
    {
        foreach( $hotel as $d )
        {
            $option .= '<option value="' . $d['hid'] . '" ' . ( $d['hid'] == $hid ? 'selected' : '' ) . ' >' . $d['hname'] . '</option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_hotel_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-hotel' )
    {
        $d = delete_hotel( $_POST['hid'], true );

        if( $d === true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        extract( $_POST );

        $data = get_hotel_table_query( $taid );

        echo json_encode( $data );
    }
}

?>