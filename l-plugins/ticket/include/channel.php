<?php

add_actions( 'channel', 'ticket_channel' );
add_actions( 'ticket-channel-ajax_page', 'ticket_channel_ajax' );

function ticket_channel()
{
    $is_access = is_access( $_COOKIE['user_id'], $_GET['state'], $_GET['sub'] );

    if( is_num_channel() == 0 && !isset( $_GET['prc'] ) && $is_access )
    {
        header( 'location:' . get_state_url( 'booking-source&sub=channel&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        if ( $is_access ) 
        {
            return ticket_add_new_channel();
        } 
        else 
        {
            return not_found_template();
        }
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_channel( $_GET['id'] ) && $is_access )
        {
            return ticket_edit_channel();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        if ( $is_access ) 
        {
            return ticket_batch_delete_channel();
        } 
        else 
        {
            return not_found_template();
        }
    }
    elseif( is_confirm_delete() && $is_access)
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_channel( $val );
        }
    }

    return ticket_channel_table();
}

/*
| -------------------------------------------------------------------------------------
| Channel Table List
| -------------------------------------------------------------------------------------
*/
function ticket_channel_table()
{
	global $db;

    $s = 'SELECT * FROM ticket_channel ORDER BY chid DESC';
    $q = $db->prepare_query( $s );
	$r = $db->do_query( $q );

    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/channel/list.html', 'channel' );

    add_block( 'list-block', 'lcblock', 'channel' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'booking-source&sub=channel' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-channel-ajax/' );
    add_variable( 'add_new_link', get_state_url( 'booking-source&sub=channel&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'booking-source&sub=channel&prc=edit' ) );
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Sales Channel List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'channel' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Channel
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_channel()
{
	run_save_channel();

    $site_url = site_url();
    $data     = get_channel();

	set_template( PLUGINS_PATH . '/ticket/tpl/channel/form.html', 'channel' );
    add_block( 'form-block', 'lcblock', 'channel' );

    add_variable( 'chid', $data['chid'] );
    add_variable( 'chname', $data['chname'] );
    add_variable( 'chcode', $data['chcode'] );
    add_variable( 'chpercentage', $data['chpercentage'] );
    add_variable( 'chdescription', $data['chdescription'] );
    add_variable( 'chpercentage_style', 'visibility:hidden; height: 0px; padding: 0px;');

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'booking-source&sub=channel&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( 'booking-source&sub=channel' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'New Sales Channel' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'channel' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Channel
| -------------------------------------------------------------------------------------
*/
function ticket_edit_channel()
{
    run_update_channel();

    $site_url = site_url();
    $data     = get_channel( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/channel/form.html', 'channel' );
    add_block( 'form-block', 'lcblock', 'channel' );

    add_variable( 'chid', $data['chid'] );
    add_variable( 'chname', $data['chname'] );
    add_variable( 'chcode', $data['chcode'] );
    add_variable( 'chpercentage', $data['chpercentage'] );
    add_variable( 'chdescription', $data['chdescription'] );
    add_variable( 'chpercentage_style', check_chanel_downline($_GET['id']) ? 'visibility:hidden; height: 0px; padding: 0px;' : 'visibility:visible');
    
    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'booking-source&sub=channel&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-channel-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'booking-source&sub=channel' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'channel' ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Edit Sales Channel' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'channel' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Channel
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_channel()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/channel/batch-delete.html', 'channel' );
    add_block( 'loop-block', 'lclblock', 'channel' );
    add_block( 'delete-block', 'lcblock', 'channel' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_channel( $val );

        add_variable('chname',  $d['chname'] );
        add_variable('chid', $d['chid'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' channel? :' );
    add_variable('action', get_state_url('booking-source&sub=channel'));

    parse_template( 'delete-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Delete channel' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'channel' );
}

function run_save_channel()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_channel_data();

        if( empty( $error ) )
        {
            $post_id = save_channel();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new sales channel' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New sales channel successfully saved' ) ) );

                header( 'location:' . get_state_url( 'booking-source&sub=channel&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_channel()
{
    global $flash;
    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_channel_data();

        if( empty( $error ) )
        {
            $post_id = update_channel();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this sales channel' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This sales channel successfully edited' ) ) );

                header( 'location:' . get_state_url( 'booking-source&sub=channel&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_channel_data()
{
    $error = array();

    if( isset($_POST['chname']) && empty( $_POST['chname'] ) )
    {
        $error[] = 'channel name can\'t be empty';
    }

    if( isset($_POST['chcode']) && empty( $_POST['chcode'] ) )
    {
        $error[] = 'channel code can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Channel List Count
| -------------------------------------------------------------------------------------
*/
function is_num_channel()
{
    global $db;

    $s = 'SELECT * FROM ticket_channel';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Channel Table Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_channel_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array( 
        1  => 'a.chname',
        2  => 'a.chcode'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.chid DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT * FROM ticket_channel AS a ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT * FROM ticket_channel AS a WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'chid'      => $d2['chid'],
                'chname'    => $d2['chname'],
                'chcode'    => $d2['chcode'],
                'edit_link' => get_state_url( 'booking-source&sub=channel&prc=edit&id=' . $d2['chid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Channel By ID
| -------------------------------------------------------------------------------------
*/
function get_channel( $id='', $field = '' )
{
    global $db;

    $data = array( 
        'chid'          => ( isset( $_POST['chid'] ) ? $_POST['chid'] : null ),
        'chcode'        => ( isset( $_POST['chcode'] ) ? $_POST['chcode'] : '' ),
        'chname'        => ( isset( $_POST['chname'] ) ? $_POST['chname'] : '' ),
        'chstatus'      => ( isset( $_POST['chstatus'] ) ? $_POST['chstatus'] : '' ),
        'chcreateddate' => ( isset( $_POST['chcreateddate'] ) ? $_POST['chcreateddate'] : '' ),
        'chdescription' => ( isset( $_POST['chdescription'] ) ? $_POST['chdescription'] : '' ),
        'luser_id'      => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' ),
        'chpercentage'  => ( isset( $_POST['chpercentage'] ) ? $_POST['chpercentage'] : '' )
    );

    $s = 'SELECT * FROM ticket_channel WHERE chid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'chid' => $d['chid'], 
                'chcode' => $d['chcode'],
                'chname' => $d['chname'],
                'chstatus' => $d['chstatus'],
                'chcreateddate' => $d['chcreateddate'],
                'chdescription' => $d['chdescription'],
                'luser_id' => $d['luser_id'],
                'chpercentage' => $d['chpercentage']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Channel List
| -------------------------------------------------------------------------------------
*/
function get_channel_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_channel';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array( 
            'chid' => $d['chid'], 
            'chname' => $d['chname'], 
            'chcode' => $d['chcode'], 
            'chdescription' => $d['chdescription'], 
            'chcreateddate' => $d['chcreateddate'], 
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Channel
| -------------------------------------------------------------------------------------
*/
function save_channel()
{
    global $db;

    $status = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );
    
    $s = 'INSERT INTO ticket_channel(
            chname,
            chcode,
            chstatus,
            chdescription,
            chcreateddate,
            luser_id)
          VALUES( %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s, 
            $_POST['chname'], 
            strtoupper( $_POST['chcode'] ), 
            $status, 
            $_POST['chdescription'], 
            date( 'Y-m-d H:i:s' ), 
            $_COOKIE['user_id'] );
       
    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();
        
        save_log( $id, 'channel', 'Add new channel - ' . $_POST['chname'] );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Channel
| -------------------------------------------------------------------------------------
*/
function update_channel()
{
    global $db;

    $current = get_channel( $_POST['chid'] );
    $status  = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );

    $s = 'UPDATE ticket_channel SET 
            chname = %s, 
            chcode = %s,
            chstatus = %s,
            chdescription = %s,
            chpercentage = %s
          WHERE chid = %d';
    $q = $db->prepare_query( $s,
            $_POST['chname'],
            strtoupper( $_POST['chcode'] ),
            $status,
            $_POST['chdescription'],
            $_POST['chpercentage'],
            $_POST['chid'] );
       
    if( $db->do_query( $q ) )
    {
        $logs = array();
        $logs[] = $current['chname'] != $_POST['chname'] ? 'Change Sales Channel Name : ' . $current['chname'] . ' ---> ' . $_POST['chname'] : '';
        $logs[] = $current['chcode'] != $_POST['chcode'] ? 'Change Channel’s Code : ' . $current['chcode'] . ' ---> ' . $_POST['chcode'] : '';
        $logs[] = $current['chdescription'] != $_POST['chdescription'] ? 'Change Description : ' . $current['chdescription'] . ' ---> ' . $_POST['chdescription'] : '';
        $logs[] = $current['chpercentage'] != $_POST['chpercentage'] ? 'Change Channel Percentage : ' . $current['chpercentage'] . ' ---> ' . $_POST['chpercentage'] : '';

        if ( !empty( $logs ) ) 
        {
            foreach ( $logs as $i => $log ) 
            {
                if ( $log == '' ) 
                {
                    unset( $logs[ $i ] );
                }
            }

            save_log( $_POST['chid'], 'channel', 'Update Channel - ' . $current['chname'] . '<br/>' . implode( '<br/>', $logs ) );
        }

        return $_POST['chid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Channel
| -------------------------------------------------------------------------------------
*/
function delete_channel( $id='', $is_ajax = false )
{
    global $db;

    $d = get_channel( $id );

    $s = 'DELETE FROM ticket_channel WHERE chid = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'booking-source&sub=channel&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'channel' , 'Delete channel - ' . $d['chname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Check Channel downline
| -------------------------------------------------------------------------------------
*/
function check_chanel_downline( $id='')
{
    global $db;

    $d = get_channel( $id );

    $s = 'SELECT * FROM ticket_agent WHERE chid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    $count = $db->num_rows( $r );

    if ($count > 0){
      return 1;
    }

    else{
      return 0;
    }

}

/*
| -------------------------------------------------------------------------------------
| Get Channel Option
| -------------------------------------------------------------------------------------
*/
function get_channel_option( $chid = '', $use_empty = true, $empty_text = 'Select Channel' )
{
    $channel = get_channel_list();
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    
    if( !empty( $channel ) )
    {
        foreach( $channel as $d )
        {
            $option .= '<option value="' . $d['chid'] . '" ' . ( $d['chid'] == $chid ? 'selected' : '' ) . ' >' . $d['chname'] . '</option>';   
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_channel_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_channel_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-channel' )
    {
        if( is_access( $_COOKIE['user_id'], $_POST['state'], $_POST['sub'] ) )
        {
            $d = delete_channel( $_POST['chid'], true );

            if( $d===true )
            {
                echo '{"result":"success"}';
            }
            else
            {
                echo json_encode( $d );
            }
        }
        else
        {
            echo '{"result":"failed"}';
        }
    }
}

?>