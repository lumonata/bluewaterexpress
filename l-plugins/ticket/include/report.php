<?php

/*
| -------------------------------------------------------------------------------------
| Page Data Actions
| -------------------------------------------------------------------------------------
*/

add_actions( 'report', 'ticket_passenger_list_report' );
add_actions( 'passenger-list-report', 'ticket_passenger_list_report' );
add_actions( 'transport-list-report', 'ticket_transport_list_report' );
add_actions( 'reservation-report', 'ticket_reservation_report' );
add_actions( 'reservation-addons-report', 'ticket_reservation_addons_report' );
add_actions( 'reservation-timeline-report', 'ticket_reservation_timeline_report' );
add_actions( 'daily-pax-numbers-report', 'ticket_daily_pax_numbers_report' );
add_actions( 'seats-production-report', 'ticket_seats_production_report' );
add_actions( 'revenue-report', 'ticket_revenue_report' );
add_actions( 'transport-revenue-report', 'ticket_transport_revenue_report' );
add_actions( 'average-rates-report', 'ticket_average_rates_report' );
add_actions( 'hs-sales-sum-report', 'ticket_hs_sales_sum_report' );
add_actions( 'market-segment-report', 'ticket_market_segment_report' );
add_actions( 'promo-code-report', 'ticket_promo_code_report' );
add_actions( 'feedback-report', 'ticket_feedback_report' );

add_actions( 'ajax_process_request_page', 'ajax_process_request' );

/*
| -------------------------------------------------------------------------------------
| Report - ajax process request
| -------------------------------------------------------------------------------------
*/
function ajax_process_request()
{
	add_actions( 'is_use_ajax', true );

	if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['plog'] ) && $_POST['plog'] == 'passanger-list' )
    {
    	extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $rid     = isset( $rid ) ? $rid : '';
        $filter  = array( 'rid' => $rid, 'boid' => $boid, 'chids' => $chids, 'taid' => $taid, 'bddate' => $bddate, 'bddateto' => $bddateto, 'bstatus' => $bstatus, 'rtype' => $rtype );
		$data    = get_passenger_list_report( $filter );

		echo json_encode( $data );
    }
    else if( isset( $_POST['plog'] ) && $_POST['plog'] == 'transport-list' )
    {
    	extract( $_POST );

    	$bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'taid' => $taid, 'rid' => $rid, 'ttype' => $ttype, 'rtype' => $rtype, 'ttdate' => $ttdate, 'ttdateto' => $ttdateto, 'bstatus' => $bstatus );
        $data 	 = get_transport_list_report( $filter );

        echo json_encode( $data );
    }
    else if( isset( $_POST['plog'] ) && $_POST['plog'] == 'reservation-list' )
    {
    	extract( $_POST );

    	$bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'rview' => $rview, 'rid' => $rid, 'chid' => $chid, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'bstatus' => $bstatus );
        $data    = get_reservation_report( $filter );

        echo json_encode( $data );
    }
    else if( isset( $_POST['plog'] ) && $_POST['plog'] == 'reservation-timeline' )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'rid' => $rid, 'chid' => $chid, 'bstatus' => $bstatus, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'rbased' => $rbased, 'cid' => $cid );
        $data    = get_reservation_timeline_report( $filter );

        echo json_encode( $data );
    }
    else if( isset( $_POST['plog'] ) && $_POST['plog'] == 'summary-addons-list' )
    {
        extract( $_POST );

        $filter  = array( 'lcid' => $lcid, 'lcid2' => $lcid2, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto );
        $data    = get_summary_addons_report( $filter );

        echo json_encode( $data );
    }
    else if( isset( $_POST['plog'] ) && $_POST['plog'] == 'daily-addons-list' )
    {
        extract( $_POST );

        $filter  = array( 'lcid' => $lcid, 'lcid2' => $lcid2, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto );
        $data    = get_daily_addons_report( $filter );

        echo json_encode( $data );
    }
    else if( isset( $_POST['plog'] ) && $_POST['plog'] == 'daily-pax-numbers-list' )
    {
    	extract( $_POST );

        $rid     = isset( $rid ) ? $rid : '';
        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'rview' => $rview, 'rid' => $rid, 'chid' => $chid, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'bstatus' => $bstatus );
        $data    = get_daily_pax_numbers_report( $filter );

        echo json_encode( $data );
    }
    else if( isset( $_POST['plog'] ) && $_POST['plog'] == 'revenue-list' )
    {
    	extract( $_POST );

        $rsource = isset( $rsource ) ? $rsource : '';
        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $deport  = isset( $deport ) ? $deport : '';
        $chid    = isset( $chid ) ? $chid : '';

        $filter  = array( 'rtype' => $rtype, 'rview' => $rview, 'bdate' => $bdate, 'bdateto' => $bdateto, 'bsource' => $rsource, 'chid' => $chid, 'agsp' => $agsp, 'bstatus' => $bstatus, 'rbased' => $rbased, 'boid' => $boid, 'deport' => $deport, 'feeopt' => $feeopt, 'addonsopt' => $addonsopt );
    	$data 	 = get_ticket_revenue_report( $filter );

    	echo json_encode( $data );
    }
    else if( isset( $_POST['plog'] ) && $_POST['plog'] == 'seats-production-list' )
    {
    	extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $ryear   = isset( $ryear ) ? $ryear : array();
        $rid     = isset( $rid ) ? $rid : array();
        $tid     = isset( $tid ) ? $tid : array();
        $rstatus = isset( $rstatus ) ? $rstatus : '';
		$bmethod = isset( $bmethod ) ? $bmethod : '';
		$pmethod = isset( $pmethod ) ? $pmethod : '';

        $filter  = array( 'ryear' => $ryear, 'rview' => $rview, 'chid' => $chid, 'agsp' => $agsp, 'bstatus' => $bstatus, 'rstatus' => $rstatus, 'rid' => $rid, 'tid' => $tid, 'bmethod' => $bmethod, 'pmethod' => $pmethod);
        $data    = get_seats_production_report( $filter );

        echo json_encode( $data );
    }
    else if( isset( $_POST['plog'] ) && $_POST['plog'] == 'hs-sales-sum-report' )
    {
        extract( $_POST );

        $rsource = isset( $rsource ) ? $rsource : '';
        $bstatus = isset( $bstatus ) ? $bstatus : '';

        $data = get_ticket_hs_sales_sum_report( array(
            'lcid'    => $lcid,
            'rtype'   => $rtype,
            'bdate'   => $bdate,
            'bstatus' => $bstatus,
            'rsource' => $rsource,
            'bdateto' => $bdateto
        ));

        echo json_encode( $data );
    }

    die();
}

/*
| -------------------------------------------------------------------------------------
| Report - function action url
| -------------------------------------------------------------------------------------
*/
function ticket_passenger_list_report()
{
	$filter = get_filter_passenger_list_report();

    extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/passenger-list.html', 'report' );

    add_block( 'report-block', 'rp-block', 'report' );

    add_variable( 'bddate', $bddate );
    add_variable( 'bddateto', $bddateto );
    add_variable( 'report_type_option', get_report_type_option( $rtype ) );
    add_variable( 'boat_option', get_boat_option( $boid, true, 'All Boat') );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'area_option', get_transport_area_option( $taid, true, 'All Transport Area') );
    add_variable( 'booking_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );
    add_variable( 'booking_source_option', get_booking_source_option( $chids, null, true, 'All Booking Source', true ) );

    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'site_url', HTSERVER . site_url() );
    add_variable( 'ajax_response', HTSERVER . site_url() . '/ajax_process_request' );

    add_actions( 'section_title', 'Report - Passenger List' );
    add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/chart.js?v=' . TICKET_VERSION );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'report-block', 'rp-block', false );

    return return_template( 'report' );
}

function ticket_transport_list_report()
{
	$filter = get_filter_transport_list_report();
	extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/transport-list.html', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );

    add_variable( 'ttdate', $ttdate );
    add_variable( 'ttdateto', $ttdateto );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Trip') );
    add_variable( 'trans_type_option', get_transport_type_option( $ttype ) );
    add_variable( 'report_type_option', get_report_type_option( $rtype ) );
    add_variable( 'area_option', get_transport_area_option( $taid, true, 'All Transport Area') );
    add_variable( 'booking_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );
    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'site_url', HTSERVER . site_url() );
    add_variable( 'ajax_response', HTSERVER . site_url() . '/ajax_process_request' );

	parse_template( 'report-block', 'rp-block', false );

    add_actions( 'section_title', 'Report - Transport List' );
	add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/chart.js?v=' . TICKET_VERSION );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

function ticket_reservation_timeline_report()
{
    $filter = get_filter_reservation_timeline_report();

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/report/reservation-timeline-report.html', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );

    add_variable( 'bdate', $bdate );
    add_variable( 'bdateto', $bdateto );
    add_variable( 'site_url', HTSERVER . site_url() );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'report_based_on_option', get_based_on_option( $rbased, 'revenue' ) );
    add_variable( 'report_type_option', get_report_type_option( $rtype, 'reservation' ) );
    add_variable( 'booking_source_option', get_booking_source_option( $chid, null, true, 'All Booking Source',true) );
    add_variable( 'booking_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );
    add_variable( 'sales_channel_option', get_sales_channel_option( $cid, true, 'All Sales Channel' ) );
    add_variable( 'ajax_response', HTSERVER . site_url() . '/ajax_process_request' );
    add_variable( 'reservation_report_link', get_state_url( 'reservation&sub=reservation-report' ) );
    add_variable( 'reservation_timeline_report', get_state_url( 'reservation&sub=reservation-timeline-report' ) );

    parse_template( 'report-block', 'rp-block', false );

    add_actions( 'section_title', 'Report - Reservation' );
    add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
    add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
    add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
    add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/chart.js?v=' . TICKET_VERSION );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

function ticket_reservation_addons_report()
{
    $filter = get_filter_reservation_addons_report();

    extract( $filter );
    extract( $_GET );

    set_template( PLUGINS_PATH . '/ticket/tpl/report/addons-report.html', 'report' );

    add_block( 'summary-report-block', 'srp-block', 'report' );
    add_block( 'daily-report-block', 'drp-block', 'report' );

    add_variable( 'bdate', $bdate );
    add_variable( 'bdateto', $bdateto );
    add_variable( 'departure_option', get_location_option( $lcid, true, 'All Departure') );
    add_variable( 'arrival_option', get_location_option( $lcid2, true, 'All Arrival Point') );
    add_variable( 'daily_report_type_option', get_report_type_option( $rtype, 'daily-addons' ) );
    add_variable( 'summary_report_type_option', get_report_type_option( $rtype, 'summary-addons' ) );

    add_variable( 'site_url', HTSERVER . site_url() );
    add_variable( 'ajax_response', HTSERVER . site_url() . '/ajax_process_request' );
    add_variable( 'addons_daily_pre_book_report', get_state_url( 'report&sub=reservation-addons-report&type=daily' ) );
    add_variable( 'addons_summary_report_link', get_state_url( 'report&sub=reservation-addons-report&type=summary' ) );

    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );

    add_actions( 'section_title', 'Report - Add-ons' );
    add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
    add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
    add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
    add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/chart.js?v=' . TICKET_VERSION );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    if( isset( $type ) && $type == 'summary' )
    {
        parse_template( 'summary-report-block', 'srp-block', false );
    }
    else
    {
        parse_template( 'daily-report-block', 'drp-block', false );
    }

    return return_template( 'report' );
}

function ticket_reservation_report()
{
    $filter = get_filter_reservation_report();

	extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/reservation-report.html', 'report' );
	add_block( 'report-block', 'rp-block', 'report' );

	add_variable( 'bdate', $bdate );
	add_variable( 'bdateto', $bdateto );
	add_variable( 'report_view_option', get_report_view_option( $rview ) );
	add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
	add_variable( 'report_type_option', get_report_type_option( $rtype, 'reservation' ) );
	add_variable( 'booking_source_option', get_booking_source_option( $chid, null, true, 'All Booking Source',true) );
	add_variable( 'booking_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );
    add_variable( 'reservation_report_link', get_state_url( 'reservation&sub=reservation-report' ) );
    add_variable( 'reservation_timeline_report', get_state_url( 'reservation&sub=reservation-timeline-report' ) );

	add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
	add_variable( 'site_url', HTSERVER . site_url() );
	add_variable( 'ajax_response', HTSERVER . site_url() . '/ajax_process_request' );

	parse_template( 'report-block', 'rp-block', false );

	add_actions( 'section_title', 'Report - Reservation' );
	add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/chart.js?v=' . TICKET_VERSION );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

	return return_template( 'report' );
}

function ticket_daily_pax_numbers_report()
{
	$filter = get_filter_daily_pax_numbers_report();
	extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/daily-pax-numbers-report.html', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );

    add_variable( 'bdate', $bdate );
    add_variable( 'bdateto', $bdateto );
    add_variable( 'report_view_option', get_report_view_option( $rview ) );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'report_type_option', get_report_type_option( $rtype, 'daily-pax-numbers' ) );
    add_variable( 'booking_source_option', get_booking_source_option( $chid, null, true, 'All Booking Source',true) );
    add_variable( 'booking_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );

    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'site_url', HTSERVER . site_url() );
    add_variable( 'ajax_response', HTSERVER . site_url() . '/ajax_process_request' );

    parse_template( 'report-block', 'rp-block', false );

    add_actions( 'section_title', 'Report - Daily Pax Numbers' );
	add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/chart.js?v=' . TICKET_VERSION );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

function ticket_transport_revenue_report()
{
    global $db;

	$filter = get_filter_transport_revenue_report();
	$data   = get_transport_revenue_report( $filter );

	extract( $data );
	extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/transport-revenue.html', 'report' );
	add_block( 'report-data-block', 'rd-block', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );
    add_block( 'no-report-block', 'nrp-block', 'report' );

    if( empty( $data['items'] ) )
    {
	    add_variable( 'total', $total );
	    add_variable( 'ttdate', $ttdate );
	    add_variable( 'trans_type_option', get_pickup_drop_option( $type ) );
	    add_variable( 'report_type_option', get_report_type_option( $rtype ) );
	    add_variable( 'report_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );

	    parse_template( 'no-report-block', 'nrp-block', false );
    }
    else
    {
	    $i = 0;

	    $category = array();
	    $series   = array();

	    foreach( $items as $obj => $groups )
	    {
	    	list( $location, $total_by_location ) = json_decode( base64_decode( $obj ) );

			$tblcontent = '
			<div class="item">
				<div class="rs-table">
					<table id="reservation-table" class="table table-striped" cellspacing="0" width="100%">
			            <thead>
			                <tr>
			                    <th width="20%">Vehicle</th>
			                    <th width="20%">Driver Name</th>
			                    <th width="10%">Type</th>
			                    <th width="10%">Date</th>
			                    <th width="10%">Time</th>
			                    <th width="10%" class="text-right">Fee</th>
			                </tr>
			            </thead>
			            <tbody>';

    				    	foreach( $groups as $d )
    						{
    							$date = date( 'd F, Y', strtotime( $d['ttdate'] ) );

    							$category[] = $date;

    							$series[ $location ][ $date ][] = $d['ttfee'];

    							$tblcontent .= '
    							<tr>
    			                    <td>' . $d['vname'] . '</td>
    			                    <td>' . $d['dname'] . '</td>
    			                    <td>' . ucwords( $d['type'] ) . '</td>
    			                    <td>' . $date . '</td>
    			                    <td>' . $d['tttime'] . '</td>
    			                    <td class="text-right">' . number_format( $d['ttfee'], 0, '.' , '.' ) .'</td>
    			                </tr>';
    						}

    						$category = array_values( array_unique( $category ) );

    	                    $tblcontent .= '
			            </tbody>
			        </table>
				</div>
			</div>';

	    	add_variable( 'trip_data', $tblcontent );
	    	add_variable( 'trip_location', $location );
	    	add_variable( 'trip_item_cls', empty( $i ) ? 'opened' : '' );
	    	add_variable( 'total_by_location', number_format( $total_by_location, 0, '.' , '.' ) );

	    	parse_template( 'report-data-block', 'rd-block', true );

	    	$i++;
	    }

	    add_variable( 'ttdate', $ttdate );
	    add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
		add_variable( 'chart_data', get_chart_options( $category, $series ) );
	    add_variable( 'trans_type_option', get_pickup_drop_option( $type ) );
	    add_variable( 'report_type_option', get_report_type_option( $rtype ) );
	    add_variable( 'report_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );

	    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
	    add_variable( 'site_url', HTSERVER . site_url() );

	    parse_template( 'report-block', 'rp-block', false );
    }

    add_actions( 'section_title', 'Report - Transport Revenue' );
	add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

function ticket_seats_production_report()
{
    $filter = get_filter_seats_production_report();

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/report/seats-production.html', 'report' );

    add_block( 'report-block', 'rp-block', 'report' );

    add_variable( 'total', '' );
    add_variable( 'bdate', '' );
    add_variable( 'report_year_option', get_report_year_option( $ryear ) );
    add_variable( 'trip_option', get_trip_option( $tid, true, 'All Trip') );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'status_option', get_booking_status_option( $bstatus, true, 'All Status') );
    add_variable( 'report_view_option', get_report_view_option( $rview, 'seats-production' ) );
    add_variable( 'sales_person_option', get_sales_person_option( $agsp, true, 'All Sales Person' ) );
    add_variable( 'bsource_option', get_booking_source_option( $chid, null, true, 'All Booking Source',true ) );
    add_variable( 'rev_status', get_reservation_status( $rstatus, true ) );

	add_variable( 'bmethod_option', get_booking_methodby_option( $bmethod, false, '') );
	add_variable( 'pmethod_option', get_payment_method_option_2( $pmethod, false, '') );

    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'site_url', HTSERVER . site_url() );
    add_variable( 'ajax_response', HTSERVER . site_url() . '/ajax_process_request' );

    parse_template( 'report-block', 'rp-block', false );

    add_actions( 'section_title', 'Report - Seats Production' );
    add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
    add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
    add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/chart.js?v=' . TICKET_VERSION );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

function ticket_revenue_report()
{
	$filter = get_filter_ticket_revenue_report();

	extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/revenue-report.html', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );

	add_variable( 'bdate', $bdate );
    add_variable( 'bdateto', $bdateto );
    add_variable( 'report_fee_option', get_booking_fee_option( $feeopt ) );
    add_variable( 'boat_option', get_boat_option( $boid, true, 'All Boat') );
    add_variable( 'report_addons_option', get_booking_addons_option( $addonsopt ) );
    add_variable( 'report_type_option', get_report_type_option( $rtype, 'revenue' ) );
    add_variable( 'report_view_option', get_report_view_option( $rview, 'revenue' ) );
    add_variable( 'report_based_on_option', get_based_on_option( $rbased, 'revenue' ) );
    add_variable( 'depart_port_list', get_availibility_loc_option( 'All Port Departure', $deport ) );
    add_variable( 'sales_person_option', get_sales_person_option( $agsp, true, 'All Sales Person' ) );
    add_variable( 'report_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );
    add_variable( 'report_source_option', get_booking_source_option_revenue( $bsource, true, 'All Booking Source',true) );
    add_variable( 'report_sale_channel_option', get_sale_channel_option_revenue( $chid, true, 'All Sales Channel' ) );

    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'site_url', HTSERVER . site_url() );
    add_variable( 'ajax_response', HTSERVER . site_url() . '/ajax_process_request' );

    parse_template( 'report-block', 'rp-block', false );

    add_actions( 'section_title', 'Report - Revenue' );
	add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
	add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/chart.js?v=' . TICKET_VERSION );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

function ticket_average_rates_report()
{
    global $db;

	$filter = get_filter_ticket_average_rates_report();
	$data   = get_ticket_average_rates_report( $filter );

	extract( $data );
	extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/average-rates-report.html', 'report' );
	add_block( 'report-data-block', 'rd-block', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );
    add_block( 'no-report-block', 'nrp-block', 'report' );

    if( empty( $data['items'] ) )
    {
	    add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
	    add_variable( 'report_year_option', get_report_year_option( $ryear ) );
        add_variable( 'trip_option', get_trip_option( $tid, 'All Trip') );
        add_variable( 'route_option', get_route_option( $rid, 'All Route') );
        add_variable( 'report_source_option', get_booking_source_option_revenue( $rsource, true, 'All Booking Source',true) );
        add_variable( 'sales_person_option', get_sales_person_option( $agsp, true, 'All Sales Person' ) );

	    parse_template( 'no-report-block', 'nrp-block', false );
    }
    else
    {
	    $i = 0;

	    $category = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	    $series   = array();

	    foreach( $items as $year => $groups )
	    {
			$tblcontent = '';
	    	$average    = $total_price_per_year[$year] / $total_seat_per_year[$year];

	    	foreach( $groups as $month => $d )
			{
				$tblcontent .= '
				<div class="item">
					<div class="rs-head">
						<div class="row">
							<div class="col-md-6">
								<h5>' . $month . '</h5>
							</div>
							<div class="col-md-6">
								<p class="text-right medium-bold">' . number_format( $d['avgprice'], 0, '.' , '.' ) . '</p>
							</div>
						</div>
					</div>
				</div>';

                if ( !isset( $series[$year][$month] ) )
                {
                    $series[$year][$month][] = $d['avgprice'];
                }
			}

		    add_variable( 'rate_year', $year );
	    	add_variable( 'rate_by_month_data', $tblcontent );
		    add_variable( 'rate_by_year', number_format( $average , 0, '.' , '.' ) );
	    	add_variable( 'trip_item_cls', empty( $i ) ? 'opened' : '' );

		    parse_template( 'report-data-block', 'rd-block', true );

	    	$i++;
		}

    	add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
		add_variable( 'chart_data', get_chart_options( $category, $series, 'column' ) );
	    add_variable( 'report_year_option', get_report_year_option( $ryear ) );
        add_variable( 'trip_option', get_trip_option( $tid, 'All Trip') );
        add_variable( 'route_option', get_route_option( $rid, 'All Route') );
        add_variable( 'report_source_option', get_booking_source_option_revenue( $rsource, true, 'All Booking Source',true) );
        add_variable( 'sales_person_option', get_sales_person_option( $agsp, true, 'All Sales Person' ) );

	    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
	    add_variable( 'site_url', HTSERVER . site_url() );

    	parse_template( 'report-block', 'rp-block', false );
    }

    add_actions( 'section_title', 'Report - Average Rates' );
	add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

function ticket_hs_sales_sum_report()
{
	$filter = get_filter_ticket_hs_sales_sum_report();

	extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/hs-sales-sum-report.html', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );

    add_variable( 'bdate', $bdate );
    add_variable( 'bdateto', $bdateto );
    add_variable( 'destination_option', get_location_name_option( $lcid ) );
    add_variable( 'report_type_option', get_report_type_option( $rtype, 'hs-sales-sum-report' ) );
    add_variable( 'report_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );
    add_variable( 'report_source_option', get_booking_source_option_revenue( $rsource, true, 'All Booking Source',true) );

    add_variable( 'site_url', HTSERVER . site_url() );
    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'ajax_response', HTSERVER . site_url() . '/ajax_process_request' );

    add_actions( 'section_title', 'Report - Hi-Season Sales Summary' );
    add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
    add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
    add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
    add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/chart.js?v=' . TICKET_VERSION );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'report-block', 'rp-block', false );

    return return_template( 'report' );
}

function ticket_market_segment_report()
{
    global $db;

	$filter = get_filter_ticket_market_segment_report();
	$data   = get_ticket_market_segment_report( $filter );

	extract( $data );
	extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/market-segment-report.html', 'report' );
	add_block( 'report-data-block', 'rd-block', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );
    add_block( 'no-report-block', 'nrp-block', 'report' );

    if( empty( $data['items'] ) )
    {
	    add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
	    add_variable( 'report_year_option', get_report_year_option( $ryear ) );
	    add_variable( 'report_view_option', get_report_view_option( $rview, 'market-segment' ) );

	    parse_template( 'no-report-block', 'nrp-block', false );
    }
    else
    {
	    $i = 0;

	    $category = array();
	    $series   = array();

	    foreach( $items as $obj => $groups )
	    {
	    	list( $rev_month, $total_revenue_by_month, $total_percent_revenue_by_month ) = json_decode( base64_decode( $obj ) );

			$tblcontent = '';
			$category[] = $rev_month;

	    	foreach( $groups as $channel => $group )
			{
				list( $chcode, $chname, $total_revenue_by_channel, $total_percent_revenue_by_channel ) = json_decode( base64_decode( $channel ) );

				$tblcontent .= '
				<div class="item">
					<div class="rs-head">
						<div class="row">
							<div class="col-md-6">
								<h5>(' . $chcode . ') ' . $chname .'</h5>
							</div>
							<div class="col-md-6">
								<p class="text-right medium-bold">' . number_format( $total_revenue_by_channel, 0, '.' , '.' ) . ' (' . round( ( $total_revenue_by_channel * 100 ) / $total, 2 ) . '%)</p>
							</div>
						</div>
					</div>
					<div class="rs-table">
						<table id="reservation-table" class="table table-striped" cellspacing="0" width="100%">
				            <thead>
				                <tr>
				                    <th width="15%">Date</th>
				                    <th width="35%">Note</th>
				                    <th width="35%">Booking Source</th>
				                    <th width="15%" class="text-right">Amount</th>
				                </tr>
				            </thead>
				            <tbody>';

				            	foreach( $group as $d )
								{
									$tblcontent .= '
									<tr>
			                            <td>' . date( 'd F Y', strtotime( $d['bdate'] ) ) . '</td>
			                            <td>Booking #' . $d['bticket'] .'</td>
			                            <td>(' . $d['chcode'] . ') ' . $d['chname'] .'</td>
			                            <td class="text-right">' . number_format( $d['btotal'], 0, '.' , '.' ) .'</td>
			                        </tr>';
			                    }

			                    $tblcontent .= '
				            </tbody>
				        </table>
					</div>
				</div>';

				$series[ '(' . $chcode . ') ' . $chname ][ $rev_month ][] = $total_revenue_by_channel;
			}

	    	add_variable( 'rev_month', $rev_month );
	    	add_variable( 'trip_data', $tblcontent );
	    	add_variable( 'total_revenue_by_month', number_format( $total_revenue_by_month, 0, '.' , '.' ) );
	    	add_variable( 'total_percent_revenue_by_month', $total_percent_revenue_by_month );
	    	add_variable( 'trip_item_cls', empty( $i ) ? 'opened' : '' );

	    	parse_template( 'report-data-block', 'rd-block', true );

	    	$i++;
	    }

	    add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
		add_variable( 'chart_data', get_chart_options( $category, $series ) );
	    add_variable( 'report_year_option', get_report_year_option( $ryear ) );
	    add_variable( 'report_view_option', get_report_view_option( $rview, 'market-segment' ) );
	    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
	    add_variable( 'site_url', HTSERVER . site_url() );

	    parse_template( 'report-block', 'rp-block', false );
    }

    add_actions( 'section_title', 'Report - Market Segment' );
	add_actions( 'admin_tail', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'admin_tail', 'get_custom_javascript', '//code.highcharts.com/highcharts.js' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

function ticket_promo_code_report()
{
    global $db;

	$filter = get_filter_ticket_promo_code_report();
	$data   = get_ticket_promo_code_report( $filter );

	extract( $data );
	extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/report/promo-code-report.html', 'report' );

	add_block( 'report-data-block', 'rd-block', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );
    add_block( 'no-report-block', 'nrp-block', 'report' );

    if( empty( $data['items'] ) )
    {
        add_variable( 'bdate', $bdate );
        add_variable( 'bdateto', $bdateto );
        add_variable( 'report_based_on_option', get_based_on_option( $rbased ) );
        add_variable( 'report_type_option', get_report_type_option( $rtype, 'transport' ) );

	    add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
	    add_variable( 'promo_code_option', get_promo_code_option( $promo_code, false ) );
	    add_variable( 'booking_source_option', get_booking_source_option( $chid, null, true, 'All Booking Source',true) );
	    add_variable( 'booking_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );

	    parse_template( 'no-report-block', 'nrp-block', false );
    }
    else
    {
	    $i = 0;

	    $category = array();
	    $series   = array();

        foreach( $items as $pmcode => $groups )
        {
			$tblcontent = '
			<div class="item">
				<div class="rs-table">
					<table id="reservation-table" class="table table-striped" cellspacing="0" width="100%">
			            <thead>
			                <tr>
			                    <th width="15%">Date</th>
			                    <th width="30%">Booking Ticket</th>
			                    <th width="20%">Booking Source</th>
                                <th width="15%">Travel Date</th>
                                <th width="25%">Route</th>
                                <th class="text-right">Commission</th>
			                    <th class="text-right">Discount</th>
			                </tr>
			            </thead>
			            <tbody>';

                            $total_sum_by_code = 0;
                            $total_sum_com_by_code = 0;

			            	foreach( $groups['items'] as $d )
							{
                                $total_sum_by_code += $d['bdiscount'];
                                $total_sum_com_by_code += $d['bcommission'];

								$tblcontent .= '
								<tr>
		                            <td>' . date( 'd F Y', strtotime( $d['bdate'] ) ) . '</td>
		                            <td>Booking #' . $d['bticket'] .'</td>
		                            <td>' . $d['bsource'] . '</td>
                                    <td>' . $d['bddate'] . '</td>
                                    <td>' . $d['route'] . '</td>
                                    <td class="text-right">' . number_format( $d['bcommission'], 0, '.' , '.' ) . '</td>
		                            <td class="text-right">' . number_format( $d['bdiscount'], 0, '.' , '.' ) . '</td>
		                        </tr>';
		                    }

		                    $tblcontent .= '
			            </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="5" class="text-right">Total :</th>
                                <th class="text-right">' . number_format( $total_sum_com_by_code, 0, '.' , '.' ) . '</th>
                                <th class="text-right">' . number_format( $total_sum_by_code, 0, '.' , '.' ) . '</th>
                            </tr>
                        </tfoot>
			        </table>
				</div>
			</div>';

			add_variable( 'pmcode', $pmcode );
	    	add_variable( 'trip_data', $tblcontent );
			add_variable( 'total_used', $groups['totalbycode'] );
	    	add_variable( 'trip_item_cls', empty( $i ) ? 'opened' : '' );
			add_variable( 'total_used_percent', round( ( $groups['totalbycode'] * 100 ) / $total, 2 ) );

	    	parse_template( 'report-data-block', 'rd-block', true );

	    	$i++;
	    }

        add_variable( 'bdate', $bdate );
        add_variable( 'bdateto', $bdateto );
        add_variable( 'report_based_on_option', get_based_on_option( $rbased ) );
        add_variable( 'report_type_option', get_report_type_option( $rtype, 'transport' ) );

	    add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
	    add_variable( 'promo_code_option', get_promo_code_option( $promo_code, false ) );
		add_variable( 'booking_source_option', get_booking_source_option( $chid, null, true, 'All Booking Source', true ) );
	    add_variable( 'booking_status_option', get_booking_status_option( $bstatus, true, 'Select Payment Status' ) );
	    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
	    add_variable( 'site_url', HTSERVER . site_url() );

		parse_template( 'report-block', 'rp-block', false );
	}

    add_actions( 'section_title', 'Report - Promo Code' );
    add_actions( 'admin_tail', 'get_custom_javascript', HTSERVER . site_url() . '/l-plugins/ticket/js/ticket.js?v=' . TICKET_VERSION );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'report' );
}

function ticket_is_report_filter_view()
{
    if( isset( $_GET['state'] ) && $_GET['state']=='report' && isset( $_POST['filter'] ) || isset( $_GET['filter'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Report - set chart option
| -------------------------------------------------------------------------------------
*/
function get_chart_options( $category, $series, $type='line', $float = false )
{
	$legend = array();

    foreach( $series as $name => $obj )
    {
        $data = array();

        foreach( $category as $loc )
        {
            if( isset( $obj[$loc] ) )
            {
                if( $float )
                {
                    $val = $obj[$loc][0];
                }
                else
                {
                    $val = floatval( str_replace( '.', '', $obj[$loc][0] ) );
                }
            }
            else
            {
                $val = 0;
            }

            $data[] = $val;
        }

        $legend[] = array( 'name' => $name, 'data' => $data );
    }

	$chart_opts = array(
		'chart'   => array( 'type' => $type, 'marginTop' => 80 ),
		'title'   => array( 'text' => '' ),
		'xAxis'   => array( 'categories' => $category ),
		'yAxis'   => array( 'title' => array( 'text' => '' ) ),
		'credits' => array( 'enabled' => false ),
		'legend'  => array( 'align' => 'left', 'verticalAlign' => 'top', 'x' => 40, 'y' => 0 ),
		'series'  => $legend
	);

	return base64_encode( json_encode( $chart_opts ) );
}

function get_series_data( $obj, $category )
{
	$data = array();

	foreach( $category as $name )
	{
		$data[] = isset( $obj[$name] ) ? $obj[$name][0] : 0;
	}
}

/*
| -------------------------------------------------------------------------------------
| Report - get data
| -------------------------------------------------------------------------------------
*/
function get_passenger_list_report( $filter )
{
    global $db;

    extract( $filter );

	$w = array();
	$b = '';

    if( $boid != '' )
    {
        $w[] = $db->prepare_query( ' b.boid = %d', $boid );
    }

    if( $chids != '' )
    {
    	$arr = explode( '|', $chids );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

    	if( isset( $arr[1] ) )
    	{
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
    	}
    }

    if( $taid != '' )
    {
        $w[] = $db->prepare_query( ' g.taid = %d', $taid );
    }

    if( $rtype != '' && $bddate != '' )
    {
    	if( $rtype == '0'  )
    	{
    		$date = date( 'Y-m-d', strtotime( $bddate ) );

    		$w[] = $db->prepare_query( ' b.bddate = %s', $date );
    	}
    	elseif( $rtype == '1' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 ' . $bddate ) );

    		$w[] = $db->prepare_query( ' YEAR( b.bddate ) = YEAR( %s ) AND MONTH( b.bddate ) = MONTH( %s )', $date, $date );
    	}
    	elseif( $rtype == '2' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 Jan ' . $bddate ) );

    		$w[] = $db->prepare_query( ' YEAR( b.bddate ) = YEAR( %s )', $date );
    	}
    	elseif( $rtype == '3' )
    	{
    		$date_from = date( 'Y-m-d', strtotime( $bddate ) );
    		$date_to   = empty( $bddateto ) ? $date_from : date( 'Y-m-d', strtotime( $bddateto ) );

    		$w[] = $db->prepare_query( ' b.bddate BETWEEN %s AND %s', $date_from, $date_to );
    	}
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = ' AND ( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' AND b.bdpstatus = %s', $bstatus );
        }
    }

    if( $rid != '' )
    {
        if( is_array( $rid ) )
        {
            $estatus = end( $rid );

            $b .= ' AND ( ';

            foreach( $rid as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'b.rid = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.rid = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b .= $db->prepare_query( ' AND b.rid = %s', $rid );
        }
    }

	if( empty( $w ) && empty( $b ) )
	{
		$where = '';
	}
	elseif( empty( $w ) && !empty( $b ) )
	{
		$where = $b;
	}
	elseif( !empty( $w ) && empty( $b ) )
	{
		$where = 'AND ' . implode( ' AND', $w );
	}
	else
	{
		$where = $b . ' AND ' . implode( ' AND', $w );
	}

	$s = 'SELECT
			a.bid,
			a.chid,
			a.bticket,
			a.bremark,
            a.bdate,
			c.rdepart,
			c.rarrive,
			c.rname,
			a.agid,
			a.bbname,
			d.chcode,
			d.chname,
			e.bocode,
			b.bdid,
			b.bdfrom,
			b.bdto,
			b.num_adult,
			b.num_child,
			b.num_infant,
            b.bddate,
			( b.num_adult + b.num_child + b.num_infant ) AS amount
		  FROM ticket_booking AS a
		  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
		  LEFT JOIN ticket_route AS c ON b.rid = c.rid
		  LEFT JOIN ticket_channel AS d ON a.chid = d.chid
		  LEFT JOIN ticket_boat AS e ON b.boid = e.boid
		  LEFT JOIN ticket_location AS f ON c.rdepart = f.lcid
		  LEFT JOIN ticket_booking_transport AS g ON b.bdid = g.bdid
		  WHERE b.bdrevstatus != "no-show" AND a.bstt <> "ar" ' . $where . ' GROUP BY b.bdid ORDER BY f.lctype ASC, b.bdfrom ASC, b.bdto ASC, b.bddate ASC';
	$r = $db->do_query( $s );
	$items  = array();
	$data   = array();
    $pasnum = array();
	$larray = array();
	$rarray = array();
	$garray = array();

	$category = array();
	$series   = array();

    $rcount = 0;
	while( $d = $db->fetch_array( $r ) )
	{
		$data[] = $d;

		$rcount += $d['amount'];

        if( isset( $larray[ $d['bdfrom'] . '-' . $d['bdto'] ] ) )
		{
            $larray[ $d['bdfrom'] . '-' . $d['bdto'] ] += $d['amount'];
        }
        else
        {
            $larray[ $d['bdfrom'] . '-' . $d['bdto'] ] = $d['amount'];
        }

        if( isset( $garray[ $d['bdfrom'] . '-' . $d['bdto'] ][ $d['chid'] ] ) )
		{
            $garray[ $d['bdfrom'] . '-' . $d['bdto'] ][ $d['chid'] ] += $d['amount'];
        }
        else
        {
            $garray[ $d['bdfrom'] . '-' . $d['bdto'] ][ $d['chid'] ] = $d['amount'];
        }
	}

	foreach( $data as $d )
	{
	    if( ( is_array( $bstatus ) && in_array( 'cn', $bstatus ) ) || $bstatus == 'cn' )
	    {
            $pass  = ticket_passenger_list( $d['bdid'], false );
            $gname = ticket_passenger_list_name( $pass, false, false, true );
	    }
	    else
	    {
            $pass = ticket_passenger_list( $d['bdid'], false, false );

            if( empty( $pass ) )
            {
                continue;
            }

            $gname = ticket_passenger_list_name( $pass, false, false, true, 'passenger-list' );
	    }

        $tarea  = ticket_transport_list_area( $d['bdid'], 'taname' );
        $pickup = isset( $tarea['pickup'] ) ? $tarea['pickup'] : '-';
        $drpoff = isset( $tarea['drop-off'] ) ? $tarea['drop-off'] : '-';

		$ltotal = $larray[ $d['bdfrom'] . '-' . $d['bdto'] ];
		$gtotal = $garray[ $d['bdfrom'] . '-' . $d['bdto'] ][ $d['chid'] ];

		$group  = base64_encode( json_encode( array( $d['chid'], $d['chcode'], $d['chname'], $gtotal, round( ( $gtotal * 100 ) / $ltotal, 2 ) ) ) );
		$index  = base64_encode( json_encode( array( $d['bdfrom'] . '-' . $d['bdto'], $ltotal, round( ( $ltotal * 100 ) / $rcount, 2 ) ) ) );

		$remark = empty( $d['bremark'] ) ? '-' : $d['bremark'];
		$bbname = empty( $d['agid'] ) ? $d['bbname'] : get_agent( $d['agid'], 'agname' );

        if( isset( $pasnum[ $index ][ $group ] ) )
        {
            $pasnum[ $index ][ $group ] += $d['amount'];
        }
        else
        {
            $pasnum[ $index ][ $group ] = $d['amount'];
        }

		$items[ $index ][ $group ][] = array(
			'gname' => $gname,
			'bid' => $d['bid'],
            'bdate' => date( 'd M Y', strtotime( $d['bdate'] ) ),
            'bddate' => date( 'd M Y', strtotime( $d['bddate'] ) ),
			'chid' => $d['chid'],
			'agid' => $d['agid'],
			'rname' => $d['rname'],
			'bocode' => $d['bocode'],
			'bbname' => $bbname,
			'chcode' => $d['chcode'],
			'chname' => $d['chname'],
			'rdepart' => $d['rdepart'],
			'rarrive' => $d['rarrive'],
			'bticket' => $d['bticket'],
			'bremark' => $remark,
			'bdpickup' => $pickup,
			'bddropoff' => $drpoff,
			'pass_num' => $d['amount']
		);
	}

	foreach( $items as $obj => $groups )
	{
		list( $location, $total_passenger, $total_percent ) = json_decode( base64_decode( $obj ) );

		$category[] = $location;

		foreach( $groups as $channel => $group )
		{
			list( $chid, $chcode, $chname ) = json_decode( base64_decode( $channel ) );

			$series[ $chname ][ $location ][] = $pasnum[ $obj ][ $channel ];
		}
	}
	if( $rcount == 0 )
	{
		return array( 'total' => $rcount );
	}
	else
	{
		return array(
            'chart'     => get_chart_options( $category, $series ),
            'total'     => $rcount,
            'items'     => $items,
            'filter'    => base64_encode( json_encode( $filter ) )
        );
	}
}

function get_passenger_row_list_report( $filter )
{
    global $db;

    extract( $filter );

    $w = array();
    $b = '';

    if( $boid != '' )
    {
        $w[] = $db->prepare_query( ' d.boid = %d', $boid );
    }

    if( $chids != '' )
    {
        $arr = explode( '|', $chids );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

        if( isset( $arr[1] ) )
        {
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
        }
    }

    if( $taid != '' )
    {
        $w[] = $db->prepare_query( ' f.taid = %d', $taid );
    }

    if( $rtype != '' && $bddate != '' )
    {
        if( $rtype == '0'  )
        {
            $date = date( 'Y-m-d', strtotime( $bddate ) );

            $w[] = $db->prepare_query( ' d.bddate = %s', $date );
        }
        elseif( $rtype == '1' )
        {
            $date = date( 'Y-m-d', strtotime( '1 ' . $bddate ) );

            $w[] = $db->prepare_query( ' YEAR( d.bddate ) = YEAR( %s ) AND MONTH( d.bddate ) = MONTH( %s )', $date, $date );
        }
        elseif( $rtype == '2' )
        {
            $date = date( 'Y-m-d', strtotime( '1 Jan ' . $bddate ) );

            $w[] = $db->prepare_query( ' YEAR( d.bddate ) = YEAR( %s )', $date );
        }
        elseif( $rtype == '3' )
        {
            $date_from = date( 'Y-m-d', strtotime( $bddate ) );
            $date_to   = empty( $bddateto ) ? $date_from : date( 'Y-m-d', strtotime( $bddateto ) );

            $w[] = $db->prepare_query( ' d.bddate BETWEEN %s AND %s', $date_from, $date_to );
        }
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = '  AND ( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'd.bdpstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'd.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' AND d.bdpstatus = %s', $bstatus );
        }
    }
    else
    {
        $b .= $db->prepare_query( ' AND d.bdpstatus NOT IN( %s )', 'ol' );
    }

    if( $rid != '' )
    {
        if( is_array( $rid ) )
        {
            $estatus = end( $rid );

            $b .= ' AND ( ';

            foreach( $rid as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'd.rid = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'd.rid = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b .= $db->prepare_query( ' AND d.rid = %s', $rid );
        }
    }

    if( empty( $w ) && empty( $b ) )
    {
        $where = 'WHERE a.bstt <> "ar" ';
    }
    elseif( empty( $w ) && !empty( $b ) )
    {
        $where = 'WHERE a.bstt <> "ar" ' . $b;
    }
    elseif( !empty( $w ) && empty( $b ) )
    {
        $where = 'WHERE a.bstt <> "ar" AND ' . implode( ' AND', $w );
    }
    else
    {
        $where = 'WHERE a.bstt <> "ar" ' . $b . ' AND ' . implode( ' AND', $w );
    }

    $s = 'SELECT
            a.bid,
            a.bticket,
            a.btype,
            a.bdate,
            a.bbrevtype,
            a.bbrevstatus,
            a.bstatus,
            a.agid,
            a.sagid,
            a.bbooking_staf,
            p.bptype,
            p.bpid,
            p.bpname,
            p.bpgender,
            p.bpbirthdate,
            p.lcountry_id,
            p.bpstatus,
            a.bremark,
            m.mname,
            a.bhotelname,
            a.bhoteladdress,
            a.bhotelphone,
            cn.chname,
            a.bbname,
            a.bbcountry,
            a.bbemail,
            a.bbphone,
            a.bbphone2,
            d.bddate,
            e.rname,
            d.bdid,
            d.bdtype,
            d.bdfrom,
            d.bdto,
            d.bddeparttime,
            d.bdarrivetime
          FROM ticket_booking AS a
          LEFT JOIN ticket_channel AS cn ON cn.chid = a.chid
          LEFT JOIN ticket_booking_payment_method AS m ON m.mid = a.bpaymethod
          LEFT JOIN ticket_booking_detail AS d ON a.bid = d.bid
          LEFT JOIN ticket_route AS e ON d.rid = e.rid
          LEFT JOIN ticket_boat AS o ON d.boid = o.boid
          LEFT JOIN ticket_booking_transport AS f ON d.bdid = f.bdid
          LEFT JOIN ticket_booking_passenger AS p ON d.bdid = p.bdid
          ' . $where . ' GROUP BY p.bpid ORDER BY a.bticket ASC';
    $r = $db->do_query( $s );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $agid = empty( $d['agid'] ) ? $d['sagid'] : $d['agid'];
        $agname = get_agent( $agid, 'agname' );
        $booked = empty( $agid ) ? $d['bbname'] : $d['bbooking_staf'];

        $type = ticket_transport_list_area( $d['bdid'], 'bttype' );
        $ttype = ticket_transport_list_area( $d['bdid'], 'bttrans_type' );
        $tarea = ticket_transport_list_area( $d['bdid'], 'taname' );
        $thotel = ticket_transport_list_area( $d['bdid'], 'bthotelname' );
        $thaddr = ticket_transport_list_area( $d['bdid'], 'bthoteladdress' );
        $thphone = ticket_transport_list_area( $d['bdid'], 'bthotelphone' );

        $p_type = isset( $type['pickup'] ) ? 'Pickup' : '-';
        $d_type = isset( $type['drop-off'] ) ? 'Drop-off' : '-';

        $p_transtype = isset( $ttype['pickup'] ) ? $ttype['pickup'] : '-';
        $d_transtype = isset( $ttype['drop-off'] ) ? $ttype['drop-off'] : '-';

        $p_area = isset( $tarea['pickup'] ) ? $tarea['pickup'] : '-';
        $d_area = isset( $tarea['drop-off'] ) ? $tarea['drop-off'] : '-';

        $p_hotel = isset( $thotel['pickup'] ) ? $thotel['pickup'] : '-';
        $d_hotel = isset( $thotel['drop-off'] ) ? $thotel['drop-off'] : '-';

        $p_hoteladdr = isset( $thaddr['pickup'] ) ? $thaddr['pickup'] : '-';
        $d_hoteladdr = isset( $thaddr['drop-off'] ) ? $thaddr['drop-off'] : '-';

        $p_hotelphone = isset( $thphone['pickup'] ) ? $thphone['pickup'] : '-';
        $d_hotelphone = isset( $thphone['drop-off'] ) ? $thphone['drop-off'] : '-';

        $country = get_country( $d['lcountry_id'], 'lcountry' );
        $bcountry = get_country( $d['bbcountry'], 'lcountry' );

        $p_ttype = '';
        $d_ttype = '';

        if ( $p_transtype == '0' )
        {
            $p_ttype = 'Shared';
        }
        else if( $p_transtype == '1' )
        {
            $p_ttype = 'Private';
        }
        else if( $p_transtype == '2' )
        {
            $p_ttype = 'No Trans';
        }

        if ( $d_transtype == '0' )
        {
            $d_ttype = 'Shared';
        }
        else if( $d_transtype == '1' )
        {
            $d_ttype = 'Private';
        }
        else if( $d_transtype == '2' )
        {
            $d_ttype = 'No Trans';
        }

        $data[] = array(
            'bticket'       => $d['bticket'],
            'btype'         => $d['btype'] == '0' ? 'One Way' : 'Return',
            'bdate'         => date( 'd M Y', strtotime( $d['bdate'] ) ),
            'bbrevtype'     => $d['bbrevtype'],
            'bbrevstatus'   => $d['bbrevstatus'],
            'mname'         => $d['mname'],
            'bstatus'       => ticket_booking_status( $d['bstatus'] ),
            'bpname'        => $d['bpname'],
            'gender'        => $d['bpgender'] == '1' ? 'M' : 'F',
            'bpbirthdate'   => date( 'd M Y', strtotime( $d['bpbirthdate'] ) ),
            'lcountry'      => $country == 'Select country' ? '' : $country,
            'remark'        => empty( $d['bremark'] ) ? '-' : str_replace( ',', ';', $d['bremark'] ),
            'adult'         => $d['bptype'] == 'adult' ? '1' : '',
            'child'         => $d['bptype'] == 'child' ? '1' : '',
            'infant'        => $d['bptype'] == 'infant' ? '1' : '',
            'bhotelname'    => $d['bhotelname'],
            'bhoteladd'     => $d['bhoteladdress'],
            'bhotelphone'   => $d['bhotelphone'],
            'chname'        => empty( $agname ) ? $d['chname'] : $agname,
            'bbname'        => $booked,
            'bbcountry'     => $bcountry == 'Select country' ? '' : $bcountry,
            'bbemail'       => $d['bbemail'],
            'bbphone'       => $d['bbphone'] == '' ? $d['bbphone2'] : $d['bbphone'],
            'bddate'        => date( 'd M Y', strtotime( $d['bddate'] ) ),
            'rname'         => $d['rname'],
            'bdfrom'        => $d['bdfrom'],
            'bdto'          => $d['bdto'],
            'bddeparttime'  => $d['bddeparttime'],
            'bdarrivetime'  => $d['bdarrivetime'],
            'p_type'        => $p_type,
            'p_transtype'   => $p_ttype,
            'p_area'        => $p_area,
            'p_hotel'       => $p_hotel,
            'p_hoteladdr'   => $p_hoteladdr,
            'p_hotelphone'  => $p_hotelphone,
            'd_type'        => $d_type,
            'd_transtype'   => $d_ttype,
            'd_area'        => $d_area,
            'd_hotel'       => $d_hotel,
            'd_hoteladdr'   => $d_hoteladdr,
            'd_hotelphone'  => $d_hotelphone,
            'bpstatus'      => $d['bpstatus']
        );
    }

    return $data;
}

function get_passenger_list_fo_report( $filter )
{
    global $db;

	$w = array();
	$b = '';

	extract( $filter );

    if( $boid != '' )
    {
        $w[] = $db->prepare_query( ' d.boid = %d', $boid );
    }

    if( $chids != '' )
    {
    	$arr = explode( '|', $chids );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

    	if( isset( $arr[1] ) )
    	{
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
    	}
    }

    if( $taid != '' )
    {
        $w[] = $db->prepare_query( ' i.taid = %d', $taid );
    }

    if( $rtype != '' && $bddate != '' )
    {
    	if( $rtype == '0'  )
    	{
    		$date = date( 'Y-m-d', strtotime( $bddate ) );

    		$w[] = $db->prepare_query( ' d.bddate = %s', $date );
    	}
    	elseif( $rtype == '1' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 ' . $bddate ) );

    		$w[] = $db->prepare_query( ' YEAR( d.bddate ) = YEAR( %s ) AND MONTH( d.bddate ) = MONTH( %s )', $date, $date );
    	}
    	elseif( $rtype == '2' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 Jan ' . $bddate ) );

    		$w[] = $db->prepare_query( ' YEAR( d.bddate ) = YEAR( %s )', $date );
    	}
    	elseif( $rtype == '3' )
    	{
    		$date_from = date( 'Y-m-d', strtotime( $bddate ) );
    		$date_to   = empty( $bddateto ) ? $date_from : date( 'Y-m-d', strtotime( $bddateto ) );

    		$w[] = $db->prepare_query( ' d.bddate BETWEEN %s AND %s', $date_from, $date_to );
    	}
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b  = ' AND ( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'd.bdpstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'd.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' AND d.bdpstatus = %s', $bstatus );
        }
    }
    else
    {
        $b = $db->prepare_query( ' AND d.bdpstatus IN( %s, %s, %s )', 'pa', 'ca', 'pp' );
    }

    if( $rid != '' )
    {
        if( is_array( $rid ) )
        {
            $estatus = end( $rid );

            $b .= ' AND ( ';

            foreach( $rid as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'd.rid = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'd.rid = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b .= $db->prepare_query( ' AND d.rid = %s', $rid );
        }
    }

	if( empty( $w ) && empty( $b ) )
	{
		$where = 'WHERE a.bstt <> "ar" ';
	}
	elseif( empty( $w ) && !empty( $b ) )
	{
		$where = 'WHERE a.bstt <> "ar" ' . $b;
	}
	elseif( !empty( $w ) && empty( $b ) )
	{
		$where = 'WHERE a.bstt <> "ar" AND ' . implode( ' AND', $w );
	}
	else
	{
		$where = 'WHERE a.bstt <> "ar" ' . $b . ' AND ' . implode( ' AND', $w );
	}

    $s = 'SELECT
            a.bticket,
            a.bid,
            a.agid,
            d.bdid,
            a.btype,
            g.rname,
            a.bpaymethod,
            b.chname,
            c.agname,
            d.bddate,
            f.bocode,
            h.bpname,
            h.bpstatus,
            h.bptype,
            h.bpid,
            j.lctype,
            l.lcountry_code,
            k.lcname AS to_loc,
            j.lcname AS from_loc,
            (
                SELECT GROUP_CONCAT( a2.bthotelname SEPARATOR " / " )
                FROM ticket_booking_transport AS a2
                LEFT JOIN ticket_transport_area AS b2 ON a2.taid = b2.taid
                WHERE a2.bdid = d.bdid GROUP BY a2.bdid
            ) AS transport,
            (
                SELECT a2.bddate FROM ticket_booking_detail AS a2
                WHERE a2.bdtype = "return" AND a2.bdid = d.bdid
            ) AS next_trip_date,
            (
                SELECT a2.rdtime FROM ticket_route_detail AS a2
                WHERE a2.rid = d.rid AND a2.lcid = d.bdfrom_id
            ) AS trip_time
          FROM ticket_booking AS a
          LEFT JOIN ticket_channel AS b ON a.chid = b.chid
          LEFT JOIN ticket_agent AS c ON a.agid = c.agid
          LEFT JOIN ticket_booking_detail AS d ON d.bid = a.bid
          LEFT JOIN ticket_boat AS f ON d.boid = f.boid
          LEFT JOIN ticket_route AS g ON d.rid = g.rid
          LEFT JOIN ticket_booking_passenger AS h ON h.bdid = d.bdid
          LEFT JOIN ticket_booking_transport AS i ON i.bdid = d.bdid
          LEFT JOIN ticket_location AS j ON d.bdfrom_id = j.lcid
          LEFT JOIN ticket_location AS k ON d.bdto_id = k.lcid
          LEFT JOIN l_country AS l ON h.lcountry_id = l.lcountry_id ' . $where . '
          GROUP BY h.bpid, a.bid, d.bdid
          ORDER BY d.bddate, h.bpid, trip_time, g.rname, from_loc, to_loc ASC';
	$r = $db->do_query( $s );
	if( is_array( $r ) )
	{
		return array();
	}
	else
	{
		while( $d = $db->fetch_array( $r ) )
		{
            // $ntrip  = empty( $d['next_trip_date'] ) ? '-' : date( 'd F Y', strtotime( $d['next_trip_date'] ) );
            $ntrip  = $d['btype'] == 0 ? '-' : get_next_trip($d['bpname'], $d['bid']);
            $index  = base64_encode( $d['bocode'] . '|' . strtotime( $d['bddate'] ) . '|' . $d['rname'] );
            $name   = $d['from_loc'] . ' ( ' . date( 'H:i', strtotime( $d['trip_time'] ) ) . ' )';
            $group  = in_array( $d['lctype'], array( 1, 2 ) ) ? 'Return' : 'Out';
            $source = empty( $d['agid'] ) ? $d['chname'] : $d['agname'];
            $trans  = empty( $d['transport'] ) ? '-' : $d['transport'];
            $method = get_booking_payment_method( $d );

            if( $d['bpstatus'] != 'cn' )
            {
                $items[ $index ][ $group ][ $name ][ $d['to_loc'] ][] = array(
                    'next_trip' => $ntrip,
                    'btrans'    => $trans,
                    'bsource'   => $source,
                    'bpmethod'  => $method,
                    'bdto'      => $d['to_loc'],
                    'bptype'    => $d['bptype'],
                    'bcountry'  => $d['lcountry_code'],
                    'bpname'    => ucwords( strtolower( $d['bpname'] ) )
                );
            }
		}

		return $items;
	}
}

function get_next_trip($bpname ,$bid){
    global $db;
    $explode = explode(" ",$bpname);
    $implode = implode(",", $explode);
    
    $s  = 'SELECT b.bpname, a.bddate from ticket_booking_detail as a left join ticket_booking_passenger as b on a.bdid = b.bdid where bid = %d and bdtype =%s';
    // $s  ="SELECT 
    //         b.bddate FROM ticket_booking_passenger as a LEFT JOIN ticket_booking_detail as b on a.bdid = b.bdid 
    //         left join ticket_booking as c on b.bid= c.bid 
    //         WHERE (a.bpname like '%".$bpname."%' OR a.bpname LIKE '".$explode[0]."%')
    //         AND b.bdtype = 'return' AND b.bid = ".$bid." AND c.bstt <> 'ar'";
    // $s2 = 'SELECT * FROM `ticket_booking_detail`as a left join ticket_booking_passenger as b on a.bdid = b.bdid where a.bid = %d AND b.bpid = %d AND bdtype = "return"';
    $p = $db->prepare_query( $s, $bid, 'return' );
    // print_r('test');

	$r = $db->do_query( $p );
	
    $n = $db->num_rows( $r );
    $data = "";
    if($n == 0){
        print_r($p);
        exit();
    }
	while( $d = $db->fetch_array( $r ) )
        {   $explode_database = explode(" ",$d['bpname']);
            if(strpos($bpname, $d['bpname']) !== false || strpos($d['bpname'], $bpname) !== false || $bpname == $d['bpname']){
                $data = $d;
                break;
            }
            if($explode_database[0] == $explode[0]){
                $data = $d;
                break;
            }
        }
        // if($data == ""){
        //     print_r($bpname);
        //     print_r(' batas ');
        //     print_r($p);
        // }
        
        if($data != ""){
            return date( 'd F Y', strtotime( $data['bddate'] ) );
        }
        else{
            return '-';
        }
        // return $data['bddate'];
}

function get_reservation_list_report( $filter, $order, $csv = false )
{
    global $db;

    $w = '';

    extract( $filter );

    if( !empty( $status ) )
    {
        if( is_array( $status ) )
        {
            $estatus = end( $status );

            $w .= ' AND ( ';

            foreach( $status as $st )
            {
                if( $st == $estatus )
                {
                    $w .= $db->prepare_query( 'd.bdpstatus = %s', $st );
                }
                else
                {
                    $w .= $db->prepare_query( 'd.bdpstatus = %s OR ', $st );
                }
            }

            $w .= ' )';
        }
        else
        {
            if( $status == 'pp' )
            {
                $w .= $db->prepare_query( ' AND d.bdpstatus IN( %s, %s )', 'pp', 'ol' );
            }
            else
            {
                $w .= $db->prepare_query( ' AND d.bdpstatus = %s', $status );
            }
        }

        $w .= $db->prepare_query( ' AND a.bstt = %s', 'aa' );
    }
    else
    {
        $w .= $db->prepare_query( ' AND d.bdpstatus NOT IN( %s )', 'ol' );
    }

    if( !empty( $rstatus ) )
    {
        if( is_array( $rstatus ) )
        {
            $estatus = end( $rstatus );

            $w .= ' AND ( ';

            foreach( $rstatus as $st )
            {
                if( $st == $estatus )
                {
                    $w .= $db->prepare_query( 'd.bdrevstatus = %s', $st );
                }
                else
                {
                    $w .= $db->prepare_query( 'd.bdrevstatus = %s OR ', $st );
                }
            }

            $w .= ' )';
        }
        else
        {
            $w .= $db->prepare_query( ' AND d.bdrevstatus = %s', $rstatus );
        }
    }

    if( $chid != '' )
    {
        $arr = explode( '|', $chid );

        if( count( $arr ) == 2 )
        {
            $w .= $db->prepare_query( ' AND a.chid = %d', $arr[0] );
            $w .= $db->prepare_query( ' AND a.agid = %d', $arr[1] );
        }
        else
        {
            $w .= $db->prepare_query( ' AND a.chid = %d', $chid );
        }
    }

    if( $lcid != '' )
    {
        $w .= $db->prepare_query( ' AND d.bdfrom = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid );
    }

    if( $lcid2 != '' )
    {
        $w .= $db->prepare_query( ' AND d.bdto = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid2 );
    }

    if( $rid != '' )
    {
        $w .= $db->prepare_query( ' AND d.rid = %d', $rid );
    }

    if( $bdate != '' )
    {
        $w .= $db->prepare_query( ' AND a.bdate = %s', date( 'Y-m-d', strtotime( $bdate ) ) );
    }

    if( $bbemail != '' )
    {
        $w .= $db->prepare_query( ' AND a.bbemail LIKE %s', '%' . $bbemail . '%' );
    }

    if( $date_start != '' && $date_end != '' )
    {
        $w .= $db->prepare_query( ' AND d.bddate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $date_start ) ), date( 'Y-m-d', strtotime( $date_end ) ) );
    }

    if( isset( $sbooking ) && !empty( $sbooking ) )
    {
        $w .= ' AND a.bticket LIKE "%' . $sbooking . '%"';

    }

    if( is_array( $order ) && !empty( $order ) )
    {
        $cols  = array(
            0  => 'a.bticket',
            1  => 'a.bdate',
            5  => 'd.bddate',
            7  => 'd.bddeparttime',
            8  => 'd.bdarrivetime',
            10 => 'a.bbrevstatus'
        );

        $o = array();

        foreach( $order as $i => $od )
        {
            $o[] = $cols[ $od[0] ] . ' ' . $od[1];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.bticket DESC, a.bdate DESC';
    }

    $s = 'SELECT
            a.bid,
            a.bcode,
            a.agid,
            a.sagid,
            a.bticket,
            c.chcode,
            c.chname,
            a.bbname,
            a.bdate,
            a.bstatus,
            d.bdid,
            d.rid,
            d.bdfrom,
            d.bdto,
            d.bdfrom_id,
            d.bdto_id,
            d.bddate,
            d.bdtype,
            d.bddeparttime,
            d.bdarrivetime,
            a.bbrevtype,
            a.bbrevstatus,
            a.bbrevagent,
            a.bcreason,
            a.bcmessage,
            a.bbphone,
            a.brcdate,
            a.bonhandtotal,
            d.total,
            a.bbooking_staf,
            a.bbemail,
            d.num_adult,
            d.num_child,
            d.num_infant,
            d.bdstatus,
            d.bdpstatus,
            ( d.num_adult + d.num_child + d.num_infant ) AS pax
          FROM ticket_booking AS a
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          LEFT JOIN ticket_booking_detail AS d ON d.bid = a.bid
          WHERE a.bstt <> "ar" ' . $w . ' ORDER BY ' . $order;
    $r = $db->do_query( $s );
    $n = $db->num_rows( $r );

    $data = array();
    $surl = site_url();

    if( $n > 0 )
    {
        $adult  = 0;
        $child  = 0;
        $infant = 0;
        $pax    = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $agid    = empty( $d['agid'] ) ? $d['sagid'] : $d['agid'];
            $agname  = get_agent( $agid, 'agname' );

    	    if( isset( $bstatus ) )
    	    {
    	        if( ( is_array( $bstatus ) && in_array( 'cn', $bstatus ) ) || $bstatus == 'cn' )
        	    {
                    $pass  = ticket_passenger_list( $d['bdid'], false );
        	    }
    	    }

    	    if( isset( $filter['status'] ) )
    	    {
    	        if( ( is_array( $filter['status'] ) && in_array( 'cn', $filter['status'] ) ) || $filter['status'] == 'cn' )
    	        {
    	            $pass  = ticket_passenger_list( $d['bdid'], false );
        	    }
        	 	else
        	 	{
	        	    if( ( is_array( $filter['status'] ) && in_array( 'rf', $filter['status'] ) ) || $filter['status'] == 'rf' )
	        	    {
	        	        $pass  = ticket_passenger_list( $d['bdid'], false );
	        	    }
	        	    else
	        	    {
	        	        $pass = ticket_passenger_list( $d['bdid'], false, false );
	        	    }
        	 	}
    	    }
    	    else
    	    {
                $pass = ticket_passenger_list( $d['bdid'], false, false );

                if( empty( $pass ) )
                {
                    continue;
                }
    	    }

    	    if( empty( $pass ) )
            {
                continue;
            }

            $booked  = empty( $agid ) ? $d['bbname'] : $d['bbooking_staf'];
            $brcdate = $d['brcdate'] == '0000-00-00' ? '' : date( 'd M Y', strtotime( $d['brcdate'] ) );

            if( $csv )
            {
                $gname  = ticket_passenger_list_name( $pass, false, false, false );
                $trans  = ticket_booking_transport_detail( $d['bdid'], true );
                $btotal = $d['total'];
            }
            else
            {
                $gname  = ticket_passenger_list_name( $pass, true, true, false );
                $trans  = ticket_booking_transport_detail( $d['bdid'] );
                $btotal = number_format( $d['total'], 0, ',', '.' );
            }

            $data[] = array(
                'guest_name'  => $gname,
                'transport'   => $trans,
                'booked_by'   => $booked,
                'btotal'      => $btotal,
                'brcdate'     => $brcdate,
                'gpax'        => $d['pax'],
                'id'          => $d['bid'],
                'agid'        => $d['agid'],
                'ref_code'    => $d['bcode'],
                'phone'       => $d['bbphone'],
                'no_ticket'   => $d['bticket'],
                'bstatus'     => $d['bstatus'],
                'bbemail'     => $d['bbemail'],
                'bcreason'    => $d['bcreason'],
                'bcmessage'   => $d['bcmessage'],
                'rsv_type'    => $d['bbrevtype'],
                'gadult'      => $d['num_adult'],
                'gchild'      => $d['num_child'],
                'ginfant'     => $d['num_infant'],
                'rsv_status'  => $d['bbrevstatus'],
                'bonhandtotal'=> $d['bonhandtotal'],
                'ref_agent'   => $d['bbrevagent'] != '' ? $d['bbrevagent'] : '-',
                'route'       => $d['bdfrom'] . ' - ' . $d['bdto'],
                'status'      => $d['bdstatus'] == 'cn' && $d['bdpstatus'] != 'rf' ? 'Cancelled' : ticket_booking_status( $d['bdpstatus'] ),
                'chanel'      => empty( $agname ) ? $d['chname'] : $agname,
                'rev_date'    => date( 'd M Y', strtotime( $d['bdate'] ) ),
                'dept_date'   => date( 'd M Y', strtotime( $d['bddate'] ) ),
                // 'dept_time'   => date( 'H:i', strtotime( $d['bddeparttime'] ) ),
                // 'arrive_time' => date( 'H:i', strtotime( $d['bdarrivetime'] ) ),
                'dept_time'   => date( 'H:i', strtotime( get_route_time( $d['rid'],$d['bdfrom_id'],'depart' ) ) ),
                'arrive_time' => date( 'H:i', strtotime( get_route_time( $d['rid'],$d['bdto_id'],'arrival' ) ) ),
                'ticket_date' => $d['bticket'].'<br/><span style="font-size:10px;">('.date( 'd M Y', strtotime( $d['bdate'] ) ).')</span>',
            );

            $infant = $infant + $d['num_infant'];
            $adult  = $adult + $d['num_adult'];
            $child  = $child + $d['num_child'];
            $pax    = $pax + $d['pax'];
        }
    }

    return array( 'data' => $data, 'pax' => $pax, 'adult' => $adult, 'child' => $child, 'infant' => $infant );
}

function get_agent_reservation_list_report( $filter, $order )
{
    global $db;

    $w = '';

    extract( $filter );

    if( $chid != '' )
    {
        $w .= $db->prepare_query( ' AND c.chid = %d', $chid );
    }

    if( $lcid != '' )
    {
        $w .= $db->prepare_query( ' AND d.bdfrom = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid );
    }

    if( $lcid2 != '' )
    {
        $w .= $db->prepare_query( ' AND d.bdto = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid2 );
    }

    if( $rid != '' )
    {
        $w .= $db->prepare_query( ' AND d.rid = %d', $rid );
    }

    if( $date_start != '' && $date_end != '' )
    {
        $w .= $db->prepare_query( ' AND d.bddate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $date_start ) ), date( 'Y-m-d', strtotime( $date_end ) ) );
    }

    if( $sub == 'canceled' )
    {
        $w .= $db->prepare_query( ' AND a.bstatus = %s', 'cr' );
    }
    else
    {
        if( !empty( $status ) )
        {
            if( $status == 'pp' )
            {
                $w .= $db->prepare_query( ' AND a.bstatus IN( %s, %s )', 'pp', 'ol' );
            }
            else
            {
                $w .= $db->prepare_query( ' AND a.bstatus = %s', $status );
            }
        }
        else
        {
            $w .= $db->prepare_query( ' AND a.bstatus NOT IN( %s, %s )', 'ol', 'pf' );
        }
    }

    if( is_array( $order ) && !empty( $order ) )
    {
        $cols  = array(
            0  => 'a.bticket',
            1  => 'a.bbrevagent',
            5  => 'd.bddate',
            7  => 'd.bddeparttime',
            8  => 'd.bdarrivetime',
            10 => 'a.bbrevstatus'
        );

        $o = array();

        foreach( $order as $i => $od )
        {
            $o[] = $cols[ $od[0] ] . ' ' . $od[1];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.bticket DESC, a.bdate DESC';
    }

    $s = 'SELECT
            a.bid,
            a.bcode,
            a.agid,
            a.sagid,
            a.bticket,
            c.chcode,
            c.chname,
            a.bbname,
            a.bdate,
            a.bstatus,
            d.bdid,
            d.rid,
            d.bdfrom,
            d.bdto,
            d.bdfrom_id,
            d.bdto_id,
            d.bddate,
            d.bddeparttime,
            d.bdarrivetime,
            a.bbrevtype,
            a.bbrevstatus,
            a.bbrevagent,
            a.bcreason,
            a.bcmessage,
            a.bbphone,
            d.total,
            a.bbooking_staf,
            a.bbemail,
            d.num_adult,
            d.num_child,
            d.num_infant,
            ( d.num_adult + d.num_child + d.num_infant ) AS pax
          FROM ticket_booking AS a
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          LEFT JOIN ticket_booking_detail AS d ON d.bid = a.bid
          WHERE a.agid = %s AND a.bstt <> "ar" ' . $w . ' ORDER BY ' . $order;
    $q = $db->prepare_query( $s, $_COOKIE['agid'] );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    $data = array();
    $surl = site_url();

    if( $n > 0 )
    {
        $adult  = 0;
        $child  = 0;
        $infant = 0;
        $pax    = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $agid    = empty( $d['agid'] ) ? $d['sagid'] : $d['agid'];
            $agname  = get_agent( $agid, 'agname' );
            $pass    = ticket_passenger_list( $d['bdid'] );
            $booked  = empty( $agid ) ? $d['bbname'] : $d['bbooking_staf'];
            $gname   = ticket_passenger_list_name( $pass, true, true, false );
            $trans   = ticket_booking_transport_detail( $d['bdid'] );

            $data[] = array(
                'guest_name'  => $gname,
                'transport'   => $trans,
                'booked_by'   => $booked,
                'id'          => $d['bid'],
                'agid'        => $d['agid'],
                'ref_code'    => $d['bcode'],
                'phone'       => $d['bbphone'],
                'no_ticket'   => $d['bticket'],
                'bstatus'     => $d['bstatus'],
                'bbemail'     => $d['bbemail'],
                'bcreason'    => $d['bcreason'],
                'bcmessage'   => $d['bcmessage'],
                'rsv_type'    => $d['bbrevtype'],
                'rsv_status'  => $d['bbrevstatus'],
                'ref_agent'   => $d['bbrevagent'],
                'route'       => $d['bdfrom'] . ' - ' . $d['bdto'],
                'status'      => ticket_booking_status( $d['bstatus'] ),
                'chanel'      => empty( $agname ) ? $d['chname'] : $agname,
                'rev_date'    => date( 'd M Y', strtotime( $d['bdate'] ) ),
                'dept_date'   => date( 'd M Y', strtotime( $d['bddate'] ) ),
                'btotal'      => number_format( $d['total'], 0, ',', '.' ),
                // 'dept_time'   => date( 'H:i', strtotime( $d['bddeparttime'] ) ),
                // 'arrive_time' => date( 'H:i', strtotime( $d['bdarrivetime'] ) ),
                'dept_time'   => date( 'H:i', strtotime( get_route_time( $d['rid'], $d['bdfrom_id'], 'depart' ) ) ),
                'arrive_time' => date( 'H:i', strtotime( get_route_time( $d['rid'], $d['bdto_id'], 'arrival' ) ) )                
            );

            $infant = $infant + $d['num_infant'];
            $adult  = $adult + $d['num_adult'];
            $child  = $child + $d['num_child'];
            $pax    = $pax + $d['pax'];
        }
    }

    return array( 'data' => $data, 'pax' => $pax, 'adult' => $adult, 'child' => $child, 'infant' => $infant );
}

function get_passenger_list_accounting_report( $filter )
{
    global $db;

	$w = array();
	$b = '';

	extract( $filter );

    if( $boid != '' )
    {
        $w[] = $db->prepare_query( ' e.boid = %d', $boid );
    }

    if( $chids != '' )
    {
    	$arr = explode( '|', $chids );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

    	if( isset( $arr[1] ) )
    	{
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
    	}
    }

    if( $taid != '' )
    {
        $w[] = $db->prepare_query( ' g.taid = %d', $taid );
    }

    if( $rtype != '' && $bddate != '' )
    {
    	if( $rtype == '0'  )
    	{
    		$date = date( 'Y-m-d', strtotime( $bddate ) );

    		$w[] = $db->prepare_query( ' b.bddate = %s', $date );
    	}
    	elseif( $rtype == '1' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 ' . $bddate ) );

    		$w[] = $db->prepare_query( ' YEAR( b.bddate ) = YEAR( %s ) AND MONTH( b.bddate ) = MONTH( %s )', $date, $date );
    	}
    	elseif( $rtype == '2' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 Jan ' . $bddate ) );

    		$w[] = $db->prepare_query( ' YEAR( b.bddate ) = YEAR( %s )', $date );
    	}
    	elseif( $rtype == '3' )
    	{
    		$date_from = date( 'Y-m-d', strtotime( $bddate ) );
    		$date_to   = empty( $bddateto ) ? $date_from : date( 'Y-m-d', strtotime( $bddateto ) );

    		$w[] = $db->prepare_query( ' b.bddate BETWEEN %s AND %s', $date_from, $date_to );
    	}
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = ' AND ( ';

            foreach( $bstatus as $st )
            {
                if( in_array( $st, array( 'pa', 'ca', 'pp', 'cn', 'cr' ) ) )
                {
                    if( $st == 'cn' )
                    {
                        if( $st == $estatus )
                        {
                            $b .= $db->prepare_query( '( b.bdpstatus = %s AND a.bcancellationfee > 0 )', $st );
                        }
                        else
                        {
                            $b .= $db->prepare_query( '( b.bdpstatus = %s AND a.bcancellationfee > 0 ) OR ', $st );
                        }
                    }
                    else
                    {
                        if( $st == $estatus )
                        {
                            $b .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                        }
                        else
                        {
                            $b .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                        }
                    }
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' AND b.bdpstatus = %s', $bstatus );
        }
    }
    else
    {
        $b = $db->prepare_query( ' AND b.bdpstatus NOT IN( %s )', 'ol' );
    }

    if( $rid != '' )
    {
        if( is_array( $rid ) )
        {
            $estatus = end( $rid );

            $b .= ' AND ( ';

            foreach( $rid as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'b.rid = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.rid = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b .= $db->prepare_query( ' AND b.rid = %s', $rid );
        }
    }

	if( empty( $w ) && empty( $b ) )
	{
		$where = 'WHERE a.bstt <> "ar"';
	}
	elseif( empty( $w ) && !empty( $b ) )
	{
		$where = 'WHERE a.bstt <> "ar" ' . $b;
	}
	elseif( !empty( $w ) && empty( $b ) )
	{
		$where = 'WHERE a.bstt <> "ar" AND ' . implode( ' AND', $w );
	}
	else
	{
		$where = 'WHERE a.bstt <> "ar" ' . $b . ' AND ' . implode( ' AND', $w );
	}

	$s = 'SELECT
			a.bid,
			a.bremark,
			b.rid,
			a.chid,
			a.agid,
			c.rname,
			b.bdfrom_id,
			b.bdto_id,
			e.bocode,
			d.lcountry_id,
			d.bpname,
			d.bptype,
			d.bpstatus,
			b.bddate,
			a.bbname,
            a.bticket,
			b.bdtranstype,
			a.bbooking_staf,
			f.mname,
			b.discount,
			b.commission,
			b.commission_type,
			b.num_adult + b.num_child + b.num_infant AS num,
			CASE WHEN d.bptype = "adult" THEN b.price_per_adult
				 WHEN d.bptype = "child" THEN b.price_per_child
				 WHEN d.bptype = "infant" THEN b.price_per_infant
				 ELSE 0
			END AS prices,
			CASE WHEN d.bptype = "adult" THEN b.selling_price_per_adult
				 WHEN d.bptype = "child" THEN b.selling_price_per_child
				 WHEN d.bptype = "infant" THEN b.selling_price_per_infant
				 ELSE 0
			END AS selling_price,
            CASE WHEN d.bptype = "adult" THEN b.net_price_per_adult
                 WHEN d.bptype = "child" THEN b.net_price_per_child
                 WHEN d.bptype = "infant" THEN b.net_price_per_infant
                 ELSE 0
            END AS net_price,
			CASE WHEN d.bptype = "adult" THEN b.disc_price_per_adult
				 WHEN d.bptype = "child" THEN b.disc_price_per_child
				 WHEN d.bptype = "infant" THEN b.disc_price_per_infant
				 ELSE 0
			END AS disc_price
		  FROM ticket_booking AS a
		  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
		  LEFT JOIN ticket_route AS c ON b.rid = c.rid
		  LEFT JOIN ticket_boat AS e ON b.boid = e.boid
		  LEFT JOIN ticket_booking_passenger AS d ON d.bdid = b.bdid
		  LEFT JOIN ticket_booking_payment_method AS f ON a.bpaymethod = f.mid
		  LEFT JOIN ticket_booking_transport AS g ON b.bdid = g.bdid
		  ' . $where . '
		  GROUP BY d.bpid ORDER BY b.bddate ASC, a.bid ASC, d.bpid ASC, c.rdepart ASC';
	$r = $db->do_query( $s );

	if( is_array( $r ) )
	{
		return array();
	}
	else
	{
		$items = array();

		while( $d = $db->fetch_array( $r ) )
		{
			$from    = get_location( $d['bdfrom_id'], 'lctype' );
			$group   = $from == 1 ? 'return' : 'out';

			$country = get_country( $d['lcountry_id'], 'lcountry_code' );
			$index   = base64_encode( $d['bocode'] . '|' . strtotime( $d['bddate'] ) );

			if( empty( $d['agid'] ) )
			{
                if( $d['net_price'] == '0.00' )
                {
                    $prices  = $d['prices'] - $d['disc_price'];
                    $nprice  = $prices;
                }
                else
                {
                    $prices  = $d['prices'] - $d['disc_price'];
                    $nprice  = $d['net_price'];
                }

				$source  = get_channel( $d['chid'], 'chname' );
				$contact = empty( $d['bbooking_staf'] ) ? $d['bbname'] : $d['bbooking_staf'];
			}
			else
			{
				$agent   = get_agent( $d['agid'] );
				$nprice  = get_agent_net_price( $agent, $d );
				$prices  = $d['prices'] - $d['disc_price'];

				$source  = $agent['agname'];
				$contact = $d['bbooking_staf'];
			}

            if( $d['commission'] > 0 )
            {
                if( $d['commission_type'] == '0' )
                {
                    $commission = ( $d['prices'] * $d['commission'] ) / 100;
                }
                else
                {
                    $commission = $d['commission'];
                }
            }
            else
            {
                $commission = 0;
            }

			if( $d['bpstatus'] != 'cn' )
			{
    			$items[ $index ][ $group ][ $d['rname'] ][ $d['bticket'] ][] = array(
    				'bpmethod'   => $d['mname'],
    				'bremark'    => $d['bremark'],
    				'bpname'     => $d['bpname'],
    				'bptype'     => $d['bptype'],
                    'commission' => $commission,
    				'bcountry'   => $country,
    				'contact'    => $contact,
    				'bsource'    => $source,
                    'prices'     => $prices,
    				'nprices'    => $nprice
    			);
            }
		}

		return $items;
	}
}

function get_transport_revenue_report( $filter )
{
    global $db;

	$w  = '';
	$w2 = '';

	$b  = '';
	$b2 = '';

	extract( $filter );

    if( $type != '' )
    {
        $w  .= $db->prepare_query( ' AND e.type = %s', $type );
        $w2 .= $db->prepare_query( ' AND e2.type = %s', $type );
    }

    if( $rtype != '' && $ttdate != '' )
    {
    	if( $rtype == '0'  )
    	{
    		$date = date( 'Y-m-d', strtotime( $ttdate ) );

    		$w  .= $db->prepare_query( ' AND e.ttdate = %s', $date );
    		$w2 .= $db->prepare_query( ' AND e2.ttdate = %s', $date );
    	}
    	elseif( $rtype == '1' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 ' . $ttdate ) );

    		$w  .= $db->prepare_query( ' AND YEAR( e.ttdate ) = YEAR( %s ) AND MONTH( e.ttdate ) = MONTH( %s )', $date, $date );
    		$w2 .= $db->prepare_query( ' AND YEAR( e.ttdate ) = YEAR( %s ) AND MONTH( e.ttdate ) = MONTH( %s )', $date, $date );
    	}
    	elseif( $rtype == '2' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 Jan ' . $ttdate ) );

    		$w  .= $db->prepare_query( ' AND YEAR( e.ttdate ) = YEAR( %s )', $date );
    		$w2 .= $db->prepare_query( ' AND YEAR( e2.ttdate ) = YEAR( %s )', $date );
    	}
    	elseif( $rtype == '3' )
    	{
    		$date_from = date( 'Y-m-d', strtotime( $ttdate ) );
    		$date_to   = empty( $ttdateto ) ? $date_from : date( 'Y-m-d', strtotime( $ttdateto ) );

    		$w  .= $db->prepare_query( ' AND e.ttdate BETWEEN %s AND %s', $date_from, $date_to );
    		$w2 .= $db->prepare_query( ' AND e2.ttdate BETWEEN %s AND %s', $date_from, $date_to );
    	}
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b  = '( ';
            $b2 = '( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b  .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                    $b2 .= $db->prepare_query( 'b2.bdpstatus = %s', $st );
                }
                else
                {
                    $b  .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                    $b2 .= $db->prepare_query( 'b2.bdpstatus = %s OR ', $st );
                }
            }

            $b  .= ' )';
            $b2 .= ' )';
        }
        else
        {
            $b  = $db->prepare_query( ' b.bdpstatus = %s', $bstatus );
            $b2 = $db->prepare_query( ' b2.bdpstatus = %s', $bstatus );
        }
    }

	if( empty( $w ) && empty( $b ) )
	{
		$where  = '';
		$where2 = '';
	}
	elseif( empty( $w ) && !empty( $b ) )
	{
		$where  = 'AND ' . $b;
		$where2 = 'AND ' . $b2;
	}
	elseif( !empty( $w ) && empty( $b ) )
	{
		$where  = $w;
		$where2 = $w2;
	}
	else
	{
		$where  = 'AND ' . $b . $w;
		$where2 = 'AND ' . $b2 . $w2;
	}

	$s = 'SELECT
			a.bstatus,
			e.type,
			e.ttid,
			e.tttype,
			e.ttfee,
			e.ttdate,
			e.ttlocation,
			e.tttime,
			g.dname,
			f.vname,
			(
				SELECT IFNULL( SUM(e2.ttfee), 0 )
				FROM ticket_booking AS a2
				LEFT JOIN ticket_booking_detail AS b2 ON b2.bid = a2.bid
				LEFT JOIN ticket_booking_transport AS c2 ON c2.bdid = b2.bdid
				RIGHT JOIN ticket_trip_transport_detail AS d2 ON d2.btid = c2.btid
				LEFT JOIN ticket_trip_transport AS e2 ON d2.ttid = e2.ttid
				WHERE a2.bstt <> "ar" AND e2.tttype = "Private" AND e2.ttlocation = e.ttlocation ' . $where2 . '
			) AS total_by_location
		  FROM ticket_booking AS a
		  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
		  LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
		  RIGHT JOIN ticket_trip_transport_detail AS d ON d.btid = c.btid
		  LEFT JOIN ticket_trip_transport AS e ON d.ttid = e.ttid
		  LEFT JOIN ticket_transport_vehicle AS f ON e.vid = f.vid
		  LEFT JOIN ticket_transport_driver AS g ON e.did = g.did
		  WHERE a.bstt <> "ar" AND e.tttype = "Private" ' . $where;
	$r = $db->do_query( $s );

	if( is_array( $r ) )
	{
		return array( 'total' => 0, 'items' => array() );
	}
	else
	{
		$s2 = 'SELECT IFNULL( SUM( e.ttfee ), 0 ) AS total
			   FROM ticket_booking AS a
			   LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
			   LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
			   RIGHT JOIN ticket_trip_transport_detail AS d ON d.btid = c.btid
			   LEFT JOIN ticket_trip_transport AS e ON d.ttid = e.ttid
		  	   WHERE a.bstt <> "ar" AND e.tttype = "Private" ' . $where;
		$r2 = $db->do_query( $s2 );
		$d2 = $db->fetch_array( $r2 );

		$items   = array();
		$summary = isset( $d2['total'] ) ? $d2['total'] : 0;

		while( $d = $db->fetch_array( $r ) )
		{
			$index = base64_encode( json_encode( array( $d['ttlocation'], $d['total_by_location'] ) ) );

			$items[ $index ][] = array(
				'type' => $d['type'],
				'tttype' => $d['tttype'],
				'vname' => $d['vname'],
				'dname' => $d['dname'],
				'ttdate' => $d['ttdate'],
				'ttlocation' => $d['ttlocation'],
				'tttime' => $d['tttime'],
				'ttfee' => $d['ttfee']
			);
		}

		return array( 'total' => $summary, 'items' => $items );
	}
}

function get_transport_list_report( $filter )
{
    global $db;

    $w = array();
    $h = '';
    $b = '';

    $fltr = base64_encode( json_encode( $filter ) );

    extract( $filter );

    if( $taid != '' )
    {
        $h = $db->prepare_query( ' HAVING c.taid = %d', $taid );
    }

    if( $rid != '' )
    {
        $w[] = $db->prepare_query( ' d.rid = %d', $rid );
    }

    if( $ttype != '' )
    {
        $w[] = $db->prepare_query( ' a.tttype = %s', ( $ttype == 0 ? 'Shared' : 'Private' ) );
    }

    if( $rtype != '' && $ttdate != '' )
    {
        if( $rtype == '0'  )
        {
            $date = date( 'Y-m-d', strtotime( $ttdate ) );

            $w[] = $db->prepare_query( ' a.ttdate = %s', $date );
        }
        elseif( $rtype == '1' )
        {
            $date = date( 'Y-m-d', strtotime( '1 ' . $ttdate ) );

            $w[] = $db->prepare_query( ' YEAR( a.ttdate ) = YEAR( %s ) AND MONTH( a.ttdate ) = MONTH( %s )', $date, $date );
        }
        elseif( $rtype == '2' )
        {
            $date = date( 'Y-m-d', strtotime( '1 Jan ' . $ttdate ) );

            $w[] = $db->prepare_query( ' YEAR( a.ttdate ) = YEAR( %s )', $date );
        }
        elseif( $rtype == '3' )
        {
            $date_from = date( 'Y-m-d', strtotime( $ttdate ) );
            $date_to   = empty( $ttdateto ) ? $date_from : date( 'Y-m-d', strtotime( $ttdateto ) );

            $w[] = $db->prepare_query( ' a.ttdate BETWEEN %s AND %s', $date_from, $date_to );
        }
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b .= ' AND ( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'd.bdpstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'd.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b .= $db->prepare_query( ' AND d.bdpstatus = %s', $bstatus );
        }
    }

    if( empty( $w ) && empty( $b ) )
    {
        $where  = 'WHERE e.bstt <> "ar"';
    }
    elseif( empty( $w ) && !empty( $b ) )
    {
        $where  = 'WHERE e.bstt <> "ar" ' . $b;
    }
    elseif( !empty( $w ) && empty( $b ) )
    {
        $where  = 'WHERE e.bstt <> "ar" AND ' . implode( ' AND', $w );
    }
    else
    {
        $where  = 'WHERE e.bstt <> "ar" ' . $b . ' AND ' . implode( ' AND', $w );
    }

    $s = 'SELECT
            a.type,
            a.ttid,
            a.tttype,
            e.bid,
            e.bticket,
            d.bdid,
            d.bdfrom,
            d.bdto,
            d.bdtype,
            e.bbname,
            e.agid,
            e.bstatus,
            f.vname,
            g.dname,
            g.dphone,
            c.taid,
            c.bttrans_type,
            c.bthotelname,
            c.bthoteladdress,
            c.bthotelphone,
            c.btdrivername,
            c.btdriverphone,
            h.taname
          FROM ticket_trip_transport AS a
          LEFT JOIN ticket_trip_transport_detail AS b ON b.ttid = a.ttid
          LEFT JOIN ticket_booking_transport AS c ON b.btid = c.btid
          LEFT JOIN ticket_booking_detail AS d ON c.bdid = d.bdid
          LEFT JOIN ticket_booking AS e ON d.bid = e.bid
          LEFT JOIN ticket_transport_vehicle AS f ON f.vid = a.vid
          LEFT JOIN ticket_transport_driver AS g ON g.did = a.did
          LEFT JOIN ticket_transport_area AS h ON c.taid = h.taid
          ' . $where . ' GROUP BY b.ttdid ' . $h . ' ORDER BY a.ttid';
    $r = $db->do_query( $s );

    $items  = array();
    $data   = array();
    $dtarr  = array();
    $rcount = $db->num_rows( $r );

    $category = array();
    $series   = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $group  = base64_encode( json_encode( array( $d['bdfrom'], $d['bdto'] ) ) );
        $taname = empty( $d['taname'] ) ? 'No accommodation' : $d['taname'];

        if( $d['bttrans_type'] == '2' )
        {
            $dphone = $d['btdriverphone'];
            $dname  = $d['btdrivername'];
            $vname = '-';
        }
        else
        {
            $dphone = $d['dphone'];
            $dname  = $d['dname'];
            $vname  = $d['vname'];
        }

        $data[ $taname ][ $group ][] = array(
            'bdid'        => $d['bdid'],
            'bid'         => $d['bid'],
            'agid'        => $d['agid'],
            'type'        => $d['type'],
            'bdto'        => $d['bdto'],
            'bbname'      => $d['bbname'],
            'bdtype'      => $d['bdtype'],
            'tttype'      => $d['tttype'],
            'bdfrom'      => $d['bdfrom'],
            'bticket'     => $d['bticket'],
            'bstatus'     => $d['bstatus'],
            'hname'       => $d['bthotelname'],
            'hphone'      => $d['bthotelphone'],
            'hphone2'     => $d['bthotelphone'],
            'haddress'    => $d['bthoteladdress'],
            'bdtranstype' => $d['bttrans_type'],
            'taname'      => $taname,
            'vname'       => $vname,
            'dname'       => $dname,
            'dphone'      => $dphone
        );

        if( isset( $dtarr[ $taname ] ) )
        {
            $dtarr[ $taname ] += 1;
        }
        else
        {
            $dtarr[ $taname ] = 1;
        }
    }

    foreach( $data as $obj => $groups )
    {
        $total_passenger = $dtarr[ $obj ];
        $total_percent   = round( ( $total_passenger * 100 ) / $rcount, 2 );
        $index           = base64_encode( json_encode( array( $obj, $total_passenger, $total_percent ) ) );

        $tblcontent = '';
        $category[] = $obj;

        foreach( $groups as $location => $group )
        {
            list( $from, $to ) = json_decode( base64_decode( $location ) );

            $num = count( $group );
            $prc = round( ( $num * 100 ) / $total_passenger, 2 );

            $series[ $from . ' - ' . $to ][  $obj ][] = $num;

            $grp   = base64_encode( json_encode( array( $from, $to, $num, $prc ) ) );

            foreach ( $group as $d )
            {
                $bbname  = empty( $d['agid'] ) ? $d['bbname'] : get_agent( $d['agid'], 'agname' );
                $pass    = ticket_passenger_list( $d['bdid'] );
                $gname   = ticket_passenger_list_name( $pass );

                $items[ $index ][ $grp ][] = array(
                    'bbname'      => $bbname,
                    'gname'       => $gname,
                    'hname'       => empty( $d['hname'] ) ? '-' : $d['hname'],
                    'haddress'    => empty( $d['haddress'] ) ? '-' : $d['haddress'],
                    'bid'         => $d['bid'],
                    'hphone'      => empty( $d['hphone'] ) ? $d['hphone2'] : $d['hphone'],
                    'tttype'      => $d['tttype'],
                    'type'        => ucwords( $d['type'] ),
                    'bdid'        => $d['bdid'],
                    'bticket'     => $d['bticket'],
                    'vname'       => $d['vname'],
                    'dname'       => $d['dname'],
                    'phone'       => $d['dphone']
                );
            }
        }
    }

    if ( $rcount == 0 )
    {
        return array( 'total' => $rcount );
    }
    else
    {
        return array( 'total' => number_format( $rcount, 0, '.' , '.' ), 'chart' => get_chart_options( $category, $series ), 'items' => $items, 'filter' => $fltr );
    }
}


function get_total_transport_by_area( $taid, $where = array() )
{
	global $db;

	if( empty( $where ) )
	{
		$s = 'SELECT e.taid
			  FROM ticket_trip_transport AS a
			  LEFT JOIN ticket_trip_transport_detail AS b ON b.ttid = a.ttid
			  LEFT JOIN ticket_booking_transport AS e ON e.btid = b.btid
			  LEFT JOIN ticket_booking_detail AS d ON d.bdid = e.bdid
			  GROUP BY b.ttdid HAVING e.taid = %s';
	}
	else
	{
		$s = 'SELECT e.taid
			  FROM ticket_trip_transport AS a
			  LEFT JOIN ticket_trip_transport_detail AS b ON b.ttid = a.ttid
			  LEFT JOIN ticket_booking_transport AS e ON e.btid = b.btid
			  LEFT JOIN ticket_booking_detail AS d ON d.bdid = e.bdid
			  WHERE ' . implode( ' AND', $where ) . '
			  GROUP BY b.ttdid HAVING e.taid = %s';
	}

    $q = $db->prepare_query( $s, $taid );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

function get_daily_reservation_report( $filter )
{
    global $db;

	extract( $filter );

	$w = array();
	$b = '';

    if( ticket_is_report_filter_view() )
	{
        if( $rid != '' )
        {
            if( is_array( $rid ) )
            {
                $w[]  = $db->prepare_query( ' a.rid IN( "' . implode( '", "', $rid ) . '" )' );
            }
            else
            {
                $w[]  = $db->prepare_query( ' a.rid = %d', $rid );
            }
        }

        if( $chid != '' )
        {
            $w[]  = $db->prepare_query( ' e.chid = %d', $chid );
        }

        if( $rtype != '' && $bdate != '' )
        {
        	if( $rtype == '0'  )
        	{
        		$date_from = date( 'Y-m-d', strtotime( $bdate ) );
        		$date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

        		$w[] = $db->prepare_query( ' a.bddate BETWEEN %s AND %s', $date_from, $date_to );
        	}
        	elseif( $rtype == '1' )
        	{
        		$date = date( 'Y-m-d', strtotime( '1 ' . $bdate ) );

        		$w[]  = $db->prepare_query( ' YEAR( a.bddate ) = YEAR( %s ) AND MONTH( a.bddate ) = MONTH( %s )', $date, $date );
        	}
        	elseif( $rtype == '2' )
        	{
        		$date = date( 'Y-m-d', strtotime( '1 Jan ' . $bdate ) );

        		$w[]  = $db->prepare_query( ' YEAR( a.bddate ) = YEAR( %s )', $date );
        	}
        	elseif( $rtype == '3' )
        	{
        		$date_from = date( 'Y-m-d', strtotime( $bdate ) );
        		$date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

        		$w[] = $db->prepare_query( ' a.bddate BETWEEN %s AND %s', $date_from, $date_to );
        	}
        }

        if( $bstatus != '' )
        {
            if( is_array( $bstatus ) )
            {
                $estatus = end( $bstatus );

                $b .= '( ';

                foreach( $bstatus as $st )
                {
                    if( $st == $estatus )
                    {
                        $b .= $db->prepare_query( 'a.bdpstatus = %s', $st );
                    }
                    else
                    {
                        $b .= $db->prepare_query( 'a.bdpstatus = %s OR ', $st );
                    }
                }

                $b .= ' )';
            }
            else
            {
	            $b .= $db->prepare_query( 'a.bdpstatus = %s', $bstatus );
            }
        }
	}

	if( empty( $w ) && !empty( $b ) )
	{
		$where  = 'AND ' . $b;
	}
	elseif( !empty( $w ) && empty( $b ) )
	{
		$where  = 'AND ' . implode( ' AND', $w );
	}
	else
	{
		$where  = 'AND ' . $b . ' AND ' . implode( ' AND', $w );
	}

	$s = 'SELECT
			a.bdfrom,
			a.bdto,
			a.bddate,
			a.num_adult + a.num_child + a.num_infant AS num,
			b.lcalias AS falias,
			c.lcalias AS talias,
			b.lctype
		  FROM ticket_booking_detail AS a
		  LEFT JOIN ticket_location AS b ON a.bdfrom = b.lcname
		  LEFT JOIN ticket_location AS c ON a.bdto = c.lcname
		  LEFT JOIN ticket_booking AS d ON a.bid = d.bid
		  LEFT JOIN ticket_channel AS e ON d.chid = e.chid
		  WHERE d.bstt <> "ar" ' . $where . ' ORDER BY b.lctype ASC, a.bdfrom ASC, a.bddate ASC';
	$r = $db->do_query( $s );

	if( is_array( $r ) )
	{
		return array();
	}
	else
	{
		$items = array();

		while( $d = $db->fetch_array( $r ) )
		{
			$index = $d['bddate'];
			$group = $d['falias'] . '-' . $d['talias'];

			$items[ $index ][ $group ][] = array(
				'bdfrom' => $d['bdfrom'],
				'bdto' => $d['bdto'],
				'bddate' => $d['bddate'],
				'bdnum' => $d['num']
			);
		}

		return $items;
	}
}

function get_daily_pax_numbers_report( $filter )
{
    global $db;

    $fltr = base64_encode( json_encode( $filter ) );

	extract( $filter );

	$w = array();
	$b = '';
    $t = '';

    if( $rid != '' )
    {
        if( is_array( $rid ) )
        {
            $erid = end( $rid );

            $t = '( ';

            foreach( $rid as $rd )
            {
                if( $rd == $erid )
                {
                    $t .= $db->prepare_query( 'b.rid = %d', $rd );
                }
                else
                {
                    $t .= $db->prepare_query( 'b.rid = %d OR ', $rd );
                }
            }

            $t .= ' )';
        }
        else
        {
            $t = $db->prepare_query( ' b.rid = %d', $rid );
        }
    }

    if( $chid != '' )
    {
    	$arr = explode( '|', $chid );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

    	if( isset( $arr[1] ) )
    	{
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
    	}
    }

    if( $rtype != '' && $bdate != '' )
    {
    	if( $rtype == '3' )
    	{
    		$date_from = date( 'Y-m-d', strtotime( $bdate ) );
			$date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

			$w[] = $db->prepare_query( ' b.bddate BETWEEN %s AND %s', $date_from, $date_to );
		}
		else
		{
			$date_from = date( 'Y-m-d', strtotime( $bdate ) );

			$w[] = $db->prepare_query( ' b.bddate = %s', $date_from );
		}
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = '( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' b.bdpstatus = %s', $bstatus );
        }
    }

	if( empty( $w ) && empty( $b ) && empty( $t ) )
	{
		$where = 'AND b.bdpstatus NOT IN ("cn","bc","pf","rf")';
	}
    elseif( empty( $w ) && !empty( $b ) && !empty( $t ) )
    {
        $where = 'AND ' . $b . ' AND ' . $t;
    }
	elseif( empty( $w ) && !empty( $b ) && empty( $t ) )
	{
		$where = 'AND ' . $b;
	}
    elseif( empty( $w ) && empty( $b ) && !empty( $t ) )
    {
        $where = 'AND ' . $t;
    }
	elseif( !empty( $w ) && empty( $b ) && empty( $t ) )
	{
		$where = 'AND b.bdpstatus NOT IN ("cn","bc","pf","rf") AND ' . implode( ' AND', $w );
	}
    elseif( !empty( $w ) && !empty( $b ) && empty( $t ) )
    {
        $where = 'AND ' . $b . ' AND ' . implode( ' AND', $w );
    }
    elseif( !empty( $w ) && empty( $b ) && !empty( $t ) )
    {
        $where = 'AND ' . $t . ' AND ' . implode( ' AND', $w );
    }
	else
	{
		$where = 'AND ' . $b . ' AND ' . $t . ' AND ' . implode( ' AND', $w );
	}

	$q = 'SELECT
			a.bid,
			a.agid,
			a.bdate,
			a.bticket,
			b.bddate,
			b.bdfrom,
			b.bdto,
			b.num_adult,
			b.num_child,
			b.num_infant,
			c.chid,
			c.chcode,
			c.chname,
			b.bdtype,
			b.num_adult + b.num_child + b.num_infant AS amount
		  FROM ticket_booking AS a
		  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
		  LEFT JOIN ticket_channel AS c ON a.chid = c.chid
		  WHERE a.bstt <> "ar" ' . $where . ' ORDER BY b.bddate, b.bdtype, b.bdfrom, b.bdto ASC';
	$r = $db->do_query( $q );

	$items  = array();
	$data   = array();
	$datas  = array();
	$darray = array();
	$larray = array();
	$rarray = array();
	$carray = array();
	$tarray = array();

	while( $d = $db->fetch_array( $r ) )
	{
        $d['bddate'] = date( 'd F Y', strtotime( $d['bddate'] ) );

		$rarray[] = 1;
		$data[]   = $d;
		$tarray[] = $d['amount'];

		$darray[ $d['bddate'] ][] = 1;
		$larray[ $d['bddate'] ][ $d['bdfrom'] . ' - ' . $d['bdto'] ][] = 1;
	}

	$rcount = array_sum( $rarray );
	$tcount = array_sum( $tarray );

	foreach( $data as $d )
	{
		$dtotal = array_sum( $darray[ $d['bddate'] ] );
		$ltotal = array_sum( $larray[ $d['bddate'] ][ $d['bdfrom'] . ' - ' . $d['bdto'] ] );

		$agname  = get_agent( $d['agid'], 'agname' );
		$bsource = empty( $agname ) ? '( ' . $d['chcode'] . ' ) ' . $d['chname'] : $agname;

		$child   = base64_encode( json_encode( array( $d['chcode'], $d['chname'] ) ) );
		$index   = base64_encode( json_encode( array( $d['bddate'], $dtotal, round( ( $dtotal * 100 ) / $rcount, 2 ) ) ) );
		$group   = base64_encode( json_encode( array( $d['bdfrom'], $d['bdto'], $ltotal, round( ( $ltotal * 100 ) / $dtotal, 2 ) ) ) );

		$datas[ $index ][ $group ][ $child ][] = array(
			'bsource'    => $bsource,
			'bid'        => $d['bid'],
			'chid'       => $d['chid'],
			'bdto'       => $d['bdto'],
			'bdate'      => $d['bdate'],
			'amount'     => $d['amount'],
			'bdfrom'     => $d['bdfrom'],
			'chcode'     => $d['chcode'],
			'chname'     => $d['chname'],
			'bticket'    => $d['bticket'],
			'num_adult'  => $d['num_adult'],
			'num_child'  => $d['num_child'],
			'num_infant' => $d['num_infant']
		);
	}

	$category = array();
    $series   = array();

    $idx 	  = array();
    $grp 	  = array();
    $chl 	  = array();

	foreach( $datas as $obj => $groups )
    {
    	list( $rev_date, $total_rev, $total_rev_percent ) = json_decode( base64_decode( $obj ) );

		$category[] = $rev_date;

		$idx = base64_encode( json_encode( array( $rev_date, number_format( $total_rev, 0, '.' , '.' ), $total_rev_percent ) ) );

		foreach( $groups as $location => $group )
		{
			list( $from, $to, $total_rev_by_loc, $total_percent_rev_by_loc ) = json_decode( base64_decode( $location ) );

			$series[ $from . ' - ' . $to ][ $rev_date ][] = $total_rev_by_loc;

			$grp = base64_encode( json_encode( array( $from, $to, number_format( $total_rev_by_loc, 0, '.' , '.' ), $total_percent_rev_by_loc ) ) );

			foreach( $group as $channel => $child )
			{
				list( $chcode, $chname ) = json_decode( base64_decode( $channel ) );

				$num = count( $child );
				$prc = round( ( $num * 100 ) / $total_rev_by_loc, 2 );
				$chl = base64_encode( json_encode( array( $chcode, $chname, $num, $prc ) ) );

				foreach( $child as $d )
				{
					$items[ $idx ][ $grp ][ $chl ][] = array(
						'bid'        => $d['bid'],
						'bticket'    => $d['bticket'],
						'amount'     => $d['amount'],
						'bsource'    => $d['bsource'],
						'num_adult'  => $d['num_adult'],
						'num_child'  => $d['num_child'],
						'num_infant' => $d['num_infant']
					);
				}
			}
		}

	}

    if ( $rcount == 0 )
    {
        return array( 'total' => $rcount );
    }
    else
    {
        return array( 'total' => number_format( $rcount, 0, '.' , '.' ), 'total_pax' => number_format( $tcount, 0, '.' , '.' ), 'chart' => get_chart_options( $category, $series ), 'items' => $items, 'filter' => $fltr );
    }
}

function get_reservation_timeline_report( $filter )
{
    global $db;

    $fltr = base64_encode( json_encode( $filter ) );

    extract( $filter );

    $w     = array();
    $b     = '';
    $sdate = $rbased == '1' ? 'd.bddate' : 'b.bdate';

    if ( $cid != '' )
    {
        $w[] = $db->prepare_query( 'b.chid = %s', $cid );
    }

    if ( $rtype != '' )
    {
        if ( $rtype == '1' )
        {
            $w[]  = $db->prepare_query( ' YEAR( ' . $sdate . ' ) = YEAR( %s ) AND MONTH( ' . $sdate . ' ) = MONTH( %s )', date( 'Y-m-d', strtotime( $bdate ) ), date( 'Y-m-d', strtotime( $bdate ) ) );
        }
        elseif( $rtype == '2' )
        {
            $bdate = date( 'Y-m-d', strtotime( '1 Jan ' . $bdate ) );
            $w[]    = $db->prepare_query( ' YEAR( ' . $sdate . ' ) = YEAR( %s )', $bdate );
        }
        elseif( $rtype == '3' )
        {
            if( $bdate != '' && $bdateto != '' )
            {
                $w[]  = $db->prepare_query( $sdate . ' BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $bdate ) ), date( 'Y-m-d', strtotime( $bdateto ) ) );
            }
        }
    }
    else
    {
        $w[] = $db->prepare_query( $sdate . ' >= DATE(CONVERT_TZ(NOW(),@@session.time_zone,"+08:00"))' );
    }

    if( $chid != '' )
    {
        $arr = explode( '|', $chid );

        $w[] = $db->prepare_query( ' b.chid = %d', $arr[0] );

        if( isset( $arr[1] ) )
        {
            $w[] = $db->prepare_query( ' b.agid = %d', $arr[1] );
        }
    }

    if( $rid != '' )
    {
        $w[] = $db->prepare_query( 'd.rid = %d', $rid );
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = '( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'd.bdpstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'd.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( 'd.bdpstatus = %s', $bstatus );
        }

        $w[] = $b;
    }
    else
    {
        $b = $db->prepare_query( 'd.bdpstatus NOT IN("cn","pf","bc","rf","ol")' );

        $w[] = $b;
    }

    $where = '';

    if ( !empty( $w ) )
    {
        $where = implode( ' AND ' , $w );
    }

    $s = 'SELECT
            b.bid,
            b.bticket,
            b.agid,
            b.bbname,
            b.bdate,
            d.bddate,
            d.bdid,
            d.bdfrom,
            d.bdto,
            d.num_adult,
            d.num_child,
            d.num_infant,
            d.bdpstatus,
            TIMESTAMPDIFF(DAY, b.bdate, d.bddate) AS daily,
            TIMESTAMPDIFF(WEEK, b.bdate, d.bddate) AS weekly,
            TIMESTAMPDIFF(MONTH, b.bdate, d.bddate) AS montly,
            SUM( d.num_adult + d.num_child + d.num_infant ) AS tpax
         FROM ticket_booking AS b
         LEFT JOIN ticket_booking_detail AS d ON b.bid = d.bid
         WHERE b.bstt <> "ar" AND ' . $where . ' GROUP BY d.bdid ORDER BY daily, d.bddate ASC';
    $r = $db->do_query( $s );
    $n = $db->num_rows( $r );

    $data     = array();
    $tbooking = 0;
    $tpax    = 0;

    if ( $n > 0 )
    {
        $dt = array();
        $ar = array( '1 - 3 Days Ahead' => 0, '4 - 6 Days Ahead' => 0, '1 Week - 1 Month Ahead' => 0, '1 Month - 3 Months Ahead' => 0, 'More Than 3 Months Ahead' => 0 );

        while ( $d = $db->fetch_array( $r ) )
        {
            if ( $d['daily'] >= 0 )
            {
                $tbooking++;

                if ( $d['daily'] >= 0 && $d['daily'] <= 3 )
                {
                    $ar['1 - 3 Days Ahead'] += $d['tpax'];

                    $dt['1 - 3 Days Ahead'][] = array(
                        'code'  => $d['bticket'],
                        'agent' => $d['agid'] != '' ? get_agent( $d['agid'], 'agname' ) : $d['bbname'],
                        'pax'   => $d['tpax'],
                        'bdate' => date( 'd M Y', strtotime( $d['bdate'] ) ),
                        'dtrip' => date( 'd M Y', strtotime( $d['bddate'] ) ),
                        'trip'  => $d['bdfrom'] . ' - ' . $d['bdto'],
                        'adult' => $d['num_adult'] != 0 ? $d['num_adult'] : '-',
                        'child' => $d['num_child'] != 0 ? $d['num_child'] : '-',
                        'infant' => $d['num_infant'] != 0 ? $d['num_infant'] : '-',
                        'status' => $d['bdpstatus']
                    );
                }
                elseif( $d['daily'] > 3 && $d['daily'] <= 6 )
                {
                    $ar['4 - 6 Days Ahead'] += $d['tpax'];

                    $dt['4 - 6 Days Ahead'][] = array(
                        'code'  => $d['bticket'],
                        'agent' => $d['agid'] != '' ? get_agent( $d['agid'], 'agname' ) : $d['bbname'],
                        'pax'   => $d['tpax'],
                        'bdate' => date( 'd M Y', strtotime( $d['bdate'] ) ),
                        'dtrip' => date( 'd M Y', strtotime( $d['bddate'] ) ),
                        'trip'  => $d['bdfrom'] . ' - ' . $d['bdto'],
                        'adult' => $d['num_adult'] != 0 ? $d['num_adult'] : '-',
                        'child' => $d['num_child'] != 0 ? $d['num_child'] : '-',
                        'infant' => $d['num_infant'] != 0 ? $d['num_infant'] : '-',
                        'status' => $d['bdpstatus']
                    );
                }
                elseif( $d['daily'] > 6 && $d['montly'] < 1  )
                {
                    $ar['1 Week - 1 Month Ahead'] += $d['tpax'];

                    $dt['1 Week - 1 Month Ahead'][] = array(
                        'code'  => $d['bticket'],
                        'agent' => $d['agid'] != '' ? get_agent( $d['agid'], 'agname' ) : $d['bbname'],
                        'pax'   => $d['tpax'],
                        'bdate' => date( 'd M Y', strtotime( $d['bdate'] ) ),
                        'dtrip' => date( 'd M Y', strtotime( $d['bddate'] ) ),
                        'trip'  => $d['bdfrom'] . ' - ' . $d['bdto'],
                        'adult' => $d['num_adult'] != 0 ? $d['num_adult'] : '-',
                        'child' => $d['num_child'] != 0 ? $d['num_child'] : '-',
                        'infant' => $d['num_infant'] != 0 ? $d['num_infant'] : '-',
                        'status' => $d['bdpstatus']
                    );
                }
                elseif( $d['montly'] >= 1 && $d['montly'] <= 3 )
                {
                    $ar['1 Month - 3 Months Ahead'] += $d['tpax'];

                    $dt['1 Month - 3 Months Ahead'][] = array(
                        'code'  => $d['bticket'],
                        'agent' => $d['agid'] != '' ? get_agent( $d['agid'], 'agname' ) : $d['bbname'],
                        'pax'   => $d['tpax'],
                        'bdate' => date( 'd M Y', strtotime( $d['bdate'] ) ),
                        'dtrip' => date( 'd M Y', strtotime( $d['bddate'] ) ),
                        'trip'  => $d['bdfrom'] . ' - ' . $d['bdto'],
                        'adult' => $d['num_adult'] != 0 ? $d['num_adult'] : '-',
                        'child' => $d['num_child'] != 0 ? $d['num_child'] : '-',
                        'infant' => $d['num_infant'] != 0 ? $d['num_infant'] : '-',
                        'status' => $d['bdpstatus']
                    );
                }
                elseif( $d['montly'] > 3 )
                {
                    $ar['More Than 3 Months Ahead'] += $d['tpax'];

                    $dt['More Than 3 Months Ahead'][] = array(
                        'code'  => $d['bticket'],
                        'agent' => $d['agid'] != '' ? get_agent( $d['agid'], 'agname' ) : $d['bbname'],
                        'pax'   => $d['tpax'],
                        'bdate' => date( 'd M Y', strtotime( $d['bdate'] ) ),
                        'dtrip' => date( 'd M Y', strtotime( $d['bddate'] ) ),
                        'trip'  => $d['bdfrom'] . ' - ' . $d['bdto'],
                        'adult' => $d['num_adult'] != 0 ? $d['num_adult'] : '-',
                        'child' => $d['num_child'] != 0 ? $d['num_child'] : '-',
                        'infant' => $d['num_infant'] != 0 ? $d['num_infant'] : '-',
                        'status' => $d['bdpstatus']
                    );
                }
            }
        }

        foreach ( $dt as $idx => $vals )
        {
            $index = base64_encode( json_encode( array( $idx, count( $dt[$idx] ), $ar[$idx], round( ( count( $dt[$idx] ) * 100 ) / $tbooking, 2 ) ) ) );

            foreach ( $vals as $key => $val )
            {
                $tpax += $val['pax'];

                $data[ $index ][] = array(
                    'code'  => $val['code'],
                    'agent' => $val['agent'],
                    'pax'   => $val['pax'],
                    'bdate' => $val['bdate'],
                    'dtrip' => $val['dtrip'],
                    'trip'  => $val['trip'],
                    'adult' => $val['adult'],
                    'child' => $val['child'],
                    'infant' => $val['infant'],
                    'status' => ticket_booking_status( $val['status'] )
                );
            }
        }
    }

    $category = array();
    $series   = array();

    foreach ( $data as $x => $vals )
    {
        list( $key, $count, $pax, $percent ) = json_decode( base64_decode( $x ) );
        $category[] = $key;

        $series[$key][$key][] = $count;
    }

    return array( 'items' => $data, 'total' => $tpax, 'tbooking' => $tbooking, 'chart' => get_chart_options( $category, $series, 'bar', false ), 'filter' => $fltr );
}

function get_summary_addons_report( $filter )
{
    global $db;

    $fltr = base64_encode( json_encode( $filter ) );

    extract( $filter );

    //-- Main Filter
    $w = array();

    if( $lcid != '' && $lcid2 != '' )
    {
        $w[] = $db->prepare_query( '( b.bdfrom_id = %d OR b.bdto_id = %d )', $lcid, $lcid2 );
    }
    elseif( $lcid != '' )
    {
        $w[] = $db->prepare_query( 'b.bdfrom_id = %d', $lcid );
    }
    elseif( $lcid2 != '' )
    {
        $w[] = $db->prepare_query( 'b.bdto_id = %d', $lcid2 );
    }

    if( $rtype != '' && $bdate != '' )
    {
        if( $rtype == '1' )
        {
            $date = date( 'Y-m-d', strtotime( '1 ' . $bdate ) );

            $w[] = $db->prepare_query( ' YEAR( b.bddate ) = YEAR( %s ) AND MONTH( b.bddate ) = MONTH( %s )', $date, $date );
        }
        elseif( $rtype == '2' )
        {
            $date = date( 'Y-m-d', strtotime( '1 Jan ' . $bdate ) );

            $w[] = $db->prepare_query( ' YEAR( b.bddate ) = YEAR( %s )', $date );
        }
        elseif( $rtype == '3' )
        {
            $date_from = date( 'Y-m-d', strtotime( $bdate ) );
            $date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

            $w[] = $db->prepare_query( ' b.bddate BETWEEN %s AND %s', $date_from, $date_to );
        }
    }

    if( empty( $w ) )
    {
        $where = '';
    }
    else
    {
        $where = 'AND ' . implode( ' AND', $w );
    }

    $q = 'SELECT
            c.bid,
            b.bdto,
            d.aoname,
            e.agname,
            f.chcode,
            f.chname,
            b.bdfrom,
            a.baoprice,
            b.bddeparttime,
            SUM( a.baopax ) AS baopax,
            FORMAT( a.baoprice, 0 ) AS bfaoprice,
            ( SUM( a.baopax ) * a.baoprice ) AS baototal,
            DATE_FORMAT( b.bddate, "%M, %Y") AS bydate,
            DATE_FORMAT( b.bddate, "%d %M, %Y") AS bdate,
            DATE_FORMAT( b.bddate, "%d %M, %Y") AS bmydate,
            FORMAT( ( SUM( a.baopax ) * a.baoprice ), 0 ) AS bfaototal,
            IF( b.bdtype = "return", "Arrival", "Departure") AS bdtype,
            CONCAT( b.bdfrom, " (", DATE_FORMAT( b.bddeparttime, "%H:%i"), ")" ) AS trip
          FROM ticket_booking_add_ons AS a
          LEFT JOIN ticket_booking_detail AS b ON a.bdid = b.bdid
          LEFT JOIN ticket_booking AS c ON b.bid = c.bid
          LEFT JOIN ticket_add_ons AS d ON a.aoid = d.aoid
          LEFT JOIN ticket_agent AS e ON c.agid = e.agid
          LEFT JOIN ticket_channel AS f ON c.chid = f.chid
          WHERE c.bstt <> "ar" AND b.bdstatus <> "cn"
          AND b.bdpstatus NOT IN( "pf", "cn" ) ' . $where . '
          GROUP BY b.sid, f.chid, d.aoid
          ORDER BY c.bdate ASC';
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return array( 'total' => 0 );
    }
    else
    {
        $items  = array();
        $data   = array();
        $darray = array();
        $tarray = array();
        $carray = array();
        $tcount = 0;
        $rcount = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            if( $rtype == '2' )
            {
                if( isset( $darray[ $d['bdtype'] ] ) )
                {
                    $darray[ $d['bdtype'] ] += $d['baototal'];
                }
                else
                {
                    $darray[ $d['bdtype'] ] = $d['baototal'];
                }

                if( isset( $tarray[ $d['bdtype'] ][ $d['trip'] ] ) )
                {
                    $tarray[ $d['bdtype'] ][ $d['trip'] ] += $d['baototal'];
                }
                else
                {
                    $tarray[ $d['bdtype'] ][ $d['trip'] ] = $d['baototal'];
                }

                if( isset( $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bydate'] ] ) )
                {
                    $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bydate'] ] += $d['baototal'];
                }
                else
                {
                    $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bydate'] ] = $d['baototal'];
                }
            }
            elseif( $rtype == '3' )
            {
                if( isset( $darray[ $d['bdtype'] ] ) )
                {
                    $darray[ $d['bdtype'] ] += $d['baototal'];
                }
                else
                {
                    $darray[ $d['bdtype'] ] = $d['baototal'];
                }

                if( isset( $tarray[ $d['bdtype'] ][ $d['trip'] ] ) )
                {
                    $tarray[ $d['bdtype'] ][ $d['trip'] ] += $d['baototal'];
                }
                else
                {
                    $tarray[ $d['bdtype'] ][ $d['trip'] ] = $d['baototal'];
                }

                if( isset( $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bdate'] ] ) )
                {
                    $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bdate'] ] += $d['baototal'];
                }
                else
                {
                    $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bdate'] ] = $d['baototal'];
                }
            }
            else
            {
                if( isset( $darray[ $d['bdtype'] ] ) )
                {
                    $darray[ $d['bdtype'] ] += $d['baototal'];
                }
                else
                {
                    $darray[ $d['bdtype'] ] = $d['baototal'];
                }

                if( isset( $tarray[ $d['bdtype'] ][ $d['trip'] ] ) )
                {
                    $tarray[ $d['bdtype'] ][ $d['trip'] ] += $d['baototal'];
                }
                else
                {
                    $tarray[ $d['bdtype'] ][ $d['trip'] ] = $d['baototal'];
                }

                if( isset( $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bmydate'] ] ) )
                {
                    $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bmydate'] ] += $d['baototal'];
                }
                else
                {
                    $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bmydate'] ] = $d['baototal'];
                }
            }

            $data[] = $d;

            $rcount += $d['baototal'];
        }

        foreach( $data as $d )
        {
            $tcount  = $tcount + $d['baopax'];

            if( $rtype == '2' )
            {
                $dtotal  = $darray[ $d['bdtype'] ];
                $ttotal  = $tarray[ $d['bdtype'] ][ $d['trip'] ];
                $gtotal  = $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bydate'] ];
                $bddate  = $d['bydate'];
            }
            elseif( $rtype == '3' )
            {
                $dtotal  = $darray[ $d['bdtype'] ];
                $ttotal  = $tarray[ $d['bdtype'] ][ $d['trip'] ];
                $gtotal  = $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bdate'] ];
                $bddate  = $d['bdate'];
            }
            else
            {
                $dtotal  = $darray[ $d['bdtype'] ];
                $ttotal  = $tarray[ $d['bdtype'] ][ $d['trip'] ];
                $gtotal  = $carray[ $d['bdtype'] ][ $d['trip'] ][ $d['bmydate'] ];
                $bddate  = $d['bmydate'];
            }

            $dftotal  = number_format( $dtotal, 0, ',', '.' );
            $tftotal  = number_format( $ttotal, 0, ',', '.' );
            $gftotal  = number_format( $gtotal, 0, ',', '.' );

            $index  = base64_encode( json_encode( array( $d['bdtype'], $dtotal, $dftotal, round( ( $dtotal * 100 ) / $rcount, 2 ) ) ) );
            $trip   = base64_encode( json_encode( array( $d['trip'], $ttotal, $tftotal, round( ( $ttotal  * 100 ) / $dtotal, 2 ) ) ) );
            $group  = base64_encode( json_encode( array( $bddate, $gtotal, $gftotal, round( ( $gtotal  * 100 ) / $ttotal, 2 ) ) ) );

            $items[ $index ][ $trip ][ $group ][] = array(
                'aoname'    => $d['aoname'],
                'baopax'    => $d['baopax'],
                'baototal'  => $d['baototal'],
                'baoprice'  => $d['baoprice'],
                'bfaototal' => $d['bfaototal'],
                'bfaoprice' => $d['bfaoprice']
            );
        }

        $frcount  = number_format( $rcount, 0, ',', '.' );
        $category = array();
        $series   = array();

        foreach( $items as $obj => $trips )
        {
            list( $bdtype, $total_rev_by_type, $total_rev_by_type_format, $total_percent_rev_by_type ) = json_decode( base64_decode( $obj ) );

            foreach( $trips as $item => $groups )
            {
                list( $trip, $total_rev_by_trip, $total_rev_by_trip_format, $total_percent_rev_by_trip ) = json_decode( base64_decode( $item ) );

                foreach( $groups as $key => $group )
                {
                    list( $date, $total_rev_by_date, $total_rev_by_date_format, $total_percent_rev_by_date ) = json_decode( base64_decode( $key ) );

                    $series[ $trip ][ $date ][] = $total_rev_by_date;

                    $category[] = $date;

                }
            }
        }

        if( $rcount == 0 )
        {
            return array( 'total' => 0 );
        }
        else
        {
            $category = array_unique( $category );

            sort( $category );

            return array( 'total' => $rcount, 'total_format' => $frcount, 'total_pax' => $tcount, 'chart' => get_chart_options( $category, $series ), 'items' => $items, 'filter' => $fltr );
        }
    }
}

function get_daily_addons_report( $filter )
{
    global $db;

    $fltr = base64_encode( json_encode( $filter ) );

    extract( $filter );

    //-- Main Filter
    $w = array();

    if( $lcid != '' && $lcid2 != '' )
    {
        $w[] = $db->prepare_query( '( b.bdfrom_id = %d OR b.bdto_id = %d )', $lcid, $lcid2 );
    }
    elseif( $lcid != '' )
    {
        $w[] = $db->prepare_query( 'b.bdfrom_id = %d', $lcid );
    }
    elseif( $lcid2 != '' )
    {
        $w[] = $db->prepare_query( 'b.bdto_id = %d', $lcid2 );
    }

    if( $bdate != '' )
    {
        $w[] = $db->prepare_query( 'b.bddate = %s', date( 'Y-m-d', strtotime( $bdate ) ) );
    }

    if( empty( $w ) )
    {
        $where = '';
    }
    else
    {
        $where = 'AND ' . implode( ' AND ', $w );
    }

    $q = 'SELECT
            c.bid,
            b.bdto,
            a.baopax,
            d.aoname,
            e.agname,
            f.chcode,
            f.chname,
            b.bdfrom,
            c.bticket,
            a.baoprice,
            b.bddeparttime,
            FORMAT( a.baoprice, 0 ) AS bfaoprice,
            ( a.baopax * a.baoprice ) AS baototal,
            DATE_FORMAT( b.bddate, "%d %M, %Y") AS bddate,
            FORMAT( ( a.baopax * a.baoprice ), 0 ) AS bfaototal,
            CONCAT( b.bdfrom, " (", DATE_FORMAT( b.bddeparttime, "%H:%i"), ")" ) AS trip,
            ( SELECT a2.bpname FROM ticket_booking_passenger AS a2 WHERE a2.bdid = a.bdid LIMIT 1 ) AS gname
          FROM ticket_booking_add_ons AS a
          LEFT JOIN ticket_booking_detail AS b ON a.bdid = b.bdid
          LEFT JOIN ticket_booking AS c ON b.bid = c.bid
          LEFT JOIN ticket_add_ons AS d ON a.aoid = d.aoid
          LEFT JOIN ticket_agent AS e ON c.agid = e.agid
          LEFT JOIN ticket_channel AS f ON c.chid = f.chid
          WHERE c.bstt <> "ar" AND b.bdstatus <> "cn"
          AND b.bdpstatus NOT IN( "pf", "cn" ) ' . $where . '
          ORDER BY b.bddate ASC';
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return array( 'total' => 0 );
    }
    else
    {
        $items  = array();
        $data   = array();
        $darray = array();
        $tarray = array();
        $carray = array();
        $tcount = 0;
        $rcount = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            if( isset( $darray[ $d['bddate'] ] ) )
            {
                $darray[ $d['bddate'] ] += $d['baototal'];
            }
            else
            {
                $darray[ $d['bddate'] ] = $d['baototal'];
            }

            if( isset( $tarray[ $d['bddate'] ][ $d['trip'] ] ) )
            {
                $tarray[ $d['bddate'] ][ $d['trip'] ] += $d['baototal'];
            }
            else
            {
                $tarray[ $d['bddate'] ][ $d['trip'] ] = $d['baototal'];
            }

            if( isset( $carray[ $d['bddate'] ][ $d['trip'] ][ $d['chcode'] ] ) )
            {
                $carray[ $d['bddate'] ][ $d['trip'] ][ $d['chcode'] ] += $d['baototal'];
            }
            else
            {
                $carray[ $d['bddate'] ][ $d['trip'] ][ $d['chcode'] ] = $d['baototal'];
            }

            $data[] = $d;

            $rcount += $d['baototal'];
        }

        foreach( $data as $d )
        {
            $bsource = empty( $d['agname'] ) ? '( ' . $d['chcode'] . ' ) ' . $d['chname'] : $d['agname'];
            $tcount  = $tcount + $d['baopax'];

            $dtotal  = $darray[ $d['bddate'] ];
            $ttotal  = $tarray[ $d['bddate'] ][ $d['trip'] ];
            $gtotal  = $carray[ $d['bddate'] ][ $d['trip'] ][ $d['chcode'] ];

            $dftotal  = number_format( $dtotal, 0, ',', '.' );
            $tftotal  = number_format( $ttotal, 0, ',', '.' );
            $gftotal  = number_format( $gtotal, 0, ',', '.' );

            $index  = base64_encode( json_encode( array( $d['bddate'], $dtotal, $dftotal, round( ( $dtotal * 100 ) / $rcount, 2 ) ) ) );
            $trip   = base64_encode( json_encode( array( $d['trip'], $ttotal, $tftotal, round( ( $ttotal  * 100 ) / $dtotal, 2 ) ) ) );
            $group  = base64_encode( json_encode( array( $d['chcode'], $d['chname'], $gtotal, $gftotal, round( ( $gtotal  * 100 ) / $ttotal, 2 ) ) ) );

            $items[ $index ][ $trip ][ $group ][] = array(
                'gname'     => $d['gname'],
                'aoname'    => $d['aoname'],
                'baopax'    => $d['baopax'],
                'bticket'   => $d['bticket'],
                'baototal'  => $d['baototal'],
                'baoprice'  => $d['baoprice'],
                'bfaototal' => $d['bfaototal'],
                'bfaoprice' => $d['bfaoprice']
            );
        }

        $frcount  = number_format( $rcount, 0, ',', '.' );
        $category = array();
        $series   = array();

        foreach( $items as $obj => $trips )
        {
            list( $bddate, $total_rev, $total_rev_format, $total_rev_percent ) = json_decode( base64_decode( $obj ) );

            foreach( $trips as $item => $groups )
            {
                list( $trip, $total_rev_by_trip, $total_rev_by_trip_format, $total_percent_rev_by_trip ) = json_decode( base64_decode( $item ) );

                foreach( $groups as $source => $group )
                {
                    list( $chcode, $chname, $total_rev_by_source, $total_rev_by_source_format, $total_percent_rev_by_source ) = json_decode( base64_decode( $source ) );

                    $series[ $trip ][ $chname ][] = $total_rev_by_source;

                    $category[] = $chname;
                }
            }
        }

        if( $rcount == 0 )
        {
            return array( 'total' => 0 );
        }
        else
        {
            $category = array_unique( $category );

            sort( $category );

            return array( 'total' => $rcount, 'total_format' => $frcount, 'total_pax' => $tcount, 'chart' => get_chart_options( $category, $series ), 'items' => $items, 'filter' => $fltr );
        }
    }
}

function get_reservation_report( $filter )
{
    global $db;

    $fltr = base64_encode( json_encode( $filter ) );

	extract( $filter );

    //-- Main Filter
	$w = array();

    if( $chid != '' )
    {
    	$arr = explode( '|', $chid );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

    	if( isset( $arr[1] ) )
    	{
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
    	}
    }

    if( $rtype != '' && $bdate != '' )
    {
    	if( $rtype == '1' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 ' . $bdate ) );

    		$w[] = $db->prepare_query( ' YEAR( a.bdate ) = YEAR( %s ) AND MONTH( a.bdate ) = MONTH( %s )', $date, $date );
    	}
    	elseif( $rtype == '2' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 Jan ' . $bdate ) );

    		$w[] = $db->prepare_query( ' YEAR( a.bdate ) = YEAR( %s )', $date );
    	}
    	elseif( $rtype == '3' )
    	{
    		$date_from = date( 'Y-m-d', strtotime( $bdate ) );
    		$date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

    		$w[] = $db->prepare_query( ' a.bdate BETWEEN %s AND %s', $date_from, $date_to );
    	}
    }

	if( empty( $w ) )
	{
		$where = '';
	}
	else
	{
		$where = 'AND ' . implode( ' AND', $w );
	}

    //-- Secondary Filter
    $sw = array();
    $b  = '';

    if( $rid != '' )
    {
        $sw[] = $db->prepare_query( 'a.rid = %d', $rid );
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = '( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'a.bdpstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'a.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( 'a.bdpstatus = %s', $bstatus );
        }
    }

    if( empty( $sw ) && empty( $b ) )
    {
        $swhere = '';
    }
    elseif( empty( $sw ) && !empty( $b ) )
    {
        $swhere = 'AND ' . $b;
    }
    elseif( !empty( $sw ) && empty( $b ) )
    {
        $swhere = 'AND ' . implode( ' AND', $sw );
    }
    else
    {
        $swhere = 'AND ' . $b . ' AND ' . implode( ' AND', $sw );
    }

	$q = 'SELECT
			a.bid,
			a.agid,
			a.bdate,
			a.bticket,
			c.chid,
			c.chcode,
			c.chname
		  FROM ticket_booking AS a
		  LEFT JOIN ticket_channel AS c ON a.chid = c.chid
		  WHERE a.bstt <> "ar" ' . $where . ' ORDER BY a.bdate ASC';
	$r = $db->do_query( $q );

	$items  = array();
	$data   = array();
	$darray = array();
	$larray = array();
	$rarray = array();
	$carray = array();
	$tarray = array();

	while( $d = $db->fetch_array( $r ) )
	{
		$s2 = 'SELECT * FROM ticket_booking_detail AS a WHERE a.bid = %d ' . $swhere;
        $q2 = $db->prepare_query( $s2, $d['bid'] );
        $r2 = $db->do_query( $q2 );

        if( $db->num_rows( $r2 ) > 0 )
        {
        	$amount = 0;
        	$adult  = 0;
        	$child  = 0;
        	$infant = 0;
        	$trips  = array();
            $t = 0;

        	while( $d2 = $db->fetch_array( $r2 ) )
        	{
        		$amount = $amount + ( $d2['num_adult'] + $d2['num_child'] + $d2['num_infant'] );
            	$adult  = $d2['num_adult'];
            	$child  = $d2['num_child'];
            	$infant = $d2['num_infant'];

            	$trips[] = $d2['bdfrom'] . ' - ' . $d2['bdto'];
        	}

            $d['amount']     = $amount;
            $d['num_infant'] = $infant;
            $d['num_adult']  = $adult;
            $d['num_child']  = $child;
            $d['trips']      = implode( '<br/>', $trips );

    		$rarray[] = 1;
    		$data[]   = $d;
    		$tarray[] = $amount;

    		if( $rtype== '2' )
    		{
    			$ymkey = date( 'Y-m', strtotime( $d['bdate'] ) );

    			$darray[ $ymkey ][] = 1;
    			$carray[ $ymkey ][ $d['chcode'] ][] = 1;
    		}
    		else
    		{
    			$darray[ $d['bdate'] ][] = 1;
    			$carray[ $d['bdate'] ][ $d['chcode'] ][] = 1;
    		}
        }
	}

	$rcount = array_sum( $rarray );
	$tcount = array_sum( $tarray );

	foreach( $data as $d )
	{
		if( $rtype== '2' )
		{
			$ymkey = date( 'Y-m', strtotime( $d['bdate'] ) );

			$dtotal = array_sum( $darray[ $ymkey ] );
			$ctotal = array_sum( $carray[ $ymkey ][ $d['chcode'] ] );
		}
		else
		{
			$dtotal = array_sum( $darray[ $d['bdate'] ] );
			$ctotal = array_sum( $carray[ $d['bdate'] ][ $d['chcode'] ] );
		}

		$agname  = get_agent( $d['agid'], 'agname' );
		$bsource = empty( $agname ) ? '( ' . $d['chcode'] . ' ) ' . $d['chname'] : $agname;

		if( $rtype== '2' )
		{
			$ymkey = date( 'M-Y', strtotime( $d['bdate'] ) );

			$index   = base64_encode( json_encode( array( $ymkey, $dtotal, round( ( $dtotal * 100 ) / $rcount, 2 ) ) ) );
		}
		else
		{
			$dates 	 = date( 'd F Y', strtotime( $d['bdate'] ) );
			$index   = base64_encode( json_encode( array( $dates, $dtotal, round( ( $dtotal * 100 ) / $rcount, 2 ) ) ) );
		}

		$group   = base64_encode( json_encode( array( $d['chcode'], $d['chname'], $ctotal, round( ( $ctotal * 100 ) / $dtotal, 2 ) ) ) );

		$items[ $index ][ $group ][] = array(
			'bsource'    => $bsource,
			'bid'        => $d['bid'],
			'chid'       => $d['chid'],
			'amount'     => $d['amount'],
			'trips'      => $d['trips'],
			'chcode'     => $d['chcode'],
			'chname'     => $d['chname'],
			'bticket'    => $d['bticket'],
			'num_adult'  => $d['num_adult'],
			'num_child'  => $d['num_child'],
			'num_infant' => $d['num_infant']
		);
	}

	$category = array();
	$series   = array();

	foreach ( $items as $obj => $groups )
	{
		list( $rev_date, $total_rev, $total_rev_percent ) = json_decode( base64_decode( $obj ) );

		$category[] = $rev_date;

		foreach ( $groups as $source => $group )
		{
			list( $chcode, $chname, $total_rev_by_source, $total_percent_rev_by_source ) = json_decode( base64_decode( $source ) );

			$series[ $chname ][ $rev_date ][] = $total_rev_by_source;
		}
	}

    if ( $rcount == 0 )
    {
        return array( 'total' => 0 );
    }
    else
    {
        return array( 'total' => $rcount, 'total_pax' => $tcount, 'chart' => get_chart_options( $category, $series ), 'items' => $items, 'filter' => $fltr );
    }
}

function get_seats_production_sum_report( $filter )
{
    global $db;

	extract( $filter );

	$w = array();
	$b = '';

    if( isset( $ryear ) &&  !empty( $ryear ) )
    {
    	$w[] = $db->prepare_query( ' YEAR( b.bddate ) IN ( ' . implode( ',', $ryear ) . ' )' );
    }

    if( isset( $chid ) && $chid != '' )
    {
    	$arr = explode( '|', $chid );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

    	if( isset( $arr[1] ) )
    	{
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
    	}
    }

    if( isset( $agsp ) && $agsp != '' )
    {
        $w[] = $db->prepare_query( ' d.agsp = %s', $agsp );
    }

    if( isset( $rstatus ) && $rstatus != '' )
    {
        if( is_array( $rstatus ) )
        {
            $estatus = end( $rstatus );

            $b = ' AND ( ';

            foreach( $rstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'b.bdrevstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.bdrevstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' AND b.bdrevstatus = %s', $rstatus );
        }
    }

    if( isset( $bstatus ) && $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b .= ' AND ( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' AND b.bdpstatus = %s', $bstatus );
        }
    }

    if( isset( $rid ) && !empty( $rid ) )
    {
        if( is_array( $rid ) )
        {
            $estatus = end( $rid );

            $b .= ' AND ( ';

            foreach( $rid as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'b.rid = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.rid = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b .= $db->prepare_query( ' AND b.rid = %s', $rid );
        }
    }

    if( isset( $tid ) && !empty( $tid ) )
    {
        if( is_array( $tid ) )
        {
            $etid = end( $tid );

            $b .= ' AND ( ';

            foreach( $tid as $td )
            {
                list( $bdfrom, $bdto ) = explode( ' - ', $td );

                if( $td == $etid )
                {
                    $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s )', $bdfrom, $bdto );
                }
                else
                {
                    $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s ) OR ', $bdfrom, $bdto );
                }
            }

            $b .= ' )';
        }
        else
        {
            list( $bdfrom, $bdto ) = explode( ' - ', $rid );

            $b = $db->prepare_query( ' AND ( b.bdfrom = %s AND b.bdto = %s )', $bdfrom, $bdto );
        }
    }

	if( empty( $w ) && empty( $b ) )
	{
		$where = 'WHERE a.bstt <> "ar" AND b.bdpstatus NOT IN ("cn","bc","pf","rf")';
	}
	elseif( empty( $w ) && !empty( $b ) )
	{
		$where = 'WHERE a.bstt <> "ar" ' . $b;
	}
	elseif( !empty( $w ) && empty( $b ) )
	{
		$where = 'WHERE a.bstt <> "ar" AND ' . implode( ' AND', $w );
	}
	else
	{
		$where = 'WHERE a.bstt <> "ar" ' . $b . ' AND ' . implode( ' AND', $w );
	}

	$prevdt = array();
	$items  = array();

	if( isset( $rview ) && $rview == 2 )
	{
		$q = 'SELECT
		        a.pmcode,
				a.bmethod_by,
				d.agpayment_type,
				d.agsp,
				b.bddate,
				c.chcode,
				c.chname,
				d.agname,
				e.lcountry_id,
				f.lcountry,
				f.lcountry_code,
				IF( e.bptype = "adult", 1, 0 ) AS num_adult,
				IF( e.bptype = "child", 1, 0 ) AS num_child,
				IF( e.bptype = "infant", 1, 0 ) AS num_infant
			  FROM ticket_booking AS a
			  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
			  LEFT JOIN ticket_channel AS c ON a.chid = c.chid
			  LEFT JOIN ticket_agent AS d ON a.agid = d.agid OR a.sagid = d.agid
			  LEFT JOIN ticket_booking_passenger AS e ON e.bdid = b.bdid
			  LEFT JOIN l_country AS f ON e.lcountry_id = f.lcountry_id
			  ' . $where . ' ORDER BY DATE_FORMAT( b.bddate, "%m" ) ASC, DATE_FORMAT( b.bddate, "%Y" ) ASC';
		$r = $db->do_query( $q );

		if( !is_array( $r ) )
		{
			while( $d = $db->fetch_array( $r ) )
			{
				$year   = date( 'Y', strtotime( $d['bddate'] ) );
				$month  = date( 'm', strtotime( $d['bddate'] ) );
				$amount = $d['num_adult'] + $d['num_child'] + $d['num_infant'];

                $cname = in_array( $d['lcountry_id'], array( 247, 248 ) ) ? '-' : $d['lcountry'];
				$ccode = empty( $d['lcountry_code'] ) ? '-' : $d['lcountry_code'];
				$index = base64_encode( json_encode( array( $ccode, $cname ) ) );

                //bayu edit
	            if(!empty($d['bmethod_by'])){
					$items[ $year ][ $index ]['bmethod_by'][0]	=	isset( $items[ $year ][ $index ]['bmethod_by'][0] ) && !empty($items[ $year ][ $index ] [ 'bmethod_by' ][0])? $items[ $year ][ $index ] ['bmethod_by'][0] : ($d['bmethod_by'] == 0 ? 'Direct' : '') ;
					$items[ $year ][ $index ]['bmethod_by'][1]	=	isset( $items[ $year ][ $index ]['bmethod_by'][1] ) && !empty($items[ $year ][ $index ] [ 'bmethod_by' ][1])? $items[ $year ][ $index ]['bmethod_by'][1] : ($d['bmethod_by'] == 1 ? 'Agent' : '');
					$items[ $year ][ $index ]['bmethod_by'][2]	=	isset( $items[ $year ][ $index ]['bmethod_by'][2] ) && !empty($items[ $year ][ $index ] [ 'bmethod_by' ][2])? $items[ $year ][ $index ]['bmethod_by'][2] : ($d['bmethod_by'] == 2 ? 'Admin' : '');
				}

				if(!empty($d['agpayment_type'])){
					$items[ $year ][ $index ]['agpayment_type'][0]	=	isset( $items[ $year ][ $index ]['agpayment_type'][0] ) && !empty($items[ $year ][ $index ] [ 'agpayment_type' ][0])? $items[ $year ][ $index ]['agpayment_type'][0] : ($d['agpayment_type'] == 'Pre Paid' ? 'Pre Paid' : '') ;
					$items[ $year ][ $index ]['agpayment_type'][1]	=	isset( $items[ $year ][ $index ]['agpayment_type'][1] ) && !empty($items[ $year ][ $index ] [ 'agpayment_type' ][1])? $items[ $year ][ $index ]['agpayment_type'][1] : ($d['agpayment_type'] == 'Credit' ? 'Credit' : '');
				}

                $items[ $year ][ $index ][ 'agpayment' ] = isset( $items[ $year ][ $index ][ 'agpayment' ] ) ? $items[ $year ][ $index ][ 'agpayment' ] : $d['agpayment_type'];
				$items[ $year ][ $index ]['pmcode']	=	isset( $items[ $year ][ $index ]['pmcode']) && !empty( $items[ $year ][ $index ]['pmcode']) ? $items[ $year ][ $index ]['pmcode'] : (!empty($d['pmcode']) ? 'YES' : '') ;
                //akhir bayu edit

				$cname = in_array( $d['lcountry_id'], array( 247, 248 ) ) ? '-' : $d['lcountry'];
				$ccode = empty( $d['lcountry_code'] ) ? '-' : $d['lcountry_code'];
				$index = base64_encode( json_encode( array( $ccode, $cname ) ) );

				$items[ $year ][ $index ][ $month ] = isset( $items[ $year ][ $index ][ $month ] ) ? $items[ $year ][ $index ][ $month ] + $amount : $amount;
			}
		}

		if( !empty( $items ) )
		{
            $w = array();
            $b = '';

            if( $chid != '' )
            {
                $arr = explode( '|', $chid );

                $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

                if( isset( $arr[1] ) )
                {
                    $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
                }
            }

            if( $agsp != '' )
            {
                $w[] = $db->prepare_query( ' d.agsp = %s', $agsp );
            }

            if( $rstatus != '' )
            {
                if( is_array( $rstatus ) )
                {
                    $estatus = end( $rstatus );

                    $b = ' AND ( ';

                    foreach( $rstatus as $st )
                    {
                        if( $st == $estatus )
                        {
                            $b .= $db->prepare_query( 'b.bdrevstatus = %s', $st );
                        }
                        else
                        {
                            $b .= $db->prepare_query( 'b.bdrevstatus = %s OR ', $st );
                        }
                    }

                    $b .= ' )';
                }
                else
                {
                    $b = $db->prepare_query( ' AND b.bdrevstatus = %s', $rstatus );
                }
            }

            if( $bstatus != '' )
            {
                if( is_array( $bstatus ) )
                {
                    $estatus = end( $bstatus );

                    $b .= ' AND ( ';

                    foreach( $bstatus as $st )
                    {
                        if( $st == $estatus )
                        {
                            $b .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                        }
                        else
                        {
                            $b .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                        }
                    }

                    $b .= ' )';
                }
                else
                {
                    $b = $db->prepare_query( ' AND b.bdpstatus = %s', $bstatus );
                }
            }

            if( !empty( $rid ) )
            {
                if( is_array( $rid ) )
                {
                    $estatus = end( $rid );

                    $b .= ' AND ( ';

                    foreach( $rid as $st )
                    {
                        if( $st == $estatus )
                        {
                            $b .= $db->prepare_query( 'b.rid = %s', $st );
                        }
                        else
                        {
                            $b .= $db->prepare_query( 'b.rid = %s OR ', $st );
                        }
                    }

                    $b .= ' )';
                }
                else
                {
                    $b .= $db->prepare_query( ' AND b.rid = %s', $rid );
                }
            }

            if( !empty( $tid ) )
            {
                if( is_array( $tid ) )
                {
                    $etid = end( $tid );

                    $b .= ' AND ( ';

                    foreach( $tid as $td )
                    {
                        list( $bdfrom, $bdto ) = explode( ' - ', $td );

                        if( $td == $etid )
                        {
                            $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s )', $bdfrom, $bdto );
                        }
                        else
                        {
                            $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s ) OR ', $bdfrom, $bdto );
                        }
                    }

                    $b .= ' )';
                }
                else
                {
                    list( $bdfrom, $bdto ) = explode( ' - ', $rid );

                    $b = $db->prepare_query( ' AND ( b.bdfrom = %s AND b.bdto = %s )', $bdfrom, $bdto );
                }
            }

            if( empty( $w ) && empty( $b ) )
            {
                $where = 'WHERE a.bstt <> "ar" AND b.bdpstatus NOT IN ("cn","bc","pf","rf")';
            }
            elseif( empty( $w ) && !empty( $b ) )
            {
                $where = 'WHERE a.bstt <> "ar" ' . $b;
            }
            elseif( !empty( $w ) && empty( $b ) )
            {
                $where = 'WHERE a.bstt <> "ar" AND ' . implode( ' AND', $w );
            }
            else
            {
                $where = 'WHERE a.bstt <> "ar" ' . $b . ' AND ' . implode( ' AND', $w );
            }

			foreach( $items as $year => $item )
			{
				$last_year = date( 'Y', strtotime( '-1 year', strtotime( $year . '-01-01' ) ) );

				$q2 = 'SELECT
						  a.agid,
						  a.sagid,
						  DATE_FORMAT(b.bddate, "%m") AS mnth,
						  SUM( IF( e.bptype = "adult", 1, 0 ) + IF( e.bptype = "child", 1, 0 ) + IF( e.bptype = "infant", 1, 0 ) ) AS num
					   FROM ticket_booking AS a
					   LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
					   LEFT JOIN ticket_channel AS c ON a.chid = c.chid
					   LEFT JOIN ticket_agent AS d ON a.agid = d.agid OR a.sagid = d.agid
			  		   LEFT JOIN ticket_booking_passenger AS e ON e.bdid = b.bdid
					   ' . $where . ' AND DATE_FORMAT( b.bddate, "%Y" ) = "' . $last_year . '"
					   GROUP BY DATE_FORMAT( b.bddate, "%m" )
					   ORDER BY DATE_FORMAT( b.bddate, "%Y" )';
				$r2 = $db->do_query( $q2 );

				while( $d2 = $db->fetch_array( $r2 ) )
				{
					$prevdt[ $last_year ][ $d2['mnth'] ] = $d2['num'];
				}
			}
		}

		ksort( $items );

		return array( 'items' => $items, 'previous' => $prevdt );
	}
	else
	{
		$q = 'SELECT
		        a.pmcode,
				a.bmethod_by,
				d.agpayment_type,
				d.agsp,
				b.bddate,
				c.chcode,
				c.chname,
				d.agname,
				b.num_adult + b.num_child + b.num_infant AS num
			  FROM ticket_booking AS a
			  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
			  LEFT JOIN ticket_channel AS c ON a.chid = c.chid
			  LEFT JOIN ticket_agent AS d ON a.agid = d.agid OR a.sagid = d.agid
			  ' . $where . ' ORDER BY DATE_FORMAT( b.bddate, "%m" ) ASC, DATE_FORMAT( b.bddate, "%Y" ) ASC';
		$r = $db->do_query( $q );

		if( !is_array( $r ) )
		{
			while( $d = $db->fetch_array( $r ) )
			{
				$year  = date( 'Y', strtotime( $d['bddate'] ) );
				$month = date( 'm', strtotime( $d['bddate'] ) );

				if( $rview == 0 )
				{
					$index = base64_encode( json_encode( array( $d['chcode'], $d['chname'] ) ) );
					$items[ $year ][ $index ]['pmcode']	        =	isset( $items[ $year ][ $index ]['pmcode']) && !empty( $items[ $year ][ $index ]['pmcode']) ? $items[ $year ][ $index ]['pmcode'] : (!empty($d['pmcode']) ? 'YES' : '') ;
				    $items[ $year ][ $index ]['sales_person']	=	isset( $items[ $year ][ $index ]['sales_person']) ? $items[ $year ][ $index ]['sales_person'] : $d['agsp'];
                    //bayu edit
                    if(!empty($d['bmethod_by'])){
						$items[ $year ][ $index ]['bmethod_by'][0]	=	isset( $items[ $year ][ $index ]['bmethod_by'][0] ) && !empty($items[ $year ][ $index ] [ 'bmethod_by' ][0])? $items[ $year ][ $index ] ['bmethod_by'][0] : ($d['bmethod_by'] == 0 ? 'Direct' : '') ;
						$items[ $year ][ $index ]['bmethod_by'][1]	=	isset( $items[ $year ][ $index ]['bmethod_by'][1] ) && !empty($items[ $year ][ $index ] [ 'bmethod_by' ][1])? $items[ $year ][ $index ]['bmethod_by'][1] : ($d['bmethod_by'] == 1 ? 'Agent' : '');
						$items[ $year ][ $index ]['bmethod_by'][2]	=	isset( $items[ $year ][ $index ]['bmethod_by'][2] ) && !empty($items[ $year ][ $index ] [ 'bmethod_by' ][2])? $items[ $year ][ $index ]['bmethod_by'][2] : ($d['bmethod_by'] == 2 ? 'Admin' : '');
					}

					if(!empty($d['agpayment_type'])){
						$items[ $year ][ $index ]['agpayment_type'][0]	=	isset( $items[ $year ][ $index ]['agpayment_type'][0] ) && !empty($items[ $year ][ $index ] [ 'agpayment_type' ][0])? $items[ $year ][ $index ]['agpayment_type'][0] : ($d['agpayment_type'] == 'Pre Paid' ? 'Pre Paid' : '') ;
						$items[ $year ][ $index ]['agpayment_type'][1]	=	isset( $items[ $year ][ $index ]['agpayment_type'][1] ) && !empty($items[ $year ][ $index ] [ 'agpayment_type' ][1])? $items[ $year ][ $index ]['agpayment_type'][1] : ($d['agpayment_type'] == 'Credit' ? 'Credit' : '');
					}

					if($d['chcode'] == 'ONL'){
						$items[ $year ][ $index ]['bmethod_by'][0]	=	isset( $items[ $year ][ $index ]['bmethod_by'][0] ) ? $items[ $year ][ $index ] ['bmethod_by'][0] : 'Direct Online' ;
					}

                    //akhir bayu edit

					$items[ $year ][ $index ][ $month ] = isset( $items[ $year ][ $index ][ $month ] ) ? $items[ $year ][ $index ][ $month ] + $d['num'] : $d['num'];
				}
				elseif( $rview == 1 )
				{
					$agsp    = empty( $d['agsp'] ) ? '-' : $d['agsp'];
					$bsource = empty( $d['agname'] ) ? $d['chname'] : $d['agname'];
					$index   = base64_encode( json_encode( array( $d['chcode'], $bsource, $agsp ) ) );

                    //bayu edit
                    if(!empty($d['bmethod_by'])){
						$items[ $year ][ $index ]['bmethod_by'][0]	=	isset( $items[ $year ][ $index ]['bmethod_by'][0] ) && !empty($items[ $year ][ $index ] [ 'bmethod_by' ][0])? $items[ $year ][ $index ] ['bmethod_by'][0] : ($d['bmethod_by'] == 0 ? 'Direct' : '') ;
						$items[ $year ][ $index ]['bmethod_by'][1]	=	isset( $items[ $year ][ $index ]['bmethod_by'][1] ) && !empty($items[ $year ][ $index ] [ 'bmethod_by' ][1])? $items[ $year ][ $index ]['bmethod_by'][1] : ($d['bmethod_by'] == 1 ? 'Agent' : '');
						$items[ $year ][ $index ]['bmethod_by'][2]	=	isset( $items[ $year ][ $index ]['bmethod_by'][2] ) && !empty($items[ $year ][ $index ] [ 'bmethod_by' ][2])? $items[ $year ][ $index ]['bmethod_by'][2] : ($d['bmethod_by'] == 2 ? 'Admin' : '');
					}

					if(!empty($d['agpayment_type'])){
						$items[ $year ][ $index ]['agpayment_type'][0]	=	isset( $items[ $year ][ $index ]['agpayment_type'][0] ) && !empty($items[ $year ][ $index ] [ 'agpayment_type' ][0])? $items[ $year ][ $index ]['agpayment_type'][0] : ($d['agpayment_type'] == 'Pre Paid' ? 'Pre Paid' : '') ;
						$items[ $year ][ $index ]['agpayment_type'][1]	=	isset( $items[ $year ][ $index ]['agpayment_type'][1] ) && !empty($items[ $year ][ $index ] [ 'agpayment_type' ][1])? $items[ $year ][ $index ]['agpayment_type'][1] : ($d['agpayment_type'] == 'Credit' ? 'Credit' : '');
					}
				    if($d['chcode'] == 'ONL'){
						$items[ $year ][ $index ]['bmethod_by'][0]	=	isset( $items[ $year ][ $index ]['bmethod_by'][0] ) ? $items[ $year ][ $index ] ['bmethod_by'][0] : 'Direct Online' ;
					}
                    $items[ $year ][ $index ][ 'agpayment' ]    = isset( $items[ $year ][ $index ][ 'agpayment' ] ) ? $items[ $year ][ $index ][ 'agpayment' ] : $d['agpayment_type'];
					$items[ $year ][ $index ]['pmcode']	        =	isset( $items[ $year ][ $index ]['pmcode']) && !empty( $items[ $year ][ $index ]['pmcode']) ? $items[ $year ][ $index ]['pmcode'] : (!empty($d['pmcode']) ? 'YES' : '') ;
				    $items[ $year ][ $index ]['sales_person']	=	isset( $items[ $year ][ $index ]['sales_person']) ? $items[ $year ][ $index ]['sales_person'] : $d['agsp'];

                    //akhir bayu edit

					$items[ $year ][ $index ][ $month ] = isset( $items[ $year ][ $index ][ $month ] ) ? $items[ $year ][ $index ][ $month ] + $d['num'] : $d['num'];
				}
			}
		}

		if( !empty( $items ) )
		{
            $w = array();
            $b = '';

            if( $chid != '' )
            {
                $arr = explode( '|', $chid );

                $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

                if( isset( $arr[1] ) )
                {
                    $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
                }
            }

            if( $agsp != '' )
            {
                $w[] = $db->prepare_query( ' d.agsp = %s', $agsp );
            }

            if( $rstatus != '' )
            {
                if( is_array( $rstatus ) )
                {
                    $estatus = end( $rstatus );

                    $b = ' AND ( ';

                    foreach( $rstatus as $st )
                    {
                        if( $st == $estatus )
                        {
                            $b .= $db->prepare_query( 'b.bdrevstatus = %s', $st );
                        }
                        else
                        {
                            $b .= $db->prepare_query( 'b.bdrevstatus = %s OR ', $st );
                        }
                    }

                    $b .= ' )';
                }
                else
                {
                    $b = $db->prepare_query( ' AND b.bdrevstatus = %s', $rstatus );
                }
            }

            if( $bstatus != '' )
            {
                if( is_array( $bstatus ) )
                {
                    $estatus = end( $bstatus );

                    $b .= ' AND ( ';

                    foreach( $bstatus as $st )
                    {
                        if( $st == $estatus )
                        {
                            $b .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                        }
                        else
                        {
                            $b .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                        }
                    }

                    $b .= ' )';
                }
                else
                {
                    $b = $db->prepare_query( ' AND b.bdpstatus = %s', $bstatus );
                }
            }

            if( !empty( $rid ) )
            {
                if( is_array( $rid ) )
                {
                    $estatus = end( $rid );

                    $b .= ' AND ( ';

                    foreach( $rid as $st )
                    {
                        if( $st == $estatus )
                        {
                            $b .= $db->prepare_query( 'b.rid = %s', $st );
                        }
                        else
                        {
                            $b .= $db->prepare_query( 'b.rid = %s OR ', $st );
                        }
                    }

                    $b .= ' )';
                }
                else
                {
                    $b .= $db->prepare_query( ' AND b.rid = %s', $rid );
                }
            }

            if( !empty( $tid ) )
            {
                if( is_array( $tid ) )
                {
                    $etid = end( $tid );

                    $b .= ' AND ( ';

                    foreach( $tid as $td )
                    {
                        list( $bdfrom, $bdto ) = explode( ' - ', $td );

                        if( $td == $etid )
                        {
                            $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s )', $bdfrom, $bdto );
                        }
                        else
                        {
                            $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s ) OR ', $bdfrom, $bdto );
                        }
                    }

                    $b .= ' )';
                }
                else
                {
                    list( $bdfrom, $bdto ) = explode( ' - ', $rid );

                    $b = $db->prepare_query( ' AND ( b.bdfrom = %s AND b.bdto = %s )', $bdfrom, $bdto );
                }
            }

            if( empty( $w ) && empty( $b ) )
            {
                $where = 'WHERE a.bstt <> "ar" AND b.bdpstatus NOT IN ("cn","bc","pf","rf")';
            }
            elseif( empty( $w ) && !empty( $b ) )
            {
                $where = 'WHERE a.bstt <> "ar" ' . $b;
            }
            elseif( !empty( $w ) && empty( $b ) )
            {
                $where = 'WHERE a.bstt <> "ar" AND ' . implode( ' AND', $w );
            }
            else
            {
                $where = 'WHERE a.bstt <> "ar" ' . $b . ' AND ' . implode( ' AND', $w );
            }

			foreach( $items as $year => $item )
			{
				$last_year = date( 'Y', strtotime( '-1 year', strtotime( $year . '-01-01' ) ) );

				$q2 = 'SELECT
						  a.agid,
						  a.sagid,
						  DATE_FORMAT(b.bddate, "%m") AS mnth,
						  SUM(b.num_adult + b.num_child + b.num_infant) AS num
					   FROM ticket_booking AS a
					   LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
					   LEFT JOIN ticket_channel AS c ON a.chid = c.chid
					   LEFT JOIN ticket_agent AS d ON a.agid = d.agid OR a.sagid = d.agid
					   ' . $where . ' AND DATE_FORMAT( b.bddate, "%Y" ) = "' . $last_year . '"
					   GROUP BY DATE_FORMAT( b.bddate, "%m" )
					   ORDER BY DATE_FORMAT( b.bddate, "%Y" )';
				$r2 = $db->do_query( $q2 );

				while( $d2 = $db->fetch_array( $r2 ) )
				{
					if( $rview == 0 )
					{
						$prevdt[ $last_year ][ $d2['mnth'] ] = $d2['num'];
					}
					elseif( $rview == 1 )
					{
						$prevdt[ $last_year ][ $d2['mnth'] ] = $d2['num'];
					}
				}
			}
		}

		ksort( $items );

		return array( 'items' => $items, 'previous' => $prevdt );
	}
}

function get_seats_production_report( $filter )
{
    global $db;

    ini_set( 'max_execution_time', 500 );

    $w = array();
    $b = '';

    $fltr = base64_encode( json_encode( $filter ) );

    extract( $filter );

    if( !empty( $ryear ) )
    {
        $w[] = $db->prepare_query( ' YEAR( b.bddate ) IN ( ' . implode( ',', $ryear ) . ' )' );
    }

    if( $chid != '' )
    {
        $arr = explode( '|', $chid );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

        if( isset( $arr[1] ) )
        {
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
        }
    }

    if( $agsp != '' )
    {
        $w[] = $db->prepare_query( ' d.agsp = %s', $agsp );
    }

    if( $rstatus != '' )
    {
        if( is_array( $rstatus ) )
        {
            $estatus = end( $rstatus );

            $b = ' AND ( ';

            foreach( $rstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'b.bdrevstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.bdrevstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' b.bdrevstatus = %s', $rstatus );
        }
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b .= ' AND ( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' b.bdpstatus = %s', $bstatus );
        }
    }

    if( !empty( $rid ) )
    {
        if( is_array( $rid ) )
        {
            $erid = end( $rid );

            $b .= ' AND ( ';

            foreach( $rid as $rd )
            {
                if( $rd == $erid )
                {
                    $b .= $db->prepare_query( 'b.rid = %s', $rd );
                }
                else
                {
                    $b .= $db->prepare_query( 'b.rid = %s OR ', $rd );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' b.rid = %s', $rid );
        }
    }

    if( !empty( $tid ) )
    {
        if( is_array( $tid ) )
        {
            $etid = end( $tid );

            $b .= ' AND ( ';

            foreach( $tid as $td )
            {
                list( $bdfrom, $bdto ) = explode( ' - ', $td );

                if( $td == $etid )
                {
                    $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s )', $bdfrom, $bdto );
                }
                else
                {
                    $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s ) OR ', $bdfrom, $bdto );
                }
            }

            $b .= ' )';
        }
        else
        {
            list( $bdfrom, $bdto ) = explode( ' - ', $rid );

            $b = $db->prepare_query( ' ( b.bdfrom = %s AND b.bdto = %s )', $bdfrom, $bdto );
        }
    }

	if( $pmethod != '' )
	{
		if( is_array( $pmethod ) )
		{
			$epmethod = end( $pmethod );

			$b .= ' AND ( ';

			foreach( $pmethod as $pm )
			{
				if( $pm == $epmethod )
				{
						$b .= $db->prepare_query( 'd.agpayment_type = %s', $pm );
				}
				else
				{
						$b .= $db->prepare_query( 'd.agpayment_type = %s OR ', $pm );
				}
			}

			$b .= ' )';
		}
		else
		{
			$b = $db->prepare_query( ' b.bdpstatus = %s', $bstatus );
		}
	}

	if( $bmethod != '' )
	{
		if( is_array( $bmethod ) )
		{
			$ebmethod = end( $bmethod );

			$b .= $db->prepare_query( 'HAVING bmethod_by = ' );

			foreach( $bmethod as $bm )
			{
				if( $bm == $ebmethod )
				{
						$b .= $db->prepare_query( ' %s', $bm );
				}
				else
				{
						$b .= $db->prepare_query( '%s OR ', $bm );
				}
			}

		}
		else
		{
			$b = $db->prepare_query( ' b.bdpstatus = %s', $bstatus );
		}
	}

    if( empty( $w ) && empty( $b ) )
    {
        $where = ' WHERE a.bstt <> "ar" AND b.bdpstatus NOT IN ("cn", "bc", "pf", "rf")';
    }
    elseif( empty( $w ) && !empty( $b ) )
    {
        $where = ' WHERE a.bstt <> "ar" ' . $b;
    }
    elseif( !empty( $w ) && empty( $b ) )
    {
        $where = ' WHERE a.bstt <> "ar" AND b.bdpstatus NOT IN ("cn","bc","pf","rf") AND ' . implode( ' AND', $w );
    }
    else
    {
        $where = ' WHERE a.bstt <> "ar" ' . $b . ' AND ' . implode( ' AND', $w );
    }

    $qr = '';
    $j  = '';

    if ( $rview == 2 )
    {
        $qr = 'b.bdid,
               e.lcountry_id,
               f.lcountry,
               IF( e.bptype = "adult", 1, 0 ) AS num_adult,
               IF( e.bptype = "child", 1, 0 ) AS num_child,
               IF( e.bptype = "infant", 1, 0 ) AS num_infant ';

        $j  = 'LEFT JOIN ticket_booking_passenger AS e ON e.bdid = b.bdid
               LEFT JOIN l_country AS f ON e.lcountry_id = f.lcountry_id';
    }
    else
    {
        $qr = 'b.num_adult,
               b.num_child,
               b.num_infant,
               b.num_adult + b.num_child + b.num_infant AS amount,
               d.agname ';
        $j  = '';
    }

    $q = 'SELECT
            a.bid,
            a.chid,
            a.bticket,
            a.agid,
            a.sagid,
            b.bddate,
            c.chcode,
            c.chname,
            d.agsp,
			CASE
				WHEN( ( a.bmethod_by IS NULL ) AND ( a.chid = "15" ) ) THEN "0"
				WHEN( ( a.bmethod_by IS NULL ) AND ( a.chid <> "15" ) AND ( a.agid IS NOT NULL ) ) THEN "1"
				WHEN( ( a.bmethod_by IS NULL ) AND ( a.chid <>"15" ) AND ( a.agid IS NULL ) ) THEN "2"
				ELSE a.bmethod_by
			END AS bmethod_by,
            DATE_FORMAT(b.bddate, "%m") AS bdmonth,
            DATE_FORMAT(b.bddate, "%Y") AS bdyear,'
            . $qr .
         'FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          LEFT JOIN ticket_agent AS d ON a.agid = d.agid '
          . $j . $where . ' ORDER BY bdyear, bdmonth, a.bticket';

    $r = $db->do_query( $q );

    $items  = array();
    $itemss = array();
    $data   = array();
    $yarray = array();
    $marray = array();
    $carray = array();
    $rcount = 0;

    if ( $rview == 2 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $y = date( 'Y', strtotime( $d['bddate'] ) );
            $m = date( 'F', strtotime( $d['bddate'] ) );

            $data[ $d['bdid'] ][] = $d;

            $rcount++;

            if( isset( $yarray[ $y ] ) )
            {
                $yarray[ $y ] += 1;
            }
            else
            {
                $yarray[ $y ] = 1;
            }

            if( isset( $marray[ $y ][ $m ] ) )
            {
                $marray[ $y ][ $m ] += 1;
            }
            else
            {
                $marray[ $y ][ $m ] = 1;
            }

            if( isset( $carray[ $y ][ $m ][ $d['lcountry_id'] ] ) )
            {
                $carray[ $y ][ $m ][ $d['lcountry_id'] ] += 1;
            }
            else
            {
                $carray[ $y ][ $m ][ $d['lcountry_id'] ] = 1;
            }
        }

        foreach( $data as $bdid => $d )
        {
            $y = date( 'Y', strtotime( $d[0]['bddate'] ) );
            $m = date( 'F', strtotime( $d[0]['bddate'] ) );

            $ytotal = $yarray[ $y ];
            $mtotal = $marray[ $y ][ $m ];
            $index  = base64_encode( json_encode( array( $y, $ytotal, round( ( $ytotal * 100 ) / $rcount, 2 ) ) ) );
            $group  = base64_encode( json_encode( array( $m, $mtotal, round( ( $mtotal * 100 ) / $ytotal, 2 ) ) ) );

            $adult  = array_sum( array_column( $d, 'num_adult' ) );
            $child  = array_sum( array_column( $d, 'num_child' ) );
            $infant = array_sum( array_column( $d, 'num_infant' ) );
            $amount = $adult + $child + $infant;

            $ctotal = $carray[ $y ][ $m ][ $d[0]['lcountry_id'] ];
            $childs = base64_encode( json_encode( array( $d[0]['lcountry_id'], $d[0]['lcountry'], $ctotal ) ) );

            $items[ $index ][ $group ][ $childs ][] = array(
                'bid'       => $d[0]['bid'],
                'chid'      => $d[0]['chid'],
                'chcode'    => $d[0]['chcode'],
                'chname'    => $d[0]['chname'],
                'bticket'   => $d[0]['bticket'],
                'amount'    => $amount,
                'num_adult' => $adult,
                'num_child' => $child,
                'num_infant' => $infant,
                'agname'    => '',
            );
        }
    }
    else
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $y = date( 'Y', strtotime( $d['bddate'] ) );
            $m = date( 'F', strtotime( $d['bddate'] ) );

            $rcount = $rcount + $d['amount'];

            $data[] = $d;

            if( isset( $yarray[ $y ] ) )
            {
                $yarray[ $y ] += $d['amount'];
            }
            else
            {
                $yarray[ $y ] = $d['amount'];
            }

            if( isset( $marray[ $y ][ $m ] ) )
            {
                $marray[ $y ][ $m ] += $d['amount'];
            }
            else
            {
                $marray[ $y ][ $m ] = $d['amount'];
            }

            if( $rview == 1 )
            {
                if( isset( $carray[ $y ][ $m ][ $d['agid'] ] ) )
                {
                    $carray[ $y ][ $m ][ $d['agid'] ] += $d['amount'];
                }
                else
                {
                    $carray[ $y ][ $m ][ $d['agid'] ] = $d['amount'];
                }
            }
            else
            {
                if( isset( $carray[ $y ][ $m ][ $d['chid'] ] ) )
                {
                    $carray[ $y ][ $m ][ $d['chid'] ] += $d['amount'];
                }
                else
                {
                    $carray[ $y ][ $m ][ $d['chid'] ] = $d['amount'];
                }
            }
        }

        foreach( $data as $d )
        {
            $y = date( 'Y', strtotime( $d['bddate'] ) );
            $m = date( 'F', strtotime( $d['bddate'] ) );

            $ytotal = isset( $yarray[ $y ] ) ? $yarray[ $y ] : 0;
            $mtotal = isset( $marray[ $y ][ $m ] ) ? $marray[ $y ][ $m ] : 0;

            $ytotal_percent = $ytotal == 0 ? 0 : round( ( $ytotal * 100 ) / $rcount, 2 );
            $mtotal_percent = $mtotal == 0 ? 0 : round( ( $mtotal * 100 ) / $ytotal, 2 );

            $index  = base64_encode( json_encode( array( $y, $ytotal, $ytotal_percent ) ) );
            $group  = base64_encode( json_encode( array( $m, $mtotal, $mtotal_percent ) ) );

            if ( $rview == 0 )
            {
                $ctotal = isset( $carray[ $y ][ $m ][ $d['chid'] ] ) ? $carray[ $y ][ $m ][ $d['chid'] ] : 0;
                $child  = base64_encode( json_encode( array( $d['chcode'], $d['chname'], $ctotal ) ) );
            }
            else
            {
                $ctotal  = isset( $carray[ $y ][ $m ][ $d['agid'] ] ) ? $carray[ $y ][ $m ][ $d['agid'] ] : 0;
                $bsource = empty( $d['agid'] ) ? $d['chname'] : get_agent( $d['agid'], 'agname' );
                $child   = base64_encode( json_encode( array( $d['chcode'], $bsource, $ctotal ) ) );
            }

            $items[ $index ][ $group ][ $child ][] = array(
                'bid'        => $d['bid'],
                'agid'       => $d['agid'],
                'sagid'      => $d['sagid'],
                'chid'       => $d['chid'],
                'agname'     => $d['agname'],
                'chcode'     => $d['chcode'],
                'chname'     => $d['chname'],
                'amount'     => $d['amount'],
                'bticket'    => $d['bticket'],
                'num_adult'  => $d['num_adult'],
                'num_child'  => $d['num_child'],
                'num_infant' => $d['num_infant'],
            );
        }
    }

    $category = array();
    $series   = array();
    foreach( $items as $obj => $groups )
    {
        list( $bdyear, $total_seat_by_year, $total_percent_seat_by_year ) = json_decode( base64_decode( $obj ) );

        $ind  = base64_encode( json_encode( array( $bdyear, number_format( $total_seat_by_year, 0, '.' , '.' ), $total_percent_seat_by_year ) ) );

        foreach( $groups as $objs => $group )
        {
            list( $bdmonth, $total_seat_by_month, $total_percent_seat_by_month ) = json_decode( base64_decode( $objs ) );

            $series[ $bdyear ][ $bdmonth ][] = $total_seat_by_month;

            if( !in_array( $bdmonth, $category ) )
            {
                $category[] = $bdmonth;
            }

            $grps  = base64_encode( json_encode( array( $bdmonth, number_format( $total_seat_by_month, 0, '.' , '.' ), $total_percent_seat_by_month ) ) );

            foreach( $group as $subgroup => $child )
            {
                if( $rview == 0 )
                {
                    list( $chcode, $chname, $num ) = json_decode( base64_decode( $subgroup ) );

                    $percent = round( ( $num * 100 ) / $total_seat_by_month, 2 );
                    $stitle  = '(' . $chcode . ') ' . $chname;
                }
                elseif( $rview == 1 )
                {
                    list( $chcode, $bsource, $num ) = json_decode( base64_decode( $subgroup ) );

                    $percent = round( ( $num * 100 ) / $total_seat_by_month, 2 );
                    $stitle  = '(' . $chcode . ') ' . $bsource;
                }
                elseif( $rview == 2 )
                {
                    list( $lcountry_id, $lcountry, $num ) = json_decode( base64_decode( $subgroup ) );

                    $percent = round( ( $num * 100 ) / $total_seat_by_month, 2 );
                    $stitle  = $lcountry;
                }

                $chl  = base64_encode( json_encode( array( $stitle, number_format( $num, 0, '.' , '.' ), $percent ) ) );

                foreach( $child as $d )
                {
                    $itemss[ $ind ][ $grps ][ $chl ][] = array(
                        'bticket'    => $d['bticket'],
                        'num_adult'  => $d['num_adult'],
                        'num_child'  => $d['num_child'],
                        'num_infant' => $d['num_infant'],
                        'agname'     => empty( $d['agname'] ) ? $d['chname'] : $d['agname'],
                        'amount'     => $d['amount'],
                    );
                }
            }
        }
    }

    if ( $rcount == 0 )
    {
        return array( 'total' => 0 );
    }
    else
    {
        return array( 'total' => number_format( $rcount, 0, '.' , '.' ), 'items' => $itemss, 'chart' => get_chart_options( $category, $series ), 'view' => $rview, 'filter' => $fltr );
    }
}

function get_ticket_revenue_report( $filter )
{
    global $db;

    $w = array();

    $odate = '';
    $based = '';

    $chid_net_price = array( 3, 37, 5, 30, 24, 25 );

    extract( $filter );

    if( $rtype != '' && $bdate != '' )
    {
        $based = ( $rbased == 1 ) ? 'e.bddate AS bdate,' : 'a.bdate AS bdate,';
        $odate = ( $rbased == 1 ) ? 'e.bddate' : 'a.bdate';

        if( $rtype == '0'  )
        {
            $date = date( 'Y-m-d', strtotime( $bdate ) );

            $w[]  = $db->prepare_query( $odate .' = %s', $date );
        }
        elseif( $rtype == '1' )
        {
            $date = date( 'Y-m-d', strtotime( '1 ' . $bdate ) );

            $w[]  = $db->prepare_query( ' YEAR( ' . $odate . ' ) = YEAR( %s ) AND MONTH( '. $odate .' ) = MONTH( %s )', $date, $date );
        }
        elseif( $rtype == '2' )
        {
            $date = date( 'Y-m-d', strtotime( '1 Jan ' . $bdate ) );

            $w[]  = $db->prepare_query( ' YEAR( '. $odate .' ) = YEAR( %s )', $date );
        }
        elseif( $rtype == '3' )
        {
            $date_from = date( 'Y-m-d', strtotime( $bdate ) );
            $date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

            $w[] = $db->prepare_query( $odate .' BETWEEN %s AND %s', $date_from, $date_to );
        }
        elseif( $rtype == '4' )
        {
            $year_from = date( 'Y-m-d', strtotime( '1 Jan ' . $bdate ) );
            $year_to   = empty( $bdateto ) ? $year_from : date( 'Y-m-d', strtotime( '1 Jan ' . $bdateto ) );

            $w[] = $db->prepare_query( ' YEAR( '. $odate .' ) BETWEEN YEAR( %s ) AND YEAR( %s )', $year_from, $year_to );
        }
    }

    if( !empty( $bsource ) || !empty( $chid ) )
    {
        $bs = '( ';

        if ( !empty( $chid ) )
        {
            if ( count( $chid ) > 0 )
            {
                $end_chid = end( $chid );

                foreach( $chid as $chidx )
                {
                    if( $chidx == $end_chid )
                    {
                        $bs .= $db->prepare_query( 'a.chid = %d', $chidx );
                    }
                    else
                    {
                        $bs .= $db->prepare_query( 'a.chid = %d OR ', $chidx );
                    }
                }
            }
            else
            {
                $bs .= $db->prepare_query( 'a.chid = %d', $chid[0] );
            }
        }

        if ( !empty( $bsource ) )
        {
            $bs .= !empty( $chid ) ? ' OR ' : '';

            $end_bsource = end( $bsource );

            foreach( $bsource as $parts )
            {
                if( strpos( $parts, '|' ) )
                {
                    list( $chidd, $agid ) = explode( '|', $parts );

                    if( $parts == $end_bsource )
                    {
                        $bs .= $db->prepare_query( 'a.agid = %d', $agid );
                    }
                    else
                    {
                        $bs .= $db->prepare_query( 'a.agid = %d OR ', $agid );
                    }
                }
                else
                {
                    if( $parts == $end_bsource )
                    {
                        $bs .= $db->prepare_query( 'a.chid = %d', $parts );
                    }
                    else
                    {
                        $bs .= $db->prepare_query( 'a.chid = %d OR ', $parts );
                    }
                }
            }
        }

        $bs .= ' )';

        $w[] = $bs;
    }

    if( !empty( $bstatus ) )
    {
        $w[] = $db->prepare_query( 'e.bdpstatus IN ( "cn", "' . ( implode( '", "', $bstatus ) ) . '" )' );
    }

    if( !empty( $deport ) )
    {
        $w[] = $db->prepare_query( 'e.bdfrom_id IN ( "' . ( implode( '", "', $deport ) ) . '" )' );
    }

    if( !empty( $boid ) )
    {
        $w[] .= $db->prepare_query( 'e.boid = %d', $boid );
    }

    if ( !empty( $agsp ) )
    {
        $w[] .= $db->prepare_query( 'd.agsp = %s', $agsp );
    }

    if( empty( $w ) )
    {
        $where = 'WHERE a.bstt <> "ar" AND e.bdpstatus NOT IN( "pf", "bc", "rf" )';
    }
    else
    {
        $where = 'WHERE a.bstt <> "ar" AND ' . implode( ' AND ', $w );
    }

    $s = 'SELECT
            '. $based . '
            a.btype,
            a.bdate AS dtt,
            a.bid,
            a.bticket,
            a.btotal,
            a.bdiscount,
            c.chcode,
            c.chid,
            c.chname,
            d.agname,
            e.discount,
            e.bdcancelfee,
            e.bdtransactionfee,
            e.bdrefund,
            e.bdstatus,
            e.bdpstatus,
            a.bstact,
            a.bcancellationfee,
            a.btransactionfee,
            a.bpaymethod,
            a.pmcode,
            a.bdiscounttype,
            a.bcommissioncondition,
            a.bcommissiontype,
            a.bcommission,
            a.bdiscountnum,
            a.bupdated,
            d.agpayment_type,
            d.agid,
            d.agsp,
            d.agtype,
            e.price_per_adult,
            e.selling_price_per_adult,
            e.net_price_per_adult,
            e.disc_price_per_adult,
            e.num_adult,
            e.price_per_child,
            e.selling_price_per_child,
            e.net_price_per_child,
            e.disc_price_per_child,
            e.num_child,
            e.price_per_infant,
            e.selling_price_per_infant,
            e.net_price_per_infant,
            e.disc_price_per_infant,
            e.num_infant,
            e.bdfrom_id,
            e.bdto_id,
            ( e.bdcancelfee + e.bdtransactionfee ) AS fee,
            ( a.bcancellationfee + a.btransactionfee ) AS bfee,
            CONCAT_WS( " - ", e.bdfrom, e.bdto ) AS route_point,
            e.transport_fee,
            e.total,
            e.add_ons,(
                SELECT SUM( a2.bpcancelfee + a2.bptransactionfee ) FROM ticket_booking_passenger AS a2
                WHERE a2.bdid = e.bdid GROUP BY a2.bdid
            ) AS trip_fee,
            (
                SELECT SUM( a2.bprefund ) FROM ticket_booking_passenger AS a2
                WHERE a2.bdid = e.bdid GROUP BY a2.bdid
            ) AS trip_refund
         FROM ticket_booking AS a
         LEFT JOIN ticket_channel AS c ON a.chid = c.chid
         LEFT JOIN ticket_agent AS d ON a.agid = d.agid OR a.sagid = d.agid
         LEFT JOIN ticket_booking_detail AS e ON a.bid = e.bid ' . $where . '
         ORDER BY ' . $odate . ', a.bid ASC';
    $r = $db->do_query( $s );

    $items   = array();
    $sumdate = array();
    $sumgrps = array();
    $datas   = array();

    $tyears  = array();
    $tmonth  = array();

    $summary = 0;
    $booking = 0;

    while( $d = $db->fetch_array( $r ) )
    {
        switch( $filter['rview'] )
        {
            case 4:
                $method = get_booking_payment_method( array( 'bid' => $d['bid'], 'agid' => $d['agid'], 'bpaymethod' => $d['bpaymethod'] ) );
                $source = $method;
                $fgroup = $method;
                break;
            case 3:
                $source = $d['route_point'];
                $fgroup = $d['route_point'];
                break;
            case 2:
                $source = $d['agsp'];
                $fgroup = $d['agsp'];
                break;
            case 1:
                $source = empty( $d['agname'] ) ? $d['chname'] : $d['agname'];
                $fgroup = $d['chcode'];
                break;
            default:
                $source = $d['chname'];
                $fgroup = $d['chcode'];
            break;
        }

        if( empty( $source ) )
        {
            continue;
        }

        if( !in_array( 'cn', $bstatus ) )
        {
            if( ( in_array( $d['bstact'], array( 2, 3, 4 ) ) && $d['bdpstatus'] == 'cn' && $d['fee'] == 0 && $d['bfee'] == 0 ) || ( $d['bdpstatus'] == 'cn' && $d['fee'] == 0 && $d['bfee'] == 0 ) )
            {
                continue;
            }
        }

        if( $feeopt == 1 )
        {
            $d['transport_fee'] = 0;
        }

        if( $addonsopt == 1 )
        {
            $d['add_ons'] = 0;
        }

        $booking_already_edited = $d['bupdated'];

        if( $d['net_price_per_adult'] != 0 || $d['net_price_per_child'] != 0 || $d['net_price_per_infant'] != 0 || in_array( $d['chid'], $chid_net_price ) )
        {
            if( $booking_already_edited )
            {
                $net_price_adlt = $d['net_price_per_adult'];
                $net_price_chld = $d['net_price_per_child'];
                $net_price_infn = $d['net_price_per_infant'];
            }
            else
            {
                $net_price_adlt = ( $d['price_per_adult'] != $d['net_price_per_adult'] ) ? $d['net_price_per_adult'] : ( $d['net_price_per_adult'] - $d['disc_price_per_adult'] );
                $net_price_chld = ( $d['price_per_child'] != $d['net_price_per_child'] ) ? $d['net_price_per_child'] : ( $d['net_price_per_child'] - $d['disc_price_per_child'] );
                $net_price_infn = ( $d['price_per_infant'] != $d['net_price_per_infant'] ) ? $d['net_price_per_infant'] : ( $d['net_price_per_infant'] - $d['disc_price_per_infant'] );
            }
        }
        else
        {
            $net_price_adlt = $d['price_per_adult'] - $d['disc_price_per_adult'];
            $net_price_chld = $d['price_per_child'] - $d['disc_price_per_child'];
            $net_price_infn = $d['price_per_infant'] - $d['disc_price_per_infant'];

            if( $d['agtype'] == 'Net Rate' )
            {
                $rate = get_agent_net_rate( $d['agid'], $d['bdfrom_id'], $d['bdto_id'] );

                if( !empty( $rate ) )
                {
                    if( !empty( $rate[0]['agdatevalidity'] ) )
                    {
                        foreach( $rate[0]['agnetprice'] as $date => $dt )
                        {
                            $darray = explode( '|' , $date );

                            foreach( $dt as $lc )
                            {
                                if( check_in_range_2( date( 'd-m-Y', strtotime( $darray[0] ) ), date( 'd-m-Y', strtotime( $darray[1] ) ), date( 'd-m-Y', strtotime( $d['dtt']) ) ) )
                                {
                                    if( empty( $d['pmcode'] ) )
                                    {
                                        if( $d['price_per_adult'] == 0 && $d['net_price_per_adult'] == 0 )
                                        {
                                            $net_price_adlt = $d['net_price_per_adult'];
                                        }
                                        else
                                        {
                                            if( $d['net_price_per_adult'] != 0 )
                                            {
                                                $net_price_adlt = $d['net_price_per_adult'];
                                            }
                                            else
                                            {
                                                if( $lc['adult_rate_inc_trans'] != 0 )
                                                {
                                                    $net_price_adlt = $lc['adult_rate_inc_trans'] - $d['disc_price_per_adult'];
                                                }
                                            }
                                        }

                                        if( $d['price_per_child'] == 0 && $d['net_price_per_child'] == 0 )
                                        {
                                            $net_price_chld = $d['net_price_per_child'];
                                        }
                                        else
                                        {
                                            if( $d['net_price_per_child'] != 0 )
                                            {
                                                $net_price_chld = $d['net_price_per_child'];
                                            }
                                            else
                                            {
                                                if( $lc['child_rate_inc_trans'] != 0 )
                                                {
                                                    $net_price_chld = $lc['child_rate_inc_trans'] - $d['disc_price_per_child'];
                                                }
                                            }
                                        }

                                        if( $d['price_per_infant'] == 0 && $d['net_price_per_infant'] == 0 )
                                        {
                                            $net_price_infn = $d['net_price_per_infant'];
                                        }
                                        else
                                        {
                                            if( $d['net_price_per_infant'] != 0 )
                                            {
                                                $net_price_infn = $d['net_price_per_infant'];
                                            }
                                            else
                                            {
                                                if( $lc['infant_rate_inc_trans'] != 0 )
                                                {
                                                    $net_price_infn = $lc['infant_rate_inc_trans'] - $d['disc_price_per_infant'];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if( $booking_already_edited === '0' )
        {
            //-- Check discount per trip
            //-- 0 = %
            //-- 1 = Rp.
            if( $d['bdiscounttype'] == '0' )
            {
                $d_disc_adult = ( $net_price_adlt * $d['bdiscountnum'] ) / 100;
                $d_disc_child = ( $net_price_chld * $d['bdiscountnum'] ) / 100;
                $d_disc_infnt = ( $net_price_infn * $d['bdiscountnum'] ) / 100;
            }
            elseif( $d['bdiscounttype'] == '2' )
            {
                $d_disc_adult = $net_price_adlt - $d['bdiscountnum'];
                $d_disc_child = $net_price_chld - $d['bdiscountnum'];
                $d_disc_infnt = $net_price_chld - $d['bdiscountnum'];

                $d_disc_adult = $d_disc_adult < 0 ? 0 : $d_disc_adult;
                $d_disc_child = $d_disc_child < 0 ? 0 : $d_disc_child;
                $d_disc_infnt = $d_disc_infnt < 0 ? 0 : $d_disc_infnt;
            }
            else
            {
                $d_disc_adult = $d['bdiscountnum'];
                $d_disc_child = $d['bdiscountnum'];
                $d_disc_infnt = $d['bdiscountnum'];
            }

            //-- Check again commission condition
            //-- If use discount => total - bdiscount
            if( $d['bcommissioncondition'] == '0' )
            {
                $d_price_adult = $net_price_adlt - $d_disc_adult;
                $d_price_child = $net_price_chld - $d_disc_child;
                $d_price_infnt = $net_price_infn - $d_disc_infnt;
            }
            else
            {
                $d_price_adult = $net_price_adlt;
                $d_price_child = $net_price_chld;
                $d_price_infnt = $net_price_infn;
            }

            //-- Check Commission Type
            //-- 0 = %
            //-- 1 = Rp.
            if( $d['bcommissiontype'] == '0' )
            {
                $d_commission_adult = $d_price_adult * ( $d['bcommission'] / 100 );
                $d_commission_child = $d_price_child * ( $d['bcommission'] / 100 );
                $d_commission_infnt = $d_price_infnt * ( $d['bcommission'] / 100 );
            }
            else
            {
                $d_commission_adult = $d_price_adult * ( $d['bcommission'] / $d_price_adult );
                $d_commission_child = $d_price_child * ( $d['bcommission'] / $d_price_child );
                $d_commission_infnt = $d_price_infnt * ( $d['bcommission'] / $d_price_infnt );
            }

            $net_price_adlt = $net_price_adlt - $d_disc_adult - $d_commission_adult;
            $net_price_chld = $net_price_chld - $d_disc_child - $d_commission_child;
            $net_price_infn = $net_price_infn - $d_disc_infnt - $d_commission_infnt;

            $net_price_adlt = $net_price_adlt < 0 ? 0 : $net_price_adlt;
            $net_price_chld = $net_price_chld < 0 ? 0 : $net_price_chld;
            $net_price_infn = $net_price_infn < 0 ? 0 : $net_price_infn;
        }

        $subtotal_trip = ( ( $net_price_adlt * $d['num_adult'] ) + ( $net_price_chld * $d['num_child'] ) + ( $net_price_infn * $d['num_infant'] ) ) + $d['transport_fee'] + $d['add_ons'];

        if( $d['bstact'] == 4 )
        {
            //-- Cancel Passanger
            $bdsubtotal = $subtotal_trip + $d['trip_fee'];
        }
        elseif( $d['bstact'] == 6 )
        {
            //-- New Cancel Modul
            if( $d['fee'] >= $d['total'] )
            {
                $bdsubtotal = $d['fee'];
            }
            else
            {
                $bdsubtotal = $d['total'] + $d['fee'];
            }
        }
        else
        {
            //-- Cancel Trip && CAncel Booking
            if ( $d['bdstatus'] == 'cn' && !in_array( 'cn', $bstatus ) )
            {
                if( $d['btype'] == 1 & $rbased == 0 )
                {
                    if( $d['fee'] == 0 && $d['bfee'] != 0 )
                    {
                        $bdsubtotal = $d['bfee'] / 2;
                    }
                    else
                    {
                        $bdsubtotal = $d['fee'];
                    }
                }
                else
                {
                    $bdsubtotal = $d['fee'];
                }
            }
            else
            {
                $bdsubtotal = $subtotal_trip + $d['fee'];
            }
        }

        if( $bdsubtotal <= 0 )
        {
            continue;
        }

        $summary += $bdsubtotal;
        $booking += 1;

        if ( $rtype == '4' )
        {
            $month  = date( 'F', strtotime( $d['bdate'] ) );
            $year   = date( 'Y', strtotime( $d['bdate'] ) );

            if ( isset( $tyears[ $year ] ) )
            {
                $tyears[ $year ]['y']         = $year;
                $tyears[ $year ]['ytotal']   += $bdsubtotal;
                $tyears[ $year ]['ybooking'] += 1;
            }
            else
            {
                $tyears[ $year ]['y']        = $year;
                $tyears[ $year ]['ytotal']   = $bdsubtotal;
                $tyears[ $year ]['ybooking'] = 1;
            }

            if ( isset( $tmonth[ $year ][ $month ] ) )
            {
                $tmonth[ $year ][ $month ]['year']      = $year;
                $tmonth[ $year ][ $month ]['month']     = $month;
                $tmonth[ $year ][ $month ]['mtotal']   += $bdsubtotal;
                $tmonth[ $year ][ $month ]['mbooking'] += 1;
            }
            else
            {
                $tmonth[ $year ][ $month ]['year']     = $year;
                $tmonth[ $year ][ $month ]['month']    = $month;
                $tmonth[ $year ][ $month ]['mtotal']   = $bdsubtotal;
                $tmonth[ $year ][ $month ]['mbooking'] = 1;
            }
        }
        else
        {
            $date = $rtype == '2' ? date( 'F Y', strtotime( $d['bdate'] ) ) : date( 'd F Y', strtotime( $d['bdate'] ) );

            if ( isset( $sumdate[ $date ] ) )
            {
                $sumdate[ $date ]['date']   = $date;
                $sumdate[ $date ]['sdate'] += $bdsubtotal;
                $sumdate[ $date ]['cdate'] += 1;
            }
            else
            {
                $sumdate[ $date ]['date']  = $date;
                $sumdate[ $date ]['sdate'] = $bdsubtotal;
                $sumdate[ $date ]['cdate'] = 1;
            }

            if ( isset( $sumgrps[ $date ][ $source ] ) )
            {
                $sumgrps[ $date ][ $source ]['source']  = $source;
                $sumgrps[ $date ][ $source ]['fgroup']  = $fgroup;
                $sumgrps[ $date ][ $source ]['sgroup'] += $bdsubtotal;
                $sumgrps[ $date ][ $source ]['cgroup'] += 1;
            }
            else
            {
                $sumgrps[ $date ][ $source ]['source'] = $source;
                $sumgrps[ $date ][ $source ]['fgroup'] = $fgroup;
                $sumgrps[ $date ][ $source ]['sgroup'] = $bdsubtotal;
                $sumgrps[ $date ][ $source ]['cgroup'] = 1;
            }

            $datas[ $date ][ $source ][ $d['bid'] ][] = array(
                'bdate'   => $d['bdate'],
                'bticket' => $d['bticket'],
                'btotal'  => $bdsubtotal,
                'chid'    => $d['chid'],
                'chcode'  => $d['chcode'],
                'chname'  => $d['chname'],
                'agname'  => $d['agname']
            );
        }
    }

    if ( $rtype == '4' )
    {
        foreach ( $tmonth as $yindx => $obj )
        {
            $ytotals  = isset( $tyears[ $yindx ]['ytotal'] ) ? $tyears[ $yindx ]['ytotal'] : 0;
            $tbooking = isset( $tyears[ $yindx ]['ybooking'] ) ? $tyears[ $yindx ]['ybooking'] : 0;
            $tavg     = round( ( $ytotals * 100 ) / $summary, 2 );

            $index    = base64_encode( json_encode( array( $yindx, number_format( $ytotals, 0, '.' , '.' ), $tavg, number_format( $tbooking, 0, '.' , '.' ) ) ) );

            foreach( $obj as $midx => $dt  )
            {
                $mtotals   = isset( $tmonth[ $yindx ][ $midx ]['mtotal'] ) ? $tmonth[ $yindx ][ $midx ]['mtotal'] : 0;
                $mbookings = isset( $tmonth[ $yindx ][ $midx ]['mbooking'] ) ? $tmonth[ $yindx ][ $midx ]['mbooking'] : 0;
                $mavg      = round( ( $mtotals * 100 ) / $summary, 2 );

                $items[ $index ][] = array(
                    'bmonth'    => $midx,
                    'mtotal'    => number_format( $mtotals, 0, '.' , '.' ),
                    'mavg'      => $mavg,
                    'mbooking'  => number_format( $mbookings, 0, '.' , '.' )
                );
            }
        }
    }
    else
    {
        foreach ( $datas as $bdate => $obj )
        {
            $totaldate = isset( $sumdate[ $bdate ]['sdate'] ) ? $sumdate[ $bdate ]['sdate'] : 0;
            $countdate = isset( $sumdate[$bdate]['cdate'] ) ? $sumdate[$bdate]['cdate'] : 0;
            //$avdate    = round( ( $totaldate * 100 ) / $summary, 2 );
            $avdate    = $summary > 0 ? round( ( $totaldate * 100 ) / $summary, 2 ) : 0;
            $index     = base64_encode( json_encode( array( $bdate, number_format( $totaldate, 0, '.' , '.' ), $avdate, number_format( $countdate, 0, '.' , '.' ) ) ) );

            foreach( $obj as $src => $dts  )
            {
                $totalgroup = isset( $sumgrps[$bdate][$src]['sgroup'] ) ? $sumgrps[$bdate][$src]['sgroup'] : 0;
                $countgroup = isset( $sumgrps[$bdate][$src]['cgroup'] ) ? $sumgrps[$bdate][$src]['cgroup'] : 0;
                //$avgroup    = round( ( $totalgroup * 100 ) / $summary, 2 );
                $avgroup    = $summary > 0 ? round( ( $totalgroup * 100 ) / $summary, 2 ) : 0;
                $group      = base64_encode( json_encode( array( $sumgrps[$bdate][$src]['fgroup'], $src, number_format( $totalgroup, 0, '.' , '.' ), $avgroup, number_format( $countgroup, 0, '.' , '.' ) ) ) );

                foreach( $dts as $bid => $dt )
                {
                    $num_trip = get_num_count_trip( $bid );

                    if( $rbased == 1 )
                    {
                        //-- Separate data by travel date
                        foreach( $dt as $d )
                        {
                            $promo_code_trip = $d['btotal'] - ( get_discount_promo_code( $bid ) / $num_trip );

                            $items[ $index ][ $group ][] = array(
                                'bid'       => $bid,
                                'bdate'     => date( 'd F Y', strtotime( $d['bdate'] ) ),
                                'bticket'   => $d['bticket'],
                                'btotal'    => number_format( $d['btotal'], 0, '.' , '.' ),
                                //'btotal'    => number_format( $promo_code_trip, 0, '.' , '.' ),
                                'chid'      => $d['chid'],
                                'chcode'    => $d['chcode'],
                                'agname'    => ( empty( $d['agname'] ) ? $d['chname'] : $d['agname'] )
                            );
                        }
                    }
                    else
                    {
                        //-- Merge data by booking date
                        $btotal = 0;

                        foreach( $dt as $d )
                        {
                            $btotal += $d['btotal'];
                        }

                        $items[ $index ][ $group ][] = array(
                            'bid'       => $bid,
                            'bdate'     => date( 'd F Y', strtotime( $d['bdate'] ) ),
                            'bticket'   => $d['bticket'],
                            'btotal'    => number_format( $btotal, 0, '.' , '.' ),
                            //'btotal'    => number_format( ( $btotal - get_discount_promo_code( $bid ) ), 0, '.' , '.' ),
                            'chid'      => $d['chid'],
                            'chcode'    => $d['chcode'],
                            'agname'    => ( empty( $d['agname'] ) ? $d['chname'] : $d['agname'] )
                        );
                    }
                }
            }
        }
    }

    $chart    = '';

    if ( $rtype == '4' )
    {
        $category = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $series   = array();

        foreach ( $items as $obj => $groups )
        {
            list( $rev_year, $total_years, $total_percent_per_year, $booking_year ) = json_decode( base64_decode( $obj ) );

            $frev_date  = $rev_year;

            foreach ( $groups as $val )
            {
                if ( isset( $series[ $frev_date ][ $val['bmonth'] ] ) )
                {
                    $series[ $frev_date ][ $val['bmonth'] ][] = 0;
                }
                else
                {
                    $series[ $frev_date ][ $val['bmonth'] ][] = str_replace( '.', '', $val['mtotal'] );
                }
            }
        }

        $chart = get_chart_options( $category, $series, 'column' );
    }
    else
    {
        $category = array();
        $series   = array();

        foreach( $items as $obj => $groups )
        {
            list( $rev_date, $total_revenue_by_date, $total_percent_revenue_by_date, $booking_by_date ) = json_decode( base64_decode( $obj ) );

            $frev_date  = $rtype == '2' ? $rev_date : date( 'd M Y', strtotime( $rev_date ) );
            $category[] = $frev_date;

            foreach( $groups as $channel => $group )
            {
                list( $chcode, $chname, $total_revenue_by_channel, $total_percent_revenue_by_channel, $booking_by_channel ) = json_decode( base64_decode( $channel ) );

                if ( $rview > 1 )
                {
                    $series[ $chname ][ $frev_date ][] = $total_revenue_by_channel;
                }
                else
                {
                    $series[ '(' . $chcode . ') ' . $chname ][ $frev_date ][] = $total_revenue_by_channel;
                }
            }
        }

        $chart = get_chart_options( $category, $series );
    }

    if ( $summary == 0 )
    {
        return array( 'total' => $summary );
    }
    else
    {
        return array( 'total' => number_format( $summary, 0, '.' , '.' ), 'items' => $items, 'booking' => number_format( $booking, 0, '.' , '.' ), 'chart' => $chart, 'rtype' => $rtype, 'rview' => $rview, 'filter' => base64_encode( json_encode( $filter ) ) );
    }
}

function get_ticket_average_rates_report( $filter )
{
    global $db;

	$w     = array();
	$where = '';
	extract( $filter );

    /* YEAR FILTER */
    if( !empty( $ryear ) )
    {
    	$w[] = $db->prepare_query( ' YEAR( a.bdate ) IN ( ' . implode( ',', $ryear ) . ' )' );
    }

    /* ROUTE FILTER */
    if ( !empty( $rid ) )
    {
        $w[] = $db->prepare_query( ' b.rid IN ( ' . implode( ',', $rid ) . ' )' );
    }

    /* BOOKING SOURCE FILTER */
    if ( !empty( $fchid ) )
    {
        $count   = count($fchid);
        $endchid = end( $fchid );

        $ch  = ' ( ';

        if ( empty( $fagid ) )
        {
            foreach ( $fchid as $chid )
            {
                if ( $count == 1 )
                {
                    $ch .= $db->prepare_query( ' a.chid = %d', $chid );
                }
                else
                {
                    $ch .= $db->prepare_query( ' a.chid = %d OR', $chid );
                }

                $count--;
            }

            $ch  .= ' )';

            $w[]  = $ch;
        }
        else
        {
            foreach ( $fchid as $chid )
            {
                if ( $count == 1 )
                {
                        $ch .= $db->prepare_query( ' a.chid = %d', $chid );
                }
                else
                {
                    $ch .= $db->prepare_query( ' a.chid = %d OR', $chid );
                }

                $count--;
            }

            $ch  .= ' )';

            $ag  = ' ( ';

            $enfagid = end( $fagid );

            foreach ( $fagid as $agid )
            {
                if ($agid == $enfagid)
                {
                    $ag  .= $db->prepare_query( ' a.agid = %d', $agid );
                }
                else
                {
                    $ag  .= $db->prepare_query( ' a.agid = %d OR', $agid );
                }
            }

            $ag  .= ' )';
            $w[]  = ' ( ' . $ch . ' OR ' . $ag . ' ) ';
        }
    }

    /* TRIP FILTER */
    if( !empty( $tid ) )
    {
        $etid = end( $tid );
        $b    = '(';

        foreach( $tid as $td )
        {
            list( $bdfrom, $bdto ) = explode( ' - ', $td );

            if( $td == $etid )
            {
                $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s )', $bdfrom, $bdto );
            }
            else
            {
                $b .= $db->prepare_query( '( b.bdfrom = %s AND b.bdto = %s ) OR ', $bdfrom, $bdto );
            }
        }

        $b .= ')';

        $w[] = $b;
    }

    /* SALES CHANNEL FILTER */
    if ( !empty( $agsp ) )
    {
        $w[] = $db->prepare_query( ' g.agsp = %s', $agsp );
    }

    if( !empty( $w ) )
    {
    	$where = ' AND ' . implode( ' AND', $w );
    }

    $s = 'SELECT
            a.bid,
            b.bdid,
            a.bdate,
            YEAR( a.bdate ) AS years,
            MONTH( a.bdate ) AS months,
            a.agid,
            g.agpayment_type,
            b.price_per_adult,
            b.price_per_child,
            b.price_per_infant,
            b.selling_price_per_adult,
            b.selling_price_per_child,
            b.selling_price_per_infant,
            b.disc_price_per_adult,
            b.disc_price_per_child,
            b.disc_price_per_infant,
            b.num_adult,
            b.num_child,
            b.num_infant
          FROM ticket_booking AS a LEFT JOIN ticket_booking_detail AS b ON a.bid = b.bid
          LEFT JOIN ticket_agent AS g ON g.agid = a.agid
		  WHERE a.bstt <> "ar" AND a.bstatus NOT IN ( "cn", "pf", "bc", "rf" )' . $where;
	$r = $db->do_query( $s );
    $n = $db->num_rows( $r );

	if ( $n <= 0 )
	{
		return array( 'total' => 0, 'items' => array(), 'avg_per_year' => array() );
	}
	else
	{
		$atotal   = array();
        $atseat   = array();
		$items    = array();
		$groups   = array();
		$sumtotal = 0;
        $sumseat  = 0;
		$total    = 0;

		while( $d = $db->fetch_array( $r ) )
		{
            if ( $d['agpayment_type'] == 'Pre Paid' )
            {
                $btotal = ( ( $d['selling_price_per_adult'] * $d['num_adult'] ) - $d['disc_price_per_adult'] ) + ( ( $d['selling_price_per_child'] * $d['num_child'] ) - $d['disc_price_per_child'] ) + ( ( $d['selling_price_per_infant'] * $d['num_infant'] ) - $d['disc_price_per_infant'] );
                $tseat  = $d['num_adult'] + $d['num_child'] + $d['num_infant'];
            }
            else
            {
                $btotal = ( ( $d['price_per_adult'] * $d['num_adult'] ) - $d['disc_price_per_adult'] ) + ( ( $d['price_per_child'] * $d['num_child'] ) - $d['disc_price_per_child'] ) + ( ( $d['price_per_infant'] * $d['num_infant'] ) - $d['disc_price_per_infant'] );
                $tseat  = $d['num_adult'] + $d['num_child'] + $d['num_infant'];
            }

			$sumtotal += $btotal;
			$sumseat  += $tseat;

			$index   = $d['years'];
			$group   = date( 'F', strtotime( $d['bdate'] ) );

			if ( isset( $groups[ $index ][ $group ] ) )
			{
				$groups[ $index ][ $group ]['byears'] = $index;
				$groups[ $index ][ $group ]['bmonth'] = $group;
				$groups[ $index ][ $group ]['bdate']  = $d['bdate'];
				$groups[ $index ][ $group ]['btotal'] += $btotal;
				$groups[ $index ][ $group ]['bseat']  += $tseat;
			}
			else
			{
                $groups[ $index ][ $group ]['byears'] = $index;
				$groups[ $index ][ $group ]['bmonth'] = $group;
				$groups[ $index ][ $group ]['bdate']  = $d['bdate'];
				$groups[ $index ][ $group ]['btotal'] = $btotal;
				$groups[ $index ][ $group ]['bseat']  = $tseat;
			}
		}

		$total = round( $sumtotal / $sumseat );

		foreach ( $groups as $iyear => $year )
		{
			foreach ( $year as $imonth => $lst )
			{
                if( isset( $atotal[$iyear] ) )
				{
                    $atotal[$iyear] += $lst['btotal'];
                    $atseat[$iyear] += $lst['bseat'];
                }
                else
                {
                    $atotal[$iyear] = $lst['btotal'];
                    $atseat[$iyear] = $lst['bseat'];
                }

				$items[$iyear][$imonth]['bdate']	= $lst['bdate'];
				$items[$iyear][$imonth]['avgprice'] = round( $lst['btotal'] / $lst['bseat'] );
			}
		}

		return array( 'total' => $total, 'items' => $items, 'total_price_per_year' => $atotal, 'total_seat_per_year' => $atseat );
	}
}

function get_ticket_market_segment_report( $filter )
{
    global $db;

	$w  = array();
	$w2 = array();

    if( ticket_is_report_filter_view() )
	{
		extract( $filter );

        if( $ryear != '' )
        {
    		$w[]  = $db->prepare_query( ' YEAR( a.bdate ) = %s', $ryear );
    		$w2[] = $db->prepare_query( ' YEAR( a2.bdate ) = %s', $ryear );
        }
	}

    if( empty( $w ) )
    {
    	$s = 'SELECT
				a.bid,
				a.bdate,
				a.bticket,
				a.btotal,
				c.chid,
				c.chcode,
				c.chname,(
					SELECT SUM(a2.btotal) AS total
					FROM ticket_booking AS a2
					WHERE MONTH( a2.bdate ) = MONTH( a.bdate )
					ORDER BY MONTH( a2.bdate ) ASC
				) AS total_by_month,(
					SELECT SUM(a2.btotal) AS total
					FROM ticket_booking AS a2
					WHERE a2.chid = a.chid AND MONTH( a2.bdate ) = MONTH( a.bdate )
					ORDER BY a2.chid ASC
				) AS total_by_channel
			  FROM ticket_booking AS a
			  LEFT JOIN ticket_channel AS c ON a.chid = c.chid
			  ORDER BY a.bdate, a.bid ASC';
		$q = $db->prepare_query( $s );
		$r = $db->do_query( $q );

		$s2 = 'SELECT SUM(a.btotal) as total FROM ticket_booking as a';
		$q2 = $db->prepare_query( $s2 );
		$r2 = $db->do_query( $q2 );
		$d2 = $db->fetch_array( $r2 );
	}
	else
	{
		$s = 'SELECT
				a.bid,
				a.bdate,
				a.bticket,
				a.btotal,
				c.chid,
				c.chcode,
				c.chname,(
					SELECT SUM(a2.btotal) AS total
					FROM ticket_booking AS a2
					WHERE a2.bstt <> "ar" AND MONTH( a2.bdate ) = MONTH( a.bdate ) AND' . implode( ' AND', $w2 ) . '
					ORDER BY MONTH( a2.bdate ) ASC
				) AS total_by_month,(
					SELECT SUM(a2.btotal) AS total
					FROM ticket_booking AS a2
					WHERE a2.bstt <> "ar" AND a2.chid = a.chid AND MONTH( a2.bdate ) = MONTH( a.bdate ) AND' . implode( ' AND', $w2 ) . '
					ORDER BY a2.chid ASC
				) AS total_by_channel
			  FROM ticket_booking AS a
			  LEFT JOIN ticket_channel AS c ON a.chid = c.chid
			  WHERE a.bstt <> "ar" AND ' . implode( ' AND', $w ) . ' ORDER BY a.bdate, a.bid ASC';
		$q = $db->prepare_query( $s );
		$r = $db->do_query( $q );

		$s2 = 'SELECT SUM(a.btotal) as total FROM ticket_booking as a WHERE ' . implode( ' AND', $w );
		$q2 = $db->prepare_query( $s2 );
		$r2 = $db->do_query( $q2 );
		$d2 = $db->fetch_array( $r2 );
	}

	$items   = array();
	$summary = isset( $d2['total'] ) ? $d2['total'] : 0;

	while( $d = $db->fetch_array( $r ) )
	{
		$index   = base64_encode( json_encode( array( date( 'F', strtotime( $d['bdate'] ) ), $d['total_by_month'], round( ( $d['total_by_month'] * 100 ) / $summary, 2 ) ) ) );
		$group   = base64_encode( json_encode( array( $d['chcode'], $d['chname'], $d['total_by_channel'], round( ( $d['total_by_channel'] * 100 ) / $summary, 2 ) ) ) );

		$items[ $index ][ $group ][] = array(
			'bid' => $d['bid'],
			'bdate' => $d['bdate'],
			'bticket' => $d['bticket'],
			'btotal' => $d['btotal'],
			'chid' => $d['chid'],
			'chcode' => $d['chcode'],
			'chname' => $d['chname']
		);
	}

	$result = array( 'total' => $summary, 'items' => $items );

	return $result;
}

function get_ticket_hs_sales_sum_report( $filter )
{
    global $db;

	$w = array();
	$b = '';

	extract( $filter );

    if( $lcid != '' )
    {
        $w[]  = $db->prepare_query( '( b.bdfrom = %s OR b.bdto = %s )', $lcid, $lcid );
    }

    if( !empty( $rsource ) )
    {
        $bs = '( ';

        if ( !empty( $rsource ) )
        {
            $bs .= !empty( $chid ) ? ' OR ' : '';

            $end_rsource = end( $rsource );

            foreach( $rsource as $parts )
            {
                if( strpos( $parts, '|' ) )
                {
                    list( $chidd, $agid ) = explode( '|', $parts );

                    if( $parts == $end_rsource )
                    {
                        $bs .= $db->prepare_query( 'a.agid = %d', $agid );
                    }
                    else
                    {
                        $bs .= $db->prepare_query( 'a.agid = %d OR ', $agid );
                    }
                }
                else
                {
                    if( $parts == $end_rsource )
                    {
                        $bs .= $db->prepare_query( 'a.chid = %d', $parts );
                    }
                    else
                    {
                        $bs .= $db->prepare_query( 'a.chid = %d OR ', $parts );
                    }
                }
            }
        }

        $bs .= ' )';

        $w[] = $bs;
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = '( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b  .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                }
                else
                {
                    $b  .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' b.bdpstatus = %s', $bstatus );
        }
    }

	if( $rtype != '' && $bdate != '' )
    {
    	if( $rtype == '1' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 ' . $bdate ) );

    		$w[] = $db->prepare_query( ' YEAR( a.bdate ) = YEAR( %s ) AND MONTH( a.bdate ) = MONTH( %s )', $date, $date );
            $w[] = $db->prepare_query( ' YEAR( b.bddate ) = YEAR( %s )', $date );
    	}
    	elseif( $rtype == '2' )
    	{
    		$date = date( 'Y-m-d', strtotime( '1 Jan ' . $bdate ) );

    		$w[] = $db->prepare_query( ' YEAR( a.bdate ) = YEAR( %s )', $date );
            $w[] = $db->prepare_query( ' YEAR( b.bddate ) = YEAR( %s )', $date );
    	}
    }

	if( empty( $w ) && empty( $b ) )
	{
		$where = 'b.bdpstatus NOT IN ( "cn", "pf", "bc", "rf" )';
	}
	elseif( empty( $w ) === false && empty( $b ) )
	{
		$where = implode( ' AND', $w );
	}
	elseif( empty( $b ) === false && empty( $w ) )
	{
		$where = $b;
	}
	else
	{
		$where = $b . ' AND ' . implode( ' AND', $w );
	}

	$s = 'SELECT
			a.bid,
			a.bdate,
			b.bddate,
			b.bdfrom,
			b.bdto,
			b.bdtype,
			c.rname,
			c.rdepart,
			d.lctype,
			MONTH( b.bddate ) AS bdmonth,
			( b.num_adult + b.num_child + b.num_infant ) AS tseat
		FROM ticket_booking AS a
		LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
		LEFT JOIN ticket_route AS c ON b.rid = c.rid
		LEFT JOIN ticket_location AS d ON c.rdepart = d.lcid
		WHERE a.bstt <> "ar" AND MONTH( b.bddate ) IN ( 7, 8, 9 ) AND ' . $where . ' ORDER BY a.bdate ASC';
	$q = $db->prepare_query( $s );
	$r = $db->do_query( $q );

	$items  = array();
	$rcount = 0;

	while( $d = $db->fetch_array( $r ) )
	{
		$index = date( 'm-Y', strtotime( $d['bdate'] ) );
		$group = date( 'm-Y', strtotime( $d['bddate'] ) );

		$items[ $index ][ $group ][]  = array(
			'bid'    => $d['bid'],
            'bdto'   => $d['bdto'],
			'bdate'  => $d['bdate'],
            'rname'  => $d['rname'],
            'tseat'  => $d['tseat'],
			'bddate' => $d['bddate'],
			'bdfrom' => $d['bdfrom'],
			'bdtype' => $d['bdtype'],
			'lctype' => $d['lctype']
		);

		ksort( $items[ $index ] );

		$rcount = $rcount + $d['tseat'];
	}

    if( empty( $items ) )
    {
        $result = array( 'total' => $rcount, 'items' => $items );
    }
    else
    {
        $total_bbm = array();
        $total_bdm = array();
        $total_btt = array();
        $total_bpt = array();
        $total_btn = array();
        $item_list = array();
        $category  = array();
        $series    = array();

        foreach( $items as $key => $obj )
        {
            $period = date( 'F Y', strtotime( '01-' . $key ) );

            $category[] = $period;

            foreach( $obj as $idx => $value )
            {
                $legend = date( 'F', strtotime( '01-' . $idx ) );
                $temps  = array();

                foreach( $value as $i => $d )
                {
                    $type = $d[ 'lctype' ] == '0' ? 'OUT' : 'RETURN';

                    $temps[ $type ][] = $d;
                }

                ksort( $temps );

                foreach( $temps as $type => $temp )
                {
                    $data  = array();

                    foreach( $temp as $trip )
                    {
                        $port = $trip[ 'bdfrom' ] . ' - ' . $trip[ 'bdto' ];

                        if( isset( $total_bbm[ $period ] ) )
                        {
                            $total_bbm[ $period ] = $total_bbm[ $period ] + $trip['tseat'];
                        }
                        else
                        {
                            $total_bbm[ $period ] = $trip['tseat'];
                        }

                        if( isset( $total_bdm[ $period ][ $legend ] ) )
                        {
                            $total_bdm[ $period ][ $legend ] = $total_bdm[ $period ][ $legend ] + $trip['tseat'];
                        }
                        else
                        {
                            $total_bdm[ $period ][ $legend ] = $trip['tseat'];
                        }

                        if( isset( $total_btt[ $period ][ $legend ][ $type ] ) )
                        {
                            $total_btt[ $period ][ $legend ][ $type ] = $total_btt[ $period ][ $legend ][ $type ] + $trip['tseat'];
                        }
                        else
                        {
                            $total_btt[ $period ][ $legend ][ $type ] = $trip['tseat'];
                        }

                        if( isset( $total_bpt[ $period ][ $legend ][ $type ][ $port ] ) )
                        {
                            $total_bpt[ $period ][ $legend ][ $type ][ $port ] = $total_bpt[ $period ][ $legend ][ $type ][ $port ] + $trip['tseat'];
                        }
                        else
                        {
                            $total_bpt[ $period ][ $legend ][ $type ][ $port ] = $trip['tseat'];
                        }

                        if( isset( $total_btn[ $period ][ $legend ][ $type ][ $port ][ $trip[ 'rname' ] ] ) )
                        {
                            $total_btn[ $period ][ $legend ][ $type ][ $port ][ $trip[ 'rname' ] ] = $total_btn[ $period ][ $legend ][ $type ][ $port ][ $trip[ 'rname' ] ] + $trip['tseat'];
                        }
                        else
                        {
                            $total_btn[ $period ][ $legend ][ $type ][ $port ][ $trip[ 'rname' ] ] = $trip['tseat'];
                        }

                        $data[ $port ][ $trip[ 'rname' ] ][] = $trip;
                    }

                    $item_list[ $period ][ 'list' ][ $legend ][ 'list' ][ $type ] = $data;
                }

                $series[ $legend ][ $period ][] = $total_bdm[ $period ][ $legend ];
            }
        }

        $result = array(
            'total'     => $rcount,
            'items'     => $item_list,
            'total_bbm' => $total_bbm,
            'total_bdm' => $total_bdm,
            'total_btt' => $total_btt,
            'total_bpt' => $total_bpt,
            'total_btn' => $total_btn,
            'chart'     => get_chart_options( $category, $series ),
            'filter'    => base64_encode( json_encode( $filter ) )
        );
    }

    return $result;
}

function get_total_hs_sales_by_booking_month( $data, $count )
{
	$num = 0;

	foreach( $data as $obj )
	{
		foreach( $obj as $key => $value )
		{
			foreach( $value as $id => $val )
			{
				foreach( $val as $i => $d )
				{
					foreach ($d as $ie => $val_en)
					{
						$num = $num + $val_en['tseat'];
					}
				}
			}
		}
	}

	return array( $num, round( ( $num * 100 ) / $count, 2 ) );
}

function get_total_hs_sales_by_depart_month( $data, $count )
{
	$num   = 0;

	foreach( $data as $obj )
	{
		foreach( $obj as $key => $value )
		{
			foreach( $value as $id => $val )
			{
				foreach ($val as $ie => $val_en)
				{
					$num = $num + $val_en['tseat'];
				}
			}
		}
	}

	return array( $num, round( ( $num * 100 ) / $count, 2 ) );
}

function get_total_hs_sales_by_port_type( $data, $count )
{
	$num   = 0;

	foreach( $data as $obj )
	{
		foreach( $obj as $key => $value )
		{
			foreach ($value as $ie => $val_en)
			{
				$num = $num + $val_en['tseat'];
			}
		}
	}

	return array( $num, round( ( $num * 100 ) / $count, 2 ) );
}

function get_total_hs_sales_by_trip( $data, $count )
{
	$num   = 0;

	foreach( $data as $obj )
	{
		foreach( $obj as $key => $value )
		{
			$num = $num + $value['tseat'];
		}
	}

	return array( $num, round( ( $num * 100 ) / $count, 2 ) );
}

function get_agent_outstanding_list_report( $filter )
{
	global $db;

    $w = '';

    if( ticket_is_report_filter_view() )
    {
    	extract( $filter );

        if( $atstatus != '' )
        {
            $w .= $db->prepare_query( ' AND atstatus = %d', $atstatus );
        }

        if( $bticket != '' )
        {
            $w .= $db->prepare_query( ' AND bticket = %s', $bticket );
        }

        if( $date_start != '' && $date_end != '' )
        {
            $w .= $db->prepare_query( ' AND atdate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $date_start ) ), date( 'Y-m-d', strtotime( $date_end ) ) );
        }
    }

    if( empty( $w ) )
    {
        $s = 'SELECT a.*, b.bticket
              FROM ticket_agent_transaction AS a
              LEFT JOIN ticket_booking AS b ON a.bcode = b.bcode
              WHERE b.bstt <> "ar" AND a.agid = %d AND b.bstatus NOT IN ( "cn", "bc" )
              ORDER BY a.atdate ASC';
        $q = $db->prepare_query( $s, $_COOKIE['agid'] );
        $r = $db->do_query( $q );
    }
    else
    {
        $s = 'SELECT a.*, b.bticket
              FROM ticket_agent_transaction AS a
              LEFT JOIN ticket_booking AS b ON a.bcode = b.bcode
              WHERE b.bstt <> "ar" AND a.agid = %d AND b.bstatus NOT IN ( "cn", "bc" ) ' . $w . '
              ORDER BY a.atdate ASC';
        $q = $db->prepare_query( $s, $_COOKIE['agid'] );
        $r = $db->do_query( $q );
    }

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        $atoutstanding = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            if( $d['atstatus'] == 1 )
            {
                $d['atstatus'] = 'Confirmed';
            }
            elseif( $d['atstatus'] == 2 )
            {
                $d['atstatus'] = 'Paid Balance';
            }
            elseif( $d['atstatus'] == 3 )
            {
                $d['atstatus'] = 'Canceled, cancelation fee applied';
            }

            $atoutstanding = $atoutstanding + ( $d['atdebet'] - $d['atcredit'] );

            $d['atoutstanding'] = $atoutstanding;

            $data[] = $d;
        }

        krsort( $data );
    }

    return $data;
}

function get_ticket_promo_used_report( $filter, $use_unicode = true )
{
    global $db;

	extract( $filter );

	$w = array();
	$b = '';
	$p = '';

    if( $chid != '' )
    {
    	$arr = explode( '|', $chid );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

    	if( isset( $arr[1] ) )
    	{
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
    	}
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = '( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    $b .= $db->prepare_query( 'a.bstatus = %s', $st );
                }
                else
                {
                    $b .= $db->prepare_query( 'a.bstatus = %s OR ', $st );
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' a.bstatus = %s', $bstatus );
        }
    }

    if( $promo_code != '' )
    {
        if( is_array( $promo_code ) )
        {
            $epromo_code = end( $promo_code );

            $p = '( ';

            foreach( $promo_code as $st )
            {
                if( $st == $epromo_code )
                {
                    $p .= $db->prepare_query( 'a.pmcode = %s', $st );
                }
                else
                {
                    $p .= $db->prepare_query( 'a.pmcode = %s OR ', $st );
                }
            }

            $p .= ' )';
        }
        else
        {
            $p = $db->prepare_query( ' a.pmcode = %s', $promo_code );
        }
    }

	if( empty( $w ) && empty( $b ) && empty( $p ) )
	{
		$where  = 'WHERE a.bstt <> "ar" AND a.pmcode <> "" AND a.bstatus NOT IN ("cn","bc","pf","rf")';
	}
	elseif( empty( $w ) && empty( $b ) && !empty( $p ) )
	{
		$where  = 'WHERE a.bstt <> "ar" AND ' . $p . ' AND a.bstatus NOT IN ("cn","bc","pf","rf")';
	}
	elseif( empty( $w ) && !empty( $b ) && empty( $p ) )
	{
		$where  = 'WHERE a.bstt <> "ar" AND a.pmcode <> "" AND ' . $b;
	}
	elseif( empty( $w ) && !empty( $b ) && !empty( $p ) )
	{
		$where  = 'WHERE a.bstt <> "ar" AND ' . $p . ' AND ' . $b;
	}
	elseif( !empty( $w ) && empty( $b ) && empty( $p ) )
	{
		$where  = 'WHERE a.bstt <> "ar" AND a.pmcode <> "" AND a.bstatus NOT IN ("cn","bc","pf","rf") AND ' . implode( ' AND', $w );
	}
	elseif( !empty( $w ) && empty( $b ) && !empty( $p ) )
	{
		$where  = 'WHERE a.bstt <> "ar" AND ' . $p . ' AND a.bstatus NOT IN ("cn","bc","pf","rf") AND ' . implode( ' AND', $w );
	}
	else
	{
		$where  = 'WHERE a.bstt <> "ar" AND ' . $p . ' AND ' . $b . ' AND ' . implode( ' AND', $w );
	}

	$q = 'SELECT
			a.bid,
			a.agid,
			a.bdate,
			a.pmcode,
			a.bticket,
			a.bdiscount,
			b.chid,
			b.chcode,
			b.chname,
			c.pmlimit,
			c.pmuselimit,
			COUNT( a.pmcode ) AS pused
		  FROM ticket_booking AS a
		  LEFT JOIN ticket_channel AS b ON a.chid = b.chid
		  LEFT JOIN ticket_promo AS c ON a.pmcode = c.pmcode
		  ' . $where . ' GROUP BY a.pmcode ORDER BY a.bdate ASC';

	$r = $db->do_query( $q );

	if( is_array( $r ) )
	{
		return array();
	}
	else
	{
		$data = array();

		while( $d = $db->fetch_array( $r ) )
		{
			if( $d['pmuselimit'] != '1' )
			{
				if( $use_unicode )
				{
					$d['pmlimit'] = '&#x221e;';
					$d['premain'] = '&#x221e;';
				}
				else
				{
					$d['pmlimit'] = 'INFINITY';
					$d['premain'] = 'INFINITY';
				}
			}
			else
			{
				$d['premain'] = $d['pused'] > $d['pmlimit'] ? 0 : ( $d['pmlimit'] - $d['pused'] );
			}

			$data[] = $d;
		}

		return $data;
	}
}

function get_ticket_promo_code_report( $filter )
{
    global $db;

	extract( $filter );

	$w = array();

    $odate = ( $rbased == 1 ) ? 'c.bddate' : 'a.bdate';

    if( $rtype != '' && $bdate != '' )
    {
        if( $rtype == '0'  )
        {
            $date = date( 'Y-m-d', strtotime( $bdate ) );

            $w[]  = $db->prepare_query( $odate .' = %s', $date );
        }
        elseif( $rtype == '1' )
        {
            $date = date( 'Y-m-d', strtotime( '1 ' . $bdate ) );

            $w[]  = $db->prepare_query( ' YEAR( ' . $odate . ' ) = YEAR( %s ) AND MONTH( '. $odate .' ) = MONTH( %s )', $date, $date );
        }
        elseif( $rtype == '2' )
        {
            $date = date( 'Y-m-d', strtotime( '1 Jan ' . $bdate ) );

            $w[]  = $db->prepare_query( ' YEAR( '. $odate .' ) = YEAR( %s )', $date );
        }
        elseif( $rtype == '3' )
        {
            $date_from = date( 'Y-m-d', strtotime( $bdate ) );
            $date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

            $w[] = $db->prepare_query( $odate .' BETWEEN %s AND %s', $date_from, $date_to );
        }
    }

    if( $chid != '' )
    {
    	$arr = explode( '|', $chid );

        $w[] = $db->prepare_query( ' a.chid = %d', $arr[0] );

    	if( isset( $arr[1] ) )
    	{
            $w[] = $db->prepare_query( ' a.agid = %d', $arr[1] );
    	}
    }

    if( $bstatus != '' )
    {
        if( is_array( $bstatus ) )
        {
            $estatus = end( $bstatus );

            $b = '( ';

            foreach( $bstatus as $st )
            {
                if( $st == $estatus )
                {
                    if( $st == 'pp' )
                    {
                        $b .= $db->prepare_query( 'c.bdpstatus IN ("pp","ol")' );
                    }
                    else
                    {
                        $b .= $db->prepare_query( 'c.bdpstatus = %s', $st );
                    }
                }
                else
                {
                    if( $st == 'pp' )
                    {
                        $b .= $db->prepare_query( 'c.bdpstatus IN ("pp","ol") OR' );
                    }
                    else
                    {
                        $b .= $db->prepare_query( 'c.bdpstatus  = %s OR ', $st );
                    }
                }
            }

            $b .= ' )';
        }
        else
        {
            $b = $db->prepare_query( ' c.bdpstatus  = %s', $bstatus );
        }

        $w[] = $b;
    }
    else
    {
        $w[] = 'c.bdpstatus  NOT IN ("cn","bc","pf","rf")';
    }

    if( $promo_code != '' )
    {
        if( is_array( $promo_code ) )
        {
            $epromo_code = end( $promo_code );

            $p = '( ';

            foreach( $promo_code as $st )
            {
                if( $st == $epromo_code )
                {
                    $p .= $db->prepare_query( 'a.pmcode = %s', $st );
                }
                else
                {
                    $p .= $db->prepare_query( 'a.pmcode = %s OR ', $st );
                }
            }

            $p .= ' )';
        }
        else
        {
            $p = $db->prepare_query( ' a.pmcode = %s', $promo_code );
        }

        $w[] = $p;
    }
    else
    {
        $w[] = ' a.pmcode <> ""';
    }

    if ( !empty( $w ) )
    {
        $where = 'WHERE a.bstt <> "ar" AND ' . implode( ' AND' , $w );
    }
    else
    {
        $where = 'WHERE a.bstt <> "ar" AND a.pmcode <> "" AND c.bdpstatus NOT IN ("cn","bc","pf","rf")';
    }

	$q = 'SELECT
            a.bdate,
            a.bid,
			a.agid,
            a.pmcode,
			a.bticket,
			a.bdiscount,
            a.bcommission,
            a.bdiscountnum,
            a.bdiscounttype,
            a.bcommissiontype,
            a.bcommissioncondition,
            a.badditionaldisc,
            a.bmethod_by,
			b.chid,
			b.chcode,
			b.chname,
            c.bdid,
            c.bdstatus,
            c.bdpstatus,
            c.bddate,
            c.bdfrom,
            c.bdto,
            c.num_adult,
            c.num_child,
            c.num_infant,
            c.price_per_adult,
            c.price_per_child,
            c.price_per_infant,
            c.disc_price_per_adult,
            c.disc_price_per_child,
            c.disc_price_per_infant
		  FROM ticket_booking AS a
		  LEFT JOIN ticket_channel AS b ON a.chid = b.chid
          LEFT JOIN ticket_booking_detail AS c ON c.bid = a.bid
		  ' . $where . ' GROUP BY c.bdid ORDER BY a.bdate, c.bddate ASC';
	$r = $db->do_query( $q );

	if( is_array( $r ) )
	{
		return array( 'total' => 0, 'items' => array() );
	}
	else
	{
        $items  = array();
        $data   = array();
        $bdata  = array();
        $darray = array();
        $rcount = array();

        while( $d = $db->fetch_array( $r ) )
        {
            if( in_array( $d['bdpstatus'], array( 'pp', 'ol' ) ) && $d['bmethod_by'] == 0 )
            {
                continue;
            }

            $npmcode = get_pmcode( strtoupper( trim( $d['pmcode'] ) ) );

            $data[ $npmcode ][ $d['bdid'] ][] = $d;
            $bdata[ $npmcode ][ $d['bid'] ][] = $d;
        }

        foreach( $data as $pmcode => $obj )
        {
            foreach( $obj as $bdid => $dt )
            {
                $dtotal = 0;
                $ctotal = 0;

                foreach( $dt as $d )
                {
                    //-- Check Base Total For Commision
                    //-- 0 = after discount
                    //-- 1 = before discount
                    if( $d['bcommissioncondition'] == '0' )
                    {
                        if( $d['badditionaldisc'] == '2' )
                        {
                            $total = ( ( $d['price_per_adult'] * $d['num_adult'] ) + ( $d['price_per_child'] * $d['num_child'] ) + ( $d['price_per_infant'] * $d['num_infant'] ) );
                        }
                        else
                        {
                            $total = ( ( ( $d['price_per_adult'] - $d['disc_price_per_adult'] ) * $d['num_adult'] ) + ( ( $d['price_per_child'] - $d['disc_price_per_child'] ) * $d['num_child'] ) + ( ( $d['price_per_infant'] - $d['disc_price_per_infant'] ) * $d['num_infant'] ) );
                        }

                        $dtotal += $total;
                    }
                    else
                    {
                        $total   = ( ( ( $d['price_per_adult'] - $d['disc_price_per_adult'] ) * $d['num_adult'] ) + ( ( $d['price_per_child'] - $d['disc_price_per_child'] ) * $d['num_child'] ) + ( ( $d['price_per_infant'] - $d['disc_price_per_infant'] ) * $d['num_infant'] ) );
                        $dtotal += $total;
                    }

                    //-- Calculate Cancel Trip
                    if( $d['bdstatus'] == 'cn' )
                    {
                        $ctotal += $total;
                    }

                    if( end( $dt ) == $d )
                    {
                        //-- Count Total By Code
                        $tbycode  = count( $bdata[ $pmcode ] );

                        //-- Count Total Used
                        $rcount[ $pmcode ] = $tbycode;

                        //-- Check discount per trip
                        //-- 0 = %
                        //-- 1 = Rp.
                        if( $d['bdiscounttype'] == '0' )
                        {
                            $d['bdiscountnum'] = ( ( ( ( $d['price_per_adult'] - $d['disc_price_per_adult'] ) * $d['num_adult'] ) + ( ( $d['price_per_child'] - $d['disc_price_per_child'] ) * $d['num_child'] ) + ( ( $d['price_per_infant'] - $d['disc_price_per_infant'] ) * $d['num_infant'] ) ) * $d['bdiscountnum'] ) / 100;
                        }

                        //-- Check again commission condition
                        //-- If use discount => total - bdiscount
                        if( $d['bcommissioncondition'] == '0' )
                        {
                            $dtotal = $dtotal - $d['bdiscountnum'];
                        }

                        //-- Check Commission Type
                        //-- 0 = %
                        //-- 1 = Rp.
                        if( $d['bcommissiontype'] == '0' )
                        {
                            $bcommission = $dtotal * ( $d['bcommission'] / 100 );
                        }
                        else
                        {
                            $bcommission = ( $dtotal - $ctotal ) * ( $d['bcommission'] / $dtotal );
                        }

                        //-- Booking Source
                        $agname  = get_agent( $d['agid'], 'agname' );
                        $bsource = empty( $agname ) ? '( ' . $d['chcode'] . ' ) ' . $d['chname'] : $agname;

                        $items[ $pmcode ][ 'totalbycode' ] = $tbycode;
                        $items[ $pmcode ][ 'items' ][]     = array(
                            'pmcode'      => $pmcode,
                            'bsource'     => $bsource,
                            'bdate'       => $d['bdate'],
                            'bcommission' => $bcommission,
                            'bticket'     => $d['bticket'],
                            'bdiscount'   => $d['bdiscountnum'],
                            'route'       => $d['bdfrom'] . ' - ' . $d['bdto'],
                            'bddate'      => date( 'd F Y', strtotime( $d['bddate'] ) )
                        );
                    }
                }
            }
        }

        $rcount = empty( $rcount ) ? 0 : array_sum( $rcount );

        return array( 'total' => $rcount, 'items' => $items );
	}
}

function get_pmcode( $pmcode )
{
    global $db;

    $p = $db->prepare_query( 'SELECT pmcode FROM ticket_promo WHERE pmcode LIKE %s', $pmcode );
    $r = $db->do_query( $p );

    while ( $f = $db->fetch_array( $r ) )
    {
        return $f['pmcode'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Report - set option
| -------------------------------------------------------------------------------------
*/
function get_report_type_option( $value = '', $type = 'transport' )
{
	if( $type == 'transport' )
    {
    	return '
	    <option value="" ' . ( $value == '' ? 'selected' : '' ) . '>Report Period</option>
	    <option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>Daily</option>
	    <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Monthly</option>
	    <option value="2" ' . ( $value == '2' ? 'selected' : '' ) . '>Yearly</option>
	    <option value="3" ' . ( $value == '3' ? 'selected' : '' ) . '>Date Range</option>';
	}
	elseif( $type == 'reservation' )
    {
    	return '
	    <option value="" ' . ( $value == '' ? 'selected' : '' ) . '>Report Period</option>
	    <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Monthly Reservation</option>
	    <option value="2" ' . ( $value == '2' ? 'selected' : '' ) . '>Yearly Reservation</option>
	    <option value="3" ' . ( $value == '3' ? 'selected' : '' ) . '>Date Range Reservation</option>';
    }
	elseif( $type == 'daily-pax-numbers' )
    {
    	return '
	    <option value="" ' . ( $value == '' ? 'selected' : '' ) . '>Report Period</option>
	    <option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>Daily</option>
	    <option value="3" ' . ( $value == '3' ? 'selected' : '' ) . '>Date Range Reservation</option>';
    }
	elseif( $type == 'revenue' )
    {
    	return '
	    <option value="" ' . ( $value == '' ? 'selected' : '' ) . '>Select Period</option>
	    <option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>Daily Revenue</option>
	    <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Monthly Revenue</option>
	    <option value="2" ' . ( $value == '2' ? 'selected' : '' ) . '>Yearly Revenue</option>
	    <option value="3" ' . ( $value == '3' ? 'selected' : '' ) . '>Date Range Revenue</option>
	    <option value="4" ' . ( $value == '4' ? 'selected' : '' ) . '>Yearly Range Revenue</option>';
    }
    elseif( $type == 'hs-sales-sum-report' )
    {
        return '
        <option value="" ' . ( $value == '' ? 'selected' : '' ) . '>Select Period</option>
        <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Monthly Revenue</option>
        <option value="2" ' . ( $value == '2' ? 'selected' : '' ) . '>Yearly Revenue</option>';
    }
    elseif( $type == 'daily-addons' )
    {
        return '
        <option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>Daily Report</option>';
    }
    elseif( $type == 'summary-addons' )
    {
        return '
        <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Monthly Report</option>
        <option value="2" ' . ( $value == '2' ? 'selected' : '' ) . '>Yearly Report</option>
        <option value="3" ' . ( $value == '3' ? 'selected' : '' ) . '>Date Range Report</option>';
    }
}

function get_report_view_option( $value, $type = 'reservation' )
{
	if( $type == 'reservation' )
    {
		return '
		<option value="" ' . ( $value == '0' ? 'selected' : '' ) . '>Select Options</option>
		<option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>View by No. of Pax</option>
		<option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>View by Occupancy</option>
		<option value="2" ' . ( $value == '2' ? 'selected' : '' ) . '>View by Agent Allotment</option>
		<option value="3" ' . ( $value == '3' ? 'selected' : '' ) . '>View by Free Seats</option>
		<option value="4" ' . ( $value == '4' ? 'selected' : '' ) . '>View by No. of Cancellation</option>';
	}
	elseif( $type == 'seats-production' )
	{
		return '
		<option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>Sales Channel</option>
		<option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Booking Source</option>
		<option value="2" ' . ( $value == '2' ? 'selected' : '' ) . '>Nationality</option>';
	}
	elseif( $type == 'revenue' )
	{
		return '
		<option value="" ' . ( $value == '0' ? 'selected' : '' ) . '>Select Options</option>
		<option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>Sales Channel</option>
		<option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Booking Source</option>
		<option value="2" ' . ( $value == '2' ? 'selected' : '' ) . '>Sales Person</option>
		<option value="3" ' . ( $value == '3' ? 'selected' : '' ) . '>Route Point</option>
        <option value="4" ' . ( $value == '4' ? 'selected' : '' ) . '>Payment Method</option>';
	}
	elseif( $type == 'market-segment' )
	{
		return '
		<option value="" ' . ( $value == '0' ? 'selected' : '' ) . '>Select Options</option>
		<option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>By Revenue</option>
		<option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>By Pax</option>';
	}
}

function get_based_on_option( $value, $type = 'revenue' )
{
    if( $type == 'revenue' )
    {
        return '
        <option value="" ' . ( $value == '0' ? 'selected' : '' ) . '>Select Based On Period</option>
        <option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>Booking Date</option>
        <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Travel Date</option>';
    }
}

function get_booking_fee_option( $value )
{
    return '
    <option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>With Transport Fee</option>
    <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Without Transport Fee</option>';
}

function get_booking_addons_option( $value )
{
    return '
    <option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>With Add-ons</option>
    <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Without Add-ons</option>';
}

function get_next_trip_schedule( $bid, $btype )
{
	if( $btype == '1' )
	{
		$data = ticket_booking_detail( $bid, 'return' );

		if( !empty( $data ) )
		{
			return $data['bddate'];
		}
	}
	else
	{
		return '-';
	}
}

function get_agent_net_price( $agent, $data )
{
	if( $agent['agpayment_type'] == 'Pre Paid' )
	{
		if( $agent['agtype'] == 'Net Rate' )
		{
			return $data['net_price'];
		}
		else
		{
			$price_after_disc = $data['prices'] - $data['disc_price'];

			if( $data['commission_type'] == '0' )
			{
				$commission = ( $price_after_disc * $data['commission'] ) / 100;
			}
			else
			{
				$commission = $data['commission'];
			}

			$net_rate = $price_after_disc - $commission;

			return $net_rate;
		}
	}
	else
	{
		$net_rate = $data['prices'] - $data['disc_price'];

		return $net_rate;
	}
}

function ticket_transport_list_area( $bdid, $field = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_booking_transport AS a
		  LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
		  WHERE a.bdid = %d';
	$q = $db->prepare_query( $s, $bdid );
	$r = $db->do_query( $q );

	if( $db->num_rows( $r ) > 0 )
	{
		$area = array();

		while( $d = $db->fetch_array( $r ) )
		{
			if( empty( $field ) )
			{
				$area[ $d['bttype'] ] = $d;
			}
			else
			{
				$area[ $d['bttype'] ] = $d[ $field ];
			}
		}

		return $area;
	}
	else
	{
		return '-';
	}
}

function get_report_year_option( $year = array() )
{
	$start = 2016;
	$end   = date( 'Y' ) + 50;
	$years = range( $start, $end );

	$option = '';

	foreach( $years as $y )
	{
		if( is_array( $year ) )
		{
			$option .= '<option value="' . $y . '" ' . ( in_array( $y, $year ) ? 'selected' : '' ) . '>' . $y . '</option>';
		}
		else
		{
			$option .= '<option value="' . $y . '" ' . ( $y == $year ? 'selected' : '' ) . '>' . $y . '</option>';
		}
	}

	return $option;
}

function get_sales_person_option( $agsp = '', $use_empty = true, $empty_text = 'Select Sales Person' )
{
	global $db;

	$s = 'SELECT DISTINCT(a.agsp) FROM ticket_agent AS a';
	$q = $db->prepare_query( $s );
	$r = $db->do_query( $q );

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

	if( !is_array( $r ) )
	{
		while( $d = $db->fetch_array( $r ) )
		{
			if( !empty( $d['agsp'] ) )
			{
				$option .= '<option value="' . $d['agsp'] . '" ' . ( $agsp == $d['agsp'] ? 'selected' : '' ) . '>' . $d['agsp'] . '</option>';
			}
		}
	}

	return $option;
}

function get_sales_channel_option( $cid = '', $use_empty = true, $empty_text = 'Select Sales Channel' )
{
    global $db;

    $s = 'SELECT c.chid, c.chcode, c.chname FROM ticket_channel AS c WHERE c.chstatus=%s ORDER BY c.chname ASC';
    $q = $db->prepare_query( $s, 'publish');
    $r = $db->do_query( $q );

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !is_array( $r ) )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            if( !empty( $d['chid'] ) )
            {
                $option .= '<option value="' . $d['chid'] . '" ' . ( $cid == $d['chid'] ? 'selected' : '' ) . '>' . '(' . $d['chcode'] . ') ' . $d['chname'] . '</option>';
            }
        }
    }

    return $option;
}

function get_reservation_status( $sts = '', $use_empty = true, $empty_text = 'Reservation Status' )
{
    $status = array(
        'Devinitive',
        'Tentative',
        'Blocking',
        'No-Show',
        'Other Boat'
    );

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $status ) )
    {
        foreach( $status as $st => $stat )
        {
            if( is_array( $sts ) )
            {
                $option .= '<option value="' . $stat . '" ' . ( in_array( $stat, $sts ) ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
            else
            {
                $option .= '<option value="' . $stat . '" ' . ( $stat == $sts ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| booking method by select option bayu edit
| -------------------------------------------------------------------------------------
*/
function get_booking_methodby_option( $sts = '', $use_empty = true, $empty_text = 'Select Booking Method' )
{
	$status = array(
		'Direct Online' => 0,
		'Direct Agent' => 1,
		'Admin' => 2
	);

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $status ) && $status != NULL)
    {
        foreach( $status as $st => $stat )
        {
            if( is_array( $sts ) )
            {
                $option .= '<option value="' . $stat . '" ' . ( in_array( $stat, $sts ) ? 'selected' : '' ) . ' >' . $st . '</option>';
            }
            else
            {
                $option .= '<option value="' . $stat . '" ' . ( $stat == $sts ? 'selected' : '' ) . ' >' . $st . '</option>';
            }
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| booking method by select option bayu edit
| -------------------------------------------------------------------------------------
*/
function get_payment_method_option_2( $sts = '', $use_empty = true, $empty_text = 'Select Payment Method' )
{
    $status = array(
        'Pre Paid',
        'Credit'
    );

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $status ) )
    {
        foreach( $status as $st => $stat )
        {
            if( is_array( $sts ) )
            {
                $option .= '<option value="' . $stat . '" ' . ( in_array( $stat, $sts ) ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
            else
            {
                $option .= '<option value="' . $stat . '" ' . ( $stat == $sts ? 'selected' : '' ) . ' >' . $stat . '</option>';
            }
        }
    }

    return $option;
}
/*
| -------------------------------------------------------------------------------------
| Report - filter
| -------------------------------------------------------------------------------------
*/
function get_filter_passenger_list_report()
{
    $filter = array( 'rid' => '', 'boid' => '', 'chids' => '', 'taid' => '', 'bddate' => date( 'F Y' ), 'bddateto' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ), 'rtype' => 1 );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $rid     = isset( $rid ) ? $rid : '';
        $filter  = array( 'rid' => $rid, 'boid' => $boid, 'chids' => $chids, 'taid' => $taid, 'bddate' => $bddate, 'bddateto' => $bddateto, 'bstatus' => $bstatus, 'rtype' => $rtype );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $rid     = isset( $rid ) ? $rid : '';
        $filter  = array( 'rid' => $rid, 'boid' => $boid, 'chids' => $chids, 'taid' => $taid, 'bddate' => $bddate, 'bddateto' => $bddateto, 'bstatus' => $bstatus, 'rtype' => $rtype );
    }

    return $filter;
}

function get_filter_transport_revenue_report()
{
    $filter = array( 'type' => '', 'rtype' => '', 'ttdate' => '', 'ttdateto' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ) );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'type' => $type, 'rtype' => $rtype, 'ttdate' => $ttdate, 'ttdateto' => $ttdateto, 'bstatus' => $bstatus );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'type' => $type, 'rtype' => $rtype, 'ttdate' => $ttdate, 'ttdateto' => $ttdateto, 'bstatus' => $bstatus );
    }

    return $filter;
}

function get_filter_transport_list_report()
{
    $filter = array( 'taid' => '', 'rid' => '', 'ttype' => '', 'rtype' => 1, 'ttdate' => '', 'ttdateto' => '', 'bstatus' =>  array( 'pa', 'ca', 'pp' ) );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'taid' => $taid, 'rid' => $rid, 'ttype' => $ttype, 'rtype' => $rtype, 'ttdate' => $ttdate, 'ttdateto' => $ttdateto, 'bstatus' => $bstatus );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'taid' => $taid, 'rid' => $rid, 'ttype' => $ttype, 'rtype' => $rtype, 'ttdate' => $ttdate, 'ttdateto' => $ttdateto, 'bstatus' => $bstatus );
    }

    return $filter;
}

function get_filter_daily_pax_numbers_report()
{
    $filter = array( 'rview' => '', 'rid' => '', 'chid' => '', 'rtype' => '0', 'bdate' => date( 'd F Y', time() ), 'bdateto' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ) );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'rview' => $rview, 'rid' => $rid, 'chid' => $chid, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'bstatus' => $bstatus );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'rview' => $rview, 'rid' => $rid, 'chid' => $chid, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'bstatus' => $bstatus );
    }

    return $filter;
}

function get_filter_reservation_report()
{
    $filter = array( 'rview' => '', 'rid' => '', 'chid' => '', 'rtype' => '1', 'bdate' => date( 'F Y', time() ), 'bdateto' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ) );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'rview' => $rview, 'rid' => $rid, 'chid' => $chid, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'bstatus' => $bstatus );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'rview' => $rview, 'rid' => $rid, 'chid' => $chid, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'bstatus' => $bstatus );
    }

    return $filter;
}

function get_filter_reservation_addons_report()
{
    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        return array( 'lcid' => $lcid, 'lcid2' => $lcid2, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        return array( 'lcid' => $lcid, 'lcid2' => $lcid2, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto );
    }
    else
    {
        if( isset( $_GET['type'] ) && $_GET['type'] == 'summary' )
        {
            return array( 'lcid' => '', 'lcid2' => '', 'rtype' => '1', 'bdate' => date( 'F Y' ), 'bdateto' => '' );
        }
        else
        {
            return array( 'lcid' => '', 'lcid2' => '', 'rtype' => '0', 'bdate' => date( 'd F Y' ), 'bdateto' => '' );
        }
    }
}

function get_filter_reservation_timeline_report()
{
    $filter = array( 'rid' => '', 'chid' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ), 'rtype' => '1', 'bdate' => date( 'F Y', time() ), 'bdateto' => '', 'rbased' => '0', 'cid' => '' );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'rid' => $rid, 'chid' => $chid, 'bstatus' => $bstatus, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'rbased' => $rbased, 'cid' => $cid );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $filter  = array( 'rid' => $rid, 'chid' => $chid, 'bstatus' => $bstatus, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto, 'rbased' => $rbased, 'cid' => $cid );
    }

    return $filter;
}

function get_filter_seats_production_report()
{
    $filter = array( 'ryear' => array( date( 'Y' ) ), 'rview' => 1, 'chid' => '', 'agsp' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ), 'rstatus' => array( 'Devinitive' => 'Devinitive', 'Tentative' => 'Tentative' ), 'rid' => '', 'tid' => '' , 'bmethod' => '3', 'pmethod' => '');

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $rstatus = isset( $rstatus ) ? $rstatus : '';
		$bmethod = isset( $bmethod ) ? $bmethod : '3';
        $ryear   = isset( $ryear ) ? $ryear : array();

        $filter  = array( 'ryear' => $ryear, 'rview' => $rview, 'chid' => $chid, 'agsp' => $agsp, 'bstatus' => $bstatus, 'rstatus' => $rstatus, 'rid' => $rid, 'tid' => $tid , 'bmethod' => $bmethod, 'pmethod' => $pmethod );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $rstatus = isset( $rstatus ) ? $rstatus : '';
		$bmethod = isset( $bmethod ) ? $bmethod : '3';
        $ryear   = isset( $ryear ) ? $ryear : array();

        $filter  = array( 'ryear' => $ryear, 'rview' => $rview, 'chid' => $chid, 'agsp' => $agsp, 'bstatus' => $bstatus, 'rstatus' => $rstatus, 'rid' => $rid, 'tid' => $tid, 'bmethod' => $bmethod, 'pmethod' => $pmethod );
    }

    return $filter;
}

function get_filter_ticket_revenue_report()
{
    $filter = array( 'rtype' => '1', 'rview' => '', 'bdate' => date( 'F Y', time() ), 'bdateto' => '', 'fchid' => '', 'fagid' => '', 'bsource' => '', 'chid' => array(), 'agsp' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ), 'rbased' => '', 'boid' => '', 'deport' => array(), 'feeopt' => 0, 'addonsopt' => 0 );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        if( empty( $rsource ) )
        {
        	$fchid = '';
        	$fagid = '';
        }
        else
        {
        	$fchid = array();
        	$fagid = array();

        	foreach( $rsource as $parts )
        	{
        		if( strpos($parts, '|') )
        		{
        			list( $chid, $agid ) = explode( '|', $parts );

        			$fchid[] = $chid;
        			$fagid[] = $agid;
        		}
        		else
        		{
        			$fchid[] = $parts;
        		}
        	}

        }

        $rsource = isset( $rsource ) ? $rsource : '';
        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $chid    = isset( $chid ) ? $chid : '';

        $filter  = array( 'rtype' => $rtype, 'rview' => $rview, 'bdate' => $bdate, 'bdateto' => $bdateto, 'fchid' => $fchid, 'fagid' => $fagid, 'bsource' => $rsource, 'chid' => $chid, 'agsp' => $agsp, 'bstatus' => $bstatus, 'rbased' => $rbased, 'boid' => $boid, 'deport' => $deport, 'feeopt' => $feeopt, 'addonsopt' => $addonsopt );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_POST );

        if( empty( $rsource ) )
        {
        	$fchid = '';
        	$fagid = '';
        }
        else
        {
        	$fchid = array();
        	$fagid = array();

        	foreach ($rsource as $parts)
        	{
        		if ( strpos($parts, '|') )
        		{
        			list( $chid, $agid ) = explode( '|', $parts );
        			$fchid[] = $chid;
        			$fagid[] = $agid;

        		}
        		else
        		{
        			$fchid[] = $parts;
        		}
        	}

        }

        $rsource = isset( $rsource ) ? $rsource : '';
        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $chid    = isset( $chid ) ? $chid : '';

        $filter  = array( 'rtype' => $rtype, 'rview' => $rview, 'bdate' => $bdate, 'bdateto' => $bdateto, 'fchid' => $fchid, 'fagid' => $fagid, 'bsource' => $rsource, 'chid' => $chid, 'agsp' => $agsp, 'bstatus' => $bstatus, 'rbased' => $rbased, 'boid' => $boid, 'deport' => $deport, 'feeopt' => $feeopt, 'addonsopt' => $addonsopt );
    }

    return $filter;
}

function get_filter_ticket_average_rates_report()
{
    $filter = array( 'ryear' => array( date( 'Y' ) ), 'rsource' => array(), 'tid' => array(), 'rid' => array(), 'agsp' => '', 'fchid' => '', 'fagid' => '' );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        if( empty( $rsource ) )
        {
            $fchid = '';
            $fagid = '';
        }
        else
        {
            $fchid = array();
            $fagid = array();

            foreach ($rsource as $parts)
            {
                if ( strpos($parts, '|') )
                {
                    list( $chid, $agid ) = explode( '|', $parts );
                    $fchid[] = $chid;
                    $fagid[] = $agid;
                }
                else
                {
                    $fchid[] = $parts;
                }
            }
        }

        $rsource = isset( $rsource ) ? $rsource : '';
        $ryear   = isset( $ryear ) ? $ryear : array();
        $tid     = isset( $tid ) ? $tid : array();
        $rid     = isset( $rid ) ? $rid : array();
        $filter  = array( 'ryear' => $ryear, 'rsource' => $rsource, 'tid' => $tid, 'rid' => $rid, 'agsp' => $agsp, 'fchid' => $fchid, 'fagid' => $fagid );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        if( empty( $rsource ) )
        {
            $fchid = '';
            $fagid = '';
        }
        else
        {
            $fchid = array();
            $fagid = array();

            foreach ($rsource as $parts)
            {
                if ( strpos($parts, '|') )
                {
                    list( $chid, $agid ) = explode( '|', $parts );
                    $fchid[] = $chid;
                    $fagid[] = $agid;
                }
                else
                {
                    $fchid[] = $parts;
                }
            }
        }

        $rsource = isset( $rsource ) ? $rsource : '';
        $ryear   = isset( $ryear ) ? $ryear : array();
        $tid     = isset( $tid ) ? $tid : array();
        $rid     = isset( $rid ) ? $rid : array();
        $filter  = array( 'ryear' => $ryear, 'rsource' => $rsource, 'tid' => $tid, 'rid' => $rid, 'agsp' => $agsp, 'fchid' => $fchid, 'fagid' => $fagid );
    }

    return $filter;
}

function get_filter_ticket_market_segment_report()
{
    $filter = array( 'ryear' => '', 'rview' => ''  );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $filter = array( 'ryear' => $ryear, 'rview' => $rview );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $filter = array( 'ryear' => $ryear, 'rview' => $rview );
    }

    return $filter;
}

function get_filter_ticket_hs_sales_sum_report()
{
    $filter = array( 'lcid' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ), 'rsource' => '' ,'bdate' => date( 'Y' ), 'bdateto' => '','rtype' => 2 );

    if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $rsource  = isset( $rsource ) ? $rsource : '';
        $bstatus  = isset( $bstatus ) ? $bstatus : '';
		$bddate   = isset( $bddate ) ? $bddate : '';
		$bddateto = isset( $bddateto ) ? $bddateto : '';

        $filter   = array( 'lcid' => $lcid, 'bstatus' => $bstatus, 'fchid' => $fchid, 'fagid' => $fagid, 'rsource' => $rsource, 'bddate' => $bddate, 'bddateto' => $bddateto,'rtype' => $rtype );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        if( empty( $rsource ) )
        {
        	$fchid = '';
        	$fagid = '';
        }
        else
        {
        	$fchid = array();
        	$fagid = array();

        	foreach ($rsource as $parts)
        	{
        		if ( strpos($parts, '|') )
        		{
        			list( $chid, $agid ) = explode( '|', $parts );

        			$fchid[] = $chid;
        			$fagid[] = $agid;

        		}
        		else
        		{
        			$fchid[] = $parts;
        		}
        	}

        }

        $rsource  = isset( $rsource ) ? $rsource : '';
        $bstatus  = isset( $bstatus ) ? $bstatus : '';
		$bddate   = isset( $bddate ) ? $bddate : '';
		$bddateto = isset( $bddateto ) ? $bddateto : '';

		$filter   = array( 'lcid' => $lcid, 'bstatus' => $bstatus, 'fchid' => $fchid, 'fagid' => $fagid, 'rsource' => $rsource, 'bddate' => $bddate, 'bddateto' => $bddateto,'rtype' => $rtype );

    }

    return $filter;
}

function get_filter_ticket_promo_code_report()
{
    $filter = array( 'chid' => '', 'promo_code' => '', 'bstatus' => array( 'pa', 'ca', 'pp' ), 'rbased' => '0', 'rtype' => '1', 'bdate' => date( 'F Y', time() ), 'bdateto' => '' );

	if( isset( $_POST['filter'] ) )
    {
        extract( $_POST );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $pmcode  = isset( $pmcode ) ? $pmcode : '';

        $filter  = array( 'chid' => $chid, 'promo_code' => $pmcode, 'bstatus' => $bstatus, 'rbased' => $rbased, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto );
    }
    elseif( isset( $_GET['filter'] ) )
    {
        extract( $_GET );

        $bstatus = isset( $bstatus ) ? $bstatus : '';
        $pmcode  = isset( $pmcode ) ? $pmcode : '';

        $filter  = array( 'chid' => $chid, 'promo_code' => $pmcode, 'bstatus' => $bstatus, 'rbased' => $rbased, 'rtype' => $rtype, 'bdate' => $bdate, 'bdateto' => $bdateto );
    }

    return $filter;
}

/*
| -------------------------------------------------------------------------------------
| Report - Feedback Report
| -------------------------------------------------------------------------------------
*/
function ticket_feedback_report()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url('report&sub=feedback-report') . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    extract( ticket_filter_guest_name() );

    set_template( PLUGINS_PATH . '/ticket/tpl/report/feedback-report.html', 'report' );
    add_block( 'report-block', 'rp-block', 'report' );

    $list    = get_ticket_feedback_list( $search );
    $content = '';

    if ( !empty( $list ) )
    {
        foreach ( $list as $dt )
        {
            $content .=
            '<tr>
                <td style="width:33.33333%">'. $dt['fname'] . '<br/>' . $dt['bticket'] . '</td>
                <td style="width:33.33333%">' . get_rating_star( $dt['frating'] ) . '</td>
                <td style="width:33.33333%">' . $dt['fmessage'] . '</td>
            </tr>';
        }
    }
    else
    {
        $content .=
        '<tr>
            <td colspan="5">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    add_variable( 'list', $content );
    add_variable( 'search', $search );
    add_variable( 'limit', post_viewed() );
    add_variable( 'avgrat', ticket_guest_feedback_average_rating('1') );
    add_variable( 'action', get_state_url( 'report&sub=feedback-report' ) );

    add_actions( 'section_title', 'Report - Feedback' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'report-block', 'rp-block', false );

    return return_template( 'report' );
}

function get_ticket_feedback_list( $search )
{
    global $db;

    $scname = '';

    if ( $search != '' )
    {
        $scname = $db->prepare_query( ' AND f.fname LIKE %s', '%'.$search.'%' );
    }

    $s = 'SELECT f.fname, b.bticket, f.frating, f.fmessage FROM ticket_booking AS b LEFT JOIN ticket_feedback AS f ON b.bid = f.bid WHERE f.fstatus="1"' . $scname;
    $r = $db->do_query( $s );

    $data = array();

    if ( $db->num_rows( $r ) > 0 )
    {
        while ( $d = $db->fetch_array( $r ) )
        {
            $data[] = array(
                'bticket'  => $d['bticket'],
                'fname'    => $d['fname'],
                'frating'  => $d['frating'],
                'fmessage' => $d['fmessage']
            );
        }
    }

    return $data;
}

function ticket_filter_guest_name()
{
    $filter = array( 'search' => '' );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array( 'search' => $search  );
    }

    return $filter;
}

function get_discount_promo_code( $bid )
{
    global $db;

    $i = 0;
    $d = $db->prepare_query( 'SELECT bdiscount FROM ticket_booking WHERE bid = %d', $bid );
    $r = $db->do_query( $d );

    if ( $d = $db->fetch_array( $r ) )
    {
        $i = $d['bdiscount'];
    }

    return $i;
}

function get_num_count_trip( $bid )
{
    global $db;

    $i = 0;
    $d = $db->prepare_query( 'SELECT COUNT(bdid) AS num FROM ticket_booking_detail WHERE bid = %d', $bid );
    $r = $db->do_query( $d );

    if ( $d = $db->fetch_array( $r ) )
    {
        $i = $d['num'];
    }

    return $i;
}

function get_sale_channel_option_revenue( $chid = '', $use_empty = true, $empty_text = 'Select Sale Channel' )
{
    global $db;

    $p = $db->prepare_query( 'SELECT chid, chcode, chname FROM ticket_channel WHERE chstatus = %s ORDER BY chname ASC', 'publish' );
    $q = $db->do_query( $p );
    $r = $db->num_rows( $q );

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if ( $r > 0 )
    {
        while ( $d = $db->fetch_array( $q ) )
        {
            $option .= '<option value="' . $d['chid'] . '" ' . ( $d['chid'] == $chid ? 'selected' : '' ) . '>(' . $d['chcode'] . ') ' . $d['chname'] . '</option>';
        }
    }

    return $option;
}


function get_seats_production_dashboard()
{
    $this_year = array(date("Y"));
    $agsp    = isset( $agsp ) ? $agsp : '';
    $bstatus = isset( $bstatus ) ? $bstatus : '';
    $ryear   = isset( $ryear ) ? $ryear : $this_year;
    $rid     = isset( $rid ) ? $rid : array();
    $tid     = isset( $tid ) ? $tid : array();
    $rview   = isset( $rview ) ? $rview : '1';
    $chid    = isset( $chid ) ? $chid : '';
    $rstatus = isset( $rstatus ) ? $rstatus : '';
	$bmethod = isset( $bmethod ) ? $bmethod : '';
	$pmethod = isset( $pmethod ) ? $pmethod : '';

    $filter  = array( 'ryear' => $ryear, 'rview' => $rview, 'chid' => $chid, 'agsp' => $agsp, 'bstatus' => $bstatus, 'rstatus' => $rstatus, 'rid' => $rid, 'tid' => $tid, 'bmethod' => $bmethod, 'pmethod' => $pmethod);

	$data = get_seats_production_sum_report( $filter );

	if( !empty( $data ) )
	{
	    add_block( 'report-loop', 'r-loop', 'dashboard' );
	    add_block( 'report-block', 'r-block', 'dashboard' );
        
        

        $page = 1;

		foreach( $data['items'] as $year => $items )
	    {
		    $tblcontent = '';
		    $tblsumdata = '';
		    $last_year  = date( 'Y', strtotime( '-1 year', strtotime( $year . '-01-01' ) ) );

            $sum_01 = array();
            $sum_02 = array();
            $sum_03 = array();
            $sum_04 = array();
            $sum_05 = array();
            $sum_06 = array();
            $sum_07 = array();
            $sum_08 = array();
            $sum_09 = array();
            $sum_10 = array();
            $sum_11 = array();
            $sum_12 = array();
            $sum_al = array();

            $tblarray = array(); 

	    	foreach( $items as $obj => $item )
	    	{
	    		$ttl_01 = isset( $item['01'] ) ? $item['01'] : 0;
	    		$ttl_02 = isset( $item['02'] ) ? $item['02'] : 0;
	    		$ttl_03 = isset( $item['03'] ) ? $item['03'] : 0;
	    		$ttl_04 = isset( $item['04'] ) ? $item['04'] : 0;
	    		$ttl_05 = isset( $item['05'] ) ? $item['05'] : 0;
	    		$ttl_06 = isset( $item['06'] ) ? $item['06'] : 0;
	    		$ttl_07 = isset( $item['07'] ) ? $item['07'] : 0;
	    		$ttl_08 = isset( $item['08'] ) ? $item['08'] : 0;
	    		$ttl_09 = isset( $item['09'] ) ? $item['09'] : 0;
	    		$ttl_10 = isset( $item['10'] ) ? $item['10'] : 0;
	    		$ttl_11 = isset( $item['11'] ) ? $item['11'] : 0;
	    		$ttl_12 = isset( $item['12'] ) ? $item['12'] : 0;
	    		$ttl_al = array_sum( $item );

                $sum_01[] = $ttl_01;
                $sum_02[] = $ttl_02;
                $sum_03[] = $ttl_03;
                $sum_04[] = $ttl_04;
                $sum_05[] = $ttl_05;
                $sum_06[] = $ttl_06;
                $sum_07[] = $ttl_07;
                $sum_08[] = $ttl_08;
                $sum_09[] = $ttl_09;
                $sum_10[] = $ttl_10;
                $sum_11[] = $ttl_11;
                $sum_12[] = $ttl_12;
                $sum_al[] = $ttl_al;

	    		if( $rview == 1 )
	    		{
	    			list( $code, $name, $sales_person ) = json_decode( base64_decode( $obj ) );

			    	$tblarray[] = array( 
			    		'code' => $code, 
			    		'name' => $name, 
			    		'ttl_01' => $ttl_01, 
			    		'ttl_02' => $ttl_02, 
			    		'ttl_03' => $ttl_03, 
			    		'ttl_04' => $ttl_04, 
			    		'ttl_05' => $ttl_05, 
			    		'ttl_06' => $ttl_06, 
			    		'ttl_07' => $ttl_07, 
			    		'ttl_08' => $ttl_08, 
			    		'ttl_09' => $ttl_09, 
			    		'ttl_10' => $ttl_10, 
			    		'ttl_11' => $ttl_11, 
			    		'ttl_12' => $ttl_12, 
			    		'ttl_al' => $ttl_al, 
			    		'sales_person' => $sales_person
			    	);
	    		}
	    		else
	    		{
	    			list( $code, $name ) = json_decode( base64_decode( $obj ) );

			    	$tblarray[] = array( 
			    		'code' => $code, 
			    		'name' => $name, 
			    		'ttl_01' => $ttl_01, 
			    		'ttl_02' => $ttl_02, 
			    		'ttl_03' => $ttl_03, 
			    		'ttl_04' => $ttl_04, 
			    		'ttl_05' => $ttl_05, 
			    		'ttl_06' => $ttl_06, 
			    		'ttl_07' => $ttl_07, 
			    		'ttl_08' => $ttl_08, 
			    		'ttl_09' => $ttl_09, 
			    		'ttl_10' => $ttl_10, 
			    		'ttl_11' => $ttl_11, 
			    		'ttl_12' => $ttl_12, 
			    		'ttl_al' => $ttl_al
			    	);
	    		}
	    	}

	    	array_multisort( array_map( function( $element ){
				return $element['ttl_al'];
			}, $tblarray ), SORT_DESC, $tblarray );

            $no = 1;
	    	foreach( $tblarray as $key => $val )
	    	{
	    	    if( $no < 11){
	    	         extract( $val );
    
    	    		if( $rview == 1 )
    	    		{
    			    	$spcol = '<td align="center">' . $sales_person . '</td>';
    				}
    				else
    				{
    			    	$spcol = '';
    				}
    
    	    		$tblcontent .= '		    	
    				<tr>
    					<td align="center">' . $no . '</td>
    					<td align="center">' . $code . '</td>
    					<td>' . $name . '</td>
    					<td align="right">' . $ttl_01 . '</td>
    					<td align="right" >' . $ttl_02 . '</td>
    					<td align="right" >' . $ttl_03 . '</td>
    					<td align="right" >' . $ttl_04 . '</td>
    					<td align="right" >' . $ttl_05 . '</td>
    					<td align="right" >' . $ttl_06 . '</td>
    					<td align="right" >' . $ttl_07 . '</td>
    					<td align="right" >' . $ttl_08 . '</td>
    					<td align="right" >' . $ttl_09 . '</td>
    					<td align="right" >' . $ttl_10 . '</td>
    					<td align="right" >' . $ttl_11 . '</td>
    					<td align="right" >' . $ttl_12 . '</td>
    					<td align="right" >' . $ttl_al . '</td>
    					' . $spcol . '
    				</tr>';
    
    				$no++;
	    	    }
	    	}

            $sum_01_prev = isset( $data['previous'][$last_year]['01'] ) ? $data['previous'][$last_year]['01'] : 0;
            $sum_02_prev = isset( $data['previous'][$last_year]['02'] ) ? $data['previous'][$last_year]['02'] : 0;
            $sum_03_prev = isset( $data['previous'][$last_year]['03'] ) ? $data['previous'][$last_year]['03'] : 0;
            $sum_04_prev = isset( $data['previous'][$last_year]['04'] ) ? $data['previous'][$last_year]['04'] : 0;
            $sum_05_prev = isset( $data['previous'][$last_year]['05'] ) ? $data['previous'][$last_year]['05'] : 0;
            $sum_06_prev = isset( $data['previous'][$last_year]['06'] ) ? $data['previous'][$last_year]['06'] : 0;
            $sum_07_prev = isset( $data['previous'][$last_year]['07'] ) ? $data['previous'][$last_year]['07'] : 0;
            $sum_08_prev = isset( $data['previous'][$last_year]['08'] ) ? $data['previous'][$last_year]['08'] : 0;
            $sum_09_prev = isset( $data['previous'][$last_year]['09'] ) ? $data['previous'][$last_year]['09'] : 0;
            $sum_10_prev = isset( $data['previous'][$last_year]['10'] ) ? $data['previous'][$last_year]['10'] : 0;
            $sum_11_prev = isset( $data['previous'][$last_year]['11'] ) ? $data['previous'][$last_year]['11'] : 0;
            $sum_12_prev = isset( $data['previous'][$last_year]['12'] ) ? $data['previous'][$last_year]['12'] : 0;
            $sum_al_prev = isset( $data['previous'][$last_year] ) ? array_sum( $data['previous'][$last_year] ) : 0;

            $sum_01_curr = array_sum( $sum_01 );
            $sum_02_curr = array_sum( $sum_02 );
            $sum_03_curr = array_sum( $sum_03 );
            $sum_04_curr = array_sum( $sum_04 );
            $sum_05_curr = array_sum( $sum_05 );
            $sum_06_curr = array_sum( $sum_06 );
            $sum_07_curr = array_sum( $sum_07 );
            $sum_08_curr = array_sum( $sum_08 );
            $sum_09_curr = array_sum( $sum_09 );
            $sum_10_curr = array_sum( $sum_10 );
            $sum_11_curr = array_sum( $sum_11 );
            $sum_12_curr = array_sum( $sum_12 );
            $sum_al_curr = array_sum( $sum_al );

            $sum_01_diff = $sum_01_curr - $sum_01_prev;
            $sum_02_diff = $sum_02_curr - $sum_02_prev;
            $sum_03_diff = $sum_03_curr - $sum_03_prev;
            $sum_04_diff = $sum_04_curr - $sum_04_prev;
            $sum_05_diff = $sum_05_curr - $sum_05_prev;
            $sum_06_diff = $sum_06_curr - $sum_06_prev;
            $sum_07_diff = $sum_07_curr - $sum_07_prev;
            $sum_08_diff = $sum_08_curr - $sum_08_prev;
            $sum_09_diff = $sum_09_curr - $sum_09_prev;
            $sum_10_diff = $sum_10_curr - $sum_10_prev;
            $sum_11_diff = $sum_11_curr - $sum_11_prev;
            $sum_12_diff = $sum_12_curr - $sum_12_prev;
            $sum_al_diff = $sum_al_curr - $sum_al_prev;

            $sum_01_perc = $sum_01_diff > 0 ? round( ( $sum_01_diff * 100 ) / $sum_01_curr, 2 ) : 0;
            $sum_02_perc = $sum_02_diff > 0 ? round( ( $sum_02_diff * 100 ) / $sum_02_curr, 2 ) : 0;
            $sum_03_perc = $sum_03_diff > 0 ? round( ( $sum_03_diff * 100 ) / $sum_03_curr, 2 ) : 0;
            $sum_04_perc = $sum_04_diff > 0 ? round( ( $sum_04_diff * 100 ) / $sum_04_curr, 2 ) : 0;
            $sum_05_perc = $sum_05_diff > 0 ? round( ( $sum_05_diff * 100 ) / $sum_05_curr, 2 ) : 0;
            $sum_06_perc = $sum_06_diff > 0 ? round( ( $sum_06_diff * 100 ) / $sum_06_curr, 2 ) : 0;
            $sum_07_perc = $sum_07_diff > 0 ? round( ( $sum_07_diff * 100 ) / $sum_07_curr, 2 ) : 0;
            $sum_08_perc = $sum_08_diff > 0 ? round( ( $sum_08_diff * 100 ) / $sum_08_curr, 2 ) : 0;
            $sum_09_perc = $sum_09_diff > 0 ? round( ( $sum_09_diff * 100 ) / $sum_09_curr, 2 ) : 0;
            $sum_10_perc = $sum_10_diff > 0 ? round( ( $sum_10_diff * 100 ) / $sum_10_curr, 2 ) : 0;
            $sum_11_perc = $sum_11_diff > 0 ? round( ( $sum_11_diff * 100 ) / $sum_11_curr, 2 ) : 0;
            $sum_12_perc = $sum_12_diff > 0 ? round( ( $sum_12_diff * 100 ) / $sum_12_curr, 2 ) : 0;
            $sum_al_perc = $sum_al_diff > 0 ? round( ( $sum_al_diff * 100 ) / $sum_al_curr, 2 ) : 0;

    		if( $rview == 1 )
    		{
		    	$spcol = '<td align="right">&nbsp;</td>';
		    	$data_sp_column = '<td rowspan="3" align="center"><b></b></td>';
			}
			else
			{
		    	$spcol = '';
		    	$data_sp_column = '';
			}

	    	$tblvar = '
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><b>%</b></td>
                <td align="right" ><b>' . $sum_01_perc . '</b></td>
                <td align="right" ><b>' . $sum_02_perc . '</b></td>
                <td align="right" ><b>' . $sum_03_perc . '</b></td>
                <td align="right" ><b>' . $sum_04_perc . '</b></td>
                <td align="right" ><b>' . $sum_05_perc . '</b></td>
                <td align="right" ><b>' . $sum_06_perc . '</b></td>
                <td align="right" ><b>' . $sum_07_perc . '</b></td>
                <td align="right" ><b>' . $sum_08_perc . '</b></td>
                <td align="right" ><b>' . $sum_09_perc . '</b></td>
                <td align="right" ><b>' . $sum_10_perc . '</b></td>
                <td align="right" ><b>' . $sum_11_perc . '</b></td>
                <td align="right" ><b>' . $sum_12_perc . '</b></td>
                <td align="right" ><b>' . $sum_al_perc . '</b></td>
                ' . $spcol . '
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><b>Variance</b></td>
                <td align="right" ><b>' . $sum_01_diff . '</b></td>
                <td align="right" ><b>' . $sum_02_diff . '</b></td>
                <td align="right" ><b>' . $sum_03_diff . '</b></td>
                <td align="right" ><b>' . $sum_04_diff . '</b></td>
                <td align="right" ><b>' . $sum_05_diff . '</b></td>
                <td align="right" ><b>' . $sum_06_diff . '</b></td>
                <td align="right" ><b>' . $sum_07_diff . '</b></td>
                <td align="right" ><b>' . $sum_08_diff . '</b></td>
                <td align="right" ><b>' . $sum_09_diff . '</b></td>
                <td align="right" ><b>' . $sum_10_diff . '</b></td>
                <td align="right" ><b>' . $sum_11_diff . '</b></td>
                <td align="right" ><b>' . $sum_12_diff . '</b></td>
                <td align="right" ><b>' . $sum_al_diff . '</b></td>
                ' . $spcol . '
            </tr>';

	    	$tblprevsum = '
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><b>' . $last_year . '</b></td>
                <td align="right" ><b>' . $sum_01_prev . '</b></td>
                <td align="right" ><b>' . $sum_02_prev . '</b></td>
                <td align="right" ><b>' . $sum_03_prev . '</b></td>
                <td align="right" ><b>' . $sum_04_prev . '</b></td>
                <td align="right" ><b>' . $sum_05_prev . '</b></td>
                <td align="right" ><b>' . $sum_06_prev . '</b></td>
                <td align="right" ><b>' . $sum_07_prev . '</b></td>
                <td align="right" ><b>' . $sum_08_prev . '</b></td>
                <td align="right" ><b>' . $sum_09_prev . '</b></td>
                <td align="right" ><b>' . $sum_10_prev . '</b></td>
                <td align="right" ><b>' . $sum_11_prev . '</b></td>
                <td align="right" ><b>' . $sum_12_prev . '</b></td>
                <td align="right" ><b>' . $sum_al_prev . '</b></td>
                ' . $spcol . '
            </tr>';

	    	$tblsumdata = '
            <tr>
                <td align="right" ><b>' . $sum_01_curr . '</b></td>
                <td align="right" ><b>' . $sum_02_curr . '</b></td>
                <td align="right" ><b>' . $sum_03_curr . '</b></td>
                <td align="right" ><b>' . $sum_04_curr . '</b></td>
                <td align="right" ><b>' . $sum_05_curr . '</b></td>
                <td align="right" ><b>' . $sum_06_curr . '</b></td>
                <td align="right" ><b>' . $sum_07_curr . '</b></td>
                <td align="right" ><b>' . $sum_08_curr . '</b></td>
                <td align="right" ><b>' . $sum_09_curr . '</b></td>
                <td align="right" ><b>' . $sum_10_curr . '</b></td>
                <td align="right" ><b>' . $sum_11_curr . '</b></td>
                <td align="right" ><b>' . $sum_12_curr . '</b></td>
                <td align="right" ><b>' . $sum_al_curr . '</b></td>
            </tr>';

	    	$tblcontent .= '		    	
			<tr>
				<td align="center" colspan="15"><b>GRAND TOTAL</b></td>
				<td align="right"  ><b>' . $sum_al_curr . '</b></td>
                ' . $spcol . '
			</tr>';

	    	add_variable( 'year', $year );
	    	add_variable( 'data_tbl', $tblcontent );
	    	add_variable( 'data_tbl_sum', $tblsumdata );
	    	add_variable( 'data_tbl_prev_sum', $tblprevsum );
	    	add_variable( 'data_sp_column', $data_sp_column );
	    	add_variable( 'data_tbl_variance_and_percent', $tblvar );
	    	add_variable( 'pagebreak', count( $data['items'] ) == $page ? '' : '<pagebreak>' );

        	parse_template( 'report-loop', 'r-loop', true );

        	$page++;
	    }

		add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}
?>
