<?php

add_actions( 'notification', 'ticket_notification' );
add_actions( 'ticket-notification-ajax_page', 'ticket_notification_ajax' );

function ticket_notification()
{
    return ticket_notification_table();
}

/*
| -------------------------------------------------------------------------------------
| Notification Table List
| -------------------------------------------------------------------------------------
*/
function ticket_notification_table()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url( 'notification' ) . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    extract( ticket_filter_notification() );
    
    $result   = ticket_notification_table_query( $status );
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/notification/list.html', 'notification' );

    add_block( 'list-block', 'lcblock', 'notification' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', ticket_notification_table_data( $result ) );
    add_variable( 'status_option', get_notification_status_option( $status, true, 'All Status') );
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Notification List' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'notification' );
}

/*
| -------------------------------------------------------------------------------------
| Notification Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_notification_table_query( $status )
{
    global $db;

    $w = '';

    if( ticket_notification_is_filter_view() )
    {
        if( $status != '' )
        {
            $w .= $db->prepare_query( 'nstatus = %d', $status );
        }
    }

    if( empty( $w ) )
    {
        $s = 'SELECT * FROM ticket_notification ORDER BY nid DESC';
        $q = $db->prepare_query( $s );
        $r = $db->do_query( $q );
    }
    else
    {
        $s = 'SELECT * FROM ticket_notification WHERE ' . $w . ' ORDER BY nid DESC';
        $q = $db->prepare_query( $s );
        $r = $db->do_query( $q );
    }

    return $r;
}

/*
| -------------------------------------------------------------------------------------
| Notification Table List Item
| -------------------------------------------------------------------------------------
*/
function ticket_notification_table_data( $r )
{
    global $db;

    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="4">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/notification/loop.html', 'notification-loop' );

    add_block( 'loop-block', 'lcloop', 'notification-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'id', $d['nid'] );
        add_variable( 'ntitle', $d['ntitle'] );
        add_variable( 'ncontent', strip_tags( $d['ncontent'], '<a>' ) );
        add_variable( 'close_txt', $d['nstatus'] == 0 ? 'Close &raquo;' : '&nbsp;' );
        add_variable( 'nstatus', $d['nstatus'] == 0 ? 'Not Read' : 'Read' );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-notification-ajax/' );

        parse_template( 'loop-block', 'lcloop', true );
    }

    return return_template( 'notification-loop' );
}

function ticket_filter_notification()
{
    $filter = array( 'status' => '' );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array( 'status' => $nstatus );
    }

    return $filter;
}

function get_notification_status_option( $sts = '', $use_empty = true, $empty_text = 'Select Status' )
{
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="0" ' . ( $sts == '0' ? 'selected' : '' ) . ' >Not Read</option>
    <option value="1" ' . ( $sts == '1' ? 'selected' : '' ) . ' >Read</option>';

    return $option;
}

function ticket_notification_is_filter_view()
{
    if( isset( $_GET['state'] ) && $_GET['state']=='notification' && isset( $_GET['prm'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function ticket_notification_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'update-ticket-notification' )
    {
        if( update_ticket_notification( $_POST['nid'], $_POST['nstatus'] ) )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"failed"}';   
        }
    }
}