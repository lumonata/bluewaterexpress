<?php

add_actions( 'guest-feedback', 'ticket_guest_feedback' );
add_actions( 'ticket-feedback-ajax_page', 'ticket_feedback_ajax' );

function ticket_guest_feedback()
{
    if( is_delete_all() )
    {
        return ticket_batch_delete_feedback();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_feedback( $val );
        }
    }

    return ticket_guest_feedback_table();
}

/*
| -------------------------------------------------------------------------------------
| Guest Feedback Table List
| -------------------------------------------------------------------------------------
*/
function ticket_guest_feedback_table()
{
    if( ( isset( $_POST ) && !empty( $_POST ) ) && !isset( $_POST['confirm_delete'] ) )
    {
        header( 'Location:' . get_state_url('admin&sub=guest-feedback') . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    extract( ticket_filter_guest_feedback() );

    $result   = ticket_guest_feedback_table_query( $rating, $feedback_status, $search );
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/feedback/list.html', 'feedback' );

    add_block( 'list-block', 'lblock', 'feedback' );

    add_variable( 'search', $search );
    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'rating_option', get_rating_option( $rating ) );
    add_variable( 'feedback_status', get_status_option( $feedback_status ) );
    add_variable( 'avrating', ticket_guest_feedback_average_rating( $feedback_status ) );
    add_variable( 'list', ticket_guest_feedback_table_data( $result ) );
    add_variable( 'action', get_state_url( 'admin&sub=guest-feedback' ) );
    add_variable( 'edit_link', get_state_url( 'admin&sub=guest-feedback&prc=edit' ) );

    parse_template( 'list-block', 'lblock', false );

    add_actions( 'section_title', 'Guest Feedback List' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'feedback' );
}

/*
| -------------------------------------------------------------------------------------
| Feedback Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_guest_feedback_average_rating( $status='' )
{
    global $db;

    $wh = '';

    if ( $status != '' )
    {
        $wh = $db->prepare_query( ' WHERE a.fstatus=%s', $status );
    }

    $q = $db->prepare_query( 'SELECT a.frating, a.fid FROM ticket_feedback AS a '.$wh );
    $r = $db->do_query( $q );

    if ( !is_array( $r ) && $db->num_rows( $r ) > 0 )
    {
        $srat = 0;
        $crat = 0;

        while ( $d = $db->fetch_array( $r ) )
        {
            $srat = $srat + intval( $d['frating'] );
            $crat++;
        }

        return get_rating_star( round( ( $srat / $crat ) ) );
    }
}


function ticket_guest_feedback_table_query( $rating = '', $feedback_status = '', $search = '' )
{
    global $db;

    $where = array();

    if ( $feedback_status != '' )
    {
        $where[] = $db->prepare_query( ' fstatus = %s', $feedback_status );
    }

    if ( $rating != '' )
    {
        $where[] = $db->prepare_query( ' frating = %s', $rating );
    }

    if( !empty( $search ) )
    {
        if( !empty( $where ) )
        {
            $s = 'SELECT * FROM ticket_feedback WHERE ( fname LIKE %s OR femail LIKE %s ) AND ' . implode( ' AND ', $where ) . ' ORDER BY fid DESC';
            $q = $db->prepare_query( $s, '%' . $search . '%', '%' . $search . '%' );
            $r = $db->do_query( $q );
        }
        else
        {
            $s = 'SELECT * FROM ticket_feedback WHERE fname LIKE %s OR femail LIKE %s ORDER BY fid DESC';
            $q = $db->prepare_query( $s, '%' . $search . '%', '%' . $search . '%' );
            $r = $db->do_query( $q );
        }
    }
    else
    {
        if( !empty( $where ) )
        {
            $s = 'SELECT * FROM ticket_feedback WHERE ' . implode( ' AND ', $where ) . ' ORDER BY fid DESC';
            $q = $db->prepare_query( $s );
            $r = $db->do_query( $q );
        }
        else
        {
            $s = 'SELECT * FROM ticket_feedback ORDER BY fid DESC';
            $q = $db->prepare_query( $s );
            $r = $db->do_query( $q );
        }
    }

    return $r;
}

/*
| -------------------------------------------------------------------------------------
| Guest Feedback Table List Item
| -------------------------------------------------------------------------------------
*/
function ticket_guest_feedback_table_data( $r )
{
    global $db;

    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="5">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/feedback/loop.html', 'feedback-loop' );

    add_block( 'loop-block', 'lloop', 'feedback-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'id', $d['fid'] );
        add_variable( 'fname', $d['fname'] );
        add_variable( 'femail', $d['femail'] );
        add_variable( 'fmessage', $d['fmessage'] );
        add_variable( 'frating', get_rating_star( $d['frating'] ) );
        add_variable( 'fstatus_option', get_status_option( $d['fstatus'] ) );
        add_variable( 'fstatus', $d['fstatus'] == 1 ? 'Approved' : ( $d['fstatus'] == 2 ? 'Rejected' : 'Pending' ) );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-feedback-ajax/' );

        parse_template( 'loop-block', 'lloop', true );
    }

    return return_template( 'feedback-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Transport Area
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_feedback()
{
    if( isset( $_POST['select'] ) && !empty( $_POST['select'] ) )
    {
        set_template( PLUGINS_PATH . '/ticket/tpl/feedback/batch-delete.html', 'feedback' );
        add_block( 'loop-block', 'lclblock', 'feedback' );
        add_block( 'delete-block', 'lcblock', 'feedback' );

        foreach( $_POST['select'] as $key=>$val )
        {
            $d = get_feedback( $val );

            add_variable( 'fname',  $d['fname'] );
            add_variable( 'fid', $d['fid'] );

            parse_template( 'loop-block', 'lclblock', true );
        }

        add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' feedback? :' );
        add_variable( 'action', get_state_url( 'admin&sub=guest-feedback' ));

        parse_template( 'delete-block', 'lcblock', false );

        add_actions( 'section_title', 'Delete Guest Feedback' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        return return_template( 'feedback' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Guest Feedback By ID
| -------------------------------------------------------------------------------------
*/
function get_feedback( $id='', $field = '' )
{
    global $db;

    $data = array(
        'fid'      => ( isset( $_POST['fid'] ) ? $_POST['fid'] : null ),
        'fname'    => ( isset( $_POST['fname'] ) ? $_POST['fname'] : '' ),
        'femail'   => ( isset( $_POST['femail'] ) ? $_POST['femail'] : '' ),
        'frating'  => ( isset( $_POST['frating'] ) ? $_POST['frating'] : '' ),
        'fstatus'  => ( isset( $_POST['fstatus'] ) ? $_POST['fstatus'] : '' ),
        'fmessage' => ( isset( $_POST['fmessage'] ) ? $_POST['fmessage'] : '' )
    );

    $s = 'SELECT * FROM ticket_feedback WHERE fid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'fid' => $d['fid'],
                'fname' => $d['fname'],
                'femail' => $d['femail'],
                'frating' => $d['frating'],
                'fstatus' => $d['fstatus'],
                'fmessage' => $d['fmessage']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Guest Feedback By ID
| -------------------------------------------------------------------------------------
*/
function get_feedback_by_booking_id( $bid = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_feedback WHERE bid = %d';
    $q = $db->prepare_query( $s, $bid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d;
}

function get_guest_feedback_invitation_data()
{
    global $db;

    $d = array();

    $s = 'SELECT
            a.bid,
            a.btype,
            d.bdtype,
            a.bbname,
            a.bbemail
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS d ON a.bid = d.bid
          WHERE a.agid IS NULL AND TIMESTAMPDIFF( DAY, d.bddate, DATE( CONVERT_TZ( NOW(),@@session.time_zone,"+08:00" ) ) ) = %d AND a.bstt <> %s
          ORDER BY d.bdid ASC';
    $q = $db->prepare_query( $s, 3, 'ar' );
    $r = $db->do_query( $q );

    while( $dt = $db->fetch_array( $r ) )
    {
        $d[] = $dt;
    }

    return $d;
}

/*
| -------------------------------------------------------------------------------------
| Get Guest Feedback invitation, for cronjob check-guest-feedback-invitation
| -------------------------------------------------------------------------------------
*/
function get_guest_feedback_invitation_data_by_feedback_time()
{
    global $db;

    $d = array();
    $time = get_meta_data( 'feedback_time' );
    $s = 'SELECT
            a.bid,
            a.btype,
            d.bdtype,
            a.bbname,
            a.bbemail
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS d ON a.bid = d.bid
          WHERE a.agid IS NULL AND ( a.bstatus = "pa" OR a.bstatus = "ca" ) AND TIMESTAMPDIFF( DAY, d.bddate, DATE( CONVERT_TZ( NOW(),@@session.time_zone,"+08:00" ) ) ) = %d AND a.bstt <> %s
          ORDER BY d.bdid ASC';
    $q = $db->prepare_query( $s, $time, 'ar' );
    $r = $db->do_query( $q );

    while( $dt = $db->fetch_array( $r ) )
    {
        $d[] = $dt;
    }

    return $d;
}
/*
| -------------------------------------------------------------------------------------
| Send Email Feedback - Cron Job
| -------------------------------------------------------------------------------------
*/
function send_feedback_email_to_customer( $bid, $bbname, $bbemail )
{
    try
    {
        $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

        $mail = new Mandrill( $keys );

        $email_from = get_meta_data( 'smtp_email_address' );
        $email_data = get_content_feedback_to_customer( $bid, $bbname );
        $email_inv  = get_meta_data( 'email_feedback_invotation' );
        $email_cc   = strpos( $email_inv, ';' ) ? explode( ';',  $email_inv ) : array( $email_inv );
        $send_to    = array();

        $send_to[] = array(
            'email' => $bbemail,
            'name'  => $bbname,
            'type'  => 'to'
        );

        foreach ( $email_cc as $em )
        {
            $send_to[] = array(
                'email' => $em,
                'name'  => $bbname,
                'type'  => 'cc'
            );
        }

        $message  = array(
            'subject'    => 'BlueWater Express - Customer Feedback',
            'from_name'  => get_meta_data( 'web_title' ),
            'from_email' => $email_from,
            'html'       => $email_data,
            'to'         => $send_to,
            'headers'    => array( 'Reply-To' => $email_from )
        );

        $async   = false;
        $result  = $mail->messages->send( $message, $async );

        if( isset( $result[0]['status'] ) )
        {
            if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
            {
                return 'invalid >> ' . $result[0]['status'];
            }
            else
            {
                return 'success';
            }
        }
        else
        {
            return 'error >> ' . $result[0]['status'];
        }
    }
    catch( Mandrill_Error $e )
    {
        return 'mandrill_error >> ' . $e . '<br><br>';
    }
}

/*
| -------------------------------------------------------------------------------------
| Send Email Feedback - Get Email Content
| -------------------------------------------------------------------------------------
*/
function get_content_feedback_to_customer( $bid, $bbname )
{
    $id  = rand( 1, 1000000 );
    $opt = get_meta_data( 'feedback_link_opt' );
    
    set_template( PLUGINS_PATH . '/ticket/tpl/email/feedback-notification-to-client.html', 'client-' . $id . '-email' );
    
    add_block( 'email-block', 'fe-' . $id . '-block', 'client-' . $id . '-email' );

    $email_data['customer_name'] = $bbname;
    $email_data['web_name']      = get_meta_data( 'web_name' );

    if( $opt == '0' )
    {
        $email_data['feedback_link'] = HTSERVER . site_url() . '/feedback-reservation/?id=' . $bid;
    }
    else
    {
        $email_data['feedback_link'] = get_meta_data( 'feedback_link' );
    }

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'email_content', return_email_setting( 'feedback_email_content', $email_data ) );

    parse_template( 'email-block', 'fe-' . $id . '-block' );

    return return_template( 'client-' . $id . '-email' );
}

/*
| -------------------------------------------------------------------------------------
| Save Guest Feedback
| -------------------------------------------------------------------------------------
*/
function save_feedback( $post )
{
    global $db;

    if( empty( $post ) )
    {
        return false;
    }
    else
    {
        $data = get_feedback_by_booking_id( $post['bid'] );

        if( empty( $data ) )
        {
            $s = 'INSERT INTO ticket_feedback(
                    bid,
                    fname,
                    femail,
                    frating,
                    fmessage,
                    fcreateddate)
                  VALUES( %d, %s, %s, %s, %s, %s )';
            $q = $db->prepare_query( $s,
                    $post['bid'],
                    $post['fname'],
                    $post['femail'],
                    $post['frating'],
                    $post['fmessage'],
                    date( 'Y-m-d H:i:s' ) );
            $r = $db->do_query( $q );

            if( !is_array( $r ) )
            {
                $id = $db->insert_id();

                sync_feedback_attachment( $id );

                return true;
            }
        }
        else
        {
            return false;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Edit Status Guest Feedback
| -------------------------------------------------------------------------------------
*/
function edit_feedback_status( $id = '', $status = 0 )
{
    global $db;

    $s = 'UPDATE ticket_feedback SET fstatus = %s WHERE fid = %d';
    $q = $db->prepare_query( $s, $status, $id );

    if( $db->do_query( $q ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Guest Feedback
| -------------------------------------------------------------------------------------
*/
function delete_feedback( $id = '' )
{
    global $db;

    $s = 'DELETE FROM ticket_feedback WHERE fid = %d';
    $q = $db->prepare_query( $s, $id );

    if( $db->do_query( $q ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function ticket_filter_guest_feedback()
{
    $filter = array( 'rating' => '', 'feedback_status' => '', 'search' => '' );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array( 'rating' => $rating, 'feedback_status' => $feedback_status, 'search' => $search  );
    }

    return $filter;
}

function get_latest_feedback_count()
{
    global $db;

    $s = 'SELECT COUNT(fid) as num FROM ticket_feedback WHERE fcreateddate >= DATE_SUB( DATE( CONVERT_TZ( NOW(),@@session.time_zone,"+08:00" ) ), INTERVAL 1 MONTH )';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d['num'];
}

function get_latest_feedback()
{
    global $db;

    $s = 'SELECT * FROM ticket_feedback ORDER BY fcreateddate DESC LIMIT %d';
    $q = $db->prepare_query( $s, 3 );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <div class="col-md-12">
            <div class="boxs">
                <p class="text-center">
                    <span class="no-data text-danger">No feedback was found</span>
                </p>
            </div>
        </div>';
    }
    else
    {
        $rows = '';

        while( $d = $db->fetch_array( $r ) )
        {
            $rows .= '
            <div class="col-md-4">
                <div class="boxs">
                    <fieldset>
                        <label>Name</label>
                        <strong>' . $d['fname'] . '</strong>
                    </fieldset>
                    <fieldset>
                        <label>Email</label>
                        <a href="mailto:' . $d['femail'] . '">' . $d['femail'] . '</a>
                    </fieldset>
                    <fieldset>
                        <label>Feedback</label>
                        <p>' . nl2br( $d['fmessage'] ) . '</p>
                    </fieldset>
                    <fieldset>
                        <label>Rating</label>
                        <div class="text">' . get_rating_star( $d['frating'] ) . '</div>
                    </fieldset>
                    <fieldset>
                        <a class="edit-feedback-status" rel="' . $d['fid'] . '">Approve/Reject &raquo;</a>
                    </fieldset>
                </div>
                <div id="edit-form-' . $d['fid'] . '" class="popup-form">
                    <fieldset class="form-group">
                        <label>Name</label>
                        <p><b>' . $d['fname'] . '</b></p>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Email</label>
                        <p><a href="mailto:' . $d['femail'] . '">' . $d['femail'] . '</a></p>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Feedback</label>
                        <p>' . nl2br( $d['fmessage'] ) . '</p>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Rating</label>
                        <p>' . get_rating_star( $d['frating'] ) . '</p>
                    </fieldset>
                    <fieldset class="form-group feedback-status">
                        <label>Status</label>
                        <select name="fstatus_' . $d['fid'] . '" autocomplete="off">
                            ' . get_status_option( $d['fstatus'] ) . '
                        </select>
                    </fieldset>
                    <fieldset class="form-action">
                        <button type="button" class="btn-save pull-left" data-ajax-link="' . HTSERVER . site_url() . '/ticket-feedback-ajax/" name="save_' . $d['fid'] . '">Save</button>
                        <a class="cancel pull-left">Cancel</a>
                    </fieldset>
                </div>
            </div>';
        }

        return $rows;
    }
}

function get_rating_star( $rate = 0 )
{
    return '
    <span class="rating">
        <label class="' . ( intval( $rate ) > 0 ? 'rating-fill' : 'rating-empty' ) . '"></label>
        <label class="' . ( intval( $rate ) > 1 ? 'rating-fill' : 'rating-empty' ) . '"></label>
        <label class="' . ( intval( $rate ) > 2 ? 'rating-fill' : 'rating-empty' ) . '"></label>
        <label class="' . ( intval( $rate ) > 3 ? 'rating-fill' : 'rating-empty' ) . '"></label>
        <label class="' . ( intval( $rate ) > 4 ? 'rating-fill' : 'rating-empty' ) . '"></label>
    </span>';
}

function get_rating_star_email( $rate = 0 )
{
    $star_fill  = HTSERVER . site_url() . '/l-plugins/ticket/images/star-fill.png';
    $star_empty = HTSERVER . site_url() . '/l-plugins/ticket/images/star-empty.png';
    return '
    <table>
        <tr>
            <td><img height="16" width="16" src="'. ( intval( $rate ) > 0 ? $star_fill : $star_empty ) .'"/></td>
            <td><img height="16" width="16" src="'. ( intval( $rate ) > 1 ? $star_fill : $star_empty ) .'"/></td>
            <td><img height="16" width="16" src="'. ( intval( $rate ) > 2 ? $star_fill : $star_empty ) .'"/></td>
            <td><img height="16" width="16" src="'. ( intval( $rate ) > 3 ? $star_fill : $star_empty ) .'"/></td>
            <td><img height="16" width="16" src="'. ( intval( $rate ) > 4 ? $star_fill : $star_empty ) .'"/></td>
        </tr>
    </table>';
}

function get_rating_option( $rate = '' )
{
    return '
    <option value="">Rating</option>
    <option value="0" ' . ( $rate == '0' ? 'selected' : '' ) . '>Zero Stars</option>
    <option value="1" ' . ( $rate == '1' ? 'selected' : '' ) . '>One Stars</option>
    <option value="2" ' . ( $rate == '2' ? 'selected' : '' ) . '>Two Stars</option>
    <option value="3" ' . ( $rate == '3' ? 'selected' : '' ) . '>Three Stars</option>
    <option value="4" ' . ( $rate == '4' ? 'selected' : '' ) . '>Four Stars</option>
    <option value="5" ' . ( $rate == '5' ? 'selected' : '' ) . '>Five Stars</option>';
}

function get_status_option( $status = 0 )
{
    return '
    <option value="">All Status</option>
    <option value="0" ' . ( $status == '0' ? 'selected' : '' ) . '>Pending</option>
    <option value="1" ' . ( $status == '1' ? 'selected' : '' ) . '>Approved</option>
    <option value="2" ' . ( $status == '2' ? 'selected' : '' ) . '>Reject</option>';
}

function ticket_feedback_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    //-- Request Ajax From Admin
    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'edit-feedback-status' )
    {
        if( is_user_logged() )
        {
            if( edit_feedback_status( $_POST['fid'], $_POST['status'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            echo json_encode( array( 'result' => 'failed', 'message' => 'You have to login to access this page!' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-feedback' )
    {
        if( is_user_logged() )
        {
            if( delete_feedback( $_POST['fid'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            echo json_encode( array( 'result' => 'failed', 'message' => 'You have to login to access this page!' ) );
        }
    }

    //-- Request Ajax From Frontend
    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-feedback-pictures' )
    {
        echo json_encode( get_feedback_attachment( $_POST['faid'] ) );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-feedback-pictures' )
    {
        echo json_encode( upload_feedback_attachment() );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-feedback-pictures' )
    {
        if( delete_feedback_attachment() )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Function | Send email after summit feedback
| -------------------------------------------------------------------------------------
*/

function get_bticket_feedback( $bid )
{
    global $db;

    $p = $db->prepare_query( 'SELECT bticket FROM ticket_booking WHERE bid=%d', $bid );
    $q = $db->do_query( $p );
    $f = $db->fetch_array( $q );

    return $f['bticket'];
}

function get_feedback_submited( $post )
{
    if( !empty( $post ) )
    {
        set_template( PLUGINS_PATH . '/ticket/tpl/email/notif-email-submited-feedback.html', 'feedback-submited' );

        add_block( 'feed-sub-block', 'fsblock', 'feedback-submited' );

        $data = ticket_booking_all_data($post['bid']);

        extract( $data );
        extract( $detail );

        add_variable( 'pickup_driver', 'N/A' );
        add_variable( 'pickup_vehicle', 'N/A' );

        add_variable( 'dropoff_driver', 'N/A' );
        add_variable( 'dropoff_vehicle', 'N/A' );

        add_variable( 'pickup_boat', 'N/A' );
        add_variable( 'dropoff_boat', 'N/A' );

        add_variable( 'return-css', 'display:none;' );

        if( isset( $departure ) )
        {
            foreach( $departure as $key => $dp_trip )
            {
                extract( $dp_trip );

                $boat = get_boat( $boid );

                add_variable( 'dep_bdto', $bdto );
                add_variable( 'dep_bdfrom', $bdfrom );
                add_variable( 'pickup_boat', $boat['boname'] != '' ? $boat['boname'] : 'N/A');

                add_variable( 'dep_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'dep_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                add_variable( 'dep_trans_detail', ticket_detail_transport_email_content( $transport, $bdfrom, $bdto, $bdate, $bddate ) );
            }
        }

        if( isset( $return ) )
        {
            add_variable( 'return-css', '' );

            foreach( $departure as $key => $dp_trip )
            {
                extract( $dp_trip );

                $boat = get_boat( $boid );

                add_variable( 'rtn_bdto', $bdto );
                add_variable( 'rtn_bdfrom', $bdfrom );
                add_variable( 'rtn_bddate', date( 'd M Y', strtotime( $bddate ) ) );
                add_variable( 'dropoff_boat', $boat['boname'] != '' ? $boat['boname'] : 'N/A' );

                add_variable( 'rtn_bddeparttime', date( 'H:i', strtotime( $bddeparttime ) ) );
                add_variable( 'rtn_bdarrivetime', date( 'H:i', strtotime( $bdarrivetime ) ) );
                add_variable( 'rtn_trans_detail', ticket_detail_transport_email_content( $transport, $bdfrom, $bdto, $bdate, $bddate ) );
            }
        }

        if( isset( $transport ) )
        {
            add_variable( 'pickuplist-css', '' );

            if( isset( $transport['pickup'] ) )
            {
                add_variable( 'pickup-css', '' );

                foreach( $transport['pickup'] as $key => $trip_pickup )
                {
                    extract( $trip_pickup );

                    $trip_detail = get_trip_transport_detail( $btid );

                    add_variable( 'pickup_driver', $trip_detail["dname"] != '' ? $trip_detail["dname"] : 'N/A' );
                    add_variable( 'pickup_vehicle', $trip_detail["vname"] != '' ? $trip_detail["vname"] : 'N/A' );
                }
            }

            if( isset( $transport['drop-off'] ) )
            {
                add_variable( 'dropoff-css', '' );

                foreach( $transport['drop-off'] as $key => $trip_drop )
                {
                    extract( $trip_drop );

                    $trip_detail = get_trip_transport_detail( $btid );

                    add_variable( 'dropoff_driver', $trip_detail["dname"] != '' ? $trip_detail["dname"] : 'N/A' );
                    add_variable( 'dropoff_vehicle', $trip_detail["vname"] != '' ? $trip_detail["vname"] : 'N/A');
                }
            }
        }

        add_variable( 'fname', $post['fname'] );
        add_variable( 'femail', $post['femail'] );
        add_variable( 'fmessage', $post['fmessage'] );

        add_variable( 'img_url', get_theme_img() );
        add_variable( 'fticket', get_bticket_feedback( $post['bid'] ) );
        add_variable( 'frating', get_rating_star_email( $post['frating'] ) );

        parse_template( 'feed-sub-block', 'fsblock' );

        return return_template( 'feedback-submited' );
    }
}

function send_email_feedback_submit( $post )
{
    if( empty( $post ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_from = get_meta_data( 'smtp_email_address' );
            $email_data = get_feedback_submited( $post );
            $email_sub  = get_meta_data( 'email_feedback_submited' );
            $email_to   = strpos( $email_sub, ';' ) ? explode( ';',  $email_sub ) : array( $email_sub );
            $send_to    = array();

            foreach( $email_to as $emto )
            {
                $send_to[] = array(
                    'email' => $emto,
                    'name'  => '',
                    'type'  => 'to'
                );
            }

            $message  = array(
                'subject'    => 'Guest Feedback #'.$post['fticket'] . ' ' . $post['fname'],
                'from_name'  => get_meta_data( 'web_title' ),
                'from_email' => $email_from,
                'html'       => $email_data,
                'to'         => $send_to,
                'headers'    => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Function | AJAX feedback pictures
| -------------------------------------------------------------------------------------
*/

function get_feedback_attachment( $fid )
{
    global $db;

    if( isset( $_POST['att_id'] ) )
    {
        $s = 'SELECT * FROM ticket_feedback_attachment WHERE faid = %d';
        $q = $db->prepare_query( $s, $fid );
        $r = $db->do_query( $q );

        if ( $db->num_rows( $r ) > 0 )
        {
            $pic = array();

            while ( $d = $db->fetch_array( $r ) )
            {
                $pic[] = array(
                    'faid'          => $d['faid'],
                    'fid'           => $d['fid'],
                    'faloc'         => $d['faloc'],
                    'faloc_thumb'   => $d['faloc_thumb'],
                    'faloc_medium'  => $d['faloc_medium'],
                    'faloc_large'   => $d['faloc_large'],
                    'famime_type'   => $d['famime_type']
                );
            }

            return array( 'result' => 'success', 'pic' => $pic );
        }
        else
        {
            return array( 'result' => 'failed' );
        }
    }
    else
    {
         return array( 'result'=>'failed' );
    }
}

function upload_feedback_attachment()
{
    if ( isset( $_FILES['file'] ) && $_FILES['file']['error'] == 0 )
    {
        $file_name   = $_FILES['file']['name'];
        $file_size   = $_FILES['file']['size'];
        $file_type   = $_FILES['file']['type'];
        $file_source = $_FILES['file']['tmp_name'];

        if ( is_allow_file_size( $file_size ) )
        {
            if ( is_allow_file_type( $file_type, 'image' ) )
            {
                $file_ext    = file_name_filter( $file_name, true );
                $sef_url     = file_name_filter( $file_name ) . '-'. time();
                $file_name_n = $sef_url . $file_ext;
                $file_name_m = $sef_url . '-medium' . $file_ext;
                $file_name_l = $sef_url . '-large' . $file_ext;
                $file_name_t = $sef_url . '-thumb' . $file_ext;

                if( !file_exists( PLUGINS_PATH . '/ticket/uploads/feedback/' ) )
                {
                    mkdir( PLUGINS_PATH . '/ticket/uploads/feedback/');
                }

                $dest   = PLUGINS_PATH . '/ticket/uploads/feedback/' . $file_name_n;
                $dest_m = PLUGINS_PATH . '/ticket/uploads/feedback/' . $file_name_m;
                $dest_l = PLUGINS_PATH . '/ticket/uploads/feedback/' . $file_name_l;
                $dest_t = PLUGINS_PATH . '/ticket/uploads/feedback/' . $file_name_t;

                $dest_save   = '/l-plugins/ticket/uploads/feedback/' . $file_name_n;
                $dest_save_m = '/l-plugins/ticket/uploads/feedback/' . $file_name_m;
                $dest_save_l = '/l-plugins/ticket/uploads/feedback/' . $file_name_l;
                $dest_save_t = '/l-plugins/ticket/uploads/feedback/' . $file_name_t;

                upload_crop( $file_source, $dest_t, $file_type, 300, 300 );
                upload_resize( $file_source, $dest_m, $file_type, 700, 700 );
                upload_resize( $file_source, $dest_l, $file_type, 1024, 1024 );
                upload( $file_source, $dest );

                $attach_id = save_feedback_attachment( $dest_save, $dest_save_t, $dest_save_m, $dest_save_l, $file_type );

                if( $attach_id )
                {
                    return array( 'result'=>'success', 'id'=>$attach_id, 'thumb'=>'http//' . site_url() . $dest_save_t );
                }
                else
                {
                    return array( 'result'=>'failed', 'message'=>'Something wrong' );
                }
            }
            else
            {
                return array( 'result' => 'failed', 'message' => 'File type must be image (jpg, png, gif)' );
            }
        }
        else
        {
            return array( 'result' => 'failed', 'message' => 'The maximum file size is 2MB' );
        }
    }
    else
    {
        return array( 'result' => 'failed', 'message' => 'Image file is empty' );
    }
}

function save_feedback_attachment( $loc='', $loc_thumb='', $loc_medium='', $loc_large='', $mime_type='' )
{
    global $db;

    $s = 'INSERT INTO ticket_feedback_attachment( faloc,faloc_thumb,faloc_medium,faloc_large,famime_type ) VALUES( %s, %s ,%s, %s, %s )';
    $q = $db->prepare_query( $s, $loc, $loc_thumb, $loc_medium, $loc_large, $mime_type );
    $r = $db->do_query( $q );

    if ( !is_array( $r ) )
    {
        return $db->insert_id();
    }
}

function delete_feedback_attachment( $faid = '' )
{
    global $db;

    $faid = isset( $_POST['att_id'] ) ? $_POST['att_id'] : $faid;

    if( !empty( $faid ) )
    {
        $attachment = get_feedback_attachment( $faid );

        if( empty( $attachment ) )
        {
            return false;
        }
        else
        {
            $s = 'DELETE FROM ticket_feedback_attachment WHERE faid = %d';
            $q = $db->prepare_query( $s, $faid );

            if( $db->do_query( $q ) )
            {
                if( file_exists( ROOT_PATH . $attachment['pic'][0]['faloc_thumb'] ) )
                {
                    unlink( ROOT_PATH . $attachment['pic'][0]['faloc_thumb'] );
                }

                if( file_exists( ROOT_PATH . $attachment['pic'][0]['faloc_medium'] ) )
                {
                    unlink( ROOT_PATH . $attachment['pic'][0]['faloc_medium'] );
                }

                if( file_exists( ROOT_PATH . $attachment['pic'][0]['faloc_large'] ) )
                {
                    unlink( ROOT_PATH . $attachment['pic'][0]['faloc_large'] );
                }

                if( file_exists( ROOT_PATH . $attachment['pic'][0]['faloc'] ) )
                {
                    unlink( ROOT_PATH . $attachment['pic'][0]['faloc'] );
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }
}

function sync_feedback_attachment( $fid )
{
    global $db;

    if( isset( $_POST['feedbackattachment'] ) )
    {
        foreach( $_POST['feedbackattachment'] as $faid )
        {
            $s = 'UPDATE ticket_feedback_attachment SET fid = %d WHERE faid = %d';
            $q = $db->prepare_query( $s, $fid, $faid );
            $r = $db->do_query( $q );
        }
    }
}

?>