<?php
/*
 * ____________________________________________________________
 *
 * Copyright (C) 2016 NICE IT&T
 *
 *
 * This config file may used as it is, there is no warranty.
 *
 * @ description : PHP SSL Client module.
 * @ name        : NicepayLite.php
 * @ author      : NICEPAY I&T (tech@nicepay.co.kr)
 * @ date        :
 * @ modify      : 09.03.2016
 *
 * 09.03.2016 Update Log
 *
 * ____________________________________________________________
 */

// Please set the following
$nicepay_imid          = get_meta_data( 'nicepay_imid', 'ticket_setting' );
$nicepay_merchant_key  = get_meta_data( 'nicepay_merchant_key', 'ticket_setting' );
$nicepay_callback_url  = get_meta_data( 'nicepay_callback_url', 'ticket_setting' );
$nicepay_dbprocess_url = get_meta_data( 'nicepay_dbprocess_url', 'ticket_setting' );

define( 'NICEPAY_IMID', $nicepay_imid ); // Merchant ID
define( 'NICEPAY_MERCHANT_KEY', $nicepay_merchant_key ); // API Key

define( 'NICEPAY_CALLBACK_URL', $nicepay_callback_url ); // Merchant's result page URL
define( 'NICEPAY_DBPROCESS_URL', $nicepay_dbprocess_url ); // Merchant's notification handler URL

/* TIMEOUT - Define as needed (in seconds) */
define( 'NICEPAY_TIMEOUT_CONNECT', 15 );
define( 'NICEPAY_TIMEOUT_READ', 25 );

define( 'NICEPAY_PROGRAM', 'NicepayLite' );
define( 'NICEPAY_VERSION', '1.11' );
define( 'NICEPAY_BUILDDATE', '20160309' );
define( 'NICEPAY_REQ_CC_URL', 'https://www.nicepay.co.id/nicepay/direct/v2/registration' ); // Credit Card API URL
define( 'NICEPAY_REQ_VA_URL', 'https://www.nicepay.co.id/nicepay/direct/v2/registration' ); // Request Virtual Account API URL
define( 'NICEPAY_CANCEL_VA_URL', 'https://www.nicepay.co.id/nicepay/direct/v2/cancel' ); // Cancel Virtual Account API URL
define( 'NICEPAY_ORDER_STATUS_URL', 'https://www.nicepay.co.id/nicepay/direct/v2/inquiry' ); // Check payment status URL

define( 'NICEPAY_READ_TIMEOUT_ERR', '10200' );

/* LOG LEVEL */
define( 'NICEPAY_LOG_CRITICAL', 1 );
define( 'NICEPAY_LOG_ERROR', 2 );
define( 'NICEPAY_LOG_NOTICE', 3 );
define( 'NICEPAY_LOG_INFO', 5 );
define( 'NICEPAY_LOG_DEBUG', 7 );

?>
