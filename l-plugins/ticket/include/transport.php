<?php

add_actions( 'pickup-guest', 'ticket_pickup_guest' );
add_actions( 'drop-off-guest', 'ticket_drop_off_guest' );
add_actions( 'pickup-list', 'ticket_pickup_list' );
add_actions( 'drop-off-list', 'ticket_drop_off_list' );
add_actions( 'ticket-transport-ajax_page', 'ticket_transport_ajax' );

/*
| -------------------------------------------------------------------------------------
| Pickup Guest
| -------------------------------------------------------------------------------------
*/
function ticket_pickup_guest()
{
    if( isset( $_POST['print'] ) )
    {
        header( 'Location:' . HTSERVER . site_url() . '/pickup-guest-print-report/?state=transport&prm=' . base64_encode( json_encode( $_POST ) ) );
    }
    else
    {
        if( is_add_trip_transport() )
        {
            return ticket_process_trip_transport( 'pickup' );
        }
        elseif( is_add_new() )
        {
            return ticket_add_trip_transport( 'pickup' );
        }

        return ticket_pickup_guest_table_data();
    }
}

/*
| -------------------------------------------------------------------------------------
| Pickup Guest Table
| -------------------------------------------------------------------------------------
*/
function ticket_pickup_guest_table_data()
{
    $filters  = get_filter_guest();
    $site_url = site_url();

    extract( $filters );

    set_template( PLUGINS_PATH . '/ticket/tpl/transport/guest-list.html', 'transport' );

    add_block( 'more-block', 'mblock', 'transport' );
    add_block( 'list-block', 'lblock', 'transport' );

    add_variable( 'etime', 'ETD' );
    add_variable( 'date', $bddate );
    add_variable( 'type', 'Pickup' );
    add_variable( 'tran_type', 'pickup' );
    add_variable( 'title', 'Pickup Guest' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_message_block() );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'trans_type_option', get_transport_type_option( $trans_type ) );
    add_variable( 'area_option', get_transport_area_option( $taid, true, 'All Area' ) );
    add_variable( 'location_option', get_location_option( $lcid, true, 'All Departure Point') );

    add_variable( 'action', get_state_url( 'transport&sub=pickup-guest' ) );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-transport-ajax/' );

    add_actions( 'section_title', 'Pickup Guest List' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'list-block', 'lblock', false );

    return return_template( 'transport' );
}

/*
| -------------------------------------------------------------------------------------
| Pickup Guest Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_pickup_guest_table_query( $taid = '', $trans_type = '', $bddate = '', $rid = '', $lcid = '', $bstatus = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1 => 'a.bbname',
        3 => 'IFNULL( d.taname, "No Accommodation" )',
        6 => 'b.bddeparttime',
        7 => 'b.bddate',
        8 => 'b.bdfrom',
        9 => 'c.bttrans_type'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.bid ASC';
    }

    if( empty( $rdata['search']['value'] ) )
    {
        $w = '';

        if( !empty( $bstatus ) )
        {
            if( is_array( $bstatus ) )
            {
                $estatus = end( $bstatus );

                $w .= ' AND ( ';

                foreach( $bstatus as $st )
                {
                    if( $st == $estatus )
                    {
                        $w .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                    }
                    else
                    {
                        $w .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                    }
                }

                $w .= ' )';
            }
            else
            {
                if( $bstatus == 'pp' )
                {
                    $w .= $db->prepare_query( ' AND b.bdpstatus IN( %s )', 'pp' );
                }
                else
                {
                    $w .= $db->prepare_query( ' AND b.bdpstatus = %s', $bstatus );
                }
            }
        }
        else
        {
            $w .= $db->prepare_query( ' AND b.bdpstatus IN( %s, %s, %s )', 'pa', 'ca', 'pp' );
        }

        if( $taid != '' )
        {
            $w .= $db->prepare_query( ' AND c.taid = %d', $taid );
        }

        if( $rid != '' )
        {
            $w .= $db->prepare_query( ' AND b.rid = %d', $rid );
        }

        if( $lcid != '' )
        {
            $w .= $db->prepare_query( ' AND b.bdfrom_id = %d', $lcid );
        }

        if( $trans_type != '' )
        {
            $w .= $db->prepare_query( ' AND c.bttrans_type = %s', $trans_type );
        }

        if( $bddate != '' )
        {
            $w .= $db->prepare_query( ' AND b.bddate = %s', date( 'Y-m-d', strtotime( $bddate ) ) );
        }

        $s = 'SELECT
                c.hid,
                a.bid,
                a.agid,
                a.bbname,
                b.bdid,
                b.num_adult,
                b.num_child,
                b.num_infant,
                c.bttrans_type,
                b.bddeparttime,
                b.bdfrom,
                b.bddate,
                c.btid,
                c.bttype,
                c.bthotelname,
                c.bthoteladdress,
                IFNULL( 
                    d.taname, "No Accommodation" 
                ) AS taname
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
              LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
              WHERE c.btid NOT IN(
                SELECT btid
                FROM ticket_trip_transport_detail
                WHERE ttdtype = "pickup"
              ) AND c.bttype = "pickup"
              AND c.bttrans_type <> "2"
              AND b.bdrevstatus = "Devinitive"
              AND a.bstt <> "ar"
              ' . $w . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                c.hid,
                a.bid,
                a.agid,
                a.bbname,
                b.bdid,
                b.num_adult,
                b.num_child,
                b.num_infant,
                c.bttrans_type,
                b.bddeparttime,
                b.bdfrom,
                b.bddate,
                c.btid,
                c.bttype,
                c.bthotelname,
                c.bthoteladdress,
                IFNULL( 
                    d.taname, "No Accommodation" 
                ) AS taname
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
              LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
              WHERE c.btid NOT IN(
                SELECT btid
                FROM ticket_trip_transport_detail
                WHERE ttdtype = "pickup"
              ) AND b.bdpstatus IN ( "pa", "ca", "pp" )
              AND c.bttype = "pickup"
              AND c.bttrans_type <> "2"
              AND b.bdrevstatus = "Devinitive"
              AND a.bstt <> "ar"
              AND ( ' . implode( ' OR ', $search ) . ' )
              ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $guest = array();

            if( !empty( $d2['num_adult'] ) )
            {
                $guest[] = $d2['num_adult'] . ( count( $d2['num_adult'] ) > 1 ? ' adults' : ' adult' );
            }

            if( !empty( $d2['num_child'] ) )
            {
                $guest[] = $d2['num_child'] . ( count( $d2['num_child'] ) > 1 ? ' childs' : ' child' );
            }

            if( !empty( $d2['num_infant'] ) )
            {
                $guest[] = $d2['num_infant'] . ( count( $d2['num_infant'] ) > 1 ? ' infants' : ' infant' );
            }

            if( empty( $d2['bthotelname'] ) && empty( $d2['bthoteladdress'] ) )
            {
                if( is_null( $d2['hid'] ) )
                {
                    $hotel_detail = 'No hotel';
                }
                else
                {
                    $h = get_hotel( $d2['hid'] );

                    $hotel_detail = $h['hname'] . '<br />' . nl2br( $h['haddress'] );
                }
            }
            elseif( !empty( $d2['bthotelname'] ) && !empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = $d2['bthotelname'] . '<br />' . nl2br( $d2['bthoteladdress'] );
            }
            elseif( !empty( $d2['bthotelname'] ) && empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = $d2['bthotelname'];
            }
            elseif( empty( $d2['bthotelname'] ) && !empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = nl2br( $d2['bthoteladdress'] );
            }

            $data[] = array(
                'id'           => $d2['bid'],
                'bdid'         => $d2['bdid'],
                'btid'         => $d2['btid'],
                'from'         => $d2['bdfrom'],
                'hotel_detail' => $hotel_detail,
                'dept_date'    => date( 'd M Y', strtotime( $d2['bddate'] ) ),
                'dept_time'    => date( 'H:i', strtotime( $d2['bddeparttime'] ) ),
                'guest_num'    => empty( $guest ) ? '-' : implode( ', ', $guest ),
                'pickup'       => is_null ( $d2['taname'] ) ? 'No accommodation' : $d2['taname'],
                'booked_by'    => empty( $d2['agid'] ) ? $d2['bbname'] : get_agent( $d2['agid'], 'agname' ),
                'guest_name'   => ticket_passenger_list_name( ticket_passenger_list( $d2['bdid'] ), true ),
                'trans_type'   => $d2['bttrans_type'] == '0' ? 'Shared' : (  $d2['bttrans_type'] == '1' ? 'Private' : 'Own Transport' )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Pickup More Guest Table
| -------------------------------------------------------------------------------------
*/
function ticket_pickup_more_guest_table_data()
{
    $site_url = site_url();
    $data     = get_trip_transport( $_POST['ttid'], 'pickup' );

    if( !empty( $data ) )
    {
        set_template( PLUGINS_PATH . '/ticket/tpl/transport/guest-list.html', 'transport' );

        add_block( 'more-block', 'mblock', 'transport' );
        add_block( 'list-block', 'lblock', 'transport' );

        extract( $data );

        $tttime = date( 'H:i', strtotime( $tttime ) );
        $ttdate = date( 'd M Y', strtotime( $ttdate ) );

        add_variable( 'ttid', $ttid );
        add_variable( 'date', $ttdate );
        add_variable( 'time', $tttime );
        add_variable( 'location', $ttlocation );
        add_variable( 'transport_type', $tttype );
        add_variable( 'capacity', get_vehicle_capacity( $vid ) );
        add_variable( 'trans_type', $tttype == 'Shared' ? '0' : '1' );
        add_variable( 'fee_format', number_format( $ttfee, 0, '', '.' ) );
        add_variable( 'status', $ttstatus == '0' ? 'Open' : 'Delivered' );
        add_variable( 'passenger', get_trip_transport_passenger( $type, $ttlocation, $vid, $ttdate, $tttime ) );

        add_variable( 'driver', $dname );
        add_variable( 'vehicle', $vname );

        add_variable( 'rname', $rname );
        add_variable( 'boname', $boname );

        add_variable( 'etime', 'ETD' );
        add_variable( 'type', 'Pickup' );
        add_variable( 'tran_type', 'pickup' );
        add_variable( 'loc_type', 'Departure' );
        add_variable( 'title', 'More Guest To Pickup' );

        add_variable( 'limit', post_viewed() );
        add_variable( 'action', get_state_url( 'transport-list&sub=pickup-list' ) );
        add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-transport-ajax/' );
        add_variable( 'cancel_link', get_state_url( 'transport-list&sub=pickup-list&prc=edit&show=false&id=' . $ttid ) );

        add_actions( 'section_title', 'Add More Guest To Pickup List' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        parse_template( 'more-block', 'mblock', false );

        return return_template( 'transport' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Pickup More Guest Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_pickup_more_guest_table_query( $ttid = '', $trans_type = '', $date = '', $time = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1 => 'a.bbname',
        3 => 'IFNULL( d.taname, "No Accommodation" )',
        6 => 'b.bddeparttime',
        7 => 'b.bddate',
        8 => 'b.bdfrom',
        9 => 'c.bttrans_type'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.bid ASC';
    }

    if( empty( $rdata['search']['value'] ) )
    {
        $s = 'SELECT
                c.hid,
                a.bid,
                a.agid,
                a.bbname,
                b.bdid,
                b.num_adult,
                b.num_child,
                b.num_infant,
                c.bttrans_type,
                b.bddeparttime,
                b.bdfrom,
                b.bddate,
                c.btid,
                c.bttype,
                c.bthotelname,
                c.bthoteladdress,
                IFNULL( 
                    d.taname, "No Accommodation" 
                ) AS taname
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
              LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
              WHERE c.btid NOT IN(
                SELECT btid
                FROM ticket_trip_transport_detail
                WHERE ttdtype = "pickup"
              ) AND b.bdpstatus IN ( "pa", "ca", "pp" )
              AND c.bttype = "pickup"
              AND b.bdrevstatus = "Devinitive"
              AND c.bttrans_type = "' . $trans_type . '"
              AND a.bstt <> "ar"
              AND b.bddate = "' . date( 'Y-m-d', strtotime( $date ) ) . '"
              AND b.bddeparttime = "' . date( 'H:i:s', strtotime( $time ) ) . '"
              ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                c.hid,
                a.bid,
                a.agid,
                a.bbname,
                b.bdid,
                b.num_adult,
                b.num_child,
                b.num_infant,
                c.bttrans_type,
                b.bddeparttime,
                b.bdfrom,
                b.bddate,
                c.btid,
                c.bttype,
                c.bthotelname,
                c.bthoteladdress,
                IFNULL( 
                    d.taname, "No Accommodation" 
                ) AS taname
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
              LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
              WHERE c.btid NOT IN(
                SELECT btid
                FROM ticket_trip_transport_detail
                WHERE ttdtype = "pickup"
              ) AND b.bdpstatus IN ( "pa", "ca", "pp" )
              AND c.bttype = "pickup"
              AND b.bdrevstatus = "Devinitive"
              AND c.bttrans_type = "' . $trans_type . '"
              AND a.bstt <> "ar"
              AND b.bddate = "' . date( 'H:i:s', strtotime( $date ) ) . '"
              AND b.bddeparttime = "' . date( 'H:i:s', strtotime( $time ) ) . '"
              AND ( ' . implode( ' OR ', $search ) . ' ) ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $guest = array();

            if( !empty( $d2['num_adult'] ) )
            {
                $guest[] = $d2['num_adult'] . ( count( $d2['num_adult'] ) > 1 ? ' adults' : ' adult' );
            }

            if( !empty( $d2['num_child'] ) )
            {
                $guest[] = $d2['num_child'] . ( count( $d2['num_child'] ) > 1 ? ' childs' : ' child' );
            }

            if( !empty( $d2['num_infant'] ) )
            {
                $guest[] = $d2['num_infant'] . ( count( $d2['num_infant'] ) > 1 ? ' infants' : ' infant' );
            }

            if( empty( $d2['bthotelname'] ) && empty( $d2['bthoteladdress'] ) )
            {
                if( is_null( $d2['hid'] ) )
                {
                    $hotel_detail = 'No hotel';
                }
                else
                {
                    $h = get_hotel( $d2['hid'] );

                    $hotel_detail = $h['hname'] . '<br />' . nl2br( $h['haddress'] );
                }
            }
            elseif( !empty( $d2['bthotelname'] ) && !empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = $d2['bthotelname'] . '<br />' . nl2br( $d2['bthoteladdress'] );
            }
            elseif( !empty( $d2['bthotelname'] ) && empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = $d2['bthotelname'];
            }
            elseif( empty( $d2['bthotelname'] ) && !empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = nl2br( $d2['bthoteladdress'] );
            }

            $data[] = array(
                'id'           => $d2['bid'],
                'bdid'         => $d2['bdid'],
                'btid'         => $d2['btid'],
                'from'         => $d2['bdfrom'],
                'hotel_detail' => $hotel_detail,
                'dept_date'    => date( 'd M Y', strtotime( $d2['bddate'] ) ),
                'dept_time'    => date( 'H:i', strtotime( $d2['bddeparttime'] ) ),
                'guest_num'    => empty( $guest ) ? '-' : implode( ', ', $guest ),
                'pickup'       => is_null ( $d2['taname'] ) ? 'No accommodation' : $d2['taname'],
                'booked_by'    => empty( $d2['agid'] ) ? $d2['bbname'] : get_agent( $d2['agid'], 'agname' ),
                'guest_name'   => ticket_passenger_list_name( ticket_passenger_list( $d2['bdid'] ), true ),
                'trans_type'   => $d2['bttrans_type'] == '0' ? 'Shared' : (  $d2['bttrans_type'] == '1' ? 'Private' : 'Own Transport' )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Drop Off Guest
| -------------------------------------------------------------------------------------
*/
function ticket_drop_off_guest()
{
    if( isset( $_POST['print'] ) )
    {
        header( 'Location:' . HTSERVER . site_url() . '/dropoff-guest-print-report/?state=transport&prm=' . base64_encode( json_encode( $_POST ) ) );
    }
    else
    {
        if( is_add_trip_transport() )
        {
            return ticket_process_trip_transport( 'drop-off' );
        }
        elseif( is_add_new() )
        {
            return ticket_add_trip_transport( 'drop-off' );
        }

        return ticket_drop_off_guest_table_data();
    }
}

/*
| -------------------------------------------------------------------------------------
| Drop Off Table
| -------------------------------------------------------------------------------------
*/
function ticket_drop_off_guest_table_data()
{
    $filters  = get_filter_guest();
    $site_url = site_url();

    extract( $filters );

    set_template( PLUGINS_PATH . '/ticket/tpl/transport/guest-list.html', 'transport' );

    add_block( 'more-block', 'mblock', 'transport' );
    add_block( 'list-block', 'lblock', 'transport' );

    add_variable( 'etime', 'ETA' );
    add_variable( 'date', $bddate );
    add_variable( 'search', $search);
    add_variable( 'type', 'Drop-off' );
    add_variable( 'tran_type', 'drop-off' );
    add_variable( 'title', 'Drop-off Guest' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_message_block() );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'trans_type_option', get_transport_type_option( $trans_type ) );
    add_variable( 'area_option', get_transport_area_option( $taid, true, 'All Area' ) );
    add_variable( 'location_option', get_location_option( $lcid, true, 'All Arrival Point') );

    add_variable( 'action', get_state_url( 'transport&sub=drop-off-guest' ) );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-transport-ajax/' );

    add_actions( 'section_title', 'Drop-off Guest List' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'list-block', 'lblock', false );

    return return_template( 'transport' );
}

/*
| -------------------------------------------------------------------------------------
| Drop-off Guest Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_drop_off_guest_table_query( $taid = '', $trans_type = '', $bddate = '', $rid = '', $lcid = '', $bstatus = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1 => 'a.bbname',
        3 => 'IFNULL( d.taname, "No Accommodation" )',
        6 => 'b.bddeparttime',
        7 => 'b.bddate',
        8 => 'b.bdfrom',
        9 => 'c.bttrans_type'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.bid ASC';
    }

    if( empty( $rdata['search']['value'] ) )
    {
        $w = '';

        if( !empty( $bstatus ) )
        {
            if( is_array( $bstatus ) )
            {
                $estatus = end( $bstatus );

                $w .= ' AND ( ';

                foreach( $bstatus as $st )
                {
                    if( $st == $estatus )
                    {
                        $w .= $db->prepare_query( 'b.bdpstatus = %s', $st );
                    }
                    else
                    {
                        $w .= $db->prepare_query( 'b.bdpstatus = %s OR ', $st );
                    }
                }

                $w .= ' )';
            }
            else
            {
                if( $bstatus == 'pp' )
                {
                    $w .= $db->prepare_query( ' AND b.bdpstatus IN( %s )', 'pp' );
                }
                else
                {
                    $w .= $db->prepare_query( ' AND b.bdpstatus = %s', $bstatus );
                }
            }
        }
        else
        {
            $w .= $db->prepare_query( ' AND b.bdpstatus IN( %s, %s, %s )', 'pa', 'ca', 'pp' );
        }

        if( $taid != '' )
        {
            $w .= $db->prepare_query( ' AND c.taid = %d', $taid );
        }

        if( $rid != '' )
        {
            $w .= $db->prepare_query( ' AND b.rid = %d', $rid );
        }

        if( $lcid != '' )
        {
            $w .= $db->prepare_query( ' AND b.bdto_id = %d', $lcid );
        }

        if( $trans_type != '' )
        {
            $w .= $db->prepare_query( ' AND c.bttrans_type = %s', $trans_type );
        }

        if( $bddate != '' )
        {
            $w .= $db->prepare_query( ' AND b.bddate = %s', date( 'Y-m-d', strtotime( $bddate ) ) );
        }

        $s = 'SELECT
                c.hid,
                a.bid,
                a.agid,
                a.bbname,
                b.bdid,
                b.num_adult,
                b.num_child,
                b.num_infant,
                c.bttrans_type,
                b.bddeparttime,
                b.bdarrivetime,
                b.bdto,
                b.bddate,
                c.btid,
                c.bttype,
                c.bthotelname,
                c.bthoteladdress,
                IFNULL( 
                    d.taname, "No Accommodation" 
                ) AS taname
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
              LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
              WHERE c.btid NOT IN(
                SELECT btid
                FROM ticket_trip_transport_detail
                WHERE ttdtype = "drop-off"
              ) AND c.bttype = "drop-off"
              AND c.bttrans_type <> "2"
              AND b.bdrevstatus = "Devinitive"
              AND a.bstt <> "ar"
              ' . $w . ' ORDER BY c.bttrans_type desc , d.taname desc,   ' . $order ;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                c.hid,
                a.bid,
                a.agid,
                a.bbname,
                b.bdid,
                b.num_adult,
                b.num_child,
                b.num_infant,
                c.bttrans_type,
                b.bddeparttime,
                b.bdarrivetime,
                b.bdto,
                b.bddate,
                c.btid,
                c.bttype,
                c.bthotelname,
                c.bthoteladdress,
                IFNULL( 
                    d.taname, "No Accommodation" 
                ) AS taname
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
              LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
              WHERE c.btid NOT IN(
                SELECT btid
                FROM ticket_trip_transport_detail
                WHERE ttdtype = "drop-off"
              ) AND b.bdpstatus IN ( "pa", "ca", "pp" )
              AND c.bttype = "drop-off"
              AND c.bttrans_type <> "2"
              AND b.bdrevstatus = "Devinitive"
              AND a.bstt <> "ar"
              AND ( ' . implode( ' OR ', $search ) . ' )
              ORDER BY c.bttrans_type desc , d.taname desc,  ' . $order ;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $guest = array();

            if( !empty( $d2['num_adult'] ) )
            {
                $guest[] = $d2['num_adult'] . ( count( $d2['num_adult'] ) > 1 ? ' adults' : ' adult' );
            }

            if( !empty( $d2['num_child'] ) )
            {
                $guest[] = $d2['num_child'] . ( count( $d2['num_child'] ) > 1 ? ' childs' : ' child' );
            }

            if( !empty( $d2['num_infant'] ) )
            {
                $guest[] = $d2['num_infant'] . ( count( $d2['num_infant'] ) > 1 ? ' infants' : ' infant' );
            }

            if( empty( $d2['bthotelname'] ) && empty( $d2['bthoteladdress'] ) )
            {
                if( is_null( $d2['hid'] ) )
                {
                    $hotel_detail = 'No hotel';
                }
                else
                {
                    $h = get_hotel( $d2['hid'] );

                    $hotel_detail = $h['hname'] . '<br />' . nl2br( $h['haddress'] );
                }
            }
            elseif( !empty( $d2['bthotelname'] ) && !empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = $d2['bthotelname'] . '<br />' . nl2br( $d2['bthoteladdress'] );
            }
            elseif( !empty( $d2['bthotelname'] ) && empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = $d2['bthotelname'];
            }
            elseif( empty( $d2['bthotelname'] ) && !empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = nl2br( $d2['bthoteladdress'] );
            }

            $data[] = array(
                'id'           => $d2['bid'],
                'bdid'         => $d2['bdid'],
                'btid'         => $d2['btid'],
                'from'         => $d2['bdto'],
                'hotel_detail' => $hotel_detail,
                'dept_date'    => date( 'd M Y', strtotime( $d2['bddate'] ) ),
                'dept_time'    => date( 'H:i', strtotime( $d2['bdarrivetime'] ) ),
                'guest_num'    => empty( $guest ) ? '-' : implode( ', ', $guest ),
                'pickup'       => is_null ( $d2['taname'] ) ? 'No accommodation' : $d2['taname'],
                'booked_by'    => empty( $d2['agid'] ) ? $d2['bbname'] : get_agent( $d2['agid'], 'agname' ),
                'guest_name'   => ticket_passenger_list_name( ticket_passenger_list( $d2['bdid'] ), true ),
                'trans_type'   => $d2['bttrans_type'] == '0' ? 'Shared' : (  $d2['bttrans_type'] == '1' ? 'Private' : 'Own Transport' )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Drop-off More Guest Table
| -------------------------------------------------------------------------------------
*/
function ticket_drop_off_more_guest_table_data()
{
    $site_url = site_url();
    $data     = get_trip_transport( $_POST['ttid'], 'drop-off' );

    if( !empty( $data ) )
    {
        set_template( PLUGINS_PATH . '/ticket/tpl/transport/guest-list.html', 'transport' );

        add_block( 'more-block', 'mblock', 'transport' );
        add_block( 'list-block', 'lblock', 'transport' );

        extract( $data );

        $tttime = date( 'H:i', strtotime( $tttime ) );
        $ttdate = date( 'd M Y', strtotime( $ttdate ) );

        add_variable( 'ttid', $ttid );
        add_variable( 'date', $ttdate );
        add_variable( 'time', $tttime );
        add_variable( 'location', $ttlocation );
        add_variable( 'transport_type', $tttype );
        add_variable( 'capacity', get_vehicle_capacity( $vid ) );
        add_variable( 'trans_type', $tttype == 'Shared' ? '0' : '1' );
        add_variable( 'fee_format', number_format( $ttfee, 0, '', '.' ) );
        add_variable( 'status', $ttstatus == '0' ? 'Open' : 'Delivered' );
        add_variable( 'passenger', get_trip_transport_passenger( $type, $ttlocation, $vid, $ttdate, $tttime ) );

        add_variable( 'driver', $dname );
        add_variable( 'vehicle', $vname );

        add_variable( 'rname', $rname );
        add_variable( 'boname', $boname );

        add_variable( 'etime', 'ETA' );
        add_variable( 'type', 'Drop-off' );
        add_variable( 'tran_type', 'drop-off' );
        add_variable( 'loc_type', 'Arrival' );
        add_variable( 'title', 'More Guest To Drop-off' );

        add_variable( 'limit', post_viewed() );
        add_variable( 'action', get_state_url( 'transport-list&sub=drop-off-list' ) );
        add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-transport-ajax/' );
        add_variable( 'cancel_link', get_state_url( 'transport-list&sub=drop-off-list&prc=edit&show=false&id=' . $ttid ) );

        add_actions( 'section_title', 'Add More Guest To Drop-off List' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        parse_template( 'more-block', 'mblock', false );

        return return_template( 'transport' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Pickup More Guest Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_drop_off_more_guest_table_query( $ttid = '', $trans_type = '', $date = '', $time = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1 => 'a.bbname',
        3 => 'IFNULL( d.taname, "No Accommodation" )',
        6 => 'b.bddeparttime',
        7 => 'b.bddate',
        8 => 'b.bdfrom',
        9 => 'c.bttrans_type'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.bid ASC';
    }

    if( empty( $rdata['search']['value'] ) )
    {
        $s = 'SELECT
                c.hid,
                a.bid,
                a.agid,
                a.bbname,
                b.bdid,
                b.num_adult,
                b.num_child,
                b.num_infant,
                c.bttrans_type,
                b.bddeparttime,
                b.bdarrivetime,
                b.bdto,
                b.bddate,
                c.btid,
                c.bttype,
                c.bthotelname,
                c.bthoteladdress,
                IFNULL( 
                    d.taname, "No Accommodation" 
                ) AS taname
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
              LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
              WHERE c.btid NOT IN(
                SELECT btid
                FROM ticket_trip_transport_detail
                WHERE ttdtype = "drop-off"
              ) AND b.bdpstatus IN ( "pa", "ca", "pp" )
              AND c.bttype = "drop-off"
              AND b.bdrevstatus = "Devinitive"
              AND c.bttrans_type = "' . $trans_type . '"
              AND a.bstt <> "ar"
              AND b.bddate = "' . date( 'Y-m-d', strtotime( $date ) ) . '"
              AND b.bdarrivetime = "' . date( 'H:i:s', strtotime( $time ) ) . '"
              ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                c.hid,
                a.bid,
                a.agid,
                a.bbname,
                b.bdid,
                b.num_adult,
                b.num_child,
                b.num_infant,
                c.bttrans_type,
                b.bddeparttime,
                b.bdarrivetime,
                b.bdto,
                b.bddate,
                c.btid,
                c.bttype,
                c.bthotelname,
                c.bthoteladdress,
                IFNULL( 
                    d.taname, "No Accommodation" 
                ) AS taname
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
              LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
              WHERE c.btid NOT IN(
                SELECT btid
                FROM ticket_trip_transport_detail
                WHERE ttdtype = "drop-off"
              ) AND b.bdpstatus IN ( "pa", "ca", "pp" )
              AND c.bttype = "drop-off"
              AND b.bdrevstatus = "Devinitive"
              AND c.bttrans_type = "' . $trans_type . '"
              AND a.bstt <> "ar"
              AND b.bddate = "' . date( 'H:i:s', strtotime( $date ) ) . '"
              AND b.bdarrivetime = "' . date( 'H:i:s', strtotime( $time ) ) . '"
              AND ( ' . implode( ' OR ', $search ) . ' ) ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $guest = array();

            if( !empty( $d2['num_adult'] ) )
            {
                $guest[] = $d2['num_adult'] . ( count( $d2['num_adult'] ) > 1 ? ' adults' : ' adult' );
            }

            if( !empty( $d2['num_child'] ) )
            {
                $guest[] = $d2['num_child'] . ( count( $d2['num_child'] ) > 1 ? ' childs' : ' child' );
            }

            if( !empty( $d2['num_infant'] ) )
            {
                $guest[] = $d2['num_infant'] . ( count( $d2['num_infant'] ) > 1 ? ' infants' : ' infant' );
            }

            if( empty( $d2['bthotelname'] ) && empty( $d2['bthoteladdress'] ) )
            {
                if( is_null( $d2['hid'] ) )
                {
                    $hotel_detail = 'No hotel';
                }
                else
                {
                    $h = get_hotel( $d2['hid'] );

                    $hotel_detail = $h['hname'] . '<br />' . nl2br( $h['haddress'] );
                }
            }
            elseif( !empty( $d2['bthotelname'] ) && !empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = $d2['bthotelname'] . '<br />' . nl2br( $d2['bthoteladdress'] );
            }
            elseif( !empty( $d2['bthotelname'] ) && empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = $d2['bthotelname'];
            }
            elseif( empty( $d2['bthotelname'] ) && !empty( $d2['bthoteladdress'] ) )
            {
                $hotel_detail = nl2br( $d2['bthoteladdress'] );
            }

            $data[] = array(
                'id'           => $d2['bid'],
                'bdid'         => $d2['bdid'],
                'btid'         => $d2['btid'],
                'from'         => $d2['bdto'],
                'hotel_detail' => $hotel_detail,
                'dept_date'    => date( 'd M Y', strtotime( $d2['bddate'] ) ),
                'dept_time'    => date( 'H:i', strtotime( $d2['bdarrivetime'] ) ),
                'guest_num'    => empty( $guest ) ? '-' : implode( ', ', $guest ),
                'pickup'       => is_null ( $d2['taname'] ) ? 'No accommodation' : $d2['taname'],
                'booked_by'    => empty( $d2['agid'] ) ? $d2['bbname'] : get_agent( $d2['agid'], 'agname' ),
                'guest_name'   => ticket_passenger_list_name( ticket_passenger_list( $d2['bdid'] ), true ),
                'trans_type'   => $d2['bttrans_type'] == '0' ? 'Shared' : (  $d2['bttrans_type'] == '1' ? 'Private' : 'Own Transport' )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Process Trip Transport
| -------------------------------------------------------------------------------------
*/
function ticket_process_trip_transport( $type = '' )
{
    global $flash;

    if( isset( $_POST['bdata'] ) )
    {
        $_POST['btid'] = is_valid_trip( $_POST['bdata'] );

        if( empty( $_POST['btid'] ) )
        {
            $flash->add( array( 'type'=> 'error', 'content' => array( 'Some of booking data not valid, please choose booking data with same departure ( date, time, location ) and transport type' ) ) );

            if( $type == 'pickup' )
            {
                return ticket_pickup_guest_table_data();
            }
            elseif( $type == 'drop-off' )
            {
                return ticket_drop_off_guest_table_data();
            }
        }
        else
        {
            header( 'Location:' . get_state_url( 'transport&sub=' . $type . '-guest&prc=add_new&prm=' . base64_encode( json_encode( $_POST ) ) ) );

            exit;
        }
    }
    else
    {
        $flash->add( array( 'type'=> 'error', 'content' => array( 'Please select one reservation from the list, by checked the checkbox field' ) ) );

        if( $type == 'pickup' )
        {
            return ticket_pickup_guest_table_data();
        }
        elseif( $type == 'drop-off' )
        {
            return ticket_drop_off_guest_table_data();
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Process More Trip Transport
| -------------------------------------------------------------------------------------
*/
function ticket_process_more_trip_transport( $type = '' )
{
    global $flash;

    if( isset( $_POST['bdata'] ) )
    {
        $_POST['btid'] = is_valid_trip( $_POST['bdata'] );

        if( empty( $_POST['btid'] ) )
        {
            $flash->add( array( 'type'=> 'error', 'content' => array( 'Some of booking data not valid, please choose booking data with same departure ( date, time, location ) and transport type' ) ) );

            if( $type == 'pickup' )
            {
                return ticket_pickup_more_guest_table_data();
            }
            elseif( $type == 'drop-off' )
            {
                return ticket_drop_off_more_guest_table_data();
            }
        }
        else
        {
            save_trip_transport_detail( $_POST['ttid'], $type );

            header( 'Location:' . get_state_url( 'transport-list&sub=' . $type . '-list&prc=edit&show=true&id=' . $_POST['ttid'] ) );

            exit;
        }
    }
    else
    {
        $flash->add( array( 'type'=> 'error', 'content' => array( 'Please select one reservation from the list, by checked the checkbox field' ) ) );

        if( $type == 'pickup' )
        {
            return ticket_pickup_more_guest_table_data();
        }
        elseif( $type == 'drop-off' )
        {
            return ticket_drop_off_more_guest_table_data();
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Add Trip Transport Form
| -------------------------------------------------------------------------------------
*/
function ticket_add_trip_transport( $type = '' )
{
    global $db;

    if( isset( $_GET['prm'] ) )
    {
        $param = $_GET['prm'];

        extract( json_decode( base64_decode( $_GET['prm'] ), true ) );
    }
    else
    {
        $param = '';

        extract( $_POST );
    }

    run_save_trip_transport( $type, $param );

    set_template( PLUGINS_PATH . '/ticket/tpl/transport/guest-form.html', 'transport' );

    add_block( 'loop-block', 'lplock', 'transport' );
    add_block( 'form-block', 'lblock', 'transport' );

    $s = 'SELECT
            a.bid,
            a.agid,
            a.bbname,
            b.bddeparttime,
            b.bdarrivetime,
            b.bdfrom,
            b.bdto,
            b.bddate,
            b.bdtranstype,
            b.num_adult,
            b.num_child,
            b.num_infant,
            c.btid,
            c.bttrans_type,
            c.bttype,
            c.hid,
            c.taid,
            c.bthotelname,
            c.bthoteladdress,
            d.taname
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
          LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
          WHERE a.bstt <> "ar" AND c.btid IN (' . implode( ',', $btid ) . ') AND c.bttype = %s';
    $q = $db->prepare_query(  $s, $type );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $i = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $vid = isset( $_POST['vid'] ) ? $_POST['vid'] : '';
            $did = isset( $_POST['did'] ) ? $_POST['did'] : '';
            $sts = isset( $_POST['status'] ) ? $_POST['status'] : '';

            if( $i == 0 )
            {
                $dh = get_hotel( $d['hid'] );

                if( !empty( $dh ) )
                {
                    $address = $dh['hname'] . '<br>' . nl2br( $dh['haddress'] ) . '<br>Ph: ' . $dh['hphone'] . '<br/>Email: ' . $dh['hemail'];
                }

                $bdloc     = strtolower( $type ) == 'pickup' ? $d['bdfrom'] : $d['bdto'];
                $bdtime    = strtolower( $type ) == 'pickup' ? $d['bddeparttime'] : $d['bdarrivetime'];

                $tttime    = date( 'H:i', strtotime( $bdtime ) );
                $ttdate    = date( 'd M Y', strtotime( $d['bddate'] ) );

                $tttype    = $d['bttrans_type'] == '1' ? 'Private' : 'Shared';

                $passenger = get_trip_transport_passenger( $type, $bdloc, $vid, $ttdate, $bdtime );
                $capacity  = get_vehicle_capacity( $vid );
            }

            if( !empty( $d['num_adult'] ) )
            {
                $guest[] = $d['num_adult'] . ( count( $d['num_adult'] ) > 1 ? ' adults' : ' adult' );
            }

            if( !empty( $d['num_child'] ) )
            {
                $guest[] = $d['num_child'] . ( count( $d['num_child'] ) > 1 ? ' childs' : ' child' );
            }

            if( !empty( $d['num_infant'] ) )
            {
                $guest[] = $d['num_infant'] . ( count( $d['num_infant'] ) > 1 ? ' infants' : ' infant' );
            }

            if( empty( $d['bthotelname'] ) && empty( $d['bthoteladdress'] ) )
            {
                if( is_null( $d['hid'] ) )
                {
                    $haddress = 'No hotel';
                }
                else
                {
                    $h = get_hotel( $d['hid'] );

                    $haddress = $h['hname'] . '<br />' . nl2br( $h['haddress'] );
                }
            }
            elseif( !empty( $d['bthotelname'] ) && !empty( $d['bthoteladdress'] ) )
            {
                $haddress = $d['bthotelname'] . '<br />' . nl2br( $d['bthoteladdress'] );
            }
            elseif( !empty( $d['bthotelname'] ) && empty( $d['bthoteladdress'] ) )
            {
                $haddress = $d['bthotelname'];
            }
            elseif( empty( $d['bthotelname'] ) && !empty( $d['bthoteladdress'] ) )
            {
                $haddress = nl2br( $d['bthoteladdress'] );
            }

            add_variable( 'bid', $d['bid'] );
            add_variable( 'btid', $d['btid'] );
            add_variable( 'bbname', $d['bbname'] );

            add_variable( 'address', $haddress );
            add_variable( 'guest_num', empty( $guest ) ? '-' : implode( ', ', $guest ) );
            add_variable( 'area', is_null( $d['taid'] ) ? 'No accommodation' : $d['taname'] );

            parse_template( 'loop-block', 'lplock', true );

            $i++;
        }
    }

    $site_url = site_url();
    $dtype    = get_transport_driver( $did, 'dtype' );

    add_variable( 'date', isset( $ttdate ) ? $ttdate : '' );
    add_variable( 'time', isset( $tttime ) ? $tttime : '' );
    add_variable( 'location', isset( $bdloc  ) ? $bdloc : '' );
    add_variable( 'transport_type', isset( $tttype ) ? $tttype : '' );
    add_variable( 'passenger', isset( $passenger ) ? $passenger : 0 );
    add_variable( 'capacity', isset( $capacity ) ? $capacity : 0 );

    add_variable( 'type', $type );
    add_variable( 'param', $param );
    add_variable( 'title', 'Add to ' . ucfirst( $type ) . ' List' );
    add_variable( 'publish_title', 'Save to ' . ucfirst( $type ) . ' List' );
    add_variable( 'print_title', 'Save to ' . ucfirst( $type ) . ' List And Print' );
    add_variable( 'loc_type', strtolower( $type ) == 'pickup' ? 'Departure' : 'Arrival' );

    add_variable( 'driver_option', get_driver_option( $did, true, 'Select Driver', '1' ) );
    add_variable( 'status_option', get_trip_status_option( $sts ) );
    add_variable( 'vehicle_option', get_trip_transport_vehicle_option( $type, $bdloc, $vid, $ttdate, $bdtime, '0' ) );

    add_variable( 'driver_type_style', $dtype == '1' ? '' : 'sr-only' );

    add_variable( 'message', generate_message_block() );

    add_variable( 'action', get_state_url( 'transport&sub=' . $_GET['sub'] .'&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=' . $_GET['sub'] ) );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-transport-ajax/' );

    parse_template( 'form-block', 'lblock', false );

    add_actions( 'section_title', 'Add Trip Transport' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'transport' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Trip Transport Form
| -------------------------------------------------------------------------------------
*/
function ticket_edit_trip_transport( $type = '' )
{
    $message = run_edit_trip_transport( $type );
    $data    = get_trip_transport( $_GET['id'], $type );

    if( !empty( $data ) )
    {
        set_template( PLUGINS_PATH . '/ticket/tpl/transport/form.html', 'transport' );
        add_block( 'loop-block', 'lplock', 'transport' );
        add_block( 'form-block', 'lblock', 'transport' );

        extract( $data );

        $tttime = date( 'H:i', strtotime( $tttime ) );
        $ttdate = date( 'd M Y', strtotime( $ttdate ) );

        if( isset( $detail ) && !empty( $detail) )
        {
            foreach( $detail as $d )
            {
                $guest = array();

                if( !empty( $d['num_adult'] ) )
                {
                    $guest[] = $d['num_adult'] . ( count( $d['num_adult'] ) > 1 ? ' adults' : ' adult' );
                }

                if( !empty( $d['num_child'] ) )
                {
                    $guest[] = $d['num_child'] . ( count( $d['num_child'] ) > 1 ? ' childs' : ' child' );
                }

                if( !empty( $d['num_infant'] ) )
                {
                    $guest[] = $d['num_infant'] . ( count( $d['num_infant'] ) > 1 ? ' infants' : ' infant' );
                }

                $pass  = ticket_passenger_list( $d['bdid'] );
                $gname = ticket_passenger_list_name( $pass );

                add_variable( 'ttdid', $d['ttdid'] );
                add_variable( 'area', $d['taname'] );
                add_variable( 'guest_name', $gname );
                add_variable( 'bbname', empty( $d['agid'] ) ? $d['bbname'] : get_agent( $d['agid'], 'agname' ) );
                add_variable( 'address', $d['bthotelname'] . '<br>' . nl2br( $d['bthoteladdress'] ) );
                add_variable( 'phone', $d['bthotelphone'] );
                add_variable( 'email', get_hotel( $d['hid'], 'hemail' ) );
                add_variable( 'guest_num', empty( $guest ) ? '-' : implode( ', ', $guest ) );
                add_variable( 'delete_cls', $ttstatus == '1' ? 'sr-only' : '' );

                parse_template( 'loop-block', 'lplock', true );
            }
        }

        $site_url = site_url();
        $dtype    = get_transport_driver( $did, 'dtype' );

        add_variable( 'ttid', $ttid );
        add_variable( 'date', $ttdate );
        add_variable( 'time', $tttime );
        add_variable( 'location', $ttlocation );
        add_variable( 'transport_type', $tttype );

        add_variable( 'fee', $ttfee );
        add_variable( 'fee_format', number_format( $ttfee, 0, '', '.' ) );

        add_variable( 'driver', $dname );
        add_variable( 'vehicle', $vname );

        add_variable( 'rname', $rname );
        add_variable( 'boname', $boname );

        add_variable( 'type', $type );
        add_variable( 'type_txt', ucfirst( $type ) );
        add_variable( 'title', ucfirst( $type ) . ' List' );
        add_variable( 'print_title', 'Print ' . ucfirst( $type ) . ' List' );

        add_variable( 'capacity', get_vehicle_capacity( $vid ) );
        add_variable( 'passenger', get_trip_transport_passenger( $type, $ttlocation, $vid, $ttdate, $tttime ) );

        add_variable( 'driver_option', get_driver_option( $did, true, 'Select Driver' ) );
        add_variable( 'status_option', get_trip_status_option( $ttstatus ) );

        add_variable( 'driver_type_style', $dtype == '1' ? '' : 'sr-only' );

        if( $ttstatus == '1' )
        {
            add_variable( 'status', 'Delivered' );
            add_variable( 'driver_option_attr', 'disabled' );
            add_variable( 'vehicle_option_attr', 'disabled' );
            add_variable( 'vehicle_option', get_vehicle_option( $vid ) );
        }
        else
        {
            add_variable( 'status', 'Open' );
            add_variable( 'driver_option_attr', '' );
            add_variable( 'vehicle_option_attr', '' );
            add_variable( 'vehicle_option', get_trip_transport_vehicle_option( $type, $ttlocation, $vid, $ttdate, $tttime ) );
        }

        if( isset( $_GET['show'] ) && $_GET['show'] == 'true' )
        {
            add_variable( 'action_text', 'Edit' );
            add_variable( 'form_type', 'show-detail' );
            add_variable( 'button_name', 'add_more_guest' );
            add_variable( 'publish_title', 'Add More Guest' );
            add_variable( 'action', get_state_url( 'transport-list&sub=' . $type . '-list' ) );
            add_variable( 'action_link', get_state_url( 'transport-list&sub=' . $type . '-list&prc=edit&show=false&id=' . $ttid ) );
        }
        else
        {
            add_variable( 'action_text', 'Back' );
            add_variable( 'form_type', 'edit-detail' );
            add_variable( 'button_name', 'publish' );
            add_variable( 'publish_title', 'Save ' . ucfirst( $type ) . ' List' );
            add_variable( 'action', get_state_url( 'transport-list&sub=' . $type . '-list&prc=edit&show=false&id=' . $ttid ) );
            add_variable( 'action_link', get_state_url( 'transport-list&sub=' . $type . '-list&prc=edit&show=true&id=' . $ttid ) );
        }

        add_variable( 'message', generate_message_block( $message ) );
        add_variable( 'cancel_link', get_state_url( 'transport&sub=' . $type . '-list' ) );
        add_variable( 'loc_type', strtolower( $type ) == 'pickup' ? 'Departure' : 'Arrival' );

        add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-transport-ajax/' );
        add_variable( 'report_url', HTSERVER . $site_url . '/trip-transport-print-report/?id=' . $ttid . '&type=' . $type );
        add_variable( 'last_update_note', get_last_update_note( $ttid, 'trip-transport' ) );

        parse_template( 'form-block', 'lblock', false );

        add_actions( 'section_title', 'Edit Trip Transport' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        return return_template( 'transport' );
    }
    else
    {
        return not_found_template();
    }
}

/*
| -------------------------------------------------------------------------------------
| Pickup List
| -------------------------------------------------------------------------------------
*/
function ticket_pickup_list()
{
    if( is_edit() )
    {
        if( isset( $_GET['id'] ) )
        {
            return ticket_edit_trip_transport( 'pickup' );
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_add_more_trip_transport() )
    {
        return ticket_process_more_trip_transport( 'pickup' );
    }
    elseif( is_add_more_guest() )
    {
        return ticket_pickup_more_guest_table_data();
    }

    return ticket_pickup_list_table_data();
}

/*
| -------------------------------------------------------------------------------------
| Pickup List Table Data
| -------------------------------------------------------------------------------------
*/
function ticket_pickup_list_table_data()
{
    $site_url = site_url();
    $filter   = get_filter_list();

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/transport/list.html', 'transport' );

    add_block( 'list-block', 'lblock', 'transport' );

    add_variable( 'date', $bddate );
    add_variable( 'search', $search );
    add_variable( 'tran_type', 'pickup' );
    add_variable( 'title', 'Pickup List' );
    add_variable( 'state', 'transport-list&sub=pickup-list' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Trip') );
    add_variable( 'trans_type_option', get_transport_type_option( $trans_type ) );
    add_variable( 'driver_option', get_driver_option( $did, true, 'All Driver' ) );
    add_variable( 'vehicle_option', get_vehicle_option( $vid, true, 'All Vehicle' ) );
    add_variable( 'area_option', get_transport_area_option( $taid, true, 'All Area', true ) );
    add_variable( 'location_option', get_location_option( $lcid, true, 'All Departure Point') );

    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-transport-ajax/' );
    add_variable( 'print_url', HTSERVER . $site_url . '/pickup-list-print-report/?prm=' . base64_encode( json_encode( $filter ) ) );
    add_variable( 'export_pdf_url', HTSERVER . $site_url . '/pickup-list-pdf-report/?prm=' . base64_encode( json_encode( $filter ) ) );
    add_variable( 'export_csv_url', HTSERVER . $site_url . '/pickup-list-csv-report/?prm=' . base64_encode( json_encode( $filter ) ) );

    add_actions( 'section_title', 'Pickup List' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/momentjs/latest/moment.min.js' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' );

    add_actions( 'header_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'list-block', 'lblock', false );

    return return_template( 'transport' );
}

function ticket_pickup_list_table_query( $rid = '', $lcid = '', $vid = '', $did = '', $trans_type = '', $bddate = '', $taid = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1 => 'b.vname',
        2 => 'a.ttdate',
        3 => 'c.dname',
        6 => 'a.tttype',
        7 => 'a.ttstatus',
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.ttdate';
    }

    if( empty( $rdata['search']['value'] ) )
    {
        $w = '';
        $h = '';

        if( $rid != '' )
        {
            $w .= $db->prepare_query( ' AND r.rid = %d', $rid );
        }

        if( $lcid != '' )
        {
            $w .= $db->prepare_query( ' AND bd.bdfrom_id = %d', $lcid );
        }

        if( $vid != '' )
        {
            $w .= $db->prepare_query( ' AND b.vid = %d', $vid );
        }

        if( $did != '' )
        {
            $w .= $db->prepare_query( ' AND c.did = %d', $did );
        }

        if( $trans_type != '' )
        {
            $w .= $db->prepare_query( ' AND a.tttype = %s', ( $trans_type == '0' ? 'Shared' : 'Private' ) );
        }

        if( $bddate != '' )
        {
            list( $start, $end ) = explode( ' - ', $bddate );

            $start = implode( '-', array_reverse( explode( '/', $start ) ) );
            $end   = implode( '-', array_reverse( explode( '/', $end ) ) );

            $w .= $db->prepare_query( ' AND a.ttdate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $start ) ), date( 'Y-m-d', strtotime( $end ) ) );
        }

        if( $taid != '' )
        {
            $h .= $db->prepare_query( ' HAVING pickup_drop_area LIKE %s', '%' . $taid . '%' );
        }

        $q = 'SELECT
                a.ttid,
                a.type,
                a.tttime,
                b.vid,
                b.vname,
                c.dname,
                a.ttdate,
                b.vpassenger,
                a.ttlocation,
                a.tttype,
                a.ttstatus, (
                    SELECT GROUP_CONCAT( DISTINCT( c2.taname ) ORDER BY c2.taname ASC SEPARATOR ", " )
                    FROM ticket_trip_transport_detail AS a2
                    LEFT JOIN ticket_booking_transport AS b2 ON a2.btid = b2.btid
                    LEFT JOIN ticket_transport_area AS c2 ON b2.taid = c2.taid
                    WHERE a2.ttid = a.ttid AND b2.bttype = a.type
                ) AS pickup_drop_area
              FROM ticket_trip_transport_detail AS d
              LEFT JOIN ticket_trip_transport AS a ON a.ttid = d.ttid
              LEFT JOIN ticket_transport_vehicle AS b ON a.vid = b.vid
              LEFT JOIN ticket_transport_driver AS c ON a.did = c.did
              LEFT JOIN ticket_booking_transport AS bt ON bt.btid = d.btid
              LEFT JOIN ticket_booking_detail AS bd ON bd.bdid = bt.bdid
              LEFT JOIN ticket_route AS r ON r.rid = bd.rid
              WHERE a.type = "pickup" ' . $w . ' GROUP BY a.ttid ' . $h . ' ORDER BY ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $q = 'SELECT
                a.ttid,
                a.type,
                a.tttime,
                b.vid,
                b.vname,
                c.dname,
                a.ttdate,
                b.vpassenger,
                a.ttlocation,
                a.tttype,
                a.ttstatus, (
                    SELECT GROUP_CONCAT( DISTINCT( c2.taname ) ORDER BY c2.taname ASC SEPARATOR ", " )
                    FROM ticket_trip_transport_detail AS a2
                    LEFT JOIN ticket_booking_transport AS b2 ON a2.btid = b2.btid
                    LEFT JOIN ticket_transport_area AS c2 ON b2.taid = c2.taid
                    WHERE a2.ttid = a.ttid AND b2.bttype = a.type
                ) AS pickup_drop_area
              FROM ticket_trip_transport_detail AS d
              LEFT JOIN ticket_trip_transport AS a ON a.ttid = d.ttid
              LEFT JOIN ticket_transport_vehicle AS b ON a.vid = b.vid
              LEFT JOIN ticket_transport_driver AS c ON a.did = c.did
              LEFT JOIN ticket_booking_transport AS bt ON bt.btid = d.btid
              LEFT JOIN ticket_booking_detail AS bd ON bd.bdid = bt.bdid
              LEFT JOIN ticket_route AS r ON r.rid = bd.rid
              WHERE a.type = "pickup" AND ( ' . implode( ' OR ', $search ) . ' ) GROUP BY a.ttid ORDER BY ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'id'               => $d2['ttid'],
                'vname'            => $d2['vname'],
                'dname'            => $d2['dname'],
                'tttype'           => $d2['tttype'],
                'ttstatus'         => $d2['ttstatus'],
                'vpassenger'       => $d2['vpassenger'],
                'pickup_drop_area' => $d2['pickup_drop_area'],
                'ttdate'           => date( 'd M Y', strtotime( $d2['ttdate'] ) ),
                'passenger'        => get_trip_transport_passenger( $d2['type'], $d2['ttlocation'], $d2['vid'], $d2['ttdate'], $d2['tttime'] ),
                'show_link'        => get_state_url( 'transport-list&sub=' . $d2['type'] . '-list&prc=edit&show=true&id=' . $d2['ttid'] ),
                'edit_link'        => get_state_url( 'transport-list&sub=' . $d2['type'] . '-list&prc=edit&show=false&id=' . $d2['ttid'] ),
                'ajax_link'        => HTSERVER . site_url() . '/ticket-transport-ajax/'
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Pickup List Report Data
| -------------------------------------------------------------------------------------
*/
function get_pickup_list_report( $filter )
{
    global $db;

    extract( $filter );

    $w = '';

    if( $rid != '' )
    {
        $w .= $db->prepare_query( ' AND b.rid = %d', $rid );
    }

    if( $lcid != '' )
    {
        $w .= $db->prepare_query( ' AND b.bdfrom_id = %d', $lcid );
    }

    if( $vid != '' )
    {
        $w .= $db->prepare_query( ' AND c.vid = %d', $vid );
    }

    if( $did != '' )
    {
        $w .= $db->prepare_query( ' AND ( SELECT c2.did
            FROM ticket_trip_transport AS a2
            LEFT JOIN ticket_trip_transport_detail AS b2 ON b2.ttid = a2.ttid
            LEFT JOIN ticket_transport_driver AS c2 ON a2.did = c2.did
            WHERE b2.btid = c.btid ) = %d', $did );
    }

    if( $trans_type != '' )
    {
        $w .= $db->prepare_query( ' AND c.bttrans_type = %s', $trans_type );
    }

    if( $bddate != '' )
    {
        list( $start, $end ) = explode( ' - ', $bddate );

        $start = implode( '-', array_reverse( explode( '/', $start ) ) );
        $end   = implode( '-', array_reverse( explode( '/', $end ) ) );

        $w .= $db->prepare_query( ' AND b.bddate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $start ) ), date( 'Y-m-d', strtotime( $end ) ) );
    }

    if( $taid != '' )
    {
        $w .= $db->prepare_query( ' AND d.taname = %s', $taid );
    }

    $q = 'SELECT
            a.bid,
            a.agid,
            a.bbname,
            b.bdid,
            b.num_adult,
            b.num_child,
            b.num_infant,
            b.bddeparttime,
            b.bdfrom,
            b.bddate,
            c.btid,
            c.bttype,
            c.hid,
            c.bthotelname,
            c.bttrans_type,
            c.bthoteladdress,
            c.bthotelphone,
            c.bthotelroomnumber,
            c.btdrivername,
            c.btdriverphone,
            d.taname,
            e.rname,
            f.hname,
            f.hphone,
            f.haddress,
            g.boname,
            CASE c.bttrans_type
                WHEN "0" THEN "1"
                WHEN "1" THEN "0"
                ELSE "2"
            END AS trans_order,
            CASE c.bttrans_type
                WHEN "0" THEN "Shared"
                WHEN "1" THEN "Private"
                ELSE "Own Transport"
            END AS trans_type,
            CASE b.num_adult
                WHEN 0 THEN ""
                WHEN 1 THEN CONCAT( b.num_adult, " adult" )
                ELSE CONCAT( b.num_adult, " adults" )
            END AS adult,
            CASE b.num_child
                WHEN 0 THEN ""
                WHEN 1 THEN CONCAT( b.num_child, " child" )
                ELSE CONCAT( b.num_child, " childs" )
            END AS child,
            CASE b.num_infant
                WHEN 0 THEN ""
                WHEN 1 THEN CONCAT( b.num_infant, " infant" )
                ELSE CONCAT( b.num_infant, " infants" )
            END AS infant,
            (
                SELECT GROUP_CONCAT( a2.bpname SEPARATOR ",<br/>" )
                FROM ticket_booking_passenger AS a2
                WHERE a2.bdid = b.bdid
                ORDER BY a2.bpid ASC
            ) AS pass_name,
            (
                SELECT c2.dname
                FROM ticket_trip_transport AS a2
                LEFT JOIN ticket_trip_transport_detail AS b2 ON b2.ttid = a2.ttid
                LEFT JOIN ticket_transport_driver AS c2 ON a2.did = c2.did
                WHERE b2.btid = c.btid
            ) AS driver_name,
            (
                SELECT c2.vname
                FROM ticket_trip_transport AS a2
                LEFT JOIN ticket_trip_transport_detail AS b2 ON b2.ttid = a2.ttid
                LEFT JOIN ticket_transport_vehicle AS c2 ON a2.vid = c2.vid
                WHERE b2.btid = c.btid
            ) AS vehicle
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
          LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
          LEFT JOIN ticket_route AS e ON b.rid = e.rid
          LEFT JOIN ticket_hotel AS f ON f.hid = c.hid
          LEFT JOIN ticket_boat AS g ON g.boid = b.boid
          WHERE a.bstt <> "ar" AND b.bdpstatus IN ( "pa", "ca", "pp" ) AND c.bttype = "pickup" ' . $w . '
          ORDER BY trans_order, d.taname , b.bddate, b.bddeparttime ASC ';
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $data   = array();
        $boname = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $haddr   = empty( $d['bthoteladdress'] ) ? ( $d['hid'] == 1 ? 'TBA' : ( is_null( $d['haddress'] ) ? '' : $d['haddress'] ) ) : $d['bthoteladdress'];
            $hphone  = empty( $d['bthotelphone'] ) ? ( $d['hid'] == 1 ? 'TBA' : ( is_null( $d['hphone'] ) ? '' : $d['hphone'] ) ) : $d['bthotelphone'];
            $hname   = empty( $d['bthotelname'] ) ? ( $d['hid'] == 1 ? 'TBA' : ( is_null( $d['hname'] ) ? '' : $d['hname'] ) ) : $d['bthotelname'];
            $hroom   = empty( $d['bthotelroomnumber'] ) ? '' : '( Room No : ' . $d['bthotelroomnumber'] . ' )';
            $driver  = $d['bttrans_type'] == 2 ? $d['btdrivername'] : ( is_null( $d['driver_name'] ) ? '' : $d['driver_name'] );
            $drvphone = $d['bttrans_type'] == 2 ? 'Phone : ' . $d['btdriverphone'] : '';
            $pssname = mb_convert_encoding( $d['pass_name'], 'UTF-8', 'UTF-8' );
            $time    = date( 'H:i', strtotime( $d['bddeparttime'] ) );
            $vehicle = is_null( $d['vehicle'] ) ? '' : $d['vehicle'];
            $taname  = empty( $d['taname'] ) ? '-' : $d['taname'];
            $loc     = 'Departure : ' . $d['bdfrom'] . ' (' . $time . ')';
            $torder  = $d['trans_order'];
            $ttype   = $d['trans_type'];
            $rname   = $d['rname'];
            $port    = $d['bdfrom'];
            $guest   = array();

            if( !empty( $d['num_adult'] ) )
            {
                $guest[] = $d['adult'];
            }

            if( !empty( $d['num_child'] ) )
            {
                $guest[] = $d['child'];
            }

            if( !empty( $d['num_infant'] ) )
            {
                $guest[] = $d['infant'];
            }

            $boname[] = $d['boname'];

            $data[ $loc ][ $driver ][ $torder ][] = array(
                'time'         => $time,
                'bddate'       => $bddate,
                'taname'       => $taname,
                'driver'       => $driver,
                'drvphone'     => $drvphone,
                'hotel_phone'  => $hphone,
                'vehicle'      => $vehicle,
                'guest_name'   => $pssname,
                'id'           => $d['bid'],
                'bdid'         => $d['bdid'],
                'btid'         => $d['btid'],
                'rname'        => $d['rname'],
                'loc'          => $d['bdfrom'],
                'booked_by'    => $d['bbname'],
                'num_adult'    => $d['num_adult'],
                'num_child'    => $d['num_child'],
                'num_infant'   => $d['num_infant'],
                'trans_type'   => $d['trans_type'],
                'hotel_detail' => $hname . $hroom . '<br />' . nl2br( $haddr ),
                'guest_num'    => empty( $guest ) ? '-' : implode( '<br />', $guest ),
            );
        }

        return array(
            'data'   => $data,
            'plabel' => 'Depart',
            'tlabel' => 'Pickup',
            'rname'  => empty( $rid ) ? 'All Routes' : $rname,
            'area'   => empty( $taid ) ? 'All Area' : $taname,
            'search' => empty( $search ) ? 'No Search' : $search,
            'bddate' => empty( $bddate ) ? 'All Trip Date' : $bddate,
            'point'  => empty( $lcid ) ? 'All Departure Point' : $port,
            'boname' => empty( $boname ) ? '-' : implode( ', ', array_unique( $boname ) ),
            'ttype'  => $trans_type == '' ? 'All Transport' : $ttype,
        );
    }
}

/*
| -------------------------------------------------------------------------------------
| Drop-off List
| -------------------------------------------------------------------------------------
*/
function ticket_drop_off_list()
{
    if( is_edit() )
    {
        if( isset( $_GET['id'] ) )
        {
            return ticket_edit_trip_transport( 'drop-off' );
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_add_more_trip_transport() )
    {
        return ticket_process_more_trip_transport( 'drop-off' );
    }
    elseif( is_add_more_guest() )
    {
        return ticket_drop_off_more_guest_table_data();
    }

    return ticket_drop_off_list_table_data();
}

/*
| -------------------------------------------------------------------------------------
| Drop-off List Table Data
| -------------------------------------------------------------------------------------
*/
function ticket_drop_off_list_table_data()
{
    $site_url = site_url();
    $filter   = get_filter_list();

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/transport/list.html', 'transport' );

    add_block( 'list-block', 'lblock', 'transport' );

    add_variable( 'date', $bddate );
    add_variable( 'search', $search );
    add_variable( 'tran_type', 'drop-off' );
    add_variable( 'title', 'Drop-off List' );
    add_variable( 'state', 'transport-list&sub=drop-off-list' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Trip') );
    add_variable( 'trans_type_option', get_transport_type_option( $trans_type ) );
    add_variable( 'driver_option', get_driver_option( $did, true, 'All Driver' ) );
    add_variable( 'vehicle_option', get_vehicle_option( $vid, true, 'All Vehicle' ) );
    add_variable( 'area_option', get_transport_area_option( $taid, true, 'All Area', true ) );
    add_variable( 'location_option', get_location_option( $lcid, true, 'All Arrival Point') );

    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-transport-ajax/' );
    add_variable( 'print_url', HTSERVER . $site_url . '/dropoff-list-print-report/?prm=' . base64_encode( json_encode( $filter ) ) );
    add_variable( 'export_pdf_url', HTSERVER . $site_url . '/dropoff-list-pdf-report/?prm=' . base64_encode( json_encode( $filter ) ) );
    add_variable( 'export_csv_url', HTSERVER . $site_url . '/dropoff-list-csv-report/?prm=' . base64_encode( json_encode( $filter ) ) );

    add_actions( 'section_title', 'Drop-off List' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/momentjs/latest/moment.min.js' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' );

    add_actions( 'header_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'list-block', 'lblock', false );

    return return_template( 'transport' );
}

function ticket_drop_off_list_table_query( $rid, $lcid = '', $vid = '', $did = '', $trans_type = '', $bddate = '', $taid = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1 => 'b.vname',
        2 => 'a.ttdate',
        3 => 'c.dname',
        6 => 'a.tttype',
        7 => 'a.ttstatus',
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.ttdate';
    }

    if( empty( $rdata['search']['value'] ) )
    {
        $w = '';
        $h = '';

        if( $rid != '' )
        {
            $w .= $db->prepare_query( ' AND r.rid = %d', $rid );
        }

        if( $lcid != '' )
        {
            $w .= $db->prepare_query( ' AND bd.bdto_id = %d', $lcid );
        }

        if( $vid != '' )
        {
            $w .= $db->prepare_query( ' AND b.vid = %d', $vid );
        }

        if( $did != '' )
        {
            $w .= $db->prepare_query( ' AND c.did = %d', $did );
        }

        if( $trans_type != '' )
        {
            $w .= $db->prepare_query( ' AND a.tttype = %s', ( $trans_type == '0' ? 'Shared' : 'Private' ) );
        }

        if( $bddate != '' )
        {
            list( $start, $end ) = explode( ' - ', $bddate );

            $start = implode( '-', array_reverse( explode( '/', $start ) ) );
            $end   = implode( '-', array_reverse( explode( '/', $end ) ) );

            $w .= $db->prepare_query( ' AND a.ttdate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $start ) ), date( 'Y-m-d', strtotime( $end ) ) );
        }

        if( $taid != '' )
        {
            $h .= $db->prepare_query( ' HAVING pickup_drop_area LIKE %s', '%' . $taid . '%' );
        }

        $q = 'SELECT
                a.ttid,
                a.type,
                a.tttime,
                b.vid,
                b.vname,
                c.dname,
                a.ttdate,
                b.vpassenger,
                a.ttlocation,
                a.tttype,
                a.ttstatus, (
                    SELECT GROUP_CONCAT( DISTINCT( c2.taname ) ORDER BY c2.taname ASC SEPARATOR ", " )
                    FROM ticket_trip_transport_detail AS a2
                    LEFT JOIN ticket_booking_transport AS b2 ON a2.btid = b2.btid
                    LEFT JOIN ticket_transport_area AS c2 ON b2.taid = c2.taid
                    WHERE a2.ttid = a.ttid AND b2.bttype = a.type
                ) AS pickup_drop_area
              FROM ticket_trip_transport_detail AS d
              LEFT JOIN ticket_trip_transport AS a ON a.ttid = d.ttid
              LEFT JOIN ticket_transport_vehicle AS b ON a.vid = b.vid
              LEFT JOIN ticket_transport_driver AS c ON a.did = c.did
              LEFT JOIN ticket_booking_transport AS bt ON bt.btid = d.btid
              LEFT JOIN ticket_booking_detail AS bd ON bd.bdid = bt.bdid
              LEFT JOIN ticket_route AS r ON r.rid = bd.rid
              WHERE a.type = "drop-off" ' . $w . ' GROUP BY a.ttid ' . $h . ' ORDER BY ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $q = 'SELECT
                a.ttid,
                a.type,
                a.tttime,
                b.vid,
                b.vname,
                c.dname,
                a.ttdate,
                b.vpassenger,
                a.ttlocation,
                a.tttype,
                a.ttstatus, (
                    SELECT GROUP_CONCAT( DISTINCT( c2.taname ) ORDER BY c2.taname ASC SEPARATOR ", " )
                    FROM ticket_trip_transport_detail AS a2
                    LEFT JOIN ticket_booking_transport AS b2 ON a2.btid = b2.btid
                    LEFT JOIN ticket_transport_area AS c2 ON b2.taid = c2.taid
                    WHERE a2.ttid = a.ttid AND b2.bttype = a.type
                ) AS pickup_drop_area
              FROM ticket_trip_transport_detail AS d
              LEFT JOIN ticket_trip_transport AS a ON a.ttid = d.ttid
              LEFT JOIN ticket_transport_vehicle AS b ON a.vid = b.vid
              LEFT JOIN ticket_transport_driver AS c ON a.did = c.did
              LEFT JOIN ticket_booking_transport AS bt ON bt.btid = d.btid
              LEFT JOIN ticket_booking_detail AS bd ON bd.bdid = bt.bdid
              LEFT JOIN ticket_route AS r ON r.rid = bd.rid
              WHERE a.type = "drop-off" AND ( ' . implode( ' OR ', $search ) . ' ) GROUP BY a.ttid ORDER BY ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'id'               => $d2['ttid'],
                'vname'            => $d2['vname'],
                'dname'            => $d2['dname'],
                'tttype'           => $d2['tttype'],
                'ttstatus'         => $d2['ttstatus'],
                'vpassenger'       => $d2['vpassenger'],
                'pickup_drop_area' => $d2['pickup_drop_area'],
                'ttdate'           => date( 'd M Y', strtotime( $d2['ttdate'] ) ),
                'passenger'        => get_trip_transport_passenger( $d2['type'], $d2['ttlocation'], $d2['vid'], $d2['ttdate'], $d2['tttime'] ),
                'show_link'        => get_state_url( 'transport-list&sub=' . $d2['type'] . '-list&prc=edit&show=true&id=' . $d2['ttid'] ),
                'edit_link'        => get_state_url( 'transport-list&sub=' . $d2['type'] . '-list&prc=edit&show=false&id=' . $d2['ttid'] ),
                'ajax_link'        => HTSERVER . site_url() . '/ticket-transport-ajax/'
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Drop-off List Report Data
| -------------------------------------------------------------------------------------
*/
function get_drop_off_list_report( $filter )
{
    global $db;

    extract( $filter );

    $w = '';

    if( $rid != '' )
    {
        $w .= $db->prepare_query( ' AND b.rid = %d', $rid );
    }

    if( $lcid != '' )
    {
        $w .= $db->prepare_query( ' AND b.bdto_id = %d', $lcid );
    }

    if( $vid != '' )
    {
        $w .= $db->prepare_query( ' AND c.vid = %d', $vid );
    }

    if( $did != '' )
    {
        $w .= $db->prepare_query( ' AND ( SELECT c2.did
            FROM ticket_trip_transport AS a2
            LEFT JOIN ticket_trip_transport_detail AS b2 ON b2.ttid = a2.ttid
            LEFT JOIN ticket_transport_driver AS c2 ON a2.did = c2.did
            WHERE b2.btid = c.btid ) = %d', $did );
    }

    if( $trans_type != '' )
    {
        $w .= $db->prepare_query( ' AND c.bttrans_type = %s', $trans_type );
    }

    if( $bddate != '' )
    {
        list( $start, $end ) = explode( ' - ', $bddate );

        $start = implode( '-', array_reverse( explode( '/', $start ) ) );
        $end   = implode( '-', array_reverse( explode( '/', $end ) ) );

        $w .= $db->prepare_query( ' AND b.bddate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $start ) ), date( 'Y-m-d', strtotime( $end ) ) );
    }

    if( $taid != '' )
    {
        $w .= $db->prepare_query( ' AND d.taname = %s', $taid );
    }

    $q = 'SELECT
            a.bid,
            a.agid,
            a.bbname,
            b.bdid,
            b.num_adult,
            b.num_child,
            b.num_infant,
            b.bdarrivetime,
            b.bdto,
            b.bddate,
            c.btid,
            c.bttype,
            c.hid,
            c.bthotelname,
            c.bttrans_type,
            c.bthoteladdress,
            c.bthotelphone,
            c.bthotelroomnumber,
            c.btdrivername,
            c.btdriverphone,
            d.taname,
            e.rname,
            f.hname,
            f.hphone,
            f.haddress,
            g.boname,
            CASE c.bttrans_type
                WHEN "0" THEN "1"
                WHEN "1" THEN "0"
                ELSE "2"
            END AS trans_order,
            CASE c.bttrans_type
                WHEN "0" THEN "Shared"
                WHEN "1" THEN "Private"
                ELSE "Own Transport"
            END AS trans_type,
            CASE b.num_adult
                WHEN 0 THEN ""
                WHEN 1 THEN CONCAT( b.num_adult, " adult" )
                ELSE CONCAT( b.num_adult, " adults" )
            END AS adult,
            CASE b.num_child
                WHEN 0 THEN ""
                WHEN 1 THEN CONCAT( b.num_child, " child" )
                ELSE CONCAT( b.num_child, " childs" )
            END AS child,
            CASE b.num_infant
                WHEN 0 THEN ""
                WHEN 1 THEN CONCAT( b.num_infant, " infant" )
                ELSE CONCAT( b.num_infant, " infants" )
            END AS infant,
            (
                SELECT GROUP_CONCAT( CONVERT( a2.bpname USING utf8 ) SEPARATOR ",<br/>" )
                FROM ticket_booking_passenger AS a2
                WHERE a2.bdid = b.bdid
                ORDER BY a2.bpid ASC
            ) AS pass_name,
            (
                SELECT c2.dname
                FROM ticket_trip_transport AS a2
                LEFT JOIN ticket_trip_transport_detail AS b2 ON b2.ttid = a2.ttid
                LEFT JOIN ticket_transport_driver AS c2 ON a2.did = c2.did
                WHERE b2.btid = c.btid
            ) AS driver_name,
            (
                SELECT c2.vname
                FROM ticket_trip_transport AS a2
                LEFT JOIN ticket_trip_transport_detail AS b2 ON b2.ttid = a2.ttid
                LEFT JOIN ticket_transport_vehicle AS c2 ON a2.vid = c2.vid
                WHERE b2.btid = c.btid
            ) AS vehicle
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          LEFT JOIN ticket_booking_transport AS c ON c.bdid = b.bdid
          LEFT JOIN ticket_transport_area AS d ON c.taid = d.taid
          LEFT JOIN ticket_route AS e ON b.rid = e.rid
          LEFT JOIN ticket_hotel AS f ON f.hid = c.hid
          LEFT JOIN ticket_boat AS g ON g.boid = b.boid
          WHERE a.bstt <> "ar" AND b.bdpstatus IN ( "pa", "ca", "pp" ) AND c.bttype = "drop-off" ' . $w . '
          ORDER BY trans_order, d.taname , b.bddate, b.bddeparttime ASC ';
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $data   = array();
        $boname = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $haddr   = empty( $d['bthoteladdress'] ) ? ( $d['hid'] == 1 ? 'TBA' : ( is_null( $d['haddress'] ) ? '' : $d['haddress'] ) ) : $d['bthoteladdress'];
            $hphone  = empty( $d['bthotelphone'] ) ? ( $d['hid'] == 1 ? 'TBA' : ( is_null( $d['hphone'] ) ? '' : $d['hphone'] ) ) : $d['bthotelphone'];
            $hname   = empty( $d['bthotelname'] ) ? ( $d['hid'] == 1 ? 'TBA' : ( is_null( $d['hname'] ) ? '' : $d['hname'] ) ) : $d['bthotelname'];
            $hroom   = empty( $d['bthotelroomnumber'] ) ? '' : '( Room No : ' . $d['bthotelroomnumber'] . ' )';
            $driver  = $d['bttrans_type'] == 2 ? $d['btdrivername'] : ( is_null( $d['driver_name'] ) ? '' : $d['driver_name'] );
            $drvphone = $d['bttrans_type'] == 2 ? 'Phone : ' . $d['btdriverphone'] : '';
            $pssname = mb_convert_encoding( $d['pass_name'], 'UTF-8', 'UTF-8' );
            $time    = date( 'H:i', strtotime( $d['bdarrivetime'] ) );
            $vehicle = is_null( $d['vehicle'] ) ? '' : $d['vehicle'];
            $taname  = empty( $d['taname'] ) ? '-' : $d['taname'];
            $loc     = 'Arrival : ' . $d['bdto'] . ' (' . $time . ')';
            $torder  = $d['trans_order'];
            $ttype   = $d['trans_type'];
            $rname   = $d['rname'];
            $port    = $d['bdto'];
            $guest   = array();

            if( !empty( $d['num_adult'] ) )
            {
                $guest[] = $d['adult'];
            }

            if( !empty( $d['num_child'] ) )
            {
                $guest[] = $d['child'];
            }

            if( !empty( $d['num_infant'] ) )
            {
                $guest[] = $d['infant'];
            }

            $boname[] = $d['boname'];

            $data[ $loc ][ $driver ][ $torder ][] = array(
                'time'         => $time,
                'bddate'       => $bddate,
                'taname'       => $taname,
                'driver'       => $driver,
                'drvphone'     => $drvphone,
                'guest_name'   => $pssname,
                'hotel_phone'  => $hphone,
                'vehicle'      => $vehicle,
                'id'           => $d['bid'],
                'bdid'         => $d['bdid'],
                'btid'         => $d['btid'],
                'rname'        => $d['rname'],
                'loc'          => $d['bdto'],
                'booked_by'    => $d['bbname'],
                'num_adult'    => $d['num_adult'],
                'num_child'    => $d['num_child'],
                'num_infant'   => $d['num_infant'],
                'trans_type'   => $d['trans_type'],
                'hotel_detail' => $hname . $hroom . '<br />' . nl2br( $haddr ),
                'guest_num'    => empty( $guest ) ? '-' : implode( '<br />', $guest ),
            );
        }

        return array(
            'data'   => $data,
            'plabel' => 'Arrival',
            'tlabel' => 'Drop-off',
            'rname'  => empty( $rid ) ? 'All Routes' : $rname,
            'area'   => empty( $taname ) ? 'All Area' : $taname,
            'search' => empty( $search ) ? 'No Search' : $search,
            'bddate' => empty( $bddate ) ? 'All Trip Date' : $bddate,
            'point'  => empty( $lcid ) ? 'All Arrival Point' : $port,
            'boname' => empty( $boname ) ? '-' : implode( ', ', array_unique( $boname ) ),
            'ttype'  => $trans_type == '' ? 'All Transport' : $ttype,
        );
    }
}

/*
| -------------------------------------------------------------------------------------
| Execute Save Trip Transport
| -------------------------------------------------------------------------------------
*/
function run_save_trip_transport( $type = '', $param = '' )
{
    global $flash;

    if( is_save_draft() || is_publish() || is_print() )
    {
        $error = validate_trip_transport_data( $type );

        if( empty( $error ) )
        {
            if( is_exist_in_transport( $_POST['btid'], $type ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Sorry this reservation already added to list before' ) ) );
            }
            else
            {
                $ttid = save_trip_transport( $type );

                if( empty( $ttid ) )
                {
                    $flash->add( array( 'type'=> 'error', 'content' => array( 'Sorry can\'t add reservation to ' . $type . ' list' ) ) );
                }
                else
                {
                    $flash->add( array( 'type'=> 'success', 'content' => array( 'This reservation has been successfully added to ' . $type . ' list' ) ) );

                    header( 'Location:' . get_state_url( 'transport&sub=' . $type . '-guest' . ( empty( $param ) ? '' : '&prm=' . $param ) ) );

                    exit;
                }
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Execute Edit Trip Transport
| -------------------------------------------------------------------------------------
*/
function run_edit_trip_transport( $type = '' )
{
    if( is_save_draft() || is_publish() )
    {
        $error = validate_trip_transport_data( $type );

        if( empty( $error ) )
        {
            if( update_trip_transport() )
            {
                return array( 'type'=> 'success', 'content' => array( 'This ' . $type . ' list has been successfully edited' ) );
            }
            else
            {
                return array( 'type'=> 'error', 'content' => array( 'Sorry can\'t edit this ' . $type . ' list' ) );
            }
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Validate Trip Transport
| -------------------------------------------------------------------------------------
*/
function validate_trip_transport_data( $type = '' )
{
    $error = array();

    extract( $_POST );

    if( isset( $vid ) && empty( $vid ) )
    {
        $error[] = 'Transport vehicle can\'t be empty';
    }

    if( isset( $did ) && empty( $did ) )
    {
        $error[] = 'Transport driver can\'t be empty';
    }

    if( isset( $vid ) && !empty( $vid ) && isset( $did ) && !empty( $did ) )
    {
        $dv = get_trip_driver( $type, $ttlocation, $vid, $ttdate, $tttime );

        if( !empty( $dv['did'] ) && $dv['did'] != $did )
        {
            $error[] = 'The driver for this trip and vehicle already been taken by <b>"' . $dv['dname'] . '"</b>';
        }
    }

    if( isset( $bid ) && empty( $bid ) )
    {
        $error[] = 'Guest can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Save Trip Transport
| -------------------------------------------------------------------------------------
*/
function save_trip_transport( $type = '' )
{
    global $db;

    $date = date( 'Y-m-d', strtotime( $_POST['ttdate'] ) );
    $time = date( 'H:i:s', strtotime( $_POST['tttime'] ) );

    $s = 'SELECT * FROM ticket_trip_transport WHERE vid = %d AND ttdate = %s AND type = %s AND ttlocation = %s AND tttime = %s';
    $q = $db->prepare_query( $s, $_POST['vid'], $date, $type, $_POST['ttlocation'], $time );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        update_trip_transport_status( $d['ttid'], $_POST['status'] );
        save_trip_transport_detail( $d['ttid'], $type );

        save_log( $d['ttid'], 'trip-transport', 'Add new trip transport #' . $d['ttid'] );

        return $d['ttid'];
    }
    else
    {
        $s = 'INSERT INTO ticket_trip_transport(
                vid,
                did,
                type,
                ttdate,
                ttlocation,
                tttime,
                tttype,
                ttfee,
                ttstatus,
                ttcreateddate,
                luser_id )
              VALUES( %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %d )';
        $q = $db->prepare_query( $s,
                $_POST['vid'],
                $_POST['did'],
                $type,
                $date,
                $_POST['ttlocation'],
                $time,
                $_POST['tttype'],
                $_POST['ttfee'],
                $_POST['status'],
                date( 'Y-m-d H:i:s' ),
                $_COOKIE['user_id'] );
        $r = $db->do_query( $q );

        if( !is_array( $r ) )
        {
            $id = $db->insert_id();

            save_trip_transport_detail( $id, $type );

            save_log( $id, 'trip-transport', 'Add new trip transport #' . $id );

            return $id;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Trip Transport Detail
| -------------------------------------------------------------------------------------
*/
function save_trip_transport_detail( $ttid, $type )
{
    global $db;

    foreach( $_POST['btid'] as $btid )
    {
        $s = 'INSERT INTO ticket_trip_transport_detail( ttid, btid, ttdtype ) VALUES( %d, %d, %s )';
        $q = $db->prepare_query( $s, $ttid, $btid, $type );

        $db->do_query( $q );
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Trip Transport
| -------------------------------------------------------------------------------------
*/
function update_trip_transport()
{
    global $db;

    $o = '';

    if( isset( $_POST['vid'] ) )
    {
       $o .= $db->prepare_query( ' vid = %d,', $_POST['vid'] );
    }

    if( isset( $_POST['vid'] ) )
    {
       $o .= $db->prepare_query( ' did = %d,' , $_POST['did'] );
    }

    if( isset( $_POST['ttfee'] ) )
    {
       $o .= $db->prepare_query( ' ttfee = %d,' , $_POST['ttfee'] );
    }

    $s = 'UPDATE ticket_trip_transport SET ' . $o . ' ttstatus = %s WHERE ttid = %d';
    $q = $db->prepare_query( $s, $_POST['status'], $_POST['ttid'] );

    return $db->do_query( $q );
}

/*
| -------------------------------------------------------------------------------------
| Update Trip Transport Status
| -------------------------------------------------------------------------------------
*/
function update_trip_transport_status( $ttid, $status = '0' )
{
    global $db;

    $s = 'UPDATE ticket_trip_transport SET ttstatus = %s WHERE ttid = %d';
    $q = $db->prepare_query( $s, $status, $ttid );

    return $db->do_query( $q );
}

/*
| -------------------------------------------------------------------------------------
| Delete Trip Transport By ID
| -------------------------------------------------------------------------------------
*/
function delete_trip_transport( $ttid )
{
    global $db;

    $s = 'DELETE FROM ticket_trip_transport WHERE ttid = %d';
    $q = $db->prepare_query( $s, $ttid );

    if( $db->do_query( $q ) )
    {
        save_log( $ttid, 'trip-transport', 'Delete trip transport #' . $ttid );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Trip Transport Detail By ID
| -------------------------------------------------------------------------------------
*/
function delete_trip_transport_detail( $ttdid )
{
    global $db;

    $s = 'DELETE FROM ticket_trip_transport_detail WHERE ttdid = %d';
    $q = $db->prepare_query( $s, $ttdid );

    return $db->do_query( $q );
}

/*
| -------------------------------------------------------------------------------------
| Get Trip By ID
| -------------------------------------------------------------------------------------
*/
function get_trip_transport( $ttid='', $type = '', $field = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_trip_transport AS a
          LEFT JOIN ticket_transport_vehicle AS b ON a.vid = b.vid
          LEFT JOIN ticket_transport_driver AS c ON a.did = c.did
          WHERE a.ttid = %d';
    $q = $db->prepare_query( $s, $ttid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $s2 = 'SELECT
            a.ttdid,
            d.bbname,
            d.agid,
            b.hid,
            b.bthotelname,
            b.bthoteladdress,
            b.bthotelphone,
            c.bdid,
            c.boid,
            c.rid,
            c.num_adult,
            c.num_child,
            c.num_infant,
            d.bid,
            e.taname,
            f.boname,
            g.rname
           FROM ticket_trip_transport_detail AS a
           LEFT JOIN ticket_booking_transport AS b ON a.btid = b.btid
           LEFT JOIN ticket_booking_detail AS c ON b.bdid = c.bdid
           LEFT JOIN ticket_booking AS d ON c.bid = d.bid
           LEFT JOIN ticket_transport_area AS e ON b.taid = e.taid
           LEFT JOIN ticket_boat AS f ON c.boid = f.boid
           LEFT JOIN ticket_route AS g ON c.rid = g.rid
           WHERE d.bstt <> "ar" AND a.ttid = %d AND b.bttype = %s';
    $q2 = $db->prepare_query( $s2, $ttid, $type );
    $r2 = $db->do_query( $q2 );

    $boname = array();
    $rname  = array();

    while( $d2 = $db->fetch_array( $r2 ) )
    {
        $d['detail'][] = $d2;

        $boname[] = $d2['boname'];
        $rname[]  = $d2['rname'];
    }

    $d['boname'] = implode( ', ', array_unique( $boname ) );
    $d['rname']  = implode( ', ', array_unique( $rname ) );

    if( !empty( $field ) && isset( $d[$field] ) )
    {
        return $d[$field];
    }
    else
    {
        return $d;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Trip Driver
| -------------------------------------------------------------------------------------
*/
function get_trip_driver( $type, $location, $vid, $date, $time )
{
    global $db;

    $s = 'SELECT * FROM ticket_trip_transport AS a
          INNER JOIN ticket_transport_driver AS b ON a.did = b.did
          WHERE a.vid = %d AND a.ttdate = %s AND a.type = %s AND a.ttlocation = %s AND a.tttime = %s';
    $q = $db->prepare_query( $s, $vid, date( 'Y-m-d', strtotime( $date ) ), $type, $location, date( 'H:i:s', strtotime( $time ) ) );
    $r = $db->do_query( $q );

    return $db->fetch_array( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Trip Pickup Drop Area
| -------------------------------------------------------------------------------------
*/
function get_trip_pickup_drop_area( $ttid, $type )
{
    global $db;

    $s = 'SELECT c.taname
          FROM ticket_trip_transport_detail AS a
          LEFT JOIN ticket_booking_transport AS b ON a.btid = b.btid
          LEFT JOIN ticket_transport_area AS c ON b.taid = c.taid
          WHERE a.ttid = %d AND b.bttype = %s';
    $q = $db->prepare_query( $s, $ttid, $type );
    $r = $db->do_query( $q );

    $area = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $area[] = $d['taname'];
    }

    return implode( ', ', array_unique( $area ) );
}

/*
| -------------------------------------------------------------------------------------
| Transport Vehicle Option
| -------------------------------------------------------------------------------------
*/
function get_trip_transport_vehicle_option( $type, $loc, $vid, $date, $time, $status='' )
{
    global $db;

    $w = '';

    if ( $status != '' )
    {
        $w = $db->prepare_query( ' WHERE vstatus = %s', $status );
    }

    $s = 'SELECT vid, vname FROM ticket_transport_vehicle' . $w . ' ORDER BY vname ASC';
    $q = $db->prepare_query( $s, $status );
    $r = $db->do_query( $q );

    $option = '<option value="">Select Vehicle</option>';

    while( $d = $db->fetch_array( $r ) )
    {
        $s2 = 'SELECT ttstatus FROM ticket_trip_transport WHERE vid = %s AND ttdate = %s AND type = %s AND ttlocation = %s AND tttime = %s';
        $q2 = $db->prepare_query( $s2, $d['vid'], date( 'Y-m-d', strtotime( $date ) ), $type, $loc, date( 'H:i:s', strtotime( $time ) ) );
        $r2 = $db->do_query( $q2 );
        $d2 = $db->fetch_array( $r2 );

        if( $d2['ttstatus'] == '' || $d2['ttstatus'] == '0' )
        {
            $option .= '<option value="' . $d['vid'] . '" ' . ( $d['vid'] == $vid ? 'selected' : '' ) . ' >' . $d['vname'] . '</option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Number of Transport Passenger
| -------------------------------------------------------------------------------------
*/
function get_trip_transport_passenger( $type, $location, $vid, $date, $time )
{
    global $db;

    $s = 'SELECT ( d.num_child + d.num_infant + d.num_adult ) AS num
          FROM ticket_trip_transport AS a
          LEFT JOIN ticket_trip_transport_detail AS b ON b.ttid = a.ttid
          LEFT JOIN ticket_booking_transport AS c ON b.btid = c.btid
          LEFT JOIN ticket_booking_detail AS d ON c.bdid = d.bdid
          LEFT JOIN ticket_booking AS e ON d.bid = e.bid
          WHERE e.bstt <> "ar" AND a.vid = %d AND a.ttdate = %s AND a.type = %s AND a.ttlocation = %s AND a.tttime = %s
          GROUP BY b.ttdid';
    $q = $db->prepare_query( $s, $vid, date( 'Y-m-d', strtotime( $date ) ), $type, $location, date( 'H:i:s', strtotime( $time ) ) );
    $r = $db->do_query( $q );

    $num = 0;

    while( $d = $db->fetch_array( $r ) )
    {
        $num = $num + $d['num'];
    }

    return $num;
}

/*
| -------------------------------------------------------------------------------------
| Get Filter
| -------------------------------------------------------------------------------------
*/
function get_filter_guest()
{
    if( isset( $_GET['prm'] ) )
    {
        extract( json_decode( base64_decode( $_GET['prm'] ), true ) );

        if( isset( $filter ) || isset( $add_trip_transport ) )
        {
            $filter  = array( 'taid' => $taid, 'rid' => $rid, 'lcid' => $lcid, 'trans_type' => $trans_type, 'bddate' => $bddate, 'search' => $s );
        }
        else
        {
            $filter = array( 'taid' => '', 'rid' => '', 'lcid' => '', 'trans_type' => '', 'bddate' => date( 'd F Y' ), 'search' => '' );
        }
    }
    elseif( !empty( $_POST ) )
    {
        extract( $_POST );

        $filter  = array( 'taid' => $taid, 'rid' => $rid, 'lcid' => $lcid, 'trans_type' => $trans_type, 'bddate' => $bddate, 'search' => $s );
    }
    else
    {
        $filter = array( 'taid' => '', 'rid' => '', 'lcid' => '', 'trans_type' => '', 'bddate' => date( 'd F Y' ), 'search' => '' );
    }

    return $filter;
}

function get_filter_list()
{
    if( isset( $_GET['prm'] ) )
    {
        extract( json_decode( base64_decode( $_GET['prm'] ), true ) );

        $filter = array( 'rid' => $rid, 'lcid' => $lcid, 'vid' => $vid, 'did' => $did, 'trans_type' => $trans_type, 'bddate' => $bddate, 'search' => $s, 'taid' => $taid  );
    }
    elseif( !empty( $_POST ) )
    {
        extract( $_POST );

        $filter = array( 'rid' => $rid, 'lcid' => $lcid, 'vid' => $vid, 'did' => $did, 'trans_type' => $trans_type, 'bddate' => $bddate, 'search' => $s, 'taid' => $taid  );
    }
    else
    {
        $filter = array( 'rid' => '', 'lcid' => '', 'vid' => '', 'did' => '', 'trans_type' => '', 'bddate' => date( 'd/m/Y' ) . ' - ' . date( 'd/m/Y' ), 'search' => '', 'taid' => '' );
    }

    return $filter;
}

/*
| -------------------------------------------------------------------------------------
| Get Today Pickup - Drop off List Table Data
| -------------------------------------------------------------------------------------
*/
function ticket_today_transport( $type )
{
    global $db;

    $s = 'SELECT
            a.ttid,
            a.type,
            a.tttime,
            b.vid,
            b.vname,
            c.dname,
            a.ttdate,
            b.vpassenger,
            a.ttlocation,
            a.tttype,
            a.ttstatus
          FROM ticket_trip_transport AS a
          LEFT JOIN ticket_transport_vehicle AS b ON a.vid = b.vid
          LEFT JOIN ticket_transport_driver AS c ON a.did = c.did
          WHERE a.type = %s AND a.ttdate = %s ORDER BY a.ttid DESC LIMIT 5';
    $q = $db->prepare_query( $s, $type, date( 'Y-m-d' ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) == 0 )
    {
        return '<tr><td colspan="8"><p class="text-center text-danger">No data found</p></td>';
    }
    else
    {
        $rows = '';

        while( $d = $db->fetch_array( $r ) )
        {
            $rows .= '
            <tr id="item-' . $d['ttid'] . '">
                <td class="vname">' . $d['vname'] . '</td>
                <td class="ttdate">' . date( 'd M Y', strtotime( $d['ttdate'] ) ) . '</td>
                <td class="dname">' . $d['dname'] . '</td>
                <td class="passenger">
                    ' . get_trip_transport_passenger( $d['type'], $d['ttlocation'], $d['vid'], $d['ttdate'], $d['tttime'] ) . '/' . $d['vpassenger'] . '
                </td>
                <td class="area">' . $d['ttlocation'] . '</td>
                <td class="transport">' . $d['tttype'] . '</td>
                <td class="status">' . (  $d['ttstatus'] == '1' ? 'Delivered' : 'Open' ) . '</td>
            </tr>';
        }

        return $rows;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Trip Status Option
| -------------------------------------------------------------------------------------
*/
function get_trip_status_option( $status = '' )
{
    return '
    <option value="0" ' . ( $status=='0' ? 'selected' : '' ) . ' >Open</option>
    <option value="1" ' . ( $status=='1' ? 'selected' : '' ) . ' >Delivered</option>';
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Type Option
| -------------------------------------------------------------------------------------
*/
function get_transport_type_option( $value = '', $use_value = false )
{
    if( $use_value )
    {
        return '
        <option value="" ' . ( $value == '' ? 'selected' : '' ) . '>Transport Type</option>
        <option value="Shared" ' . ( $value == 'Shared' ? 'selected' : '' ) . '>Shared Transport</option>
        <option value="Private" ' . ( $value == 'Private' ? 'selected' : '' ) . '>Private Transport</option>';
    }
    else
    {
        return '
        <option value="" ' . ( $value == '' ? 'selected' : '' ) . '>Transport Type</option>
        <option value="0" ' . ( $value == '0' ? 'selected' : '' ) . '>Shared Transport</option>
        <option value="1" ' . ( $value == '1' ? 'selected' : '' ) . '>Private Transport</option>';
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Pickup Drop Type Option
| -------------------------------------------------------------------------------------
*/
function get_pickup_drop_option( $type = '', $use_empty = true, $empty_text = 'All' )
{
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    $option .= '
    <option value="pickup" ' . ( $type=='pickup' ? 'selected' : '' ) . ' >Pickup</option>
    <option value="drop-off" ' . ( $type=='drop-off' ? 'selected' : '' ) . ' >Drop-off</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Check if page is trip transport added form
| -------------------------------------------------------------------------------------
*/
function is_add_trip_transport()
{
    if( isset( $_POST['add_trip_transport'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Check if page is trip transport added more form
| -------------------------------------------------------------------------------------
*/
function is_add_more_trip_transport()
{
    if( isset( $_POST['add_more_trip_transport'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Check if page is trip transport added more
| -------------------------------------------------------------------------------------
*/
function is_add_more_guest()
{
    if( isset( $_POST['add_more_guest'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Check if page used filter
| -------------------------------------------------------------------------------------
*/
function is_transport_filter_view()
{
    if( isset( $_GET['state'] ) && $_GET['state']=='transport' && isset( $_GET['prm'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Check if transport still opened for specific vehicle and date
| -------------------------------------------------------------------------------------
*/
function is_opened_transport( $vid, $date )
{
    global $db;

    $s = 'SELECT ttstatus FROM ticket_trip_transport WHERE vid = %s AND ttdate = %s';
    $q = $db->prepare_query( $s, $vid, date('Y-m-d', strtotime( $date ) ) );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( $d['ttstatus'] == '0' || empty( $d['ttstatus'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Check if reservation exist in transport list
| -------------------------------------------------------------------------------------
*/
function is_exist_in_transport( $btid = array(), $type )
{
    global $db;

    $s = 'SELECT btid FROM ticket_trip_transport_detail WHERE ttdtype = %s AND btid IN ( ' . implode( ',', $btid ) . ' )';
    $q = $db->prepare_query( $s, $type );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Check if reservation valid for added in transport list
| -------------------------------------------------------------------------------------
*/
function is_valid_trip( $data )
{
    foreach( $data as $d )
    {
        list( $btid[], $from[], $date[], $time[], $type[] ) = explode( '|', $d );
    }

    $num_from = count( array_count_values( $from ) );
    $num_date = count( array_count_values( $date ) );
    $num_time = count( array_count_values( $time ) );
    $num_type = count( array_count_values( $type ) );

    if( $num_from == 1 &&  $num_date == 1 && $num_time == 1 && $num_type == 1 )
    {
        return $btid;
    }
}

function get_transport_capacity( $type, $loc, $vid, $date, $time )
{
    $capacity  = get_vehicle_capacity( $vid );
    $passenger = get_trip_transport_passenger( $type, $loc, $vid, $date, $time );

    return $passenger . '/' . $capacity;
}

/*
| -------------------------------------------------------------------------------------
| GET DRIVER NAME AND VEHICLE NAME
| -------------------------------------------------------------------------------------
*/
function get_trip_transport_detail( $btid )
{
	global $db;
	
	$s = 'SELECT
	  	  d.dname,
	  	  e.vname
	  	  FROM ticket_booking_transport as a
	  	  LEFT JOIN ticket_trip_transport_detail as b ON a.btid = b.btid
	  	  LEFT JOIN ticket_trip_transport as c ON b.ttid = c.ttid
	  	  LEFT JOIN ticket_transport_driver as d ON c.did = d.did
	  	  LEFT JOIN ticket_transport_vehicle as e ON e.vid = c.vid
	  	  WHERE a.btid = %s';
	$q = $db->prepare_query( $s, $btid );
	$r = $db->do_query( $q );
	$d = $db->fetch_array( $r );
	
	return $d;
}

/*
| -------------------------------------------------------------------------------------
| Transport Ajax Request
| -------------------------------------------------------------------------------------
*/
function ticket_transport_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    extract( $_POST );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $pkey ) && $pkey == 'add-trip-transport' )
    {
        echo ticket_add_trip_transport( $type );
    }

    if( isset( $pkey ) && $pkey == 'get-transport-capacity' )
    {
        echo json_encode( array( 'result' => 'success', 'data' => get_transport_capacity( $type, $loc, $vid, $date, $time ) ) );
    }

    if( isset( $pkey ) && $pkey == 'delete-trip-transport-detail' )
    {
        if( delete_trip_transport_detail( $ttdid ) )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"failed"}';
        }
    }

    if( isset( $pkey ) && $pkey == 'delete-trip-transport' )
    {
        if( delete_trip_transport( $ttid ) )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"failed"}';
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-guest-data' )
    {
        extract( $_POST );

        if( $type == 'pickup' )
        {
            $data = ticket_pickup_guest_table_query( $taid, $trans_type, $bddate, $rid, $lcid );
        }
        else
        {
            $data = ticket_drop_off_guest_table_query( $taid, $trans_type, $bddate, $rid, $lcid );
        }

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-more-guest-data' )
    {
        extract( $_POST );

        if( $type == 'pickup' )
        {
            $data = ticket_pickup_more_guest_table_query( $ttid, $trans_type, $date, $time );
        }
        else
        {
            $data = ticket_drop_off_more_guest_table_query( $ttid, $trans_type, $date, $time );
        }

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-transport-list-data' )
    {
        extract( $_POST );

        if( $type == 'pickup' )
        {
            $data = ticket_pickup_list_table_query( $rid, $lcid, $vid, $did, $trans_type, $bddate, $taid );
        }
        else
        {
            $data = ticket_drop_off_list_table_query( $rid, $lcid, $vid, $did, $trans_type, $bddate, $taid );
        }

        echo json_encode( $data );
    }
}

?>