<?php

add_actions( 'availability', 'ticket_availability' );
add_actions( 'ticket-availability-ajax_page', 'ticket_availability_ajax' );
add_actions( 'ticket-admin-booking-process_page', 'ticket_booking_process', 'admin' );
add_actions( 'ticket-agent-booking-process_page', 'ticket_booking_process', 'agent' );
add_actions( 'ticket-front-booking-process_page', 'ticket_booking_process' );

function ticket_availability()
{
	if( is_check_availability() )
	{
		return ticket_check_availability_result();
	}
	elseif( is_review_booking() )
	{
		return ticket_review_booking();
	}
	elseif( is_detail_booking() )
	{
		return ticket_detail_booking();
	}

    return ticket_check_availability();
}

function ticket_check_availability()
{
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/availability/form.html', 'availability' );
    add_block( 'availability-block', 'ablock', 'availability' );

    add_variable( 'site_url', site_url() );
    add_variable( 'destination_list_from', get_availibility_loc_option(null, null, 'from') );
    add_variable( 'destination_list_to', get_availibility_loc_option(null, null, 'to') );
    add_variable( 'destination_list_from_return', get_availibility_loc_option(null, null, 'to') );
    add_variable( 'destination_list_to_return', get_availibility_loc_option(null, null, 'from') );
    add_variable( 'booking_source_list', get_booking_source_option() );
    add_variable( 'seat_avaibility_calendar', get_seat_availibility_by_schedule() );

    add_variable( 'ajax_url', HTSERVER . $site_url .'/ticket-availability-ajax' );
    add_variable( 'action', HTSERVER . $site_url .'/ticket-admin-booking-process' );

    parse_template( 'availability-block', 'ablock', false );

    add_actions( 'section_title', 'Check Availability' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/slick.min.css' );

    return return_template( 'availability' );
}

function ticket_check_availability_result()
{
    $site_url = site_url();
    $booking  = booking_item();

    //-- Redirect If empty booking
	if( !isset( $booking['sess_id'] ) || !isset( $booking['sess_data'] ) )
	{
		header( 'location:' . get_state_url( 'reservation&sub=availability' ) );
	}

	extract( $booking['sess_data'] );

    //-- Redirect If date of depart greater than current date
    if( strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
    {
        header( 'location:' . get_state_url( 'reservation&sub=availability' ) );
    }

	set_template( PLUGINS_PATH . '/ticket/tpl/availability/search-result.html', 'availability' );
    add_block( 'search-result-block', 'srblock', 'availability' );

    add_variable( 'sess_id', $booking['sess_id'] );

    if( empty( $date_of_return ) )
    {
    	add_variable( 'return_css', 'sr-only' );
    }
    else
    {
    	add_variable( 'return_css', '' );
    	add_variable( 'return_port', $return_port );
    	add_variable( 'return_date', $date_of_return );
        add_variable( 'destination_port_rtn', $destination_port_rtn );
        add_variable( 'return_date_field', date( 'd F Y', strtotime( $date_of_return ) ) );
    	add_variable( 'return_date_format', date( 'D j F Y', strtotime( $date_of_return ) ) );
    	add_variable( 'return_availability_result', get_return_availability_result( $booking['sess_data'], $agid , $chid ) );
    	add_variable( 'return_route', get_location( $return_port, 'lcname' ) . ' to ' . get_location( $destination_port_rtn, 'lcname' ) );
    }

    if( empty( $type_of_route ) )
    {
        add_variable( 'one_way_css', 'checked' );
        add_variable( 'return_way_css', '' );

        add_variable( 'return_date_css', 'disabled');
        add_variable( 'return_port_css', 'disabled' );
        add_variable( 'destination_to_port_css', 'disabled' );
    }
    else
    {
        add_variable( 'one_way_css', '' );
        add_variable( 'return_way_css', 'checked' );

        add_variable( 'return_date_css', '');
        add_variable( 'return_port_css', '' );
        add_variable( 'destination_to_port_css', '' );
    }

    add_variable( 'depart_port', $depart_port );
    add_variable( 'depart_date', $date_of_depart );
    add_variable( 'depart_date_field', date( 'd F Y', strtotime( $date_of_depart ) ) );
    add_variable( 'depart_date_format', date( 'D j F Y', strtotime( $date_of_depart ) ) );
    add_variable( 'departure_availability_result', get_departure_availability_result( $booking['sess_data'], $agid , $chid) );
    add_variable( 'depart_route', get_location( $depart_port, 'lcname' ) . ' to ' . get_location( $destination_port, 'lcname' ) );

    add_variable( 'chid', $chid );
    add_variable( 'agid', $agid );
    add_variable( 'adult_num', $adult_num );
    add_variable( 'child_num', $child_num );
    add_variable( 'infant_num', $infant_num );
    add_variable( 'type_of_route', $type_of_route );
    add_variable( 'destination_port', $destination_port );

    add_variable( 'booking_source_list', get_booking_source_option( $chid, $agid ) );
    add_variable( 'depart_port_list', get_availibility_loc_option( 'From', $depart_port, 'from' ) );
    add_variable( 'destination_port_list', get_availibility_loc_option( 'To', $destination_port, 'to' ) );
    add_variable( 'return_port_list', get_availibility_loc_option( 'Return From', $return_port, 'to' ) );
    add_variable( 'destination_to_port_list', get_availibility_loc_option( 'Return To', $destination_port_rtn, 'from' ) );

    add_variable( 'action', HTSERVER . $site_url .'/ticket-admin-booking-process' );

    parse_template( 'search-result-block', 'srblock', false );

    add_actions( 'section_title', 'Search Availability Result' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'availability' );
}

function ticket_review_booking()
{
    $site_url = site_url();
    $booking  = booking_item();

    //-- Redirect If empty booking
	if( !isset( $booking['sess_id'] ) || !isset( $booking['sess_data'] ) )
	{
		header( 'location:' . get_state_url( 'reservation&sub=availability&step=search-result' ) );
	}

	extract( $booking['sess_data'] );

    //-- Redirect If empty trip
    if( empty( $trip ) )
    {
        header( 'location:' . get_state_url( 'reservation&sub=availability&step=search-result' ) );
    }

    //-- Redirect If date of depart greater than current date
    if( strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
    {
        header( 'location:' . get_state_url( 'reservation&sub=availability' ) );
    }

	set_template( PLUGINS_PATH . '/ticket/tpl/availability/review.html', 'availability' );
    add_block( 'review-block', 'rvblock', 'availability' );

    add_variable( 'sess_id', $booking['sess_id'] );

    //-- Check if have error data
    if( isset( $_GET['error'] ) )
    {
        add_variable( 'message', generate_message_block( json_decode( base64_decode( $_GET['error'] ), true ) ) );
    }

    if( isset( $trip['return'] ) )
    {
    	add_variable( 'return_css', '' );
    	add_variable( 'return_port', $return_port );
    	add_variable( 'return_date', $date_of_return );
        add_variable( 'destination_port_rtn', $destination_port_rtn );
    	add_variable( 'return_date_format', date( 'D j F Y', strtotime( $date_of_return ) ) );
    	add_variable( 'return_route', get_location( $return_port, 'lcname' ) . ' to ' . get_location( $destination_port_rtn, 'lcname' ) );

		add_variable( 'return_rid', $trip['return']['id'] );
		add_variable( 'return_boid', $trip['return']['boid'] );
    	add_variable( 'return_price', $trip['return']['total'] );
    	add_variable( 'return_boat_name', get_boat( $trip['return']['boid'], 'boname' ) );

	    if( $trip['return']['discount'] > 0 )
	    {
			add_variable( 'return_price_disc_num', '<span class="disc-price">' . number_format( $trip['return']['total'], 0, ',', '.' ) . '</span>' );
	    	add_variable( 'return_price_num', number_format( ( $trip['return']['total'] - $trip['return']['discount'] ), 0, ',', '.' ) );
	    }
	    else
	    {
	    	add_variable( 'return_price_num', number_format( $trip['return']['total'], 0, ',', '.' ) );
	    }

    	add_variable( 'return_trans', get_route_detail_transport( $trip['return']['transport']  ) );
		add_variable( 'return_passenger_number', get_passenger_num_content( $adult_num, $child_num, $infant_num ) );
		add_variable( 'return_route_list', get_route_detail_list_content( $trip['return']['id'], $return_port, $destination_port_rtn ) );
		add_variable( 'return_rate_price', get_rate_price_content( $trip['return']['id'], $adult_num, $child_num, $infant_num, $trip['return'], true ) );
        add_variable( 'return_dropoff_transport', get_pickup_drop_off_transport( $trip['return']['id'], $return_port, $destination_port_rtn, $trip['return']['transport'], $booking['sess_data'], true, true ) );
        add_variable( 'return_dropoff_transport_price', get_pickup_drop_off_transport_price( $trip['return']['id'], $return_port, $destination_port_rtn, $trip['return']['transport'], $booking['sess_data'], true ) );
    }
    else
    {
        add_variable( 'return_css', 'sr-only' );
    }

    add_variable( 'depart_port', $depart_port );
    add_variable( 'depart_date', $date_of_depart );
    add_variable( 'depart_date_format', date( 'D j F Y', strtotime( $date_of_depart ) ) );
    add_variable( 'depart_route', get_location( $depart_port, 'lcname' ) . ' to ' . get_location( $destination_port, 'lcname' ) );

	add_variable( 'depart_rid', $trip['departure']['id'] );
	add_variable( 'depart_boid', $trip['departure']['boid'] );
	add_variable( 'depart_price', $trip['departure']['total'] );
    add_variable( 'depart_boat_name', get_boat( $trip['departure']['boid'], 'boname' ) );

    if( $trip['departure']['discount'] > 0 )
    {
		add_variable( 'depart_price_disc_num', '<span class="disc-price">' . number_format( $trip['departure']['total'], 0, ',', '.' ) . '</span>' );
		add_variable( 'depart_price_num', number_format( ( $trip['departure']['total'] - $trip['departure']['discount'] ), 0, ',', '.' ) );
    }
    else
    {
		add_variable( 'depart_price_num', number_format( $trip['departure']['total'], 0, ',', '.' ) );
    }

	add_variable( 'depart_trans', get_route_detail_transport( $trip['departure']['transport'] ) );
	add_variable( 'depart_passenger_number', get_passenger_num_content( $adult_num, $child_num, $infant_num ) );
	add_variable( 'depart_route_list', get_route_detail_list_content( $trip['departure']['id'], $depart_port, $destination_port ) );
	add_variable( 'depart_rate_price', get_rate_price_content( $trip['departure']['id'], $adult_num, $child_num, $infant_num, $trip['departure'] ) );
    add_variable( 'depart_pickup_transport', get_pickup_drop_off_transport( $trip['departure']['id'], $depart_port, $destination_port, $trip['departure']['transport'], $booking['sess_data'], false, true ) );
    add_variable( 'depart_pickup_transport_price', get_pickup_drop_off_transport_price( $trip['departure']['id'], $depart_port, $destination_port, $trip['departure']['transport'], $booking['sess_data'] ) );

    add_variable( 'discount_css', empty( $promo_code ) ? 'sr-only' : '' );
    add_variable( 'freelance_code_css', empty( $freelance_code ) ? 'sr-only' : '' );
    add_variable( 'promo_code', empty( $promo_code ) ? '-' : 'Promo Code : ' . $promo_code );
    add_variable( 'discount_price_num', empty( $discount ) ? '' : number_format( $discount, 0, ',', '.' ) );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'type_of_route', $type_of_route );
    add_variable( 'destination_port', $destination_port );
    add_variable( 'subtotal_num', number_format ( $subtotal, 0, ',', '.' ) );
    add_variable( 'grand_total_price', number_format ( $grandtotal, 0, ',', '.' ) );

    add_variable( 'action', HTSERVER . $site_url .'/ticket-admin-booking-process' );
    add_variable( 'ajax_link', HTSERVER . $site_url .'/ticket-availability-ajax/' );

    parse_template( 'review-block', 'rvblock', false );

    add_actions( 'section_title', 'Review Booking' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'availability' );
}

function ticket_detail_booking()
{
    $site_url = site_url();
    $booking  = booking_item();

    //-- Redirect If empty session booking
	if( !isset( $booking['sess_id'] ) || !isset( $booking['sess_data'] ) )
	{
		header( 'location:' . get_state_url( 'reservation&sub=availability&step=review' ) );
	}

    extract( $booking['sess_data'] );

    //-- Redirect If empty trip
    if( empty( $trip ) )
    {
        header( 'location:' . get_state_url( 'reservation&sub=availability&step=search-result' ) );
    }

    //-- Redirect If date of depart greater than current date
    if( strtotime( $date_of_depart ) < strtotime( date( 'Y-m-d' ) ) )
    {
        header( 'location:' . get_state_url( 'reservation&sub=availability' ) );
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/availability/detail.html', 'availability' );
    add_block( 'detail-block', 'dtblock', 'availability' );

    add_variable( 'sess_id', $booking['sess_id'] );

    if( isset( $trip['return'] ) )
    {
        $rd = get_route_detail_content( $trip['return']['id'], $return_port, $destination_port_rtn );

        add_variable( 'return_to_port', $rd['arrive'] );
        add_variable( 'return_etd', $rd['depart_time'] );
        add_variable( 'return_eta', $rd['arrive_time'] );
        add_variable( 'return_from_port', $rd['depart'] );
        add_variable( 'return_boat_name', get_boat( $trip['return']['boid'], 'boname' ) );
        add_variable( 'return_date_format', date( 'D j F Y', strtotime( $date_of_return ) ) );
        add_variable( 'return_transport', get_transport_data( $trip['return'], $trip['return']['transport'] ) );
        add_variable( 'return_passenger', get_passenger_num_detail_content( $adult_num, $child_num, $infant_num ) );
        add_variable( 'return_passenger_field_list', passenger_field_list_content( $adult_num, $child_num, $infant_num, true ) );
        add_variable( 'return_transport_detail', get_return_transport_detail( $trip['return'], $trip['return']['transport'] ) );
    }
    else
    {
        add_variable( 'return_css', 'sr-only' );
    }

    if( empty( $agid ) )
    {
        $pmethod = '
        <div class="form-group">
            <label>Choose Payment <small>*</small></label>
            <select class="select-option" name="payment">
                ' . get_payment_method_option( true ) . '
            </select>
        </div>';

        add_variable( 'full_name', '' );
        add_variable( 'phone', '' );
        add_variable( 'email', get_meta_data( 'default_booking_email', 'ticket_setting' ) );
    }
    else
    {
        $agent = get_agent( $agid );

        if( $agent['agpayment_type'] == 'Credit' )
        {
            $pmethod = '
            <div class="form-group no-border no-padding">
                <input type="text" class="sr-only" name="payment" value="8" />
            </div>';
        }
        else
        {
            $pmethod = '
            <div class="form-group">
                <label>Choose Payment <small>*</small></label>
                <select class="select-option" name="payment">
                    ' . get_payment_method_option( true ) . '
                </select>
            </div>';
        }

        add_variable( 'full_name', $agent['agname'] );
        add_variable( 'phone', $agent['agphone'] );
        add_variable( 'email', $agent['agemail'] );
    }

    $dd  = get_route_detail_content( $trip['departure']['id'], $depart_port, $destination_port );
    $var = get_variable_by_port_type( $depart_port, $destination_port );
    $aid = $var['drop_sts'] ? $dd['depart'] : $dd['arrive'];

    add_variable( 'depart_to_port', $dd['arrive'] );
    add_variable( 'depart_etd', $dd['depart_time'] );
    add_variable( 'depart_eta', $dd['arrive_time'] );
    add_variable( 'depart_from_port', $dd['depart'] );
    add_variable( 'depart_boat_name', get_boat( $trip['departure']['boid'], 'boname' ) );
    add_variable( 'depart_date_format', date( 'D j F Y', strtotime( $date_of_depart ) ) );
    add_variable( 'd_date',  date( 'Y-m-d', strtotime( $date_of_depart ) ) );
    add_variable( 'depart_transport', get_transport_data( $trip['departure'], $trip['departure']['transport'] ) );
    add_variable( 'depart_passenger', get_passenger_num_detail_content( $adult_num, $child_num, $infant_num ) );
    add_variable( 'depart_passenger_field_list', passenger_field_list_content( $adult_num, $child_num, $infant_num ) );
    add_variable( 'depart_transport_detail', get_depart_transport_detail( $trip['departure'], $trip['departure']['transport'] ) );

    add_variable( 'payment_method', $pmethod );
    add_variable( 'country_list_option', get_country_list_option() );
    add_variable( 'grand_total_price', number_format ( $grandtotal, 0, ',', '.' ) );

    add_variable( 'suggestion_accommodation_area', $aid );
    add_variable( 'suggestion_accommodation', get_suggestion_accommodation( $aid, $trip['departure'] ) );
    add_variable( 'action', HTSERVER . $site_url .'/ticket-admin-booking-process' );

    parse_template( 'detail-block', 'dtblock', false );

    add_actions( 'section_title', 'Detail Booking' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'availability' );
}

function get_suggestion_accommodation( $port_id, $trip = array() )
{
    global $db;

    $s = 'SELECT * FROM ticket_hotel AS a
          LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
          LEFT JOIN ticket_transport_area_fee AS c ON c.taid = b.taid
          WHERE a.hstatus = %d AND c.lcid = %d AND b.taairport <> %s ORDER BY RAND() LIMIT 1';
    $q = $db->prepare_query( $s, 1, $port_id, '1' );

    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $content = '
        <div class="form-group">
            <label>Hotel/Villa Name</label>
            <input type="text" class="text form-control" name="hotel_name" value="' . $d['hname'] . '" />
        </div>
        <div class="form-group">
            <label>Hotel/Villa Address</label>
            <input type="text" class="text form-control" name="hotel_address" value="' . $d['haddress'] . '" />
        </div>
        <div class="form-group">
            <label>Hotel/Villa Phone</label>
            <input type="text" class="text form-control" name="hotel_phone" value="' . $d['hphone'] . '" />
        </div>
        <div class="form-group">
            <label>Hotel/Villa Email</label>
            <input type="text" class="text form-control" name="hotel_email" value="' . $d['hemail'] . '" />
        </div>';
    }
    else
    {
        $content = '
        <div class="form-group">
            <label>Hotel/Villa Name</label>
            <input type="text" class="text form-control" name="hotel_name" />
        </div>
        <div class="form-group">
            <label>Hotel/Villa Address</label>
            <input type="text" class="text form-control" name="hotel_address" />
        </div>
        <div class="form-group">
            <label>Hotel/Villa Phone</label>
            <input type="text" class="text form-control" name="hotel_phone" />
        </div>
        <div class="form-group">
            <label>Hotel/Villa Email</label>
            <input type="text" class="text form-control" name="hotel_email" />
        </div>';
    }

    return $content;
}

function passenger_field_list_content( $adult = 0, $child = 0, $infant = 0, $is_return = false )
{
	$content   = '';
	$trip_type = $is_return ? 'return' : 'departure';
    $admtype   = get_uri_by_part( 0 );

	if( !empty( $adult ) )
	{
		for( $i = 0; $i < $adult; $i++ )
		{
    		$content .= '
    		<li class="field-item">
				<h4>Adult ' . ( $i + 1 ) . '</h4>
				<div class="form-group full-name">
					<label>Full Name <small>*</small></label>
					<input type="text" class="text form-control required" name="gfullname[' . $trip_type . '][adult][' . ( $i + 1 ) . ']" />
				</div>
				<div class="form-group gender">
					<label>Gender <small>*</small></label>
					<div class="form-inline">
			            <div class="form-group">
			                <div class="radio">
			                    <input id="gender-' . $trip_type . '-adult-option-' . ( $i + 1 ) . '-1" value="1" name="ggender[' . $trip_type . '][adult][' . ( $i + 1 ) . ']" autocomplete="off" type="radio" checked />
			                    <label for="gender-' . $trip_type . '-adult-option-' . ( $i + 1 ) . '-1">Male</label>
			                </div>
			            </div>
			            <div class="form-group">
			                <div class="radio">
			                    <input id="gender-' . $trip_type . '-adult-option-' . ( $i + 1 ) . '-2" value="0" name="ggender[' . $trip_type . '][adult][' . ( $i + 1 ) . ']" autocomplete="off" type="radio" />
			                    <label for="gender-' . $trip_type . '-adult-option-' . ( $i + 1 ) . '-2">Female</label>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="form-group birthday">
					<label>Date of Birth</label>
					<input type="text" class="text form-control date" name="gbirth[' . $trip_type . '][adult][' . ( $i + 1 ) . ']" readonly />
				</div>
				<div class="form-group nationality">
					<label>Nationality <small>*</small></label>
					<select class="select-option required" name="gcountry[' . $trip_type . '][adult][' . ( $i + 1 ) . ']">
						' . get_country_list_option() . '
					</select>
				</div>
			</li>';
		}
	}

	if( !empty( $child ) )
	{
		for( $i = 0; $i < $child; $i++ )
		{
    		$content .= '
    		<li class="field-item">
				<h4>Child ' . ( $i + 1 ) . '</h4>
				<div class="form-group full-name">
					<label>Full Name <small>*</small></label>
					<input type="text" class="text form-control required" name="gfullname[' . $trip_type . '][child][' . ( $i + 1 ) . ']" />
				</div>
				<div class="form-group gender">
					<label>Gender <small>*</small></label>
					<div class="form-inline">
			            <div class="form-group">
			                <div class="radio">
			                    <input id="gender-' . $trip_type . '-child-option-' . ( $i + 1 ) . '-1" value="1" name="ggender[' . $trip_type . '][child][' . ( $i + 1 ) . ']" autocomplete="off" type="radio" checked />
			                    <label for="gender-' . $trip_type . '-child-option-' . ( $i + 1 ) . '-1">Male</label>
			                </div>
			            </div>
			            <div class="form-group">
			                <div class="radio">
			                    <input id="gender-' . $trip_type . '-child-option-' . ( $i + 1 ) . '-2" value="0" name="ggender[' . $trip_type . '][child][' . ( $i + 1 ) . ']" autocomplete="off" type="radio" />
			                    <label for="gender-' . $trip_type . '-child-option-' . ( $i + 1 ) . '-2">Female</label>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="form-group birthday">
					<label>Date of Birth</label>
					<input type="text" class="text form-control date-child" name="gbirth[' . $trip_type . '][child][' . ( $i + 1 ) . ']" readonly />
				</div>
				<div class="form-group nationality">
					<label>Nationality <small>*</small></label>
					<select class="select-option required" name="gcountry[' . $trip_type . '][child][' . ( $i + 1 ) . ']">
						' . get_country_list_option() . '
					</select>
				</div>
			</li>';
		}
	}

	if( !empty( $infant ) )
	{
		for( $i = 0; $i < $infant; $i++ )
		{
    		$content .= '
    		<li class="field-item">
				<h4>Infant ' . ( $i + 1 ) . '</h4>
				<div class="form-group full-name">
					<label>Full Name <small>*</small></label>
					<input type="text" class="text form-control required" name="gfullname[' . $trip_type . '][infant][' . ( $i + 1 ) . ']" />
				</div>
				<div class="form-group gender">
					<label>Gender <small>*</small></label>
					<div class="form-inline">
			            <div class="form-group">
			                <div class="radio">
			                    <input id="gender-' . $trip_type . '-infant-option-' . ( $i + 1 ) . '-1" value="1" name="ggender[' . $trip_type . '][infant][' . ( $i + 1 ) . ']" autocomplete="off" type="radio" checked />
			                    <label for="gender-' . $trip_type . '-infant-option-' . ( $i + 1 ) . '-1">Male</label>
			                </div>
			            </div>
			            <div class="form-group">
			                <div class="radio">
			                    <input id="gender-' . $trip_type . '-infant-option-' . ( $i + 1 ) . '-2" value="0" name="ggender[' . $trip_type . '][infant][' . ( $i + 1 ) . ']" autocomplete="off" type="radio" />
			                    <label for="gender-' . $trip_type . '-infant-option-' . ( $i + 1 ) . '-2">Female</label>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="form-group birthday">
					<label>Date of Birth</label>
					<input type="text" class="text form-control date-infant" name="gbirth[' . $trip_type . '][infant][' . ( $i + 1 ) . ']" readonly />
				</div>
				<div class="form-group nationality">
					<label>Nationality <small>*</small></label>
					<select class="select-option required" name="gcountry[' . $trip_type . '][infant][' . ( $i + 1 ) . ']">
						' . get_country_list_option() . '
					</select>
				</div>
			</li>';
		}
	}

	return $content;
}

function get_route_detail_content( $rid, $depart, $arrive )
{
	global $db;

    $ds = 'SELECT * FROM ticket_route_detail AS a LEFT JOIN ticket_location AS b ON a.lcid = b.lcid WHERE a.rid = %d AND b.lcid = %d';
    $dq = $db->prepare_query( $ds, $rid, $depart );
    $dr = $db->do_query( $dq );
    $dd = $db->fetch_array( $dr );

    $as = 'SELECT * FROM ticket_route_detail AS a LEFT JOIN ticket_location AS b ON a.lcid = b.lcid WHERE a.rid = %d AND b.lcid = %d';
    $aq = $db->prepare_query( $as, $rid, $arrive );
    $ar = $db->do_query( $aq );
    $ad = $db->fetch_array( $ar );

    $rt = get_route( $rid );

    if( $rt['rtype'] == '1' && !empty( $rt['rhoppingpoint'] )  )
    {
        if( $dd['lcid'] == $arrive && $dd['lcid'] == $rt['rhoppingpoint'] )
        {
            $dd['rdtime'] = $dd['rdtime2'];
        }

        if( $dd['lcid'] == $arrive && $ad['lcid'] == $rt['rhoppingpoint'] )
        {
            $ad['rdtime'] = $ad['rdtime2'];
        }
    }

    $data = array(
    	'depart' => $dd['lcname'],
    	'arrive' => $ad['lcname'],
        'depart_port_type' => $dd['lctype'],
        'arrive_port_type' => $ad['lctype'],
    	'depart_time' => date( 'H:i', strtotime( $dd['rdtime'] ) ),
    	'arrive_time' => date( 'H:i', strtotime( $ad['rdtime'] ) ),
    );

	return $data;
}

function get_route_detail_list_content_pdf( $rid, $depart, $arrive, $api = false )
{
	global $db;

    //-- Get Route Detail
    $s = 'SELECT
            a.lcid,
            b.lcname,
            a.rdtype,
            a.rdtime,
            a.rdtime2
          FROM ticket_route_detail AS a
		  INNER JOIN ticket_location AS b ON a.lcid = b.lcid
		  WHERE rid = %d ORDER BY rdid ASC';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );

    $hopid  = '';
    $dep    = false;
    $ret    = false;

    //-- Get Route
    $s2 = 'SELECT
            a.rtype,
            a.rhoppingpoint
          FROM ticket_route AS a
          WHERE a.rid = %d';
    $q2 = $db->prepare_query( $s2, $rid );
    $r2 = $db->do_query( $q2 );
    $d2 = $db->fetch_array( $r2 );

    if( $d2['rtype'] == '1' && !empty( $d2['rhoppingpoint'] ) )
    {
        $hopid = $d2['rhoppingpoint'];
    }

    if( $api )
    {
        $list = array();
    }
    else
    {
        $list = '';
    }

    $dnum  = 1;
    $anum  = 1;
    $count = 0;
    $class = '';

    while( $d = $db->fetch_array( $r ) )
    {
    	if( $d['lcid'] == $depart && $dep == false )
    	{
    		$style = '';
    		$dep   = true;
    		$class = "route-custom ";
    		$variable_depart = strtotime( $d['rdtime'] );
    		$variable_time_depart = date( 'H:i', strtotime( $d['rdtime2'] ) ) ;
    		add_variable('depart_time', $variable_time_depart );
    	}
    	elseif( $d['lcid'] == $arrive && $ret == false )
    	{
    		$style = '';
    		$ret   = true;
    		$class = "route-custom ";
    	}
    	else
    	{
    		if( $dep == false )
    		{
    			$style = 'style="text-decoration:line-through; display:none;"';
    		}
    		elseif( $dep == true && $ret == true )
    		{
    			$style = 'style="text-decoration:line-through; display:none;"';
    		}
    	}

        //-- Set Label
        if( $d['rdtype'] == '3' )
        {
            $lbl = 'Clearance';
        }
        else if( $d['rdtype'] == '2' )
        {
            //-- Get Location
            $s3 = 'SELECT a.lctype FROM ticket_location AS a WHERE a.lcid = %d';
            $q3 = $db->prepare_query( $s3, $arrive );
            $r3 = $db->do_query( $q3 );
            $d3 = $db->fetch_array( $r3 );

            if( in_array( $d3, array( 0, 2 ) ) )
            {
                $lbl = 'Arr. ' . $anum . ' - ';

                $anum++;
            }
            else
            {
                $lbl = 'Dep. ' . $dnum . ' - ';

                $dnum++;
            }
        }
        else if( $d['rdtype'] == '1' )
        {
            $lbl = 'Arr. ' . $anum . ' - ';

            $anum++;
        }
        else if( $d['rdtype'] == '0' )
        {
            $lbl = 'Dep. ' . $dnum . ' - ';

            $dnum++;
        }
        
        // get time different betwen route
        $time_before_depart = isset($time_before_depart) ? $time_before_depart: "";
        $time_before_arrival = isset($time_before_arrival) ? $time_before_arrival: "";
		if($time_before_depart == "" && $time_before_arrival == ""){
            $time_before_depart = $d['rdtime2'];
            $time_before_arrival = $d['rdtime'] ;
        }

        if( $count != 0 ){
			$t_arrival = strtotime( $d['rdtime'] );
	        $lastTime  = strtotime($time_before_depart);
	        $firstTime   = strtotime( $d['rdtime'] );
	        $timeDiff   = ($firstTime - $lastTime) / 60;
	        $list .= '<div style="padding-top:-2px; width:50px; text-align:center; border-bottom:1px solid rgb(0, 148, 160);" class= "different r-'.$count.'"' . $style . '><b style="font-size:9px;">'.$timeDiff.' Minutes</b></div>';
        }
        $time_before_depart = $d['rdtime2'];
        $time_before_arrival = $d['rdtime'] ;

				if($style == ""){
					if( $d['lcid'] == $hopid  )
					{
						$list	.= '<div style="display:flex; flex-direction:row; width:100px; flex-wrap:wrap;">';
						// $list	.= '<div style="width:100px; height:25px; display:flex; flex-direction:row;"><img class="circle-point" style="width:25px;height:25px; z-index:2;" src="https://www.bluewater-express.com/staging-rsv/l-admin/themes/custom/images/circle.png"/>';
						// $list	.= '</div>';
							if( $d['lcid'] == $arrive )
							{
									if( $api )
									{
											$list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime2'] ) ) );
									}
									else
									{
											$arrival_time = strtotime( $d['rdtime2'] );
											$list	.= '<div style="width:100px; display:flex; flex-direction:row; align-items:flex-end; justify-content:flex-end; padding-bottom:10px">
																	<div style="width:35px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
																	<div style="width:20px; height:20px; background-color:rgb(0, 148, 160); border-radius:50%;"></div>
																</div>';
											$list .= '<div style="background-color: rgb(255, 255, 255); border: 1px solid rgb(0, 148, 160); color:rgb(0, 148, 160);" class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> ETA ' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></div>';
											$list .= '<div style="background-color: rgb(0, 148, 160); border: 1px solid rgb(0, 148, 160); color:rgb(255, 255, 255);" class= "'.$class .'r-'.$count .'"' . $style . '><b style="font-size:8px;">' . $d['lcname'] . '</b> </div>';
											$list .= '</div>';
											// $list .= '<li ' . $style . '><b>' . $lbl . ' ' . $d['lcname'] . '</b> (' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . ')</li>';
									}
							}
							if( $lbl == "Clearance" )
							{
									$deapart_time = strtotime( $d['rdtime']);
									$list	.= '<div style="width:100px; display:flex; flex-direction:row; align-items:flex-end; justify-content:flex-end; padding-bottom:10px">
													<div style="width:35px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
													<div style="width:20px; height:20px; border:1px solid rgb(0, 148, 160); border-radius:50%;"></div>
													<div style="width:42.7px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
												</div>';
									$list .= '<div style="background-color: rgb(255, 255, 255); border: 1px solid rgb(0, 148, 160); color:rgb(0, 148, 160);" class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b style="font-size:9px;" class="lbl-arrival"> ETA ' . date( 'H:i', strtotime( $d['rdtime'] ) ) . ' / ETD ' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></div>';
									$list .= '<div style="background-color: rgb(0, 148, 160); border: 1px solid rgb(0, 148, 160); color:rgb(255, 255, 255);" class= "'.$class .'r-'.$count .'"' . $style . '><b style="font-size:8px;">' . $d['lcname'] . '</b> </div>';
									$list .= '</div>';
							}
							if($count != 0 && $arrive != $d['lcid'] && $lbl != "Clearance")
							{
									if( $api )
									{
											$list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime'] ) ) );
									}
									else
									{
								    if($count == 0){
    									$list	.= '<div style="width:100px; display:flex; flex-direction:row; align-items:flex-end; justify-content:flex-end; padding-bottom:10px">
    															<div style="width:20px; height:20px; background-color:rgb(0, 148, 160); border-radius:50%; margin-left:40px;"></div>
    															<div style="width:39.7px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
    														</div>';
    										$list .= '<div style="background-color: rgb(255, 255, 255); border: 1px solid rgb(0, 148, 160); color:rgb(0, 148, 160);" class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure" style="font-size:12px; font-weight:bold;"> ETD </b> <b style="font-size:12px; font-weight:bold;">'.date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></div>';
    										$list .= '<div style="background-color: rgb(0, 148, 160); border: 1px solid rgb(0, 148, 160); color:rgb(255, 255, 255);" class= "'.$class .'r-'.$count .'"' . $style . '><b style="font-size:8px;">' . $d['lcname'] . '</b> </div>';
    										$list	.= '</div>';
    								}
                                    else{
                                        $deapart_time = strtotime( $d['rdtime']);
                                                                        $list	.= '<div style="width:100px; display:flex; flex-direction:row; align-items:flex-end; justify-content:flex-end; padding-bottom:10px">
                                        												<div style="width:35px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
                                        												<div style="width:20px; height:20px; border:1px solid rgb(0, 148, 160); border-radius:50%;"></div>
                                        												<div style="width:42.7px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
                                        											</div>';
                                        								$list .= '<div style="background-color: rgb(255, 255, 255); border: 1px solid rgb(0, 148, 160); color:rgb(0, 148, 160);" class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b style="font-size:9px;" class="lbl-arrival"> ETA ' . date( 'H:i', strtotime( $d['rdtime'] ) ) . ' / ETD ' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></div>';
                                        								$list .= '<div style="background-color: rgb(0, 148, 160); border: 1px solid rgb(0, 148, 160); color:rgb(255, 255, 255);" class= "'.$class .'r-'.$count .'"' . $style . '><b style="font-size:8px;">' . $d['lcname'] . '</b> </div>';
                                        								$list .= '</div>';
                                    }
									}
							}
					}
					else
					{
							if( $api )
							{
									$list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime'] ) ) );
							}
							else
							{
									// $list .= '<li ' . $style . '><b>' . $lbl . ' ' . $d['lcname'] . '</b> (' . date( 'H:i', strtotime( $d['rdtime'] ) ) . ')</li>';
									$list	.= '<div style="display:flex; flex-direction:row; width:100px; flex-wrap:wrap;">';
									// $list	.= '<div style="width:100px; height:25px; display:flex; flex-direction:row;"><img class="circle-point" style="width:25px; height:25px; z-index:2;" src="https://www.bluewater-express.com/staging-rsv/l-admin/themes/custom/images/circle.png"/>';
									// $list	.= '<div style="width:50px; border-bottom:1px solid rgb(0, 148, 160); float:none;"></div></div>';
									if($count == 0){
										$list	.= '<div style="width:100px; display:flex; flex-direction:row; align-items:flex-end; justify-content:flex-end; padding-bottom:10px">
																<div style="width:20px; height:20px; background-color:rgb(0, 148, 160); border-radius:50%; margin-left:40px;"></div>
																<div style="width:39.7px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
															</div>';
											$list .= '<div style="background-color: rgb(255, 255, 255); border: 1px solid rgb(0, 148, 160); color:rgb(0, 148, 160);" class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure" style="font-size:12px; font-weight:bold;"> ETD </b> <b style="font-size:12px; font-weight:bold;">'.date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></div>';
											$list .= '<div style="background-color: rgb(0, 148, 160); border: 1px solid rgb(0, 148, 160); color:rgb(255, 255, 255);" class= "'.$class .'r-'.$count .'"' . $style . '><b style="font-size:8px;">' . $d['lcname'] . '</b> </div>';
											$list	.= '</div>';
									}
									if($arrive == $d['lcid']){
											$deapart_time = strtotime( $d['rdtime'] );
											$list	.= '<div style="width:100px; display:flex; flex-direction:row; align-items:flex-end; justify-content:flex-end; padding-bottom:10px">
																	<div style="width:35px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
																	<div style="width:20px; height:20px; background-color:rgb(0, 148, 160); border-radius:50%;"></div>
																</div>';
											$list .= '<div style="background-color: rgb(255, 255, 255); border: 1px solid rgb(0, 148, 160); color:rgb(0, 148, 160);" class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival" style="font-size:12px; font-weight:bold;"> ETA </b> <b style="font-size:12px; font-weight:bold;">' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></div>';
											$list .= '<div style="background-color: rgb(0, 148, 160); border: 1px solid rgb(0, 148, 160); color:rgb(255, 255, 255);" class= "'.$class .'r-'.$count .'"' . $style . '><b style="font-size:8px;">'. $d['lcname'] . '</b> </div>';
											$list	.= '</div>';
									}
									if( $lbl == "Clearance" )
									{
											$deapart_time = strtotime( $d['rdtime']);
											$list	.= '<div style="width:100px; display:flex; flex-direction:row; align-items:flex-end; justify-content:flex-end; padding-bottom:10px">
															<div style="width:35px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
															<div style="width:20px; height:20px; border:1px solid rgb(0, 148, 160); border-radius:50%;"></div>
															<div style="width:42.7px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
														</div>';
        									$list .= '<div style="background-color: rgb(255, 255, 255); border: 1px solid rgb(0, 148, 160); color:rgb(0, 148, 160);" class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b style="font-size:9px;" class="lbl-arrival"> ETA ' . date( 'H:i', strtotime( $d['rdtime'] ) ) . ' / ETD ' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></div>';
        									$list .= '<div style="background-color: rgb(0, 148, 160); border: 1px solid rgb(0, 148, 160); color:rgb(255, 255, 255);" class= "'.$class .'r-'.$count .'"' . $style . '><b style="font-size:8px;">' . $d['lcname'] . '</b> </div>';
        									$list .= '</div>';
									}
									if($count != 0 && $arrive != $d['lcid'] && $lbl != "Clearance"){
											$deapart_time = strtotime( $d['rdtime'] );
											$list	.= '<div style="width:100px; display:flex; flex-direction:row; align-items:flex-end; justify-content:flex-end; padding-bottom:10px">
															<div style="width:35px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
															<div style="width:20px; height:20px;border:1px solid rgb(0, 148, 160); border-radius:50%;"></div>
															<div style="width:42.7px; border-bottom:1px solid rgb(0, 148, 160); padding-top:10px;"></div>
														</div>';
        									$list .= '<div style="background-color: rgb(255, 255, 255); border: 1px solid rgb(0, 148, 160); color:rgb(0, 148, 160);" class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b style="font-size:9px;" class="lbl-arrival"> ETA ' . date( 'H:i', strtotime( $d['rdtime'] ) ) . ' / ETD ' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></div>';
        									$list .= '<div style="background-color: rgb(0, 148, 160); border: 1px solid rgb(0, 148, 160); color:rgb(255, 255, 255);" class= "'.$class .'r-'.$count .'"' . $style . '><b style="font-size:8px;">' . $d['lcname'] . '</b> </div>';
        									$list .= '</div>';
									}
							}
					}
				}



        if($style == ""){
            $count = $count + 1;
        }
    }
    if($count == 1){
        add_variable('margin_left', "margin-left:21.5%%" );
    }
    if($count == 2){
        add_variable('margin_left', "padding-left:27.5%; width:100%;" );
    }
    if($count == 3){
        add_variable('margin_left', "margin-left:17.5%" );
    }
    if($count == 4){
        add_variable('margin_left', "margin-left:15.5%" );
    }
    if($count == 5){
        add_variable('margin_left', "margin-left:2.5%%" );
    }
    return $list;
}

function get_country( $id, $field = '' )
{
    global $db;

    $s = 'SELECT * FROM l_country WHERE lcountry_id = %d ORDER BY lcountry ASC';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( empty( $field ) )
    {
        return $d;
    }
    else
    {
        return $d[ $field ];
    }
}

function get_country_list_option( $id = 248, $use_empty = false, $empty_text = 'Select Country', $api = false )
{
	global $db;

	$s = 'SELECT * FROM l_country ORDER BY lorder_id ASC, lcountry ASC';
	$q = $db->prepare_query( $s, '0' );
	$r = $db->do_query( $q );

    if( $api )
    {
        $content = array();

        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                $content[] = array( 'id' => $d['lcountry_id'], 'name' => mb_convert_encoding( $d['lcountry'], 'UTF-8', 'auto' ) );
            }
        }
    }
    else
    {
        $content = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                $content .= '<option value="' . $d['lcountry_id'] . '" ' . ( $d['lcountry_id'] == $id ? 'selected' : '' ) . '>' . mb_convert_encoding( $d['lcountry'], 'UTF-8', 'auto' ) . '</option>';
            }
        }
    }

    return $content;
}

function get_payment_method_option( $is_admin = false, $value = '' )
{
    $method = get_payment_method();

    if( $is_admin )
    {
        $exclude = array( 'Agent', 'Credit Card' );
        $option  = '';

        foreach( $method as $idx => $obj )
        {
            if( !in_array( $obj['mname'], $exclude ) )
            {
                $option .= '<option value="' . $obj['mid'] . '" ' . ( $value == $obj['mid'] ? 'selected' : '' ) . '>' . $obj['mname'] . '</option>';
            }
        }

        return $option;
    }
    else
    {
        $include = array( 'Paypal', 'Credit Card' );
        $option  = array();

        foreach( $method as $idx => $obj )
        {
            if( in_array( $obj['mname'], $include ) )
            {
                $option[ $obj['mname'] ] = '<option value="' . $obj['mid'] . '" ' . ( $value == $obj['mid'] ? 'selected' : '' ) . '>' . $obj['mname'] . '</option>';
            }
        }

        ksort( $option );

        return implode( '', $option );
    }
}

function get_transport_data( $trip, $trans_type )
{
	global $db;

    $transport = '';

	if( empty( $trans_type ) )
	{
        if( isset( $trip['pickup'] ) )
        {
            $transport .= '<li><p><b>Pickup</b><br />Own Transport</p></li>';
        }

        if( isset( $trip['drop-off'] ) )
        {
            $transport .= '<li><p><b>Drop-off</b><br />Own Transport</p></li>';
        }
	}
	else
	{
        if( isset( $trip['pickup'] ) )
        {
            $taid  = isset( $trip['pickup']['area_id'] ) ? $trip['pickup']['area_id'] : '';
            $trans = isset( $trip['pickup']['transport_type'] ) ? $trip['pickup']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '<li><p><b>Pickup</b><br />Own Transport</p></li>';
            }
            else
            {
                if( $trans !='' && $taid != '' )
                {
                    $taname = get_transport_area( $taid, 'taname' );
                    $rpto   = isset( $trip['pickup']['rpto'] ) && !empty( $trip['pickup']['rpto'] ) ? date( 'H:i', strtotime( $trip['pickup']['rpto'] ) ) : '';
                    $rpfrom = isset( $trip['pickup']['rpfrom'] ) && !empty( $trip['pickup']['rpfrom'] ) ? date( 'H:i', strtotime( $trip['pickup']['rpfrom'] ) ) : '';
                    $note   = empty( $rpfrom ) && empty( $rpto ) ? '' : 'in between ' . $rpfrom . ' - ' . $rpto;

                    $transport .= '
                    <li>
                        <p><b>Pickup</b><br />
                        ' . ( $trans == 0 ? 'Shared Transport to ' . $taname : 'Private Transport to ' . $taname ) . '<br />
                        ' . $note . '
                        </p>
                    </li>';
                }
                elseif( $trans !='' && $taid == '' )
                {
                    $transport .= '<li><p><b>Pickup</b><br />' . ( $trans == 0 ? 'Shared Transport' : 'Private Transport' ) . '</p></li>';
                }
                else
                {
                    $transport .= '<li><p><b>Pickup</b><br />-</p></li>';
                }
            }
        }

        if( isset( $trip['drop-off'] ) )
        {
            $taid  = isset( $trip['drop-off']['area_id'] ) ? $trip['drop-off']['area_id'] : '';
            $trans = isset( $trip['drop-off']['transport_type'] ) ? $trip['drop-off']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '<li><p><b>Drop-off</b><br />Own Transport</p></li>';
            }
            else
            {
                if( $trans !='' && $taid != '' )
                {
                    $taname = get_transport_area( $taid, 'taname' );
                    $rpto   = isset( $trip['drop-off']['rpto'] ) && !empty( $trip['drop-off']['rpto'] )  ? date( 'H:i', strtotime( $trip['drop-off']['rpto'] ) ) : '';
                    $rpfrom = isset( $trip['drop-off']['rpfrom'] ) && !empty( $trip['drop-off']['rpfrom'] )  ? date( 'H:i', strtotime( $trip['drop-off']['rpfrom'] ) ) : '';
                    $note   = empty( $rpfrom ) && empty( $rpto ) ? '' : 'in between ' . $rpfrom . ' - ' . $rpto;

                    $transport .= '
                    <li>
                        <p><b>Drop-off</b><br />
                        ' . ( $trans == 0 ? 'Shared Transport to ' . $taname : 'Private Transport to ' . $taname ) . '
                        ' . $note . '
                        </p>
                    </li>';
                }
                elseif( $trans !='' && $taid == '' )
                {
                    $transport .= '<li><p><b>Drop-off</b><br />' . ( $trans == 0 ? 'Shared Transport' : 'Private Transport' ) . '</p></li>';
                }
                else
                {
                    $transport .= '<li><p><b>Drop-off</b><br />-</p></li>';
                }
            }
        }
	}

    return $transport;
}

function get_depart_transport_detail( $trip, $trans_type, $is_agent = false )
{
    global $db;

    $transport = '';

	if( empty( $trans_type ) )
	{
        if( isset( $trip['pickup'] ) )
        {
            $transport .= '
            <div class="wrapp-detail other-detail">
                <h3>Pickup</h3>
                <h4>Own Transport</h4>
                <div class="form-group">
                    <label>Driver\'s Name</label>
                    <input type="text" class="text form-control" name="dep_pickup_driver_name" />
                </div>
                <div class="form-group">
                    <label>Driver\'s Phone</label>
                    <input type="text" class="text form-control" name="dep_pickup_driver_phone" />
                </div>
            </div>';
        }

        if( isset( $trip['drop-off'] ) )
        {
            $transport .= '
            <div class="wrapp-detail other-detail">
                <h3>Drop-off</h3>
                <h4>Own Transport</h4>
                <div class="form-group">
                    <label>Driver\'s Name</label>
                    <input type="text" class="text form-control" name="dep_dropoff_driver_name" />
                </div>
                <div class="form-group">
                    <label>Driver\'s Phone</label>
                    <input type="text" class="text form-control" name="dep_dropoff_driver_phone" />
                </div>
            </div>';
        }
	}
	else
	{
        if( isset( $trip['pickup'] ) )
        {
            $airport = isset( $trip['pickup']['airport'] ) ? $trip['pickup']['airport'] : '';
            $taid    = isset( $trip['pickup']['area_id'] ) ? $trip['pickup']['area_id'] : '';
            $hid     = isset( $trip['pickup']['hotel_id'] ) ? $trip['pickup']['hotel_id'] : '';
            $ftime   = isset( $trip['pickup']['flight_time'] ) ? $trip['pickup']['flight_time'] : '';
            $trans   = isset( $trip['pickup']['transport_type'] ) ? $trip['pickup']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '
                <div class="wrapp-detail other-detail">
                    <h3>Pickup</h3>
                    <h4>Own Transport</h4>
                    <div class="form-group">
                        <label>Driver\'s Name</label>
                        <input type="text" class="text form-control" name="dep_pickup_driver_name" />
                    </div>
                    <div class="form-group">
                        <label>Driver\'s Phone</label>
                        <input type="text" class="text form-control" name="dep_pickup_driver_phone" />
                    </div>
                </div>';
            }
            else
            {
                if( $trans != '' && $taid != '' )
                {
                    $s = 'SELECT a.taname FROM ticket_transport_area AS a WHERE a.taid = %d';
                    $q = $db->prepare_query( $s, $taid );
                    $r = $db->do_query( $q );
                    $d = $db->fetch_array( $r );

                    $type_trans = $trans == 0 ? 'Shared Transport to ' . $d['taname'] : 'Private Transport to ' . $d['taname'];
                }
                elseif( $trans != '' && $taid == '' )
                {
                    $type_trans = $trans == 0 ? 'Shared Transport' : 'Private Transport';
                }
                else
                {
                    $type_trans = '-';
                }

                if( in_array( $hid, array( 1, 2 ) ) )
                {
                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Pickup</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">
                                ' . ( $hid == 1 ? 'Hotel to be advised' : 'Other Hotel' ) . '<br/>
                                Please note that some hotels may be outside the free pick up/drop-offs area and may be subject to additional fees.
                            </div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '">
                                <div class="form-group">
                                    <label>Hotel/Villa Name</label>
                                    <input type="text" class="text form-control" name="dep_pickup_hotel_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Address</label>
                                    <input type="text" class="text form-control" name="dep_pickup_hotel_address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Phone</label>
                                    <input type="text" class="text form-control" name="dep_pickup_hotel_phone" value=""/>
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                elseif( empty( $hid ) )
                {
                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Pickup</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">No Accommodation Booked</div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '">
                                <div class="form-group">
                                    <label>Hotel/Villa Name</label>
                                    <input type="text" class="text form-control" name="dep_pickup_hotel_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Address</label>
                                    <input type="text" class="text form-control" name="dep_pickup_hotel_address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Phone</label>
                                    <input type="text" class="text form-control" name="dep_pickup_hotel_phone" value=""/>
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                else
                {
                    $d = get_hotel( $hid );
                    $t = $d['taairport'] == '1' ? 'Airport' : 'Hotel/Villa';

                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Pickup</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div>
                                <div class="form-group">
                                    <label>' . $t . ' Name <small>*</small></label>
                                    <input type="text" class="text form-control required" name="dep_pickup_hotel_name" value="' . $d['hname'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label>' . $t . ' Address<small>*</small></label>
                                    <input type="text" class="text form-control required" name="dep_pickup_hotel_address" value="' . $d['haddress'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label>' . $t . ' Phone <small>*</small></label>
                                    <input type="text" class="text form-control required" name="dep_pickup_hotel_phone" value="' . $d['hphone'] . '" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
            }
        }

        if( isset( $trip['drop-off'] ) )
        {
            $airport = isset( $trip['drop-off']['airport'] ) ? $trip['drop-off']['airport'] : '';
            $taid    = isset( $trip['drop-off']['area_id'] ) ? $trip['drop-off']['area_id'] : '';
            $hid     = isset( $trip['drop-off']['hotel_id'] ) ? $trip['drop-off']['hotel_id'] : '';
            $ftime   = isset( $trip['drop-off']['flight_time'] ) ? $trip['drop-off']['flight_time'] : '';
            $trans   = isset( $trip['drop-off']['transport_type'] ) ? $trip['drop-off']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '
                <div class="wrapp-detail other-detail">
                    <h3>Drop-off</h3>
                    <h4>Own Transport</h4>
                    <div class="form-group">
                        <label>Driver\'s Name</label>
                        <input type="text" class="text form-control" name="dep_dropoff_driver_name" />
                    </div>
                    <div class="form-group">
                        <label>Driver\'s Phone</label>
                        <input type="text" class="text form-control" name="dep_dropoff_driver_phone" />
                    </div>
                </div>';
            }
            else
            {
                if( $trans !='' && $taid != '' )
                {
                    $s = 'SELECT a.taname FROM ticket_transport_area AS a WHERE a.taid = %d';
                    $q = $db->prepare_query( $s, $taid );
                    $r = $db->do_query( $q );
                    $d = $db->fetch_array( $r );

                    $type_trans = $trans == 0 ? 'Shared Transport to ' . $d['taname'] : 'Private Transport to ' . $d['taname'];
                }
                elseif( $trans != '' && $taid == '' )
                {
                    $type_trans = $trans == 0 ? 'Shared Transport' : 'Private Transport';
                }
                else
                {
                    $type_trans = '-';
                }

                if( in_array( $hid, array( 1, 2 ) ) )
                {
                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Drop-off</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">' . ( $hid == 1 ? 'Hotel to be advised' : 'Other Hotel' ) . '<br/>
                                Please note that some hotels may be outside the free pick up/drop-offs area and may be subject to additional fees.
                            </div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '">
                                <div class="form-group">
                                    <label>Hotel/Villa Name</label>
                                    <input type="text" class="text form-control" name="dep_dropoff_hotel_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Address</label>
                                    <input type="text" class="text form-control" name="dep_dropoff_hotel_address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Phone</label>
                                    <input type="text" class="text form-control" name="dep_dropoff_hotel_phone" value=""/>
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                elseif( empty( $hid ) )
                {
                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Drop-off</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">No Accommodation Booked</div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '">
                                <div class="form-group">
                                    <label>Hotel/Villa Name</label>
                                    <input type="text" class="text form-control" name="dep_dropoff_hotel_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Address</label>
                                    <input type="text" class="text form-control" name="dep_dropoff_hotel_address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Phone</label>
                                    <input type="text" class="text form-control" name="dep_dropoff_hotel_phone" value=""/>
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                else
                {
                    $d = get_hotel( $hid );
                    $t = $d['taairport'] == '1' ? 'Airport' : 'Hotel/Villa';

                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Drop-off</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div>
                                <div class="form-group">
                                    <label>' . $t . ' Name <small>*</small></label>
                                    <input type="text" class="text form-control required" name="dep_dropoff_hotel_name" value="' . $d['hname'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label>' . $t . ' Address<small>*</small></label>
                                    <input type="text" class="text form-control required" name="dep_dropoff_hotel_address" value="' . $d['haddress'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label>' . $t . ' Phone <small>*</small></label>
                                    <input type="text" class="text form-control required" name="dep_dropoff_hotel_phone" value="' . $d['hphone'] . '" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
            }
        }
	}

    return $transport;
}

function get_return_transport_detail( $trip, $trans_type, $is_agent = false )
{
    global $db;

    $transport = '';

	if( empty( $trans_type ) )
	{
        if( isset( $trip['pickup'] ) )
        {
            $transport .= '
            <div class="wrapp-detail other-detail">
                <h3>Pickup</h3>
                <h4>Own Transport</h4>
                <div class="form-group">
                    <label>Driver\'s Name</label>
                    <input type="text" class="text form-control" name="rtn_pickup_driver_name" />
                </div>
                <div class="form-group">
                    <label>Driver\'s Phone</label>
                    <input type="text" class="text form-control" name="rtn_pickup_driver_phone" />
                </div>
            </div>';
        }

        if( isset( $trip['drop-off'] ) )
        {
            $transport .= '
            <div class="wrapp-detail other-detail">
                <h3>Drop-off</h3>
                <h4>Own Transport</h4>
                <div class="form-group">
                    <label>Driver\'s Name</label>
                    <input type="text" class="text form-control" name="rtn_dropoff_driver_name" />
                </div>
                <div class="form-group">
                    <label>Driver\'s Phone</label>
                    <input type="text" class="text form-control" name="rtn_dropoff_driver_phone" />
                </div>
            </div>';
        }
	}
	else
	{
        if( isset( $trip['pickup'] ) )
        {
            $airport = isset( $trip['pickup']['airport'] ) ? $trip['pickup']['airport'] : '';
            $taid    = isset( $trip['pickup']['area_id'] ) ? $trip['pickup']['area_id'] : '';
            $hid     = isset( $trip['pickup']['hotel_id'] ) ? $trip['pickup']['hotel_id'] : '';
            $ftime   = isset( $trip['pickup']['flight_time'] ) ? $trip['pickup']['flight_time'] : '';
            $trans   = isset( $trip['pickup']['transport_type'] ) ? $trip['pickup']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '
                <div class="wrapp-detail other-detail">
                    <h3>Pickup</h3>
                    <h4>Own Transport</h4>
                    <div class="form-group">
                        <label>Driver\'s Name</label>
                        <input type="text" class="text form-control" name="rtn_pickup_driver_name" />
                    </div>
                    <div class="form-group">
                        <label>Driver\'s Phone</label>
                        <input type="text" class="text form-control" name="rtn_pickup_driver_phone" />
                    </div>
                </div>';
            }
            else
            {
                if( $trans != '' && $taid != '' )
                {
                    $s = 'SELECT a.taname FROM ticket_transport_area AS a WHERE a.taid = %d';
                    $q = $db->prepare_query( $s, $taid );
                    $r = $db->do_query( $q );
                    $d = $db->fetch_array( $r );

                    $type_trans = $trans == 0 ? 'Shared Transport to ' . $d['taname'] : 'Private Transport to ' . $d['taname'];
                }
                elseif( $trans != '' && $taid == '' )
                {
                    $type_trans = $trans == 0 ? 'Shared Transport' : 'Private Transport';
                }
                else
                {
                    $type_trans = '-';
                }

                if( in_array( $hid, array( 1, 2 ) ) )
                {
                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Pickup</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">' . ( $hid == 1 ? 'Hotel to be advised' : 'Other Hotel' ) . '<br/>
                                Please note that some hotels may be outside the free pick up/drop-offs area and may be subject to additional fees.
                            </div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '">
                                <div class="form-group">
                                    <label>Hotel/Villa Name</label>
                                    <input type="text" class="text form-control" name="rtn_pickup_hotel_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Address</label>
                                    <input type="text" class="text form-control" name="rtn_pickup_hotel_address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Phone</label>
                                    <input type="text" class="text form-control" name="rtn_pickup_hotel_phone" value=""/>
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                elseif( empty( $hid ) )
                {
                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Pickup</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">No Accommodation Booked</div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '">
                                <div class="form-group">
                                    <label>Hotel/Villa Name</label>
                                    <input type="text" class="text form-control" name="rtn_pickup_hotel_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Address</label>
                                    <input type="text" class="text form-control" name="rtn_pickup_hotel_address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Phone</label>
                                    <input type="text" class="text form-control" name="rtn_pickup_hotel_phone" value=""/>
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                else
                {
                    $d = get_hotel( $hid );
                    $t = $d['taairport'] == '1' ? 'Airport' : 'Hotel/Villa';

                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Pickup</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div>
                                <div class="form-group">
                                    <label>' . $t . ' Name <small>*</small></label>
                                    <input type="text" class="text form-control required" name="rtn_pickup_hotel_name" value="' . $d['hname'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label>' . $t . ' Address<small>*</small></label>
                                    <input type="text" class="text form-control required" name="rtn_pickup_hotel_address" value="' . $d['haddress'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label>' . $t . ' Phone <small>*</small></label>
                                    <input type="text" class="text form-control required" name="rtn_pickup_hotel_phone" value="' . $d['hphone'] . '" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
            }
        }

        if( isset( $trip['drop-off'] ) )
        {
            $airport = isset( $trip['drop-off']['airport'] ) ? $trip['drop-off']['airport'] : '';
            $taid    = isset( $trip['drop-off']['area_id'] ) ? $trip['drop-off']['area_id'] : '';
            $hid     = isset( $trip['drop-off']['hotel_id'] ) ? $trip['drop-off']['hotel_id'] : '';
            $ftime   = isset( $trip['drop-off']['flight_time'] ) ? $trip['drop-off']['flight_time'] : '';
            $trans   = isset( $trip['drop-off']['transport_type'] ) ? $trip['drop-off']['transport_type'] : '';

            if( $trans == 2 )
            {
                $transport .= '
                <div class="wrapp-detail other-detail">
                    <h3>Drop-off</h3>
                    <h4>Own Transport</h4>
                    <div class="form-group">
                        <label>Driver\'s Name</label>
                        <input type="text" class="text form-control" name="rtn_dropoff_driver_name" />
                    </div>
                    <div class="form-group">
                        <label>Driver\'s Phone</label>
                        <input type="text" class="text form-control" name="rtn_dropoff_driver_phone" />
                    </div>
                </div>';
            }
            else
            {
                if( $trans != '' && $taid != '' )
                {
                    $s = 'SELECT a.taname FROM ticket_transport_area AS a WHERE a.taid = %d';
                    $q = $db->prepare_query( $s, $taid );
                    $r = $db->do_query( $q );
                    $d = $db->fetch_array( $r );

                    $type_trans = $trans == 0 ? 'Shared Transport to ' . $d['taname'] : 'Private Transport to ' . $d['taname'];
                }
                elseif( $trans != '' && $taid == '' )
                {
                    $type_trans = $trans == 0 ? 'Shared Transport' : 'Private Transport';
                }
                else
                {
                    $type_trans = '-';
                }

                if( in_array( $hid, array( 1, 2 ) ) )
                {
                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Drop-off</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">' . ( $hid == 1 ? 'Hotel to be advised' : 'Other Hotel' ) . '<br/>
                                Please note that some hotels may be outside the free pick up/drop-offs area and may be subject to additional fees.
                            </div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '">
                                <div class="form-group">
                                    <label>Hotel/Villa Name</label>
                                    <input type="text" class="text form-control" name="rtn_dropoff_hotel_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Address</label>
                                    <input type="text" class="text form-control" name="rtn_dropoff_hotel_address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Phone</label>
                                    <input type="text" class="text form-control" name="rtn_dropoff_hotel_phone" value=""/>
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                elseif( empty( $hid ) )
                {
                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Drop-off</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div class="subtext">No Accommodation Booked</div>
                            <div class="' . ( empty( $taid ) ? 'sr-only' : '' ) . '">
                                <div class="form-group">
                                    <label>Hotel/Villa Name</label>
                                    <input type="text" class="text form-control" name="rtn_dropoff_hotel_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Address</label>
                                    <input type="text" class="text form-control" name="rtn_dropoff_hotel_address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Hotel/Villa Phone</label>
                                    <input type="text" class="text form-control" name="rtn_dropoff_hotel_phone" value=""/>
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
                else
                {
                    $d = get_hotel( $hid );
                    $t = $d['taairport'] == '1' ? 'Airport' : 'Hotel/Villa';

                    $transport .= '
                    <div class="wrapp-detail other-detail">
                        <h3>Drop-off</h3>
                        <h4>' . $type_trans . '</h4>';

                        if( $airport == 1 && empty( $ftime ) === false )
                        {
                            $transport .= '
                            <div class="subtext">
                                Flight time ' . $ftime . '
                            </div>';
                        }
                        else
                        {
                            $transport .= '
                            <div>
                                <div class="form-group">
                                    <label>' . $t . ' Name <small>*</small></label>
                                    <input type="text" class="text form-control required" name="rtn_dropoff_hotel_name" value="' . $d['hname'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label>' . $t . ' Address<small>*</small></label>
                                    <input type="text" class="text form-control required" name="rtn_dropoff_hotel_address" value="' . $d['haddress'] . '" readonly />
                                </div>
                                <div class="form-group">
                                    <label>' . $t . ' Phone <small>*</small></label>
                                    <input type="text" class="text form-control required" name="rtn_dropoff_hotel_phone" value="' . $d['hphone'] . '" readonly />
                                </div>
                            </div>';
                        }

                        $transport .= '
                    </div>';
                }
            }
        }
	}

    return $transport;
}

function get_area_option_by_trip( $taid, $trans, $count = 0, $ttype = 0 )
{
    $options   = '';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        foreach( $trans as $d )
        {
            $fee      = $d['tafee'] * $count;
            $fee_num  = number_format( $fee, 0, ',', '.' );
            $selected = $taid == $d['taid'] ? 'selected' : '';

            $options .= '
            <option value="' . $d['taid'] . '"
                data-supporttrans="' . $d['tatypeforonline'] . '"
                data-rpfrom="' . $d['rpfrom'] . '"
                data-rpto="' . $d['rpto'] . '"
                data-area-id="' . $d['taid'] . '"
                data-area="' . $d['taname'] . '"
                data-airport="' . $d['taairport'] . '"
                data-fee="' . $fee . '"
                data-fee-num="' . $fee_num . '" ' . $selected . '>
                ' . $d['taname'] . '
            </option>';
        }

        return $options;
    }
}

function get_hotel_option_by_trip( $hid, $trans, $count = 0, $ttype = 0 )
{
    global $db;

    $options   = '
    <option value="1" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="" data-address="" data-phone="" data-email="" data-fee="0" data-fee-num="Free" ' . ( $hid == 1  ? 'selected' : '' ) . '>Not in list/To be Advised</option>
    <!--option value="2" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="" data-address="" data-phone="" data-email="" data-fee="0" data-fee-num="Free" ' . ( $hid == 2 ? 'selected' : '' ) . '>Other Hotel</option-->';

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        $rpfrom = array();
        $rpto   = array();
        $area   = array();
        $lcid   = '';

        foreach( $trans as $t )
        {
            $rpfrom[ $t['taid'] ] = $t['rpfrom'];
            $rpto[ $t['taid'] ]   = $t['rpto'];

            $area[] = $t['taid'];
            $lcid   = $t['lcid'];
        }

        $s = 'SELECT * FROM ticket_hotel AS a
              LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
              LEFT JOIN ticket_transport_area_fee AS c ON c.taid = b.taid
              WHERE a.hstatus = %d AND c.lcid = %s AND a.taid IN ( ' . implode( ',', $area ) . ' ) ORDER BY a.hname';
        $q = $db->prepare_query( $s, 1, $lcid );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            $fee      = $d['tafee'] * $count;
            $fee_num  = number_format( $fee, 0, ',', '.' );
            $selected = $hid == $d['hid'] ? 'selected' : '';

            $options .= '
            <option value="' . $d['hid'] . '"
                data-supporttrans="' . $d['tatypeforonline'] . '"
                data-rpfrom="' . $rpfrom[ $d['taid'] ] . '"
                data-rpto="' . $rpto[ $d['taid'] ] . '"
                data-area-id="' . $d['taid'] . '"
                data-area="' . $d['taname'] . '"
                data-airport="' . $d['taairport'] . '"
                data-fee="' . $fee . '"
                data-fee-num="' . $fee_num . '" ' . $selected . '
                data-address="' . $d['haddress'] . '"
                data-phone="' . $d['hphone'] . '"
                data-email="' . $d['hemail'] . '">
                ' . $d['hname'] . '
            </option>';
        }

        return $options;
    }
}

function get_all_transport_area_option( $taid, $trans, $count, $ttype = 0, $api = false )
{
    global $db;

    $rpfrom = array();
    $rpto   = array();
    $lcid   = '';

    if( !empty( $trans ) )
    {
        foreach( $trans as $t )
        {
            $rpfrom[ $t['taid'] ] = $t['rpfrom'];
            $rpto[ $t['taid'] ]   = $t['rpto'];
            $lcid                 = $t['lcid'];
        }
    }

    $s = 'SELECT * FROM ticket_transport_area AS a
          LEFT JOIN ticket_transport_area_fee AS b ON b.taid = a.taid
          WHERE b.lcid = %s  ORDER BY a.taname';
    $q = $db->prepare_query( $s, $lcid );
    $r = $db->do_query( $q );

    if( $api )
    {
        $options = array();
    }
    else
    {
        $options = '';
    }

    while( $d = $db->fetch_array( $r ) )
    {
        $fee      = $d['tafee'] * $count;
        $fee_num  = number_format( $fee, 0, ',', '.' );
        $selected = $taid == $d['taid'] ? 'selected' : '';
        $from     = isset( $rpfrom[ $d['taid'] ] ) ? $rpfrom[ $d['taid'] ] : '';
        $to       = isset( $rpto[ $d['taid'] ] ) ? $rpto[ $d['taid'] ] : '';

        if( $api )
        {
            $options[] = array(
                'taid'         => $d['taid'],
                'supporttrans' => $d['tatypeforonline'],
                'rpfrom'       => $from,
                'rpto'         => $to,
                'area'         => $d['taname'],
                'airport'      => $d['taairport'],
                'fee'          => $fee,
                'fee_num'      => $fee_num,
                'label'        => $d['taname'],
            );
        }
        else
        {
            $options .= '
            <option value="' . $d['taid'] . '"
                data-supporttrans="' . $d['tatypeforonline'] . '"
                data-rpfrom="' . $from . '"
                data-rpto="' . $to . '"
                data-area-id="' . $d['taid'] . '"
                data-area="' . $d['taname'] . '"
                data-airport="' . $d['taairport'] . '"
                data-fee="' . $fee . '"
                data-fee-num="' . $fee_num . '" ' . $selected . '>
                ' . $d['taname'] . '
            </option>';
        }
    }

    return $options;
}

function get_all_hotel_option( $hid, $trans, $count = 0, $ttype = 0, $api = false )
{
    global $db;

    if( $api )
    {
        $options[] = array(
            'hid'          => 1,
            'taid'         => '',
            'supporttrans' => '',
            'rpfrom'       => '',
            'rpto'         => '',
            'area'         => '',
            'airport'      => '',
            'fee'          => 0,
            'fee_num'      => '',
            'address'      => '',
            'phone'        => '',
            'email'        => '',
            'label'        => 'Not in list/To be Advised'
        );
    }
    else
    {
        $options = '
        <option value="1" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="" data-address="" data-phone="" data-email="" data-fee="0" data-fee-num="Free" ' . ( $hid == 1  ? 'selected' : '' ) . '>Not in list/To be Advised</option>
        <!--option value="2" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="" data-address="" data-phone="" data-email="" data-fee="0" data-fee-num="Free" ' . ( $hid == 2 ? 'selected' : '' ) . '>Other Hotel</option-->';
    }

    if( empty( $trans ) )
    {
        return $options;
    }
    else
    {
        $rpfrom = array();
        $rpto   = array();
        $area   = array();
        $lcid   = '';

        foreach( $trans as $t )
        {
            $rpfrom[ $t['taid'] ] = $t['rpfrom'];
            $rpto[ $t['taid'] ]   = $t['rpto'];

            $area[] = $t['taid'];
            $lcid   = $t['lcid'];
        }

        $s = 'SELECT * FROM ticket_hotel AS a
              LEFT JOIN ticket_transport_area AS b ON a.taid = b.taid
              LEFT JOIN ticket_transport_area_fee AS c ON c.taid = b.taid
              WHERE a.hstatus = %d AND c.lcid = %s ORDER BY a.hname';
        $q = $db->prepare_query( $s, 1, $lcid );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            $fee      = $d['tafee'] * $count;
            $fee_num  = number_format( $fee, 0, ',', '.' );
            $selected = $hid == $d['hid'] ? 'selected' : '';
            $from     = isset( $rpfrom[ $d['taid'] ] ) ? $rpfrom[ $d['taid'] ] : '';
            $to       = isset( $rpto[ $d['taid'] ] ) ? $rpto[ $d['taid'] ] : '';
            $hname    = empty( $d['hname'] ) ? '' : mb_convert_encoding( $d['hname'], 'UTF-8', 'auto' );
            $haddress = empty( $d['haddress'] ) ? '' : mb_convert_encoding( $d['haddress'], 'UTF-8', 'auto' );

            if( $api )
            {
                $options[] = array(
                    'hid'          => $d['hid'],
                    'taid'         => $d['taid'],
                    'supporttrans' => $d['tatypeforonline'],
                    'rpfrom'       => $from,
                    'rpto'         => $to,
                    'area'         => $d['taname'],
                    'airport'      => $d['taairport'],
                    'fee'          => $fee,
                    'fee_num'      => $fee_num,
                    'address'      => $haddress,
                    'phone'        => $d['hphone'],
                    'email'        => $d['hemail'],
                    'label'        => $hname
                );
            }
            else
            {
                $options .= '
                <option value="' . $d['hid'] . '"
                    data-supporttrans="' . $d['tatypeforonline'] . '"
                    data-rpfrom="' . $from . '"
                    data-rpto="' . $to . '"
                    data-area-id="' . $d['taid'] . '"
                    data-area="' . $d['taname'] . '"
                    data-airport="' . $d['taairport'] . '"
                    data-fee="' . $fee . '"
                    data-fee-num="' . $fee_num . '" ' . $selected . '
                    data-address="' . $haddress . '"
                    data-phone="' . $d['hphone'] . '"
                    data-email="' . $d['hemail'] . '">
                    ' . $hname . '
                </option>';
            }
        }

        return $options;
    }
}

function get_pickup_drop_list_data( $rid, $lcid, $rptype = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_route_detail AS a
          LEFT JOIN ticket_route_pickup_drop AS b ON b.rdid = a.rdid
          LEFT JOIN ticket_transport_area AS c ON b.taid = c.taid
          LEFT JOIN ticket_transport_area_fee AS d ON d.taid = c.taid
          WHERE a.rid = %d AND a.lcid = %d AND d.lcid = %d ORDER BY c.taname';
    $q = $db->prepare_query( $s, $rid, $lcid, $lcid );
    $r = $db->do_query( $q );

    $trans = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $trans[ $d['rptype'] ][] = $d;
    }

    if( empty( $rptype ) )
    {
        return $trans;
    }
    else
    {
        return isset( $trans[ $rptype ] ) ? $trans[ $rptype ] : '';
    }
}

function get_pickup_drop_off_transport( $rid, $from, $to, $transport_type = 0, $sess = array(), $is_return = false, $is_admin = false, $api = false )
{
    $content     = '';
    $content_arr = array();
    $dtrans      = get_pickup_drop_list_data( $rid, $from );
    $atrans      = get_pickup_drop_list_data( $rid, $to );
    $trip_type   = $is_return ? 'return' : 'departure';

    if( isset( $dtrans['pickup'] ) )
    {
        if( empty( $transport_type ) )
        {
            if( $api )
            {
                $content_arr[] = array(
                    'used_trans'  => false,
                    'trip_type'   => $trip_type,
                    'trans_value' => 2,
                    'trans_type'  => 'pickup',
                    'trans_label' => 'Pickup',
                    'trans_name'  => 'Own Transport',
                    'trans_desc'  => 'Latest check in 30 minutes prior to departure'
                );
            }
            else
            {
                $content .= '
                <p class="own-trans">
                    <b>Pickup : </b>
                    <em>Own Transport &nbsp;</em>
                    <span class="text-danger">Latest check in 30 minutes prior to departure</span>
                    <input type="text" class="sr-only" name="trans[' . $trip_type . '][pickup][type]" value="2" />
                </p>';
            }
        }
        else
        {
            if( $api )
            {
                $content_arr[] = get_pickup_drop_off_transport_detail( 'pickup', $dtrans['pickup'], $sess, $is_return, $is_admin, $api );
            }
            else
            {
                $content .= get_pickup_drop_off_transport_detail( 'pickup', $dtrans['pickup'], $sess, $is_return, $is_admin, $api );
            }
        }
    }

    if( isset( $atrans['drop-off'] ) )
    {
        if( empty( $transport_type ) )
        {
            if( $api )
            {
                $content_arr[] = array(
                    'used_trans'  => false,
                    'trip_type'   => $trip_type,
                    'trans_value' => 2,
                    'trans_type'  => 'drop-off',
                    'trans_label' => 'Drop-off',
                    'trans_name'  => 'Own Transport',
                    'trans_desc'  => 'Latest check in 30 minutes prior to departure'
                );
            }
            else
            {
                $content .= '
                <p class="own-trans">
                    <b>Drop-off : </b>
                    <em>Own Transport &nbsp;</em>
                    <span class="text-danger">Latest check in 30 minutes prior to departure</span>
                    <input type="text" class="sr-only" name="trans[' . $trip_type . '][drop-off][type]" value="2" />
                </p>';
            }
        }
        else
        {
            if( $api )
            {
                $content_arr[] = get_pickup_drop_off_transport_detail( 'drop-off', $atrans['drop-off'], $sess, $is_return, $is_admin, $api );
            }
            else
            {
                $content .= get_pickup_drop_off_transport_detail( 'drop-off', $atrans['drop-off'], $sess, $is_return, $is_admin, $api );
            }
        }
    }

    if( $api )
    {
        return $content_arr;
    }
    else
    {
        return $content;
    }
}

function get_pickup_drop_off_transport_detail( $trans_type, $trans, $sess = array(), $is_return = false, $is_admin = false, $api = false )
{
    extract( $sess['trip'] );

    //-- Get Transport Fee and Flight Text
    $flight = $trans_type == 'pickup' ? 'Flight Landing Time' : 'Flight Take Off Time';
    $pass   = $sess['adult_num'] + $sess['child_num'] + $sess['infant_num'];
    $count  = ceil( $pass / 4 );

    if( $api )
    {
        if( $is_return )
        {
            $content_arr = array(
                'trans_value'  => 0,
                'used_trans'   => true,
                'flight_text'  => $flight,
                'trip_type'    => 'return',
                'trans_type'   => $trans_type,
                'trans_label'  => ucfirst( $trans_type ),
                'area_option'  => get_all_transport_area_option( null, $trans, $count, 0, true ),
                'hotel_option' => get_all_hotel_option( null, $trans, $count, 0, true )
            );
        }
        else
        {
            $content_arr = array(
                'trans_value'  => 0,
                'used_trans'   => true,
                'flight_text'  => $flight,
                'trip_type'    => 'departure',
                'trans_type'   => $trans_type,
                'trans_label'  => ucfirst( $trans_type ),
                'area_option'  => get_all_transport_area_option( null, $trans, $count, 0, true ),
                'hotel_option' => get_all_hotel_option( null, $trans, $count, 0, true )
            );
        }

        return $content_arr;
    }
    else
    {
        if( $is_return )
        {
            $ttype   = isset( $return[$trans_type]['transport_type'] ) ? $return[$trans_type]['transport_type'] : 0;
            $ftime   = isset( $return[$trans_type]['flight_time'] ) ? $return[$trans_type]['flight_time'] : '';
            $hid     = isset( $return[$trans_type]['hotel_id'] ) ? $return[$trans_type]['hotel_id'] : '';
            $taid    = isset( $return[$trans_type]['area_id'] ) ? $return[$trans_type]['area_id'] : '';
            $airport = isset( $return[$trans_type]['airport'] ) ? $return[$trans_type]['airport'] : 0;
            $rpfrom  = isset( $return[$trans_type]['rpfrom'] ) ? $return[$trans_type]['rpfrom'] : '';
            $rpto    = isset( $return[$trans_type]['rpto'] ) ? $return[$trans_type]['rpto'] : '';
            $hstatus = empty( $taid ) ? 'disabled' : '';
            $astatus = $ttype == 2 ? 'disabled' : '';

            if( $is_admin )
            {
                $area_option  = get_all_transport_area_option( $taid, $trans, $count, $ttype );
                $hotel_option = get_all_hotel_option( $hid, $trans, $count, $ttype );
            }
            else
            {
                $area_option  = get_area_option_by_trip( $taid, $trans, $count, $ttype );
                $hotel_option = get_hotel_option_by_trip( $hid, $trans, $count, $ttype );
            }

            $content = '
            <p>
                <b>' . ucfirst( $trans_type ) . ' : </b>
                <select class="select-option transport-type" name="trans[return][' . $trans_type . '][type]" autocomplete="off">
                    <option value="0" ' . ( $ttype == 0 ? 'selected' : '' ) . '>Shared Transport</option>
                    <option value="1" ' . ( $ttype == 1 ? 'selected' : '' ) . '>Private Transport</option>
                    <option value="2" ' . ( $ttype == 2 ? 'selected' : '' ) . '>Own Transport</option>
                </select>
                <select class="select-option hotels-area required" name="trans[return][' . $trans_type . '][taid]" autocomplete="off" data-error="Area can\'t be empty" ' . $astatus . '>
                    <option value="" data-supporttrans="0" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="0" data-fee="0" data-fee-num="Free">No Accommodation Booked</option>
                    ' . $area_option . '
                </select>
                <select class="select-option hotels required" name="trans[return][' . $trans_type . '][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" ' . $hstatus . '>
                    <option value="" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="0" data-fee="0" data-fee-num="Free" data-address="" data-phone="" data-email="">Select Hotel</option>
                    ' . $hotel_option . '
                </select>
                <span class="flight-time ' . ( $airport == 1 ? '' : 'sr-only' ) . '">
                    <b>' . $flight . ' : </b>
                    <input type="text" class="trans-ftime" name="trans[return][' . $trans_type . '][ftime]" value="' . $ftime . '" readonly="readonly" />
                    <input type="text" class="trans-airport sr-only" name="trans[return][' . $trans_type . '][taairport]" value="' . $airport . '" readonly="readonly" />
                </span>
                <input type="text" name="trans[return][' . $trans_type . '][rpto]"  class="trans-rpto sr-only" value="" />
                <input type="text" name="trans[return][' . $trans_type . '][rpfrom]"  class="trans-rpfrom sr-only" value="" />
                <input type="text" name="trans[return][' . $trans_type . '][trip-type]"  class="trip-type sr-only" value="return" />
                <input type="text" name="trans[return][' . $trans_type . '][trans-type]"  class="trans-type sr-only" value="' . $trans_type . '" />
            </p>';
        }
        else
        {
            $ttype   = isset( $departure[$trans_type]['transport_type'] ) ? $departure[$trans_type]['transport_type'] : 0;
            $ftime   = isset( $departure[$trans_type]['flight_time'] ) ? $departure[$trans_type]['flight_time'] : '';
            $hid     = isset( $departure[$trans_type]['hotel_id'] ) ? $departure[$trans_type]['hotel_id'] : '';
            $taid    = isset( $departure[$trans_type]['area_id'] ) ? $departure[$trans_type]['area_id'] : '';
            $airport = isset( $departure[$trans_type]['airport'] ) ? $departure[$trans_type]['airport'] : 0;
            $rpfrom  = isset( $departure[$trans_type]['rpfrom'] ) ? $departure[$trans_type]['rpfrom'] : '';
            $rpto    = isset( $departure[$trans_type]['rpto'] ) ? $departure[$trans_type]['rpto'] : '';
            $hstatus = empty( $taid ) ? 'disabled' : '';
            $astatus = $ttype == 2 ? 'disabled' : '';

            if( $is_admin )
            {
                $area_option  = get_all_transport_area_option( $taid, $trans, $count, $ttype );
                $hotel_option = get_all_hotel_option( $hid, $trans, $count, $ttype );
            }
            else
            {
                $area_option  = get_area_option_by_trip( $taid, $trans, $count, $ttype );
                $hotel_option = get_hotel_option_by_trip( $hid, $trans, $count, $ttype );
            }

            $content = '
            <p>
                <b>' . ucfirst( $trans_type ) . ' : </b>
                <select class="select-option transport-type" name="trans[departure][' . $trans_type . '][type]" autocomplete="off">
                    <option value="0" ' . ( $ttype == 0 ? 'selected' : '' ) . '>Shared Transport</option>
                    <option value="1" ' . ( $ttype == 1 ? 'selected' : '' ) . '>Private Transport</option>
                    <option value="2" ' . ( $ttype == 2 ? 'selected' : '' ) . '>Own Transport</option>
                </select>
                <select class="select-option hotels-area required" name="trans[departure][' . $trans_type . '][taid]" autocomplete="off" data-error="Area can\'t be empty" ' . $astatus . '>
                    <option value="" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="0" data-fee="0" data-fee-num="Free">No Accommodation Booked</option>
                    ' . $area_option . '
                </select>
                <select class="select-option hotels required" name="trans[departure][' . $trans_type . '][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" ' . $hstatus . '>
                    <option value="" data-rpfrom="" data-rpto="" data-area-id="" data-area="" data-airport="0" data-fee="0" data-fee-num="Free" data-address="" data-phone="" data-email="">Select Hotel</option>
                    ' . $hotel_option . '
                </select>
                <span class="flight-time ' . ( $airport == 1 ? '' : 'sr-only' ) . '">
                    <b>' . $flight . ' : </b>
                    <input type="text" class="trans-ftime" name="trans[departure][' . $trans_type . '][ftime]" value="' . $ftime . '" readonly="readonly" />
                    <input type="text" class="trans-airport sr-only" name="trans[departure][' . $trans_type . '][taairport]" value="' . $airport . '" readonly="readonly" />
                </span>
                <input type="text" name="trans[departure][' . $trans_type . '][trip-type]" class="trip-type sr-only" value="departure" />
                <input type="text" name="trans[departure][' . $trans_type . '][rpto]"  class="trans-rpto sr-only" value="' . $rpto . '" />
                <input type="text" name="trans[departure][' . $trans_type . '][rpfrom]"  class="trans-rpfrom sr-only" value="' . $rpfrom . '" />
                <input type="text" name="trans[departure][' . $trans_type . '][trans-type]"  class="trans-type sr-only" value="' . $trans_type . '" />
            </p>';
        }

        return $content;
    }
}

function get_pickup_drop_off_transport_price( $rid, $from, $to, $transport_type = 0, $sess = array(), $return = false )
{
    $dtrans    = get_pickup_drop_list_data( $rid, $from );
    $atrans    = get_pickup_drop_list_data( $rid, $to );
    $trip_type = $return ? 'return' : 'departure';
    $content   = '';

	if( empty( $transport_type ) )
	{
        $trans_class = isset( $dtrans['pickup'] ) && isset( $atrans['drop-off'] ) ? 'both' : '';

        if( isset( $dtrans['pickup'] ) )
        {
            $content .= '
            <div class="pickup-trans trans ' . $trans_class . '">
                <p>Free</p>
                <input type="text" name="trans[' . $trip_type . '][pickup][trans-fee]" class="pickup-trans-fee sr-only" value="0" />
            </div>';
        }

        if( isset( $atrans['drop-off'] ) )
        {
            $content .= '
            <div class="drop-off-trans trans ' . $trans_class . '">
                <p>Free</p>
                <input type="text" name="trans[' . $trip_type . '][drop-off][trans-fee]" class="drop-off-trans-fee sr-only" value="0" />
            </div>';
        }
	}
	else
	{
		if( isset( $sess['trip'] ) )
		{
            $trip_data   = $return ? $sess['trip']['return'] : $sess['trip']['departure'];
            $trans_class = isset( $dtrans['pickup'] ) && isset( $atrans['drop-off'] ) ? 'both' : '';

            if( isset( $dtrans['pickup'] ) )
            {
				$fee      = isset( $trip_data['pickup']['transport_fee'] ) ? $trip_data['pickup']['transport_fee'] : 0;
				$content .= '
                <div class="pickup-trans trans has-opt ' . $trans_class . '">
                    <p>' . ( $fee == 0 ? 'Free' : number_format( $fee, 0, ',', '.' ) ) . '</p>
                    <input type="text" name="trans[' . $trip_type . '][pickup][trans-fee]" class="pickup-trans-fee sr-only" value="' . $fee . '" />
                </div>';
            }

            if( isset( $atrans['drop-off'] ) )
            {
                $fee      = isset( $trip_data['drop-off']['transport_fee'] ) ? $trip_data['drop-off']['transport_fee'] : 0;
                $content .= '
                <div class="drop-off-trans trans has-opt ' . $trans_class . '">
                    <p>' . ( $fee == 0 ? 'Free' : number_format( $fee, 0, ',', '.' ) ) . '</p>
                    <input type="text" name="trans[' . $trip_type . '][drop-off][trans-fee]" class="drop-off-trans-fee sr-only" value="' . $fee . '" />
                </div>';
            }
		}
		else
		{
            if( isset( $dtrans['pickup'] ) )
            {
                $content .= '
                <div class="pickup-trans trans">
                    <p>Free</p>
                    <input type="text" name="trans[' . $trip_type . '][pickup][trans-fee]" class="pickup-trans-fee sr-only" value="0" />
                </div>';
            }

            if( isset( $atrans['drop-off'] ) )
            {
                $content .= '
                <div class="drop-off-trans trans">
                    <p>Free</p>
                    <input type="text" name="trans[' . $trip_type . '][drop-off][trans-fee]" class="drop-off-trans-fee sr-only" value="0" />
                </div>';
            }
		}
	}

	return $content;
}

function get_passenger_num_content( $adult = 0, $child = 0, $infant = 0, $api = false )
{
    if( $api )
	{
        $content = array();

        if( !empty( $adult ) )
        {
            $content[] = array( 'name' => 'Adult', 'num' => $adult );
        }

        if( !empty( $child ) )
        {
            $content[] = array( 'name' => 'Child', 'num' => $child );
        }

        if( !empty( $infant ) )
        {
            $content[] = array( 'name' => 'Infant', 'num' => $infant );
        }
    }
    else
    {
        $content = '';

        if( !empty( $adult ) )
        {
            $content .= '<p><b>Adult</b> (' . $adult . ')</p>';
        }

        if( !empty( $child ) )
        {
            $content .= '<p><b>Child</b> (' . $child . ')</p>';
        }

        if( !empty( $infant ) )
        {
            $content .= '<p><b>Infant</b> (' . $infant . ')</p>';
        }
    }

	return $content;
}

function get_passenger_num_detail_content( $adult = 0, $child = 0, $infant = 0 )
{
	$content = array();

	if( !empty( $adult ) )
	{
		$content[] = $adult > 1 ? $adult . ' adults' : $adult . ' adult';
	}

	if( !empty( $child ) )
	{
		$content[] = $child > 1 ? $child . ' childs' : $child . ' child';
	}

	if( !empty( $infant ) )
	{
		$content[] = $infant > 1 ? $infant . ' infants' : $infant . ' infant';
	}

	return empty( $content ) ? '-' : implode( ', ', $content );
}

function get_rate_price_content( $rid, $adult = 0, $child = 0, $infant = 0, $trip = array(), $return = false, $api = false )
{
    if( empty( $trip ) )
    {
        return '';
    }
    else
    {
        extract( $trip );

        if( $api )
        {
            $content = array();

            if( empty( $adult ) === false )
            {
                $content[] = empty( $adult_price ) ? 'Free' : 'IDR ' . number_format( $adult_price, 0, ',', '.' );
            }

            if( empty( $child ) === false )
            {
                $content[] = empty( $child_price ) ? 'Free' : 'IDR ' . number_format( $child_price, 0, ',', '.' );
            }

            if( empty( $infant ) === false )
            {
                $content[] = empty( $infant_price ) ? 'Free' : 'IDR ' . number_format( $infant_price, 0, ',', '.' );
            }
        }
        else
        {
            $content = '';
            $booking = booking_item();
            $agid    = $booking['sess_data']['agid'];
            $type    = $return ? 'return' : 'departure';

            if( empty( $adult ) === false )
            {
                $sprice_num = empty( $adult_sell_price ) ? 'Free' : 'IDR ' . number_format( $adult_sell_price, 0, ',', '.' );
                $price_num  = empty( $adult_price ) ? 'Free' : 'IDR ' . number_format( $adult_price, 0, ',', '.' );
                $content   .= '
                <p>
                    <span class="ptxt">' . $price_num . '</span>
                    <a class="edit"
                        data-price-num="' . $price_num . '"
                        data-selector="' . $type . '-' . $rid . '"
                        data-price="' . $adult_price . '"
                        data-price-type="adult_price"
                        data-type="' . $type . '"
                        data-sell-price-num="' . $sprice_num . '"
                        data-sell-price="' . $adult_sell_price . '"
                        data-sell-price-type="adult_sell_price"
                        data-toggle="popover">Edit Price
                    </a>
                </p>';
            }

            if( empty( $child ) === false )
            {
                $sprice_num = empty( $child_sell_price ) ? 'Free' : 'IDR ' . number_format( $child_sell_price, 0, ',', '.' );
                $price_num  = empty( $child_price ) ? 'Free' : 'IDR ' . number_format( $child_price, 0, ',', '.' );
                $content   .= '
                <p>
                    <span class="ptxt">' . $price_num . '</span>
                    <a class="edit"
                        data-price-num="' . $price_num . '"
                        data-selector="' . $type . '-' . $rid . '"
                        data-price="' . $child_price . '"
                        data-price-type="child_price"
                        data-type="' . $type . '"
                        data-sell-price-num="' . $sprice_num . '"
                        data-sell-price="' . $child_sell_price . '"
                        data-sell-price-type="child_sell_price"
                        data-toggle="popover">Edit Price
                    </a>
                </p>';
            }

            if( empty( $infant ) === false )
            {
                $sprice_num = empty( $infant_sell_price ) ? 'Free' : 'IDR ' . number_format( $infant_sell_price, 0, ',', '.' );
                $price_num  = empty( $infant_price ) ? 'Free' : 'IDR ' . number_format( $infant_price, 0, ',', '.' );
                $content   .= '
                <p>
                    <span class="ptxt">' . $price_num . '</span>
                    <a class="edit"
                        data-price-num="' . $price_num . '"
                        data-selector="' . $type . '-' . $rid . '"
                        data-price="' . $infant_price . '"
                        data-price-type="infant_price"
                        data-type="' . $type . '"
                        data-sell-price-num="' . $sprice_num . '"
                        data-sell-price="' . $infant_sell_price . '"
                        data-sell-price-type="infant_sell_price"
                        data-toggle="popover">Edit Price
                    </a>
                </p>';
            }
        }

        return $content;
    }
}

function get_seat_availibility_by_boat( $is_dashboard = false )
{
	$boats = get_boat_list();

	if( !empty( $boats ) )
	{
        if( $is_dashboard )
		{
            $item  = '';
            $nav   = '';

            foreach( $boats as $key => $d )
            {
                if( $d['bostatus'] != '2' )
                {
                    $item .= '
                    <div role="tabpanel" class="tab-pane ' . ( $key == 0 ? 'active' : '' ) . '" id="boat-' . $d['boid'] . '">
                        <div class="container-fluid">
                            <div class="row">';

                            $start = strtotime( date('Y-m-d') );

                            for( $i = 0; $i<=2; $i++)
                            {
                                $time  = strtotime( '+' . $i . ' day', $start );
                                $item .= '
                                <div class="col col-md-4 nopadding">
                                    <span>' .  date( 'd F Y', $time ) . '</span>
                                    <p><strong>' . get_seat_availibility_num_by_boat( $d['bopassenger'], $d['boid'], $time ) . '</strong> / ' . $d['bopassenger'] . '</p>
                                </div>';
                            }

                            $item .= '
                            </div>
                            <div class="row">
                                <div class="col col-md-12 nopadding">
                                    <small>available seats / total seats</small>
                                </div>
                            </div>
                        </div>
                    </div>';

                    $nav .= '
                    <li role="presentation" class="' . ( $key == 0 ? 'active' : '' ) . '">
                        <a href="#boat-' . $d['boid'] . '" aria-controls="boat-' . $d['boid'] . '" role="tab" data-toggle="tab">' . $d['boname'] . '</a>
                    </li>';
                }
            }

            $content = '
            <div class="boxs boat-block">
                <div class="tab-content">
                    ' . $item . '
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    ' . $nav . '
                </ul>
            </div>';

            return $content;
        }
        else
        {
            $item  = '';
    		$nav   = '';
    		$month = date( 'm' );
    		$year  = date( 'Y' );

    		foreach( $boats as $key => $d )
    		{
    			if( $d['bostatus'] != '2' )
    			{
    				$item .= '
    				<div class="tab-slide ' . ( $key == 0 ? 'active' : '' ) . '" id="boat-' . $d['boid'] . '">
                        <div class="container-fluid">
                            <div class="row">
                            	<div class="seat-slide" data-type="boat">';

                                    for( $day = 1; $day <= 31; $day++ )
                                    {
                                        $time = mktime( 12, 0, 0, $month, $day, $year );

                                        if( date('m', $time) == $month )
                                        {
                                            $item .= '
                                            <div id="boat-' . $d['boid'] . '-' . $day . '" class="col nopadding">
                                                <span>' . date( 'd F Y', $time ) . '</span>
                                                <p><strong>' . get_seat_availibility_num_by_boat( $d['bopassenger'], $d['boid'], $time ) . '</strong> / ' . $d['bopassenger'] . '</p>
                                            </div>';
                                        }
                                    }

                                    $item .= '
                            	</div>
                            </div>
                            <div class="row">
                                <div class="col col-md-12 nopadding">
                                    <p class="text-center"><small>available seats / total seats</small></p>
                                </div>
                            </div>
                        </div>
                    </div>';

    				$nav .= '
    				<li role="presentation" class="' . ( $key == 0 ? 'active' : '' ) . '">
    					<a href="#boat-' . $d['boid'] . '" aria-controls="boat-' . $d['boid'] . '" role="tab" data-toggle="tab">' . $d['boname'] . '</a>
    				</li>';
    			}
    		}

    		$content = '
    		<div class="boxs boat-block">
                <div class="tab-slide-content">
                	' . $item . '
                </div>
            </div>
            <ul class="nav nav-tabs">
                ' . $nav . '
            </ul>';

            return $content;
        }
	}
}

function get_seat_availability_by_agent( $agid = '' )
{
    global $db;

    $s = 'SELECT
            a.aid,
            a.sid,
            b.agid,
            d.rname,
            b.avallot,
            c.sfrom,
            c.sto,
            e.boname,
            e.bopassenger
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          LEFT JOIN ticket_route AS d ON c.rid = d.rid
          LEFT JOIN ticket_boat AS e ON c.boid = e.boid
          WHERE b.agid = %d AND c.sto >= %s ORDER BY c.sfrom, c.rid';
    $q = $db->prepare_query( $s, $agid, date( 'Y-m-d' ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $content = '
        <div class="seat-availability-agent">
            <div class="inner">';

                while( $d = $db->fetch_array( $r ) )
                {
                    $content .= '
                    <div class="item">
                        <div class="desc">
                            <div class="form-inline">
                                <div class="form-group">
                                    <p class="col-md-12">
                                        <strong>Schedule:</strong>
                                        <span>' . date( 'd M y', strtotime( $d['sfrom'] ) ) . ' - ' . date( 'd M y', strtotime( $d['sto'] ) ) . ' / ' . $d['rname'] . '</span>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <p class="col-md-12">
                                        <strong>Boat:</strong>
                                        <span class="boat-name">' . $d['boname'] . '</span>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <p class="col-md-12">
                                        <strong>Total Passenger:</strong>
                                        <span class="boat-passenger">' . $d['bopassenger'] . '</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="container-fluid">
                                <div class="row seven-cols">';

                                    $s2 = 'SELECT
                                            a.aid,
                                            a.sid,
                                            b.agid,
                                            b.dagid,
                                            b.avallot,
                                            c.rid,
                                            c.sfrom,
                                            c.sto
                                          FROM ticket_allotment AS a
                                          LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
                                          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
                                          WHERE a.sid = %d AND b.agid = %d';
                                    $q2 = $db->prepare_query( $s2, $d['sid'], $agid );
                                    $r2 = $db->do_query( $q2 );

                                    if( $db->num_rows( $r2 ) )
                                    {
                                        $d2 = $db->fetch_array( $r2 );

                                        $start = strtotime( date('Y-m-d') );

                                        for( $i = 0; $i <= 6 ; $i++ )
                                        {
                                            $time   = strtotime( '+' . $i . ' day', $start );
                                            $from   = strtotime( $d['sfrom'] );
                                            $to     = strtotime( $d['sto'] );

                                            if( ( $time >= $from ) && ( $time <= $to ) )
                                            {
                                                $time  = strtotime( '+' . $i . ' day', $start );
                                                $seat  = get_total_seat_capacity( $d2['rid'], $time );

                                                // <p><strong>' . get_seat_availibility_num_by_schedule( $seat, $d2['rid'], $d2['sid'], $time, $agid ) . '</strong> / ' . $d2['avallot'] . '</p>
                                                $content .= '
                                                <div class="col col-md-1 nopadding">
                                                    <span>' . date( 'd F, Y', $time ) . '</span>
                                                    <p><strong>'. $d2['avallot'] . '</p>
                                                </div>';
                                            }
                                            else
                                            {
                                                $content .= '
                                                <div class="col col-md-1 nopadding">
                                                    <span>' . date( 'd F, Y', $time ) . '</span>
                                                    <p><strong>0</strong> / 0</p>
                                                </div>';
                                            }
                                        }
                                    }
                                    // <small>remain allotment / daily allotment</small>
                                    $content .= '
                                </div>
                                <div class="row">
                                    <div class="col col-md-12 nopadding">
                                        <small>daily allotment</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
                }

            $content .= '
            </div>
            <div class="inner-nav">
                <nav></nav>
            </div>
        </div>';

        return $content;
    }
    else
    {
        $content = '
        <div class="seat-availability-agent">
            <p class="text-center text-danger">No schedule was found</p>
        </div>';

        return $content;
    }
}

function get_seat_availibility_by_schedule()
{
    global $db;

    $s = 'SELECT
            a.sid,
            b.rname,
            a.sfrom,
            a.sto,
            c.boid,
            c.boname,
            c.bopassenger
          FROM ticket_schedule AS a
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          WHERE a.sstatus = %s AND (( %s BETWEEN a.sfrom AND a.sto ) OR ( %s BETWEEN a.sfrom AND a.sto ))
          ORDER BY a.sfrom, a.rid';
    $q = $db->prepare_query( $s, 'publish', date( 'Y-m-01' ), date( 'Y-m-t' ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $idx         = 0;
        $sid         = '';
        $sfrom       = '';
        $sto         = '';
        $boname      = '';
        $bopassenger = '';
        $boat_option = '';
        $sche_option = '';
        $prio_option = '';
        $boat_arr    = array();

        //-- Boat & Schedule
        while( $d = $db->fetch_array( $r ) )
        {
            if( $idx == 0 )
            {
                $sid         = $d['sid'];
                $boid        = $d['boid'];
                $sfrom       = $d['sfrom'];
                $sto         = $d['sto'];
                $boname      = $d['boname'];
                $bopassenger = $d['bopassenger'];
            }

            if( !in_array( $d['boid'], $boat_arr ) )
            {
                $boat_option .= '
                <option value="' . $d['boid'] . '" ' . ( $boid == $d['boid'] ? 'selected' : '' ) . '>
                    ' . $d['boname'] . '
                </option>';
            }

            $sche_option .= '
            <option value="' . $d['sid'] . '" data-boid="' . $d['boid'] . '" data-name="' . $d['boname'] . '" data-passenger= "' . $d['bopassenger'] . '" data-from="' . $d['sfrom'] . '" data-to="' . $d['sto'] . '" ' . ( $sid == $d['sid'] ? 'selected' : '' ) . '>
                ' . date( 'd M y', strtotime( $d['sfrom'] ) ) . ' - ' . date( 'd M y', strtotime( $d['sto'] ) ) . ' / ' . $d['rname'] .'
            </option>';

            $boat_arr[] = $d['boid'];

            $idx++;
        }

        //-- Period
        $date  = date( 'Y-m-d' );
        $start = strtotime( $date );
        $end   = strtotime( '+3 year', $start );

        $prio_option = '
        <option value="' . date( 'm-Y', $start ) . '">
            ' . date( 'F Y', $start ) . '
        </option>';

        while( $start <= strtotime( '-1 month', $end ) )
        {
            $start = strtotime( '+1 month', $start );
            $value = date( 'm-Y', $start );
            $label = date( 'F Y', $start );

            $prio_option .= '
            <option value="' . $value . '">
                ' . $label . '
            </option>';
        }

        $content = '
        <div class="row">
            <div class="col-md-12">
                <div class="calendar-detail">
                    <p>
                        <strong>Current Schedule:</strong>
                        <select name="period" style="width:200px;">
                            ' . $prio_option . '
                        </select>
                        <select name="trip_boat" style="width:200px;">
                            <option value="">Select Boat</option>
                            ' . $boat_option . '
                        </select>
                        <select name="trip_schedule" style="width:500px;">
                            ' . $sche_option . '
                        </select>
                        <strong>Capacity:</strong>
                        <span class="boat-passenger">' . $bopassenger . '</span>
                        <span>, <i>NB. : available seats / capacity</i></span>
                        <input name="sid" class="sr-only" value="' . $sid . '" />
                        <input name="sto" class="sr-only" value="' . $sto . '" />
                        <input name="sfrom" class="sr-only" value="' . $sfrom . '" />
                        <input name="bopassenger" class="sr-only" value="' . $bopassenger . '" />
                    </p>
                </div>

                <div id="availability-calendar" class="availability-calendar"></div>
            </div>
        </div>';

        return $content;
    }
    else
    {
        //-- Period
        $date  = date( 'Y-m-d' );
        $start = strtotime( $date );
        $end   = strtotime( '+3 year', $start );

        $prio_option = '
        <option value="' . date( 'm-Y', $start ) . '">
            ' . date( 'F Y', $start ) . '
        </option>';

        while( $start <= strtotime( '-1 month', $end ) )
        {
            $start = strtotime( '+1 month', $start );
            $value = date( 'm-Y', $start );
            $label = date( 'F Y', $start );

            $prio_option .= '
            <option value="' . $value . '">
                ' . $label . '
            </option>';
        }

        $content = '
        <div class="row">
            <div class="col-md-12">
                <div class="calendar-detail">
                    <p>
                        <strong>Current Schedule:</strong>
                        <select name="period" style="width:200px;">
                            ' . $prio_option . '
                        </select>
                        <select name="trip_boat" style="width:200px;">
                            <option value="">Select Boat</option>
                        </select>
                        <select name="trip_schedule" style="width:500px;">
                        </select>
                        <strong>Capacity:</strong>
                        <span class="boat-passenger"></span>
                        <span>, <i>NB. : available seats / capacity</i></span>
                        <input name="sid" class="sr-only" value="" />
                        <input name="sto" class="sr-only" value="" />
                        <input name="sfrom" class="sr-only" value="" />
                        <input name="bopassenger" class="sr-only" value="" />
                    </p>
                </div>

                <div id="availability-calendar" class="availability-calendar"></div>
            </div>
        </div>';

        return $content;
    }
}

function get_seat_availibility_by_trip( $is_dashboard = false )
{
    global $db;

    $s = 'SELECT * FROM ticket_route AS a';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        if( $is_dashboard )
        {
            $item  = '';
            $nav   = '';
            $no    = 0;

            while( $d = $db->fetch_array( $r ) )
            {
                $item .= '
                <div role="tabpanel" class="tab-pane ' . ( $no == 0 ? 'active' : '' ) . '" id="route-' . $d['rid'] . '">
                    <div class="container-fluid">
                        <div class="row">';

                        $start = strtotime( date('Y-m-d') );

                        if( $d['rtype'] == '1' && !empty( $d['rhoppingpoint'] ) )
                        {
                            for( $i = 0; $i<=2; $i++)
                            {
                                $time  = strtotime( '+' . $i . ' day', $start );
                                $seat  = get_total_seat_capacity( $d['rid'], $time );
                                $item .= '
                                <div class="col col-md-4 nopadding">
                                    <span>' . date( 'd F Y', $time ) . '</span>
                                    <p>' . get_seat_availibility_num_by_hopping_trip( $seat, $d['rid'], $time ) .'</p>
                                </div>';
                            }
                        }
                        else
                        {
                            for( $i = 0; $i<=2; $i++)
                            {
                                $time  = strtotime( '+' . $i . ' day', $start );
                                $seat  = get_total_seat_capacity( $d['rid'], $time );
                                $item .= '
                                <div class="col col-md-4 nopadding">
                                    <span>' . date( 'd F Y', $time ) . '</span>
                                    <p><strong>' . get_seat_availibility_num_by_trip( $seat, $d['rid'], $time ) . '</strong> / ' . $seat . '</p>
                                </div>';
                            }
                        }

                        $item .= '
                        </div>
                        <div class="row">
                            <div class="col col-md-12 nopadding">
                                <small>available seats / total seats</small>
                            </div>
                        </div>
                    </div>
                </div>';

                $nav .= '
                <li role="presentation" class="' . ( $no == 0 ? 'active' : '' ) . '">
                    <a href="#route-' . $d['rid'] . '" aria-controls="route-' . $d['rid'] . '" role="tab" data-toggle="tab">' . $d['rname'] . '</a>
                </li>';

                $no++;
            }

            $content = '
            <div class="boxs trip-block">
                <div class="tab-content">
                    ' . $item . '
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    ' . $nav . '
                </ul>
            </div>';

            return $content;
        }
        else
        {
            $item  = '';
            $nav   = '';
            $month = date( 'm' );
            $year  = date( 'Y' );
            $no    = 0;

            while( $d = $db->fetch_array( $r ) )
            {
                $item .= '
                <div class="tab-slide ' . ( $no == 0 ? 'active' : '' ) . '" id="route-' . $d['rid'] . '">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="seat-slide" data-type="route">';

                                if( $d['rtype'] == '1' && !empty( $d['rhoppingpoint'] ) )
                                {
                                    for( $day=1; $day<=31; $day++ )
                                    {
                                        $time = mktime( 12, 0, 0, $month, $day, $year );
                                        $seat = get_total_seat_capacity( $d['rid'], $time );

                                        if( date('m', $time) == $month )
                                        {
                                            $item .= '
                                            <div id="route-' . $d['rid'] . '-' . $day . '" class="col nopadding">
                                                <span>' . date( 'd F Y', $time ) . '</span>
                                                <p>' . get_seat_availibility_num_by_hopping_trip( $seat, $d['rid'], $time ) . '</p>
                                            </div>';
                                        }
                                    }
                                }
                                else
                                {
                                    for( $day=1; $day<=31; $day++ )
                                    {
                                        $time = mktime( 12, 0, 0, $month, $day, $year );
                                        $seat = get_total_seat_capacity( $d['rid'], $time );

                                        if( date('m', $time) == $month )
                                        {
                                            $item .= '
                                            <div id="route-' . $d['rid'] . '-' . $day . '" class="col nopadding">
                                                <span>' . date( 'd F Y', $time ) . '</span>
                                                <p><strong>' . get_seat_availibility_num_by_trip( $seat, $d['rid'], $time ) . '</strong> / ' . $seat . '</p>
                                            </div>';
                                        }
                                    }
                                }

                                $item .= '
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-md-12 nopadding">
                                <p class="text-center"><small>available seats / total seats</small></p>
                            </div>
                        </div>
                    </div>
                </div>';

                $nav .= '
                <li role="presentation" class="' . ( $no == 0 ? 'active' : '' ) . '">
                    <a href="#route-' . $d['rid'] . '" aria-controls="route-' . $d['rid'] . '" role="tab" data-toggle="tab">' . $d['rname'] . '</a>
                </li>';

                $no++;
            }

            $content = '
            <div class="boxs boat-block">
                <div class="tab-slide-content">
                    ' . $item . '
                </div>
            </div>
            <ul class="nav nav-tabs">
                ' . $nav . '
            </ul>';
        }

        return $content;
    }
}

function get_total_agent_daily_allotment( $id, $time )
{
    global $db;

    $s = 'SELECT
            a.sid,
            c.agid,
            c.avallot,
            c.dagid,
            d.agtopup,
            d.agtopupval
          FROM ticket_schedule AS a
          LEFT JOIN ticket_allotment AS b ON b.sid = a.sid
          LEFT JOIN ticket_allotment_agent AS c ON c.aid = b.aid
          LEFT JOIN ticket_agent AS d ON c.agid = d.agid
          WHERE a.rid = %d AND a.sfrom <= %s AND a.sto >= %s';
    $q = $db->prepare_query( $s, $id, date( 'Y-m-d', $time ), date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );
    $total = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $s2 = 'SELECT a.allotment
               FROM ticket_allotment_detail AS a
               WHERE a.dagid = %d AND a.adate = %s
               ORDER BY a.adid DESC LIMIT 1';
        $q2 = $db->prepare_query( $s2, $d['dagid'], date( 'Y-m-d', $time ) );
        $r2 = $db->do_query( $q2 );
        $d2 = $db->fetch_array( $r2 );

        if( $db->num_rows( $r2 ) > 0 )
        {
            $total[] = $d2['allotment'];
        }
        else
        {
            $total[] = $d['avallot'];
        }
    }

    return array_sum( $total );
}

function get_agent_total_daily_allotment( $id, $time, $by_schedule = false )
{
    global $db;

    $date  = date( 'Y-m-d', $time );
    $total = array();

    if( $by_schedule )
    {
        $s = 'SELECT
                c.agid,
                d.agtopup,
                d.agtopupval,
                IFNULL((
                    SELECT a2.allotment
                    FROM ticket_allotment_detail AS a2
                    WHERE a2.dagid = c.dagid AND a2.adate = %s
                ), c.avallot ) AS total_allot
              FROM ticket_schedule AS a
              LEFT JOIN ticket_allotment AS b ON b.sid = a.sid
              LEFT JOIN ticket_allotment_agent AS c ON c.aid = b.aid
              LEFT JOIN ticket_agent AS d ON d.agid = c.agid
              WHERE a.sid = %d AND a.sfrom <= %s AND a.sto >= %s';
        $q = $db->prepare_query( $s, $date, $id, $date, $date );
    }
    else
    {
        $s = 'SELECT
                c.agid,
                d.agtopup,
                d.agtopupval,
                IFNULL((
                    SELECT a2.allotment
                    FROM ticket_allotment_detail AS a2
                    WHERE a2.dagid = c.dagid AND a2.adate = %s
                ), c.avallot ) AS total_allot
              FROM ticket_schedule AS a
              LEFT JOIN ticket_allotment AS b ON b.sid = a.sid
              LEFT JOIN ticket_allotment_agent AS c ON c.aid = b.aid
              LEFT JOIN ticket_agent AS d ON d.agid = c.agid
              WHERE a.rid = %d AND a.sfrom <= %s AND a.sto >= %s';
        $q = $db->prepare_query( $s, $date, $id, $date, $date );
    }

    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                $total[ $time ][ $id ][ $d['agid'] ] = array( 'allot' => $d['total_allot'], 'taken' => 0, 'topup' => $d['agtopup'], 'val' => $d['agtopupval'] );
            }
        }
    }

    return $total;
}

function get_all_agent_total_daily_allotment( $max, $sid, $time )
{
    global $db;

    $total = array();

    $s = 'SELECT
            a.aid,
            a.sid,
            b.agid,
            b.dagid,
            b.avallot,
            c.rid,
            c.sfrom,
            c.sto,
            d.agtopup,
            d.agtopupval
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          LEFT JOIN ticket_agent AS d ON d.agid = b.agid
          WHERE a.sid = %d';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            if( !empty( $d['agid'] ) )
            {
                $allot = get_seat_availibility_agent_num_by_schedule( $max, $d['rid'], $sid, $time, $d['agid'] );

                $total[ $time ][ $sid ][ $d['agid'] ] = array( 'allot' => $allot, 'taken' => 0, 'topup' => $d['agtopup'], 'val' => $d['agtopupval'] );
            }
        }
    }

    return $total;
}

function get_online_total_daily_allotment( $id, $time )
{
    global $db;

    $date  = date( 'Y-m-d', $time );
    $total = array();

    $s = 'SELECT
            c.avallot,
            IFNULL((
                SELECT a2.allotment
                FROM ticket_allotment_detail_online AS a2
                WHERE a2.daoid = c.daoid AND a2.adate = %s
            ), c.avallot ) AS total_allot
          FROM ticket_schedule AS a
          LEFT JOIN ticket_allotment AS b ON b.sid = a.sid
          LEFT JOIN ticket_allotment_online AS c ON c.aid = b.aid
          WHERE a.sid = %d AND a.sfrom <= %s AND a.sto >= %s  AND c.aid IS NOT NULL';
    $q = $db->prepare_query( $s, $date, $id, $date, $date );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                if( !empty( $d['total_allot'] ) )
                {
                    $total[ $time ][ $id ] = array( 'avallot' => $d['avallot'], 'allot' => $d['total_allot'], 'taken' => 0 );
                }
            }
        }
    }

    return $total;
}

function get_total_agent_allot_num( $data, $id, $time )
{
    $total_agent_allot = 0;

    if( isset( $data[ $time ][ $id ] ) )
    {
        foreach( $data[ $time ][ $id ] as $agid => $obj )
        {
            $total_agent_allot = $total_agent_allot + $obj['allot'];
        }
    }

    return $total_agent_allot;
}

function get_total_online_allot_num( $data, $id, $time )
{
    $total_online_allot = 0;

    if( isset( $data[ $time ][ $id ] ) )
    {
        $total_online_allot = $total_online_allot + $data[ $time ][ $id ]['allot'];
    }

    return $total_online_allot;
}

function get_seat_availibility_num_by_boat( $max, $id, $time )
{
    global $db;

    $rid    = get_route_by_boat_and_depart_date( $id, $time );
    $remain = $max - get_total_agent_daily_allotment( $rid, $time );

    if( $remain < 0 )
    {
        return 0;
    }
    else
    {
        $s = 'SELECT
                b.rid,
                b.num_adult,
                b.num_child,
                b.num_infant
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              WHERE b.boid = %d AND b.bddate = %s AND a.agid IS NULL AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar"';
        $q = $db->prepare_query( $s, $id, date( 'Y-m-d', $time ) );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $seat = 0;

            while( $d = $db->fetch_array( $r ) )
            {
                $seat = $seat + ( $d['num_adult'] + $d['num_child'] + $d['num_infant'] );
            }

            $available = $remain - $seat;

            return $available < 0 ? 0 : $available;
        }
        else
        {
            return $remain;
        }
    }
}

function get_seat_availibility_num_by_hopping_trip( $max, $id, $time, $return_array = false )
{
    global $db;

    $date = date( 'Y-m-d', $time );

    $s = 'SELECT * FROM ticket_schedule AS a LEFT JOIN ticket_route AS b ON a.rid = b.rid WHERE a.rid = %d AND a.sfrom <= %s AND a.sto >= %s';
    $q = $db->prepare_query( $s, $id, $date, $date );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( $db->num_rows( $r ) == 0 )
    {
        return 0;
    }
    else
    {
        extract( $d );

        $total_agent_allot_arr = get_total_all_agent_daily_allotment( $sid, $time );
        $total_agent_allot     = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
        $total_online_allot    = get_seat_availibility_online_num_by_schedule( $max, $rid, $sid, $time );
        $rcapacity             = $max - $total_agent_allot - $total_online_allot;

        $spoint = get_route_point_list( $rid, 0 );
        $epoint = get_route_point_list( $rid, 1 );
        $hpoint = get_route_point_list( $rid, 2 );

        //-- A = spoint
        //-- B = hpoint
        //-- C = epoint
        //-- X = capacity
        $ac_max = 0;
        $ab_max = 0;
        $bc_max = 0;

        //-- Get max capacity from start point to end point
        //-- If BC > AB --> ACmax = X - AB - AC
        //-- If BC > AB --> ACmax = X - BC - AC
        $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true );
        $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true );
        $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true );

        //-- Maximum seat for AC
        if( $ab_seat > $bc_seat )
        {
            $ac_max = $rcapacity - $ac_seat - $ab_seat;
        }
        elseif( $ab_seat < $bc_seat )
        {
            $ac_max = $rcapacity - $ac_seat - $bc_seat;
        }
        else
        {
            $ac_max = $rcapacity - $ab_seat - $ac_seat - $bc_seat;
        }

        if( $ac_max <= 0 )
        {
            $ac_max = 0;
        }

        //-- Get max capacity from start point to hopping point
        //-- ABmax = X - AC - AB
        $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true );
        $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true );

        //-- Maximum seat for AB
        $ab_max = $rcapacity - $ac_seat - $ab_seat;

        if( $ab_max <= 0 )
        {
            $ab_max = 0;
        }

        //-- Get max capacity from hopping point to end point
        //-- BCmax = X - AC - BC
        $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true );
        $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true );

        //-- Maximum seat for BC
        $bc_max = $rcapacity - $ac_seat - $bc_seat;

        if( $bc_max <= 0 )
        {
            $bc_max = 0;
        }

        $spoint_name = get_location( $spoint[0], 'lcalias' );
        $hpoint_name = get_location( $hpoint[0], 'lcalias' );
        $epoint_name = get_location( $epoint[0], 'lcalias' );

        if( $return_array )
        {
            return array(
                array( 'point' => $spoint_name . '-' . $epoint_name, 'seat_max' => $ac_max, 'seat' => $max ),
                array( 'point' => $spoint_name . '-' . $hpoint_name, 'seat_max' => $ab_max, 'seat' => $max ),
                array( 'point' => $hpoint_name . '-' . $epoint_name, 'seat_max' => $bc_max, 'seat' => $max )
            );
        }
        else
        {
            return '
            <span><b>' . $spoint_name . '-' . $epoint_name . ' : ' . $ac_max . '</b> / ' . $max . '</span>
            <span><b>' . $spoint_name . '-' . $hpoint_name . ' : ' . $ab_max . '</b> / ' . $max . '</span>
            <span><b>' . $hpoint_name . '-' . $epoint_name . ' : ' . $bc_max . '</b> / ' . $max . '</span>';
        }
    }
}

function get_seat_availibility_num_by_trip( $max, $id, $time )
{
    global $db;

    $total_agent_allot_arr = get_agent_total_daily_allotment( $id, $time );
    $total_agent_allot     = get_total_agent_allot_num( $total_agent_allot_arr, $id, $time );
    $num_seat_remain       = $max - $total_agent_allot;

    $s = 'SELECT
            a.agid,
            b.rid,
            b.num_adult,
            b.num_child,
            b.num_infant
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          WHERE b.rid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar"';
    $q = $db->prepare_query( $s, $id, date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $taken_pass_seat  = 0;
        $taken_agent_seat = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $num_of_passenger = $d['num_adult'] + $d['num_child'] + $d['num_infant'];

            if( empty( $d['agid'] ) )
            {
                $taken_pass_seat = $taken_pass_seat + $num_of_passenger;
                $num_seat_remain = $num_seat_remain - $num_of_passenger;
            }
            else
            {
                if( isset( $total_agent_allot_arr[ $time ][ $id ][ $d['agid'] ] ) )
                {
                    if( $total_agent_allot_arr[ $time ][ $id ][ $d['agid'] ][ 'topup' ] == 1 )
                    {
                        if( ( $num_seat_remain - $num_of_passenger ) > $total_agent_allot_arr[ $time ][ $id ][ $d['agid'] ][ 'val' ] )
                        {
                            $num_seat_remain  = $num_seat_remain - $num_of_passenger;
                        }
                        else
                        {
                            //-- Change Allotment
                            $taken_agent_seat = $taken_agent_seat + $num_of_passenger;

                            $total_agent_allot_arr[ $time ][ $id ][ $d['agid'] ][ 'allot' ] = $total_agent_allot_arr[ $time ][ $id ][ $d['agid'] ][ 'allot' ] - $num_of_passenger;
                            $total_agent_allot_arr[ $time ][ $id ][ $d['agid'] ][ 'taken' ] = $taken_agent_seat;

                            //-- Recount Total Allotment Agent
                            $total_agent_allot = get_total_agent_allot_num( $total_agent_allot_arr, $id, $time );
                            $num_seat_remain   = ( $max - $total_agent_allot ) - $taken_pass_seat - $taken_agent_seat;
                        }
                    }
                    else
                    {
                        //-- Change Allotment
                        $taken_agent_seat = $taken_agent_seat + $num_of_passenger;

                        $total_agent_allot_arr[ $time ][ $id ][ $d['agid'] ][ 'allot' ] = $total_agent_allot_arr[ $time ][ $id ][ $d['agid'] ][ 'allot' ] - $num_of_passenger;
                        $total_agent_allot_arr[ $time ][ $id ][ $d['agid'] ][ 'taken' ] = $taken_agent_seat;

                        //-- Recount Total Allotment Agent
                        $total_agent_allot = get_total_agent_allot_num( $total_agent_allot_arr, $id, $time );
                        $num_seat_remain   = ( $max - $total_agent_allot ) - $taken_pass_seat - $taken_agent_seat;
                    }
                }
            }
        }

        if( $num_seat_remain < 0 )
        {
            return 0;
        }
        else
        {
            return $num_seat_remain;
        }
    }
    else
    {
        return $num_seat_remain;
    }
}

function is_agent_has_allotment( $agid )
{
    global $db;

    $s = 'SELECT COUNT(a.agid) AS num
          FROM ticket_allotment_agent AS a
          LEFT JOIN ticket_allotment_detail AS b ON b.dagid = a.dagid
          WHERE a.agid = %d';
    $q = $db->prepare_query( $s, $agid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( $d['num'] > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function get_seat_availibility_agent_num_by_schedule( $max, $rid, $sid, $time, $agid )
{
    global $db;

    $total_agent_allot_arr = get_agent_total_daily_allotment( $sid, $time, true );

    if( is_agent_has_allotment( $agid ) )
    {
        if( isset( $total_agent_allot_arr[ $time ][ $sid ][ $agid ] ) === false )
        {
            return 0;
        }
    }

    $total_agent_allot = get_total_agent_allot_num( $total_agent_allot_arr, $sid, $time );

    if( $total_agent_allot == 0 )
    {
        return 0;
    }

    $num_seat_remain   = $max - $total_agent_allot;

    $s = 'SELECT
            a.agid,
            b.rid,
            b.num_adult,
            b.num_child,
            b.num_infant
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          WHERE b.sid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar"';
    $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $taken_pass_seat       = 0;
        $taken_agent_seat      = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $num_of_passenger = $d['num_adult'] + $d['num_child'] + $d['num_infant'];

            if( empty( $d['agid'] ) )
            {
                $taken_pass_seat = $taken_pass_seat + $num_of_passenger;
                $num_seat_remain = $num_seat_remain - $num_of_passenger;
            }
            else
            {
                if( isset( $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ] ) )
                {
                    if( $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'topup' ] == 1 )
                    {
                        if( ( $num_seat_remain - $num_of_passenger ) > $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'val' ] )
                        {
                            $num_seat_remain = $num_seat_remain - $num_of_passenger;
                        }
                        else
                        {
                            //-- Change Allotment
                            $taken_agent_seat = $taken_agent_seat + $num_of_passenger;

                            $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] = $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] - $num_of_passenger;
                            $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'taken' ] = $taken_agent_seat;

                            //-- Recount Total Allotment Agent
                            $total_agent_allot = get_total_agent_allot_num( $total_agent_allot_arr, $sid, $time );
                            $num_seat_remain   = ( $max - $total_agent_allot ) - $taken_pass_seat - $taken_agent_seat;
                        }
                    }
                    else
                    {
                        //-- Change Allotment
                        $taken_agent_seat = $taken_agent_seat + $num_of_passenger;

                        $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] = $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] - $num_of_passenger;
                        $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'taken' ] = $taken_agent_seat;

                        //-- Recount Total Allotment Agent
                        $total_agent_allot = get_total_agent_allot_num( $total_agent_allot_arr, $sid, $time );
                        $num_seat_remain   = ( $max - $total_agent_allot ) - $taken_pass_seat - $taken_agent_seat;
                    }
                }
                else
                {
                    $num_seat_remain = $num_seat_remain - $num_of_passenger;
                }
            }
        }

        if( isset( $total_agent_allot_arr[ $time ][ $sid ][ $agid ][ 'allot' ] ) )
        {
            if( $total_agent_allot_arr[ $time ][ $sid ][ $agid ][ 'allot' ] < 0 )
            {
                return 0;
            }
            else
            {
                return $total_agent_allot_arr[ $time ][ $sid ][ $agid ][ 'allot' ];
            }
        }
        else
        {
            if( $num_seat_remain < 0 )
            {
                return 0;
            }
            else
            {
                return $num_seat_remain;
            }
        }
    }
    else
    {
        if( isset( $total_agent_allot_arr[ $time ][ $sid ][ $agid ][ 'allot' ] ) )
        {
            return $total_agent_allot_arr[ $time ][ $sid ][ $agid ][ 'allot' ];
        }
        else
        {
            if( $num_seat_remain < 0 )
            {
                return 0;
            }
            else
            {
                return $num_seat_remain;
            }
        }
    }
}

function get_taken_seat_online_num_by_schedule( $sid, $time )
{
    global $db;

    $chid = get_meta_data( 'channel_id', 'ticket_setting' );
    $nums = 0;

    $s = 'SELECT
            b.rid,
            b.num_adult,
            b.num_child,
            b.num_infant
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          WHERE b.sid = %d AND a.chid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar"';
    $q = $db->prepare_query( $s, $sid, $chid, date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $nums += $d['num_adult'] + $d['num_child'] + $d['num_infant'];
        }
    }

    return $nums;
}

function get_seat_availibility_online_num_by_schedule( $max, $rid, $sid, $time, $from_agent_panel = false )
{
    global $db;

    $total_online_allot_arr = get_online_total_daily_allotment( $sid, $time );
    $total_online_allot     = get_total_online_allot_num( $total_online_allot_arr, $sid, $time );
    $num_seat_remain        = $total_online_allot;

    $chid = get_meta_data( 'channel_id', 'ticket_setting' );

    $s = 'SELECT
            b.rid,
            b.num_adult,
            b.num_child,
            b.num_infant
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          WHERE b.sid = %d AND a.chid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar"';
    $q = $db->prepare_query( $s, $sid, $chid, date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $num_of_passenger = $d['num_adult'] + $d['num_child'] + $d['num_infant'];
            $num_seat_remain  = $num_seat_remain - $num_of_passenger;
        }
    }

    if( $num_seat_remain < 0 )
    {
        return 0;
    }
    else
    {
        return $num_seat_remain;
    }
}

function get_seat_availibility_num_by_schedule( $max, $rid, $sid, $time, $agid = '', $from_agent_panel = false, $online = false, $bdid = null )
{
    global $db;

    $chid = get_meta_data( 'channel_id', 'ticket_setting' );

    $total_agent_allot_arr = get_all_agent_total_daily_allotment( $max, $sid, $time );

    if( $from_agent_panel === true )
    {
        if( is_agent_has_allotment( $agid ) )
        {
            if( !isset( $total_agent_allot_arr[ $time ][ $sid ][ $agid ] ) )
            {
                return 0;
            }
        }
    }

    $not_set_allotment = false;

    if( $online )
    {
        $total_online_allot_arr = get_online_total_daily_allotment( $sid, $time );

        if( isset( $total_online_allot_arr[ $time ][ $sid ] ) )
        {
            //-- If allotment set = 0 get global avaibility
            if( $total_online_allot_arr[ $time ][ $sid ][ 'avallot' ] == 0 )
            {
                $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $sid, $time );
                $total_online_allot = get_seat_availibility_online_num_by_schedule( $max, $rid, $sid, $time, $from_agent_panel );
                $num_seat_remain    = $max - $total_agent_allot - $total_online_allot;

                if( is_null( $bdid ) )
                {
                    $s = 'SELECT
                            a.chid,
                            a.agid,
                            b.rid,
                            b.num_adult,
                            b.num_child,
                            b.num_infant
                          FROM ticket_booking AS a
                          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                          WHERE b.sid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar"';
                    $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', $time ) );
                    $r = $db->do_query( $q );
                }
                else
                {
                    $s = 'SELECT
                            a.chid,
                            a.agid,
                            b.rid,
                            b.num_adult,
                            b.num_child,
                            b.num_infant
                          FROM ticket_booking AS a
                          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                          WHERE b.sid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar" AND b.bdid <> %d';
                    $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', $time ), $bdid );
                    $r = $db->do_query( $q );
                }

                $not_set_allotment = true;
            }
            else
            {
                $total_online_allot = get_total_online_allot_num( $total_online_allot_arr, $sid, $time );
                $num_seat_remain    = $total_online_allot;

                if( is_null( $bdid ) )
                {
                    $s = 'SELECT
                            a.chid,
                            a.agid,
                            b.rid,
                            b.num_adult,
                            b.num_child,
                            b.num_infant
                          FROM ticket_booking AS a
                          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                          WHERE b.sid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar" AND a.chid = %d';
                    $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', $time ), $chid );
                    $r = $db->do_query( $q );
                }
                else
                {
                    $s = 'SELECT
                            a.chid,
                            a.agid,
                            b.rid,
                            b.num_adult,
                            b.num_child,
                            b.num_infant
                          FROM ticket_booking AS a
                          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                          WHERE b.sid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar" AND a.chid = %d AND b.bdid <> %d';
                    $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', $time ), $chid, $bdid );
                    $r = $db->do_query( $q );
                }
            }
        }
        else
        {
            //-- If allotment not set yet get global avaibility
            $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $sid, $time );
            $total_online_allot = get_seat_availibility_online_num_by_schedule( $max, $rid, $sid, $time, $from_agent_panel );
            $num_seat_remain    = $max - $total_agent_allot - $total_online_allot;

            if( is_null( $bdid ) )
            {
                $s = 'SELECT
                        a.chid,
                        a.agid,
                        b.rid,
                        b.num_adult,
                        b.num_child,
                        b.num_infant
                      FROM ticket_booking AS a
                      LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                      WHERE b.sid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar"';
                $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', $time ) );
                $r = $db->do_query( $q );
            }
            else
            {
                $s = 'SELECT
                        a.chid,
                        a.agid,
                        b.rid,
                        b.num_adult,
                        b.num_child,
                        b.num_infant
                      FROM ticket_booking AS a
                      LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                      WHERE b.sid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar" AND b.bdid <> %d';
                $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', $time ), $bdid );
                $r = $db->do_query( $q );
            }

            $not_set_allotment = true;
        }
    }
    else
    {
        $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $sid, $time );
        $total_online_allot = get_seat_availibility_online_num_by_schedule( $max, $rid, $sid, $time, $from_agent_panel );
        $num_seat_remain    = $max - $total_agent_allot - $total_online_allot;

        if( is_null( $bdid ) )
        {
            $s = 'SELECT
                    a.chid,
                    a.agid,
                    b.rid,
                    b.num_adult,
                    b.num_child,
                    b.num_infant
                  FROM ticket_booking AS a
                  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                  WHERE b.sid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar"';
            $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', $time ) );
            $r = $db->do_query( $q );
        }
        else
        {
            $s = 'SELECT
                    a.chid,
                    a.agid,
                    b.rid,
                    b.num_adult,
                    b.num_child,
                    b.num_infant
                  FROM ticket_booking AS a
                  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                  WHERE b.sid = %d AND b.bddate = %s AND b.bdpstatus NOT IN( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar" AND b.bdid <> %d';
            $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', $time ), $bdid );
            $r = $db->do_query( $q );
        }
    }

    if( $db->num_rows( $r ) > 0 )
    {
        $taken_global_seat = 0;
        $taken_online_seat = 0;
        $taken_agent_seat  = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $num_of_passenger = $d['num_adult'] + $d['num_child'] + $d['num_infant'];

            if( empty( $d['agid'] ) )
            {
                if( $d['chid'] == $chid  )
                {
                    $taken_online_seat += $num_of_passenger;
                }
                else
                {
                    $taken_global_seat += $num_of_passenger;
                }
            }
            else
            {
                if( isset( $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ] ) )
                {
                    if( $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'topup' ] == 1 )
                    {
                        if( ( $num_seat_remain - $num_of_passenger ) > $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'val' ] )
                        {
                            $taken_global_seat += $num_of_passenger;
                        }
                        else
                        {
                            //-- If booker is agent and booked from agent panel then change the allotment and recount total allotment
                            if( empty( $agid ) )
                            {
                                $taken_global_seat += $num_of_passenger;
                            }
                            else
                            {
                                if( $from_agent_panel )
                                {
                                    if( $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] >= $num_of_passenger )
                                    {
                                        //-- Change Allotment
                                        $taken_agent_seat += $num_of_passenger;

                                        $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] = $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] - $num_of_passenger;
                                        $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'taken' ] = $taken_agent_seat;
                                    }
                                    else
                                    {
                                        $taken_global_seat += $num_of_passenger;
                                    }
                                }
                                else
                                {
                                    $taken_global_seat += $num_of_passenger;
                                }
                            }
                        }
                    }
                    else
                    {
                        //-- If booker is agent and booked from agent panel then change the allotment and recount total allotment
                        if( empty( $agid ) )
                        {
                            $taken_global_seat += $num_of_passenger;
                        }
                        else
                        {
                            if( $from_agent_panel )
                            {
                                if( $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] >= $num_of_passenger )
                                {
                                    //-- Change Allotment
                                    $taken_agent_seat += $num_of_passenger;

                                    $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] = $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'allot' ] - $num_of_passenger;
                                    $total_agent_allot_arr[ $time ][ $sid ][ $d['agid'] ][ 'taken' ] = $taken_agent_seat;
                                }
                                else
                                {
                                    $taken_global_seat += $num_of_passenger;
                                }
                            }
                            else
                            {
                                $taken_global_seat += $num_of_passenger;
                            }
                        }
                    }
                }
                else
                {
                    $taken_global_seat += $num_of_passenger;
                }
            }

            if( $online && $not_set_allotment === false )
            {
                $num_seat_remain = $total_online_allot - $taken_online_seat;
            }
            else
            {
                $num_seat_remain = $max - $total_agent_allot - $total_online_allot - ( $taken_global_seat + $taken_online_seat + $taken_agent_seat );
            }
        }
    }

    if( empty( $agid ) )
    {
        if( $num_seat_remain <= 0 )
        {
            return 0;
        }
        else
        {
            return $num_seat_remain;
        }
    }
    else
    {
        if( isset( $total_agent_allot_arr[ $time ][ $sid ][ $agid ][ 'allot' ] ) && $from_agent_panel )
        {
            if( $total_agent_allot_arr[ $time ][ $sid ][ $agid ][ 'allot' ] <= 0 )
            {
                return 0;
            }
            else
            {
                return $total_agent_allot_arr[ $time ][ $sid ][ $agid ][ 'allot' ];
            }
        }
        else
        {
            if( $from_agent_panel )
            {
                if( $time <= strtotime( '2018-09-30' ) )
                {
                    return $total_online_allot;
                }
                else
                {
                    if( $num_seat_remain <= 0 )
                    {
                        return 0;
                    }
                    else
                    {
                        return $num_seat_remain;
                    }
                }
            }
            else
            {
                if( $num_seat_remain <= 0 )
                {
                    return 0;
                }
                else
                {
                    return $num_seat_remain;
                }
            }
        }
    }
}

function get_route_by_boat_and_depart_date( $boid, $time )
{
    global $db;

    $s = 'SELECT a.rid
          FROM ticket_schedule AS a
          WHERE a.boid = %d AND a.sfrom <= %s AND a.sto >= %s';
    $q = $db->prepare_query( $s, $boid, date( 'Y-m-d', $time ), date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );
    return $d['rid'];
}

function get_total_seat_capacity( $rid, $time )
{
	global $db;

	$t = date( 'Y-m-d', $time );

    $s = 'SELECT
            a.boid,
            c.bopassenger
          FROM ticket_schedule AS a
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          WHERE b.rid = %d AND a.sfrom <= %s AND a.sto >= %s AND b.rstatus = %s';
    $q = $db->prepare_query( $s, $rid, $t, $t, 'publish' );
    $r = $db->do_query( $q );

    $seat = 0;

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $boat = get_boat( $d['boid'] );
            $seat = intval( $seat ) + intval( $boat['bopassenger'] );
        }
    }

    return $seat;
}

function get_departure_availability_result( $post, $agid = '', $from_agent_panel = false , $chid = '' )
{
	global $db;

	extract( $post );

    $dep_date = date( 'Y-m-d', strtotime( $date_of_depart ) );
    $pass_num = $adult_num + $child_num + $infant_num;
    $frmadmin = $from_agent_panel ? false : true;

	$s = 'SELECT * FROM ticket_schedule AS a
		  LEFT JOIN ticket_route AS b ON a.rid = b.rid
		  LEFT JOIN ticket_boat AS c ON a.boid = c.boid
		  WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
	$q = $db->prepare_query( $s, $dep_date, $dep_date, 'publish', 'publish', '1' );
    $r = $db->do_query( $q );

    set_template( PLUGINS_PATH . '/ticket/tpl/availability/search-result-loop.html', 'availability-depart-loop' );

    add_block( 'search-suggestion-result-loop-block', 'dssrlblock', 'availability-depart-loop' );
    add_block( 'search-suggestion-result-block', 'dssrblock', 'availability-depart-loop' );
    add_block( 'search-empty-loop-block', 'dselblock', 'availability-depart-loop' );
    add_block( 'search-result-loop-block', 'dsrlblock', 'availability-depart-loop' );

    if( $db->num_rows( $r ) > 0 )
    {
	    $i = 0;
        $o = 0;

        $schedule = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $schedule[] = $d;

            $not_closed_date   = is_date_not_in_close_allotment_list( $agid, $d['sid'], $dep_date );
            $available_depart  = is_available_depart_on_list( $d['rid'], $depart_port, $dep_date );
            $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_port );
            $available_date    = is_available_date_on_list( $d['sid'], $dep_date, $frmadmin );

            if( $from_agent_panel )
            {
                $not_pass_time = is_not_pass_time_limit( $d['rid'], $post, 'depart' );
            }
            else
            {
                $not_pass_time = true;
            }

        	if( $available_depart && $available_arrival && $available_date && $not_pass_time )
        	{
        		$p = get_result_price( $d['sid'], $d['rid'], $dep_date, $depart_port, $destination_port, $adult_num, $child_num, $infant_num, $agid, null, $chid);

                $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
    			$exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                $remain_seat = remain_seat( $dep_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $depart_port, $destination_port, $agid, $from_agent_panel );
                //-- $num_of_seat = $remain_seat - $pass_num;
                $num_of_seat = $remain_seat;
        		if( !empty( $inc_total_price ) || !empty( $exc_total_price ) )
        		{
                    if( $from_agent_panel === true && is_block_request_transport( $d['rid'], $post, 'depart' ) )
                    {
                        add_variable( 'include_trans_css', 'sr-only' );
                        add_variable( 'exclude_trans_css', '' );
                    }
                    else
                    {
                        add_variable( 'include_trans_css', '' );
                        add_variable( 'exclude_trans_css', 'sr-only' );
                    }

		        	add_variable( 'sid', $d['sid'] );
		        	add_variable( 'rid', $d['rid'] );
		        	add_variable( 'boid', $d['boid'] );
		        	add_variable( 'boname', $d['boname'] );
		        	add_variable( 'radio_name', 'dep_rid' );
                    add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );
                    add_variable( 'select_trip_btn_cls', $from_agent_panel ? '' : 'hidden' );
                    //-- add_variable( 'closed_attr', $not_closed_date && $remain_seat > 0 && $num_of_seat >= 0 ? '' : 'disabled' );
                    add_variable( 'closed_attr', $not_closed_date && $remain_seat > 0 && $remain_seat - $pass_num >= 0 ? '' : 'disabled' );

		        	add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
		        	add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
		        	add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

		        	add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
		        	add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
		        	add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                    add_variable( 'port_name', get_route_first_port( $d['rid'], $depart_port ) );

                    if( $num_of_seat < 7 )
                    {
                        if( $num_of_seat <= 0 )
                        {
                            add_variable( 'remain_seat', 'No seat left' );
                        }
                        elseif( $num_of_seat == 1 )
                        {
                            add_variable( 'remain_seat', '1 seat left' );
                        }
                        else
                        {
                            add_variable( 'remain_seat', $num_of_seat . ' seats left' );
                        }

                        add_variable( 'remain_seat_css', '' );
                    }
                    else
                    {
                        add_variable( 'remain_seat', '' );
                        add_variable( 'remain_seat_css', 'sr-only' );
                    }

                    if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                    {
                        if( !empty( $p['early_discount'] ) )
                        {
                            if( $p['early_discount']['type'] == '0' )
                            {
                                $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                            }
                            else
                            {
                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                            }

                            add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                        }

                        if( !empty( $p['seat_discount'] ) )
                        {
                            if( $p['seat_discount']['type'] == '0' )
                            {
                                $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                            }
                            else
                            {
                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                            }

                            add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                        }

                        add_variable( 'inc_disc_price', $inc_disc_price );
                        add_variable( 'exc_disc_price', $exc_disc_price );

                        add_variable( 'inc_disc_adult', $inc_disc_adult );
                        add_variable( 'inc_disc_child', $inc_disc_child );
                        add_variable( 'inc_disc_infant', $inc_disc_infant );

                        add_variable( 'exc_disc_adult', $exc_disc_adult );
                        add_variable( 'exc_disc_child', $exc_disc_child );
                        add_variable( 'exc_disc_infant', $exc_disc_infant );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                        add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                    }
                    elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                    {
                        if( $p['agent_discount']['type'] == '0' )
                        {
                            $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                            $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                            $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                            $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                            $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                            $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                            $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                            $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                        }
                        else
                        {
                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                            $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                            $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                        }

                        add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                

                        add_variable( 'inc_disc_price', $inc_disc_price );
                        add_variable( 'exc_disc_price', $exc_disc_price );

                        add_variable( 'inc_disc_adult', $inc_disc_adult );
                        add_variable( 'inc_disc_child', $inc_disc_child );
                        add_variable( 'inc_disc_infant', $inc_disc_infant );

                        add_variable( 'exc_disc_adult', $exc_disc_adult );
                        add_variable( 'exc_disc_child', $exc_disc_child );
                        add_variable( 'exc_disc_infant', $exc_disc_infant );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                        add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                    }
                    else
                    {
                        add_variable( 'discount_notif', '' );

                        add_variable( 'inc_disc_price', 0 );
                        add_variable( 'exc_disc_price', 0 );

                        add_variable( 'inc_disc_adult', 0 );
                        add_variable( 'inc_disc_child', 0 );
                        add_variable( 'inc_disc_infant', 0 );

                        add_variable( 'exc_disc_adult', 0 );
                        add_variable( 'exc_disc_child', 0 );
                        add_variable( 'exc_disc_infant', 0 );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '' );
                        add_variable( 'exclude_price_disc_number', '' );
                    }

                    add_variable( 'rtdid', $p['rtdid'] );

                    add_variable( 'inc_total_price', $inc_total_price );
                    add_variable( 'exc_total_price', $exc_total_price );

                    add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                    add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

					add_variable( 'passenger_number', get_passenger_num_content( $adult_num, $child_num, $infant_num ) );
                    add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $depart_port, $destination_port ) );

                    parse_template( 'search-result-loop-block', 'dsrlblock', true );

                    if( $remain_seat > 0 )
                    {
                        $i++;
                    }
                    else
                    {
                        $o++;
                    }
	        	}
	        }
        }

        if( empty( $i )  )
        {
            $i2 = 0;

            $suggest_post_data = $post;

            if( empty( $suggest_depart_port ) === false )
            {
                foreach( $suggest_depart_port as $stype => $sdepart_port )
                {
                    foreach( $sdepart_port as $idx => $sport )
                    {
                        foreach( $schedule as $sc )
                        {
                            if( $stype == 'from' )
                            {
                                $suggest_post_data['depart_port'] = $sport;
                            }
                            elseif( $stype == 'to' )
                            {
                                $suggest_post_data['destination_port'] = $sport;
                            }

                            $not_closed_date   = is_date_not_in_close_allotment_list( $agid, $sc['sid'], $dep_date );
                            $available_depart  = is_available_depart_on_list( $sc['rid'], $suggest_post_data['depart_port'], $dep_date );
                            $available_arrival = is_available_arrival_on_list( $sc['rid'], $suggest_post_data['destination_port'] );
                            $available_date    = is_available_date_on_list( $sc['sid'], $dep_date, $frmadmin );

                            if( $from_agent_panel )
                            {
                                $not_pass_time = is_not_pass_time_limit( $sc['rid'], $suggest_post_data, 'depart' );
                            }
                            else
                            {
                                $not_pass_time = true;
                            }

                            if( $available_depart && $available_arrival && $available_date && $not_pass_time )
                            {
                                $sremain_seat = remain_seat( $dep_date, $sc['sid'], $sc['rid'], $pass_num, $sc['bopassenger'], $suggest_post_data['depart_port'], $suggest_post_data['destination_port'], $agid, $from_agent_panel );
                                //--$snum_of_seat = $sremain_seat - $pass_num;
                                $snum_of_seat = $sremain_seat;
                                if( $sremain_seat > 0 )
                                {
                                    $p = get_result_price( $sc['sid'], $sc['rid'], $dep_date, $suggest_post_data['depart_port'], $suggest_post_data['destination_port'], $adult_num, $child_num, $infant_num, $agid, null, $chid);

                                    $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                                    $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                                    if( !empty( $inc_total_price ) || !empty( $exc_total_price ) )
                                    {
                                        if( $from_agent_panel === true && is_block_request_transport( $sc['rid'], $suggest_post_data, 'depart' ) )
                                        {
                                            add_variable( 'include_trans_css', 'sr-only' );
                                            add_variable( 'exclude_trans_css', '' );
                                        }
                                        else
                                        {
                                            add_variable( 'include_trans_css', '' );
                                            add_variable( 'exclude_trans_css', 'sr-only' );
                                        }

                                        add_variable( 'sid', $sc['sid'] );
                                        add_variable( 'rid', $sc['rid'] );
                                        add_variable( 'boid', $sc['boid'] );
                                        add_variable( 'boname', $sc['boname'] );
                                        add_variable( 'radio_name', 'dep_rid' );

                                        add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                        add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );
                                        add_variable( 'select_trip_btn_cls', $from_agent_panel ? '' : 'hidden' );

                                        add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                        add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                        add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                        add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                        add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                        add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                        add_variable( 'port_name', get_route_first_port( $sc['rid'], $suggest_post_data['depart_port'] ) );

                                        if( $snum_of_seat < 7 )
                                        {
                                            if( $snum_of_seat <= 0 )
                                            {
                                                aadd_variable( 'remain_seat', 'No seat left' );
                                            }
                                            elseif( $snum_of_seat == 1 )
                                            {
                                                add_variable( 'remain_seat', '1 seat left' );
                                            }
                                            else
                                            {
                                                add_variable( 'remain_seat', $snum_of_seat . ' seats left' );
                                            }

                                            add_variable( 'remain_seat_css', '' );
                                        }
                                        else
                                        {
                                            add_variable( 'remain_seat', '' );
                                            add_variable( 'remain_seat_css', 'sr-only' );
                                        }

                                        if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                                        {
                                            if( !empty( $p['early_discount'] ) )
                                            {
                                                if( $p['early_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                            }

                                            if( !empty( $p['seat_discount'] ) )
                                            {
                                                if( $p['seat_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                            }

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }                                        
                                        elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                                        {
                                            if( $p['agent_discount']['type'] == '0' )
                                            {
                                                $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                                                $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                                            }
                                            else
                                            {
                                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                
                                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                            }

                                            add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }
                                        else
                                        {
                                            add_variable( 'discount_notif', '' );

                                            add_variable( 'inc_disc_price', 0 );
                                            add_variable( 'exc_disc_price', 0 );

                                            add_variable( 'inc_disc_adult', 0 );
                                            add_variable( 'inc_disc_child', 0 );
                                            add_variable( 'inc_disc_infant', 0 );

                                            add_variable( 'exc_disc_adult', 0 );
                                            add_variable( 'exc_disc_child', 0 );
                                            add_variable( 'exc_disc_infant', 0 );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '' );
                                            add_variable( 'exclude_price_disc_number', '' );
                                        }

                                        add_variable( 'rtdid', $p['rtdid'] );

                                        add_variable( 'inc_total_price', $inc_total_price );
                                        add_variable( 'exc_total_price', $exc_total_price );

                                        add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                        add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                                        add_variable( 'passenger_number', get_passenger_num_content( $adult_num, $child_num, $infant_num ) );
                                        add_variable( 'route_list', get_route_detail_list_content( $sc['rid'], $suggest_post_data['depart_port'], $suggest_post_data['destination_port'] ) );

                                        parse_template( 'search-suggestion-result-loop-block', 'dssrlblock', true );

                                        $i2++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if( $i2 > 0 )
            {
                add_variable( 'sugested_routes', get_location( $suggest_post_data['depart_port'], 'lcname' ) . ' to ' . get_location( $suggest_post_data['destination_port'], 'lcname' ) );

                parse_template( 'search-suggestion-result-block', 'dssrblock' );
            }

            if( empty( $o ) )
            {
                parse_template( 'search-empty-loop-block', 'dselblock' );
            }
        }
    }
    else
    {
        parse_template( 'search-empty-loop-block', 'dselblock' );
    }

    return return_template( 'availability-depart-loop' );
}

function get_return_availability_result( $post, $agid = '', $from_agent_panel = false , $chid ='')
{
	global $db;

	extract( $post );

    $rtn_date = date( 'Y-m-d', strtotime( $date_of_return ) );
    $pass_num = $adult_num + $child_num + $infant_num;
    $frmadmin = $from_agent_panel ? false : true;

	$s = 'SELECT * FROM ticket_schedule AS a
		  LEFT JOIN ticket_route AS b ON a.rid = b.rid
		  LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
	$q = $db->prepare_query( $s, $rtn_date, $rtn_date, 'publish', 'publish', '1' );
    $r = $db->do_query( $q );

    set_template( PLUGINS_PATH . '/ticket/tpl/availability/search-result-loop.html', 'availability-return-loop' );

    add_block( 'search-suggestion-result-loop-block', 'rssrlblock', 'availability-return-loop' );
    add_block( 'search-suggestion-result-block', 'rssrblock', 'availability-return-loop' );
    add_block( 'search-empty-loop-block', 'rselblock', 'availability-return-loop' );
    add_block( 'search-result-loop-block', 'rsrlblock', 'availability-return-loop' );

    if( $db->num_rows( $r ) > 0 )
    {
        $i = 0;
        $o = 0;

        $schedule = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $schedule[] = $d;

            $not_closed_date   = is_date_not_in_close_allotment_list( $agid, $d['sid'], $rtn_date );
            $available_depart  = is_available_depart_on_list( $d['rid'], $return_port, $rtn_date );
            $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_port_rtn );
            $available_date    = is_available_date_on_list( $d['sid'], $rtn_date, $frmadmin );
            
            $remain_seat = remain_seat( $rtn_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $return_port, $destination_port_rtn, $agid, $from_agent_panel );
            //-- $num_of_seat = $remain_seat - $pass_num;
            $num_of_seat = $remain_seat;
            
            if( $from_agent_panel )
            {
                $not_pass_time = is_not_pass_time_limit( $d['rid'], $post, 'return' );
            }
            else
            {
                $not_pass_time = true;
            }

            if( $available_depart && $available_arrival && $available_date && $not_pass_time && $num_of_seat > 0)
            {
        		$p = get_result_price( $d['sid'], $d['rid'], $rtn_date, $return_port, $destination_port_rtn, $adult_num, $child_num, $infant_num, $agid, null, $chid);

                $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                $remain_seat = remain_seat( $rtn_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $return_port, $destination_port_rtn, $agid, $from_agent_panel );
                //-- $num_of_seat = $remain_seat - $pass_num;
                $num_of_seat = $remain_seat;
        		if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
        		{
                    if( $from_agent_panel === true && is_block_request_transport( $d['rid'], $post, 'return' ) )
                    {
                        add_variable( 'include_trans_css', 'sr-only' );
                    }
                    else
                    {
                        add_variable( 'include_trans_css', '' );
                    }

		        	add_variable( 'sid', $d['sid'] );
		        	add_variable( 'rid', $d['rid'] );
		        	add_variable( 'boid', $d['boid'] );
		        	add_variable( 'boname', $d['boname'] );
		        	add_variable( 'radio_name', 'rtn_rid' );

                    add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );
                    add_variable( 'select_trip_btn_cls', $from_agent_panel ? '' : 'hidden' );
                    //-- add_variable( 'closed_attr', $not_closed_date && $remain_seat > 0 && $num_of_seat >= 0 ? '' : 'disabled' );
                    add_variable( 'closed_attr', $not_closed_date && $remain_seat > 0 && $remain_seat - $pass_num >= 0 ? '' : 'disabled' );

                    add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                    add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                    add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                    add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                    add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                    add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                    add_variable( 'port_name', get_route_first_port( $d['rid'], $return_port ) );

                    if( $num_of_seat < 7 )
                    {
                        if( $num_of_seat <= 0 )
                        {
                            add_variable( 'remain_seat', 'No seat left' );
                        }
                        elseif( $num_of_seat == 1 )
                        {
                            add_variable( 'remain_seat', '1 seat left' );
                        }
                        else
                        {
                            add_variable( 'remain_seat', $num_of_seat . ' seats left' );
                        }

                        add_variable( 'remain_seat_css', '' );
                    }
                    else
                    {
                        add_variable( 'remain_seat', '' );
                        add_variable( 'remain_seat_css', 'sr-only' );
                    }

                    if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                    {
                        if( !empty( $p['early_discount'] ) )
                        {
                            if( $p['early_discount']['type'] == '0' )
                            {
                                $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                            }
                            else
                            {
                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                            }

                            add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                        }

                        if( !empty( $p['seat_discount'] ) )
                        {
                            if( $p['seat_discount']['type'] == '0' )
                            {
                                $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                            }
                            else
                            {
                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                            }

                            add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                        }

                        add_variable( 'inc_disc_price', $inc_disc_price );
                        add_variable( 'exc_disc_price', $exc_disc_price );

                        add_variable( 'inc_disc_adult', $inc_disc_adult );
                        add_variable( 'inc_disc_child', $inc_disc_child );
                        add_variable( 'inc_disc_infant', $inc_disc_infant );

                        add_variable( 'exc_disc_adult', $exc_disc_adult );
                        add_variable( 'exc_disc_child', $exc_disc_child );
                        add_variable( 'exc_disc_infant', $exc_disc_infant );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                        add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                    }
                    elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                    {
                        if( $p['agent_discount']['type'] == '0' )
                        {
                            $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                            $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                            $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                            $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                            $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                            $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                            $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                            $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                        }
                        else
                        {
                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                            $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                            $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                        }

                        add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                

                        add_variable( 'inc_disc_price', $inc_disc_price );
                        add_variable( 'exc_disc_price', $exc_disc_price );

                        add_variable( 'inc_disc_adult', $inc_disc_adult );
                        add_variable( 'inc_disc_child', $inc_disc_child );
                        add_variable( 'inc_disc_infant', $inc_disc_infant );

                        add_variable( 'exc_disc_adult', $exc_disc_adult );
                        add_variable( 'exc_disc_child', $exc_disc_child );
                        add_variable( 'exc_disc_infant', $exc_disc_infant );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                        add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                        add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                        add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                    }
                    else
                    {
                        add_variable( 'discount_notif', '' );

                        add_variable( 'inc_disc_price', 0 );
                        add_variable( 'exc_disc_price', 0 );

                        add_variable( 'inc_disc_adult', 0 );
                        add_variable( 'inc_disc_child', 0 );
                        add_variable( 'inc_disc_infant', 0 );

                        add_variable( 'exc_disc_adult', 0 );
                        add_variable( 'exc_disc_child', 0 );
                        add_variable( 'exc_disc_infant', 0 );

                        add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                        add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                        add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                        add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                        add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                        add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                        add_variable( 'inc_net_adult_price', $p['inc_trans']['adult']['net_price'] );
                        add_variable( 'inc_net_child_price', $p['inc_trans']['child']['net_price'] );
                        add_variable( 'inc_net_infant_price', $p['inc_trans']['infant']['net_price'] );

                        add_variable( 'exc_net_adult_price', $p['exc_trans']['adult']['net_price'] );
                        add_variable( 'exc_net_child_price', $p['exc_trans']['child']['net_price'] );
                        add_variable( 'exc_net_infant_price', $p['exc_trans']['infant']['net_price'] );

                        add_variable( 'include_price_number', $inc_total_price );
                        add_variable( 'exclude_price_number', $exc_total_price );

                        add_variable( 'include_price_disc_number', '' );
                        add_variable( 'exclude_price_disc_number', '' );
                    }

                    add_variable( 'rtdid', $p['rtdid'] );

                    add_variable( 'inc_total_price', $inc_total_price );
                    add_variable( 'exc_total_price', $exc_total_price );

                    add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                    add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                    add_variable( 'passenger_number', get_passenger_num_content( $adult_num, $child_num, $infant_num ) );
                    add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $return_port, $destination_port_rtn ) );

                    parse_template( 'search-result-loop-block', 'rsrlblock', true );

                    if( $remain_seat > 0 )
                    {
                        $i++;
                    }
                    else
                    {
                        $o++;
                    }
		        }
	        }
        }

        if( empty( $i )  )
        {
            $i2 = 0;

            $suggest_post_data = $post;

            if( empty( $suggest_return_port ) === false )
            {
                foreach( $suggest_return_port as $stype => $sreturn_port )
                {
                    foreach( $sreturn_port as $idx => $sport )
                    {
                        foreach( $schedule as $sc )
                        {
                            if( $stype == 'from' )
                            {
                                $suggest_post_data['return_port'] = $sport;
                            }
                            elseif( $stype == 'to' )
                            {
                                $suggest_post_data['destination_port_rtn'] = $sport;
                            }

                            $not_closed_date   = is_date_not_in_close_allotment_list( $agid, $sc['sid'], $rtn_date );
                            $available_depart  = is_available_depart_on_list( $sc['rid'], $suggest_post_data['return_port'], $rtn_date );
                            $available_arrival = is_available_arrival_on_list( $sc['rid'], $suggest_post_data['destination_port_rtn'] );
                            $available_date    = is_available_date_on_list( $sc['sid'], $rtn_date, $frmadmin );

                            if( $from_agent_panel )
                            {
                                $not_pass_time = is_not_pass_time_limit( $sc['rid'], $suggest_post_data, 'return' );
                            }
                            else
                            {
                                $not_pass_time = true;
                            }

                            if( $available_depart && $available_arrival && $available_date && $not_pass_time )
                            {
                                $sremain_seat = remain_seat( $rtn_date, $sc['sid'], $sc['rid'], $pass_num, $sc['bopassenger'], $suggest_post_data['return_port'], $suggest_post_data['destination_port_rtn'], $agid, $from_agent_panel );
                                //-- $snum_of_seat = $sremain_seat - $pass_num;
                                $num_of_seat = $remain_seat;
                                if( $sremain_seat > 0 )
                                {
                                    $p = get_result_price( $sc['sid'], $sc['rid'], $rtn_date, $suggest_post_data['return_port'], $suggest_post_data['destination_port_rtn'], $adult_num, $child_num, $infant_num, $agid, null, $chid);

                                    $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                                    $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                                    if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                                    {
                                        if( $from_agent_panel === true && is_block_request_transport( $sc['rid'], $suggest_post_data, 'return' ) )
                                        {
                                            add_variable( 'include_trans_css', 'sr-only' );
                                        }
                                        else
                                        {
                                            add_variable( 'include_trans_css', '' );
                                        }

                                        add_variable( 'sid', $sc['sid'] );
                                        add_variable( 'rid', $sc['rid'] );
                                        add_variable( 'boid', $sc['boid'] );
                                        add_variable( 'boname', $sc['boname'] );
                                        add_variable( 'radio_name', 'rtn_rid' );

                                        add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                        add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );
                                        add_variable( 'select_trip_btn_cls', $from_agent_panel ? '' : 'hidden' );

                                        add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                        add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                        add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                        add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                        add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                        add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                        add_variable( 'port_name', get_route_first_port( $sc['rid'], $suggest_post_data['return_port'] ) );

                                        if( $snum_of_seat < 7 )
                                        {
                                            if( $snum_of_seat <= 0 )
                                            {
                                                aadd_variable( 'remain_seat', 'No seat left' );
                                            }
                                            elseif( $snum_of_seat == 1 )
                                            {
                                                add_variable( 'remain_seat', '1 seat left' );
                                            }
                                            else
                                            {
                                                add_variable( 'remain_seat', $snum_of_seat . ' seats left' );
                                            }

                                            add_variable( 'remain_seat_css', '' );
                                        }
                                        else
                                        {
                                            add_variable( 'remain_seat', '' );
                                            add_variable( 'remain_seat_css', 'sr-only' );
                                        }

                                        if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                                        {
                                            if( !empty( $p['early_discount'] ) )
                                            {
                                                if( $p['early_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                            }

                                            if( !empty( $p['seat_discount'] ) )
                                            {
                                                if( $p['seat_discount']['type'] == '0' )
                                                {
                                                    $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                    $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                    $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                    $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                    $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                    $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                    $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                                    $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                                }
                                                else
                                                {
                                                    $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                                    $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                                    $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                    $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                                }

                                                add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                            }

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $inc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }
                                        elseif( isset( $p['agent_discount'] ) && !empty( $p['agent_discount'] ) )
                                        {
                                            if( $p['agent_discount']['type'] == '0' )
                                            {
                                                $inc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                                $inc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                                $inc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                                $exc_disc_adult  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                                $exc_disc_child  = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                                $exc_disc_infant = ( $p['agent_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                                $inc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $inc_total_price;
                                                $exc_disc_price  = ( $p['agent_discount']['disc'] / 100 ) * $exc_total_price;
                                            }
                                            else
                                            {
                                                $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                                                $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['agent_discount']['disc'] : 0;
                                                $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['agent_discount']['disc'] : 0;

                                                $inc_disc_price  = ( $inc_disc_adult * $adult_num ) + ( $inc_disc_child * $child_num ) + ( $inc_disc_infant * $infant_num );
                                                $exc_disc_price  = ( $exc_disc_adult * $adult_num ) + ( $exc_disc_child * $child_num ) + ( $exc_disc_infant * $infant_num );
                                            }

                                            add_variable( 'discount_notif', empty( $p['agent_discount']['notif'] ) ? '' : $p['agent_discount']['notif'] );                                

                                            add_variable( 'inc_disc_price', $inc_disc_price );
                                            add_variable( 'exc_disc_price', $exc_disc_price );

                                            add_variable( 'inc_disc_adult', $inc_disc_adult );
                                            add_variable( 'inc_disc_child', $inc_disc_child );
                                            add_variable( 'inc_disc_infant', $inc_disc_infant );

                                            add_variable( 'exc_disc_adult', $exc_disc_adult );
                                            add_variable( 'exc_disc_child', $exc_disc_child );
                                            add_variable( 'exc_disc_infant', $exc_disc_infant );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                            add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                                            add_variable( 'include_price_disc_number', '<span class="disc-price">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                            add_variable( 'exclude_price_disc_number', '<span class="disc-price">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                                        }
                                        else
                                        {
                                            add_variable( 'discount_notif', '' );

                                            add_variable( 'inc_disc_price', 0 );
                                            add_variable( 'exc_disc_price', 0 );

                                            add_variable( 'inc_disc_adult', 0 );
                                            add_variable( 'inc_disc_child', 0 );
                                            add_variable( 'inc_disc_infant', 0 );

                                            add_variable( 'exc_disc_adult', 0 );
                                            add_variable( 'exc_disc_child', 0 );
                                            add_variable( 'exc_disc_infant', 0 );

                                            add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                            add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                            add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                            add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                            add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                            add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                            add_variable( 'inc_net_adult_price', $p['inc_trans']['adult']['net_price'] );
                                            add_variable( 'inc_net_child_price', $p['inc_trans']['child']['net_price'] );
                                            add_variable( 'inc_net_infant_price', $p['inc_trans']['infant']['net_price'] );

                                            add_variable( 'exc_net_adult_price', $p['exc_trans']['adult']['net_price'] );
                                            add_variable( 'exc_net_child_price', $p['exc_trans']['child']['net_price'] );
                                            add_variable( 'exc_net_infant_price', $p['exc_trans']['infant']['net_price'] );

                                            add_variable( 'include_price_number', $inc_total_price );
                                            add_variable( 'exclude_price_number', $exc_total_price );

                                            add_variable( 'include_price_disc_number', '' );
                                            add_variable( 'exclude_price_disc_number', '' );
                                        }

                                        add_variable( 'rtdid', $p['rtdid'] );

                                        add_variable( 'inc_total_price', $inc_total_price );
                                        add_variable( 'exc_total_price', $exc_total_price );

                                        add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                        add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                                        add_variable( 'passenger_number', get_passenger_num_content( $adult_num, $child_num, $infant_num ) );
                                        add_variable( 'route_list', get_route_detail_list_content( $sc['rid'], $suggest_post_data['return_port'], $suggest_post_data['destination_port_rtn'] ) );

                                        parse_template( 'search-suggestion-result-loop-block', 'rssrlblock', true );

                                        $i2++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if( $i2 > 0 )
            {
                add_variable( 'sugested_routes', get_location( $suggest_post_data['return_port'], 'lcname' ) . ' to ' . get_location( $suggest_post_data['destination_port_rtn'], 'lcname' ) );

                parse_template( 'search-suggestion-result-block', 'rssrblock' );
            }

            if( empty( $o ) )
            {
                parse_template( 'search-empty-loop-block', 'rselblock' );
            }
        }
    }
    else
    {
        parse_template( 'search-empty-loop-block', 'rselblock' );
    }

    return return_template( 'availability-return-loop' );
}

function get_route_detail_transport( $trans = 0 )
{
    if( $trans == 1 )
    {
    	$content = '
    	<p><b>Shared</b> (Free)</p>
		<p><b>Private</b> (Additional)</p>';
    }
    else
    {
    	$content = '<p><b>Excluded </b></p>';
    }

    return $content;
}

function is_available_date_on_list( $sid, $depart_date, $from_admin = false )
{
    global $db;

    $date = date( 'Y-m-d', strtotime( $depart_date ) );

    $s = 'SELECT
            a.esid,
            a.eapplication
          FROM ticket_exception AS a
          INNER JOIN ticket_exception_detail AS b ON b.eid = a.eid
          WHERE b.edatefrom <= %s AND b.edateto >= %s';
    $q = $db->prepare_query( $s, $date, $date );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $uri   = cek_url();
        $exist = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $apps = $from_admin ? 2 : ( $uri[0] == 'agent' ? 1 : 0 );

            if( empty( $d['eapplication'] ) && in_array( $apps, array( 0, 1, 2 ) ) )
            {
                $esid = json_decode( $d['esid'], true );

                if( is_array( $esid ) && json_last_error() === JSON_ERROR_NONE && in_array( $sid, $esid ) )
                {
                    $exist++;
                }
            }
            else
            {
                $eapp = json_decode( $d['eapplication'], true );

                if( is_array( $eapp ) && json_last_error() === JSON_ERROR_NONE && in_array( $apps, $eapp ) )
                {
                    $esid = json_decode( $d['esid'], true );

                    if( is_array( $esid ) && json_last_error() === JSON_ERROR_NONE && in_array( $sid, $esid ) )
                    {
                        $exist++;
                    }
                }
            }
        }

        if( $exist > 0 )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return true;
    }
}

function is_date_not_in_close_allotment_list( $agid = null, $sid, $depart_date )
{
    global $db;

    if( is_null( $agid ) )
    {
        return true;
    }

    $s = 'SELECT d.aclose
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          LEFT JOIN ticket_allotment_detail AS d ON d.dagid = b.dagid
          WHERE a.sid = %d AND b.agid = %d AND d.adate = %s';
    $q = $db->prepare_query( $s, $sid, $agid, date( 'Y-m-d', strtotime( $depart_date ) ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        $d = $db->fetch_array( $r );

        if( $d['aclose'] == '1' )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return true;
    }
}

function is_date_not_in_close_online_allotment_list( $sid, $depart_date )
{
    global $db;

    $s = 'SELECT d.aclose
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_online AS b ON a.aid = b.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          LEFT JOIN ticket_allotment_detail_online AS d ON d.daoid = b.daoid
          WHERE a.sid = %d AND d.adate = %s';
    $q = $db->prepare_query( $s, $sid, date( 'Y-m-d', strtotime( $depart_date ) ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        $d = $db->fetch_array( $r );

        if( $d['aclose'] == '1' )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return true;
    }
}

function is_available_depart_on_list( $rid, $depart, $depart_date = '', $revise_trip = false )
{
	global $db;

	$s = 'SELECT
            a.lcid,
            a.rdtime
          FROM ticket_route_detail AS a
          WHERE a.rid = %d AND a.rdtype IN ( %s, %s )';
    $q = $db->prepare_query( $s, $rid, '0', '2' );
    $r = $db->do_query( $q );

    $list = array();

    while( $d = $db->fetch_array( $r ) )
    {
        if( empty( $depart_date ) )
        {
            $list[] = $d['lcid'];
        }
        else
        {
            if( $revise_trip )
            {
                $list[] = $d['lcid'];
            }
            else
            {
                if( time() < strtotime( $depart_date . ' ' . $d['rdtime'] ) )
                {
                    $list[] = $d['lcid'];
                }
            }
        }
    }
    
    if( in_array( $depart, $list ) )
    {
    	return true;
    }
    else
    {
    	return false;
    }
}

function is_available_arrival_on_list( $rid, $arrive )
{
	global $db;

	$s = 'SELECT
            a.lcid
          FROM ticket_route_detail AS a
          WHERE a.rid = %d AND a.rdtype IN ( %s, %s )';
    $q = $db->prepare_query( $s, $rid, '2', '1' );
    $r = $db->do_query( $q );

    $list = array();
    
    while( $d = $db->fetch_array( $r ) )
    {
       $list[] = $d['lcid'];
    }

    if( in_array( $arrive, $list ) )
    {
    	return true;
    }
    else
    {
    	return false;
    }
}

function remain_seat( $date, $sid, $rid, $passenger, $capacity, $depart_port, $destination_port, $agid = '', $from_agent_panel = false, $bdid = null, $online = false )
{
    global $db;

    $s = 'SELECT * FROM ticket_schedule AS a LEFT JOIN ticket_route AS b ON a.rid = b.rid WHERE a.sid = %d AND a.rid = %d';
    $q = $db->prepare_query( $s, $sid, $rid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( $d['rtype'] == '1' && !empty( $d['rhoppingpoint'] ) )
    {
        $time = strtotime( $date );

        $total_agent_allot_arr  = get_total_all_agent_daily_allotment( $sid, $time );

        if( $from_agent_panel === true )
        {
            if( is_agent_has_allotment( $agid ) )
            {
                if( !isset( $total_agent_allot_arr[ $time ][ $rid ][ $agid ] ) )
                {
                    return 0;
                }
            }
        }

        if( empty( $agid ) )
        {
            if( $online )
            {
                $total_online_allot_arr = get_online_total_daily_allotment( $sid, $time );
                $total_online_allot     = get_seat_availibility_online_num_by_schedule( $capacity, $rid, $sid, $time, $from_agent_panel );

                if( isset( $total_online_allot_arr[ $time ][ $sid ] ) )
                {
                    $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
                    $rcapacity          = $capacity - $total_agent_allot;
                }
                else
                {
                    //-- Get Global Seat If No Set Allotment
                    $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
                    $rcapacity          = $capacity - $total_agent_allot - $total_online_allot;
                }
            }
            else
            {
                //-- Get Global Seat
                $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
                $total_online_allot = get_seat_availibility_online_num_by_schedule( $capacity, $rid, $sid, $time, $from_agent_panel );
                $rcapacity          = $capacity - $total_agent_allot - $total_online_allot;
            }

            $spoint = get_route_point_list( $rid, 0 );
            $epoint = get_route_point_list( $rid, 1 );
            $hpoint = get_route_point_list( $rid, 2 );

            //-- A = spoint
            //-- B = hpoint
            //-- C = epoint
            //-- X = capacity
            if( in_array( $depart_port, $spoint ) && in_array( $destination_port, $epoint ) )
            {
                //-- Get max capacity from start point to end point
                //-- If BC > AB --> ACmax = X - AB - AC
                //-- If BC > AB --> ACmax = X - BC - AC
                $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true, $bdid );
                $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true, $bdid );
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for AC
                if( $ab_seat > $bc_seat )
                {
                    $max_seat = $rcapacity - $ac_seat - $ab_seat;
                }
                elseif( $ab_seat < $bc_seat )
                {
                    $max_seat = $rcapacity - $ac_seat - $bc_seat;
                }
                else
                {
                    $max_seat = $rcapacity - $ab_seat - $ac_seat - $bc_seat;
                }

                if( $max_seat <= 0 )
                {
                    return 0;
                }
                else
                {
                    //-- Remain seat
                    if( $online && isset( $total_online_allot_arr[ $time ][ $sid ] ) )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat;
                }
            }
            elseif( in_array( $depart_port, $spoint ) && in_array( $destination_port, $hpoint ) )
            {
                //-- Get max capacity from start point to hopping point
                //-- ABmax = X - AC - AB
                $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true, $bdid );
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for AB
                $max_seat = $rcapacity - $ac_seat - $ab_seat;

                if( $max_seat <= 0 )
                {
                    return 0;
                }
                else
                {
                    //-- Remain seat
                    if( $online && isset( $total_online_allot_arr[ $time ][ $sid ] ) )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat;
                }
            }
            elseif( in_array( $depart_port, $hpoint ) && in_array( $destination_port, $epoint ) )
            {
                //-- Get max capacity from hopping point to end point
                //-- BCmax = X - AC - BC
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );
                $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for BC
                $max_seat = $rcapacity - $ac_seat - $bc_seat;

                if( $max_seat <= 0 )
                {
                    return 0;
                }
                else
                {
                    //-- Remain seat
                    if( $online && isset( $total_online_allot_arr[ $time ][ $sid ] ) )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat;
                }
            }
            else
            {
                $remain = $rcapacity;
            }

            if( $remain < 0 )
            {
                return 0;
            }
            else
            {
                return $remain;
            }
        }
        else
        {
            $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
            $total_online_allot = get_seat_availibility_online_num_by_schedule( $capacity, $rid, $sid, $time, $from_agent_panel );
            $rcapacity          = $capacity - $total_agent_allot - $total_online_allot;

            $spoint = get_route_point_list( $rid, 0 );
            $epoint = get_route_point_list( $rid, 1 );
            $hpoint = get_route_point_list( $rid, 2 );

            //-- A = spoint
            //-- B = hpoint
            //-- C = epoint
            //-- X = capacity
            if( in_array( $depart_port, $spoint ) && in_array( $destination_port, $epoint ) )
            {
                //-- Get max capacity from start point to end point
                //-- If BC > AB --> ACmax = X - AB - AC
                //-- If BC > AB --> ACmax = X - BC - AC
                $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true, $bdid );
                $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true, $bdid );
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for AC
                if( $ab_seat > $bc_seat )
                {
                    $max_seat = $rcapacity - $ac_seat - $ab_seat;
                }
                elseif( $ab_seat < $bc_seat )
                {
                    $max_seat = $rcapacity - $ac_seat - $bc_seat;
                }
                else
                {
                    $max_seat = $rcapacity - $ab_seat - $ac_seat - $bc_seat;
                }

                if( $max_seat <= 0 )
                {
                    return 0;
                }
                else
                {
                    //-- Remain seat
                    if( $online )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat;
                }
            }
            elseif( in_array( $depart_port, $spoint ) && in_array( $destination_port, $hpoint ) )
            {
                //-- Get max capacity from start point to hopping point
                //-- ABmax = X - AC - AB
                $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true, $bdid );
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for AB
                $max_seat = $rcapacity - $ac_seat - $ab_seat;

                if( $max_seat <= 0 )
                {
                    return 0;
                }
                else
                {
                    //-- Remain seat
                    if( $online )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat;
                }
            }
            elseif( in_array( $depart_port, $hpoint ) && in_array( $destination_port, $epoint ) )
            {
                //-- Get max capacity from hopping point to end point
                //-- BCmax = X - AC - BC
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );
                $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for BC
                $max_seat = $rcapacity - $ac_seat - $bc_seat;

                if( $max_seat <= 0 )
                {
                    return 0;
                }
                else
                {
                    //-- Remain seat
                    if( $online )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat;
                }
            }
            else
            {
                $remain = $rcapacity - $passenger;
            }

            if( $remain < 0 )
            {
                return 0;
            }
            else
            {
                return $remain;
            }
        }
    }
    else
    {
        $remain = get_seat_availibility_num_by_schedule( $capacity, $rid, $sid, strtotime( $date ), $agid, $from_agent_panel, $online, $bdid );

        if( $remain < 0 )
        {
            return 0;
        }
        else
        {
            return $remain;
        }
    }
}

function is_available_allotment( $date, $sid, $rid, $passenger, $capacity, $depart_port, $destination_port, $agid = '', $from_agent_panel = false, $bdid = null, $online = false )
{
    global $db;

    $s = 'SELECT * FROM ticket_schedule AS a LEFT JOIN ticket_route AS b ON a.rid = b.rid WHERE a.sid = %d AND a.rid = %d';
    $q = $db->prepare_query( $s, $sid, $rid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( $d['rtype'] == '1' && !empty( $d['rhoppingpoint'] ) )
    {
        $time = strtotime( $date );

        $total_agent_allot_arr  = get_total_all_agent_daily_allotment( $sid, $time );

        if( $from_agent_panel === true )
        {
            if( is_agent_has_allotment( $agid ) )
            {
                if( !isset( $total_agent_allot_arr[ $time ][ $rid ][ $agid ] ) )
                {
                    return false;
                }
            }
        }

        if( empty( $agid ) )
        {
            if( $online )
            {
                $total_online_allot_arr = get_online_total_daily_allotment( $sid, $time );
                $total_online_allot     = get_seat_availibility_online_num_by_schedule( $capacity, $rid, $sid, $time, $from_agent_panel );

                if( isset( $total_online_allot_arr[ $time ][ $sid ] ) )
                {
                    $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
                    $rcapacity          = $capacity - $total_agent_allot;
                }
                else
                {
                    //-- Get Global Seat If No Set Allotment
                    $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
                    $rcapacity          = $capacity - $total_agent_allot - $total_online_allot;
                }
            }
            else
            {
                //-- Get Global Seat
                $total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
                $total_online_allot = get_seat_availibility_online_num_by_schedule( $capacity, $rid, $sid, $time, $from_agent_panel );
                $rcapacity          = $capacity - $total_agent_allot - $total_online_allot;
            }

            $spoint = get_route_point_list( $rid, 0 );
            $epoint = get_route_point_list( $rid, 1 );
            $hpoint = get_route_point_list( $rid, 2 );

            //-- A = spoint
            //-- B = hpoint
            //-- C = epoint
            //-- X = capacity
            if( in_array( $depart_port, $spoint ) && in_array( $destination_port, $epoint ) )
            {
                //-- Get max capacity from start point to end point
                //-- If BC > AB --> ACmax = X - AB - AC
                //-- If BC > AB --> ACmax = X - BC - AC
                $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true, $bdid );
                $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true, $bdid );
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for AC
                if( $ab_seat > $bc_seat )
                {
                    $max_seat = $rcapacity - $ac_seat - $ab_seat;
                }
                elseif( $ab_seat < $bc_seat )
                {
                    $max_seat = $rcapacity - $ac_seat - $bc_seat;
                }
                else
                {
                    $max_seat = $rcapacity - $ab_seat - $ac_seat - $bc_seat;
                }

                if( $max_seat <= 0 )
                {
                    return false;
                }
                else
                {
                    //-- Remain seat
                    if( $online && isset( $total_online_allot_arr[ $time ][ $sid ] ) )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat - $passenger;
                }
            }
            elseif( in_array( $depart_port, $spoint ) && in_array( $destination_port, $hpoint ) )
            {
                //-- Get max capacity from start point to hopping point
                //-- ABmax = X - AC - AB
                $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true, $bdid );
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for AB
                $max_seat = $rcapacity - $ac_seat - $ab_seat;

                if( $max_seat <= 0 )
                {
                    return false;
                }
                else
                {
                    //-- Remain seat
                    if( $online && isset( $total_online_allot_arr[ $time ][ $sid ] ) )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat - $passenger;
                }
            }
            elseif( in_array( $depart_port, $hpoint ) && in_array( $destination_port, $epoint ) )
            {
                //-- Get max capacity from hopping point to end point
                //-- BCmax = X - AC - BC
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );
                $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for BC
                $max_seat = $rcapacity - $ac_seat - $bc_seat;

                if( $max_seat <= 0 )
                {
                    return false;
                }
                else
                {
                    //-- Remain seat
                    if( $online && isset( $total_online_allot_arr[ $time ][ $sid ] ) )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat - $passenger;
                }
            }
            else
            {
                $remain = $rcapacity - $passenger;
            }

            if( $remain < 0 )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
        	$total_agent_allot  = get_total_agent_allot_num( $total_agent_allot_arr, $rid, $time );
            $total_online_allot = get_seat_availibility_online_num_by_schedule( $capacity, $rid, $sid, $time, $from_agent_panel );
            $rcapacity          = $capacity - $total_agent_allot - $total_online_allot;

            $spoint = get_route_point_list( $rid, 0 );
            $epoint = get_route_point_list( $rid, 1 );
            $hpoint = get_route_point_list( $rid, 2 );

            //-- A = spoint
            //-- B = hpoint
            //-- C = epoint
            //-- X = capacity
            if( in_array( $depart_port, $spoint ) && in_array( $destination_port, $epoint ) )
            {
                //-- Get max capacity from start point to end point
                //-- If BC > AB --> ACmax = X - AB - AC
                //-- If BC > AB --> ACmax = X - BC - AC
                $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true, $bdid );
                $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true, $bdid );
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for AC
                if( $ab_seat > $bc_seat )
                {
                    $max_seat = $rcapacity - $ac_seat - $ab_seat;
                }
                elseif( $ab_seat < $bc_seat )
                {
                    $max_seat = $rcapacity - $ac_seat - $bc_seat;
                }
                else
                {
                    $max_seat = $rcapacity - $ab_seat - $ac_seat - $bc_seat;
                }

                if( $max_seat <= 0 )
                {
                    return false;
                }
                else
                {
                    //-- Remain seat
                    if( $online )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat - $passenger;
                }
            }
            elseif( in_array( $depart_port, $spoint ) && in_array( $destination_port, $hpoint ) )
            {
                //-- Get max capacity from start point to hopping point
                //-- ABmax = X - AC - AB
                $ab_seat = get_taken_seat_by_schedule( $sid, $spoint, $hpoint, $date, true, $bdid );
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for AB
                $max_seat = $rcapacity - $ac_seat - $ab_seat;

                if( $max_seat <= 0 )
                {
                    return false;
                }
                else
                {
                    //-- Remain seat
                    if( $online )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat - $passenger;
                }
            }
            elseif( in_array( $depart_port, $hpoint ) && in_array( $destination_port, $epoint ) )
            {
                //-- Get max capacity from hopping point to end point
                //-- BCmax = X - AC - BC
                $ac_seat = get_taken_seat_by_schedule( $sid, $spoint, $epoint, $date, true, $bdid );
                $bc_seat = get_taken_seat_by_schedule( $sid, $hpoint, $epoint, $date, true, $bdid );

                //-- Maximum seat for BC
                $max_seat = $rcapacity - $ac_seat - $bc_seat;

                if( $max_seat <= 0 )
                {
                    return false;
                }
                else
                {
                    //-- Remain seat
                    if( $online )
                    {
                        if( ( $max_seat - $total_online_allot ) >= 0 )
                        {
                            $max_seat = $total_online_allot;
                        }
                    }

                    $remain = $max_seat - $passenger;
                }
            }
            else
            {
                $remain = $rcapacity - $passenger;
            }

            if( $remain < 0 )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    else
    {
        $allot  = get_seat_availibility_num_by_schedule( $capacity, $rid, $sid, strtotime( $date ), $agid, $from_agent_panel, $online, $bdid );
        $remain = $allot - $passenger;

        if( $remain < 0 )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

function get_agent_seat_allotment( $agid, $sid, $date )
{
    $allot = get_allotment_detail_by_agent( $agid, $sid );

    if( empty( $allot ) )
    {
        return 0;
    }
    else
    {
        $day_allotment = get_agent_allotment_detail_by_date( $allot[0]['dagid'], strtotime( $date ) );
        $fix_allotment = $day_allotment['result'] == 1 ? $day_allotment['allotment'] : $allot[0]['allotment'];

        return $fix_allotment;
    }
}

function get_taken_seat_by_schedule( $sid, $from, $to, $date, $is_hopping = false, $bdid = null )
{
    global $db;

    if( $is_hopping )
    {
        $num = 0;

        foreach( $from as $fid )
        {
            foreach( $to as $tid )
            {
                if( is_null( $bdid ) )
                {
                    $s = 'SELECT
                            b.num_adult,
                            b.num_child,
                            b.num_infant,
                            a.bblockingtime,
                            a.bbrevstatus,
                            b.bdrevstatus
                          FROM ticket_booking AS a
                          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                          WHERE b.bdpstatus NOT IN ( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar" AND b.sid = %d AND b.bdfrom_id = %d AND b.bdto_id = %d AND b.bddate = %s';
                    $q = $db->prepare_query( $s, $sid, $fid, $tid, $date );
                }
                else
                {
                    $s = 'SELECT
                            b.num_adult,
                            b.num_child,
                            b.num_infant,
                            a.bblockingtime,
                            a.bbrevstatus,
                            b.bdrevstatus
                          FROM ticket_booking AS a
                          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                          WHERE b.bdpstatus NOT IN ( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar" AND b.bdid <> %d AND b.sid = %d AND b.bdfrom_id = %d AND b.bdto_id = %d AND b.bddate = %s';
                    $q = $db->prepare_query( $s, $bdid, $sid, $fid, $tid, $date );
                }

                $r = $db->do_query( $q );

                if( $db->num_rows( $r ) > 0 )
                {
                    while( $d = $db->fetch_array( $r ) )
                    {
                        $num = $num + ( $d['num_adult'] + $d['num_child'] + $d['num_infant'] );
                    }
                }
            }
        }

        return $num;
    }
    else
    {
        $total = 0;

        if( is_null( $bdid ) )
        {
            $s = 'SELECT
                    b.num_adult,
                    b.num_child,
                    b.num_infant,
                    a.bblockingtime,
                    a.bbrevstatus,
                    b.bdrevstatus
                  FROM ticket_booking AS a
                  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                  WHERE b.bdpstatus NOT IN ( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar" AND b.sid = %d AND b.bddate = %s';
            $q = $db->prepare_query( $s, $sid, $date );
        }
        else
        {
            $s = 'SELECT
                    b.num_adult,
                    b.num_child,
                    b.num_infant,
                    a.bblockingtime,
                    a.bbrevstatus,
                    b.bdrevstatus
                  FROM ticket_booking AS a
                  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                  WHERE b.bdpstatus NOT IN ( "cn", "bc", "pf", "ol" ) AND a.bstt <> "ar" AND b.bdid <> %d AND b.sid = %d AND b.bddate = %s';
            $q = $db->prepare_query( $s, $bdid, $sid, $date );
        }

        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                $total = $total + ( $d['num_adult'] + $d['num_child'] + $d['num_infant'] );
            }
        }

        return $total;
    }
}

function is_available_allotment_agent( $date, $sid, $passenger, $agid = '' )
{
    global $db;

    //-- Check Remain Allotment Agent
    $s = 'SELECT
			b.avallot,
			IFNULL((
                SELECT a2.allotment
                FROM ticket_allotment_detail AS a2
                WHERE a2.dagid = b.dagid AND a2.adate = %s
            ),0) AS day_allotment
           FROM ticket_allotment AS a
           LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
           LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
           WHERE a.sid = %d AND b.agid = %s';
    $q = $db->prepare_query( $s, $date, $sid, $agid );
    $r = $db->do_query( $q );

    $rallotremain = 0;

    if( $db->num_rows( $r ) )
    {
        $d = $db->fetch_array( $r );

        $rallotment   = get_remaining_agent_allotment_by_schedule( $sid, $agid, strtotime( $date ), $d['avallot'] );
        $rallotment   = $d['day_allotment'] == 0 ? $rallotment : $d['day_allotment'];
        $rallotremain = $rallotment - $passenger;
    }

    if( $rallotremain < 0 )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function get_result_price( $sid, $rid, $date, $from, $to, $adult = 0, $child = 0, $infant = 0, $agid = '', $bdetail = '' , $chid = '')
{
    global $db;

    //-- Don't change rate if booking already paid
    $old_price = array();

    if( !empty( $bdetail ) )
    {
        if( isset( $bdetail['sid'] ) && $bdetail['sid'] == $sid )
        {
            extract( $bdetail );

            $old_price = array(
                'inc_trans' => array(
                    'adult'  => array( 'price' => $price_per_adult, 'selling_price' => $selling_price_per_adult, 'net_price' => $net_price_per_adult, 'total' => ( $adult * $price_per_adult ) ),
                    'child'  => array( 'price' => $price_per_child, 'selling_price' => $selling_price_per_child, 'net_price' => $net_price_per_child, 'total' => ( $child * $price_per_child ) ),
                    'infant' => array( 'price' => $price_per_infant, 'selling_price' => $selling_price_per_infant, 'net_price' => $net_price_per_infant, 'total' => ( $infant * $price_per_infant ) )
                ),
                'exc_trans' => array(
                    'adult'  => array( 'price' => $price_per_adult, 'selling_price' => $selling_price_per_adult, 'net_price' => $net_price_per_adult, 'total' => ( $adult * $price_per_adult ) ),
                    'child'  => array( 'price' => $price_per_child, 'selling_price' => $selling_price_per_child, 'net_price' => $net_price_per_child, 'total' => ( $child * $price_per_child ) ),
                    'infant' => array( 'price' => $price_per_infant, 'selling_price' => $selling_price_per_infant, 'net_price' => $net_price_per_infant, 'total' => ( $infant * $price_per_infant ) )
                ),
                'fix_discount' => array(
                    'disc_per_adult'  => $disc_price_per_adult,
                    'disc_per_child'  => $disc_price_per_child,
                    'disc_per_infant' => $disc_price_per_infant
                ),
                'rtdid' => ''
            );
        }
    }

    $s = 'SELECT
            c.rtid,
            c.rtdid,
            b.rttype,
            c.rtiadult,
            c.rtichild,
            c.rtiinfant,
            c.rteadult,
            c.rtechild,
            c.rteinfant
          FROM ticket_rate_period AS a
          LEFT JOIN ticket_rate AS b ON b.rpid = a.rpid
          LEFT JOIN ticket_rate_detail AS c ON c.rtid = b.rtid
          WHERE a.rpfrom <= %s AND a.rpto >= %s AND c.rid = %d';
    $q = $db->prepare_query( $s, $date, $date, $rid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $crate = get_channel( $chid, 'chpercentage' );
        $edisc = get_rate_early_discount( $d['rtid'], $agid );
        $sdisc = get_rate_seat_discount( $d['rtid'], $agid, $sid, $rid, $date );

        if( $d['rttype'] == '1' )
        {
            $s2 = 'SELECT * FROM ticket_rate_route_detail WHERE rtdid = %d AND lcid = %d AND lcid_to = %d';
            $q2 = $db->prepare_query( $s2, $d['rtdid'], $from, $to );
            $r2 = $db->do_query( $q2 );
            $dt = $db->fetch_array( $r2 );
        }
        else
        {
            $dt = $d;
        }

        if( empty( $agid ) )
        {
            if( $crate > 0 )
            {
                //-- SP = Selling Price
                //-- NP = Net Price
                //-- PC = Price base on channel percentage

                //-- Calculate the PC
                $ch_rtiadult_rate  = $dt['rtiadult'] - ( $dt['rtiadult'] * $crate ) / 100;
                $ch_rtichild_rate  = $dt['rtichild'] - ( $dt['rtichild'] * $crate ) / 100;
                $ch_rtiinfant_rate = $dt['rtiinfant'] - ( $dt['rtiinfant'] * $crate ) / 100;

                $ch_rteadult_rate  = $dt['rteadult'] - ( $dt['rteadult'] * $crate ) / 100;
                $ch_rtechild_rate  = $dt['rtechild'] - ( $dt['rtechild'] * $crate ) / 100;
                $ch_rteinfant_rate = $dt['rteinfant'] - ( $dt['rteinfant'] * $crate ) / 100;

                //-- Calculate Disc
                if( !empty( $sdisc ) )
                {
                    if( $sdisc['type'] == '0' )
                    {
                        $sdisc_rtiadult  = ( $sdisc['disc'] * $dt['rtiadult'] ) / 100;
                        $sdisc_rtichild  = ( $sdisc['disc'] * $dt['rtichild'] ) / 100;
                        $sdisc_rtiinfant = ( $sdisc['disc'] * $dt['rtiinfant'] ) / 100;

                        $sdisc_rteadult  = ( $sdisc['disc'] * $dt['rteadult'] ) / 100;
                        $sdisc_rtechild  = ( $sdisc['disc'] * $dt['rtechild'] ) / 100;
                        $sdisc_rteinfant = ( $sdisc['disc'] * $dt['rteinfant'] ) / 100;
                    }
                    else
                    {
                        $sdisc_rtiadult  = $sdisc['disc'];
                        $sdisc_rtichild  = $sdisc['disc'];
                        $sdisc_rtiinfant = $sdisc['disc'];

                        $sdisc_rteadult  = $sdisc['disc'];
                        $sdisc_rtechild  = $sdisc['disc'];
                        $sdisc_rteinfant = $sdisc['disc'];
                    }
                }
                else
                {
                    $sdisc_rtiadult  = 0;
                    $sdisc_rtichild  = 0;
                    $sdisc_rtiinfant = 0;

                    $sdisc_rteadult  = 0;
                    $sdisc_rtechild  = 0;
                    $sdisc_rteinfant = 0;
                }

                if( !empty( $edisc ) )
                {
                    if( $edisc['type'] == '0' )
                    {
                        $edisc_rtiadult  = ( $edisc['disc'] * $dt['rtiadult'] ) / 100;
                        $edisc_rtichild  = ( $edisc['disc'] * $dt['rtichild'] ) / 100;
                        $edisc_rtiinfant = ( $edisc['disc'] * $dt['rtiinfant'] ) / 100;

                        $edisc_rteadult  = ( $edisc['disc'] * $dt['rteadult'] ) / 100;
                        $edisc_rtechild  = ( $edisc['disc'] * $dt['rtechild'] ) / 100;
                        $edisc_rteinfant = ( $edisc['disc'] * $dt['rteinfant'] ) / 100;
                    }
                    else
                    {
                        $edisc_rtiadult  = $edisc['disc'];
                        $edisc_rtichild  = $edisc['disc'];
                        $edisc_rtiinfant = $edisc['disc'];

                        $edisc_rteadult  = $edisc['disc'];
                        $edisc_rtechild  = $edisc['disc'];
                        $edisc_rteinfant = $edisc['disc'];
                    }
                }
                else
                {
                    $edisc_rtiadult  = 0;
                    $edisc_rtichild  = 0;
                    $edisc_rtiinfant = 0;

                    $edisc_rteadult  = 0;
                    $edisc_rtechild  = 0;
                    $edisc_rteinfant = 0;
                }

                //-- Calculate SP
                $sp_rtiadult_rate  = $dt['rtiadult'] - ( $sdisc_rtiadult + $edisc_rtiadult );
                $sp_rtichild_rate  = $dt['rtichild'] - ( $sdisc_rtichild + $edisc_rtichild );
                $sp_rtiinfant_rate = $dt['rtiinfant'] - ( $sdisc_rtiinfant + $edisc_rtiinfant );

                $sp_rteadult_rate  = $dt['rteadult'] - ( $sdisc_rteadult + $edisc_rteadult );
                $sp_rtechild_rate  = $dt['rtechild'] - ( $sdisc_rtechild + $edisc_rtechild );
                $sp_rteinfant_rate = $dt['rteinfant'] - ( $sdisc_rteinfant + $edisc_rteinfant );

                //-- Choose NP
                //-- If SP < PC Use SP
                //-- If SP > PC Use PC
                $nt_rtiadult_rate  = $sp_rtiadult_rate < $ch_rtiadult_rate ? $sp_rtiadult_rate : $ch_rtiadult_rate;
                $nt_rtichild_rate  = $sp_rtichild_rate < $ch_rtichild_rate ? $sp_rtichild_rate : $ch_rtichild_rate;
                $nt_rtiinfant_rate = $sp_rtiinfant_rate < $ch_rtiinfant_rate ? $sp_rtiinfant_rate : $ch_rtiinfant_rate;

                $nt_rteadult_rate  = $sp_rteadult_rate < $ch_rteadult_rate ? $sp_rteadult_rate : $ch_rteadult_rate;
                $nt_rtechild_rate  = $sp_rtechild_rate < $ch_rtechild_rate ? $sp_rtechild_rate : $ch_rtechild_rate;
                $nt_rteinfant_rate = $sp_rteinfant_rate < $ch_rteinfant_rate ? $sp_rteinfant_rate : $ch_rteinfant_rate;
            }
            else
            {
                $nt_rtiadult_rate  = $dt['rtiadult'];
                $nt_rtichild_rate  = $dt['rtichild'];
                $nt_rtiinfant_rate = $dt['rtiinfant'];

                $nt_rteadult_rate  = $dt['rteadult'];
                $nt_rtechild_rate  = $dt['rtechild'];
                $nt_rteinfant_rate = $dt['rteinfant'];
            }

            $price = array(
                'inc_trans' => array(
                    'adult'  => array( 'price' => $dt['rtiadult'], 'selling_price' => $dt['rtiadult'], 'net_price' => $nt_rtiadult_rate, 'total' => ( $adult * $dt['rtiadult'] ) ),
                    'child'  => array( 'price' => $dt['rtichild'], 'selling_price' => $dt['rtichild'], 'net_price' => $nt_rtichild_rate, 'total' => ( $child * $dt['rtichild'] ) ),
                    'infant' => array( 'price' => $dt['rtiinfant'], 'selling_price' => $dt['rtiinfant'], 'net_price' => $nt_rtiinfant_rate, 'total' => ( $infant * $dt['rtiinfant'] ) )
                ),
                'exc_trans' => array(
                    'adult'  => array( 'price' => $dt['rteadult'], 'selling_price' => $dt['rteadult'], 'net_price' => $nt_rteadult_rate, 'total' => ( $adult * $dt['rteadult'] ) ),
                    'child'  => array( 'price' => $dt['rtechild'], 'selling_price' => $dt['rtechild'], 'net_price' => $nt_rtechild_rate, 'total' => ( $child * $dt['rtechild'] ) ),
                    'infant' => array( 'price' => $dt['rteinfant'], 'selling_price' => $dt['rteinfant'], 'net_price' => $nt_rteinfant_rate, 'total' => ( $infant * $dt['rteinfant'] ) )
                ),
                'early_discount' => $edisc,
                'seat_discount' => $sdisc,
                'rtdid' => $d['rtdid']
            );
        }
        else
        {                
            $adisc = get_rate_agent_discount( $agid, $date );

            $sa = 'SELECT
                    a.adult_rate_inc_trans,
                    a.child_rate_inc_trans,
                    a.infant_rate_inc_trans,
                    a.adult_rate_exc_trans,
                    a.child_rate_exc_trans,
                    a.infant_rate_exc_trans
                   FROM ticket_agent_net_rate AS a
                   WHERE a.agid = %d AND a.agvdatefrom <= %s AND a.agvdateto >= %s AND ( ( a.lcid = %d AND a.lcid_to = %d ) OR ( a.lcid_to = %d AND a.lcid = %d ) )';
            $qa = $db->prepare_query( $sa, $agid, $date, $date, $from, $to, $from, $to );
            $ra = $db->do_query( $qa );

            if( $db->num_rows( $ra ) > 0 )
            {
                $da = $db->fetch_array( $ra );
                $ag = get_agent( $agid );

                $price = array(
                    'inc_trans' => array(
                        'adult'  => array( 'price' => $da['adult_rate_inc_trans'], 'selling_price' => $da['adult_rate_inc_trans'], 'net_price' => $da['adult_rate_inc_trans'], 'total' => ( $adult * $da['adult_rate_inc_trans'] ) ),
                        'child'  => array( 'price' => $da['child_rate_inc_trans'], 'selling_price' => $da['child_rate_inc_trans'], 'net_price' => $da['child_rate_inc_trans'], 'total' => ( $child * $da['child_rate_inc_trans'] ) ),
                        'infant' => array( 'price' => $da['infant_rate_inc_trans'], 'selling_price' => $da['infant_rate_inc_trans'], 'net_price' => $da['infant_rate_inc_trans'], 'total' => ( $infant * $da['infant_rate_inc_trans'] ) )
                    ),
                    'exc_trans' => array(
                        'adult'  => array( 'price' => $da['adult_rate_exc_trans'], 'selling_price' => $da['adult_rate_exc_trans'], 'net_price' => $da['adult_rate_exc_trans'], 'total' => ( $adult * $da['adult_rate_exc_trans'] ) ),
                        'child'  => array( 'price' => $da['child_rate_exc_trans'], 'selling_price' => $da['child_rate_exc_trans'], 'net_price' => $da['child_rate_exc_trans'], 'total' => ( $child * $da['child_rate_exc_trans'] ) ),
                        'infant' => array( 'price' => $da['infant_rate_exc_trans'], 'selling_price' => $da['infant_rate_exc_trans'], 'net_price' => $da['infant_rate_exc_trans'], 'total' => ( $infant * $da['infant_rate_exc_trans'] ) )
                    ),
                    'early_discount' => $edisc,
                    'seat_discount' => $sdisc,
                    'agent_discount' => $adisc,
                    'rtdid' => $d['rtdid']
                );
            }
            else
            {
				$aprate = get_agent_percentage( $agid );
                $price  = array(
                    'inc_trans' => array(
                        'adult'  => array( 'price' => $dt['rtiadult'], 'selling_price' => $dt['rtiadult'], 'net_price' => $dt['rtiadult'], 'total' => ( $adult * $dt['rtiadult'] * $aprate / 100 ) ),
                        'child'  => array( 'price' => $dt['rtichild'], 'selling_price' => $dt['rtichild'], 'net_price' => $dt['rtichild'], 'total' => ( $child * $dt['rtichild'] * $aprate / 100 ) ),
                        'infant' => array( 'price' => $dt['rtiinfant'], 'selling_price' => $dt['rtiinfant'], 'net_price' => $dt['rtiinfant'], 'total' => ( $infant * $dt['rtiinfant'] * $aprate / 100 ) )
                    ),
                    'exc_trans' => array(
                        'adult'  => array( 'price' => $dt['rteadult'], 'selling_price' => $dt['rteadult'], 'net_price' => $dt['rteadult'], 'total' => ( $adult * $dt['rteadult'] * $aprate / 100 ) ),
                        'child'  => array( 'price' => $dt['rtechild'], 'selling_price' => $dt['rtechild'], 'net_price' => $dt['rtechild'], 'total' => ( $child * $dt['rtechild'] * $aprate / 100 ) ),
                        'infant' => array( 'price' => $dt['rteinfant'], 'selling_price' => $dt['rteinfant'], 'net_price' => $dt['rteinfant'], 'total' => ( $infant * $dt['rteinfant'] * $aprate / 100 ) )
                    ),
                    'early_discount' => $edisc,
                    'seat_discount' => $sdisc,
                    'agent_discount' => $adisc,
                    'rtdid' => $d['rtdid']
                );
            }
        }

        if( !empty( $old_price ) && $bdetail['bdpstatus'] == 'pa' )
        {
            $price = $old_price;
        }
    }
    else
    {
        $price = array(
            'inc_trans' => array(
                'adult'  => array( 'price' => 0, 'selling_price' => 0, 'net_price' => 0, 'total' => 0 ),
                'child'  => array( 'price' => 0, 'selling_price' => 0, 'net_price' => 0, 'total' => 0 ),
                'infant' => array( 'price' => 0, 'selling_price' => 0, 'net_price' => 0, 'total' => 0 )
            ),
            'exc_trans' => array(
                'adult'  => array( 'price' => 0, 'selling_price' => 0, 'net_price' => 0, 'total' => 0 ),
                'child'  => array( 'price' => 0, 'selling_price' => 0, 'net_price' => 0, 'total' => 0 ),
                'infant' => array( 'price' => 0, 'selling_price' => 0, 'net_price' => 0, 'total' => 0 )
            ),
            'early_discount' => array(),
            'seat_discount' => array(),
            'rtdid' => ''
        );
    }

    return $price;
}

function get_route_detail_list_content( $rid, $depart, $arrive, $api = false )
{
	global $db;

    //-- Get Route Detail
    $s = 'SELECT
            a.lcid,
            b.lcname,
            a.rdtype,
            a.rdtime,
            a.rdtime2
          FROM ticket_route_detail AS a
		  INNER JOIN ticket_location AS b ON a.lcid = b.lcid
		  WHERE rid = %d ORDER BY rdid ASC';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );

    $hopid  = '';
    $dep    = false;
    $ret    = false;

    //-- Get Route
    $s2 = 'SELECT
            a.rtype,
            a.rhoppingpoint
          FROM ticket_route AS a
          WHERE a.rid = %d';
    $q2 = $db->prepare_query( $s2, $rid );
    $r2 = $db->do_query( $q2 );
    $d2 = $db->fetch_array( $r2 );

    if( $d2['rtype'] == '1' && !empty( $d2['rhoppingpoint'] ) )
    {
        $hopid = $d2['rhoppingpoint'];
    }

    if( $api )
    {
        $list = array();
    }
    else
    {
        $list  = '';
    }

    $dnum  = 1;
    $anum  = 1;
    $count = 0;
    $class = '';

    while( $d = $db->fetch_array( $r ) )
    {
    	if( $d['lcid'] == $depart && $dep == false )
    	{
    		$style = '';
    		$dep   = true;
    		$class = "route-custom ";
    		$variable_depart = strtotime( $d['rdtime'] );
    		$variable_time_depart = date( 'H:i', strtotime( $d['rdtime'] ) ) ;

    		add_variable('custom_departname', $d['lcname'].' ( '.$variable_time_depart.' )'   );
    	}
    	elseif( $d['lcid'] == $arrive && $ret == false )
    	{
    		$style = '';
    		$ret   = true;
    		$class = "route-custom ";
    	}
    	else
    	{
    		if( $dep == false )
    		{
    			$style = 'style="text-decoration:line-through; display:none;"';
    		}
    		elseif( $dep == true && $ret == true )
    		{
    			$style = 'style="text-decoration:line-through; display:none;"';
    		}
    	}

        //-- Set Label
        if( $d['rdtype'] == '3' )
        {
            $lbl = 'Clearance';
        }
        else if( $d['rdtype'] == '2' )
        {
            //-- Get Location
            $s3 = 'SELECT a.lctype FROM ticket_location AS a WHERE a.lcid = %d';
            $q3 = $db->prepare_query( $s3, $arrive );
            $r3 = $db->do_query( $q3 );
            $d3 = $db->fetch_array( $r3 );

            if( in_array( $d3, array( 0, 2 ) ) )
            {
                $lbl = 'Arr. ' . $anum . ' - ';

                $anum++;
            }
            else
            {
                $lbl = 'Dep. ' . $dnum . ' - ';

                $dnum++;
            }
        }
        else if( $d['rdtype'] == '1' )
        {
            $lbl = 'Arr. ' . $anum . ' - ';

            $anum++;
        }
        else if( $d['rdtype'] == '0' )
        {
            $lbl = 'Dep. ' . $dnum . ' - ';

            $dnum++;
        }
        
        $time_before_depart  = isset( $time_before_depart ) ? $time_before_depart : '';
        $time_before_arrival = isset( $time_before_arrival ) ? $time_before_arrival : '';

        if( $time_before_depart == '' && $time_before_arrival == '' )
        {
            $time_before_depart  = $d['rdtime2'];
            $time_before_arrival = $d['rdtime'] ;
        }

        $t_arrival = strtotime( $d['rdtime'] );
        $lastTime  = strtotime($time_before_depart);
        $firstTime = strtotime( $d['rdtime'] );
        $timeDiff  = ( $firstTime - $lastTime ) / 60;

        if( $api === false )
        {
            $list .= '<li class= "different r-'.$count.'"' . $style . '><b>'.$timeDiff.' Minutes</b> <span class="arrow-down"></span></li>';
        }

        $time_before_depart  = $d['rdtime2'];
        $time_before_arrival = $d['rdtime'] ;

        if( $d['lcid'] == $hopid  )
        {
            if( $d['lcid'] == $arrive )
            {
                if( $api )
                {
                    $list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime'] ) ) );
                }
                else
                {
                    $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>' . $d['lcname'] . '</b> </li>';
                    $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                }
            }

            if( $lbl == "Clearance" )
            {
                if( $api )
                {
                    $list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime'] ) ) );
                }
                else
                {
                    $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Clearance </b>  <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b> Port Clearance : ' . $d['lcname'] . '</b> </li>';
                    $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' .  date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                }
            }
            else
            {
                if( $api )
                {
                    $list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime'] ) ) );
                }
                else
                {
                    $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>' . $d['lcname'] . '</b> </li>';
                    $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                }
            }
        }
        else
        {
            if( $count == 0 )
            {
                if( $api )
                {
                    $list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime2'] ) ) );
                }
                else
                {
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>' . $d['lcname'] . '</b> </li>';
                    $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>'.date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                }
            }

            if( $arrive == $d['lcid'] )
            {
                if( $api )
                {
                    $list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime'] ) ) );
                }
                else
                {
                    $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>'. $d['lcname'] . '</b> </li>';
                }
            }

            if( $lbl == "Clearance" )
            {
                if( $api )
                {
                    $list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime'] ) ) );
                }
                else
                {
                    $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Clearance </b>  <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b> </li>';
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>  Port Clearance : ' . $d['lcname'] . '</b> </li>';
                    $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                }
            }

            if( $count != 0 && $arrive != $d['lcid'] && $lbl != 'Clearance' )
            {
                // if( $timeDiff == 10 )
                // {
                //     $deapart_time = strtotime( $d['rdtime'] );

                //     $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( '-5 minutes', $deapart_time ) ) . '</b></li>';
                //     $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>'. $d['lcname'] . '</b> </li>';
                //     $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';

                // }
                // else
                // {
                //     $deapart_time = strtotime( $d['rdtime'] );

                //     $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                //     $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>'. $d['lcname'] . '</b> </li>';
                //     $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                // }

                if( $api )
                {
                    $list[] = array( 'style' => $style, 'name' => $d['lcname'], 'time' => date( 'H:i', strtotime( $d['rdtime'] ) ) );
                }
                else
                {
                    $list .= '<li class= "lbl-arrival '.$class .'r-'.$count .'"' . $style . '><b class="lbl-arrival"> Arrival </b> <b>' . date( 'H:i', strtotime( $d['rdtime'] ) ) . '</b></li>';
                    $list .= '<li class= "'.$class .'r-'.$count .'"' . $style . '><b>'. $d['lcname'] . '</b> </li>';
                    $list .= '<li class= "lbl-departure '.$class .'r-'.$count .'"' . $style . '><b class="lbl-departure"> Departure </b> <b>' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . '</b></li>';
                }
            }
        }

        if( $style == '' )
        {
            $count = $count + 1;
        }
    }

    return $list;
}

function get_route_first_port(  $rid, $port  )
{
    global $db;

    $s = 'SELECT
            a.lcid,
            a.rdtime,
            a.rdtime2,
            b.lcname
          FROM ticket_route_detail AS a
          INNER JOIN ticket_location AS b ON a.lcid = b.lcid
          WHERE a.rid = %d ORDER BY a.rdid ASC';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );

    while( $d = $db->fetch_array( $r ) )
    {
        if( $d['lcid'] == $port )
        {
            return $d['lcname']. ' (' . date( 'H:i', strtotime( $d['rdtime2'] ) ) . ')';

            break;
        }
    }
}

function set_seat_availibility_option( $period )
{
    global $db;

    $s = 'SELECT
            a.sid,
            b.rname,
            a.sfrom,
            a.sto,
            c.boid,
            c.boname,
            c.bopassenger
          FROM ticket_schedule AS a
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          WHERE a.sstatus = %s AND (( %s BETWEEN a.sfrom AND a.sto ) OR ( %s BETWEEN a.sfrom AND a.sto ))
          ORDER BY a.sfrom, a.rid';
    $q = $db->prepare_query( $s, 'publish', date( 'Y-m-01', strtotime( $period ) ), date( 'Y-m-t', strtotime( $period ) ) );
    $r = $db->do_query( $q );

    $boat_option = '';
    $sche_option = '';

    if( $db->num_rows( $r ) > 0 )
    {
        $idx         = 0;
        $sid         = '';
        $sfrom       = '';
        $sto         = '';
        $boname      = '';
        $bopassenger = '';
        $boat_arr    = array();

        //-- Boat & Schedule
        while( $d = $db->fetch_array( $r ) )
        {
            if( $idx == 0 )
            {
                $sid         = $d['sid'];
                $boid        = $d['boid'];
                $sfrom       = $d['sfrom'];
                $sto         = $d['sto'];
                $boname      = $d['boname'];
                $bopassenger = $d['bopassenger'];
            }

            if( !in_array( $d['boid'], $boat_arr ) )
            {
                $boat_option .= '
                <option value="' . $d['boid'] . '" ' . ( $boid == $d['boid'] ? 'selected' : '' ) . '>
                    ' . $d['boname'] . '
                </option>';

                $boat_arr[] = $d['boid'];
            }

            $sche_option .= '
            <option value="' . $d['sid'] . '" data-boid="' . $d['boid'] . '" data-name="' . $d['boname'] . '" data-passenger= "' . $d['bopassenger'] . '" data-from="' . $d['sfrom'] . '" data-to="' . $d['sto'] . '" ' . ( $sid == $d['sid'] ? 'selected' : '' ) . '>
                ' . date( 'd M y', strtotime( $d['sfrom'] ) ) . ' - ' . date( 'd M y', strtotime( $d['sto'] ) ) . ' / ' . $d['rname'] .'
            </option>';

            $idx++;
        }

        return array( 'boat' => $boat_option, 'schedule' => $sche_option );
    }
    else
    {
        return array( 'boat' => $boat_option, 'schedule' => $sche_option );
    }
}

function get_seat_availibility_calendar( $month, $year, $sid = '' )
{
    global $db;

    $temp = array();
    $list = array();

    $s = 'SELECT * FROM ticket_schedule AS a
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          WHERE a.sid = %d';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $fdate  = $year . '-' . $month . '-01';
            $next   = strtotime( '+1 month', strtotime( $fdate ) );
            $nmonth = date( 'n', $next );
            $nyear  = date( 'Y', $next );
            $ldate  = $nyear . '-' . $nmonth . '-01';

            $period = get_interval_date( $d['sfrom'], $d['sto'] );
            $dlist  = get_interval_date( $fdate, $ldate );

            foreach( $dlist as $day )
            {
                $time  = strtotime( $day );
                $max   = get_total_seat_capacity( $d['rid'], $time );
                $key   = date( 'j', $time ) . '-' . date( 'n', $time ) . '-' . date( 'Y', $time );

                if( $d['rtype'] == '1' && !empty( $d['rhoppingpoint'] ) )
                {
                    $allot = get_seat_availibility_num_by_hopping_trip( $max, $d['rid'], $time, true );
                }
                else
                {
                    $allot = get_seat_availibility_num_by_schedule( $max, $d['rid'], $sid, $time );
                }

                if( in_array( date( 'm', $time ), array( $month, $nmonth ) ) == $month && in_array( $day, $period ) )
                {
                    if( is_array( $allot ) )
                    {
                        $temp[$key][] = array( 'available' => $allot, 'capacity' => $max, 'type' => $d['rtype'], 'status' => 'open' );
                    }
                    else
                    {
                        $temp[$key][] = array( 'available' => $allot, 'capacity' => $max, 'type' => $d['rtype'], 'status' => ( $allot == 0 ? 'closed' : 'open' ) );
                    }
                }
                else
                {
                    $temp[$key][] = array( 'available' => 0, 'capacity' => $max, 'type' => $d['rtype'], 'status' => 'closed' );
                }
            }
        }

        foreach( $temp as $date => $obj )
        {
            foreach( $obj as $d )
            {
                if( is_array( $d['available'] ) )
                {
                    $content = array();

                    foreach( $d['available'] as $dd )
                    {
                        $content[] = $dd['point'] . ' : ' . $dd['seat_max'] . '/' . $d['capacity'];
                    }

                    $list[$date] = array( 'content' => implode( "\n", $content ), 'type' => $d['type'], 'status' => 'open' );
                }
                else
                {
                    $list[$date] = array( 'content' => $d['available'] . '/' . $d['capacity'], 'type' => $d['type'], 'status' => $d['status'] );
                }
            }
        }
    }

    return $list;
}

function get_booking_source( $chid, $agid = null )
{
    global $db;

    if( empty( $agid ) )
    {
        $s = 'SELECT
                a.chid AS id,
                a.chname AS name
              FROM ticket_channel AS a
              LEFT JOIN ticket_agent AS b ON b.chid = a.chid
              WHERE a.chstatus = %s AND a.chid = %s
              ORDER BY b.agname ASC';
        $q = $db->prepare_query( $s, 'publish', $chid );
    }
    else
    {
        $s = 'SELECT
                b.agid AS id,
                b.agname AS name
              FROM ticket_channel AS a
              LEFT JOIN ticket_agent AS b ON b.chid = a.chid
              WHERE a.chstatus = %s AND a.chid = %s AND b.agid = %s
              ORDER BY b.agname ASC';
        $q = $db->prepare_query( $s, 'publish', $chid, $agid );
    }

    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return $db->fetch_array( $r );
    }
    else
    {
        return array();
    }
}

function get_booking_source_list( $suspend = false )
{
    global $db;

	$agstatus = $suspend == true ? '': 'AND b.agstatus = "0" OR b.agstatus IS NULL';

    $s = 'SELECT
            a.chid,
            a.chname,
            b.agid,
            b.agname
          FROM ticket_channel AS a
          LEFT JOIN ticket_agent AS b ON b.chid = a.chid
          WHERE a.chstatus = %s '.$agstatus.'
          ORDER BY b.agname ASC';
    $q = $db->prepare_query( $s, 'publish' );
    $r = $db->do_query( $q );

    $source = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $source[] = $d;
        }
    }

    return $source;
}

function get_availibility_loc_option( $empty_txt = '', $port = '', $type = '' )
{
	$loc    = get_location_list( null, $type);
    $option = '<option value="">' . $empty_txt . '</option>';

    if( !empty( $loc ) )
    {
        foreach( $loc as $d )
        {
            if( $d['lctype'] != '3' )
            {
                $option .= '<option value="' . $d['lcid'] . '" ' . ( $d['lcid'] == $port ? 'selected' : '' ) . '>' . $d['lcname'] . '</option>';
            }
        }
    }

    return $option;
}

function get_booking_source_option( $chid = '', $agid = '', $use_empty = true, $empty_text = 'Select Booking Source', $suspend = false )
{
    $source = get_booking_source_list( $suspend );
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $source ) )
    {
        foreach( $source as $d )
        {
            $value  = empty( $d['agid'] ) ? $d['chid'] : $d['chid'] . '|' . $d['agid'];
            $name   = empty( $d['agid'] ) ? $d['chname'] : $d['agname'];

            if( empty( $agid ) )
            {
                $sorceval = $chid;
                $selected = $sorceval == $value ? 'selected' : '';
            }
            else
            {
                $sorceval = $chid . '|' . $agid;
                $selected = $sorceval == $value ? 'selected' : '';
            }

            $option .= '<option value="' . $value . '" ' . $selected . '>' . $name . '</option>';
        }
    }

    return $option;
}

function get_booking_source_option_revenue( $bsource = '', $use_empty = true, $empty_text = 'Select Booking Source', $suspend = false )
{
    $source = get_booking_source_list( $suspend );
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $source ) )
    {
        foreach( $source as $x => $d )
        {
            $value  = empty( $d['agid'] ) ? $d['chid'] : $d['chid'] . '|' . $d['agid'];
            $name   = empty( $d['agid'] ) ? $d['chname'] : $d['agname'];

            if ( is_array( $bsource ) )
            {
                $option .= '<option value="' . $value . '" ' . ( in_array( $value, $bsource ) ? 'selected' : '' ) . ' >' . $name . '</option>';
            }
            else
            {
                $option .= '<option value="' . $value . '" ' . ( $d == $bsource ? 'selected' : '' ) . ' >' . $name . '</option>';
            }
        }
    }

    return $option;
}

function is_check_availability()
{
	if( isset( $_GET['step'] ) && $_GET['step'] == 'search-result' )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function is_review_booking()
{
	if( isset( $_GET['step'] ) && $_GET['step'] == 'review' )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function is_detail_booking()
{
	if( isset( $_GET['step'] ) && $_GET['step'] == 'detail' )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function is_booking_payment()
{
	if( isset( $_GET['step'] ) && $_GET['step'] == 'payment' )
	{
		return true;
	}
	else
	{
		return false;
	}
}


function is_not_pass_time_limit( $rid, $sess, $type, $is_edit = false )
{
    global $db;

    if( $is_edit )
    {
        $locid = $sess[ $type . '_start_point' ];
        $dport = $sess[ $type . '_start_point' ];
        $aport = $sess[ $type . '_end_point' ];
        $dates = $sess[ 'depart_date' ];
    }
    else
    {
        $locid = $sess[ $type . '_port'];
        $dates = $sess[ 'date_of_' . $type ];

        if( $type == 'depart' )
        {
            $dport = $sess[ 'depart_port'];
            $aport = $sess[ 'destination_port'];
        }
        else
        {
            $dport = $sess[ 'return_port'];
            $aport = $sess[ 'destination_port_rtn'];
        }
    }

    $s = 'SELECT a.rid, a.rtimelimit, b.rdtime FROM ticket_route AS a
          LEFT JOIN ticket_route_detail AS b ON b.rid = a.rid
          WHERE a.rid = %d AND b.lcid = %d';
    $q = $db->prepare_query( $s, $rid, $locid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $port_type    = get_location( $dport, 'lctype' );
        $port_type_to = get_location( $aport, 'lctype' );

        $sctime = strtotime( date( 'Y-m-d H:i:s' ) );
        $sdtime = strtotime( $dates . ' ' . $d['rdtime'] );

        if( $sctime < $sdtime )
        {
            $current_time = date( 'Y-m-d H:i:s', $sctime );
            $depart_time  = date( 'Y-m-d H:i:s', $sdtime );

            $ctime = new DateTime( $current_time );
            $dtime = new DateTime( $depart_time );

            $interval = $ctime->diff( $dtime );

            if( $interval->days < 1 && $interval->h < $d['rtimelimit'] )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
}

function is_block_request_transport( $rid, $sess, $type, $is_edit = false )
{
    global $db;

    if( $is_edit )
    {
        $locid = $sess[ $type . '_start_point' ];
        $dport = $sess[ $type . '_start_point' ];
        $aport = $sess[ $type . '_end_point' ];
        $dates = $sess[ 'depart_date' ];
    }
    else
    {
        $locid = $sess[ $type . '_port'];
        $dates = $sess[ 'date_of_' . $type ];

        if( $type == 'depart' )
        {
            $dport = $sess[ 'depart_port'];
            $aport = $sess[ 'destination_port'];
        }
        else
        {
            $dport = $sess[ 'return_port'];
            $aport = $sess[ 'destination_port_rtn'];
        }
    }

    $s = 'SELECT a.rid, a.rcot, b.rdtime FROM ticket_route AS a
          LEFT JOIN ticket_route_detail AS b ON b.rid = a.rid
          WHERE a.rid = %d AND b.lcid = %d';
    $q = $db->prepare_query( $s, $rid, $locid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $port_type    = get_location( $dport, 'lctype' );
        $port_type_to = get_location( $aport, 'lctype' );

        $sctime = strtotime( date( 'Y-m-d H:i:s' ) );
        $sdtime = strtotime( $dates . ' ' . $d['rdtime'] );

        if( $sctime < $sdtime )
        {
            $current_time = date( 'Y-m-d H:i:s', $sctime );
            $depart_time  = date( 'Y-m-d H:i:s', $sdtime );

            $ctime = new DateTime( $current_time );
            $dtime = new DateTime( $depart_time );

            $interval = $ctime->diff( $dtime );

            if( $interval->days < 1 && $interval->h < $d['rcot'] )
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
}

function get_variable_by_port_type( $from, $to )
{
    $port_type    = get_location( $from, 'lctype' );
    $port_type_to = get_location( $to, 'lctype' );

    if( $port_type == '1' )
    {
        if( $port_type_to == '2' )
        {
            $result = array(
                'trans_type' => 'drop-off',
                'trans_txt'  => 'Drop-off',
                'port_id'    => $to,
                'flight_txt' => '',
                'drop_sts'   => true,
                'pick_sts'   => false,
            );
        }
        else
        {
            $result = array(
                'trans_type' => 'drop-off',
                'trans_txt'  => 'Drop-off',
                'port_id'    => $to,
                'flight_txt' => 'Flight Take Off Time',
                'drop_sts'   => true,
                'pick_sts'   => false,
            );
        }
    }
    elseif( $port_type == '2' )
    {
        if( $port_type_to == '1' )
        {
            $result = array(
                'trans_type' => 'pickup',
                'trans_txt'  => 'Pickup',
                'port_id'    => $from,
                'flight_txt' => '',
                'drop_sts'   => false,
                'pick_sts'   => true,
            );
        }
        else
        {
            $result = array(
                'trans_type' => 'drop-off',
                'trans_txt'  => 'Drop-off',
                'port_id'    => $to,
                'flight_txt' => 'Flight Take Off Time',
                'drop_sts'   => true,
                'pick_sts'   => false,
            );
        }
    }
    else
    {
        $result = array(
            'trans_type' => 'pickup',
            'trans_txt'  => 'Pickup',
            'port_id'    => $from,
            'flight_txt' => 'Flight Landing Time',
            'drop_sts'   => false,
            'pick_sts'   => true,
        );
    }

    return $result;
}

function check_valid_transport_time( $post, $sess )
{
    global $db;

    extract( $post );

    // print_r($post);
    // print_r($sess);
    // exit;

    if( isset( $trans ) )
    {
        $error = array();

        foreach( $trans as $trip_type => $obj )
        {
            foreach( $obj as $trans_type => $dt )
            {
                if( isset( $dt['ftime'] ) && isset( $dt['taairport'] ) && $dt['taairport'] == 1 )
                {
                    if( empty( $dt['ftime'] ) )
                    {
                        if( $trans_type == 'pickup' )
                        {
                            $error[] = 'Sorry ' . $trip_type . ' trip flight landing time can\'t be empty';
                        }
                        elseif( $trans_type == 'drop-off' )
                        {
                            $error[] = 'Sorry ' . $trip_type . ' trip flight take off time can\'t be empty';
                        }
                    }
                    else
                    {
                        if( $trans_type == 'pickup' )
                        {
                            if( $trip_type == 'departure' )
                            {
                                $dloc = $sess['depart_port'];
                            }
                            else
                            {
                                $dloc = $sess['return_port'];
                            }
                        }
                        else
                        {
                            if( $trip_type == 'departure' )
                            {
                                $dloc = $sess['destination_port'];
                            }
                            else
                            {
                                $dloc = $sess['destination_port_rtn'];
                            }
                        }
                            
                        $s = 'SELECT a.rdtime, b.rpcot
                              FROM ticket_route_detail AS a
                              LEFT JOIN ticket_route_pickup_drop AS b ON b.rdid = a.rdid
                              WHERE a.rid = %d AND b.taid = %d AND a.lcid = %d';
                        $q = $db->prepare_query( $s, $sess['trip'][$trip_type]['id'], $dt['taid'], $dloc );
                        
                        $r = $db->do_query( $q );

                        if( $db->num_rows( $r ) > 0 )
                        {
                            $d = $db->fetch_array( $r );

                            $sftime = strtotime( $dt['ftime'] );
                            $sddate = $trip_type == 'departure' ? $sess['date_of_depart'] : $sess['date_of_return'];
                            $sdtime = strtotime( $sddate . ' ' . $d['rdtime'] );

                            if( $trans_type == 'pickup' )
                            {
                                //-- Check flight time
                                //-- Set error if flight landing time
                                //-- Greater than boat depart/arrive time
                                if( $sdtime > $sftime )
                                {
                                    $flight_time = date( 'Y-m-d H:i:s', $sftime );
                                    $depart_time = date( 'Y-m-d H:i:s', $sdtime );

                                    $ftime = new DateTime( $flight_time );
                                    $dtime = new DateTime( $depart_time );

                                    $interval = $ftime->diff( $dtime );

                                    if( $interval->days < 1 && $interval->h < $d['rpcot'] )
                                    {
                                        $error[] = 'Sorry can\'t accept ' . $trip_type . ' trip transport request base on your flight landing time';
                                    }
                                }
                                else
                                {
                                    $error[] = 'Sorry can\'t accept ' . $trip_type . ' trip transport request base on your flight landing time';
                                }
                            }
                            elseif( $trans_type == 'drop-off' )
                            {
                                //-- Check flight time
                                //-- Set error if boat depart/arrive time
                                //-- Greater than flight take off time
                                if( $sdtime < $sftime )
                                {
                                    $flight_time = date( 'Y-m-d H:i:s', $sftime );
                                    $depart_time = date( 'Y-m-d H:i:s', $sdtime );

                                    $ftime = new DateTime( $flight_time );
                                    $dtime = new DateTime( $depart_time );

                                    $interval = $ftime->diff( $dtime );

                                    if( $interval->days < 1 && $interval->h < $d['rpcot'] )
                                    {
                                        $error[] = 'Sorry can\'t accept ' . $trip_type . ' trip transport request base on your flight take off time';
                                    }
                                }
                                else
                                {
                                    $error[] = 'Sorry can\'t accept ' . $trip_type . ' trip transport request base on your flight take off time';
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if( !empty( $error ) )
    {
        return array( 'type' => 'error', 'content' => $error );
    }
}

function unset_booking_session()
{
	if( isset( $_SESSION['booking'] ) )
	{
		unset( $_SESSION['booking'] );
	}
}

function add_booking_item( $idx, $item )
{
	$_SESSION['booking'][$idx] = $item;

	setcookie( 'booking', serialize( $_SESSION['booking'] ), 3600 );
}

function booking_item( $idx = '' )
{
    if( empty( $idx ) )
    {
        if( isset( $_SESSION['booking'] ) )
        {
            foreach( $_SESSION['booking'] as $idx => $data )
            {
                return array( 'sess_id' => $idx, 'sess_data' => $data );
            }
        }
        else
        {
            return array();
        }
    }
    else
    {
        if( isset( $_SESSION['booking'][$idx] ) )
        {
            return array( 'sess_id' => $idx, 'sess_data' => $_SESSION['booking'][$idx] );
        }
        else
        {
            return array();
        }
    }
}

function booking_subtotal( $idx )
{
	$sess = booking_item( $idx );
	$subs = 0;

	if( !empty( $sess ) && isset( $sess['sess_data']['trip'] ) )
	{
		extract( $sess['sess_data']['trip'] );

		if( empty( $sess['sess_data']['type_of_route'] ) )
		{
            $dep_addons = 0;

            if( isset( $departure['addons'] ) )
            {
                foreach( $departure['addons'] as $d )
                {
                    $dep_addons = $dep_addons + ( $d['price'] * $d['paxs'] );
                }
            }

			$dep_price = isset( $departure['total'] ) ? $departure['total'] : 0;
			$dep_disc  = isset( $departure['discount'] ) ? $departure['discount'] : 0;
			$dep_pfee  = isset( $departure['pickup']['transport_fee'] ) ? $departure['pickup']['transport_fee'] : 0;
            $dep_dfee  = isset( $departure['drop-off']['transport_fee'] ) ? $departure['drop-off']['transport_fee'] : 0;

            $dep_fee    = $dep_pfee + $dep_dfee;
			$subs       = $dep_fee + $dep_addons + ( $dep_price - $dep_disc );
		}
		else
		{
            $dep_addons = 0;

            if( isset( $departure['addons'] ) )
            {
                foreach( $departure['addons'] as $d )
                {
                    $dep_addons = $dep_addons + ( $d['price'] * $d['paxs'] );
                }
            }

            $rtn_addons = 0;

            if( isset( $return['addons'] ) )
            {
                foreach( $return['addons'] as $d )
                {
                    $rtn_addons = $rtn_addons + ( $d['price'] * $d['paxs'] );
                }
            }

            $dep_price  = isset( $departure['total'] ) ? $departure['total'] : 0;
            $dep_disc   = isset( $departure['discount'] ) ? $departure['discount'] : 0;
            $dep_pfee   = isset( $departure['pickup']['transport_fee'] ) ? $departure['pickup']['transport_fee'] : 0;
            $dep_dfee   = isset( $departure['drop-off']['transport_fee'] ) ? $departure['drop-off']['transport_fee'] : 0;
            $dep_fee    = $dep_pfee + $dep_dfee;

			$rtn_price  = isset( $return['total'] ) ? $return['total'] : 0;
			$rtn_disc   = isset( $return['discount'] ) ? $return['discount'] : 0;
            $rtn_pfee   = isset( $return['pickup']['transport_fee'] ) ? $return['pickup']['transport_fee'] : 0;
            $rtn_dfee   = isset( $return['drop-off']['transport_fee'] ) ? $return['drop-off']['transport_fee'] : 0;
            $rtn_fee    = $rtn_pfee + $rtn_dfee;
			$subs       = $dep_fee + $dep_addons + ( $dep_price - $dep_disc ) + $rtn_fee + $rtn_addons + ( $rtn_price - $rtn_disc );
		}

		$sess['sess_data']['subtotal'] = $subs;

		add_booking_item( $idx, $sess['sess_data'] );
	}

	return $subs;
}

function get_pmcode_total_used( $pmcode, $bddate)
{
    global $db;
		$bddate = date( 'yy-m-d', strtotime( $bddate ) );
		$total_seats = 0;
    $s = 'SELECT
            a.*,
            d.ntitle,
            b.agname,
            b.agemail,
            b.agphone,
            b.agpayment_type,
            c.chname,
            e.bdid,
						e.num_adult,
						e.num_child,
						e.num_infant
          FROM ticket_booking AS a
          LEFT JOIN ticket_agent AS b ON a.agid = b.agid
          LEFT JOIN ticket_channel AS c ON a.chid = c.chid
          LEFT JOIN ticket_notification AS d ON d.bid = a.bid
          LEFT JOIN ticket_booking_detail AS e ON a.bid = e.bid
          WHERE a.pmcode = %s AND a.bstt <> %s AND e.bddate = %s';
    $q = $db->prepare_query( $s, $pmcode, 'ar' , $bddate);
    $r = $db->do_query( $q );


		while( $d = $db->fetch_array( $r ) )
		{
			$total_seats +=  $d['num_adult'];
			$total_seats +=  $d['num_child'];
			$total_seats +=  $d['num_infant'];
		}
		return $total_seats;
}
function check_limit_perday($usage_promo, $limit_promo, $total_seats_order){
	$status = 0;
	if($limit_promo == 0){
		$status = 1;
	}
	else{
		$limit_available = $limit_promo - $usage_promo;
		$check_limit = $limit_available - $total_seats_order;
		if($check_limit < 0 ){
			$status = 0;
		}
		else{
			$status = 1;
		}
	}
	return $status;
}

function booking_discount( $idx )
{
	global $db;

	$sess = booking_item( $idx );
	$disc = 0;

	if( !empty( $sess ) && !empty( $sess['sess_data']['promo_code'] ) )
	{
		$s = 'SELECT * FROM ticket_promo WHERE pmcode = %s AND pmstatus = %s';
	    $q = $db->prepare_query( $s, $sess['sess_data']['promo_code'], '1' );
	    $r = $db->do_query( $q );
        
        $promo_usage_perday = get_pmcode_total_used($sess['sess_data']['promo_code'] , $sess['sess_data']['date_of_depart']);
		$promo_limit_perday = get_promo_limit_perday($sess['sess_data']['promo_code']);
		$total_seats = $sess['sess_data']['adult_num'] + $sess['sess_data']['child_num'] + $sess['sess_data']['infant_num'];
	    if( $db->num_rows( $r ) > 0 && check_limit_perday($promo_usage_perday, $promo_limit_perday , $total_seats))
	    {
	    	$d = $db->fetch_array( $r );

            //-- Count Total Trip
            $trip_total = 0;

            if( isset( $sess['sess_data']['trip'] ) )
            {
                foreach( $sess['sess_data']['trip'] as $t )
                {
                    $trip_total += $t['total'] - $t['discount'];
                }
            }

            //-- Check 24 Advance Booking
            if( $d['pmbook24inadvance'] == '2' )
            {
                $dpfrom = strtotime( $sess['sess_data']['date_of_depart'] );
                $drprm  = time() + 86400;
                $bodate = time();

                if( $dpfrom <= $drprm && $bodate <= $drprm )
                {
                    if( get_remain_limit( $d ) > 0 )
                    {
                        if( $d['pmdiscounttype'] == '0' )
                        {
                            $disc = ( floatval( $d['pmdiscount'] ) / 100 ) * floatval( $trip_total );
                        }
                        elseif( $d['pmdiscounttype'] == '2' )
                        {
                            $disc = floatval( $trip_total ) - floatval( $d['pmdiscount'] );
                        }
                        else
                        {
                            $disc = floatval( $d['pmdiscount'] );
                        }
                    }
                }
            }
            else
            {
                //-- Check Promo Code Application
                $valid_apps = true;

                if( $d['pmapplication'] != '' && isset( $sess['sess_data']['promo_apply'] ) && $sess['sess_data']['promo_apply'] != '' )
                {
                    $pmapplication = json_decode( $d['pmapplication'], true );

                    if( $pmapplication !== null && json_last_error() === JSON_ERROR_NONE && in_array( $sess['sess_data']['promo_apply'], $pmapplication ) === false )
                    {
                        $valid_apps = false;
                    }
                }

                if( $valid_apps )
                {
                    $pmfrom = strtotime( $d['pmfrom'] );
                    $pmto   = strtotime( $d['pmto'] );

                    //-- One Way Trip
                    if( empty( $sess['sess_data']['date_of_return'] ) )
                    {
                        //-- Check Baseon
                        if( $d['pmbaseon'] == '0' )
                        {
                            $trfrom = strtotime( $sess['sess_data']['date_of_depart'] );
                        }
                        else
                        {
                            $trfrom = time();
                        }

                        //-- Check Schedule
                        if( $d['pmsid'] != '' )
                        {
                            $pmsid = json_decode( $d['pmsid'], true );

                            if( $pmsid !== null && json_last_error() === JSON_ERROR_NONE && in_array( $sess['sess_data']['trip']['departure']['sid'], $pmsid ) )
                            {
                                $valid_sid = true;
                            }
                            else
                            {
                                $valid_sid = false;
                            }
                        }
                        else
                        {
                            $valid_sid = true;
                        }

                        if( $pmfrom <= $trfrom && $pmto >= $trfrom && $valid_sid )
                        {
                            if( get_remain_limit( $d ) > 0 )
                            {
                                if( $d['pmadditionaldisc'] == 1 )
                                {
                                    $sess['sess_data']['trip']['departure']['discount_old'] = $sess['sess_data']['trip']['departure']['discount'];

                                    $sess['sess_data']['trip']['departure']['discount'] = 0;

                                    add_booking_item( $idx, $sess['sess_data'] );

                                    booking_subtotal( $idx );

                                    $sess = booking_item( $idx );
                                }

                                if( $d['pmdiscounttype'] == '0' )
                                {
                                    $disc = ( floatval( $d['pmdiscount'] ) / 100 ) * floatval( $trip_total );
                                }
                                elseif( $d['pmdiscounttype'] == '2' )
                                {
                                    $pmdiscount = floatval( $d['pmdiscount'] );
                                    $num_adult  = floatval( $sess['sess_data']['adult_num'] );
                                    $num_child  = floatval( $sess['sess_data']['child_num'] );
                                    $num_infant = floatval( $sess['sess_data']['infant_num'] );

                                    $dep_adisc = ( floatval( $sess['sess_data']['trip']['departure']['adult_price'] ) - $pmdiscount ) * $num_adult;
                                    $dep_cdisc = ( floatval( $sess['sess_data']['trip']['departure']['child_price'] ) - $pmdiscount ) * $num_child;
                                    $dep_idisc = ( floatval( $sess['sess_data']['trip']['departure']['infant_price'] ) - $pmdiscount ) * $num_infant;

                                    $dep_adisc = $dep_adisc < 0 ? 0 : $dep_adisc;
                                    $dep_cdisc = $dep_cdisc < 0 ? 0 : $dep_cdisc;
                                    $dep_idisc = $dep_idisc < 0 ? 0 : $dep_idisc;

                                    $disc = $dep_adisc + $dep_cdisc + $dep_idisc;
                                }
                                else
                                {
                                    $disc = floatval( $d['pmdiscount'] );
                                }
                            }
                        }
                    }
                    else
                    {
                        //-- Check Baseon
                        if( $d['pmbaseon'] == '0' )
                        {
                            $trfrom = strtotime( $sess['sess_data']['date_of_depart'] );
                            $trto   = strtotime( $sess['sess_data']['date_of_return'] );
                        }
                        else
                        {
                            $trfrom = time();
                            $trto   = time();
                        }

                        //-- Check Schedule
                        if( $d['pmsid'] != '' )
                        {
                            $pmsid = json_decode( $d['pmsid'], true );

                            if( $pmsid !== null && json_last_error() === JSON_ERROR_NONE && ( in_array( $sess['sess_data']['trip']['departure']['sid'], $pmsid ) || in_array( $sess['sess_data']['trip']['return']['sid'], $pmsid ) ) )
                            {
                                $valid_sid = true;
                            }
                            else
                            {
                                $valid_sid = false;
                            }
                        }
                        else
                        {
                            $valid_sid = true;
                        }

                        if( ( ( $pmfrom <= $trfrom && $pmto >= $trfrom ) || ( $pmfrom <= $trto && $pmto >= $trto ) ) && $valid_sid )
                        {
                            if( get_remain_limit( $d ) > 0 )
                            {
                                if( $d['pmadditionaldisc'] == 1 )
                                {
                                    $sess['sess_data']['trip']['departure']['adult_disc_old']  = $sess['sess_data']['trip']['departure']['adult_disc'];
                                    $sess['sess_data']['trip']['departure']['child_disc_old']  = $sess['sess_data']['trip']['departure']['child_disc'];
                                    $sess['sess_data']['trip']['departure']['infant_disc_old'] = $sess['sess_data']['trip']['departure']['infant_disc'];
                                    $sess['sess_data']['trip']['departure']['discount_old']    = $sess['sess_data']['trip']['departure']['discount'];

                                    $sess['sess_data']['trip']['return']['adult_disc_old']  = $sess['sess_data']['trip']['return']['adult_disc'];
                                    $sess['sess_data']['trip']['return']['child_disc_old']  = $sess['sess_data']['trip']['return']['child_disc'];
                                    $sess['sess_data']['trip']['return']['infant_disc_old'] = $sess['sess_data']['trip']['return']['infant_disc'];
                                    $sess['sess_data']['trip']['return']['discount_old']    = $sess['sess_data']['trip']['return']['discount'];

                                    $sess['sess_data']['trip']['departure']['adult_disc']  = 0;
                                    $sess['sess_data']['trip']['departure']['child_disc']  = 0;
                                    $sess['sess_data']['trip']['departure']['infant_disc'] = 0;
                                    $sess['sess_data']['trip']['departure']['discount']    = 0;

                                    $sess['sess_data']['trip']['return']['adult_disc']  = 0;
                                    $sess['sess_data']['trip']['return']['child_disc']  = 0;
                                    $sess['sess_data']['trip']['return']['infant_disc'] = 0;
                                    $sess['sess_data']['trip']['return']['discount']    = 0;

                                    add_booking_item( $idx, $sess['sess_data'] );

                                    booking_subtotal( $idx );

                                    $sess = booking_item( $idx );
                                }

                                if( $d['pmdiscounttype'] == '0' )
                                {
                                    $disc = ( floatval( $d['pmdiscount'] ) / 100 ) * floatval( $trip_total );
                                }
                                elseif( $d['pmdiscounttype'] == '2' )
                                {
                                    $pmdiscount = floatval( $d['pmdiscount'] );
                                    $num_adult  = floatval( $sess['sess_data']['adult_num'] );
                                    $num_child  = floatval( $sess['sess_data']['child_num'] );
                                    $num_infant = floatval( $sess['sess_data']['infant_num'] );

                                    $dep_adisc = ( floatval( $sess['sess_data']['trip']['departure']['adult_price'] ) - $pmdiscount ) * $num_adult;
                                    $dep_cdisc = ( floatval( $sess['sess_data']['trip']['departure']['child_price'] ) - $pmdiscount ) * $num_child;
                                    $dep_idisc = ( floatval( $sess['sess_data']['trip']['departure']['infant_price'] ) - $pmdiscount ) * $num_infant;

                                    $rtn_adisc = ( floatval( $sess['sess_data']['trip']['return']['adult_price'] ) - $pmdiscount ) * $num_adult;
                                    $rtn_cdisc = ( floatval( $sess['sess_data']['trip']['return']['child_price'] ) - $pmdiscount ) * $num_child;
                                    $rtn_idisc = ( floatval( $sess['sess_data']['trip']['return']['infant_price'] ) - $pmdiscount ) * $num_infant;

                                    $dep_adisc = $dep_adisc < 0 ? 0 : $dep_adisc;
                                    $dep_cdisc = $dep_cdisc < 0 ? 0 : $dep_cdisc;
                                    $dep_idisc = $dep_idisc < 0 ? 0 : $dep_idisc;

                                    $rtn_adisc = $rtn_adisc < 0 ? 0 : $rtn_adisc;
                                    $rtn_cdisc = $rtn_cdisc < 0 ? 0 : $rtn_cdisc;
                                    $rtn_idisc = $rtn_idisc < 0 ? 0 : $rtn_idisc;

                                    $disc = $dep_adisc + $dep_cdisc + $dep_idisc + $rtn_adisc + $rtn_cdisc + $rtn_idisc;
                                }
                                else
                                {
                                    $disc = floatval( $d['pmdiscount'] );
                                }
                            }
                        }
                    }
                }
            }

            $sess['sess_data']['freelance_code']  = '';
            $sess['sess_data']['discount']        = $disc;
            $sess['sess_data']['promo_disc']      = $d['pmdiscount'];
            $sess['sess_data']['promo_disc_type'] = $d['pmdiscounttype'];
            $sess['sess_data']['promo_code']      = $disc <= 0 ? '' : $sess['sess_data']['promo_code'];
	    }
        else
        {
            $s = 'SELECT * FROM ticket_agent_freelance_code
                  WHERE agfreelance_code = %s AND agfneverend = %s
                  OR ( agfneverend = %s AND DATE(agfvdatefrom) <= CURDATE() AND DATE(agfvdateto) >= CURDATE() );';
            $q = $db->prepare_query( $s, $sess['sess_data']['promo_code'], '1', '0' );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
                $d = $db->fetch_array( $r );

                $sess['sess_data']['discount']        = 0;
                $sess['sess_data']['promo_disc']      = 0;
                $sess['sess_data']['promo_code']      = '';
                $sess['sess_data']['promo_disc_type'] = '0';
                $sess['sess_data']['freelance_code']  = $d['agfreelance_code'];
            }
            else
            {
                $sess['sess_data']['discount']        = 0;
                $sess['sess_data']['promo_disc']      = 0;
                $sess['sess_data']['promo_code']      = '';
                $sess['sess_data']['promo_disc_type'] = '0';
                $sess['sess_data']['freelance_code']  = '';
            }
        }

		add_booking_item( $idx, $sess['sess_data'] );
	}

	return $disc;
}

function booking_grandtotal( $idx )
{
	$sess   = booking_item( $idx );
	$gtotal = 0;

	if( !empty( $sess ) )
	{
		$discount = booking_discount( $idx );
        $subtotal = booking_subtotal( $idx );
		$nsess    = booking_item( $idx );

		$nsess['sess_data']['subtotal']   = $subtotal;
		$nsess['sess_data']['discount']   = $discount;
		$nsess['sess_data']['grandtotal'] = $subtotal - $discount;

		add_booking_item( $idx, $nsess['sess_data'] );
	}

	return $gtotal;
}

function add_add_ons_detail( $post, $is_ajax = false )
{
    extract( $post );

    if( $is_ajax )
    {
        if( isset( $sid ) && !empty( $sid ) )
        {
            $sess = booking_item( $sid );

            if( !empty( $sess ) )
            {
                if( !empty( $addons ) )
                {
                    if( isset( $sess['sess_data']['trip']['departure'] ) )
                    {
                        $sess['sess_data']['trip']['departure']['addons'] = array();

                        if( array_search( 'departure', array_column( $addons, 'type' ) ) === false )
                        {
                            $sess['sess_data']['trip']['departure']['addons'] = array();
                        }
                    }

                    if( isset( $sess['sess_data']['trip']['return'] ) )
                    {
                        $sess['sess_data']['trip']['return']['addons'] = array();

                        if( array_search( 'return', array_column( $addons, 'type' ) ) === false )
                        {
                            $sess['sess_data']['trip']['return']['addons'] = array();
                        }
                    }

                    foreach( $addons as $d )
                    {
                        $sess['sess_data']['trip'][ $d['type'] ]['addons'][] = array(
                            'id'    => $d['id'],
                            'notes' => $d['notes'],
                            'price' => $d['price'],
                            'paxs'  => $d['paxs'],
                        );
                    }
                }
                else
                {
                    if( isset( $sess['sess_data']['trip']['departure']['addons'] ) )
                    {
                        $sess['sess_data']['trip']['departure']['addons'] = array();
                    }

                    if( isset( $sess['sess_data']['trip']['return']['addons'] ) )
                    {
                        $sess['sess_data']['trip']['return']['addons'] = array();
                    }
                }

                add_booking_item( $sid, $sess['sess_data'] );

                booking_grandtotal( $sid );

                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        $sess = booking_item( $sess_id );

        if( !empty( $sess ) )
        {
            if( !empty( $addons ) )
            {
                foreach( $addons as $trip_type => $obj )
                {
                    if( isset( $obj['sid'] ) )
                    {
                        $sess['sess_data']['trip'][$trip_type]['addons'] = $obj;
                    }
                }

                add_booking_item( $sess_id, $sess['sess_data'] );

                booking_grandtotal( $sess_id );

                return true;
            }
        }
    }
}

function add_transport_detail( $post, $is_ajax = false )
{
	extract( $post );

    if( $is_ajax )
    {
        if( isset( $sid ) && !empty( $sid ) )
        {
            $sess = booking_item( $sid );

            if( !empty( $sess ) )
            {
                $sess['sess_data']['trip'][$type][$pdtype]['area_id'] = $taid;
                $sess['sess_data']['trip'][$type][$pdtype]['hotel_id'] = $hotel;
                $sess['sess_data']['trip'][$type][$pdtype]['rpfrom'] = $rpfrom;
                $sess['sess_data']['trip'][$type][$pdtype]['rpto'] = $rpto;
                $sess['sess_data']['trip'][$type][$pdtype]['airport'] = $airport;
                $sess['sess_data']['trip'][$type][$pdtype]['transport_type'] = $ttype;
                $sess['sess_data']['trip'][$type][$pdtype]['transport_fee'] = $ttype == 0 ? 0 : $fee;

                add_booking_item( $sid, $sess['sess_data'] );

                booking_grandtotal( $sid );

                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        $sess = booking_item( $sess_id );

        if( !empty( $sess ) )
        {
            foreach( $trans as $trip_type => $obj )
            {
                foreach( $obj as $trans_type => $d )
                {
                    $hotel = isset( $d['hid'] ) ? $d['hid'] : '';
                    $taid  = isset( $d['taid'] ) ? $d['taid'] : '';

                    $sess['sess_data']['trip'][$trip_type][$trans_type]['area_id'] = $taid;
                    $sess['sess_data']['trip'][$trip_type][$trans_type]['hotel_id'] = $hotel;
                    $sess['sess_data']['trip'][$trip_type][$trans_type]['rpfrom'] = $d['rpfrom'];
                    $sess['sess_data']['trip'][$trip_type][$trans_type]['rpto'] = $d['rpto'];
                    $sess['sess_data']['trip'][$trip_type][$trans_type]['airport'] = $d['taairport'];
                    $sess['sess_data']['trip'][$trip_type][$trans_type]['transport_type'] = $d['type'];
                    $sess['sess_data']['trip'][$trip_type][$trans_type]['flight_time'] = $d['ftime'];
                    $sess['sess_data']['trip'][$trip_type][$trans_type]['transport_fee'] = $d['trans-fee'];
                }
            }

            add_booking_item( $sess_id, $sess['sess_data'] );

            booking_grandtotal( $sess_id );

            return true;
        }
    }
}

function edit_trip_price( $post )
{
	extract( $post );

	if( isset( $sid ) && !empty( $sid ) )
	{
		$sess = booking_item( $sid );

		if( !empty( $sess ) )
		{
            $sess['sess_data']['trip'][$type][$ptype] = $price;
            $sess['sess_data']['trip'][$type]['discount'] = 0;

			$adult_price  = $sess['sess_data']['trip'][$type]['adult_price'] * $sess['sess_data']['adult_num'];
			$child_price  = $sess['sess_data']['trip'][$type]['child_price'] * $sess['sess_data']['child_num'];
			$infant_price = $sess['sess_data']['trip'][$type]['infant_price'] * $sess['sess_data']['infant_num'];

            $sess['sess_data']['trip'][$type]['total'] = $adult_price + $child_price + $infant_price;

            if( isset( $sell_ptype ) )
            {
                $sess['sess_data']['trip'][$type][$sell_ptype] = $sell_price;
            }

			add_booking_item( $sid, $sess['sess_data'] );

			booking_grandtotal( $sid );

			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function apply_coupon_code( $post )
{
	extract( $post );

	if( isset( $sid ) && !empty( $sid ) )
	{
		$sess = booking_item( $sid );

		if( !empty( $sess ) )
		{
			$sess['sess_data']['promo_code']  = $code;
            $sess['sess_data']['promo_apply'] = $apply;

			add_booking_item( $sid, $sess['sess_data'] );

			booking_grandtotal( $sid );

			$nsess = booking_item( $sid );

			if( isset( $nsess['sess_data']['promo_code'] ) && !empty( $nsess['sess_data']['promo_code'] ) || isset( $nsess['sess_data']['freelance_code'] ) && !empty( $nsess['sess_data']['freelance_code'] ) )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function delete_coupon_code( $post )
{
	extract( $post );

	if( isset( $sid ) && !empty( $sid ) )
	{
		$sess = booking_item( $sid );

		if( !empty( $sess ) && isset( $sess['sess_data']['promo_code'] ) )
		{
			$sess['sess_data']['promo_code'] = '';
			$sess['sess_data']['discount']   = 0;

            if( isset( $sess['sess_data']['trip']['departure']['adult_disc_old'] ) )
            {
                $sess['sess_data']['trip']['departure']['adult_disc'] = $sess['sess_data']['trip']['departure']['adult_disc_old'];
            }

            if( isset( $sess['sess_data']['trip']['departure']['child_disc_old'] ) )
            {
                $sess['sess_data']['trip']['departure']['child_disc'] = $sess['sess_data']['trip']['departure']['child_disc_old'];
            }

            if( isset( $sess['sess_data']['trip']['departure']['infant_disc_old'] ) )
            {
                $sess['sess_data']['trip']['departure']['infant_disc'] = $sess['sess_data']['trip']['departure']['infant_disc_old'];
            }

            if( isset( $sess['sess_data']['trip']['departure']['discount_old'] ) )
            {
                $sess['sess_data']['trip']['departure']['discount'] = $sess['sess_data']['trip']['departure']['discount_old'];
            }

            if( isset( $sess['sess_data']['trip']['return']['adult_disc_old'] ) )
            {
                $sess['sess_data']['trip']['return']['adult_disc'] = $sess['sess_data']['trip']['return']['adult_disc_old'];
            }

            if( isset( $sess['sess_data']['trip']['return']['child_disc_old'] ) )
            {
                $sess['sess_data']['trip']['return']['child_disc'] = $sess['sess_data']['trip']['return']['child_disc_old'];
            }

            if( isset( $sess['sess_data']['trip']['return']['infant_disc_old'] ) )
            {
                $sess['sess_data']['trip']['return']['infant_disc'] = $sess['sess_data']['trip']['return']['infant_disc_old'];
            }

            if( isset( $sess['sess_data']['trip']['return']['discount_old'] ) )
            {
                $sess['sess_data']['trip']['return']['discount'] = $sess['sess_data']['trip']['return']['discount_old'];
            }

			add_booking_item( $sid, $sess['sess_data'] );

			booking_grandtotal( $sid );

			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function get_remain_limit( $data )
{
	global $db;

	//-- Check if promo usage limitation
	//-- if yes return 1 limit as default
	if( $data['pmuselimit'] == '1' )
	{
		$s = 'SELECT COUNT(pmcode) as used FROM ticket_booking WHERE pmcode = %s AND bstatus != "cn"';
		$q = $db->prepare_query( $s, $data['pmcode'] );
		$r = $db->do_query( $q );
		$d = $db->fetch_array( $r );

		$remain = $data['pmlimit'] - $d['used'];
		$remain = $remain < 0 ? 0 : $remain;

		return $remain;
	}
	else
	{
		return 1;
	}
}

function get_diff_date( $start, $end )
{
    $sctime = strtotime( $start );
    $sdtime = strtotime( $end );

    $current_time = date( 'Y-m-d H:i:s', $sctime );
    $depart_time  = date( 'Y-m-d H:i:s', $sdtime );

    $ctime = new DateTime( $current_time );
    $dtime = new DateTime( $depart_time );

    $interval = $ctime->diff( $dtime );

    return $interval;
}

function get_suggestion_port( $from, $to )
{
    global $db;

    $data = array();

    $s = 'SELECT a.lcid FROM ticket_location AS a
          WHERE a.lctype = %s AND a.lcid <> %d AND ( SELECT a2.lctype FROM ticket_location AS a2 WHERE a2.lcid = %d ) = 0';
    $q = $db->prepare_query( $s, 0, $from, $from );
    $r = $db->do_query( $q );
    if( is_array( $r ) === false )
    {   
        while( $d = $db->fetch_array( $r ) )
        {
            $data['from'][] = $d['lcid'];
        }
    }
    $s = 'SELECT a.lcid FROM ticket_location AS a
          WHERE a.lctype = %s AND a.lcid <> %d AND ( SELECT a2.lctype FROM ticket_location AS a2 WHERE a2.lcid = %d ) = 0';
    $q = $db->prepare_query( $s, 0, $to, $to );
    $r = $db->do_query( $q );

    if( is_array( $r ) === false )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data['to'][] = $d['lcid'];
        }
    }
    if($from == 12 && $to == 8){
        $data['from'] ="";
        $data['to'] ="";
        $data['from'][] = 12;
        $data['to'][] = 10;
    }
    if($from == 12 && $to == 9){
        $data['from'] ="";
        $data['to'] ="";
        $data['from'][] = 12;
        $data['to'][] = 16;
    }
   
    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Booking Process Functions
| -------------------------------------------------------------------------------------
*/
function ticket_booking_process( $type = '' )
{
    if( !empty( $_POST ) || !empty( $_GET ) )
    {
        extract( $_POST );
        extract( $_GET );

		$bmethod_by = $type == '' ? '0' : ( $type == 'agent' ? '1' : '2' );

        if( isset( $check_availability ) )
        {
            $get_pmcode            = isset( $get_pmcode ) ? str_replace("."," ",$get_pmcode) : '';
            $return_date           = isset( $return_date ) ? $return_date : '';
            $destination_return    = isset( $destination_return ) ? $destination_return : '';
            $destination_return_to = isset( $destination_return_to ) ? $destination_return_to : '';
            $port_type             = get_location( $destination_from, 'lctype' );
            $suggest_depart_port   = get_suggestion_port( $destination_from, $destination_to );
            $suggest_return_port   = get_suggestion_port( $destination_return, $destination_return_to );

            $source = explode( '|', $booking_source );
            $chid   = isset( $source[0] ) ? $source[0] : '';
            $agid   = isset( $source[1] ) ? $source[1] : '';

            $idx  = md5( microtime() . rand() );
            $item = array(
                'adult_num'            => $adult,
                'child_num'            => $child,
                'infant_num'           => $infant,
                'port_type'            => $port_type,
				'bmethod_by'           => $bmethod_by,
                'type_of_route'        => $route_type,
                'date_of_depart'       => $depart_date,
                'date_of_return'       => $return_date,
                'destination_port'     => $destination_to,
                'depart_port'          => $destination_from,
                'return_port'          => $destination_return,
                'destination_port_rtn' => $destination_return_to,
                'suggest_depart_port'  => $suggest_depart_port,
                'suggest_return_port'  => $suggest_return_port,
                'chid'                 => $chid,
                'agid'                 => $agid,
                'promo_code'           => '',
                'freelance_code'       => '',
                'subtotal'             => 0,
                'discount'             => 0,
                'grandtotal'           => 0,
                'get_pmcode'		   => $get_pmcode
            );

            unset_booking_session();

            add_booking_item( $idx, $item );

            if( $type == 'admin' )
            {
                header( 'location:' . get_state_url( 'reservation&sub=availability&step=search-result' ) );
            }
            elseif( $type == 'agent' )
            {
                header( 'location:' . HTSERVER . site_url() . '/agent/admin/?state=reservation&sub=availability&step=search-result' );
            }
            else
            {
                header( 'location:' . HTSERVER . site_url() . '/availability-result' );
            }
        }
        elseif( isset( $review_booking ) )
        {
            $port_type = get_location( $depart_port, 'lctype' );

            if( isset( $use_sugested_trip ) )
            {
                $suggest_depart_port = get_suggestion_port( $depart_port, $destination_port );

                foreach( $suggest_depart_port as $stype => $sport )
                {
                    if( $stype == 'from' )
                    {
                        $depart_port = $sport[0];
                    }
                    elseif( $stype == 'to' )
                    {
                        $destination_port = $sport[0];
                    }
                }

                $suggest_return_port = get_suggestion_port( $return_port, $destination_port_rtn );

                foreach( $suggest_return_port as $stype => $sport )
                {
                    if( $stype == 'from' )
                    {
                        $return_port = $sport[0];
                    }
                    elseif( $stype == 'to' )
                    {
                        $destination_port_rtn = $sport[0];
                    }
                }
            }

            $item = array(
                'adult_num'            => $adult_num,
                'child_num'            => $child_num,
                'port_type'            => $port_type,
                'infant_num'           => $infant_num,
				'bmethod_by'           => $bmethod_by,
                'depart_port'          => $depart_port,
                'return_port'          => $return_port,
                'type_of_route'        => $type_of_route,
                'date_of_depart'       => $date_of_depart,
                'date_of_return'       => $date_of_return,
                'destination_port'     => $destination_port,
                'destination_port_rtn' => $destination_port_rtn,
                'chid'                 => $chid,
                'agid'                 => $agid,
                'promo_code'           => '',
                'freelance_code'       => '',
                'subtotal'             => 0,
                'discount'             => 0,
                'grandtotal'           => 0,
                'get_pmcode'		   => $get_pmcode
            );

            if( $type_of_route == 1 )
            {
                $item['trip'] = array();

                $drid    = '';
                $dsid    = '';
                $dboid   = '';
                $drtdid  = '';
                $dtrans  = '';
                $daprice = 0;
                $dcprice = 0;
                $diprice = 0;
                $dadisc  = 0;
                $dcdisc  = 0;
                $didisc  = 0;
                $dtprice = 0;
                $dtdisc  = 0;

                $rrid    = '';
                $rsid    = '';
                $rboid   = '';
                $rrtdid  = '';
                $rtrans  = '';
                $raprice = 0;
                $rcprice = 0;
                $riprice = 0;
                $radisc  = 0;
                $rcdisc  = 0;
                $ridisc  = 0;
                $rtprice = 0;
                $rtdisc  = 0;

                list( $drid, $dsid, $dboid, $drtdid, $daprice, $dcprice, $diprice, $dasprice, $dcsprice, $disprice, $dadisc, $dcdisc, $didisc, $dtprice, $dtdisc, $dtrans ) = explode( '|', $dep_rid  );
                list( $rrid, $rsid, $rboid, $rrtdid, $raprice, $rcprice, $riprice, $rasprice, $rcsprice, $risprice, $radisc, $rcdisc, $ridisc, $rtprice, $rtdisc, $rtrans ) = explode( '|', $rtn_rid );

                if( !empty( $drid ) )
                {
                    $item['trip']['departure'] = array(
                        'id' => $drid,
                        'sid' => $dsid,
                        'boid' => $dboid,
                        'rtdid' => $drtdid,
                        'adult_price' => $daprice,
                        'child_price' => $dcprice,
                        'infant_price' => $diprice,
                        'adult_disc' => $dadisc,
                        'child_disc' => $dcdisc,
                        'infant_disc' => $didisc,
                        'adult_sell_price' => $dasprice,
                        'child_sell_price' => $dcsprice,
                        'infant_sell_price' => $disprice,
                        'total' => $dtprice,
                        'discount' => $dtdisc,
                        'transport' => $dtrans
                    );
                }

                if( !empty( $rrid ) )
                {
                    $item['trip']['return'] = array(
                        'id' => $rrid,
                        'sid' => $rsid,
                        'boid' => $rboid,
                        'rtdid' => $rrtdid,
                        'adult_price' => $raprice,
                        'child_price' => $rcprice,
                        'infant_price' => $riprice,
                        'adult_disc' => $radisc,
                        'child_disc' => $rcdisc,
                        'infant_disc' => $ridisc,
                        'adult_sell_price' => $rasprice,
                        'child_sell_price' => $rcsprice,
                        'infant_sell_price' => $risprice,
                        'total' => $rtprice,
                        'discount' => $rtdisc,
                        'transport' => $rtrans
                    );
                }

                $item['subtotal']   = ( $dtprice - $dtdisc ) + ( $rtprice - $rtdisc );
                $item['discount']   = 0;
                $item['grandtotal'] = ( $dtprice - $dtdisc ) + ( $rtprice - $rtdisc );
            }
            else
            {
                $item['trip'] = array();

                $drid    = '';
                $dsid    = '';
                $dboid   = '';
                $drtdid  = '';
                $dtrans  = '';
                $daprice = 0;
                $dcprice = 0;
                $diprice = 0;
                $dadisc  = 0;
                $dcdisc  = 0;
                $didisc  = 0;
                $dtprice = 0;
                $dtdisc  = 0;

                if( !empty( $dep_rid ) )
                {
                    list( $drid, $dsid, $dboid, $drtdid, $daprice, $dcprice, $diprice, $dasprice, $dcsprice, $disprice, $dadisc, $dcdisc, $didisc, $dtprice, $dtdisc, $dtrans ) = explode( '|', $dep_rid  );

                    $item['trip']['departure'] = array(
                        'id' => $drid,
                        'sid' => $dsid,
                        'boid' => $dboid,
                        'rtdid' => $drtdid,
                        'adult_price' => $daprice,
                        'child_price' => $dcprice,
                        'infant_price' => $diprice,
                        'adult_disc' => $dadisc,
                        'child_disc' => $dcdisc,
                        'infant_disc' => $didisc,
                        'adult_sell_price' => $dasprice,
                        'child_sell_price' => $dcsprice,
                        'infant_sell_price' => $disprice,
                        'total' => $dtprice,
                        'discount' => $dtdisc,
                        'transport' => $dtrans
                    );
                }

                $item['subtotal']   = $dtprice - $dtdisc;
                $item['discount']   = 0;
                $item['grandtotal'] = $dtprice - $dtdisc;
            }

            add_booking_item( $sess_id, $item );

            if( $type == 'admin' )
            {
                header( 'location:' . get_state_url( 'reservation&sub=availability&step=review' ) );
            }
            elseif( $type == 'agent' )
            {
                header( 'location:' . HTSERVER . site_url() . '/agent/admin/?state=reservation&sub=availability&step=review' );
            }
            else
            {
                header( 'location:' . HTSERVER . site_url() . '/review-booking' );
            }
        }
        elseif( isset( $detail_booking ) )
        {
            add_transport_detail( $_POST );
            add_add_ons_detail( $_POST );

            $booking = booking_item( $sess_id );
            $message = check_valid_transport_time( $_POST, $booking['sess_data'] );

            if( empty( $message ) )
            {
                if( $type == 'admin' )
                {
                    header( 'location:' . get_state_url( 'reservation&sub=availability&step=detail' ) );
                }
                elseif( $type == 'agent' )
                {
                    header( 'location:' . HTSERVER . site_url() . '/agent/admin/?state=reservation&sub=availability&step=detail' );
                }
                else
                {
                    header( 'location:' . HTSERVER . site_url() . '/detail-booking' );
                }
            }
            else
            {
                if( $type == 'admin' )
                {
                    header( 'location:' . get_state_url( 'reservation&sub=availability&step=review&error=' . base64_encode( json_encode( $message ) ) ) );
                }
                elseif( $type == 'agent' )
                {
                    header( 'location:' . HTSERVER . site_url() . '/agent/admin/?state=reservation&sub=availability&step=review&error=' . base64_encode( json_encode( $message ) ) );
                }
                else
                {
                    header( 'location:' . HTSERVER . site_url() . '/review-booking/?error=' . base64_encode( json_encode( $message ) ) );
                }
            }
        }
        elseif( isset( $add_booking ) )
        {
            $booking = booking_item( $sess_id );

            if( empty( $booking ) || ( isset( $booking['sess_data']['trip'] ) && empty( $booking['sess_data']['trip'] ) ) || empty( $_POST ) )
            {
                if( $type == 'admin' )
                {
                    header( 'location:' . get_state_url( 'reservation&sub=availability&step=detail' ) );
                }
                elseif( $type == 'agent' )
                {
                    header( 'location:' . HTSERVER . site_url() . '/agent/admin/?state=reservation&sub=availability&step=detail' );
                }
                else
                {
                    header( 'location:' . HTSERVER . site_url() . '/booking-process' );
                }
            }
            else
            {
                if( empty( $type ) )
                {
                    $bid = save_transaction( $_POST, $booking['sess_data'] );

                    if( !empty( $bid ) )
                    {
                        unset_booking_session();

                        if( $_POST['payment'] == 2 )
                        {
                            header( 'location:' . HTSERVER . site_url() . '/booking/payment/?type=nicepay&id=' . $bid );
                        }
                        elseif( $_POST['payment'] == 3 )
                        {
                            header( 'location:' . HTSERVER . site_url() . '/booking/payment/?type=paypal&id=' . $bid );
                        }
                        elseif( $_POST['payment'] == 5 )
                        {
                            $cctype = get_meta_data( 'cc_payment_gateway_opt');

                            header( 'location:' . HTSERVER . site_url() . '/booking/payment/?type=' . $cctype . '&id=' . $bid );
                        }
                    }
                }
                else
                {
                    $bid = save_transaction( $_POST, $booking['sess_data'] );

                    if( !empty( $bid ) )
                    {
                        $data = ticket_booking_all_data( $bid );

                        save_log( $bid, 'reservation', 'Create new reservation #' . $data['bticket'] );

                        unset_booking_session();

                        if( $type == 'admin' )
                        {
                            header( 'location:' . get_state_url( 'reservation&sub=booking' ) );
                        }
                        elseif( $type == 'agent' )
                        {
                            send_booking_notification_to_supplier( $bid );

                            if( $_POST['payment'] == 4 )
                            {
                                send_booking_receipt_to_client( $data );
                            }
                            elseif( $_POST['payment'] == 8 )
                            {
                                send_booking_ticket_to_client( $data );
                            }

                            header( 'location:' . HTSERVER . site_url() . '/agent/admin/?state=reservation&sub=booking' );
                        }
                    }
                }
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_availability_ajax()
{
	global $db;

    add_actions( 'is_use_ajax', true );

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-transport' )
    {
        if( add_transport_detail( $_POST, true ) )
        {
        	$booking = booking_item( $_POST['sid'] );

        	extract( $booking['sess_data'] );

            echo json_encode(
            	array(
	            	'result' => 'success',
            		'promo_code' =>  $promo_code,
            		'subtotal_num' => number_format( $subtotal, 0, ',', '.' ),
            		'discount_num' => number_format( $discount, 0, ',', '.' ),
	            	'grandtotal_num' => number_format( $grandtotal, 0, ',', '.' )
	            )
            );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-add-ons' )
    {
        if( add_add_ons_detail( $_POST, true ) )
        {
            $booking = booking_item( $_POST['sid'] );

            extract( $booking['sess_data'] );

            if( isset( $trip['departure']['addons'] ) && !empty( $trip['departure']['addons'] ) )
            {
                $dep_addons = array_reduce( $trip['departure']['addons'], function( $total, $prm ) {
                    return $total + ( $prm['price'] * $prm['paxs'] );
                });
            }
            else
            {
                $dep_addons = 0;
            }

            if( isset( $trip['return']['addons'] ) && !empty( $trip['return']['addons'] ) )
            {
                $rtn_addons = array_reduce( $trip['return']['addons'], function( $total, $prm ) {
                    return $total + ( $prm['price'] * $prm['paxs'] );
                });
            }
            else
            {
                $rtn_addons = 0;
            }

            echo json_encode(
                array(
                    'result' => 'success',
                    'dep_addons' => number_format( $dep_addons, 0, ',', '.' ),
                    'rtn_addons' => number_format( $rtn_addons, 0, ',', '.' ),
                    'subtotal_num' => number_format( $subtotal, 0, ',', '.' ),
                    'discount_num' => number_format( $discount, 0, ',', '.' ),
                    'grandtotal_num' => number_format( $grandtotal, 0, ',', '.' )
                )
            );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'edit-trip-price' )
    {
    	if( edit_trip_price( $_POST ) )
        {
        	$booking = booking_item( $_POST['sid'] );

        	extract( $booking['sess_data'] );
        	extract( $_POST );

            echo json_encode(
            	array(
            		'result' => 'success',
            		'promo_code' =>  $promo_code,
            		'price' => $trip[$type][$ptype],
                    'sell_price' => $trip[$type][$sell_ptype],
            		'subtotal_num' => number_format( $subtotal, 0, ',', '.' ),
            		'discount_num' => number_format( $discount, 0, ',', '.' ),
            		'price_num' => number_format( $trip[$type][$ptype], 0, ',', '.' ),
                    'sell_price_num' => number_format( $trip[$type][$sell_ptype], 0, ',', '.' ),
	            	'pricetotal_num' => number_format( $trip[$type]['total'], 0, ',', '.' ),
            		'grandtotal_num' => number_format( $grandtotal, 0, ',', '.' )
            	)
            );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'apply-coupon-code' )
    {
    	if( apply_coupon_code( $_POST ) )
        {
        	$booking = booking_item( $_POST['sid'] );

        	extract( $booking['sess_data'] );

            if( empty( $freelance_code ) )
            {
                echo json_encode(
                    array(
                        'result' => 'success',
                        'promo_code' =>  $promo_code,
                        'subtotal_num' => number_format( $subtotal, 0, ',', '.' ),
                        'discount_num' => number_format( $discount, 0, ',', '.' ),
                        'grandtotal_num' => number_format( $grandtotal, 0, ',', '.' )
                    )
                );
            }
            else
            {
                echo json_encode(
                    array(
                        'result' => 'success',
                        'freelance_code' =>  $freelance_code,
                        'subtotal_num' => number_format( $subtotal, 0, ',', '.' ),
                        'discount_num' => number_format( $discount, 0, ',', '.' ),
                        'grandtotal_num' => number_format( $grandtotal, 0, ',', '.' )
                    )
                );
            }
        }
        else
        {
        	$booking = booking_item( $_POST['sid'] );

        	extract( $booking['sess_data'] );

            echo json_encode(
            	array(
            		'result' => 'failed',
            		'promo_code' =>  $promo_code,
            		'subtotal_num' => number_format( $subtotal, 0, ',', '.' ),
            		'discount_num' => number_format( $discount, 0, ',', '.' ),
            		'grandtotal_num' => number_format( $grandtotal, 0, ',', '.' )
            	)
            );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-coupon-code' )
    {
        if( delete_coupon_code( $_POST ) )
        {
            $booking = booking_item( $_POST['sid'] );

            extract( $booking['sess_data'] );

            echo json_encode(
                array(
                    'result' => 'success',
                    'promo_code' =>  $promo_code,
                    'subtotal_num' => number_format( $subtotal, 0, ',', '.' ),
                    'discount_num' => number_format( $discount, 0, ',', '.' ),
                    'grandtotal_num' => number_format( $grandtotal, 0, ',', '.' )
                )
            );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-freelance-code' )
    {
        if( delete_coupon_code( $_POST ) )
        {
            $booking = booking_item( $_POST['sid'] );

            extract( $booking['sess_data'] );

            echo json_encode(
                array(
                    'result' => 'success',
                    'freelance_code' =>  $freelance_code,
                    'subtotal_num' => number_format( $subtotal, 0, ',', '.' ),
                    'discount_num' => number_format( $discount, 0, ',', '.' ),
                    'grandtotal_num' => number_format( $grandtotal, 0, ',', '.' )
                )
            );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-seat-avaibility-calendar' )
    {
        $data = get_seat_availibility_calendar( $_POST['month'], $_POST['year'], $_POST['sid'] );

        if( !empty( $data ) )
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'set-seat-avaibility-option' )
    {
        $data = set_seat_availibility_option( $_POST['period'] );

        if( !empty( $data ) )
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

?>
