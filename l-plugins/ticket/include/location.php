<?php

add_actions( 'location', 'ticket_location' );
add_actions( 'ticket-location-ajax_page', 'ticket_location_ajax' );

function ticket_location()
{
    if( is_num_location() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'schedules&sub=location&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_location();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_location( $_GET['id'] ) )
        {
            return ticket_edit_location();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_location();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_location( $val );
        }
    }

    return ticket_location_table();
}

/*
| -------------------------------------------------------------------------------------
| Location Table List
| -------------------------------------------------------------------------------------
*/
function ticket_location_table()
{
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/location/list.html', 'location' );

    add_block( 'list-block', 'lcblock', 'location' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'schedules&sub=location' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-location-ajax/' );
    add_variable( 'edit_link', get_state_url( 'schedules&sub=location&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=location&prc=add_new' ) );

    parse_template( 'list-block', 'lcblock', false );

    add_actions( 'section_title', 'Location List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'location' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Location
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_location()
{
	run_save_location();

    $site_url = site_url();
    $data     = get_location();

	set_template( PLUGINS_PATH . '/ticket/tpl/location/form.html', 'location' );
    add_block( 'form-block', 'lcblock', 'location' );

    add_variable( 'lcid', $data['lcid'] );
    add_variable( 'lcname', $data['lcname'] );
    add_variable( 'lcalias', $data['lcalias'] );
    add_variable( 'lcorder_to', $data['lcorder_to'] );
    add_variable( 'lcorder_from', $data['lcorder_from'] );
    add_variable( 'lctype', get_location_type_option( $data['lctype'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=location' ) );
    add_variable( 'action', get_state_url( 'schedules&sub=location&prc=add_new' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'New Location' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'location' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Location
| -------------------------------------------------------------------------------------
*/
function ticket_edit_location()
{
    run_update_location();

    $site_url = site_url();
    $data     = get_location( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/location/form.html', 'location' );
    add_block( 'form-block', 'lcblock', 'location' );

    add_variable( 'lcid', $data['lcid'] );
    add_variable( 'lcname', $data['lcname'] );
    add_variable( 'lcalias', $data['lcalias'] );
    add_variable( 'lcorder_to', $data['lcorder_to'] );
    add_variable( 'lcorder_from', $data['lcorder_from'] );
    add_variable( 'lctype', get_location_type_option( $data['lctype'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=location' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-location-ajax/' );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'location' ) );
    add_variable( 'action', get_state_url( 'schedules&sub=location&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'Edit Location' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'location' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Location
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_location()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/location/batch-delete.html', 'location' );
    add_block( 'loop-block', 'lclblock', 'location' );
    add_block( 'delete-block', 'lcblock', 'location' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_location( $val );

        add_variable('lcname',  $d['lcname'] );
        add_variable('lcid', $d['lcid'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' location? :' );
    add_variable('action', get_state_url('schedules&sub=location'));

    parse_template( 'delete-block', 'lcblock', false );

    add_actions( 'section_title', 'Delete Location' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'location' );
}

function run_save_location()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_location_data();

        if( empty($error) )
        {
            $post_id = save_location();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new location' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New location successfully saved' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=location&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_location()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_location_data();

        if( empty($error) )
        {
            $post_id = update_location();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this sales channel' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This location successfully edited' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=location&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_location_data()
{
    $error = array();

    if( isset($_POST['lcname']) && empty( $_POST['lcname'] ) )
    {
        $error[] = 'Location name can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Location List Count
| -------------------------------------------------------------------------------------
*/
function is_num_location()
{
    global $db;

    $s = 'SELECT * FROM ticket_location';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Location Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_location_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1 => 'a.lcid'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.lcid DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT * FROM ticket_location AS a ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT * FROM ticket_location AS a WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'lcid'      => $d2['lcid'],
                'lcname'    => $d2['lcname'],
                'edit_link' => get_state_url( 'schedules&sub=location&prc=edit&id=' . $d2['lcid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Location By ID
| -------------------------------------------------------------------------------------
*/
function get_location( $id = '', $field = '' , $type = '' )
{
    global $db;

    $data = array(
        'lcid'          => ( isset( $_POST['lcid'] ) ? $_POST['lcid'] : null ),
        'lcname'        => ( isset( $_POST['lcname'] ) ? $_POST['lcname'] : '' ),
        'lcalias'       => ( isset( $_POST['lcalias'] ) ? $_POST['lcalias'] : '' ),
        'lctype'        => ( isset( $_POST['lctype'] ) ? $_POST['lctype'] : '' ),
        'lcstatus'      => ( isset( $_POST['lcstatus'] ) ? $_POST['lcstatus'] : '' ),
        'lccreateddate' => ( isset( $_POST['lccreateddate'] ) ? $_POST['lccreateddate'] : '' ),
        'luser_id'      => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' ),
        'lcorder_from'  => ( isset( $_POST['lcorder_from'] ) ? $_POST['lcorder_from'] : '' ),
        'lcorder_to'    => ( isset( $_POST['lcorder_to'] ) ? $_POST['lcorder_to'] : '' )
    );

    $s = 'SELECT * FROM ticket_location WHERE lcid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'lcid' => $d['lcid'],
                'lcname' => $d['lcname'],
                'lcalias' => $d['lcalias'],
                'lctype' => $d['lctype'],
                'lcstatus' => $d['lcstatus'],
                'lccreateddate' => $d['lccreateddate'],
                'luser_id' => $d['luser_id'],
                'lcorder_from' => $d['lcorder_from'],
                'lcorder_to' => $d['lcorder_to']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Location By Name
| -------------------------------------------------------------------------------------
*/
function get_location_by_name( $name = '', $field = '' )
{
    global $db;

    $data = array(
        'lcid'          => ( isset( $_POST['lcid'] ) ? $_POST['lcid'] : null ),
        'lcname'        => ( isset( $_POST['lcname'] ) ? $_POST['lcname'] : '' ),
        'lcalias'       => ( isset( $_POST['lcalias'] ) ? $_POST['lcalias'] : '' ),
        'lctype'        => ( isset( $_POST['lctype'] ) ? $_POST['lctype'] : '' ),
        'lcstatus'      => ( isset( $_POST['lcstatus'] ) ? $_POST['lcstatus'] : '' ),
        'lccreateddate' => ( isset( $_POST['lccreateddate'] ) ? $_POST['lccreateddate'] : '' ),
        'luser_id'      => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' )
    );

    $s = 'SELECT * FROM ticket_location WHERE lcname = %s';
    $q = $db->prepare_query( $s, $name );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'lcid' => $d['lcid'],
                'lcname' => $d['lcname'],
                'lcalias' => $d['lcalias'],
                'lctype' => $d['lctype'],
                'lcstatus' => $d['lcstatus'],
                'lccreateddate' => $d['lccreateddate'],
                'luser_id' => $d['luser_id']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Location List
| -------------------------------------------------------------------------------------
*/
function get_location_list( $where = array() , $type = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_location';

    if( empty( $where ) === false )
    {
        $w = array();

        foreach( $where as $field => $val )
        {
            $w[] = $db->prepare_query( $field . ' = %s', $val );
        }

        $s .= ' WHERE ' . implode( 'AND', $w );
    }
    
    if( empty( $type ) === false )
    {
        if( strpos( $type, 'from' ) !== false )
        {
            $s .= ' ORDER BY lcorder_from';
        }
        else
        {
            $s .= ' ORDER BY lcorder_to';
        }
    }
    
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'lcid' => $d['lcid'],
            'lcname' => $d['lcname'],
            'lcalias' => $d['lcalias'],
            'lctype' => $d['lctype'],
            'lcstatus' => $d['lcstatus'],
            'lccreateddate' => $d['lccreateddate'],
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| GET ALL POSSIBLE ROUTE
| -------------------------------------------------------------------------------------
*/
function get_all_location_probability()
{
    global $db;

    $s = 'SELECT * FROM ticket_location WHERE lctype = %s OR lctype = %s';
    $q = $db->prepare_query( $s, '0', '2' );
    $r = $db->do_query( $q );

    $data = array();
    $num  = 0;

    while( $d = $db->fetch_array( $r ) )
    {
        $s2 = 'SELECT * FROM ticket_location WHERE lctype = %s OR lctype = %s';
        $q2 = $db->prepare_query( $s2, '1', '2' );
        $r2 = $db->do_query( $q2 );

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            if( $d['lcalias'] == $d2['lcalias'] )
            {
                continue;
            }

            $data[0][] = $d['lcalias'] . '-' . $d2['lcalias'];
            $data[1][] = $d2['lcalias'] . '-' . $d['lcalias'];

            $num++;
        }
    }

    return array( 'trip' => $data, 'num' => ( $num * 2 ) );
}

/*
| -------------------------------------------------------------------------------------
| Save Location
| -------------------------------------------------------------------------------------
*/
function save_location()
{
    global $db;

    $status = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );

    $s = 'INSERT INTO ticket_location(
            lcname,
            lcalias,
            lctype,
            lcstatus,
            lccreateddate,
            lcorder_from,
            lcorder_to,
            luser_id)
          VALUES( %s, %s, %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s, $_POST['lcname'], $_POST['lcalias'], $_POST['lctype'], $status, date( 'Y-m-d H:i:s' ), $_POST['lcorder_from'], $_POST['lcorder_to'], $_COOKIE['user_id'] );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $id = $db->insert_id();

        save_log( $id, 'location', 'Add new port location - ' . $_POST['lcname'] );

        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Location
| -------------------------------------------------------------------------------------
*/
function update_location()
{
    global $db;

    $current = get_location( $_POST['lcid'] );
    $status  = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );

    $s = 'UPDATE ticket_location SET
            lcname = %s,
            lcalias = %s,
            lctype = %s,
            lcstatus = %s,
            lcorder_from = %s,
            lcorder_to = %s
          WHERE lcid = %d';
    $q = $db->prepare_query( $s, $_POST['lcname'], $_POST['lcalias'], $_POST['lctype'], $status, $_POST['lcorder_from'], $_POST['lcorder_to'], $_POST['lcid'] );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        if( $current['lcname'] != $_POST['lcname'] )
        {
            save_log( $_POST['lcid'], 'location', 'Update port location - ' . $current['lcname'] . ' to ' . $_POST['lcname'] );
        }
        else
        {
            save_log( $_POST['lcid'], 'location', 'Update port location - ' . $_POST['lcname'] );
        }

        return $_POST['lcid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Location
| -------------------------------------------------------------------------------------
*/
function delete_location( $id = '', $is_ajax = false )
{
    global $db;

    $d = get_location( $id );

    $s = 'DELETE FROM ticket_location WHERE lcid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'schedules&sub=location&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'location', 'Delete port location - ' . $d['lcname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Location Option
| -------------------------------------------------------------------------------------
*/
function get_location_option( $lcid = '', $use_empty = true, $empty_text = 'Select Location', $type = '' )
{
    $loc    = get_location_list(null,$type);
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $loc ) )
    {
        foreach( $loc as $d )
        {
            $option .= '<option value="' . $d['lcid'] . '" ' . ( $d['lcid'] == $lcid ? 'selected' : '' ) . ' >' . $d['lcname'] . '</option>';
        }
    }

    return $option;
}

function get_location_where_option( $where = array(), $lcid = '', $use_empty = true, $empty_text = 'Select Location' )
{
    $loc    = get_location_list( $where );
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $loc ) )
    {
        foreach( $loc as $d )
        {
            $option .= '<option value="' . $d['lcid'] . '" ' . ( $d['lcid'] == $lcid ? 'selected' : '' ) . ' >' . $d['lcname'] . '</option>';
        }
    }

    return $option;
}

function get_location_name_option( $lcname = '', $use_empty = true, $empty_text = 'Select Location' )
{
    $loc    = get_location_list();
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $loc ) )
    {
        foreach( $loc as $d )
        {
            $option .= '<option value="' . $d['lcname'] . '" ' . ( $d['lcname'] == $lcname ? 'selected' : '' ) . ' >' . $d['lcname'] . '</option>';
        }
    }

    return $option;
}

function get_location_type_option( $lctype = '' )
{
    return '
    <option value="0" ' . ( $lctype == '0' ? 'selected' : '' ) . ' >Port in Bali</option>
    <option value="1" ' . ( $lctype == '1' ? 'selected' : '' ) . ' >Port other than Bali</option>
    <option value="2" ' . ( $lctype == '2' ? 'selected' : '' ) . ' >Port as hopping point</option>
    <option value="3" ' . ( $lctype == '3' ? 'selected' : '' ) . ' >Port as clearance point</option>';
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_location_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_location_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-location' )
    {
        $d = delete_location( $_POST['lcid'], true );

        if( $d===true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}

?>