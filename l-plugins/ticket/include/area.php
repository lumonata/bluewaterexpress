<?php

add_actions( 'area', 'ticket_transport_area' );
add_actions( 'ticket-transport-area-ajax_page', 'ticket_transport_area_ajax' );

function ticket_transport_area()
{
    if( is_num_transport_area() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'transport&sub=area&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_transport_area();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_transport_area( $_GET['id'] ) )
        {
            return ticket_edit_transport_area();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_transport_area();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_transport_area( $val );
        }
    }

    return ticket_transport_area_table();
}

/*
| -------------------------------------------------------------------------------------
| Transport Area Table List
| -------------------------------------------------------------------------------------
*/
function ticket_transport_area_table()
{
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/area/list.html', 'transport-area' );

    add_block( 'list-block', 'lcblock', 'transport-area' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );
    
    add_variable( 'action', get_state_url( 'transport&sub=area' ) );
    add_variable( 'edit_link', get_state_url( 'transport&sub=area&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'transport&sub=area&prc=add_new' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-transport-area-ajax/' );
    
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Transport Area List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'transport-area' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Transport Area
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_transport_area()
{
	run_save_transport_area();

    $site_url = site_url();
    $data     = get_transport_area();

	set_template( PLUGINS_PATH . '/ticket/tpl/area/form.html', 'transport-area' );
    add_block( 'form-block', 'lcblock', 'transport-area' );

    add_variable( 'taid', $data['taid'] );
    add_variable( 'taname', $data['taname'] );
    add_variable( 'is_taairport_no_checked', $data['taairport']=='0' ? 'checked' : '' );
    add_variable( 'is_taairport_yes_checked', $data['taairport']=='1' ? 'checked' : '' );
    add_variable( 'tatypeforagent_option', get_tatypetransport_option( $data['tatypeforagent'] ) );
    add_variable( 'tatypeforonline_option', get_tatypetransport_option( $data['tatypeforonline'] ) );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'message', generate_message_block() );
    add_variable( 'departure_and_fee', get_departure_and_fee_list( $data['taid'] ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-transport-area-ajax/' );
    add_variable( 'action', get_state_url( 'transport&sub=area&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=area' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'New Transport Area' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'transport-area' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Transport Area
| -------------------------------------------------------------------------------------
*/
function ticket_edit_transport_area()
{
    run_update_transport_area();

    $site_url = site_url();
    $data     = get_transport_area( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/area/form.html', 'transport-area' );
    add_block( 'form-block', 'lcblock', 'transport-area' );

    add_variable( 'taid', $data['taid'] );
    add_variable( 'taname', $data['taname'] );
    add_variable( 'is_taairport_no_checked', $data['taairport']=='0' ? 'checked' : '' );
    add_variable( 'is_taairport_yes_checked', $data['taairport']=='1' ? 'checked' : '' );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'message', generate_message_block() );
    add_variable( 'departure_and_fee', get_departure_and_fee_list( $data['taid'] ) );
    add_variable( 'action', get_state_url( 'transport&sub=area&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-transport-area-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=area' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'transport-area' ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Edit Transport Area' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'transport-area' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Transport Area
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_transport_area()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/area/batch-delete.html', 'transport-area' );
    add_block( 'loop-block', 'lclblock', 'transport-area' );
    add_block( 'delete-block', 'lcblock', 'transport-area' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_transport_area( $val );

        add_variable('taname',  $d['taname'] );
        add_variable('taid', $d['taid'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' area? :' );
    add_variable('action', get_state_url('transport&sub=area'));

    parse_template( 'delete-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Delete Transport Area' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'transport-area' );
}

function run_save_transport_area()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_transport_area_data();

        if( empty( $error ) )
        {
            $post_id = save_transport_area();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new transport area' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New transport area successfully saved' ) ) );

                header( 'location:' . get_state_url( 'transport&sub=area&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_transport_area()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_transport_area_data();

        if( empty( $error ) )
        {
            $post_id = update_transport_area();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this transport area' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This transport area successfully edited' ) ) );

                header( 'location:' . get_state_url( 'transport&sub=area&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_transport_area_data()
{
    $error = array();

    if( isset( $_POST['taname'] ) && empty( $_POST['taname'] ) )
    {
        $error[] = 'Area name can\'t be empty';
    }

    if( isset( $_POST['lcid'] ) )
    {
        $arrid   = array();
        $errorid = 0;

        foreach( $_POST['lcid'] as $lcid )
        {
            $arrid[] = $lcid;

            if( empty( $lcid ) )
            {
                $errorid++;
            }
        }

        if( $errorid > 0 )
        {
            $error[] = 'Departure location can\'t be empty';
        }

        if( count( $arrid ) !== count( array_unique($arrid) ) )
        {
            $error[] = 'Some of your daparture have same value';
        }
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Area List Count
| -------------------------------------------------------------------------------------
*/
function is_num_transport_area()
{
    global $db;

    $s = 'SELECT * FROM ticket_transport_area';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Transport Area Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_transport_area_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array( 
        1  => 'a.taname'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.taid DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT * FROM ticket_transport_area AS a ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT * FROM ticket_transport_area AS a WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'taid'      => $d2['taid'],
                'taname'    => $d2['taname'],
                'edit_link' => get_state_url( 'transport&sub=area&prc=edit&id=' . $d2['taid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Area By ID
| -------------------------------------------------------------------------------------
*/
function get_transport_area( $id='', $field = '' )
{
    global $db;

    $data = array( 
        'taid'            => ( isset( $_POST['taid'] ) ? $_POST['taid'] : null ), 
        'taname'          => ( isset( $_POST['taname'] ) ? $_POST['taname'] : '' ), 
        'taairport'       => ( isset( $_POST['taairport'] ) ? $_POST['taairport'] : '0' ),
        'tatypeforagent'  => ( isset( $_POST['tatypeforagent'] ) ? $_POST['tatypeforagent'] : '' ),
        'tatypeforonline' => ( isset( $_POST['tatypeforonline'] ) ? $_POST['tatypeforonline'] : '' ),
        'tacreateddate'   => ( isset( $_POST['tacreateddate'] ) ? $_POST['tacreateddate'] : '' ), 
        'luser_id'        => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' )
    );

    $s = 'SELECT * FROM ticket_transport_area WHERE taid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'taid' => $d['taid'], 
                'taname' => $d['taname'],
                'taairport' => $d['taairport'],
                'tatypeforagent' => $d['tatypeforagent'],
                'tatypeforonline' => $d['tatypeforonline'],
                'tacreateddate' => $d['tacreateddate'], 
                'luser_id' => $d['luser_id']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Area List
| -------------------------------------------------------------------------------------
*/
function get_transport_area_list( $loc_group = false )
{
    global $db;

    $s = 'SELECT * FROM ticket_transport_area';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();
    $temp = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $depart = get_departure_and_fee( $d['taid'] );

        if( $loc_group )
        {
            foreach( $depart as $key=>$dp )
            {
                $temp[$dp['lcid']][] = array(
                    'taid' => $d['taid'],
                    'taname' => $d['taname'],
                    'taairport' => $d['taairport'],
                    'lcid' => $dp['lcid'],
                    'tafee' => $dp['tafee'],
                    'tatypeforagent' => $dp['tatypeforagent'],
                    'tatypeforonline' => $dp['tatypeforonline'],
                    'tacreateddate' => $d['tacreateddate'],
                    'luser_id' => $d['luser_id']
                );
            }
        }
        else
        {
            foreach( $depart as $dp )
            {
                $data[] = array(
                    'taid' => $d['taid'],
                    'taname' => $d['taname'],
                    'taairport' => $d['taairport'],
                    'lcid' => $dp['lcid'],
                    'tafee' => $dp['tafee'],
                    'tatypeforagent' => $dp['tatypeforagent'],
                    'tatypeforonline' => $dp['tatypeforonline'],
                    'tacreateddate' => $d['tacreateddate'],
                    'luser_id' => $d['luser_id']
                );
            }
        }
    }

    if( !empty( $temp ) && $loc_group  )
    {
        $i = 0;

        foreach( $temp as $idx => $obj )
        {
            foreach( $obj as $d )
            {
                $data[$idx][$i][] = $d;

                $i = isset( $data[$idx][$i] ) && count( $data[$idx][$i] ) == 2 ? $i+1 : $i;
            }
        }
    }
    
    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Transport Area
| -------------------------------------------------------------------------------------
*/
function save_transport_area()
{
    global $db;
    
    $s = 'INSERT INTO ticket_transport_area( taname, tacreateddate, taairport,  luser_id ) VALUES( %s, %s, %s, %d )';
    $q = $db->prepare_query( $s, $_POST['taname'], date( 'Y-m-d H:i:s' ), $_POST['taairport'], $_COOKIE['user_id'] );
       
    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();
        
        save_transport_area_fee( $id );

        save_log( $id, 'transport-area', 'Add new transport area - ' . $_POST['taname'] );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Transport Area Departure & Fee
| -------------------------------------------------------------------------------------
*/
function save_transport_area_fee( $taid = '' )
{
    global $db;

    foreach( $_POST['lcid'] as $idx => $lcid )
    {
        $s = 'INSERT INTO ticket_transport_area_fee( taid, lcid, tafee, tatypeforagent, tatypeforonline ) VALUES( %d, %d, %s, %s, %s )';
        $q = $db->prepare_query( $s, $taid, $lcid, $_POST['tafee'][$idx], $_POST['tatypeforagent'][$idx], $_POST['tatypeforonline'][$idx] );
        $r = $db->do_query( $q );
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Transport Area
| -------------------------------------------------------------------------------------
*/
function update_transport_area()
{
    global $db;

    $d = get_transport_area( $_POST['taid'] );

    $s = 'UPDATE ticket_transport_area SET taname = %s, taairport = %s WHERE taid = %d';     
    $q = $db->prepare_query( $s, $_POST['taname'], $_POST['taairport'], $_POST['taid'] );
       
    if( $db->do_query( $q ) )
    {
        update_transport_area_fee( $_POST['taid'] );

        $logs = array();
        $aport_log = $d['taairport'] == '0' ? 'No' : 'Yes';
        $aport     = $_POST['taairport'] == '0' ? 'No' : 'Yes';
        $logs[] = $d['taname'] != $_POST['taname'] ? 'Change Area Name : ' . $d['taname'] . ' to ' . $_POST['taname'] : '';
        $logs[] = $d['taairport'] != $_POST['taairport'] ? 'Change Is Airport Area : ' . $aport_log . ' to ' . $aport : '';

        if ( !empty( $logs ) ) 
        {
            foreach ( $logs as $i => $log ) 
            {
                if ( $log == '' ) 
                {
                    unset( $logs[ $i ] );
                }
            }

            save_log( $_POST['taid'], 'transport-area', 'Update transport area<br/>' . implode( '<br/>',  $logs ) );
        }

        return $_POST['taid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Transport Area Departure & Fee
| -------------------------------------------------------------------------------------
*/
function update_transport_area_fee( $taid = '' )
{
    global $db;

    $s = 'DELETE FROM ticket_transport_area_fee WHERE taid = %d';          
    $q = $db->prepare_query( $s, $taid );

    if( $db->do_query( $q ) )
    {
        save_transport_area_fee( $taid );
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Transport Area
| -------------------------------------------------------------------------------------
*/
function delete_transport_area( $taid = '' )
{
    global $db;

    $d = get_transport_area( $taid );

    $s = 'DELETE FROM ticket_transport_area WHERE taid = %d';          
    $q = $db->prepare_query( $s, $taid );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'transport&sub=area&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $taid, 'transport-area', 'Delete transport area - ' . $d['taname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Transport Fee
| -------------------------------------------------------------------------------------
*/
function delete_transport_fee( $tdid = '' )
{
    global $db;

    $s = 'DELETE FROM ticket_transport_area_fee WHERE tdid = %d';          
    $q = $db->prepare_query( $s, $tdid );
       
    if( $db->do_query( $q ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Area Departure and Fee
| -------------------------------------------------------------------------------------
*/
function get_departure_and_fee( $taid = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_transport_area AS a 
          LEFT JOIN ticket_transport_area_fee AS b ON b.taid = a.taid
          WHERE a.taid = %d';
    $q = $db->prepare_query( $s, $taid );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'tdid'            => $d['tdid'],
            'lcid'            => $d['lcid'],
            'tafee'           => $d['tafee'],
            'tatypeforonline' => $d['tatypeforonline'],
            'tatypeforagent'  => $d['tatypeforagent']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Area Departure and Fee List
| -------------------------------------------------------------------------------------
*/
function get_departure_and_fee_list(  $taid = '' )
{
    $surl    = site_url();
    $content = '';

    if( isset( $_POST ) && !empty( $_POST ) )
    {
        foreach( $_POST['lcid'] as $idx => $lcid )
        {
            $content .= '
            <div class="item">
                <div class="form-group">
                    <label>Departure</label>
                    <select name="lcid[]" autocomplete="off">
                        ' . get_location_option( $lcid ) . '
                    </select>
                </div>
                <div class="form-group">
                    <label>Private Transport Fee</label>
                    <input type="text" class="text text-medium form-control" name="tafee[]" value="' . $_POST['tafee'][$idx] . '" />
                    <input type="text" class="tdid sr-only" name="tdid[]" value="' . $_POST['tdid'][$idx] . '" />
                </div>
                <div class="form-group">
                    <label>Direct/Online Support Transport</label>
                    <select name="tatypeforonline[]" autocomplete="off" style="width:100%;">
                        ' . get_tatypetransport_option( $_POST['tatypeforonline'][$idx] ) . '
                    </select>
                </div>
                <div class="form-group">
                    <label>Agent Support Transport</label>
                    <select name="tatypeforagent[]" autocomplete="off" style="width:100%;">
                        ' . get_tatypetransport_option( $_POST['tatypeforagent'][$idx] ) . '
                    </select>
                </div>
                <div class="form-group">
                    <a class="delete-depart-fee" href="' . HTSERVER . $surl . '/ticket-transport-area-ajax/">delete</a>
                </div>
            </div>';
        }
    }
    else
    {
        $items = get_departure_and_fee( $taid );

        if( !empty( $items ) )
        {
            foreach( $items as $d )
            {
                $content .= '
                <div class="item">
                    <div class="form-group">
                        <label>Departure</label>
                        <select name="lcid[]" autocomplete="off">
                            ' . get_location_option( $d['lcid'] ) . '
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Private Transport Fee</label>
                        <input type="text" class="text text-medium form-control" name="tafee[]" value="' . $d['tafee'] . '" />
                        <input type="text" class="tdid sr-only" name="tdid[]" value="' . $d['tdid'] . '" />
                    </div>
                    <div class="form-group">
                        <label>Direct/Online Support Transport</label>
                        <select name="tatypeforonline[]" autocomplete="off" style="width:100%;">
                            ' . get_tatypetransport_option( $d['tatypeforonline'] ) . '
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Agent Support Transport</label>
                        <select name="tatypeforagent[]" autocomplete="off" style="width:100%;">
                            ' . get_tatypetransport_option( $d['tatypeforagent'] ) . '
                        </select>
                    </div>
                    <div class="form-group">
                        <a class="delete-depart-fee" href="' . HTSERVER . $surl . '/ticket-transport-area-ajax/">delete</a>
                    </div>
                </div>';   
            }
        }
        else
        {
            $content .= '
            <div class="item">
                <div class="form-group">
                    <label>Departure</label>
                    <select name="lcid[]" autocomplete="off">
                        ' . get_location_option() . '
                    </select>
                </div>
                <div class="form-group">
                    <label>Private Transport Fee</label>
                    <input type="text" class="text text-medium form-control" name="tafee[]" value="0" />
                    <input type="text" class="tdid sr-only" name="tdid[]" value="" />
                </div>
                <div class="form-group">
                    <label>Direct/Online Support Transport</label>
                    <select name="tatypeforonline[]" autocomplete="off" style="width:100%;">
                        ' . get_tatypetransport_option() . '
                    </select>
                </div>
                <div class="form-group">
                    <label>Agent Support Transport</label>
                    <select name="tatypeforagent[]" autocomplete="off" style="width:100%;">
                        ' . get_tatypetransport_option() . '
                    </select>
                </div>
                <div class="form-group">
                    <a class="delete-depart-fee" href="' . HTSERVER . $surl . '/ticket-transport-area-ajax/">delete</a>
                </div>
            </div>';
        }
    }

    return $content;
}

/*
| -------------------------------------------------------------------------------------
| Append New Transport Area Departure and Fee
| -------------------------------------------------------------------------------------
*/
function add_departure_and_fee()
{
    return '
    <div class="item">
        <div class="form-group">
            <label>Departure</label>
            <select name="lcid[]" autocomplete="off">
                ' . get_location_option() . '
            </select>
        </div>
        <div class="form-group">
            <label>Private Transport Fee</label>
            <input type="text" class="text text-medium form-control" name="tafee[]" value="0" />
            <input type="text" class="tdid sr-only" name="tdid[]" value="" />
        </div>
        <div class="form-group">
            <label>Direct/Online Support Transport</label>
            <select name="tatypeforonline[]" autocomplete="off" style="width:100%;">
                ' . get_tatypetransport_option() . '
            </select>
        </div>
        <div class="form-group">
            <label>Agent Support Transport</label>
            <select name="tatypeforagent[]" autocomplete="off" style="width:100%;">
                ' . get_tatypetransport_option() . '
            </select>
        </div>
        <div class="form-group">
            <a class="delete-depart-fee" href="' . HTSERVER . site_url() . '/ticket-transport-area-ajax/">delete</a>
        </div>
    </div>';
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Area Option
| -------------------------------------------------------------------------------------
*/
function get_transport_area_option( $taid = '', $use_empty = true, $empty_text = 'Select Area', $use_name_as_value = false )
{
    global $db;

    $s = 'SELECT * FROM ticket_transport_area';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    while( $d = $db->fetch_array( $r ) )
    {
        if( $use_name_as_value )
        {
            $option .= '<option value="' . $d['taname'] . '" ' . ( $d['taname'] == $taid ? 'selected' : '' ) . ' >' . $d['taname'] . '</option>';
        }
        else
        {
            $option .= '<option value="' . $d['taid'] . '" ' . ( $d['taid'] == $taid ? 'selected' : '' ) . ' >' . $d['taname'] . '</option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Area Support Type Option
| -------------------------------------------------------------------------------------
*/
function get_tatypetransport_option( $val = '0' )
{
    return '
    <option value="0" ' . ( $val == '0' ? 'selected' : '' ) . '>Both transport</option>
    <option value="1" ' . ( $val == '1' ? 'selected' : '' ) . '>Shared transport only</option>
    <option value="2" ' . ( $val == '2' ? 'selected' : '' ) . '>Private transport only</option>';
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_transport_area_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_transport_area_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-transport-area' )
    {
        if( delete_transport_area( $_POST['taid'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-departure' )
    {
        echo add_departure_and_fee();
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-transport-fee' )
    {
        $d = get_departure_and_fee( $_POST['taid'] );

        if( count( $d ) > 1 )
        {
            if( delete_transport_fee( $_POST['tdid'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            echo json_encode( array( 'result' => 'error' ) );
        }
    }
}

?>