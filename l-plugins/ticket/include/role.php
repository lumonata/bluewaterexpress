<?php

add_actions( 'role', 'ticket_role' );

function ticket_role()
{
    if( is_num_role() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'admin&sub=role&prc=add_new' ) );
    }

    if( is_add_new() )
    {
        return users_add_new_role();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_role( $_GET['id'] ) )
        {
            return users_edit_role();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return users_batch_delete_role();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_role( $val );
        }
    }

    return users_role_table();
}

?>