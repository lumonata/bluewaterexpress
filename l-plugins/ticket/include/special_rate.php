<?php
if (isset($_POST['update_sort_id']) && isset($_POST['state']) && $_POST['state']=='ticket' && $_POST['sub']=='special_rate' ){
	ticket_special_rate_update_sort_id();
	exit;
}

add_actions('special_rate','ticket_special_rate_index');
add_actions("ticket-special_rate-ajax_page","ticket_special_rate_ajax");
function ticket_special_rate_index(){	
	if(is_add_new()){			
		return ticket_special_rate_new() ;
	}elseif(is_edit()){ 
		return ticket_special_rate_edit($_GET['id']);
	}elseif(is_edit_all() && isset($_POST['select'])){
		
	}elseif(is_delete_all()){
		return ticket_special_rate_delete_multiple(); 
	}elseif(is_confirm_delete()){
		foreach($_POST['id'] as $key=>$val){ticket_special_rate_delete($val);}
	}
	
	//Display data special_rate
    if(ticket_special_rate_num()>0){
        return ticket_special_rate_table_data();
    }else{			
        return ticket_special_rate_new();
    }	
}
function ticket_special_rate_new(){
	ticket_special_rate_set_template('special_rate_new.html','tplspecial_rateNew','special_rateNew','rNew');
	$i 		= 0;
	$id 	= '';
	$button	= "";
	$button	.="	
	<li>".button("button=add_new",get_state_url('ticket&sub=special_rate')."&prc=add_new")."</li>	
	<li>".button("button=save_changes&label=Save")."</li>
	<li>".button("button=cancel",get_state_url('ticket&sub=special_rate'))."</li>";
		
	
	add_actions('section_title','Special rate - Add New');
	add_variable('title_Form','Special rate');	
	
	//init
	add_variable('i',$i);
	add_variable('option_route','<option value="0">All routes</option>'.ticket_route_option());
	add_variable('option_schedule','<option value="0" rel="all">All schedules</option>'.ticket_schedule_option());
	add_variable('status_checked_publish','checked');
	
	if (isset($_POST['save_changes'])){
		$validation = true;		
		$error	= '';
		//validation
		if ($_POST['srname'][$i]==''){			
			$error .= '<div class="error_red">Please type special rate name</div>';			
			$validation = false;
		}
		if ($_POST['rid'][$i]==''){			
			$error .= '<div class="error_red">Please choose route</div>';			
			$validation = false;
		}
		if ($_POST['sid'][$i]==''){			
			$error .= '<div class="error_red">Please choose schedule</div>';			
			$validation = false;
		}
		if ($_POST['srdate_start'][$i]==''){			
			$error .= '<div class="error_red">Please set start date</div>';			
			$validation = false;
		}	
		if ($_POST['srdate_start'][$i]==''){			
			$error .= '<div class="error_red">Please set end date</div>';			
			$validation = false;
		}		
		if ($_POST['srprice_1way'][$i]==''){			
			$error .= '<div class="error_red">Please type price one way</div>';			
			$validation = false;
		}
		if ($_POST['srprice_2way'][$i]==''){			
			$error .= '<div class="error_red">Please type price return way</div>';			
			$validation = false;
		}
		if ($_POST['srprice_1way_child'][$i]==''){			
			$error .= '<div class="error_red">Please type price one way for child</div>';			
			$validation = false;
		}
		if ($_POST['srprice_2way_child'][$i]==''){			
			$error .= '<div class="error_red">Please type price return way for child</div>';			
			$validation = false;
		}		
		
		if ($validation==false){
			add_variable('error',$error);
			add_variable('val_srname',$_POST['srname'][$i]);	
			add_variable('val_srdate_start',$_POST['srdate_start'][$i]);	
			add_variable('val_srdate_end',$_POST['srdate_end'][$i]);	
			
			add_variable('option_route','<option value="0">All routes</option>'.ticket_route_option($_POST['rid'][$i]));
			add_variable('option_schedule','<option value="0" rel="all">All schedules</option>'.ticket_schedule_option($_POST['sid'][$i],$_POST['rid'][$i]));
			add_variable('val_srprice_1way',$_POST['srprice_1way'][$i]);
			add_variable('val_srprice_2way',$_POST['srprice_2way'][$i]);
			add_variable('val_srprice_1way_child',$_POST['srprice_1way_child'][$i]);
			add_variable('val_srprice_2way_child',$_POST['srprice_2way_child'][$i]);			
			
			add_variable('val_srprice_1way_infant',$_POST['srprice_1way_infant'][$i]);
			add_variable('val_srprice_2way_infant',$_POST['srprice_2way_infant'][$i]);
				
		}
		
		//save
		if ($validation==true){
			if (ticket_special_rate_insert()){
				$error = '<div class="error_green">Add special rate has save succesfully.</div>';
				add_variable('error',$error);	
				add_variable('val_rname','');	
				add_variable('val_rboat','');		
				add_variable('val_rfrom','');
				add_variable('val_rto','');
				add_variable('val_srprice_1way','');
				add_variable('val_srprice_2way','');
				add_variable('val_srprice_1way_child','');
				add_variable('val_srprice_2way_child','');			
			
				add_variable('val_srprice_1way_infant','');
				add_variable('val_srprice_2way_infant','');
				
				add_variable('val_rallotment','');				
			}else{
				$error = '<div class="error_red">Something wrong, please try againt.</div>';
				add_variable('error',$error);
			}
		}		
	}
	
	
	add_variable('button',$button);
	parse_template('loopPage','lPage',false);
	 
	return ticket_special_rate_return_template();
}
function ticket_special_rate_edit($id){
	global $db;

	$index=0;
	$button="";
	ticket_special_rate_set_template();

	$button.="
			<li>".button("button=add_new",get_state_url('ticket&sub=special_rate')."&prc=add_new")."</li>
		   	<li>".button("button=save_changes&label=Save")."</li>
			<li>".button("button=cancel",get_state_url('ticket&sub=special_rate'))."</li>";
	
	//set the page Title
	add_actions('section_title','Special Rate - Edit');

	//echo "is_single_edit";
	add_variable('title_Form','Edit Special Rate');
		
	if (isset($_POST['save_changes']) and isset($_GET['prc']) and $_GET['prc']=='edit'){			
		$i 	= 0;
		$id = $_GET['id'];					
		$validation = true;		
		$error	= '';
		//validation
		if ($_POST['srname'][$i]==''){			
			$error .= '<div class="error_red">Please type special rate name</div>';			
			$validation = false;
		}
		if ($_POST['rid'][$i]==''){			
			$error .= '<div class="error_red">Please choose route</div>';			
			$validation = false;
		}
		if ($_POST['sid'][$i]==''){			
			$error .= '<div class="error_red">Please choose schedule</div>';			
			$validation = false;
		}
		if ($_POST['srdate_start'][$i]==''){			
			$error .= '<div class="error_red">Please set start date</div>';			
			$validation = false;
		}	
		if ($_POST['srdate_start'][$i]==''){			
			$error .= '<div class="error_red">Please set end date</div>';			
			$validation = false;
		}		
		if ($_POST['srprice_1way'][$i]==''){			
			$error .= '<div class="error_red">Please type price one way</div>';			
			$validation = false;
		}
		if ($_POST['srprice_2way'][$i]==''){			
			$error .= '<div class="error_red">Please type price return way</div>';			
			$validation = false;
		}
		if ($_POST['srprice_1way_child'][$i]==''){			
			$error .= '<div class="error_red">Please type price one way for child</div>';			
			$validation = false;
		}
		if ($_POST['srprice_2way_child'][$i]==''){			
			$error .= '<div class="error_red">Please type price return way for child</div>';			
			$validation = false;
		}		
		
		if ($validation==false){
			add_variable('error',$error);
			add_variable('val_srname',$_POST['srname'][$i]);	
			add_variable('val_srdate_start',$_POST['srdate_start'][$i]);	
			add_variable('val_srdate_end',$_POST['srdate_end'][$i]);	
			
			add_variable('option_route','<option value="0">All routes</option>'.ticket_route_option($_POST['rid'][$i]));
			add_variable('option_schedule','<option value="0" rel="all">All schedules</option>'.ticket_schedule_option($_POST['sid'][$i],$_POST['rid'][$i]));
			add_variable('val_srprice_1way',$_POST['srprice_1way'][$i]);
			add_variable('val_srprice_2way',$_POST['srprice_2way'][$i]);
			add_variable('val_srprice_1way_child',$_POST['srprice_1way_child'][$i]);
			add_variable('val_srprice_2way_child',$_POST['srprice_2way_child'][$i]);			
			
			add_variable('val_srprice_1way_infant',$_POST['srprice_1way_infant'][$i]);
			add_variable('val_srprice_2way_infant',$_POST['srprice_2way_infant'][$i]);
		}
		
		//save
		if ($validation==true){
			if (ticket_special_rate_update()){
				$error = '<div class="error_green">'.$_POST['srname'][$i].' has been update succesfully.</div>';	
				add_variable('error',$error);					
			}else{
				$error = '<div class="error_red">Something wrong, please try againt.</div>';
				add_variable('error',$error);
			}
		}			
			
	}

	//show data from db
	$q = $db->prepare_query("SELECT * from ticket_special_rate WHERE srid=%d",$id);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);	
	add_variable('val_srid',$d['srid']);	
	add_variable('val_srname',$d['srname']);	
	
	add_variable('val_srdate_start',date("m/d/Y",$d['srdate_start']));	
	add_variable('val_srdate_end',date("m/d/Y",$d['srdate_end']));	
	
	$selected = $d['rid']==0? 'selected':'';	
	add_variable('option_route','<option value="0" '.$selected.' rel="all">All routes</option>'.ticket_route_option($d['rid']));
	$selected = $d['sid']==0? 'selected':'';
	add_variable('option_schedule','<option value="0" '.$selected.' rel="all">All schedules</option>'.ticket_schedule_option($d['sid'],$d['rid']));
	
	add_variable('val_srprice_1way',$d['srprice_1way']);
	add_variable('val_srprice_2way',$d['srprice_2way']);
	add_variable('val_srprice_1way_child',$d['srprice_1way_child']);
	add_variable('val_srprice_2way_child',$d['srprice_2way_child']);			
			
	add_variable('val_srprice_1way_infant',$d['srprice_1way_infant']);
	add_variable('val_srprice_2way_infant',$d['srprice_2way_infant']);
		
	if ($d['status']==1){
		add_variable('status_checked_publish','checked');	
	}else{
		add_variable('status_checked_not_publish','checked');
	}
	
	add_variable('i',$index);
	parse_template('loopPage','lPage',false);
	
	add_variable('button',$button);
	return ticket_special_rate_return_template();
}
function ticket_special_rate_table_data(){
	global $db;
	
	$app_name = 'ticket_special_rate';
	$list='';
	$option_viewed="";
	$data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
	
	if(isset($_POST['data_to_show']))
		$show_data=$_POST['data_to_show'];
	elseif(isset($_GET['data_to_show']))
		$show_data=$_GET['data_to_show'];
   
	
	foreach($data_to_show as $key=>$val){
		if(isset($show_data)){
			if($show_data==$key){
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
			}else{
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
			}
		}elseif($key=='all'){
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
		}else{
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
		}
	}
	
	
	
	if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
		$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	}else{
		$w="";
	}
	
	//paging system
	$viewed=list_viewed();
	if(isset($_GET['page'])){
		$page= $_GET['page'];
	}else{
		$page=1;
	}
	
	$limit=($page-1)*$viewed;
	$url=get_state_url('ticket&sub=special_rate')."&page=";
	
	if(is_search_special_rate()){ 
		$key = 	$_POST['s']; 	
		$sql=$db->prepare_query("SELECT * from ticket_special_rate 
				WHERE srname like %s 
				ORDER BY sort_id ASC",
				"%".$key."%");
		$num_rows=count_rows($sql);	
	}else{ 
		$sql=$db->prepare_query("select * from ticket_special_rate ORDER BY sort_id ASC");
		$num_rows=count_rows($sql);
		       
        $sql=$db->prepare_query("select * from ticket_special_rate ORDER BY sort_id ASC limit %d, %d",$limit,$viewed);		
	}		
	
	$result=$db->do_query($sql);
	
	$start_order=($page - 1) * $viewed + 1; //start order number
	$button="
		<li>".button("button=add_new",get_state_url("ticket&sub=special_rate")."&prc=add_new")."</li>
		<li>".button('button=delete&type=submit&enable=false')."</li>
		<!--li>".button('button=unpublish&type=submit&enable=false')."</li-->";
	
	
	$url_ajax = HTSERVER.SITE_URL.'/ticket-special_rate-ajax/';
	$list.="<h1>Special Rate</h1>
			<div class=\"tab_container\"> 	
				<div class=\"single_content\">
					<div id=\"response\"></div>
					<form action=\"".get_state_url('ticket&sub=special_rate')."\" method=\"post\" name=\"alist\">
					   <div class=\"button_right\">
							".ticket_special_rate_search_box($url_ajax,'list_item','state=ticket&sub=special_rate&prc=search&','right','alert_green_form','Search')."
							
					   </div>
					   <br clear=\"all\" />
					    <input type=\"hidden\" name=\"thaURL\" value=\"".get_state_url('ticket&sub=special_rate')."\" />
					   <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
					   <input type=\"hidden\" name=\"state\" value=\"ticket&sub=special_rate\" />
						<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
									<ul class=\"button_navigation\">
											$button
									</ul>
							</div>
						   
						</div>
						<!--div class=\"status_to_show\">Show: $option_viewed</div-->
						<div class=\"list\">
							<div class=\"list_title\">
								<input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" style=\"margin-left:7px;\" />								
								
								<div class=\"pages_title\" style=\"width:20%;\">Name</div>	
								<div class=\"pages_title\" style=\"width:20%;\">Applied to</div>		
								<div class=\"pages_title\" style='max-width:10%;' >Start date</div>		
								<div class=\"pages_title\" style='max-width:10%;' >End date</div>		
								<div class=\"pages_title\" style='max-width:15%;' >Price one way</div>
								<div class=\"pages_title\" style='max-width:15%;' >Price return</div>	
								<div class=\"pages_title\" style='max-width:5%;' >Status</div>							
							</div>
							<div id=\"list_item\">";
	$list.=ticket_special_rate_list_data($result,$start_order);
	$list.="		</div>
						</div>
					</form>
					
					<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
								<ul class=\"button_navigation\">
									
								</ul>   
							</div>
					</div>
					<div class=\"paging_right\">
								".paging($url,$num_rows,$page,$viewed,5)."
					</div>
				</div>
			</div>
		";
	
	$list .='<script type="text/javascript" src="http://'.site_url().'/l-plugins/ticket/js/list.js" ></script>';	
	add_actions('section_title','Special rate');
	return $list;
}
function ticket_special_rate_list_data($result,$i=1){
 		global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No data found</div>";
        }
        
        while($d=$db->fetch_array($result)){      
        		$status_publish =   $d['status']?'Publish':'Not publish';		
				$list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['srid']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['srid']."\" style='margin-left:13px;' />
                                <div class=\"pages_title\" style=\"width:20%;\">".$d['srname']."</div>
                                 <div class=\"pages_title\" style=\"width:20%;\">".ticket_special_rate_apply_to($d['rid'],$d['sid'])."</div>
								<div class=\"pages_title\" style=\"width:10%;\">".date("d/m/Y",$d['srdate_start'])."</div>
								<div class=\"pages_title\" style=\"width:10%;\">".date("d/m/Y",$d['srdate_end'])."</div>
								<div class=\"pages_title\" style=\"width:15%;\">".$d['srprice_1way']."</div>
								<div class=\"pages_title\" style=\"width:15%;\">".$d['srprice_2way']."</div>
								<div class=\"pages_title\" style=\"width:5%;\">".$status_publish."</div>
								
								
                                <div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['srid']."\">
                                                <a href=\"".get_state_url('ticket&sub=special_rate')."&prc=edit&id=".$d['srid']."\">Edit</a> |
                                                <a href=\"javascript:;\" rel=\"delete_".$d['srid']."\">Delete</a>

                                        </div>
                                </div>
                                <script type=\"text/javascript\" language=\"javascript\">
                                        $('#theitem_".$d['srid']."').mouseover(function(){
                                                $('#the_navigation_".$d['srid']."').show();
                                        });
                                        $('#theitem_".$d['srid']."').mouseout(function(){
                                                $('#the_navigation_".$d['srid']."').hide();
                                        });
                                </script>
                                
                        </div>";
                $msg="Are you sure to delete ".$d['srname']."?";
				$url = HTSERVER.SITE_URL.'/ticket-special_rate-ajax/';
                add_actions('admin_tail','ticket_special_rate_delete_confirmation_box',$d['srid'],$msg,$url,"theitem_".$d['srid'],'state=ticket&sub=special_rate&prc=delete&id='.$d['srid']);                
                $i++;                 
        }
        return $list;
}
function ticket_special_rate_delete_multiple(){
	 global $db;
	 add_actions('section_title','Delete Special Rate');
				$warning="<h1>Special Rate</h1>
                <div class=\"tab_container\">
                    <div class=\"single_content\">";
                $warning.="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this special_rate:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these special_rate:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
						$q = $db->prepare_query("SELECT * FROM ticket_special_rate WHERE srid=%d",$val);
						$r = $db->do_query($q);
                        $d = $db->fetch_array($r);
                        $warning.="<li>".$d['srname']."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$val."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('ticket&sub=special_rate')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
				$warning.="</div></div>";
                
    return $warning;	
}
function ticket_special_rate_search_box($url='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){
	$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
					<div class=\"textwrap\">
						<input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
					</div>
					<div class=\"buttonwrap\" style='position:relative;'>
						<input type='submit' name='search_special_rate' value='yes' style='position:absolute;bottom:0;left:0;top:0;opacity:0;cursor:pointer;'>	
						<input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"_search\" class=\"searchbutton\" value=\"yes\" />						
					</div>
				</div>
				<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
					<img src=\"". get_theme_img() ."/loader.gif\"  />
				</div>
				";
			
	if(!empty($url)){
		$searchbox.="<script type=\"text/javascript\">
			$(function(){
				
				$('.searchtext').keyup(function(){
					
					$('#$results_id').html('<div class=".$class.">Searching...</div>');
					var s = $('input[name=s]').val();
					var parameter='".$param."s='+s;
					
					$('#search_loader').show();
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{
							jpKEY : 'ticket_special_rate_search',
							val_s : s
						}, function(data){
								$('#".$results_id."').html(data);
								$('#search_loader').hide();
						});
					
					
					$('#response').html('');
					
				});
				
				
			});
			
			$(function(){
				$('.searchtext').focus(function(){
					$('.searchtext').val('');
				});
			});
			$(function(){
				var search_text='".$text."';
				$('.searchtext').blur(function(){
					$('.searchtext').val($(this).val()==''?search_text:$(this).val());
				});
			});
			</script>";
	}	    
	return $searchbox;
}
//function only
function is_search_special_rate(){
	if((isset($_POST['search_special_rate']) && $_POST['search_special_rate']=='yes'))
		return true;
	return false;		
}
function ticket_special_rate_delete($id){
	global $db;
	$query = $db->prepare_query("DELETE FROM ticket_special_rate Where srid = %d",$id);
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}
function ticket_special_rate_delete_confirmation_box($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
				$('#delete_yes_".$id."').click(function(){
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{ 
							jpKEY : 'ticket_special_rate_delete',
							val_id : ".$id."
						}, function(data){
							//alert(data);
							if (data='true'){							
								$('#delete_confirmation_wrapper_".$id."').hide('fast'); 
								$('#".$close_frameid."').css('background','#FF6666');
								$('#".$close_frameid."').delay(500);
								$('#".$close_frameid."').fadeOut(700);
								setTimeout(
									function(){
										location.reload(true);
									}, 1500);
									
								return false;
							}
						});
					
					
					/*
					$('select').show();
				    $.post('".$url."', '".$var."', function(theResponse){
						$('#response').html(theResponse);
					});
				    $('#delete_confirmation_wrapper_".$id."').hide('fast');
				    $('#".$close_frameid."').css('background','#FF6666');
				    $('#".$close_frameid."').delay(500);
				    $('#".$close_frameid."').fadeOut(700);
				    setTimeout(
				    	function(){
				  			location.reload(true);
                    	}, 1500);
                    	
				    return false;
					*/
					
				});
			    });
		";
		$delbox.="</script>";
		
		return $delbox;
	}
function ticket_special_rate_get_detail_by_id($srid,$field=''){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_special_rate where srid=%d",$srid);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	if (empty($d)){
		return false;
	}
	if (isset($d[$field])){
		return $d[$field];
	}else{
		return $d;
	}
}
function ticket_special_rate_insert($i=0){
	global $db;
	$q = $db->prepare_query("INSERT INTO ticket_special_rate 
			(srname,srdate_start,srdate_end,
			srprice_1way,srprice_2way,srprice_1way_child,srprice_2way_child,
			rid,sid,
			sort_id,status,created_by,created_date,
			srprice_1way_infant,srprice_2way_infant) 
			VALUES 			
			(%s,%s,%s,
			%d,%d,%d,%d,
			%d,%d,
			%d,%d,%s,%d,%d,%d)",	
			$_POST['srname'][$i],strtotime($_POST['srdate_start'][$i]),strtotime($_POST['srdate_end'][$i]),		
			$_POST['srprice_1way'][$i],$_POST['srprice_2way'][$i],$_POST['srprice_1way_child'][$i],$_POST['srprice_2way_child'][$i],
			$_POST['rid'][$i],$_POST['sid'][$i],
			0,$_POST['status'][$i],$_COOKIE['username'],time(),
			$_POST['srprice_1way_infant'][$i],$_POST['srprice_2way_infant'][$i]			
			);
	$r = $db->do_query($q);
	return $r;
}
function ticket_special_rate_update($i=0){
	global $db;
	$q = $db->prepare_query("UPDATE ticket_special_rate SET
			srname=%s,srdate_start=%d,srdate_end=%d,
			srprice_1way=%d,srprice_2way=%d,srprice_1way_child=%d,srprice_2way_child=%d,
			rid=%d,sid=%d,
			status=%d,dlu=%d,srprice_1way_infant=%d,srprice_2way_infant=%d WHERE srid=%d",			
			$_POST['srname'][$i],strtotime($_POST['srdate_start'][$i]),strtotime($_POST['srdate_end'][$i]),		
			$_POST['srprice_1way'][$i],$_POST['srprice_2way'][$i],$_POST['srprice_1way_child'][$i],$_POST['srprice_2way_child'][$i],
			$_POST['rid'][$i],$_POST['sid'][$i],
			$_POST['status'][$i],time(),
			$_POST['srprice_1way_infant'][$i],$_POST['srprice_2way_infant'][$i],
			$_POST['srid'][$i]			
			);
	$r = $db->do_query($q);
	return $r;
}
function ticket_special_rate_num(){
	global $db;
	
	$q = $db->prepare_query("SELECT * FROM ticket_special_rate");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	return $n;
}
function ticket_special_rate_set_template(){
        //set template
        set_template(PLUGINS_PATH."/".ticket_template_dir()."/special_rate_new.html",'template_special_rate_new');
        //set block
        add_block('loopPage','lPage','template_special_rate_new');
        add_block('special_rateNew','rNew','template_special_rate_new');
}

function ticket_special_rate_return_template($loop=false){       
        parse_template('special_rateNew','rNew',$loop);
        return return_template('template_special_rate_new');
}
function ticket_special_rate_ajax(){
	global $db;
	$app_name = 'ticket_special_rate'; 
	add_actions('is_use_ajax', true);
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_special_rate_delete'){
		$id = $_POST['val_id'];
		if (ticket_special_rate_delete($id)){
			echo "true";
		}else{
			echo "false";
		}
	}	
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_special_rate_search'){
		if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
			$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	    }else{
			$w="";
	    }	 

	    $sql=$db->prepare_query("select * from ticket_special_rate 
				WHERE  
				srname like %s OR 
				srprice_1way like %s OR 
				srprice_2way like %s OR 
				srprice_1way_child like %s OR 
				srprice_2way_child like %s
				order by sort_id ASC",
				"%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%");
	 
		 $r=$db->do_query($sql);
        if($db->num_rows($r) > 0){			
			echo ticket_special_rate_list_data($r);
        }else{
            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['val_s']."</em>. Check your spellling or try another terms</div>";
        }
	}	
}
function ticket_special_rate_update_sort_id(){
	global $db;
	$items 		= $_POST['theitem'];
	$start 		= $_POST['start'];	
 	
	foreach($items as $key=>$val){
         $sql=$db->prepare_query("UPDATE ticket_special_rate SET 
                                     sort_id=%d,dlu=%d WHERE srid=%d",
                                     $key+$start,time(),$val);
            $db->do_query($sql);     
    }
}
?>