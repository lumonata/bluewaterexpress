<?php

add_actions( 'agent-discount', 'ticket_agent_discount' );
add_actions( 'agent-discount-ajax_page', 'ticket_agent_discount_ajax' );

function ticket_agent_discount()
{
    if( is_num_agent_discount() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'booking-source&sub=agent-discount&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_agent_discount();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_agent_discount( $_GET['id'] ) )
        {
            return ticket_edit_agent_discount();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_agent_discount();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_agent_discount( $val );
        }
    }

    return ticket_agent_discount_table();
}

/*
| -------------------------------------------------------------------------------------
| Agent Discount Table List
| -------------------------------------------------------------------------------------
*/
function ticket_agent_discount_table()
{
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/agent-discount/list.html', 'agent-discount' );

    add_block( 'list-block', 'lcblock', 'agent-discount' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'booking-source&sub=agent-discount' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/agent-discount-ajax/' );
    add_variable( 'edit_link', get_state_url( 'booking-source&sub=agent-discount&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'booking-source&sub=agent-discount&prc=add_new' ) );

    parse_template( 'list-block', 'lcblock', false );

    add_actions( 'section_title', 'Agent Discount List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'agent-discount' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Agent Discount
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_agent_discount()
{
	run_save_agent_discount();

    $site_url = site_url();
    $data     = get_agent_discount();

	set_template( PLUGINS_PATH . '/ticket/tpl/agent-discount/form.html', 'agent-discount' );
    add_block( 'form-block', 'lcblock', 'agent-discount' );

    add_variable( 'adid', $data['adid'] );
    add_variable( 'adname', $data['adname'] );
    add_variable( 'addisc', $data['addisc'] );
    add_variable( 'adto', date( 'd F Y', strtotime( $data['adto'] ) ) );
    add_variable( 'adfrom', date( 'd F Y', strtotime( $data['adfrom'] ) ) );
    add_variable( 'adapply', get_agent_option( $data['adapply'], false ) );
    add_variable( 'adtype', get_agent_discount_type_option( $data['adtype'], false ) );
    add_variable( 'adstatus', get_agent_discount_status_option( $data['adstatus'], false ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'booking-source&sub=agent-discount' ) );
    add_variable( 'action', get_state_url( 'booking-source&sub=agent-discount&prc=add_new' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'New Agent Discount' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'agent-discount' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Agent Discount
| -------------------------------------------------------------------------------------
*/
function ticket_edit_agent_discount()
{
    run_update_agent_discount();

    $site_url = site_url();
    $data     = get_agent_discount( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/agent-discount/form.html', 'agent-discount' );
    add_block( 'form-block', 'lcblock', 'agent-discount' );

    add_variable( 'adid', $data['adid'] );
    add_variable( 'adname', $data['adname'] );
    add_variable( 'addisc', $data['addisc'] );
    add_variable( 'adto', date( 'd F Y', strtotime( $data['adto'] ) ) );
    add_variable( 'adfrom', date( 'd F Y', strtotime( $data['adfrom'] ) ) );
    add_variable( 'adapply', get_agent_option( $data['adapply'], false ) );
    add_variable( 'adtype', get_agent_discount_type_option( $data['adtype'], false ) );
    add_variable( 'adstatus', get_agent_discount_status_option( $data['adstatus'], false ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'booking-source&sub=agent-discount' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/agent-discount-ajax/' );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'agent-discount' ) );
    add_variable( 'action', get_state_url( 'booking-source&sub=agent-discount&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'Edit Agent Discount' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'agent-discount' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Agent Discount
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_agent_discount()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/agent-discount/batch-delete.html', 'agent-discount' );
    add_block( 'loop-block', 'lclblock', 'agent-discount' );
    add_block( 'delete-block', 'lcblock', 'agent-discount' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_agent_discount( $val );

        add_variable('adname',  $d['adname'] );
        add_variable('adid', $d['adid'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' agent-discount? :' );
    add_variable('action', get_state_url('booking-source&sub=agent-discount'));

    parse_template( 'delete-block', 'lcblock', false );

    add_actions( 'section_title', 'Delete Agent Discount' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'agent-discount' );
}

function run_save_agent_discount()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_agent_discount_data();

        if( empty( $error ) )
        {
            $post_id = save_agent_discount();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new agent discount' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New agent discount successfully saved' ) ) );

                header( 'location:' . get_state_url( 'booking-source&sub=agent-discount&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_agent_discount()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_agent_discount_data();

        if( empty($error) )
        {
            $post_id = update_agent_discount();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this sales channel' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This agent discount successfully edited' ) ) );

                header( 'location:' . get_state_url( 'booking-source&sub=agent-discount&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_agent_discount_data()
{
    $error = array();

    if( isset( $_POST['adname'] ) && empty( $_POST['adname'] ) )
    {
        $error[] = 'Agent Discount name can\'t be empty';
    }

    if( ( isset( $_POST['adfrom'] ) && empty( $_POST['adfrom'] ) ) || ( isset( $_POST['adto'] ) && empty( $_POST['adto'] ) ) )
    {
        $error[] = 'Agent Discount period not valid';
    }

    if( isset( $_POST['addisc'] ) && empty( $_POST['addisc'] ) )
    {
        $error[] = 'Agent Discount value can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Discount List Count
| -------------------------------------------------------------------------------------
*/
function is_num_agent_discount()
{
    global $db;

    $s = 'SELECT * FROM ticket_agent_discount';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Agent Discount Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_agent_discount_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1 => 'a.adid'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.adid DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT * FROM ticket_agent_discount AS a ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT * FROM ticket_agent_discount AS a WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'adid'      => $d2['adid'],
                'adname'    => $d2['adname'],
                'addisc'    => $d2['addisc'],
                'adto'      => date( 'd F Y', strtotime( $d2['adto'] ) ),
                'adfrom'    => date( 'd F Y', strtotime( $d2['adfrom'] ) ),
                'adstatus'  => ( $d2['adstatus'] == '0' ? 'Publish' : 'Unpublish' ),
                'edit_link' => get_state_url( 'booking-source&sub=agent-discount&prc=edit&id=' . $d2['adid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Discount By ID
| -------------------------------------------------------------------------------------
*/
function get_agent_discount( $id = '', $field = '' , $type = '' )
{
    global $db;

    $data = array(
        'adid'     => ( isset( $_POST['adid'] ) ? $_POST['adid'] : null ),
        'adname'   => ( isset( $_POST['adname'] ) ? $_POST['adname'] : '' ),
        'adapply'  => ( isset( $_POST['adapply'] ) ? $_POST['adapply'] : '' ),
        'adstatus' => ( isset( $_POST['adstatus'] ) ? $_POST['adstatus'] : '' ),
        'adfrom'   => ( isset( $_POST['adfrom'] ) ? $_POST['adfrom'] : date( 'd F Y') ),
        'adto'     => ( isset( $_POST['adto'] ) ? $_POST['adto'] : date( 'd F Y') ),
        'addisc'   => ( isset( $_POST['addisc'] ) ? $_POST['addisc'] : '' ),
        'adtype'   => ( isset( $_POST['adtype'] ) ? $_POST['adtype'] : '' )
    );

    $s = 'SELECT * FROM ticket_agent_discount WHERE adid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'adid'     => $d['adid'],
                'adname'   => $d['adname'],
                'adapply'  => $d['adapply'],
                'adstatus' => $d['adstatus'],
                'adfrom'   => $d['adfrom'],
                'adto'     => $d['adto'],
                'addisc'   => $d['addisc'],
                'adtype'   => $d['adtype']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Discount By Name
| -------------------------------------------------------------------------------------
*/
function get_agent_discount_by_name( $name = '', $field = '' )
{
    global $db;

    $data = array(
        'adid'     => ( isset( $_POST['adid'] ) ? $_POST['adid'] : null ),
        'adname'   => ( isset( $_POST['adname'] ) ? $_POST['adname'] : '' ),
        'adapply'  => ( isset( $_POST['adapply'] ) ? $_POST['adapply'] : '' ),
        'adstatus' => ( isset( $_POST['adstatus'] ) ? $_POST['adstatus'] : '' ),
        'adfrom'   => ( isset( $_POST['adfrom'] ) ? $_POST['adfrom'] : '' ),
        'adto'     => ( isset( $_POST['adto'] ) ? $_POST['adto'] : '' ),
        'addisc'   => ( isset( $_POST['addisc'] ) ? $_POST['addisc'] : '' ),
        'adtype'   => ( isset( $_POST['adtype'] ) ? $_POST['adtype'] : '' )
    );

    $s = 'SELECT * FROM ticket_agent_discount WHERE adname = %s';
    $q = $db->prepare_query( $s, $name );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'adid'     => $d['adid'],
                'adname'   => $d['adname'],
                'adapply'  => $d['adapply'],
                'adstatus' => $d['adstatus'],
                'adfrom'   => $d['adfrom'],
                'adto'     => $d['adto'],
                'addisc'   => $d['addisc'],
                'adtype'   => $d['adtype']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Discount List
| -------------------------------------------------------------------------------------
*/
function get_agent_discount_list( $where = array() )
{
    global $db;

    $s = 'SELECT * FROM ticket_agent_discount';

    if( empty( $where ) === false )
    {
        $w = array();

        foreach( $where as $field => $val )
        {
            $w[] = $db->prepare_query( $field . ' = %s', $val );
        }

        $s .= ' WHERE ' . implode( 'AND', $w );
    }
    
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'adid'     => $d['adid'],
            'adname'   => $d['adname'],
	        'adapply'  => $d['adapply'],
	        'adstatus' => $d['adstatus'],
	        'adfrom'   => $d['adfrom'],
            'adto'     => $d['adto'],
            'addisc'   => $d['addisc'],
            'adtype'   => $d['adtype']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Agent Discount
| -------------------------------------------------------------------------------------
*/
function get_rate_agent_discount( $agid, $date )
{
    global $db;

    $s = 'SELECT * FROM ticket_agent_discount AS a WHERE a.adfrom <= %s AND a.adto >= %s AND a.adstatus = %s';
    $q = $db->prepare_query( $s, $date, $date, "0" );
    $r = $db->do_query( $q );

    $adisc = array();

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $agapply = json_decode( $d[ 'adapply' ], true );

        if( $agapply !== null && json_last_error() === JSON_ERROR_NONE )
        {
            if( in_array( $agid, $agapply ) )
            {
                $adisc = array( 'type' => $d['adtype'], 'disc' => $d['addisc'], 'notif' => $d['adname'] );
            }
        }
        else
        {
            $adisc = array( 'type' => $d['adtype'], 'disc' => $d['addisc'], 'notif' => $d['adname'] );
        }
    }

    return $adisc;
}

/*
| -------------------------------------------------------------------------------------
| Save Agent Discount
| -------------------------------------------------------------------------------------
*/
function save_agent_discount()
{
    global $db;

    extract( $_POST );

    $adapply = isset( $adapply ) ? json_encode( $adapply ) : null;

    $s = 'INSERT INTO ticket_agent_discount(
            adname,
            adapply,
            adstatus,
            adfrom,
            adto,
            addisc,
            adtype ) 
          VALUES( %s, %s, %s, %s, %s, %s, %s )';
    $q = $db->prepare_query( $s, 
            $adname, 
            $adapply, 
            $adstatus,
            date( 'Y-m-d', strtotime( $adfrom ) ),
            date( 'Y-m-d', strtotime( $adto ) ),
            $addisc,
            $adtype );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $id = $db->insert_id();

        save_log( $id, 'agent-discount', 'Add new agent discount - ' . $_POST['adname'] );

        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Agent Discount
| -------------------------------------------------------------------------------------
*/
function update_agent_discount()
{
    global $db;

    extract( $_POST );

    $adapply = isset( $adapply ) ? json_encode( $adapply ) : null;
    $current = get_agent_discount( $adid );
    $flogs   = array();
    $fname   = array(
        'adname'   => 'Discount Name',
        'adfrom'   => 'From Period',
        'adto'     => 'To Period',
        'addisc'   => 'Value',
        'adstatus' => 'Status'
    );

    foreach( $fname as $i => $d )
    {
        if( $current[ $i ] != $_POST[ $i ] )
        {
            if( in_array( $i, array( 'adfrom', 'adto' ) ) )
            {
                $flogs[] = $fname[ $i ] . ' : ' . date( 'd F Y', strtotime( $current[ $i ] ) ) . ' --> ' . date( 'd F Y', strtotime( $_POST[ $i ] ) );
            }
            else
            {
                $flogs[] = $fname[ $i ] . ' : ' . $current[ $i ] . ' --> ' . $_POST[ $i ];
            }
        }
    }

    $s = 'UPDATE ticket_agent_discount SET
            adfrom = %s,
            adto = %s, 
            adstatus = %s,
            adapply = %s,
            adname = %s,
            addisc = %s,
            adtype = %s
          WHERE adid = %d';
    $q = $db->prepare_query( $s,
            date( 'Y-m-d', strtotime( $adfrom ) ),
            date( 'Y-m-d', strtotime( $adto ) ), 
            $adstatus,
            $adapply,
            $adname, 
            $addisc, 
            $adtype,
            $adid );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        if( !empty( $flogs ) )
        {
            save_log( $adid, 'agent-discount', 'Update agent discount : <br/>' . implode( '<br/>', array_unique( $flogs ) ) );
        }

        return $adid;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Agent Discount
| -------------------------------------------------------------------------------------
*/
function delete_agent_discount( $id = '', $is_ajax = false )
{
    global $db;

    $d = get_agent_discount( $id );

    $s = 'DELETE FROM ticket_agent_discount WHERE adid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'booking-source&sub=agent-discount&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'location', 'Delete agent discount - ' . $d['adname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Discount Option
| -------------------------------------------------------------------------------------
*/
function get_agent_discount_option( $adid = '', $use_empty = true, $empty_text = 'Select Agent Discount', $type = '' )
{
    $rcat   = get_agent_discount_list();
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $rcat ) )
    {
        foreach( $rcat as $d )
        {
            $option .= '<option value="' . $d['adid'] . '" ' . ( $d['adid'] == $adid ? 'selected' : '' ) . ' >' . $d['adname'] . '</option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Discount Type Option
| -------------------------------------------------------------------------------------
*/
function get_agent_discount_type_option( $adtype = '', $use_empty = true, $empty_text = 'Select Agent Discount', $type = '' )
{
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="0" ' . ( $adtype == '0' ? 'selected' : '' ) . '>%</option>
    <option value="1" ' . ( $adtype == '1' ? 'selected' : '' ) . '>Rp.</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Discount Status Option
| -------------------------------------------------------------------------------------
*/
function get_agent_discount_status_option( $adid = '', $use_empty = true, $empty_text = 'Select Agent Discount', $type = '' )
{
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="0" ' . ( $adid == '0' ? 'selected' : '' ) . '>Publish</option>
    <option value="1" ' . ( $adid == '1' ? 'selected' : '' ) . '>Unpublish</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_agent_discount_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_agent_discount_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-agent-discount' )
    {
        $d = delete_agent_discount( $_POST['adid'], true );

        if( $d === true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}