<?php

add_actions( 'log', 'ticket_log' );
add_actions( 'ticket-log-ajax_page', 'ticket_log_ajax' );

function ticket_log()
{
    return ticket_log_table();
}

/*
| -------------------------------------------------------------------------------------
| Log Table List
| -------------------------------------------------------------------------------------
*/
function ticket_log_table()
{
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/log/list.html', 'log' );

    add_block( 'list-block', 'lcblock', 'log' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-log-ajax/' );
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Admin Log List' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'log' );
}

function ticket_log_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array( 
        0 => 'a.lupdate_date', 
        1 => 'a.lapp_type', 
        2 => 'a.ldescription',
        3 => 'b.ldisplay_name'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.lupdate_date DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT 
                a.ldescription, 
                b.ldisplay_name, 
                DATE_FORMAT( lupdate_date, "%d %M %Y, %T") as log_date,
                CASE
                    WHEN a.lapp_type = "boat" THEN "Boat"
                    WHEN a.lapp_type = "payment-method" THEN "Setting - Payment Method"
                    WHEN a.lapp_type = "role" THEN "Admin - Admin Role"
                    WHEN a.lapp_type = "reservation" THEN "Reservation"
                    WHEN a.lapp_type = "agent" THEN "Booking Source - Agent"
                    WHEN a.lapp_type = "channel" THEN "Booking Source - Channel"
                    WHEN a.lapp_type = "rate" THEN "Schedule - Rate"
                    WHEN a.lapp_type = "rate-detail" THEN "Schedule - Rate"
                    WHEN a.lapp_type = "route" THEN "Schedule - Route"
                    WHEN a.lapp_type = "promo" THEN "Schedule - Promo Code"
                    WHEN a.lapp_type = "location" THEN "Schedule - Location"
                    WHEN a.lapp_type = "allotment" THEN "Schedule - Allotment"
                    WHEN a.lapp_type = "rate-period" THEN "Schedule - Rate Period"
                    WHEN a.lapp_type = "schedule" THEN "Schedule - Manage Schedule"
                    WHEN a.lapp_type = "exception" THEN "Schedule - Manage Exception"
                    WHEN a.lapp_type = "hotel" THEN "Transport - Hotel"
                    WHEN a.lapp_type = "vehicle" THEN "Transport - Vehicle"
                    WHEN a.lapp_type = "transport-drive" THEN "Transport - Driver"
                    WHEN a.lapp_type = "vehicle-owner" THEN "Transport - Vehicle Owner"
                    WHEN a.lapp_type = "transport-area" THEN "Transport - Manage Transport Area"
                    WHEN a.lapp_type = "trip-transport" THEN "Transport - Pickup/Drop-off Guest"
                    WHEN a.lapp_type = "port-clearance" THEN "Report - Port Clearance"
                    ELSE "Unknown Application"
                END AS log_app
              FROM l_log AS a LEFT JOIN l_users AS b ON a.luser_id = b.luser_id 
              WHERE a.ldescription <> "" ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT  
                a.ldescription, 
                b.ldisplay_name, 
                DATE_FORMAT( lupdate_date, "%d %M %Y, %T") as log_date,
                CASE
                    WHEN a.lapp_type = "boat" THEN "Boat"
                    WHEN a.lapp_type = "role" THEN "Admin - Admin Role"
                    WHEN a.lapp_type = "reservation" THEN "Reservation"
                    WHEN a.lapp_type = "agent" THEN "Booking Source - Agent"
                    WHEN a.lapp_type = "channel" THEN "Booking Source - Channel"
                    WHEN a.lapp_type = "rate" THEN "Schedule - Rate"
                    WHEN a.lapp_type = "route" THEN "Schedule - Route"
                    WHEN a.lapp_type = "promo" THEN "Schedule - Promo Code"
                    WHEN a.lapp_type = "location" THEN "Schedule - Location"
                    WHEN a.lapp_type = "allotment" THEN "Schedule - Allotment"
                    WHEN a.lapp_type = "rate-period" THEN "Schedule - Rate Period"
                    WHEN a.lapp_type = "schedule" THEN "Schedule - Manage Schedule"
                    WHEN a.lapp_type = "exception" THEN "Schedule - Manage Exception"
                    WHEN a.lapp_type = "hotel" THEN "Transport - Hotel"
                    WHEN a.lapp_type = "vehicle" THEN "Transport - Vehicle"
                    WHEN a.lapp_type = "transport-drive" THEN "Transport - Driver"
                    WHEN a.lapp_type = "vehicle-owner" THEN "Transport - Vehicle Owner"
                    WHEN a.lapp_type = "transport-area" THEN "Transport - Manage Transport Area"
                    WHEN a.lapp_type = "trip-transport" THEN "Transport - Pickup/Drop-off Guest"
                    WHEN a.lapp_type = "port-clearance" THEN "Report - Port Clearance"
                    ELSE "Unknown Application"
                END AS log_app
              FROM l_log AS a LEFT JOIN l_users AS b ON a.luser_id = b.luser_id 
              WHERE a.ldescription <> "" AND ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $d2['ldescription'] = $d2['ldescription'] == '' ? 'No Description' : $d2['ldescription'];

            $data[] = $d2;
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

function ticket_log_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        extract( $_POST );

        $data = ticket_log_table_query();

        echo json_encode( $data );
    }

    exit;
}