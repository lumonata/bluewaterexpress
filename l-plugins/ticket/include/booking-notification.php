<?php

add_actions( 'booking-notification', 'ticket_booking_notification' );
add_actions( 'ticket-booking-notification-ajax_page', 'ticket_booking_notification_ajax' );

function ticket_booking_notification()
{
    return ticket_booking_notification_table_data();
}

/*
| -------------------------------------------------------------------------------------
| Booking Notification Table List
| -------------------------------------------------------------------------------------
*/
function ticket_booking_notification_table_data()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url('reservation&sub=booking-notification') . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    $site_url = site_url();
    $filter   = ticket_filter_booking_notification();

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/booking-notification/list.html', 'booking-notification' );

    add_block( 'list-block', 'bblock', 'booking-notification' );

    add_variable( 'bdate', $bdate );
    add_variable( 'bddate', $bddate );
    add_variable( 'search', $search );
    add_variable( 'bbemail', $bbemail );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'filter', base64_encode( json_encode( $filter ) ) );
    add_variable( 'action', get_state_url( 'reservation&sub=booking-notification' ) );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-booking-notification-ajax' );

    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'location_option', get_location_option( $lcid, true, 'All Departure') );
    add_variable( 'status_option', get_booking_status_option( $status, true, 'All Status') );
    add_variable( 'location2_option', get_location_option( $lcid2, true, 'All Arrival Point') );
    add_variable( 'rev_status', get_reservation_status( $rstatus, true,'All Reservation Status') );
    add_variable( 'bsource_option', get_booking_source_option( $chid, null, true, 'All Booking Source' ) );

    add_actions( 'section_title', 'Send Notification' );
    add_actions( 'other_elements', 'get_javascript', 'jquery.base64.min' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/momentjs/latest/moment.min.js' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' );

    add_actions( 'header_elements', 'get_javascript_inc', 'tiny_mce/tinymce.min.js' );
    add_actions( 'header_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'list-block','bblock', false );

    return return_template( 'booking-notification' );
}

/*
| -------------------------------------------------------------------------------------
| Booking Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_booking_notification_table_query( $chid = '', $lcid = '', $lcid2 = '', $rid = '', $status = '', $bddate = '', $search = '', $bdate = '', $bbemail = '' , $rstatus = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1  => 'a.bticket',
        2  => 'a.bdate',
        5  => 'c.bddate',
        7  => 'c.bddeparttime',
        8  => 'c.bdarrivetime',
        10 => 'a.bbrevstatus'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.bticket DESC, a.bdate DESC';
    }

    if( empty( $rdata['search']['value'] ) )
    {
        $w = '';

        if( !empty( $status ) )
        {
            if( is_array( $status ) )
            {
                $estatus = end( $status );

                $w .= '( ';

                foreach( $status as $st )
                {
                    if( $st == $estatus )
                    {
                        $w .= $db->prepare_query( 'c.bdpstatus = %s', $st );
                    }
                    else
                    {
                        $w .= $db->prepare_query( 'c.bdpstatus = %s OR ', $st );
                    }
                }

                $w .= ' )';
            }
            else
            {
                $w .= $db->prepare_query( 'c.bdpstatus = %s', $status );
            }
        }
        else
        {
            $w .= $db->prepare_query( 'c.bdpstatus NOT IN( %s )', 'ol' );
        }

        if( !empty( $search ) )
        {
            $cols[2] = '(
                SELECT GROUP_CONCAT( CONVERT( bpname USING utf8 ) SEPARATOR "<br/><br/>" )
                FROM ticket_booking_passenger AS a2
                WHERE a2.bdid = c.bdid
                ORDER BY a2.bpid ASC
            )';

            $esearch = end( $cols );

            $w .= ' AND ( ';

            foreach( $cols as $col )
            {
                if( $col == $esearch )
                {
                    $w .= $db->prepare_query( $col . ' LIKE %s', '%' . $search . '%' );
                }
                else
                {
                    $w .= $db->prepare_query( $col . ' LIKE %s OR ', '%' . $search . '%' );
                }
            }

            $w .= ' )';
        }

        if( !empty( $rstatus ) )
        {
            if( is_array( $rstatus ) )
            {
                $estatus = end( $rstatus );

                $w .= ' AND ( ';

                foreach( $rstatus as $st )
                {
                    if( $st == $estatus )
                    {
                        $w .= $db->prepare_query( 'c.bdrevstatus = %s', $st );
                    }
                    else
                    {
                        $w .= $db->prepare_query( 'c.bdrevstatus = %s OR ', $st );
                    }
                }

                $w .= ' )';
            }
            else
            {
                $w .= $db->prepare_query( ' AND c.bdrevstatus = %s', $rstatus );
            }
        }

        if( $chid != '' )
        {
            $arr = explode( '|', $chid );

            if( count( $arr ) == 2 )
            {
                $w .= $db->prepare_query( ' AND a.chid = %d', $arr[0] );
                $w .= $db->prepare_query( ' AND a.agid = %d', $arr[1] );
            }
            else
            {
                $w .= $db->prepare_query( ' AND a.chid = %d', $chid );
            }
        }

        if( $lcid != '' )
        {
            $w .= $db->prepare_query( ' AND c.bdfrom = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid );
        }

        if( $lcid2 != '' )
        {
            $w .= $db->prepare_query( ' AND c.bdto = ( SELECT lcname FROM ticket_location WHERE lcid = %d )', $lcid2 );
        }

        if( $rid != '' )
        {
            $w .= $db->prepare_query( ' AND c.rid = %d', $rid );
        }

        if( $bdate != '' )
        {
            list( $start, $end ) = explode( ' - ', $bdate );

            $start = implode( '-', array_reverse( explode( '/', $start ) ) );
            $end   = implode( '-', array_reverse( explode( '/', $end ) ) );

            $w .= $db->prepare_query( ' AND a.bdate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $start ) ), date( 'Y-m-d', strtotime( $end ) ) );
        }

        if( $bbemail != '' )
        {
            $w .= $db->prepare_query( ' AND a.bbemail LIKE %s', '%' . $bbemail . '%' );
        }

        if( $bddate != '' )
        {
            list( $start, $end ) = explode( ' - ', $bddate );

            $start = implode( '-', array_reverse( explode( '/', $start ) ) );
            $end   = implode( '-', array_reverse( explode( '/', $end ) ) );

            $w .= $db->prepare_query( ' AND c.bddate BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $start ) ), date( 'Y-m-d', strtotime( $end ) ) );
        }

        $s = 'SELECT
                a.bid,
                a.bcode,
                a.agid,
                a.sagid,
                a.bticket,
                b.chcode,
                b.chname,
                a.bbname,
                a.bdate,
                c.bdpstatus,
                c.bdid,
                c.bdfrom,
                c.bdto,
                c.bddate,
                c.bdtype,
                c.bddeparttime,
                c.bdarrivetime,
                a.bbrevtype,
                c.bdrevstatus,
                a.bbrevagent,
                a.bcreason,
                a.bcmessage,
                a.bbphone,
                a.brcdate,
                a.bonhandtotal,
                a.btotal,
                a.bbooking_staf,
                a.bprintboardingstatus,
                a.bbemail,
                a.bremark,
                c.total,
                d.agname,
                c.num_adult,
                c.num_child,
                c.num_infant,
                c.bdstatus,
                a.btype,
                a.pmcode,
                a.bdiscountnum,
                a.bdiscounttype,
                ( c.num_adult + c.num_child + c.num_infant ) AS num_pax,
                (
                    SELECT GROUP_CONCAT( IF( bpstatus = "cn", CONCAT("<strike>",CONVERT( bpname USING utf8 ),"</strike>"), CONVERT( bpname USING utf8 ) ) SEPARATOR "<br/><br/>" )
                    FROM ticket_booking_passenger AS a2
                    WHERE a2.bdid = c.bdid
                    ORDER BY a2.bpid ASC
                ) AS pass_name
              FROM ticket_booking AS a
              LEFT JOIN ticket_channel AS b ON a.chid = b.chid
              LEFT JOIN ticket_booking_detail AS c ON c.bid = a.bid
              LEFT JOIN ticket_agent AS d ON d.agid = a.agid
              WHERE ' . $w . ' AND a.bstt <> "ar" ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $bsearch = array();
        $cols[2] = '(
            SELECT GROUP_CONCAT( CONVERT( bpname USING utf8 ) SEPARATOR "<br/><br/>" )
            FROM ticket_booking_passenger AS a2
            WHERE a2.bdid = c.bdid
            ORDER BY a2.bpid ASC
        )';

        foreach( $cols as $col )
        {
            $bsearch[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                a.bid,
                a.bcode,
                a.agid,
                a.sagid,
                a.bticket,
                b.chcode,
                b.chname,
                a.bbname,
                a.bdate,
                c.bdpstatus,
                c.bdid,
                c.bdfrom,
                c.bdto,
                c.bddate,
                c.bdtype,
                c.bddeparttime,
                c.bdarrivetime,
                a.bbrevtype,
                c.bdrevstatus,
                a.bbrevagent,
                a.bcreason,
                a.bcmessage,
                a.bbphone,
                a.brcdate,
                a.bonhandtotal,
                a.btotal,
                a.bbooking_staf,
                a.bprintboardingstatus,
                a.bbemail,
                a.bremark,
                c.total,
                d.agname,
                c.num_adult,
                c.num_child,
                c.num_infant,
                c.bdstatus,
                a.btype,
                a.pmcode,
                a.bdiscountnum,
                a.bdiscounttype,
                ( c.num_adult + c.num_child + c.num_infant ) AS num_pax,
                (
                    SELECT GROUP_CONCAT( IF( bpstatus = "cn", CONCAT("<strike>",CONVERT( bpname USING utf8 ),"</strike>"), CONVERT( bpname USING utf8 ) ) SEPARATOR "<br/><br/>" )
                    FROM ticket_booking_passenger AS a2
                    WHERE a2.bdid = c.bdid
                    ORDER BY a2.bpid ASC
                ) AS pass_name
              FROM ticket_booking AS a
              LEFT JOIN ticket_channel AS b ON a.chid = b.chid
              LEFT JOIN ticket_booking_detail AS c ON c.bid = a.bid
              LEFT JOIN ticket_agent AS d ON d.agid = a.agid
              WHERE ( ' . implode( ' OR ', $bsearch ) . ' ) AND a.bstt <> "ar" ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data   = array();
    $filter = array( 'rid' => $rid, 'lcid' => $lcid, 'lcid2' => $lcid2, 'chid' => $chid, 'bstatus' => $status, 'bdate' => $bdate, 'bddate' => $bddate, 'sbooking' => $search, 'bbemail' => $bbemail );

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $cnccss    = $d2['bdpstatus'] == 'cn' ? 'sr-only' : '-';
            $trans     = ticket_booking_transport_detail( $d2['bdid'] );
            $booked    = empty( $d2['agid'] ) ? $d2['bbname'] : $d2['bbooking_staf'];
            $brcdate   = $d2['brcdate'] == '0000-00-00' ? '' : date( 'd M Y', strtotime( $d2['brcdate'] ) );
            $pass_name = $d2['pass_name'] . '<br /><br />' . 'Pax : ' . $d2['num_pax'] . ' | Adult : ' . $d2['num_adult'] . ' | Child : ' . $d2['num_child'] . ' | Infant : ' . $d2['num_infant'] . '<span class="sr-only">' . $d2['num_pax'] . '|' .  $d2['num_adult'] . '|' . $d2['num_child'] . '|' . $d2['num_infant'] . '</span>';
            $pass_name = mb_convert_encoding( $pass_name, 'UTF-8', 'UTF-8' );
            $phone     = mb_convert_encoding( $d2['bbphone'], 'UTF-8', 'UTF-8' );

            if( $d2['pmcode'] != '' && $d2['bdiscountnum'] >= 0 )
            {
                if( $d2['bdiscounttype'] == '0' )
                {
                    $d2['total'] = $d2['total'] - ( ( $d2['total'] * $d2['bdiscountnum'] ) / 100  );
                }
                else
                {
                    $d2['total'] = $d2['total'] - $d2['bdiscountnum'];
                }
            }

            $data[] = array(
                'transport'   => $trans,
                'booked_by'   => $booked,
                'cancel_css'  => $cnccss,
                'brcdate'     => $brcdate,
                'guest_name'  => $pass_name,
                'id'          => $d2['bid'],
                'bdid'        => $d2['bdid'],
                'agid'        => $d2['agid'],
                'ref_code'    => $d2['bcode'],
                'phone'       => $phone,
                'no_ticket'   => $d2['bticket'],
                'bstatus'     => $d2['bdpstatus'],
                'bbemail'     => $d2['bbemail'],
                'bcreason'    => $d2['bcreason'],
                'bcmessage'   => $d2['bcmessage'],
                'rsv_type'    => $d2['bbrevtype'],
                'rsv_status'  => $d2['bdrevstatus'],
                'bonhandtotal'=> $d2['bonhandtotal'],
                'print_sts'   => $d2['bprintboardingstatus'],
                'remarks'     => $d2['bcmessage'] . '<br>' . $d2['bremark'],
                'route'       => $d2['bdfrom'] . ' - ' . $d2['bdto'],
                'bdtype'      => ucwords( $d2['bdtype'] ),
                'ref_agent'   => $d2['bbrevagent'] != '' ? $d2['bbrevagent'] : '-',
                'status'      => $d2['bdstatus'] == 'cn' ? 'Cancelled' : ticket_booking_status( $d2['bdpstatus'] ),
                'total'       => number_format( $d2['total'], 0, ',', '.' ),
                'btotal'      => number_format( $d2['btotal'], 0, ',', '.' ),
                'rev_date'    => date( 'd M Y', strtotime( $d2['bdate'] ) ),
                'dept_date'   => date( 'd M Y', strtotime( $d2['bddate'] ) ),
                'dept_time'   => date( 'H:i', strtotime( $d2['bddeparttime'] ) ),
                'arrive_time' => date( 'H:i', strtotime( $d2['bdarrivetime'] ) ),
                'chanel'      => empty( $d2['agid'] ) ? $d2['chname'] : $d2['agname'],
                'ticket_date' => $d2['bticket'] . '<br/><span style="font-size:10px;">(' . date( 'd M Y', strtotime( $d2['bdate'] ) ) . ')</span>'
            );
        }
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

function ticket_filter_booking_notification()
{
    $filter = array( 'rid' => '', 'lcid' => '', 'lcid2' => '', 'chid' => '', 'status' => array( 'pa', 'ca', 'pp' ), 'bdate' => '', 'bddate' => '', 'rstatus' => array( 'Devinitive' => 'Devinitive', 'Tentative' => 'Tentative' ), 'search' => '', 'bbemail' => '' );

    if( isset( $_GET['prm'] ) )
    {
        extract( json_decode( base64_decode( $_GET['prm'] ), true ) );

        $status  = isset( $bstatus ) ? $bstatus : '';
        $rstatus = isset( $rstatus ) ? $rstatus : '';
        $filter  = array( 'rid' => $rid, 'lcid' => $lcid, 'lcid2' => $lcid2, 'chid' => $chid, 'status' => $status, 'bdate' => $bdate, 'bddate' => $bddate, 'rstatus' => $rstatus, 'search' => $sbooking, 'bbemail' => $bbemail  );
    }

    return $filter;
}

function is_email_booking_notification()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking-notification' && isset( $_GET['prc'] ) && $_GET['prc'] == 'view' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_popup_email_booking_notification()
{
    if( isset( $_GET['sub'] ) && $_GET['sub'] == 'booking-notification' && isset( $_GET['prc'] ) && $_GET['prc'] == 'popup-email' )
    {
        add_actions( 'is_use_ajax', true );

        return true;
    }
    else
    {
        return false;
    }
}

function get_booking_notification_email_content( $content )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/email/booking-notification-to-client.html', 'send-notif-email' );

    add_block( 'email-block', 'sn-block', 'send-notif-email' );

    add_variable( 'email_content' , $content );

    parse_template( 'email-block', 'sn-block' );

    return return_template( 'send-notif-email' );
}

function send_booking_notification( $data, $sender, $subject, $content )
{
    if( empty( $data ) )
    {
        return false;
    }
    else
    {
        try
        {
            $keys = get_meta_data( 'mandrill_api_key', 'ticket_setting' );

            $mail = new Mandrill( $keys );

            $email_from = $sender;
            $email_to   = $data['bbemail'];
            $email_data = get_booking_notification_email_content( $content );

            $message  = array(
                'subject'    => $subject,
                'from_name'  => get_meta_data('web_title'),
                'from_email' => $email_from,
                'html'       => $email_data,
                'to' => array(
                    array(
                        'email' => $email_to,
                        'name'  => '',
                        'type'  => 'to'
                    ),
                    array(
                        'email' => $email_from,
                        'name'  => '',
                        'type'  => 'cc'
                    )
                ),
                'headers' => array( 'Reply-To' => $email_from )
            );

            $async   = false;
            $result  = $mail->messages->send( $message, $async );

            if( isset( $result[0]['status'] ) )
            {
                if( in_array( $result[0]['status'], array( 'rejected', 'invalid' ) ) )
                {
                    return false;
                }
                else
                {
                    update_booking_remark( $data );

                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        catch( Mandrill_Error $e )
        {
            return false;
        }
    }
}

function set_notification_param( $bid )
{
    $data = ticket_booking_all_data( $bid );

    extract( $data['detail'] );

    $param = array();

    //-- Go Trip
    if( isset( $departure ) )
    {
        $departure_route = array();
        $departure_trans = array();
        $departure_date  = array();
        $departure_ptime = array();
        $dep_pickup_time = array();
        $dep_pickup_area = array();

        foreach( $departure as $key => $dp_trip )
        {
            extract( $dp_trip );

            if( $bdpstatus == 'cn' )
            {
                continue;
            }

            $dep_bdto         = $bdto;
            $dep_bdfrom       = $bdfrom;
            $dep_bddate       = date( 'd M Y', strtotime( $bddate ) );
            $dep_bddeparttime = date( 'h:i A', strtotime( $bddeparttime ) );
            $dep_bdarrivetime = date( 'h:i A', strtotime( $bdarrivetime ) );
            $dep_pick_area    = array();
            $dep_pick_time    = array();

            if( !empty( $transport ) )
            {
                foreach( $transport as $ttype => $trans )
                {
                    foreach( $trans as $t )
                    {
                        if( $t['bttrans_type'] != '2' )
                        {
                            if( !empty( $t['taid'] ) )
                            {
                                $tarea  = get_transport_area( $t['taid'] );
                                $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                                $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];
                                $rpto   = $t['btrpto'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpto'] ) );
                                $rpfrom = $t['btrpfrom'] == '00:00:00' ? '' : date( 'H:i', strtotime( $t['btrpfrom'] ) );

                                if( !empty( $rpfrom ) || !empty( $rpto ) )
                                {
                                    if( $ttype == 'pickup' )
                                    {
                                        $dep_pick_time[] = 'In between ' . $rpfrom . ' - ' . $rpto;
                                    }
                                }

                                if( empty( $t['hid'] ) )
                                {
                                    $dep_pick_area[] = $tarea['taname'];
                                }
                                elseif( $t['hid'] == 1 )
                                {
                                    if( empty( $hname ) && empty( $haddr ) )
                                    {
                                        $dep_pick_area[] = 'Hotel to be advised, ' . $tarea['taname'];
                                    }
                                    else
                                    {
                                        $dep_pick_area[] = $hname . ', ' . $tarea['taname'];
                                    }
                                }
                                else
                                {
                                    if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                    {
                                        $dep_pick_area[] = $tarea['taname'];
                                    }
                                    else
                                    {
                                        $dep_pick_area[] = $hname . ', ' . $tarea['taname'];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $departure_route[] = $dep_bddate . ', ' . $dep_bdfrom . ' - ' . $dep_bdto . ', ' . ' ETD ' . $dep_bddeparttime . ' / ETA ' . $dep_bdarrivetime;
            $departure_ptime[] = $bdfrom . ' ETD ' . $dep_bddeparttime;
            $departure_date[]  = $dep_bddate;
        }
        
        $param['departure_route']          = empty( $departure_route ) ? '' : implode( '<br/>', $departure_route );
        $param['departure_point_and_time'] = empty( $departure_ptime ) ? '' : implode( '<br/>', $departure_ptime );
        $param['trip_date']                = empty( $departure_date ) ? '' : implode( '<br/>', $departure_date );
        $param['pickup_area']              = empty( $dep_pick_area ) ? '-' : implode( '<br/>', $dep_pick_area );
        $param['pickup_time']              = empty( $dep_pick_time ) ? '-' : implode( '<br/>', $dep_pick_time );
    }

    //-- Back Trip
    if( isset( $return ) && $data['btype'] == '1' )
    {
        $return_route = array();
        $return_trans = array();

        foreach( $return as $key => $rt_trip )
        {
            extract( $rt_trip );

            if( $bdpstatus == 'cn' )
            {
                continue;
            }

            $rtn_bdto         = $bdto;
            $rtn_bdfrom       = $bdfrom;
            $rtn_bddate       = date( 'd M Y', strtotime( $bddate ) );
            $rtn_bddeparttime = date( 'h:i A', strtotime( $bddeparttime ) );
            $rtn_bdarrivetime = date( 'h:i A', strtotime( $bdarrivetime ) );
            $rtn_drop_area    = array();

            if( !empty( $transport ) )
            {
                foreach( $transport as $ttype => $trans )
                {
                    foreach( $trans as $t )
                    {
                        if( $t['bttrans_type'] != '2' )
                        {
                            if( !empty( $t['taid'] ) )
                            {
                                $tarea  = get_transport_area( $t['taid'] );
                                $hname  = empty( $t['bthotelname'] ) ? '' : $t['bthotelname'];
                                $haddr  = empty( $t['bthoteladdress'] ) ? '' : ' - ' . $t['bthoteladdress'];

                                if( empty( $t['hid'] ) )
                                {
                                    $rtn_drop_area[] = $tarea['taname'];
                                }
                                elseif( $t['hid'] == 1 )
                                {
                                    if( empty( $hname ) && empty( $haddr ) )
                                    {
                                        $rtn_drop_area[] = 'Hotel to be advised, ' . $tarea['taname'];
                                    }
                                    else
                                    {
                                        $rtn_drop_area[] = $hname . ', ' . $tarea['taname'];
                                    }
                                }
                                else
                                {
                                    if( empty( $t['bthotelname'] ) && empty( $t['bthoteladdress'] ) )
                                    {
                                        $rtn_drop_area[] = $tarea['taname'];
                                    }
                                    else
                                    {
                                        $rtn_drop_area[] = $hname . ', ' . $tarea['taname'];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $return_route[] = $rtn_bddate . ', ' . $rtn_bdfrom . ' - ' . $rtn_bdto . ', ' . ' ETD ' . $rtn_bddeparttime . ' / ETA ' . $rtn_bdarrivetime;
        }

        $param['return_route'] = empty( $return_route ) ? '' : implode( '<br/>', $return_route );
        $param['dropoff_area'] = empty( $rtn_drop_area ) ? '-' : implode( '<br/>', $rtn_drop_area );
    }
    else
    {
        $param['return_route'] = '';
        $param['dropoff_area'] = '';
    }

    $dep_pdata = get_pass_name_and_num_of_pass( $data['detail'] );

    $param['booking_ticket'] = $data['bticket'];
    $param['customer_name']  = $dep_pdata['pass_name'][0];
    $param['passanger_name'] = implode( ', ', $dep_pdata['pass_name'] );
    $param['num_of_person']  = implode( ', ', $dep_pdata['num_of_person'] );
    $param['users']          = get_display_name( $_COOKIE['user_id'] );

    return $param;
}

function set_notification_form( $bid = array() )
{
    if( count( $bid ) > 1 )
    {
        $subject_1 = get_meta_data( 'reschedule_email_multi_guest_subject', 'ticket_setting' );
        $subject_2 = get_meta_data( 'force_majeure_email_multi_guest_subject', 'ticket_setting' );
        $subject_3 = get_meta_data( 'cancellation_email_multi_guest_subject', 'ticket_setting' );

        $template_1 = get_meta_data( 'reschedule_email_multi_guest_content', 'ticket_setting' );
        $template_2 = get_meta_data( 'force_majeure_email_multi_guest_content', 'ticket_setting' );
        $template_3 = get_meta_data( 'cancellation_email_multi_guest_content', 'ticket_setting' );

        return array(
            'subject1'  => $subject_1,
            'subject2'  => $subject_2,
            'subject3'  => $subject_3,
            'template1' => $template_1,
            'template2' => $template_2,
            'template3' => $template_3
        );
    }
    else
    {
        $param = set_notification_param( $bid[0] );

        $subject_1 = get_meta_data( 'reschedule_email_single_guest_subject', 'ticket_setting' );
        $subject_2 = get_meta_data( 'force_majeure_email_single_guest_subject', 'ticket_setting' );
        $subject_3 = get_meta_data( 'cancellation_email_single_guest_subject', 'ticket_setting' );

        $template_1 = get_meta_data( 'reschedule_email_single_guest_content', 'ticket_setting' );
        $template_2 = get_meta_data( 'force_majeure_email_single_guest_content', 'ticket_setting' );
        $template_3 = get_meta_data( 'cancellation_email_single_guest_content', 'ticket_setting' );

        if( !empty( $subject_1 ) )
        {
            preg_match_all( '/\[([a-zA-Z0-9-_]+?)\]/', $subject_1, $output );
        
            foreach( $output[1] as $obj )
            {
                $subject_1 = str_replace( '[' . $obj . ']', run_email_shortcode( $obj, $param ), $subject_1 );
            }
        }

        if( !empty( $subject_2 ) )
        {
            preg_match_all( '/\[([a-zA-Z0-9-_]+?)\]/', $subject_2, $output );
        
            foreach( $output[1] as $obj )
            {
                $subject_2 = str_replace( '[' . $obj . ']', run_email_shortcode( $obj, $param ), $subject_2 );
            }
        }

        if( !empty( $subject_3 ) )
        {
            preg_match_all( '/\[([a-zA-Z0-9-_]+?)\]/', $subject_3, $output );
        
            foreach( $output[1] as $obj )
            {
                $subject_3 = str_replace( '[' . $obj . ']', run_email_shortcode( $obj, $param ), $subject_3 );
            }
        }

        if( !empty( $template_1 ) )
        {
            preg_match_all( '/\[([a-zA-Z0-9-_]+?)\]/', $template_1, $output );
        
            foreach( $output[1] as $obj )
            {
                $template_1 = str_replace( '[' . $obj . ']', run_email_shortcode( $obj, $param ), $template_1 );
            }
        }

        if( !empty( $template_2 ) )
        {
            preg_match_all( '/\[([a-zA-Z0-9-_]+?)\]/', $template_2, $output );
        
            foreach( $output[1] as $obj )
            {
                $template_2 = str_replace( '[' . $obj . ']', run_email_shortcode( $obj, $param ), $template_2 );
            }
        }

        if( !empty( $template_3 ) )
        {
            preg_match_all( '/\[([a-zA-Z0-9-_]+?)\]/', $template_3, $output );
        
            foreach( $output[1] as $obj )
            {
                $template_3 = str_replace( '[' . $obj . ']', run_email_shortcode( $obj, $param ), $template_3 );
            }
        }

        return array(
            'subject1'  => $subject_1,
            'subject2'  => $subject_2, 
            'subject3'  => $subject_3,
            'template1' => $template_1,
            'template2' => $template_2, 
            'template3' => $template_3
        );
    }
}

function update_booking_remark( $data )
{
    global $db;

    extract( $data );

    if( empty( $bremark ) )
    {
        $bremark = 'Notif Sent!';
    }
    else
    {
        $bremark = $bremark . "\n\n" . 'Notif Sent!';
    }

    $s = 'UPDATE ticket_booking AS a SET a.bremark = %s WHERE a.bid = %d';
    $q = $db->prepare_query( $s, $bremark, $bid );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ticket_booking_notification_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        extract( $_POST );

        $data = ticket_booking_notification_table_query( $chid, $lcid, $lcid2, $rid, $status, $bddate, $sbooking, $bdate, $bbemail, $rstatus );

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'set-notification-form' )
    {
        $data = set_notification_form( $_POST['bid'] );

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'send-notification' )
    {
        $data = ticket_booking_all_data( $_POST['bid'] );

        if( send_booking_notification( $data, $_POST['sender'], $_POST['subject'], $_POST['message'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    exit;
}