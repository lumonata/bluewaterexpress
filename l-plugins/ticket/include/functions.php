<?php

add_actions( 'ticket-admin-ajax_page', 'ticket_admin_ajax' );

function ticket_dashboard_memo()
{
	global $db;

	$s = 'SELECT * FROM ticket_notification WHERE nstatus = %d ORDER BY ndate DESC LIMIT 3';
	$q = $db->prepare_query( $s, 0 );
	$r = $db->do_query( $q );

	if( $db->num_rows( $r ) > 0 )
	{		
		$site_url = site_url();
		$content  = '
		<div class="memo-wrapp">
			<h4>Memo</h4>';

			while( $d = $db->fetch_array( $r ) )
			{			
				$content .= '
				<div class="memo">
					<a class="close" data-id="' . $d['nid'] . '" data-ajax-link="' . HTSERVER . $site_url . '/ticket-notification-ajax/">X</a>
					<p>
						<strong>';
						if( empty( $d['bid'] ) )
						{
							$content .= $d['ntitle'];
						}
						else
						{
							$content .= '<a href="' . HTSERVER . $site_url . '/l-admin/?state=reservation&sub=booking&prc=view&id=' . $d['bid'] . '">' . $d['ntitle'] . '</a>';
						}
						$content .= '
						</strong>
					</p>
					' . $d['ncontent'] . '
				</div>';
			}

			$content .= '
			<div class="text-right">
				<a class="show-all" href="' . get_state_url( 'notification' ) . '">Show All</a>
			</div>
		</div>';

		return $content;
	}
}

function save_ticket_notification( $bid, $ntitle, $ncontent, $nstatus, $ntype, $ndate )
{
	global $db;

	$s = 'INSERT INTO ticket_notification( bid, ntitle, ncontent, nstatus, ntype, ndate) VALUES( %d, %s, %s, %s, %s, %s )';
    $q = $db->prepare_query( $s, $bid, $ntitle, $ncontent, 0, 0, date( 'Y-m-d H:i:s' ) );

    $db->do_query( $q );
}

function update_ticket_notification( $nid, $nstatus )
{
	global $db;

	$s = 'UPDATE ticket_notification SET nstatus = %s WHERE nid = %d';
    $q = $db->prepare_query( $s, $nstatus, $nid );

    return $db->do_query( $q );
}

function ticket_admin_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }
}

function ticket_template_dir(){
	return 'ticket/tpl';
}
function check_date_in_range($start_date, $end_date, $date_from_user){
  // Convert to timestamp
  $start_ts = strtotime($start_date);
  $end_ts = strtotime($end_date);
  $user_ts = strtotime($date_from_user);

  // Check that user date is between start & end
  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}
function _is_get_special_rate($rid,$sid,$date){
	global $db; 
	$q = $db->prepare_query("SELECT * from ticket_special_rate
		WHERE %d>=srdate_start AND %d<=srdate_end AND status=%d",
		strtotime($date),strtotime($date),1
		); 
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if ($n==0){
		return false;
	}else{
		$a_rid = array();
		$a_sid = array();
		$a_srid= array();
		while ($d=$db->fetch_array($r)){
			array_push($a_rid, $d['rid']);
			array_push($a_sid, $d['sid']);
			array_push($a_srid, $d['srid']);
		}
		
		if (in_array($rid, $a_rid) &&  in_array($sid,$a_sid)){ 		//apply to selected route && selected schedule						[****]
			return true;
		}elseif (in_array($rid, $a_rid) &&  in_array('0',$a_sid)){ 	//apply to selected route && all schedule of the selected route		[***]
			return true;
		}elseif (in_array('0', $a_rid) &&  in_array($sid,$a_sid)){ 	//apply to all routes && selected schedule 							[**]
			return true;
		}elseif (in_array('0', $a_rid) && in_array('0',$a_sid)){ 	//apply to all routes && all schedules 								[*]
			return true;
		}else{
			return false;
		}
	}
	return false;
}
function is_get_special_rate($rid,$sid,$date){
	global $db; 
	//apply to selected route && selected schedule							[rid!=0,sid!=0]		[****]
	$q1 = $db->prepare_query("SELECT * from ticket_special_rate WHERE 
			%d>=srdate_start AND %d<=srdate_end AND status=%d AND
			rid=%d AND sid=%d 
			ORDER BY sort_id ASC limit %d",
			strtotime($date),strtotime($date),1,
			$rid,$sid,
			1
			); 
	//apply to selected route && all schedule of the selected route			[rid!=0,sid==0]		[***]
	$q2 = $db->prepare_query("SELECT * from ticket_special_rate WHERE 
			%d>=srdate_start AND %d<=srdate_end AND status=%d AND
			rid=%d AND sid=%d 
			ORDER BY sort_id ASC limit %d",
			strtotime($date),strtotime($date),1,
			$rid,0,
			1
			);
	//apply to all routes && selected schedule 								[rid==0,sid!=0]		[**] 
	$q3 = $db->prepare_query("SELECT * from ticket_special_rate WHERE 
			%d>=srdate_start AND %d<=srdate_end AND status=%d AND
			rid=%d AND sid=%d 
			ORDER BY sort_id ASC limit %d",
			strtotime($date),strtotime($date),1,
			0,$sid,
			1
			); 
	//apply to all routes && all schedules 									[rid==0,sid==0]		[*]
	$q4 = $db->prepare_query("SELECT * from ticket_special_rate WHERE 
			%d>=srdate_start AND %d<=srdate_end AND status=%d AND
			rid=%d AND sid=%d 
			ORDER BY sort_id ASC limit %d",
			strtotime($date),strtotime($date),1,
			0,0,
			1
			);
	
	if (ticket_is_num_rows($q1)){
		return $q1;
	}elseif (ticket_is_num_rows($q2)){
		return $q2;
	}elseif (ticket_is_num_rows($q3)){
		return $q3;
	}elseif (ticket_is_num_rows($q4)){
		return $q4;
	}else{
		return false;
	}
	return false;
}
function ticket_is_num_rows($sql){
	global $db;
	$r = $db->do_query($sql);
	$n = $db->num_rows($r);
	if ($n==0){
		return false;
	}else{
		return true;
	}
}
function ticket_get_data_rows($sql){
	global $db;
	$r = $db->do_query($sql);
	return $db->fetch_array($r);
}
function _ticket_over_quota($sid,$date,$num_adult,$num_child=0,$num_infant=0){
	global $db;
	$allotment 	= ticket_get_allotment($sid);
	$num_book	= ticket_calendar_num_booking_per_schedule($sid, $date);
	$rest 		= $allotment - $num_book;
	if ($rest<($num_adult+$num_child)){
		return true;
	}else{
		return false;
	}	
}
function is_ticket_over_quota($rid,$sid,$date,$num_adult,$num_child=0,$num_infant=0){	
	$ticket 	= new ticketAvailability($rid, $sid, $date, $num_adult, $num_child);
	if ($ticket->is_available()){
		return false;
	}else{
		return true;
	}	
}

function ticket_get_port_option($selected_name=''){ 
	global $db;
	$html = '';
	$q = $db->prepare_query("SELECT * from ticket_master_port order by sort_id ASC");
	$r = $db->do_query($q);
	while ($d=$db->fetch_array($r)){
		$selected = '';
		if ( strtolower($d['pname']) == strtolower($selected_name) ){
			$selected = 'selected';
		}		
		$html .='<option value="'.$d['pname'].'" '.$selected.'>'.$d['pname'].'</option>';
	}
	return $html;
}
function ticket_schedule_parent_option($selected_id=''){ 
	$schedule_parent = array('Departure Sunrise','Departure Sunset');
	$html = '';
	for ($i=0;$i<count($schedule_parent);$i++){
		$selected = '';
		if ("$i"=="$selected_id"){
			$selected = 'selected';			
		}
		$html .='<option value="'.$i.'" '.$selected.'>'.$schedule_parent[$i].'</option>';
	}
	return $html;
}
function ticket_route_option($selected_id=''){
	global $db;
	$html = '';
	$q = $db->prepare_query("SELECT * from ticket_route order by sort_id ASC");
	$r = $db->do_query($q);
	while ($d=$db->fetch_array($r)){
		$selected = '';
		if ($d['rid']==$selected_id){
			$selected = 'selected';
		}
		$html .='<option value="'.$d['rid'].'" '.$selected.'>'.$d['rname'].'</option>';
	}
	return $html;
}
function ticket_schedule_option($selected_id='',$route_id=''){
	global $db;
	
	$html = '';	
	$q = $db->prepare_query("SELECT * from ticket_schedule order by sort_id ASC");
	$r = $db->do_query($q);
	while ($d=$db->fetch_array($r)){
		$selected = '';
		$style= 'display:none;';
		if ($d['sid']==$selected_id){
			$selected = 'selected';
			$style = 'display:block;';
		}elseif ($d['rid']==$route_id){
			$style = 'display:block;';
		}
		$html .='<option  style="'.$style.'" rel="'.$d['rid'].'" value="'.$d['sid'].'" '.$selected.'>'.$d['sname'].'</option>';
	}
	return $html;
}
function ticket_get_schedule_option($selected_id=''){
	global $db;
	
	$html = '';	
	$q = $db->prepare_query("SELECT * from ticket_schedule order by sort_id ASC");
	$r = $db->do_query($q);
	while ($d=$db->fetch_array($r)){
		$selected = '';		
		if ($d['sid']==$selected_id){
			$selected = 'selected';			
		}
		$html .='<option rel="'.$d['rid'].'" value="'.$d['sid'].'" '.$selected.'>'.$d['sname'].'</option>';
	}
	return $html;
}
function ticket_special_rate_apply_to($route_id,$schedule_id){
	$html = '';
	if (($route_id==0) && ($schedule_id==0)){
		$html .= 'All routes<br>';
		$html .= 'All schedules';
		return $html;
	}
	
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_route WHERE rid=%d",$route_id);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	$html .= $d['rname'].'<br>';
	
	$q = $db->prepare_query("SELECT * from ticket_schedule WHERE sid=%d",$schedule_id);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	$html .= $d['sname'];
	return $html;
}
function generate_ticket_no($type=0){
	//No ticket : [type][year][month][date][no-urut], ex : [0][2013][01][20][9999]= 0201301209999
	global $db;
	$pre	= $type.date('Y').date('m').date('d');	 
	$q		= $db->prepare_query("SELECT MAX(substring(no_ticket,10,4)) as current_no FROM ticket_booking WHERE substring(no_ticket,1,9)=%d",$pre);
	$r		= $db->do_query($q);
	$d 		= $db->fetch_array($r);
	if ($d['current_no']==NULL){
		$no_urut = 1;
	}else{		
		$no_urut = intval ($d['current_no']) + 1;		
	}	
	
	if ($no_urut>9999){
		$no_urut = 1;
	}elseif ($no_urut<10){
		$no_urut = '000'.$no_urut;
	}elseif ($no_urut<100){
		$no_urut = '00'.$no_urut;
	}elseif ($no_urut<1000){
		$no_urut = '0'.$no_urut;	
	}else{
		$no_urut = $no_urut;
	}
	
	$no_ticket = $pre.$no_urut;
	return $no_ticket;
}
function get_next_increment_id($tb){
	global $db;
	$q = $db->prepare_query("select auto_increment from information_schema.TABLES where TABLE_NAME ='".$tb."' and TABLE_SCHEMA='".DBNAME."'");
 	$r = $db->do_query($q);
 	$d = $db->fetch_array($r);
 	$id = $d['auto_increment']; 	
 	return $id;
}
function ticket_setting_get_value($sef){
	global $db;
	$q = $db->prepare_query("select value from ticket_setting WHERE sef=%s",$sef);
 	$r = $db->do_query($q);
 	$d = $db->fetch_array($r);
 	
 	return $d['value']; 	
}
function ticket_get_allotment($sid){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_schedule WHERE sid=%d",$sid);
 	$r = $db->do_query($q);
 	$d = $db->fetch_array($r);
 	if ($d['sallotment']==0){
 		return ticket_get_allotment_from_route($d['rid']);
 	}else{
 		return $d['sallotment'];
 	}
}
function ticket_get_allotment_from_route($rid){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_route WHERE rid=%d",$rid);
 	$r = $db->do_query($q);
 	$d = $db->fetch_array($r);
 	return $d['rallotment'];
}
function ticket_get_booking($id){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_booking WHERE bid=%d",$id);
 	$r = $db->do_query($q);
 	$d = $db->fetch_array($r);
 	return $d;
}
function ticket_get_booking_id_by_no_ticket($no_ticket){
	global $db;
	$q = $db->prepare_query("SELECT bid from ticket_booking WHERE no_ticket=%s",$no_ticket);
 	$r = $db->do_query($q);
 	$d = $db->fetch_array($r);
 	return $d['bid'];
}
function ticket_save_payment_info($bid,$no_ticket,$status,$total,$to_rek,$metode,$frm_bank,$note){
	return true;
}
function ticket_email_paypal_payment_confirmation($bid,$no_ticket,$post){	
	$email_admin = get_meta_data('email');
	$web_name	 = get_meta_data('web_name');	
	
	ini_set("SMTP",SMTP_SERVER);
	ini_set("sendmail_from",$email_admin);
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";				
	$headers .= "From:  ".$web_name." <automail@pacha-express.com>\r\nReply-To :'automail@pacha-express.com'\r\n";	
	
	//paypal var	
    $item_name 			= $_POST['item_name'];
    $item_number 		= $_POST['item_number'];
    $payment_status 	= $_POST['payment_status'];
    $payment_amount 	= $_POST['mc_gross'];
    $payment_currency 	= $_POST['mc_currency'];
    $txn_id 			= $_POST['txn_id'];
    $receiver_email 	= $_POST['receiver_email'];
    $payer_email 		= $_POST['payer_email'];	
	
	//sen email to admin
	$to		 = $email_admin;
	$subject = "Payment confirmation for ticket no : ".$no_ticket;		
	$message = '<table>
					<tr><td>Ticket No</td><td>: '.$no_ticket.'</td></tr>
					<tr><td>Payment amount</td><td>: '.$payment_currency.' '.$payment_amount.'</td></tr>					
					<tr><td>Paid to</td><td>: '.$receiver_email.'</td></tr>
					<tr><td>Payment status</td><td>: '.$payment_status.'</td></tr>
				</table>	
				';
	
	$email_to_admin =mail($to,$subject,$message,$headers);
	
	//sen email to client
	$booking 	= ticket_get_booking($bid);
	$booking_by = ticket_get_booking_by($booking['booked_by']);
	
	$to		 = $booking_by['email'];
	$subject = "Payment confirmation for ticket no : ".$no_ticket;		
	$message = '<table>
					<tr><td>Ticket No</td><td>: '.$no_ticket.'</td></tr>
					<tr><td>Payment amount</td><td>: '.$payment_currency.' '.$payment_amount.'</td></tr>					
					<tr><td>Paid to</td><td>: '.$receiver_email.'</td></tr>
					<tr><td>Payment status</td><td>: '.$payment_status.'</td></tr>
				</table>	
				';
	$message .= '<br>Your payment has accepted succesfully.Thank you for booking ticket with us.';
	$message .= '<br><br>Best regards, <br><strong>'.$web_name.'</strong> ';
		
	if ($email_to_admin){
		$email_to_client=mail($to,$subject,$message,$headers);
		if (!$email_to_admin){
			return 'send email to client failed';
		}
	}else{
		return 'send email failed';
	}
	
}

function get_detail_booking($id){
	global $db;
	
	$booking   = ticket_get_booking($id);
	$booked_by = ticket_get_booking_by($booking['booked_by']);
	
	$trip_type = $booking['type']=='0' ? 'One way' : 'Return trip'; 
	$booked_data = array(
		'booking_id'=>$booking['bid'],
		'booking_by'=>$booking['booked_by'],
		'booking_date'=>date('d M Y',$booking['created_date']),
		'no_ticket'=>$booking['no_ticket'],
		'boat'=>$booking['boat'],
		'num_passenger'=>$booking['num_passenger'],
		'total'=>$booking['total'],
		'status'=>$booking['status'],
		'type'=>$booking['type'],
		'trip_type'=>$trip_type,
		'client_detail'=>array(
			'name'=>$booked_by['title'].' '.$booked_by['fname'].' '.$booked_by['lname'],
			'address'=>$booked_by['address'],
			'country'=>$booked_by['country'],
			'phone'=>$booked_by['phone'],
			'email'=>$booked_by['email'])
	);
	
	$s = 'SELECT * FROM ticket_booking_passenger WHERE bid="'.$id.'"';
	$r = $db->do_query($s);
	while ($d=$db->fetch_array($r)){
		$booked_data['guest_detail'][] = array(
			'guest_name'=>$d['fname'],
			'guest_type'=>$d['type'],
			'guest_country'=>$d['country']
		);
	}
	
	$s = 'SELECT * FROM ticket_booking_detail WHERE bid="'.$id.'" ORDER BY bdid ASC';
	$r = $db->do_query($s);
	while ($d=$db->fetch_array($r)){
		$booked_data['booking_detail'][] = array(
			'from'=>$d['rfrom'],
			'to'=>$d['rto'],
			'type'=>$d['bdtype'],
			'date'=>$d['date'],
			'departure'=>$d['stime_departure'],
			'arrive'=>$d['stime_arrive'],
		);
		
		if($d['bdtype']=='departure'){
			$booked_data['from'] = $d['rfrom'];
			$booked_data['to'] = $d['rto'];
			$booked_data['num_adult'] = $d['num_adult'];
			$booked_data['num_child'] = $d['num_child'];
			$booked_data['num_infant'] = $d['num_infant'];
			$booked_data['price_per_adult'] = $d['price_per_adult'];
			$booked_data['price_per_child'] = $d['price_per_child'];
			$booked_data['price_per_infant'] = $d['price_per_infant'];
			$booked_data['price_total_adult'] = $d['total_adult'];
			$booked_data['price_total_child'] = $d['total_child'];
			$booked_data['price_total_infant'] = $d['total_infant'];
			$booked_data['pickup_address'] = $d['address_pickup'];
			$booked_data['transfer_address'] = $d['address_transfer'];
			$booked_data['pickup_destination'] = $d['pickup_destination'];
			$booked_data['transfer_destination'] = $d['transfer_destination'];
			$booked_data['pickup_cost'] = $d['pickup_cost'];
			$booked_data['transfer_cost'] = $d['transfer_cost'];
			$booked_data['departure'] = $d['date'];
			$booked_data['subtotal'] = $d['total'];
		};	
		
		if($d['bdtype']=='return'){
			$booked_data['return'] = $d['date'];
		}
	}
	
	return $booked_data;
}

function get_destination_by_id($id){
	global $db;
	
	$s = 'SELECT * FROM ticket_pick_trans_rate WHERE ptid="'.$id.'"';
	$r = $db->do_query($s);
	$n = $db->num_rows($r);
	
	$result = array();
	if($n > 0){
		while($d=$db->fetch_array($r)){
			$result = array(
				'destination_name'=>$d['destination_name'],
				'adult_rate'=>$d['adult_rate'],
				'child_rate'=>$d['child_rate'],
				'created_by'=>$d['created_by'],
				'created_date'=>$d['created_date']
			);
		}	
	}
	
	return $result;
}
?>