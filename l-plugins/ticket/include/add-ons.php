<?php

add_actions( 'add-ons', 'ticket_add_ons' );
add_actions( 'ticket-add-ons-ajax_page', 'ticket_add_ons_ajax' );

function ticket_add_ons()
{
    $is_access = is_access( $_COOKIE['user_id'], $_GET['state'], $_GET['sub'] );

    if( is_num_add_ons() == 0 && !isset( $_GET['prc'] ) && $is_access )
    {
        header( 'location:' . get_state_url( 'schedules&sub=add-ons&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        if( $is_access ) 
        {
            return ticket_add_new_add_ons();
        } 
        else 
        {
            return not_found_template();
        }
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_add_ons( $_GET['id'] ) && $is_access )
        {
            return ticket_edit_add_ons();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        if ( $is_access ) 
        {
            return ticket_batch_delete_add_ons();
        } 
        else 
        {
            return not_found_template();
        }
    }
    elseif( is_confirm_delete() && $is_access )
    {
        foreach( $_POST['id'] as $key => $val )
        {
            delete_add_ons( $val );
        }
    }

    return ticket_add_ons_table();
}

/*
| -------------------------------------------------------------------------------------
| Add-ons Table List
| -------------------------------------------------------------------------------------
*/
function ticket_add_ons_table()
{
	global $db;

    $s = 'SELECT * FROM ticket_add_ons ORDER BY aoid DESC';
    $q = $db->prepare_query( $s );
	$r = $db->do_query( $q );

    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/add-ons/list.html', 'add-ons' );

    add_block( 'list-block', 'lcblock', 'add-ons' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'schedules&sub=add-ons' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-add-ons-ajax/' );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=add-ons&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'schedules&sub=add-ons&prc=edit' ) );
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Add-ons List' );

	add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'add-ons' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Add-ons
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_add_ons()
{
	run_save_add_ons();

    $site_url = site_url();
    $data     = get_add_ons();
    $aoend    = date( 'd/m/Y', ( empty( $data['aoend'] ) ? time() : strtotime( $data['aoend'] ) ) );
    $aostart  = date( 'd/m/Y', ( empty( $data['aostart'] ) ? time() : strtotime( $data['aostart'] ) ) );

	set_template( PLUGINS_PATH . '/ticket/tpl/add-ons/form.html', 'add-ons' );
    add_block( 'form-block', 'lcblock', 'add-ons' );

    add_variable( 'aoid', $data['aoid'] );
    add_variable( 'aoname', $data['aoname'] );
    add_variable( 'aodesc', $data['aodesc'] );
    add_variable( 'aobrief', $data['aobrief'] );
    add_variable( 'aoprice', $data['aoprice'] );
    add_variable( 'aodiscount', $data['aodiscount'] );
    add_variable( 'aopicture', get_add_ons_featured_image_content( $data['aopicture'] ) );

    add_variable( 'aosname', $data['aosname'] );
    add_variable( 'aostelp', $data['aostelp'] );
    add_variable( 'aosemail', $data['aosemail'] );
    add_variable( 'aoperiod', $aostart . ' - ' . $aoend );

    add_variable( 'aotype', get_add_ons_type_option( $data['aotype'] ) );
    add_variable( 'aosid', get_add_ons_schedule_option( $data['aosid'] ) );
    add_variable( 'aostatus', get_add_ons_status_option( $data['aostatus'] ) );
    add_variable( 'aoarrive', get_add_ons_arrive_point_option( $data['aoarrive'] ) );
    add_variable( 'aodepart', get_add_ons_depart_point_option( $data['aodepart'] ) );
    add_variable( 'aobsource', get_add_ons_booking_source_option( $data['aobsource'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'schedules&sub=add-ons&prc=add_new' ) );
    add_variable( 'site_link', HTSERVER . $site_url );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-add-ons-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=add-ons' ) );
    add_variable( 'delete_class', 'sr-only' );
    
    add_actions( 'section_title', 'New Add-ons' );

    add_actions( 'other_elements', 'get_javascript', 'dropzone.js' );
    add_actions( 'other_elements', 'get_javascript_inc', 'tiny_mce/tinymce.min.js' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/momentjs/latest/moment.min.js' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' );

    add_actions( 'header_elements', 'get_css', 'dropzone.css' );
    add_actions( 'header_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'form-block', 'lcblock', false );

    return return_template( 'add-ons' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Add-ons
| -------------------------------------------------------------------------------------
*/
function ticket_edit_add_ons()
{
    run_update_add_ons();

    $site_url = site_url();
    $data     = get_add_ons( $_GET['id'] );
    $aoend    = date( 'd/m/Y', ( empty( $data['aoend'] ) ? time() : strtotime( $data['aoend'] ) ) );
    $aostart  = date( 'd/m/Y', ( empty( $data['aostart'] ) ? time() : strtotime( $data['aostart'] ) ) );

    set_template( PLUGINS_PATH . '/ticket/tpl/add-ons/form.html', 'add-ons' );
    add_block( 'form-block', 'lcblock', 'add-ons' );

    add_variable( 'aoid', $data['aoid'] );
    add_variable( 'aoname', $data['aoname'] );
    add_variable( 'aodesc', $data['aodesc'] );
    add_variable( 'aobrief', $data['aobrief'] );
    add_variable( 'aoprice', $data['aoprice'] );
    add_variable( 'aodiscount', $data['aodiscount'] );
    add_variable( 'aopicture', get_add_ons_featured_image_content( $data['aopicture'] ) );

    add_variable( 'aosname', $data['aosname'] );
    add_variable( 'aostelp', $data['aostelp'] );
    add_variable( 'aosemail', $data['aosemail'] );
    add_variable( 'aoperiod', $aostart . ' - ' . $aoend );

    add_variable( 'aotype', get_add_ons_type_option( $data['aotype'] ) );
    add_variable( 'aosid', get_add_ons_schedule_option( $data['aosid'] ) );
    add_variable( 'aostatus', get_add_ons_status_option( $data['aostatus'] ) );
    add_variable( 'aoarrive', get_add_ons_arrive_point_option( $data['aoarrive'] ) );
    add_variable( 'aodepart', get_add_ons_depart_point_option( $data['aodepart'] ) );
    add_variable( 'aobsource', get_add_ons_booking_source_option( $data['aobsource'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'schedules&sub=add-ons&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'site_link', HTSERVER . $site_url );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-add-ons-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=add-ons' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'add-ons' ) );
    add_variable( 'delete_class', '' );

    add_variable( 'sub', $_GET['sub'] );
    add_variable( 'state', $_GET['state'] );
    
    add_actions( 'section_title', 'Edit Add-ons' );
    
    add_actions( 'other_elements', 'get_javascript', 'dropzone.js' );
    add_actions( 'other_elements', 'get_javascript_inc', 'tiny_mce/tinymce.min.js' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/momentjs/latest/moment.min.js' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' );
    
    add_actions( 'header_elements', 'get_css', 'dropzone.css' );
    add_actions( 'header_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'form-block', 'lcblock', false );

    return return_template( 'add-ons' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Add-ons
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_add_ons()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/add-ons/batch-delete.html', 'add-ons' );
    add_block( 'loop-block', 'lclblock', 'add-ons' );
    add_block( 'delete-block', 'lcblock', 'add-ons' );

    foreach( $_POST['select'] as $key => $val )
    {
        $d = get_add_ons( $val );

        add_variable( 'aoname',  $d['aoname'] );
        add_variable( 'aoid', $d['aoid'] );

        parse_template( 'loop-block', 'lclblock', true );
    }

    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' add-ons? :' );
    add_variable( 'action', get_state_url('schedules&sub=add-ons' ));

    parse_template( 'delete-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Delete add-ons' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'add-ons' );
}

function run_save_add_ons()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_add_ons_data();

        if( empty( $error ) )
        {
            $post_id = save_add_ons();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new add-ons' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New add-ons successfully saved' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=add-ons&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_add_ons()
{
    global $flash;
    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_add_ons_data();

        if( empty( $error ) )
        {
            $post_id = update_add_ons();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this add-ons' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This add-ons successfully edited' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=add-ons&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_add_ons_data()
{
    $error = array();

    if( isset($_POST['aoname']) && empty( $_POST['aoname'] ) )
    {
        $error[] = 'add-ons name can\'t be empty';
    }

    if( isset($_POST['aoperiod']) && empty( $_POST['aoperiod'] ) )
    {
        $error[] = 'add-ons period can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons List Count
| -------------------------------------------------------------------------------------
*/
function is_num_add_ons()
{
    global $db;

    $s = 'SELECT * FROM ticket_add_ons';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Add-ons Table Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_add_ons_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array( 
        1  => 'a.aoname',
        2  => 'CONCAT( DATE_FORMAT( a.aostart, "%d %M %Y" ), " - ", DATE_FORMAT( a.aoend, "%d %M %Y" ) )',
        3  => 'IF( a.aostatus = "1", "Aktif", "Tidak Aktif" )'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.aoid DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT 
                a.aoid, 
                a.aoname,
                IF( a.aostatus = "1", "Aktif", "Tidak Aktif" ) AS aostatus,
                CONCAT( DATE_FORMAT( a.aostart, "%d %M %Y" ), " - ", DATE_FORMAT( a.aoend, "%d %M %Y" ) ) AS aoperiod
              FROM ticket_add_ons AS a ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT 
                a.aoid, 
                a.aoname,
                IF( a.aostatus = "1", "Aktif", "Tidak Aktif" ) AS aostatus,
                CONCAT( DATE_FORMAT( a.aostart, "%d %M %Y" ), " - ", DATE_FORMAT( a.aoend, "%d %M %Y" ) ) AS aoperiod
              FROM ticket_add_ons AS a WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'aoid'      => $d2['aoid'],
                'aoname'    => $d2['aoname'],
                'aostatus'  => $d2['aostatus'],
                'aoperiod'  => $d2['aoperiod'],
                'edit_link' => get_state_url( 'schedules&sub=add-ons&prc=edit&id=' . $d2['aoid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons By ID
| -------------------------------------------------------------------------------------
*/
function get_add_ons( $id='', $field = '' )
{
    global $db;

    $data = array( 
        'aoid'       => ( isset( $_POST['aoid'] ) ? $_POST['aoid'] : null ),
        'aotype'     => ( isset( $_POST['aotype'] ) ? $_POST['aotype'] : 0 ),        
        'aoname'     => ( isset( $_POST['aoname'] ) ? $_POST['aoname'] : '' ),
        'aodesc'     => ( isset( $_POST['aodesc'] ) ? $_POST['aodesc'] : '' ),
        'aobrief'    => ( isset( $_POST['aobrief'] ) ? $_POST['aobrief'] : '' ),
        'aostart'    => ( isset( $_POST['aostart'] ) ? $_POST['aostart'] : '' ),
        'aoend'      => ( isset( $_POST['aoend'] ) ? $_POST['aoend'] : '' ),
        'aosid'      => ( isset( $_POST['aosid'] ) ? $_POST['aosid'] : '' ),
        'aoprice'    => ( isset( $_POST['aoprice'] ) ? $_POST['aoprice'] : 0 ),
        'aodiscount' => ( isset( $_POST['aodiscount'] ) ? $_POST['aodiscount'] : 0 ),
        'aobsource'  => ( isset( $_POST['aobsource'] ) ? $_POST['aobsource'] : '' ),
        'aodepart'   => ( isset( $_POST['aodepart'] ) ? $_POST['aodepart'] : '' ),
        'aoarrive'   => ( isset( $_POST['aoarrive'] ) ? $_POST['aoarrive'] : '' ),
        'aosname'    => ( isset( $_POST['aosname'] ) ? $_POST['aosname'] : '' ),
        'aostelp'    => ( isset( $_POST['aostelp'] ) ? $_POST['aostelp'] : '' ),
        'aosemail'   => ( isset( $_POST['aosemail'] ) ? $_POST['aosemail'] : '' ),
        'aopicture'  => ( isset( $_POST['aopicture'] ) ? $_POST['aopicture'] : '' ),
        'aostatus'   => ( isset( $_POST['aostatus'] ) ? $_POST['aostatus'] : '' )
    );

    $s = 'SELECT * FROM ticket_add_ons WHERE aoid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'aoid'       => $d['aoid'], 
                'aotype'     => $d['aotype'], 
                'aoname'     => $d['aoname'],
                'aodesc'     => $d['aodesc'],
                'aobrief'    => $d['aobrief'],
                'aostart'    => $d['aostart'],
                'aoend'      => $d['aoend'],
                'aosid'      => $d['aosid'],
                'aoprice'    => $d['aoprice'],
                'aodiscount' => $d['aodiscount'],
                'aobsource'  => $d['aobsource'], 
                'aodepart'   => $d['aodepart'],
                'aodepart'   => $d['aodepart'],
                'aoarrive'   => $d['aoarrive'],
                'aosname'    => $d['aosname'],
                'aostelp'    => $d['aostelp'],
                'aosemail'   => $d['aosemail'],
                'aopicture'  => $d['aopicture'],
                'aostatus'   => $d['aostatus']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons List
| -------------------------------------------------------------------------------------
*/
function get_add_ons_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_add_ons';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'aoid'       => $d['aoid'], 
            'aotype'     => $d['aotype'], 
            'aoname'     => $d['aoname'],
            'aodesc'     => $d['aodesc'],
            'aobrief'    => $d['aobrief'],
            'aostart'    => $d['aostart'],
            'aoend'      => $d['aoend'],
            'aosid'      => $d['aosid'],
            'aoprice'    => $d['aoprice'],
            'aodiscount' => $d['aodiscount'],
            'aobsource'  => $d['aobsource'], 
            'aodepart'   => $d['aodepart'],
            'aodepart'   => $d['aodepart'],
            'aoarrive'   => $d['aoarrive'],
            'aosname'    => $d['aosname'],
            'aostelp'    => $d['aostelp'],
            'aosemail'   => $d['aosemail'],
            'aopicture'  => $d['aopicture'],
            'aostatus'   => $d['aostatus']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Add-ons
| -------------------------------------------------------------------------------------
*/
function save_add_ons()
{
    global $db;

    list( $aostart, $aoend ) = explode( ' - ', $_POST['aoperiod'] );
    
    $aostart   = date( 'Y-m-d', strtotime( implode( '-', array_reverse( explode( '/', $aostart ) ) ) ) );
    $aoend     = date( 'Y-m-d', strtotime( implode( '-', array_reverse( explode( '/', $aoend ) ) ) ) );

    $aotype     = empty( $_POST['aotype'] ) ? 0 : $_POST['aotype'];
    $aoprice    = empty( $_POST['aoprice'] ) ? 0 : $_POST['aoprice'];
    $aodiscount = empty( $_POST['aodiscount'] ) ? 0 : $_POST['aodiscount'];
    $aosid      = empty( $_POST['aosid'] ) ? '' : json_encode( $_POST['aosid'] );
    $aodepart   = empty( $_POST['aodepart'] ) ? '' : json_encode( $_POST['aodepart'] );
    $aoarrive   = empty( $_POST['aoarrive'] ) ? '' : json_encode( $_POST['aoarrive'] );
    $aobsource  = empty( $_POST['aobsource'] ) ? '' : json_encode( $_POST['aobsource'] );
    $aopicture  = empty( $_POST['aopicture'] ) ? '' : base64_decode( $_POST['aopicture'] );
    
    $s = 'INSERT INTO ticket_add_ons(
            aoname,
            aodesc,
            aobrief,
            aostart,
            aoend,
            aosid,
            aobsource,
            aodepart,
            aoarrive,
            aosname,
            aostelp,
            aosemail,
            aoprice,
            aotype,
            aodiscount,
            aopicture,
            aostatus )
          VALUES( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
    $q = $db->prepare_query( $s, 
            $_POST['aoname'], 
            $_POST['aodesc'], 
            $_POST['aobrief'], 
            $aostart, 
            $aoend, 
            $aosid, 
            $aobsource,
            $aodepart,
            $aoarrive,
            $_POST['aosname'], 
            $_POST['aostelp'], 
            $_POST['aosemail'], 
            $aoprice,
            $aotype,
            $aodiscount,
            $aopicture,
            $_POST['aostatus'] );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $id = $db->insert_id();
        
        save_log( $id, 'add-ons', 'Add new add-ons - ' . $_POST['aoname'] );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Add-ons
| -------------------------------------------------------------------------------------
*/
function update_add_ons()
{
    global $db;

    $current = get_add_ons( $_POST['aoid'] );

    list( $aostart, $aoend ) = explode( ' - ', $_POST['aoperiod'] );
    
    $aostart   = date( 'Y-m-d', strtotime( implode( '-', array_reverse( explode( '/', $aostart ) ) ) ) );
    $aoend     = date( 'Y-m-d', strtotime( implode( '-', array_reverse( explode( '/', $aoend ) ) ) ) );

    $aotype     = empty( $_POST['aotype'] ) ? 0 : $_POST['aotype'];
    $aoprice    = empty( $_POST['aoprice'] ) ? 0 : $_POST['aoprice'];
    $aodiscount = empty( $_POST['aodiscount'] ) ? 0 : $_POST['aodiscount'];
    $aosid      = empty( $_POST['aosid'] ) ? '' : json_encode( $_POST['aosid'] );
    $aodepart   = empty( $_POST['aodepart'] ) ? '' : json_encode( $_POST['aodepart'] );
    $aoarrive   = empty( $_POST['aoarrive'] ) ? '' : json_encode( $_POST['aoarrive'] );
    $aobsource  = empty( $_POST['aobsource'] ) ? '' : json_encode( $_POST['aobsource'] );
    $aopicture  = empty( $_POST['aopicture'] ) ? '' : base64_decode( $_POST['aopicture'] );

    $s = 'UPDATE ticket_add_ons SET 
            aoname = %s,
            aodesc = %s,
            aobrief = %s,
            aostart = %s,
            aoend = %s,
            aosid = %s,
            aobsource = %s,
            aodepart = %s,
            aoarrive = %s,
            aosname = %s,
            aostelp = %s,
            aosemail = %s,
            aoprice = %s,
            aotype = %s,
            aodiscount = %s,
            aopicture = %s,
            aostatus = %s
          WHERE aoid = %d';     
    $q = $db->prepare_query( $s, 
            $_POST['aoname'],
            $_POST['aodesc'],
            $_POST['aobrief'],
            $aostart,
            $aoend,
            $aosid,
            $aobsource,
            $aodepart,
            $aoarrive,
            $_POST['aosname'],
            $_POST['aostelp'],
            $_POST['aosemail'],
            $aoprice,
            $aotype,
            $aodiscount,
            $aopicture,
            $_POST['aostatus'],
            $_POST['aoid'] );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $logs = array();

        $logs[] = $current['aotype'] != $_POST['aotype'] ? 'Change Add-ons Type : ' . $current['aotype'] . ' ---> ' . $_POST['aotype'] : '';
        $logs[] = $current['aoname'] != $_POST['aoname'] ? 'Change Add-ons Name : ' . $current['aoname'] . ' ---> ' . $_POST['aoname'] : '';
        $logs[] = $current['aodesc'] != $_POST['aodesc'] ? 'Change Add-ons Description : ' . $current['aodesc'] . ' ---> ' . $_POST['aodesc'] : '';
        $logs[] = $current['aobrief'] != $_POST['aobrief'] ? 'Change Add-ons Brief text : ' . $current['aobrief'] . ' ---> ' . $_POST['aobrief'] : '';
        $logs[] = $current['aostart'] != $aostart ? 'Change Add-ons Start Period : ' . $current['aostart'] . ' ---> ' . $aostart : '';
        $logs[] = $current['aoend'] != $aoend ? 'Change Add-ons End Period : ' . $current['aoend'] . ' ---> ' . $aoend : '';
        $logs[] = $current['aoprice'] != $aoprice ? 'Change Add-ons Price : ' . $current['aoprice'] . ' ---> ' . $aoprice : '';
        $logs[] = $current['aodiscount'] != $aodiscount ? 'Change Add-ons Price : ' . $current['aodiscount'] . ' ---> ' . $aodiscount : '';
        $logs[] = $current['aosname'] != $_POST['aosname'] ? 'Change Add-ons Supplier Name : ' . $current['aosname'] . ' ---> ' . $_POST['aosname'] : '';
        $logs[] = $current['aostelp'] != $_POST['aostelp'] ? 'Change Add-ons Supplier Phone : ' . $current['aostelp'] . ' ---> ' . $_POST['aostelp'] : '';
        $logs[] = $current['aosemail'] != $_POST['aosemail'] ? 'Change Add-ons Supplier Email : ' . $current['aosemail'] . ' ---> ' . $_POST['aosemail'] : '';

        if ( !empty( $logs ) ) 
        {
            foreach ( $logs as $i => $log ) 
            {
                if ( $log == '' ) 
                {
                    unset( $logs[ $i ] );
                }
            }

            save_log( $_POST['aoid'], 'add-ons', 'Update Add-ons - ' . $current['aoname'] . '<br/>' . implode( '<br/>', $logs ) );
        }

        return $_POST['aoid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Add-ons Picture
| -------------------------------------------------------------------------------------
*/
function update_add_ons_picture( $aoid, $aopicture )
{
    global $db;

    $d = get_add_ons( $aoid );

    $s = 'UPDATE ticket_add_ons SET aopicture = %s WHERE aoid = %d';     
    $q = $db->prepare_query( $s, $aopicture, $aoid );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        return false;
    }
    else
    {
        save_log( $aoid, 'add-ons' , 'Update add-ons picture - ' . $d['aoname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Add-ons
| -------------------------------------------------------------------------------------
*/
function delete_add_ons( $id='', $is_ajax = false )
{
    global $db;

    $d = get_add_ons( $id );

    $s = 'DELETE FROM ticket_add_ons WHERE aoid = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'schedules&sub=add-ons&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        if( empty( $d['aopicture'] ) === false )
        {
            $arr = json_decode( $d['aopicture'], true );

            if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
            {
                if( isset( $arr['large'] ) && !empty( $arr['large'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['large'] ) )
                {
                    unlink( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['large'] );
                }

                if( isset( $arr['medium'] ) && !empty( $arr['medium'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['medium'] ) )
                {
                    unlink( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['medium'] );
                }

                if( isset( $arr['original'] ) && !empty( $arr['original'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['original'] ) )
                {
                    unlink( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['original'] );
                }
            }
        }

        save_log( $id, 'add-ons' , 'Delete add-ons - ' . $d['aoname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons Option
| -------------------------------------------------------------------------------------
*/
function get_add_ons_option( $aoid = '', $use_empty = true, $empty_text = 'Select Add-ons' )
{
    $add_ons = get_add_ons_list();
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    
    if( !empty( $add_ons ) )
    {
        foreach( $add_ons as $d )
        {
            $option .= '<option data-type="' . $d['aotype'] . '" data-price="' . ( $d['aoprice'] - $d['aodiscount'] ) . '" value="' . $d['aoid'] . '" ' . ( $d['aoid'] == $aoid ? 'selected' : '' ) . ' >' . $d['aoname'] . '</option>';   
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons Option
| -------------------------------------------------------------------------------------
*/
function get_add_ons_status_option( $aostatus )
{
    return '
    <option value="1" ' . ( $aostatus == '1' ? 'selected' : '' ) . ' >Aktif</option>
    <option value="0" ' . ( $aostatus == '0' ? 'selected' : '' ) . ' >Tidak Aktif</option>';
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons Type Option
| -------------------------------------------------------------------------------------
*/
function get_add_ons_type_option( $aotype )
{
    return '
    <option value="0" ' . ( $aotype == '0' ? 'selected' : '' ) . ' >Product Items</option>
    <option value="1" ' . ( $aotype == '1' ? 'selected' : '' ) . ' >Private Transport</option>';
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons Departure Point Option
| -------------------------------------------------------------------------------------
*/
function get_add_ons_depart_point_option( $aodepart )
{
    global $db;

    $aodepart = json_decode( $aodepart, true );
    $aodepart = $aodepart !== null && json_last_error() === JSON_ERROR_NONE ? $aodepart : array();

    $s = 'SELECT * FROM ticket_location WHERE lctype <> %s';
    $q = $db->prepare_query( $s, 3 );
    $r = $db->do_query( $q );

    $option = '<option value=""></option>';

    if( empty( $r ) === false )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $option .= '<option value="' . $d['lcid'] . '" ' . ( in_array( $d['lcid'], $aodepart ) ? 'selected' : '' ) . ' >' . $d['lcname'] . '</option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons Arrival Point Option
| -------------------------------------------------------------------------------------
*/
function get_add_ons_arrive_point_option( $aoarrive )
{
    global $db;

    $aoarrive = json_decode( $aoarrive, true );
    $aoarrive = $aoarrive !== null && json_last_error() === JSON_ERROR_NONE ? $aoarrive : array();

    $s = 'SELECT * FROM ticket_location WHERE lctype <> %s';
    $q = $db->prepare_query( $s, 3 );
    $r = $db->do_query( $q );

    $option = '<option value=""></option>';

    if( empty( $r ) === false )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $option .= '<option value="' . $d['lcid'] . '" ' . ( in_array( $d['lcid'], $aoarrive ) ? 'selected' : '' ) . ' >' . $d['lcname'] . '</option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons Booking Source Option
| -------------------------------------------------------------------------------------
*/
function get_add_ons_schedule_option( $aosid )
{
    $aosid = json_decode( $aosid, true );
    $aosid = $aosid !== null && json_last_error() === JSON_ERROR_NONE ? $aosid : array();

    $schedule = get_schedule_list( 'a.sstatus = "publish"' );
    $option   = '<option value=""></option>';

    if( !empty( $schedule ) )
    {
        foreach( $schedule as $d )
        {
            $from = date( 'd F Y', strtotime( $d['sfrom'] ) );
            $to   = date( 'd F Y', strtotime( $d['sto'] ) );

            $option .= '<option value="' . $d['sid'] . '" ' . ( in_array( $d['sid'], $aosid ) ? 'selected' : '' ) . '>' . $from . ' - '. $to . ' ( ' . $d['rname'] . ' )</option>';   
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons Booking Source Option
| -------------------------------------------------------------------------------------
*/
function get_add_ons_booking_source_option( $aobsource )
{
    $aobsource = json_decode( $aobsource, true );
    $aobsource = $aobsource !== null && json_last_error() === JSON_ERROR_NONE ? $aobsource : array();

    $source = get_booking_source_list();
    $option = '<option value=""></option>';

    if( !empty( $source ) )
    {
        foreach( $source as $d )
        {
            $value  = empty( $d['agid'] ) ? $d['chid'] : $d['chid'] . '|' . $d['agid'];
            $name   = empty( $d['agid'] ) ? $d['chname'] : $d['agname'];

            $option .= '<option value="' . $value . '" ' . ( in_array( $value, $aobsource ) ? 'selected' : '' ) . ' >' . $name . '</option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Upload Add-ons Image
| -------------------------------------------------------------------------------------
*/
function upload_featured_image()
{
    global $db;

    if( isset( $_FILES['featured'] ) && $_FILES['featured']['error'] == 0 )
    {
        $file_name   = $_FILES['featured']['name'];
        $file_size   = $_FILES['featured']['size'];
        $file_type   = $_FILES['featured']['type'];
        $file_source = $_FILES['featured']['tmp_name'];
                        
        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {        
                $file_ext    = file_name_filter( $file_name, true );
                $sef_url     = file_name_filter( $file_name ) . '-'. time();
                $file_name_n = $sef_url . $file_ext;
                $file_name_l = $sef_url . '-large' . $file_ext;
                $file_name_m = $sef_url . '-medium' . $file_ext;

                if( !file_exists( PLUGINS_PATH . '/ticket/uploads/featured/' ) )
                {
                    mkdir( PLUGINS_PATH . '/ticket/uploads/featured/');
                }
                    
                $dest   = PLUGINS_PATH . '/ticket/uploads/featured/' . $file_name_n;
                $dest_l = PLUGINS_PATH . '/ticket/uploads/featured/' . $file_name_l;
                $dest_m = PLUGINS_PATH . '/ticket/uploads/featured/' . $file_name_m;
                
                $aopicture = json_encode( array( 'large' => $file_name_l, 'medium' => $file_name_m,  'original' => $file_name_n ) );

                if( upload_resize( $file_source, $dest_l, $file_type, 1920, 1920 ) )
                {
                    if( upload_resize( $file_source, $dest_m, $file_type, 500, 500 ) )
                    {
                        if( upload( $file_source, $dest ) )
                        {
                            if( empty( $_POST['aoid'] ) === false )
                            {
                                $oaopicture = get_add_ons( $_POST['aoid'], 'aopicture' );

                                if( update_add_ons_picture( $_POST['aoid'], $aopicture ) )
                                {
                                    $arr = json_decode( $oaopicture, true );

                                    if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
                                    {
                                        if( isset( $arr['large'] ) && !empty( $arr['large'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['large'] ) )
                                        {
                                            unlink( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['large'] );
                                        }

                                        if( isset( $arr['medium'] ) && !empty( $arr['medium'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['medium'] ) )
                                        {
                                            unlink( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['medium'] );
                                        }

                                        if( isset( $arr['original'] ) && !empty( $arr['original'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['original'] ) )
                                        {
                                            unlink( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['original'] );
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return array( 'result' => 'success', 'filename' => $file_name_m, 'picture' => base64_encode( $aopicture ) );
                }
                else
                {
                    return array( 'result' =>' error', 'message' => 'Something wrong' );
                }
            }
            else
            {
                return array( 'result' =>' error', 'message' => 'File type must be image (jpg, png, gif)' );
            }
        }
        else
        {
            return array( 'result' =>' error', 'message' => 'The maximum file size is 2MB' );
        }
    }
    else
    {
        return array( 'result' => 'error', 'message' => 'Image file is empty' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Add-ons Image
| -------------------------------------------------------------------------------------
*/
function delete_featured_image()
{
    $oaopicture = get_add_ons( $_POST['aoid'], 'aopicture' );

    if( update_add_ons_picture( $_POST['aoid'], '' ) )
    {
        if( !empty( $oaopicture ) )
        {
            $arr = json_decode( $oaopicture, true );

            if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
            {
                if( isset( $arr['large'] ) && !empty( $arr['large'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['large'] ) )
                {
                    unlink( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['large'] );
                }

                if( isset( $arr['medium'] ) && !empty( $arr['medium'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['medium'] ) )
                {
                    unlink( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['medium'] );
                }

                if( isset( $arr['original'] ) && !empty( $arr['original'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['original'] ) )
                {
                    unlink( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['original'] );
                }
            }
        }

        return array( 'result' => 'success' );
    }
    else
    {
        return array( 'result' => 'failed' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons Image Content
| -------------------------------------------------------------------------------------
*/
function get_add_ons_featured_image_content( $aopicture = '' )
{
    $arr = json_decode( $aopicture, true );

    if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
    {
        if( isset( $arr['medium'] ) && !empty( $arr['medium'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr['medium'] ) )
        {
            $content = '
            <div class="featured-overlay">
                <a class="delete-featured"></a>
            </div>
            <div class="featured-img">
                <textarea class="sr-only" name="aopicture">' . base64_encode( $aopicture ) . '</textarea>
                <img src="' . HTSERVER . site_url() . '/l-plugins/ticket/uploads/featured/' . $arr['medium'] . '" alt="Featured Image" />
            </div>';

            return $content;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Add-ons Image URL
| -------------------------------------------------------------------------------------
*/
function get_featured_image_url( $aoid, $size = 'large' )
{
    $aopicture = get_add_ons( $aoid, 'aopicture' );

    $arr = json_decode( $aopicture, true );

    if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
    {
        if( isset( $arr[ $size ] ) && !empty( $arr[ $size ] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/featured/' . $arr[ $size ] ) )
        {
            return HTSERVER . site_url() . '/l-plugins/ticket/uploads/featured/' . $arr[ $size ];
        }
        else
        {
            return HTSERVER . TEMPLATE_URL . '/images/first-bg.jpeg';
        }
    }
    else
    {
        return HTSERVER . TEMPLATE_URL . '/images/first-bg.jpeg';
    }
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_add_ons_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_add_ons_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-add-ons' )
    {
        if( is_access( $_COOKIE['user_id'], $_POST['state'], $_POST['sub'] ) )
        {
            $d = delete_add_ons( $_POST['aoid'], true );

            if( $d === true )
            {
                echo '{"result":"success"}';
            }
            else
            {
                echo json_encode( $d );
            }
        }
        else
        {
            echo '{"result":"failed"}';
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-featured-image' )
    {
        $data = upload_featured_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-featured-image' )
    {
        $data = delete_featured_image();

        echo json_encode( $data );
    }
}

?>