<?php

add_actions( 'schedule', 'ticket_schedule' );
add_actions( 'ticket-schedule-ajax_page', 'ticket_schedule_ajax' );

function ticket_schedule()
{
    if( is_num_schedule() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'schedules&sub=schedule&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_schedule();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_schedule( $_GET['id'] ) )
        {
            return ticket_edit_schedule();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_schedule();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_schedule( $val );
        }
    }

    return ticket_schedule_table();
}

/*
| -------------------------------------------------------------------------------------
| Schedule Table List
| -------------------------------------------------------------------------------------
*/
function ticket_schedule_table()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url('schedules&sub=schedule') . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    $site_url = site_url();
    $filter   = ticket_filter_schedule();

    extract( $filter );

	set_template( PLUGINS_PATH . '/ticket/tpl/schedule/list.html', 'schedule' );

    add_block( 'list-block', 'sblock', 'schedule' );

    add_variable( 'period', $period );
    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'boat_option', get_boat_option( $boid, true, 'All Boat') );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'status_option', get_status_schedule_option( $sstatus, true, 'All Status') );

    add_variable( 'action', get_state_url( 'schedules&sub=schedule' ) );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=schedule&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'schedules&sub=schedule&prc=edit' ) );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-schedule-ajax/' );
    
    add_actions( 'section_title', 'Schedule List' );

    add_actions( 'header_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/momentjs/latest/moment.min.js' );
    add_actions( 'other_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' );
    
    parse_template( 'list-block', 'sblock', false );

    return return_template( 'schedule' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Schedule
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_schedule()
{
	$message = run_save_schedule();
    $data    = get_schedule();

	set_template( PLUGINS_PATH . '/ticket/tpl/schedule/form.html', 'schedule' );
    add_block( 'form-block', 'sblock', 'schedule' );

    add_variable( 'sid', $data['sid'] );
    add_variable( 'boid', get_boat_option( $data['boid'] ) );
    add_variable( 'rid', get_schedule_route_option( $data['rid'] ) );
    add_variable( 'radio_publish_opt', $data['sstatus']=='publish' ? 'checked' : '' );
    add_variable( 'radio_draft_opt', $data['sstatus']=='draft' ? 'checked' : '' );
    add_variable( 'sto', empty( $data['sto'] ) ? '' : date( 'd F Y', strtotime( $data['sto'] ) ) );
    add_variable( 'sfrom', empty( $data['sfrom'] ) ? '' : date( 'd F Y', strtotime( $data['sfrom'] ) ) );

    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'schedules&sub=schedule&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=schedule' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'sblock', false );
    
    add_actions( 'section_title', 'New Schedule' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'schedule' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Schedule
| -------------------------------------------------------------------------------------
*/
function ticket_edit_schedule()
{
    $message = run_update_schedule();
    $data    = get_schedule( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/schedule/form.html', 'schedule' );
    add_block( 'form-block', 'sblock', 'schedule' );

    add_variable( 'sid', $data['sid'] );
    add_variable( 'boid', get_boat_option( $data['boid'] ) );
    add_variable( 'rid', get_schedule_route_option( $data['rid'], true ) );
    add_variable( 'radio_publish_opt', $data['sstatus']=='publish' ? 'checked' : '' );
    add_variable( 'radio_draft_opt', $data['sstatus']=='draft' ? 'checked' : '' );
    add_variable( 'sto', empty( $data['sto'] ) ? '' : date( 'd F Y', strtotime( $data['sto'] ) ) );
    add_variable( 'sfrom', empty( $data['sfrom'] ) ? '' : date( 'd F Y', strtotime( $data['sfrom'] ) ) ); 

    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'schedules&sub=schedule&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-schedule-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=schedule' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'schedule' ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'sblock', false );
    
    add_actions( 'section_title', 'Edit Schedule' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'schedule' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Schedule
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_schedule()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/schedule/batch-delete.html', 'schedule' );
    add_block( 'loop-block', 'pmlblock', 'schedule' );
    add_block( 'delete-block', 'sblock', 'schedule' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_schedule( $val );

        add_variable('sfrom', date( 'd F Y', strtotime( $d['sfrom'] ) ) );
        add_variable('sto', date( 'd F Y', strtotime( $d['sto'] ) ) );
        add_variable('sid', $d['sid'] );

        parse_template('loop-block', 'pmlblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' schedule? :' );
    add_variable('action', get_state_url('schedules&sub=schedule'));

    parse_template( 'delete-block', 'sblock', false );
    
    add_actions( 'section_title', 'Delete Schedule' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'schedule' );
}

function run_save_schedule()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_schedule_data();

        if( empty($error) )
        {
            $post_id = save_schedule();
            
            header( 'location:'.get_state_url( 'schedules&sub=schedule&prc=add_new&result=1' ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'New schedule successfully saved' ) );
    }
}

function run_update_schedule()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_schedule_data();

        if( empty($error) )
        {
            $post_id = update_schedule();
            
            header( 'location:'.get_state_url( 'schedules&sub=schedule&prc=edit&result=1&id=' . $post_id ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This schedule successfully edited' ) );
    }
}

function validate_schedule_data()
{
    $error = array();

    if( !isset($_POST['rid']) || ( isset($_POST['rid']) && empty( $_POST['rid'] ) ) )
    {
        $error[] = 'Route can\'t be empty';
    }

    if( isset($_POST['boid']) && empty( $_POST['boid'] ) )
    {
        $error[] = 'Boat can\'t be empty';
    }

    if( !isset($_POST['sstatus']) )
    {
        $error[] = 'Schedule status can\'t be empty';
    }

    if( isset($_POST['sfrom']) && empty( $_POST['sfrom'] ) )
    {
        $error[] = 'Start schedule date can\'t be empty';
    }

    if( isset($_POST['sto']) && empty( $_POST['sto'] ) )
    {
        $error[] = 'End schedule date can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Schedule List Count
| -------------------------------------------------------------------------------------
*/
function is_num_schedule()
{
    global $db;

    $s = 'SELECT * FROM ticket_schedule';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Schedule By ID
| -------------------------------------------------------------------------------------
*/
function get_schedule( $id = '', $field = '' )
{
    global $db;

    $data = array( 
        'sid'     => ( isset( $_POST['sid'] ) ? $_POST['sid'] : null ), 
        'rid'     => ( isset( $_POST['rid'] ) ? $_POST['rid'] : '' ), 
        'boid'    => ( isset( $_POST['boid'] ) ? $_POST['boid'] : '' ),
        'sfrom'   => ( isset( $_POST['sfrom'] ) ? $_POST['sfrom'] : '' ),
        'sto'     => ( isset( $_POST['sto'] ) ? $_POST['sto'] : '' ), 
        'sstatus' => ( isset( $_POST['sstatus'] ) ? $_POST['sstatus'] : '' )
    );

    $s = 'SELECT * FROM ticket_schedule WHERE sid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'sid'     => $d['sid'],
                'rid'     => $d['rid'],
                'boid'    => $d['boid'],
                'sfrom'   => $d['sfrom'],
                'sto'     => $d['sto'],
                'sstatus' => $d['sstatus']
            );
        }
    }

    if( !empty( $field ) && isset( $data[ $field ] ) )
    {
        return $data[ $field ];
    }
    else
    {
        return $data;
    }
}

function ticket_schedule_table_query( $rid = '', $period = '', $boid = '', $sstatus = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1  => 'b.rname',
        2  => 'CONCAT( DATE_FORMAT( a.sfrom, "%d %M %Y" ), " - ", DATE_FORMAT( a.sto, "%d %M %Y" ) )',
        5  => 'c.boname',
        7  => 'a.sstatus'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.sfrom DESC';
    }
    
    $w = array();

    if( $rid != '' )
    {
        $w[] = $db->prepare_query( 'b.rid = %d', $rid );
    }

    if( $boid != '' )
    {
        $w[] = $db->prepare_query( 'c.boid = %d', $boid );
    }

    if( $sstatus != '' )
    {
        $w[] = $db->prepare_query( 'a.sstatus = %s', $sstatus );
    }

    if( $period != '' )
    {
        list( $sfrom, $sto ) = explode( ' - ', str_replace( '/' , '-', $period ) );

        $w[] = $db->prepare_query( '(( %s BETWEEN a.sfrom AND a.sto ) OR ( %s BETWEEN a.sfrom AND a.sto ))', date( 'Y-m-d', strtotime( $sfrom ) ), date( 'Y-m-d', strtotime( $sto ) ) );
    }

    if( empty( $w ) )
    {
        $where = '';
    }
    else
    {
        $where = 'WHERE ' . implode( ' AND ', $w );
    }

    $q = 'SELECT
            a.sid,
            b.rname,
            c.boname,
            a.sstatus,
            CONCAT( DATE_FORMAT( a.sfrom, "%d %M %Y" ), " - ", DATE_FORMAT( a.sto, "%d %M %Y" ) ) AS period
          FROM ticket_schedule AS a
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          ' . $where . ' ORDER BY ' . $order;
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'sid'       => $d2['sid'],
                'rname'     => $d2['rname'],
                'boname'    => $d2['boname'],
                'period'    => $d2['period'],
                'sstatus'   => ucfirst( $d2['sstatus'] ),
                'edit_link' => get_state_url( 'schedules&sub=schedule&prc=edit&id=' . $d2['sid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Schedule List
| -------------------------------------------------------------------------------------
*/
function get_schedule_list( $where = '' )
{
    global $db;

    if( empty( $where ) )
    {
        $s = 'SELECT * FROM ticket_schedule AS a LEFT JOIN ticket_route AS b ON a.rid = b.rid ORDER BY b.rorder';
    }
    else
    {
        $s = 'SELECT * FROM ticket_schedule AS a LEFT JOIN ticket_route AS b ON a.rid = b.rid WHERE ' . $where . '  ORDER BY b.rorder';
    }
    
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = $d;
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Schedule
| -------------------------------------------------------------------------------------
*/
function save_schedule()
{
    global $db;

    if( is_array( $_POST['rid'] ) )
    {
        foreach( $_POST['rid'] as $rid )
        {
            $s = 'INSERT INTO ticket_schedule(
                    rid,
                    boid,
                    sfrom,
                    sto,
                    sstatus,
                    screateddate,
                    luser_id)
                  VALUES( %d, %d, %s, %s, %s, %s, %d )';
            $q = $db->prepare_query( $s,
                    $rid,
                    $_POST['boid'],
                    date( 'Y-m-d', strtotime( $_POST['sfrom'] ) ),
                    date( 'Y-m-d', strtotime( $_POST['sto'] ) ),
                    $_POST['sstatus'],
                    date( 'Y-m-d H:i:s' ),
                    $_COOKIE['user_id'] );
               
            if( $db->do_query( $q ) )
            {
                $id = $db->insert_id();
                
                save_allotment( $id );

                $from = date( 'd/m/Y', strtotime( $_POST['sfrom'] ) );
                $to   = date( 'd/m/Y', strtotime( $_POST['sto'] ) );

                save_log( $id, 'schedule', 'Add new schedule from ' . $from . ' until ' . $to );
            }
        }

        return true;
    }
    else
    {
        $s = 'INSERT INTO ticket_schedule(
                rid,
                boid,
                sfrom,
                sto,
                sstatus,
                screateddate,
                luser_id)
              VALUES( %d, %d, %s, %s, %s, %s, %d )';
        $q = $db->prepare_query( $s,
                $_POST['rid'],
                $_POST['boid'],
                date( 'Y-m-d', strtotime( $_POST['sfrom'] ) ),
                date( 'Y-m-d', strtotime( $_POST['sto'] ) ),
                $_POST['sstatus'],
                date( 'Y-m-d H:i:s' ),
                $_COOKIE['user_id'] );
           
        if( $db->do_query( $q ) )
        {
            $id = $db->insert_id();

            $from = date( 'd/m/Y', strtotime( $_POST['sfrom'] ) );
            $to   = date( 'd/m/Y', strtotime( $_POST['sto'] ) );
            
            save_log( $id, 'schedule', 'Add new schedule from ' . $from . ' until ' . $to );
        
            return $id;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Schedule
| -------------------------------------------------------------------------------------
*/
function update_schedule()
{
    global $db;

    $d = get_schedule( $_POST['sid'] );

    $s = 'UPDATE ticket_schedule SET 
            rid = %d, 
            boid = %d, 
            sfrom = %s, 
            sto = %s, 
            sstatus = %s
          WHERE sid = %d';     
    $q = $db->prepare_query( $s,
              $_POST['rid'],
              $_POST['boid'],
              date( 'Y-m-d', strtotime( $_POST['sfrom'] ) ),
              date( 'Y-m-d', strtotime( $_POST['sto'] ) ),
              $_POST['sstatus'],
              $_POST['sid'] );

    /* BEGIN CREATE LOG */
    $field_log = array();
    $field_log[] = $d['rid'] != $_POST['rid'] ? 'Change Route : ' . get_data_by_id( 'rname', 'ticket_route', 'rid', $d['rid'] ) . ' to ' . get_data_by_id( 'rname', 'ticket_route', 'rid', $_POST['rid'] ) : '';
    $field_log[] = $d['boid'] != $_POST['boid'] ? 'Change Boat : ' . get_data_by_id( 'boname', 'ticket_boat', 'boid', $d['boid'] ) . ' to ' . get_data_by_id( 'boname', 'ticket_boat', 'boid', $_POST['boid'] ) : '';
    $field_log[] = $d['sstatus'] != $_POST['sstatus'] ? 'Change Status : ' . $d['sstatus'] . ' to ' . $_POST['sstatus'] : '';
    $field_log[] = $d['sfrom'].$d['sto'] != date( 'Y-m-d', strtotime( $_POST['sfrom'] ) ).date( 'Y-m-d', strtotime( $_POST['sto'] ) ) ? 'Change Date : ' . date( 'd/m/Y', strtotime( $d['sfrom'] ) ) . ' until ' . date( 'd/m/Y', strtotime( $d['sto'] ) ) . ' to ' . date( 'd/m/Y', strtotime( $_POST['sfrom'] ) ) . ' until ' . date( 'd/m/Y', strtotime( $_POST['sto'] ) ) : '';
    /* END CREATE LOG */

    if( $db->do_query( $q ) )
    {
        save_allotment( $_POST['sid'] );

        /* BEGIN SAVE LOG */
        if ( !empty( $field_log ) ) 
        {
            foreach ( $field_log as $k => $log ) 
            {
                if ( $log == '' ) 
                {
                    unset( $field_log[$k] );
                }
            }

            save_log( $_POST['sid'], 'schedule', 'Update Manage Schedule ID#' . $_POST['sid'] . '<br/>' . implode( '<br/>', $field_log ) );
        }
        /* END SAVE LOG */

        return $_POST['sid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Schedule
| -------------------------------------------------------------------------------------
*/
function delete_schedule( $id='' )
{
    global $db;

    $d = get_schedule( $id );

    $s = 'DELETE FROM ticket_schedule WHERE sid = %d';          
    $q = $db->prepare_query( $s, $id );
       
    if( $db->do_query( $q ) )
    {
        $from = date( 'd/m/Y', strtotime( $d['sfrom'] ) );
        $to   = date( 'd/m/Y', strtotime( $d['sto'] ) );

        save_log( $id, 'schedule', 'Delete schedule from ' . $from . ' until ' . $to );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Schedule Route Option
| -------------------------------------------------------------------------------------
*/
function get_schedule_route_option( $rid, $is_edit = false )
{
    if( $is_edit )
    {
        return '
        <select class="select-option" name="rid" autocomplete="off" {is_multiple} data-placeholder="Select Route">
            ' . get_route_option( $rid ) . '
        </select>';
    }
    else
    {
        return '
        <select class="select-option select-medium" name="rid[]" autocomplete="off" multiple="multiple" data-placeholder="Select Route">
            ' . get_route_option( $rid, false ) . '
        </select>';
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Schedule Option
| -------------------------------------------------------------------------------------
*/
function get_schedule_option( $sid = '', $use_empty = true, $empty_text = 'Select Schedule', $where = 'a.sstatus = "publish"' )
{
    $schedule = get_schedule_list( $where );
    $option   = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $schedule ) )
    {
        foreach( $schedule as $d )
        {
            $from = date( 'd F Y', strtotime( $d['sfrom'] ) );
            $to   = date( 'd F Y', strtotime( $d['sto'] ) );

            if( is_array( $sid ) )
            {
                $class = in_array( $d['sid'], $sid ) ? 'selected' : '';
            }
            else
            {
                $class = $d['sid'] == $sid ? 'selected' : '';
            }

            $option .= '<option value="' . $d['sid'] . '" ' . $class . ' >' . $from . ' - '. $to . ' ( ' . $d['rname'] . ' )</option>';   
        }
    }

    return $option;
}

function get_status_schedule_option( $status = '', $use_empty = true, $empty_text = 'Select Schedule' )
{
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="publish" ' . ( $status == 'publish' ? 'selected' : '' ) . '>Publish</option>
    <option value="draft" ' . ( $status == 'draft' ? 'selected' : '' ) . '>Draft</option>';

    return $option;
}

function ticket_filter_schedule()
{
    $filter = array( 'rid' => '', 'period' => '', 'boid' => '', 'sstatus' => '' );

    if( isset( $_GET['prm'] ) )
    {
        extract( json_decode( base64_decode( $_GET['prm'] ), true ) );

        $filter = array( 'rid' => $rid, 'period' => $period, 'boid' => $boid, 'sstatus' => $sstatus );
    }

    return $filter;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_schedule_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        extract( $_POST );

        $data = ticket_schedule_table_query( $rid, $period, $boid, $sstatus );

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-schedule' )
    {
        if( delete_schedule( $_POST['sid'] ) )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"failed"}';   
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Log | Data Log
| -------------------------------------------------------------------------------------
*/
function get_data_by_id( $field, $table, $fsearch, $id )
{
    global $db;

    $s = 'SELECT '.$field.' FROM '.$table.' WHERE '.$fsearch.'='.$id;
    $r = $db->do_query( $s );
    $f = $db->fetch_array( $r );

    return $f[$field];
}

?>