<?php

add_actions( 'driver', 'ticket_transport_driver' );
add_actions( 'ticket-transport-driver-ajax_page', 'ticket_transport_driver_ajax' );

function ticket_transport_driver()
{
    if( is_num_transport_driver() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'transport&sub=driver&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_transport_driver();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_transport_driver( $_GET['id'] ) )
        {
            return ticket_edit_transport_driver();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_transport_driver();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_transport_driver( $val );
        }
    }

    return ticket_transport_driver_table();
}

/*
| -------------------------------------------------------------------------------------
| Transport Driver Table List
| -------------------------------------------------------------------------------------
*/
function ticket_transport_driver_table()
{
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/driver/list.html', 'transport-driver' );

    add_block( 'list-block', 'lcblock', 'transport-driver' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'transport&sub=driver' ) );
    add_variable( 'edit_link', get_state_url( 'transport&sub=driver&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'transport&sub=driver&prc=add_new' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-transport-driver-ajax/' );
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Driver List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'transport-driver' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Transport Driver
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_transport_driver()
{
	run_save_transport_driver();

    $site_url = site_url();
    $data     = get_transport_driver();

	set_template( PLUGINS_PATH . '/ticket/tpl/driver/form.html', 'transport-driver' );
    add_block( 'form-block', 'lcblock', 'transport-driver' );

    add_variable( 'did', $data['did'] );
    add_variable( 'dname', $data['dname'] );
    add_variable( 'dphone', $data['dphone'] );
    add_variable( 'dphone2', $data['dphone2'] );
    add_variable( 'dstatus', $data['dstatus'] );
    add_variable( 'davatar', $data['davatar'] );
    add_variable( 'dtype', get_driver_type_option( $data['dtype'] ) );
    add_variable( 'davatar_thumb', get_driver_avatar_img( $data['davatar_thumb'] ) );
    add_variable( 'radio_active_opt', $data['dstatus'] == '1' ? 'checked' : '' );
    add_variable( 'radio_inactive_opt', $data['dstatus'] == '0' ? 'checked' : '' );
    add_variable( 'default_avatar', HTSERVER . $site_url . '/l-plugins/ticket/images/driver.png' );

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'transport&sub=driver&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=driver' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'New Driver' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'transport-driver' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Transport Driver
| -------------------------------------------------------------------------------------
*/
function ticket_edit_transport_driver()
{
    run_update_transport_driver();

    $site_url = site_url();
    $data     = get_transport_driver( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/driver/form.html', 'transport-driver' );
    add_block( 'form-block', 'lcblock', 'transport-driver' );

    add_variable( 'did', $data['did'] );
    add_variable( 'dname', $data['dname'] );
    add_variable( 'dphone', $data['dphone'] );
    add_variable( 'dphone2', $data['dphone2'] );
    add_variable( 'dstatus', $data['dstatus'] );
    add_variable( 'davatar', $data['davatar'] );
    add_variable( 'dtype', get_driver_type_option( $data['dtype'] ) );
    add_variable( 'davatar_thumb', get_driver_avatar_img( $data['davatar_thumb'] ) );
    add_variable( 'radio_active_opt', $data['dstatus']=='1' ? 'checked' : '' );
    add_variable( 'radio_inactive_opt', $data['dstatus']=='0' ? 'checked' : '' );
    add_variable( 'default_avatar', HTSERVER . $site_url . '/l-plugins/ticket/images/driver.png' );

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'transport&sub=driver&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-transport-driver-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=driver' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'transport-driver' ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Edit Driver' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'transport-driver' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Transport Driver
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_transport_driver()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/driver/batch-delete.html', 'transport-driver' );
    add_block( 'loop-block', 'lclblock', 'transport-driver' );
    add_block( 'delete-block', 'lcblock', 'transport-driver' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_transport_driver( $val );

        add_variable('dname',  $d['dname'] );
        add_variable('did', $d['did'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' driver? :' );
    add_variable('action', get_state_url('transport&sub=driver'));

    parse_template( 'delete-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Delete Driver' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'transport-driver' );
}

function run_save_transport_driver()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_transport_driver_data();

        if( empty( $error ) )
        {
            $post_id = save_transport_driver();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new driver' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New driver successfully saved' ) ) );

                header( 'location:' . get_state_url( 'transport&sub=driver&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_transport_driver()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_transport_driver_data();

        if( empty( $error ) )
        {
            $post_id = update_transport_driver();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this driver' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This driver successfully edited' ) ) );

                header( 'location:' . get_state_url( 'transport&sub=driver&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_transport_driver_data()
{
    $error = array();

    if( isset( $_POST['dname'] ) && empty( $_POST['dname'] ) )
    {
        $error[] = 'Driver name can\'t be empty';
    }

    if( isset( $_POST['dphone'] ) && empty( $_POST['dphone'] ) )
    {
        $error[] = 'Handphone number can\'t be empty';
    }

    if( !isset( $_POST['dstatus'] ) )
    {
        $error[] = 'Driver status can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Driver List Count
| -------------------------------------------------------------------------------------
*/
function is_num_transport_driver()
{
    global $db;

    $s = 'SELECT * FROM ticket_transport_driver';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Transport Driver Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_transport_driver_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array( 
        1  => 'a.dname',
        2  => 'a.dphone',
        3  => 'a.dphone2',
        4  => 'a.dtype',
        5  => 'a.dstatus'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.did DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT * FROM ticket_transport_driver AS a ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT * FROM ticket_transport_driver AS a WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $dtype   = $d2['dtype'] == '0' ? 'BWE Driver' : 'Freelance Driver';
            $dstatus = $d2['dstatus'] == '0' ? 'Inactive' : 'Active';

            $data[] = array(
                'dtype'     => $dtype,
                'dstatus'   => $dstatus,
                'did'       => $d2['did'],
                'dname'     => $d2['dname'],
                'dphone'    => $d2['dphone'],
                'dphone2'   => $d2['dphone2'],
                'davatar'   => get_driver_avatar_img( $d2['davatar_thumb'] ),
                'edit_link' => get_state_url( 'transport&sub=driver&prc=edit&id=' . $d2['did'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Driver By ID
| -------------------------------------------------------------------------------------
*/
function get_transport_driver( $id='', $field = '' )
{
    global $db;

    $data = array( 
        'did'           => ( isset( $_POST['did'] ) ? $_POST['did'] : null ), 
        'dname'         => ( isset( $_POST['dname'] ) ? $_POST['dname'] : '' ), 
        'dphone'        => ( isset( $_POST['dphone'] ) ? $_POST['dphone'] : '' ), 
        'dphone2'       => ( isset( $_POST['dphone2'] ) ? $_POST['dphone2'] : '' ), 
        'dtype'         => ( isset( $_POST['dtype'] ) ? $_POST['dtype'] : '' ),
        'dstatus'       => ( isset( $_POST['dstatus'] ) ? $_POST['dstatus'] : '' ),
        'dcreateddate'  => ( isset( $_POST['dcreateddate'] ) ? $_POST['dcreateddate'] : '' ),
        'luser_id'      => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' ),
        'davatar'       => '',
        'davatar_thumb' => ''
    );

    $s = 'SELECT * FROM ticket_transport_driver WHERE did = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'did' => $d['did'], 
                'dname' => $d['dname'], 
                'dphone' => $d['dphone'], 
                'dphone2' => $d['dphone2'], 
                'dtype' => $d['dtype'],
                'dstatus' => $d['dstatus'],
                'davatar' => $d['davatar'],
                'davatar_thumb' => $d['davatar_thumb'],
                'dcreateddate' => $d['dcreateddate'],
                'luser_id' => $d['luser_id']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Driver List
| -------------------------------------------------------------------------------------
*/
function get_transport_driver_list( $status )
{
    global $db;

    $w = '';

    if ( $status != '' ) 
    {
        $w = $db->prepare_query( ' WHERE dstatus = %s', $status );
    }

    $s = 'SELECT * FROM ticket_transport_driver' . $w . ' ORDER BY dtype ASC';

    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'did' => $d['did'],
            'dname' => $d['dname'],
            'dphone' => $d['dphone'],
            'dphone2' => $d['dphone2'],
            'dtype' => $d['dtype'],
            'dstatus' => $d['dstatus'],
            'davatar' => $d['davatar'],
            'davatar_thumb' => $d['davatar_thumb'],
            'dcreateddate' => $d['dcreateddate'],
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Transport Driver
| -------------------------------------------------------------------------------------
*/
function save_transport_driver()
{
    global $db;
    
    $s = 'INSERT INTO ticket_transport_driver(
            dname,
            dphone,
            dphone2,
            dtype,
            dstatus,
            dcreateddate,
            luser_id)
          VALUES( %s, %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s,
            $_POST['dname'], 
            $_POST['dphone'], 
            $_POST['dphone2'],
            $_POST['dtype'],
            $_POST['dstatus'],
            date( 'Y-m-d H:i:s' ), 
            $_COOKIE['user_id'] );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        $id = $db->insert_id();
        
        save_log( $id, 'transport-driver', 'Add new driver - ' . $_POST['dname'] );

        upload_driver_avatar( $id );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Transport Driver
| -------------------------------------------------------------------------------------
*/
function update_transport_driver()
{
    global $db;

    $d = get_transport_driver( $_POST['did'] );

    $s = 'UPDATE ticket_transport_driver SET
            dname = %s,
            dphone = %s,
            dphone2 = %s,
            dtype = %s,
            dstatus = %s
          WHERE did = %d';     
    $q = $db->prepare_query( $s, 
            $_POST['dname'], 
            $_POST['dphone'],  
            $_POST['dphone2'], 
            $_POST['dtype'], 
            $_POST['dstatus'],
            $_POST['did'] );
       
    if( $db->do_query( $q ) )
    {
        
        $type_old = $d['dtype'] == '0' ? 'BWE Driver' : 'Freelance Driver';
        $type     = $_POST['dtype'] == '0' ? 'BWE Driver' : 'Freelance Driver';
        $stt_old  = $d['dstatus'] == '0' ? 'Inactive' : 'Active';
        $stt      = $_POST['dstatus'] == '0' ? 'Inactive' : 'Active';

        $logs   = array();
        $logs[] = $d['dname'] != $_POST['dname'] ? 'Change Driver Name : ' . $d['dname'] . ' to ' . $_POST['dname'] : '';
        $logs[] = $d['dphone'] != $_POST['dphone'] ? 'Change Handphone 1 : ' . $d['dphone'] . ' to ' . $_POST['dphone'] : '';
        $logs[] = $d['dphone2'] != $_POST['dphone2'] ? 'Change Handphone 2 : ' . $d['dphone2'] . ' to ' . $_POST['dphone2'] : '';
        $logs[] = $d['dtype'] != $_POST['dtype'] ? 'Change Driver Type : ' . $type_old . ' to ' . $type : '';
        $logs[] = $d['dstatus'] != $_POST['dstatus'] ? 'Change Driver Status : ' . $stt_old . ' to ' . $stt : '';

        if ( !empty( $logs ) ) 
        {
            foreach ( $logs as $i => $log ) 
            {
                if ( $log == '' ) 
                {
                    unset( $logs[$i] );
                }
            }

            save_log( $_POST['did'], 'transport-driver', 'Update Driver - ' . $d['dname'] . '<br/>' . implode( '<br/>', $logs ) );
        }

        upload_driver_avatar( $_POST['did'] );

        return $_POST['did'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Upload Driver Avatar
| -------------------------------------------------------------------------------------
*/
function upload_driver_avatar( $id = '' )
{
    global $db;

    if( $_FILES['avatar']['error']==0 )
    {
        $file_name   = $_FILES['avatar']['name'];
        $file_size   = $_FILES['avatar']['size'];
        $file_type   = $_FILES['avatar']['type'];
        $file_source = $_FILES['avatar']['tmp_name'];
                        
        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {        
                $file_ext    = file_name_filter( $file_name, true );
                $sef_url     = file_name_filter( $file_name ) . '-'. time();
                $file_name_n = $sef_url . $file_ext;
                $file_name_t = $sef_url . '-thumb' . $file_ext;

                if( !file_exists( PLUGINS_PATH . '/ticket/uploads/avatar/' ) )
                {
                    mkdir( PLUGINS_PATH . '/ticket/uploads/avatar/');
                }
                    
                $dest   = PLUGINS_PATH . '/ticket/uploads/avatar/' . $file_name_n;
                $dest_t = PLUGINS_PATH . '/ticket/uploads/avatar/' . $file_name_t;

                upload_crop( $file_source, $dest_t, $file_type, 150, 150 );
                upload( $file_source, $dest );
                
                $s = 'UPDATE ticket_transport_driver SET davatar = %s, davatar_thumb = %s WHERE did = %d';     
                $q = $db->prepare_query( $s, $file_name_n, $file_name_t, $id );
                $d = get_transport_driver( $id );

                if( $db->do_query( $q ) )
                {
                    //-- Delete Previous Avatar
                    if( !empty( $d['davatar'] ) && file_exists( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar'] ) )
                    {
                        unlink( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar'] );
                    }

                    if( !empty( $d['davatar_thumb'] ) && file_exists( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar_thumb'] ) )
                    {
                        unlink( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar_thumb'] );
                    }
                }
            }
        }
    }
    else
    {
        if( empty( $_POST['davatar'] ) )
        {
            $s = 'UPDATE ticket_transport_driver SET davatar = %s, davatar_thumb = %s WHERE did = %d';     
            $q = $db->prepare_query( $s, '', '', $id );
            $d = get_transport_driver( $id );

            if( $db->do_query( $q ) )
            {
                //-- Delete Previous Avatar
                if( !empty( $d['davatar'] ) && file_exists( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar'] ) )
                {
                    unlink( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar'] );
                }

                if( !empty( $d['davatar_thumb'] ) && file_exists( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar_thumb'] ) )
                {
                    unlink( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar_thumb'] );
                }
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Transport Driver
| -------------------------------------------------------------------------------------
*/
function delete_transport_driver( $id='', $is_ajax = false )
{
    global $db;

    $d = get_transport_driver( $id );

    $s = 'DELETE FROM ticket_transport_driver WHERE did = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'transport&sub=driver&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        if( !empty( $d['davatar'] ) && file_exists( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar'] ) )
        {
            unlink( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar'] );
        }

        if( !empty( $d['davatar_thumb'] ) && file_exists( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar_thumb'] ) )
        {
            unlink( PLUGINS_PATH . '/ticket/uploads/avatar/' . $d['davatar_thumb'] );
        }

        save_log( $id, 'transport-driver', 'Delete driver - ' . $d['dname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Driver Avatar
| -------------------------------------------------------------------------------------
*/
function get_driver_avatar_img( $filename = '' )
{
    if( !empty( $filename ) && file_exists( PLUGINS_PATH . '/ticket/uploads/avatar/' . $filename ) )
    {
        return HTSERVER . site_url() . '/l-plugins/ticket/uploads/avatar/' . $filename;
    }
    else
    {
        return HTSERVER . site_url() . '/l-plugins/ticket/images/driver.png';
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Driver Type Option
| -------------------------------------------------------------------------------------
*/
function get_driver_type_option( $type='' )
{
    $option = '
    <option value="0" ' . ( $type=='0' ? 'selected' : '' ) . '>BWE Driver</option>
    <option value="1" ' . ( $type=='1' ? 'selected' : '' ) . '>Freelance Driver</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Transport Driver Option
| -------------------------------------------------------------------------------------
*/
function get_driver_option( $did = '', $use_empty = true, $empty_text = 'Select Driver', $status = '' )
{
    $driver = get_transport_driver_list( $status );
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $driver ) )
    {
        foreach( $driver as $d )
        {
            $option .= '<option data-type="' . $d['dtype'] . '" value="' . $d['did'] . '" ' . ( $d['did'] == $did ? 'selected' : '' ) . ' >' . $d['dname'] . '</option>';   
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_transport_driver_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_transport_driver_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-transport-driver' )
    {
        $d = delete_transport_driver( $_POST['did'], true );

        if( $d === true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}

?>