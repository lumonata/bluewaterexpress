<?php

add_actions( 'vehicle-owner', 'ticket_vehicle_owner' );
add_actions( 'ticket-vehicle-owner-ajax_page', 'ticket_vehicle_owner_ajax' );

function ticket_vehicle_owner()
{
    if( is_num_vehicle_owner() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'transport&sub=vehicle-owner&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_vehicle_owner();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_vehicle_owner( $_GET['id'] ) )
        {
            return ticket_edit_vehicle_owner();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_vehicle_owner();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_vehicle_owner( $val );
        }
    }

    return ticket_vehicle_owner_table();
}

/*
| -------------------------------------------------------------------------------------
| Vehicle Owner Table List
| -------------------------------------------------------------------------------------
*/
function ticket_vehicle_owner_table()
{
	global $db;

    $s = 'SELECT * FROM ticket_transport_vehicle_owner ORDER BY void DESC';
    $q = $db->prepare_query( $s );
	$r = $db->do_query( $q );

    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/vehicle-owner/list.html', 'vehicle-owner' );

    add_block( 'list-block', 'lcblock', 'vehicle-owner' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', ticket_vehicle_owner_table_data( $r ) );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'transport&sub=vehicle-owner' ) );
    add_variable( 'add_new_link', get_state_url( 'transport&sub=vehicle-owner&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'transport&sub=vehicle-owner&prc=edit' ) );
    
    parse_template( 'list-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Vehicle Owner List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'vehicle-owner' );
}

/*
| -------------------------------------------------------------------------------------
| Vehicle Owner Table List Item
| -------------------------------------------------------------------------------------
*/
function ticket_vehicle_owner_table_data( $r, $i = 1 )
{
    global $db;
    
    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="3">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/vehicle-owner/loop.html', 'vehicle-owner-loop' );

    add_block( 'loop-block', 'lcloop', 'vehicle-owner-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'id', $d['void'] );
        add_variable( 'voname', $d['voname'] );
        add_variable( 'edit_link', get_state_url( 'transport&sub=vehicle-owner&prc=edit&id=' . $d['void'] ) );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-vehicle-owner-ajax/' );

        parse_template( 'loop-block', 'lcloop', true );
    }

    return return_template( 'vehicle-owner-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Vehicle Owner
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_vehicle_owner()
{
	$message = run_save_vehicle_owner();
    $data    = get_vehicle_owner();

	set_template( PLUGINS_PATH . '/ticket/tpl/vehicle-owner/form.html', 'vehicle-owner' );
    add_block( 'form-block', 'lcblock', 'vehicle-owner' );

    add_variable( 'void', $data['void'] );
    add_variable( 'voname', $data['voname'] );
    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'transport&sub=vehicle-owner&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=vehicle-owner' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'New Vehicle Owner' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'vehicle-owner' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Vehicle Owner
| -------------------------------------------------------------------------------------
*/
function ticket_edit_vehicle_owner()
{
    $message = run_update_vehicle_owner();
    $data    = get_vehicle_owner( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/vehicle-owner/form.html', 'vehicle-owner' );
    add_block( 'form-block', 'lcblock', 'vehicle-owner' );

    add_variable( 'void', $data['void'] );
    add_variable( 'voname', $data['voname'] );
    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'transport&sub=vehicle-owner&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-vehicle-owner-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'transport&sub=vehicle-owner' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'vehicle-owner' ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Edit Vehicle Owner' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'vehicle-owner' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Vehicle Owner
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_vehicle_owner()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/vehicle-owner/batch-delete.html', 'vehicle-owner' );
    add_block( 'loop-block', 'lclblock', 'vehicle-owner' );
    add_block( 'delete-block', 'lcblock', 'vehicle-owner' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_vehicle_owner( $val );

        add_variable('voname',  $d['voname'] );
        add_variable('void', $d['void'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' vehicle owner? :' );
    add_variable('action', get_state_url('transport&sub=vehicle-owner'));

    parse_template( 'delete-block', 'lcblock', false );
    
    add_actions( 'section_title', 'Delete vehicle-owner' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'vehicle-owner' );
}

function run_save_vehicle_owner()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_vehicle_owner_data();

        if( empty($error) )
        {
            $post_id = save_vehicle_owner();
            
            header( 'location:'.get_state_url( 'transport&sub=vehicle-owner&prc=add_new&result=1' ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'New vehicle owner successfully saved' ) );
    }
}

function run_update_vehicle_owner()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_vehicle_owner_data();

        if( empty($error) )
        {
            $post_id = update_vehicle_owner();
            
            header( 'location:'.get_state_url( 'transport&sub=vehicle-owner&prc=edit&result=1&id=' . $post_id ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This vehicle owner successfully edited' ) );
    }
}

function validate_vehicle_owner_data()
{
    $error = array();

    if( isset($_POST['voname']) && empty( $_POST['voname'] ) )
    {
        $error[] = 'Owner name can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle Owner List Count
| -------------------------------------------------------------------------------------
*/
function is_num_vehicle_owner()
{
    global $db;

    $s = 'SELECT * FROM ticket_transport_vehicle_owner';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle Owner By ID
| -------------------------------------------------------------------------------------
*/
function get_vehicle_owner( $id = '', $field = '' )
{
    global $db;

    $data = array( 
        'void'          => ( isset( $_POST['void'] ) ? $_POST['void'] : null ), 
        'voname'        => ( isset( $_POST['voname'] ) ? $_POST['voname'] : '' ), 
        'vocreateddate' => ( isset( $_POST['vocreateddate'] ) ? $_POST['vocreateddate'] : '' ), 
        'luser_id'      => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' )
    );

    $s = 'SELECT * FROM ticket_transport_vehicle_owner WHERE void = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'void' => $d['void'], 
                'voname' => $d['voname'],
                'vocreateddate' => $d['vocreateddate'],
                'luser_id' => $d['luser_id']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle Owner List
| -------------------------------------------------------------------------------------
*/
function get_vehicle_owner_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_transport_vehicle_owner';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array( 
            'void' => $d['void'], 
            'voname' => $d['voname'],
            'vocreateddate' => $d['vocreateddate'],
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Vehicle Owner
| -------------------------------------------------------------------------------------
*/
function save_vehicle_owner()
{
    global $db;
    
    $s = 'INSERT INTO ticket_transport_vehicle_owner( voname, vocreateddate, luser_id ) VALUES( %s, %s, %d )';
    $q = $db->prepare_query( $s, $_POST['voname'], date( 'Y-m-d H:i:s' ), $_COOKIE['user_id'] );
       
    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();
        
        save_log( $id, 'vehicle-owner', 'Add new vehicle owner - ' . $_POST['voname'] );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Vehicle Owner
| -------------------------------------------------------------------------------------
*/
function update_vehicle_owner()
{
    global $db;

    $d = get_vehicle_owner( $_POST['void'] );

    $s = 'UPDATE ticket_transport_vehicle_owner SET voname = %s WHERE void = %d';     
    $q = $db->prepare_query( $s, $_POST['voname'], $_POST['void'] );
       
    if( $db->do_query( $q ) )
    {
        if( $d['voname'] != $_POST['voname'] )
        {
            save_log( $_POST['void'], 'vehicle-owner', 'Update vehicle owner - ' . $d['voname'] . ' to ' . $_POST['voname'] );
        }
        else
        {
            save_log( $_POST['void'], 'vehicle-owner', 'Update vehicle owner - ' . $_POST['voname'] );
        }

        return $_POST['void'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Vehicle Owner
| -------------------------------------------------------------------------------------
*/
function delete_vehicle_owner( $id = '', $is_ajax = false )
{
    global $db;

    $d = get_vehicle_owner( $id );

    $s = 'DELETE FROM ticket_transport_vehicle_owner WHERE void = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'transport&sub=vehicle-owner&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'vehicle-owner', 'Delete vehicle owner - ' . $d['voname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Vehicle Owner Option
| -------------------------------------------------------------------------------------
*/
function get_vehicle_owner_option( $void = '' )
{
    $owner  = get_vehicle_owner_list();
    $option = '<option value="">Select Owner</option>';

    if( !empty( $owner ) )
    {
        foreach( $owner as $d )
        {
            $option .= '<option value="' . $d['void'] . '" ' . ( $d['void'] == $void ? 'selected' : '' ) . ' >' . $d['voname'] . '</option>';   
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_vehicle_owner_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-vehicle-owner' )
    {
        $d = delete_vehicle_owner( $_POST['void'], true );

        if( $d===true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}

?>