<?php

add_actions( 'route', 'ticket_route' );
add_actions( 'ticket-route-ajax_page', 'ticket_route_ajax' );

function ticket_route()
{
    if( is_num_route() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'schedules&sub=route&prc=add_new' ) );
    }

    if( is_add_new() )
    {
        return ticket_add_new_route();
    }
    elseif( is_edit() )
    {
    	if( isset( $_GET['id'] ) && get_route( $_GET['id'] ) )
    	{
        	return ticket_edit_route();
    	}
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_route();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key => $val )
        {
            delete_route( $val );
        }
    }

    return ticket_route_table();
}

/*
| -------------------------------------------------------------------------------------
| Route Table List
| -------------------------------------------------------------------------------------
*/
function ticket_route_table()
{
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/route/list.html', 'route' );

    add_block( 'list-block', 'lcblock', 'route' );

    add_variable( 'limit', 100 );
    add_variable( 'site_url', $site_url );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'schedules&sub=route' ) );
    add_variable( 'edit_link', get_state_url( 'schedules&sub=route&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=route&prc=add_new' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-route-ajax/' );

    add_actions( 'section_title', 'Route List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'list-block', 'lcblock', false );

    return return_template( 'route' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Route
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_route()
{
    $message = run_save_route();
    $data    = get_route();

    set_template( PLUGINS_PATH . '/ticket/tpl/route/form.html', 'route' );
    add_block( 'form-block', 'lcblock', 'route' );

    add_variable( 'rid', time() );
    add_variable( 'rname', $data['rname'] );
    add_variable( 'hopping_css', $data['rtype'] == '1' ? '' : 'sr-only' );
    add_variable( 'rclearance_chck', $data['rclearance'] == '1' ? 'checked' : '' );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'arrival', arrival_option( $data['rid'] ) );
    add_variable( 'hopping', hopping_option( $data['rid'] ) );
    add_variable( 'departure', departure_option( $data['rid'] ) );
    add_variable( 'clearance', clearance_option( $data['rid'] ) );
    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'rcot_option', get_cut_of_time_option( $data['rcot'] ) ) ;
    add_variable( 'rtype_option', get_route_type_option( $data['rtype'] ) );
    add_variable( 'tl_option', get_time_limit_option( $data['rtimelimit'] ) ) ;

    add_variable( 'delete_class', 'sr-only' );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=route' ) );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-route-ajax/' );
    add_variable( 'action', get_state_url( 'schedules&sub=route&prc=add_new' ) );
    add_variable( 'via_option', get_location_option( '', false, '' ) );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'New Route' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'route' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Route
| -------------------------------------------------------------------------------------
*/
function ticket_edit_route()
{
    $message  = run_update_route();
    $data     = get_route( $_GET['id'] );
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/route/form.html', 'route' );
    add_block( 'form-block', 'lcblock', 'route' );

    add_variable( 'rid', $data['rid'] );
    add_variable( 'rname', $data['rname'] );
    add_variable( 'hopping_css', $data['rtype'] == '1' ? '' : 'sr-only' );
    add_variable( 'rclearance_chck', $data['rclearance'] == '1' ? 'checked' : '' );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'arrival', arrival_option( $data['rid'] ) );
    add_variable( 'hopping', hopping_option( $data['rid'] ) );
    add_variable( 'departure', departure_option( $data['rid'] ) );
    add_variable( 'clearance', clearance_option( $data['rid'] ) );
    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'rcot_option', get_cut_of_time_option( $data['rcot'] ) ) ;
    add_variable( 'rtype_option', get_route_type_option( $data['rtype'] ) );
    add_variable( 'tl_option', get_time_limit_option( $data['rtimelimit'] ) ) ;
    add_variable( 'last_update_note', get_last_update_note( $data['rid'], 'route' ) );
    add_variable( 'via_option', get_location_option( '', false, '' ) );
    add_variable( 'via_id', $data['via_id'] );

    add_variable( 'delete_class', '' );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=route' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-route-ajax/' );
    add_variable( 'action', get_state_url( 'schedules&sub=route&prc=edit&id=' . $data['rid'] ) );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'Edit Route Location' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'route' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Route
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_route()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/route/batch-delete.html', 'route' );
    add_block( 'loop-block', 'lclblock', 'route' );
    add_block( 'delete-block', 'lcblock', 'route' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_route( $val );

        add_variable('rname',  $d['rname'] );
        add_variable('rid', $d['rid'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' route? :' );
    add_variable('action', get_state_url('schedules&sub=route'));

    parse_template( 'delete-block', 'lcblock', false );

    add_actions( 'section_title', 'Delete Route Location' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'route' );
}

function departure_option( $rid = '', $idx = 0 )
{
    $departure = get_route_detail_list( $rid );
    
    set_template( PLUGINS_PATH . '/ticket/tpl/route/location.html', 'departure-location' );
    add_block( 'location-block', 'dlocblock', 'departure-location' );

    if( empty( $departure ) )
    {
        if( isset( $_POST['lcid'][0] ) )
        {
            extract( $_POST );

            foreach( $lcid[0] as $idx => $lcid )
            {
                $pd_css    = isset( $rdpickup[0][$idx] ) ? '' : 'hidden';
                $check_opt = isset( $rdpickup[0][$idx] ) ? 'checked' : '';
                $time_val  = isset( $rdtime[0][$idx] ) ? date( 'H:i', strtotime( $rdtime[0][$idx] ) ) : '';
                $time_val_2  = isset( $rdtime2[0][$idx] ) ? date( 'H:i', strtotime( $rdtime2[0][$idx] ) ) : '';
                
                add_variable( 'i', $idx );
                add_variable( 'type', 0 );
                add_variable( 'time_label', 'ETA' );
                add_variable( 'time_label_2', 'ETD' );
                add_variable( 'type_label', 'Pickup' );
                add_variable( 'row_id', 'row-departure-' . $idx );
                add_variable( 'label', 'Departure ' . ( $idx + 1 ) );
                add_variable( 'include_label', 'Include Pickup' );
                add_variable( 'checkbox_id', 'rdpickup-' . $idx );
                add_variable( 'checkbox_name', 'rdpickup[0][' . $idx . ']' );
                add_variable( 'check_opt', $check_opt );
                add_variable( 'pickup_drop_css', $pd_css );
                add_variable( 'rdtime', $time_val );
                add_variable( 'rdtime2', $time_val_2 );
                add_variable( 'no_data_css', empty( $check_opt ) ? '' : 'sr-only' );
                add_variable( 'location_option', get_location_option( $lcid ) );
                add_variable( 'pickup_drop_location_option', get_pickup_drop_location_option( 0, $idx, null, $lcid ) );

                parse_template( 'location-block', 'dlocblock', true );
            }

            return return_template( 'departure-location' );
        }
    }
    else
    {
        foreach( $departure as $idx => $d )
        {
            $rpd = get_route_pickup_drop_list( $d['rdid'] );

            add_variable( 'i', $idx );
            add_variable( 'type', 0 );
            add_variable( 'time_label', 'ETA' );
            add_variable( 'time_label_2', 'ETD' );
            add_variable( 'type_label', 'Pickup' );
            add_variable( 'row_id', 'row-departure-' . $idx );
            add_variable( 'label', 'Departure ' . ( $idx + 1 ) );
            add_variable( 'include_label', 'Include Pickup' );
            add_variable( 'checkbox_id', 'rdpickup-' . $idx );
            add_variable( 'checkbox_name', 'rdpickup[0][' . $idx . ']' );
            add_variable( 'check_opt', count( $rpd ) > 0 ? 'checked' : '' );
            add_variable( 'pickup_drop_css', count( $rpd ) > 0 ? '' : 'hidden' );
            add_variable( 'rdtime', date( 'H:i', strtotime( $d['rdtime'] ) ) );
            add_variable( 'rdtime2', date( 'H:i', strtotime( $d['rdtime2'] ) ) );
            add_variable( 'location_option', get_location_option( $d['lcid'] ) );
            add_variable( 'no_data_css', empty( $rpd ) ? '' : 'sr-only' );
            add_variable( 'pickup_drop_location_option', get_pickup_drop_location_option( 0, $idx, $d['rdid'], $d['lcid'], $rpd ) );

            parse_template( 'location-block', 'dlocblock', true );
        }

        return return_template( 'departure-location' );
    }
}

function add_new_departure_option( $rid = '', $idx = 0 )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/route/location.html', 'departure-location' );
    add_block( 'location-block', 'dlocblock', 'departure-location' );

    add_variable( 'i', $idx );
    add_variable( 'type', 0 );
    add_variable( 'time_label', 'ETA' );
    add_variable( 'time_label_2', 'ETD' );
    add_variable( 'type_label', 'Pickup' );
    add_variable( 'row_id', 'row-departure-' . $idx );
    add_variable( 'label', 'Departure ' . ( $idx + 1 ) );
    add_variable( 'include_label', 'Include Pickup' );
    add_variable( 'checkbox_id', 'rdpickup-' . $idx );
    add_variable( 'checkbox_name', 'rdpickup[0][' . $idx . ']' );
    add_variable( 'check_opt', '' );
    add_variable( 'pickup_drop_css', 'hidden' );
    add_variable( 'rdtime', '' );
    add_variable( 'rdtime2', '' );
    add_variable( 'location_option', get_location_option() );
    add_variable( 'no_data_css', '' );
    add_variable( 'pickup_drop_location_option', get_pickup_drop_location_option( 0, $idx ) );

    parse_template( 'location-block', 'dlocblock', false );

    return return_template( 'departure-location' );
}

function clearance_option( $rid = '', $idx = 0 )
{
    $clearance = get_route_detail_list( $rid, 3 );

    set_template( PLUGINS_PATH . '/ticket/tpl/route/clearance.html', 'clearance-location' );
    add_block( 'clearance-block', 'clocblock', 'clearance-location' );

    if( empty( $clearance ) )
    {
        if( isset( $_POST['lcid'][3] ) )
        {
            extract( $_POST );

            foreach( $lcid[3] as $idx => $lcid )
            {
                $etd_val = isset( $rdtime[3][$idx] ) ? date( 'H:i', strtotime( $rdtime[3][$idx] ) ) : '';
                $eta_val = isset( $rdtime2[3][$idx] ) ? date( 'H:i', strtotime( $rdtime2[3][$idx] ) ) : '';

                add_variable( 'rdtime', $etd_val );
                add_variable( 'rdtime2', $eta_val );
                add_variable( 'location_option', get_location_where_option( $lcid, array( 'lctype' => 3 ) ) );

                parse_template( 'clearance-block', 'clocblock', true );

                return return_template( 'clearance-location' );
            }
        }
        else
        {
            add_variable( 'rdtime', '' );
            add_variable( 'rdtime2', '' );
            add_variable( 'location_option', get_location_where_option( array( 'lctype' => 3 ) ) );

            parse_template( 'clearance-block', 'clocblock', true );

            return return_template( 'clearance-location' );
        }
    }
    else
    {
        foreach( $clearance as $idx => $d )
        {
            add_variable( 'rdtime', date( 'H:i', strtotime( $d['rdtime'] ) ) );
            add_variable( 'rdtime2', date( 'H:i', strtotime( $d['rdtime2'] ) ) );
            add_variable( 'location_option', get_location_where_option( array( 'lctype' => 3 ), $d['lcid'] ) );

            parse_template( 'clearance-block', 'clocblock', true );
        }

        return return_template( 'clearance-location' );
    }
}

function hopping_option( $rid = '', $idx = 0 )
{
    $hopping = get_route_detail_list( $rid, 2 );

    set_template( PLUGINS_PATH . '/ticket/tpl/route/hopping.html', 'hopping-location' );
    add_block( 'hopping-block', 'hlocblock', 'hopping-location' );

    if( empty( $hopping ) )
    {
        if( isset( $_POST['lcid'][2] ) )
        {
            extract( $_POST );

            foreach( $lcid[2] as $idx => $lcid )
            {
                $pc_css    = isset( $rdpickup[2][$idx] ) ? '' : 'hidden';
                $pc_check  = isset( $rdpickup[2][$idx] ) ? 'checked' : '';
                $do_css    = isset( $rddropoff[2][$idx] ) ? '' : 'hidden';
                $do_check  = isset( $rddropoff[2][$idx] ) ? 'checked' : '';
                $etd_val   = isset( $rdtime[2][$idx] ) ? date( 'H:i', strtotime( $rdtime[2][$idx] ) ) : '';
                $eta_val   = isset( $rdtime2[2][$idx] ) ? date( 'H:i', strtotime( $rdtime2[2][$idx] ) ) : '';

                add_variable( 'i', $idx );
                add_variable( 'rdtime', $etd_val );
                add_variable( 'rdtime2', $eta_val );
                add_variable( 'pickup_check_opt', $pc_check );
                add_variable( 'dropoff_check_opt', $do_check );
                add_variable( 'pickup_css', $pc_css );
                add_variable( 'dropoff_css', $do_css );
                add_variable( 'row_id', 'row-hopping-' . $idx );
                add_variable( 'label', 'Hopping Point ' . ( $idx + 1 ) );
                add_variable( 'location_option', get_location_option( $lcid ) );
                add_variable( 'pickup_no_data_css', empty( $pc_check ) ? '' : 'sr-only' );
                add_variable( 'dropoff_no_data_css', empty( $do_check ) ? '' : 'sr-only' );
                add_variable( 'pickup_hopping_option', get_pickup_drop_hopping_option( 2, $idx, true, null, $lcid ) );
                add_variable( 'dropoff_hopping_option', get_pickup_drop_hopping_option( 2, $idx, false, null, $lcid ) );

                parse_template( 'hopping-block', 'hlocblock', true );

                return return_template( 'hopping-location' );
            }
        }
    }
    else
    {
        foreach( $hopping as $idx => $d )
        {
            $rpd = get_route_pickup_drop_hopping_list( $d['rdid'] );

            $pc_css    = isset( $rpd['pickup'] ) ? '' : 'hidden';
            $pc_check  = isset( $rpd['pickup'] ) ? 'checked' : '';
            $do_css    = isset( $rpd['drop-off'] ) ? '' : 'hidden';
            $do_check  = isset( $rpd['drop-off'] ) ? 'checked' : '';

            add_variable( 'i', $idx );
            add_variable( 'rdtime', date( 'H:i', strtotime( $d['rdtime'] ) ) );
            add_variable( 'rdtime2', date( 'H:i', strtotime( $d['rdtime2'] ) ) );
            add_variable( 'pickup_check_opt', $pc_check );
            add_variable( 'dropoff_check_opt', $do_check );
            add_variable( 'pickup_css', $pc_css );
            add_variable( 'dropoff_css', $do_css );
            add_variable( 'row_id', 'row-hopping-' . $idx );
            add_variable( 'label', 'Hopping Point ' . ( $idx + 1 ) );
            add_variable( 'location_option', get_location_option( $d['lcid'] ) );
            add_variable( 'pickup_no_data_css', empty( $pc_check ) ? '' : 'sr-only' );
            add_variable( 'dropoff_no_data_css', empty( $do_check ) ? '' : 'sr-only' );
            add_variable( 'pickup_hopping_option', get_pickup_drop_hopping_option( 2, $idx, true, null, $d['lcid'], $rpd ) );
            add_variable( 'dropoff_hopping_option', get_pickup_drop_hopping_option( 2, $idx, false, null, $d['lcid'], $rpd ) );

            parse_template( 'hopping-block', 'hlocblock', true );
        }

        return return_template( 'hopping-location' );
    }
}

function add_new_hopping_option( $rid = '', $idx = 0 )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/route/hopping.html', 'hopping-location' );
    add_block( 'hopping-block', 'hlocblock', 'hopping-location' );

    add_variable( 'i', $idx );
    add_variable( 'rdtime', '' );
    add_variable( 'rdtime2', '' );
    add_variable( 'pickup_check_opt', '' );
    add_variable( 'dropoff_check_opt', '' );
    add_variable( 'pickup_css', 'hidden' );
    add_variable( 'dropoff_css', 'hidden' );
    add_variable( 'row_id', 'row-hopping-' . $idx );
    add_variable( 'label', 'Hopping Point ' . ( $idx + 1 ) );
    add_variable( 'location_option', get_location_option() );
    add_variable( 'pickup_hopping_option', get_pickup_drop_hopping_option( 2, $idx, true ) );
    add_variable( 'dropoff_hopping_option', get_pickup_drop_hopping_option( 2, $idx, false ) );

    parse_template( 'hopping-block', 'hlocblock', false );

    return return_template( 'hopping-location' );
}

function arrival_option( $rid = '', $idx = 0 )
{
    $arrival = get_route_detail_list( $rid, 1 );

    set_template( PLUGINS_PATH . '/ticket/tpl/route/location.html', 'arrival-location' );
    add_block( 'location-block', 'alocblock', 'arrival-location' );

    if( empty( $arrival ) )
    {
        if( isset( $_POST['lcid'][1] ) )
        {
            extract( $_POST );

            foreach( $lcid[1] as $idx => $lcid )
            {
                $pd_css    = isset( $rddropoff[1][$idx] ) ? '' : 'hidden';
                $check_opt = isset( $rddropoff[1][$idx] ) ? 'checked' : '';
                $time_val  = isset( $rdtime[1][$idx] ) ? date( 'H:i', strtotime( $rdtime[1][$idx] ) ) : '';
                $time_val_2  = isset( $rdtime2[1][$idx] ) ? date( 'H:i', strtotime( $rdtime2[1][$idx] ) ) : '';
                
                add_variable( 'i', $idx );
                add_variable( 'type', 1 );
                add_variable( 'time_label', 'ETA' );
                add_variable( 'time_label_2', 'ETD' );
                add_variable( 'type_label', 'Drop-Off' );
                add_variable( 'row_id', 'row-arrival-' . $idx );
                add_variable( 'label', 'Arrival ' . ( $idx + 1 ) );
                add_variable( 'include_label', 'Include Drop-Off' );
                add_variable( 'checkbox_id', 'rddropoff-' . $idx );
                add_variable( 'checkbox_name', 'rddropoff[1][' . $idx . ']' );
                add_variable( 'check_opt', $check_opt );
                add_variable( 'pickup_drop_css', $pd_css );
                add_variable( 'rdtime', $time_val );
                add_variable( 'rdtime2', $time_val_2 );
                add_variable( 'no_data_css', empty( $check_opt ) ? '' : 'sr-only' );
                add_variable( 'location_option', get_location_option( $lcid ) );
                add_variable( 'pickup_drop_location_option', get_pickup_drop_location_option( 1, $idx, null, $lcid ) );

                parse_template( 'location-block', 'alocblock', true );
            }

            return return_template( 'arrival-location' );
        }
    }
    else
    {
        foreach( $arrival as $idx => $d )
        {
            $rpd = get_route_pickup_drop_list( $d['rdid'] );
            add_variable( 'i', $idx );
            add_variable( 'type', 1 );
            add_variable( 'time_label', 'ETA' );
            add_variable( 'time_label_2', 'ETD' );
            add_variable( 'type_label', 'Drop-Off' );
            add_variable( 'row_id', 'row-arrival-' . $idx );
            add_variable( 'label', 'Arrival ' . ( $idx + 1 ) );
            add_variable( 'include_label', 'Include Drop-Off' );
            add_variable( 'checkbox_id', 'rddropoff-' . $idx );
            add_variable( 'checkbox_name', 'rddropoff[1][' . $idx . ']' );
            add_variable( 'check_opt', count( $rpd ) > 0 ? 'checked' : '' );
            add_variable( 'pickup_drop_css', count( $rpd ) > 0 ? '' : 'hidden' );
            add_variable( 'rdtime', date( 'H:i', strtotime( $d['rdtime'] ) ) );
            add_variable( 'rdtime2', date( 'H:i', strtotime( $d['rdtime2'] ) ) );
            add_variable( 'location_option', get_location_option( $d['lcid'] ) );
            add_variable( 'no_data_css', empty( $rpd ) ? '' : 'sr-only' );
            add_variable( 'pickup_drop_location_option', get_pickup_drop_location_option( 1, $idx, $d['rdid'], $d['lcid'], $rpd ) );

            parse_template( 'location-block', 'alocblock', true );
        }

        return return_template( 'arrival-location' );
    }
}

function add_new_arrival_option( $rid = '', $idx = 0 )
{
    set_template( PLUGINS_PATH . '/ticket/tpl/route/location.html', 'arrival-location' );
    add_block( 'location-block', 'alocblock', 'arrival-location' );

    add_variable( 'i', $idx );
    add_variable( 'type', 1 );
    add_variable( 'time_label', 'ETA' );
    add_variable( 'time_label_2', 'ETD' );
    add_variable( 'type_label', 'Drop-Off' );
    add_variable( 'row_id', 'row-arrival-' . $idx );
    add_variable( 'label', 'Arrival ' . ( $idx + 1 ) );
    add_variable( 'include_label', 'Include Drop-Off' );
    add_variable( 'checkbox_id', 'rddropoff-' . $idx );
    add_variable( 'checkbox_name', 'rddropoff[1][' . $idx . ']' );
    add_variable( 'check_opt', '' );
    add_variable( 'pickup_drop_css', 'hidden' );
    add_variable( 'rdtime', '' );
    add_variable( 'rdtime2', '' );
    add_variable( 'location_option', get_location_option() );
    add_variable( 'pickup_drop_location_option', get_pickup_drop_location_option( 1, $idx ) );

    parse_template( 'location-block', 'alocblock', false );

    return return_template( 'arrival-location' );
}

function run_save_route()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_route_data();

        if( empty($error) )
        {
            $post_id = save_route();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new route' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New route successfully saved' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=route&prc=add_neww' ) );

                exit;
            }
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'New route successfully saved' ) );
    }
}

function run_update_route()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_route_data();

        if( empty($error) )
        {
            $post_id = update_route();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new route' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This route successfully edited' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=route&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This route successfully edited' ) );
    }
}

function validate_route_data()
{
    $error = array();

    if( isset( $_POST['rname'] ) && empty( $_POST['rname'] ) )
    {
        $error[] = 'Route name can\'t be empty';
    }

    if( isset( $_POST['lcid'] ) && !empty( $_POST['lcid'] ) )
    {
        $i = 0;

        foreach( $_POST['lcid'] as $key => $obj )
        {
            foreach( $obj as $idx => $lcid )
            {
                if( $key == 3 && isset( $_POST['rclearance'] ) && empty( $lcid ) )
                {
                    $i++;
                }
                else if( $key != 3 && empty( $lcid ) )
                {
                    $i++;
                }
            }
        }

        if( $i > 0 )
        {
            $error[] = 'Location name can\'t be empty';
        }
    }

    if( isset( $_POST['rdpickup'] ) && !empty( $_POST['rdpickup'] ) )
    {
        $i = 0;

        foreach( $_POST['rdpickup'] as $key => $obj )
        {
            if( $key == 2 )
            {
                foreach( $obj as $idx => $val )
                {
                    if( !isset( $_POST['hoptaid'][$key][$idx][0] ) )
                    {
                        $i++;
                    }
                }
            }
            else
            {
                foreach( $obj as $idx => $val )
                {
                    if( !isset( $_POST['taid'][$key][$idx] ) )
                    {
                        $i++;
                    }
                }
            }
        }

        if( $i > 0 )
        {
            $error[] = 'Please select pickup location';
        }
    }

    if( isset( $_POST['rddropoff'] ) && !empty( $_POST['rddropoff'] ) )
    {
        $i = 0;

        foreach( $_POST['rddropoff'] as $key => $obj )
        {
            if( $key == 2 )
            {
                foreach( $obj as $idx => $val )
                {
                    if( !isset( $_POST['hoptaid'][$key][$idx][1] ) )
                    {
                        $i++;
                    }
                }
            }
            else
            {
                foreach( $obj as $idx => $val )
                {
                    if( !isset( $_POST['taid'][$key][$idx] ) )
                    {
                        $i++;
                    }
                }
            }
        }

        if( $i > 0 )
        {
            $error[] = 'Please select drop-off location';
        }
    }

    if( isset( $_POST['taid'] ) && !empty( $_POST['taid'] ) )
    {
        $i = 0;

        foreach( $_POST['taid'] as $key => $obj )
        {
            foreach( $obj as $idx => $val )
            {
                if( isset( $_POST['taid'][$key][$idx] ) )
                {
                    foreach( $_POST['taid'][$key][$idx] as $ii => $id )
                    {
                        if( isset( $_POST['rpfrom'][$key][$idx][$ii][$id] ) && empty( $_POST['rpfrom'][$key][$idx][$ii][$id] ) )
                        {
                            $i++;
                        }

                        if( isset( $_POST['rpto'][$key][$idx][$ii][$id] ) && empty( $_POST['rpto'][$key][$idx][$ii][$id] ) )
                        {
                            $i++;
                        }
                    }
                }
            }
        }

        if( $i > 0 )
        {
            $error[] = 'Please fill time field if you include pick up / drop-off';
        }
    }

    if( isset( $_POST['hoptaid'] ) && !empty( $_POST['hoptaid'] ) )
    {
        $i = 0;

        if( isset( $_POST['hoptaid'][2][$idx][0] ) )
        {
            foreach( $_POST['hoptaid'][2][$idx][0] as $ii => $id )
            {
                if( isset( $_POST['hoprpfrom'][2][$idx][0][$ii][$id] ) && empty( $_POST['hoprpfrom'][2][$idx][0][$ii][$id] ) )
                {
                    $i++;
                }

                if( isset( $_POST['hoprpto'][2][$idx][0][$ii][$id] ) && empty( $_POST['hoprpto'][2][$idx][0][$ii][$id] ) )
                {
                    $i++;
                }
            }
        }

        if( isset( $_POST['hoptaid'][2][$idx][1] ) )
        {
            foreach( $_POST['hoptaid'][2][$idx][1] as $ii => $id )
            {
                if( isset( $_POST['hoprpfrom'][2][$idx][1][$ii][$id] ) && empty( $_POST['hoprpfrom'][2][$idx][1][$ii][$id] ) )
                {
                    $i++;
                }

                if( isset( $_POST['hoprpto'][2][$idx][1][$ii][$id] ) && empty( $_POST['hoprpto'][2][$idx][1][$ii][$id] ) )
                {
                    $i++;
                }
            }
        }

        if( $i > 0 )
        {
            $error[] = 'Please fill time field if you include pick up / drop-off';
        }
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Route List Count
| -------------------------------------------------------------------------------------
*/
function is_num_route()
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM ticket_route' );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    return $n;
}

/*
| -------------------------------------------------------------------------------------
| Route Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_route_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        0 => 'a.rorder',
        2 => 'a.rname',
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.rorder ASC';
    }

    $s = 'SELECT * FROM ticket_route AS a ORDER BY ' . $order;
    $r = $db->do_query( $s );
    $n = $db->num_rows( $r );

    $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
    $r2 = $db->do_query( $s2 );
    $n2 = $db->num_rows( $r2 );

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $s3 = 'SELECT b.lcname, a.rdtime
                   FROM ticket_route_detail AS a
                   LEFT JOIN ticket_location AS b ON a.lcid = b.lcid
                   WHERE a.rid = %s ORDER BY a.rdid';
            $q3 = $db->prepare_query( $s3, $d2['rid'] );
            $r3 = $db->do_query( $q3 );
            $n3 = $db->num_rows( $r3 );

            $route = array();
            $etd   = '';
            $eta   = '';
            $idx   = 0;

            while( $d3 = $db->fetch_array( $r3 ) )
            {
                if( $idx == 0 )
                {
                    $etd = $d3['rdtime'];
                }
                elseif( $idx == ( $n3 - 1 ) )
                {
                    $eta = $d3['rdtime'];
                }

                $route[] = $d3['lcname'];

                $idx++;
            }

            $data[] = array(
                'rid'       => $d2['rid'],
                'rname'     => $d2['rname'],
                'rroute'    => implode( ' - ', $route ),
                'retd'      => date( 'H:i', strtotime( $etd ) ),
                'reta'      => date( 'H:i', strtotime( $eta ) ),
                'edit_link' => get_state_url( 'schedules&sub=route&prc=edit&id=' . $d2['rid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Route By ID
| -------------------------------------------------------------------------------------
*/
function get_route( $id = '', $field = '' )
{
    global $db;

    $data = array(
        'rid'           => ( isset( $_POST['rid'] ) ? $_POST['rid'] : time() ),
        'rname'         => ( isset( $_POST['rname'] ) ? $_POST['rname'] : '' ),
        'rstatus'       => ( isset( $_POST['rstatus'] ) ? $_POST['rstatus'] : '' ),
        'rdepart'       => ( isset( $_POST['rdepart'] ) ? $_POST['rdepart'] : '' ),
        'rarrive'       => ( isset( $_POST['rarrive'] ) ? $_POST['rarrive'] : '' ),
        'rcot'          => ( isset( $_POST['rcot'] ) ? $_POST['rcot'] : '' ),
        'rtimelimit'    => ( isset( $_POST['rtimelimit'] ) ? $_POST['rtimelimit'] : '' ),
        'rtype'         => ( isset( $_POST['rtype'] ) ? $_POST['rtype'] : '' ),
        'rhoppingpoint' => ( isset( $_POST['rhoppingpoint'] ) ? $_POST['rhoppingpoint'] : '' ),
        'rcreateddate'  => ( isset( $_POST['rcreateddate'] ) ? $_POST['rcreateddate'] : '' ),
        'rclearance'    => ( isset( $_POST['rclearance'] ) ? $_POST['rclearance'] : '' ),
        'via_id'        => ( isset( $_POST['via_id'] ) ? $_POST['via_id'] : '' ),
        'luser_id'      => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' )
    );

    $s = 'SELECT * FROM ticket_route WHERE rid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'rid'           => $d['rid'],
                'rname'         => $d['rname'],
                'rstatus'       => $d['rstatus'],
                'rdepart'       => $d['rdepart'],
                'rarrive'       => $d['rarrive'],
                'rcot'          => $d['rcot'],
                'rtimelimit'    => $d['rtimelimit'],
                'rtype'         => $d['rtype'],
                'rhoppingpoint' => $d['rhoppingpoint'],
                'rcreateddate'  => $d['rcreateddate'],
                'rclearance'    => $d['rclearance'],
                'via_id'        => $d['via_id'],
                'luser_id'      => $d['luser_id'],
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

function get_route_multiple( $rid = array(), $field = '' )
{
    global $db;

    $data = array();

    if( is_array( $rid ) )
    {
        if( empty( $field ) )
        {
            $q = 'SELECT * FROM ticket_route AS a WHERE a.rid IN("' . implode( '", "', $rid ) . '")';
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $data[] = array(
                        'rid'           => $d['rid'],
                        'rname'         => $d['rname'],
                        'rstatus'       => $d['rstatus'],
                        'rdepart'       => $d['rdepart'],
                        'rarrive'       => $d['rarrive'],
                        'rcot'          => $d['rcot'],
                        'rtimelimit'    => $d['rtimelimit'],
                        'rtype'         => $d['rtype'],
                        'rhoppingpoint' => $d['rhoppingpoint'],
                        'rcreateddate'  => $d['rcreateddate'],
                		'via_id'        => $d['via_id'],
                        'luser_id'      => $d['luser_id'],
                    );
                }
            }
        }
        else
        {
            $q = 'SELECT ' . $field . ' FROM ticket_route AS a WHERE a.rid IN("' . implode( '", "', $rid ) . '")';
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $data[] = $d[ $field ];
                }
            }
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Route List
| -------------------------------------------------------------------------------------
*/
function get_route_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_route AS a ORDER BY a.rorder';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'rid' => $d['rid'],
            'rname' => $d['rname'],
            'rstatus' => $d['rstatus'],
            'rcreateddate' => $d['rcreateddate'],
            'luser_id' => $d['luser_id'],
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Route Detail By rid
| -------------------------------------------------------------------------------------
*/
function get_route_detail_by_id( $rid )
{
    global $db;

    $s = 'SELECT * FROM ticket_route_detail WHERE rid = %d';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d;
}

/*
| -------------------------------------------------------------------------------------
| Get Route Detail By rid & lcid
| -------------------------------------------------------------------------------------
*/
function get_route_detail_by_id_and_loc( $rid, $lcid )
{
    global $db;

    $s = 'SELECT * FROM ticket_route_detail WHERE rid = %d AND lcid = %d';
    $q = $db->prepare_query( $s, $rid, $lcid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d;
}

/*
| -------------------------------------------------------------------------------------
| Get Route Detail By rid & rdtype
| -------------------------------------------------------------------------------------
*/
function get_route_detail_list( $rid = '', $type = 0 )
{
    global $db;

    $s = 'SELECT * FROM ticket_route_detail WHERE rid = %d AND rdtype= %s';
    $q = $db->prepare_query( $s, $rid, $type );
    $r = $db->do_query( $q );

    $detail = array();

    while( $d = $db->fetch_array( $r ) )
    {
       $detail[] = $d;
    }

    return $detail;
}

function get_route_point_list( $rid = '', $type = 0 )
{
    global $db;

    $s = 'SELECT * FROM ticket_route_detail WHERE rid = %d AND rdtype= %s';
    $q = $db->prepare_query( $s, $rid, $type );
    $r = $db->do_query( $q );

    $detail = array();

    while( $d = $db->fetch_array( $r ) )
    {
       $detail[] = $d['lcid'];
    }

    return $detail;
}

/*
| -------------------------------------------------------------------------------------
| Get Route Pickup - Dropoff By rdid
| -------------------------------------------------------------------------------------
*/
function get_route_pickup_drop_list( $rdid='', $field='' )
{
    global $db;

    $s = 'SELECT * FROM ticket_route_pickup_drop WHERE rdid = %d';
    $q = $db->prepare_query( $s, $rdid );
    $r = $db->do_query( $q );

    $pickup_drop = array();

    while( $d = $db->fetch_array( $r ) )
    {
        if( empty( $field ) )
        {
            $pickup_drop[] = $d;
        }
        else
        {
           $pickup_drop[] = $d[$field];
        }
    }

    return $pickup_drop;
}

/*
| -------------------------------------------------------------------------------------
| Get Route Hooping Pickup - Dropoff By rdid
| -------------------------------------------------------------------------------------
*/
function get_route_pickup_drop_hopping_list( $rdid='', $field='' )
{
    global $db;

    $s = 'SELECT * FROM ticket_route_pickup_drop WHERE rdid = %d';
    $q = $db->prepare_query( $s, $rdid );
    $r = $db->do_query( $q );

    $pickup_drop = array();

    while( $d = $db->fetch_array( $r ) )
    {
        if( empty( $field ) )
        {
            $pickup_drop[ $d['rptype'] ] = $d;
        }
        else
        {
           $pickup_drop[ $d['rptype'] ] = $d[$field];
        }
    }

    return $pickup_drop;
}

function get_depart_and_arrive_loc()
{
    if( !empty( $_POST['lcid'] ) && is_array( $_POST['lcid'] ) )
    {
        $depart  = isset( $_POST['lcid'][0] ) ? $_POST['lcid'][0][ min( array_keys( $_POST['lcid'][0] ) ) ] : 0;
        $arrive  = isset( $_POST['lcid'][1] ) ? $_POST['lcid'][1][ max( array_keys( $_POST['lcid'][1] ) ) ] : 0;
        $hopping = isset( $_POST['lcid'][2] ) ? $_POST['lcid'][2][ max( array_keys( $_POST['lcid'][2] ) ) ] : 0;

        return array( 'depart' => $depart, 'arrive' => $arrive, 'hopping' => $hopping );
    }
    else
    {
        return array( 'depart' => 0, 'arrive' => 0, 'hopping' => 0 );
    }
}

/*
| -------------------------------------------------------------------------------------
| Reset Route Order
| -------------------------------------------------------------------------------------
*/
function reset_route_order()
{
    global $db;

    $s = 'UPDATE ticket_route AS a SET a.rorder = a.rorder + 1';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
         return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Route
| -------------------------------------------------------------------------------------
*/
function save_route()
{
    global $db;

    $loc    = get_depart_and_arrive_loc();
    $status = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );
    $via_id = empty( $_POST['via_id'] ) ? '0' : $_POST['via_id'];
    $rclear = isset( $_POST['rclearance'] ) ? '1' : '0';

    $s = 'INSERT INTO ticket_route(
            rname,
            rstatus,
            rdepart,
            rhoppingpoint,
            rarrive,
            rcot,
            rtimelimit,
            rtype,
            rcreateddate,
            rclearance,
            via_id,
            luser_id)
          VALUES( %s, %s, %d, %d, %d, %d, %d, %s, %s, %s, %d, %d )';
    $q = $db->prepare_query( $s,
            $_POST['rname'],
            $status,
            $loc['depart'],
            $loc['hopping'],
            $loc['arrive'],
            $_POST['rcot'],
            $_POST['rtimelimit'],
            $_POST['rtype'],
            date( 'Y-m-d H:i:s' ),
            $rclear,
            $via_id,
            $_COOKIE['user_id'] );

    if( reset_route_order() )
    {
        $r = $db->do_query( $q );

        if( !is_array( $r ) )
        {
            $rid = $db->insert_id();

            save_route_detail( $rid );

            save_log( $rid, 'route', 'Add new route - ' . $_POST['rname'] );

            return $rid;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Detail Route
| -------------------------------------------------------------------------------------
*/
function save_route_detail( $rid = '' )
{
    global $db;

    if( !empty( $_POST['lcid'] ) && is_array( $_POST['lcid'] ) )
    {
        foreach( $_POST['lcid'] as $type => $obj )
        {
            if( $type == 2 )
            {
                foreach( $obj as $i => $lcid )
                {
                    $s = 'INSERT INTO ticket_route_detail( rid, lcid, rdtype, rdtime, rdtime2 ) VALUES( %d, %d, %s, %s, %s )';
                    $q = $db->prepare_query( $s, $rid, $lcid, $type, $_POST['rdtime'][$type][$i], $_POST['rdtime2'][$type][$i] );

                    if( $db->do_query( $q ) )
                    {
                        $rdid = $db->insert_id();

                        save_route_pickup_drop( $rdid, $type, $i );
                    }
                }
            }
            elseif( $type == 3 )
            {
                foreach( $obj as $i => $lcid )
                {
                    $s = 'INSERT INTO ticket_route_detail( rid, lcid, rdtype, rdtime, rdtime2 ) VALUES( %d, %d, %s, %s, %s )';
                    $q = $db->prepare_query( $s, $rid, $lcid, $type, $_POST['rdtime'][$type][$i], $_POST['rdtime2'][$type][$i] );

                    if( $db->do_query( $q ) )
                    {
                        $rdid = $db->insert_id();
                    }
                }
            }
            else
            {
                foreach( $obj as $i => $lcid )
                {
                    $s = 'INSERT INTO ticket_route_detail( rid, lcid, rdtype, rdtime, rdtime2 ) VALUES( %d, %d, %s, %s, %s )';
                    $q = $db->prepare_query( $s, $rid, $lcid, $type, $_POST['rdtime'][$type][$i], $_POST['rdtime2'][$type][$i] );

                    if( $db->do_query( $q ) )
                    {
                        $rdid = $db->insert_id();

                        save_route_pickup_drop( $rdid, $type, $i );
                    }
                }
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Route Pick / Drop
| -------------------------------------------------------------------------------------
*/
function save_route_pickup_drop( $rdid = '', $type = 0, $i = 0 )
{
    global $db;

    if( $type == 2 )
    {
        if( isset( $_POST['hoptaid'][$type][$i][0] ) && !empty( $_POST['hoptaid'][$type][$i][0] ) && is_array( $_POST['hoptaid'][$type][$i][0] ) )
        {
            foreach( $_POST['hoptaid'][$type][$i][0] as $idx => $taid )
            {
                $s = 'INSERT INTO ticket_route_pickup_drop( rdid, taid, rpfrom, rpto, rpcot, rptype ) VALUES( %d, %d, %s, %s, %d, %s )';
                $q = $db->prepare_query( $s, $rdid, $taid, $_POST['hoprpfrom'][$type][$i][0][$idx][$taid], $_POST['hoprpto'][$type][$i][0][$idx][$taid], $_POST['hoprpcot'][$type][$i][0][$idx][$taid], 'pickup' );

                if( $db->do_query( $q ) )
                {
                    $pdid = $db->insert_id();
                }
            }
        }

        if( isset( $_POST['hoptaid'][$type][$i][1] ) && !empty( $_POST['hoptaid'][$type][$i][1] ) && is_array( $_POST['hoptaid'][$type][$i][1] ) )
        {
            foreach( $_POST['hoptaid'][$type][$i][1] as $idx => $taid )
            {
                $s = 'INSERT INTO ticket_route_pickup_drop( rdid, taid, rpfrom, rpto, rpcot, rptype ) VALUES( %d, %d, %s, %s, %d, %s )';
                $q = $db->prepare_query( $s, $rdid, $taid, $_POST['hoprpfrom'][$type][$i][1][$idx][$taid], $_POST['hoprpto'][$type][$i][1][$idx][$taid], $_POST['hoprpcot'][$type][$i][1][$idx][$taid], 'drop-off' );

                if( $db->do_query( $q ) )
                {
                    $pdid = $db->insert_id();
                }
            }
        }
    }
    else
    {
        if( isset( $_POST['taid'][$type][$i] ) && !empty( $_POST['taid'][$type][$i] ) && is_array( $_POST['taid'][$type][$i] ) )
        {
            $rptype = isset( $_POST['rdpickup'][$type][$i] ) ? 'pickup' : 'drop-off';

            foreach( $_POST['taid'][$type][$i] as $idx => $taid )
            {
                $s = 'INSERT INTO ticket_route_pickup_drop( rdid, taid, rpfrom, rpto, rpcot, rptype ) VALUES( %d, %d, %s, %s, %d, %s )';
                $q = $db->prepare_query( $s, $rdid, $taid, $_POST['rpfrom'][$type][$i][$idx][$taid], $_POST['rpto'][$type][$i][$idx][$taid], $_POST['rpcot'][$type][$i][$idx][$taid], $rptype );

                if( $db->do_query( $q ) )
                {
                    $pdid = $db->insert_id();
                }
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Route Location
| -------------------------------------------------------------------------------------
*/
function update_route()
{
    global $db;

    $loc     = get_depart_and_arrive_loc();
    $current = get_route( $_POST['rid'] );
    $status  = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );
    $via_id  = empty( $_POST['via_id'] ) ? '0' : $_POST['via_id'];
    $rclear  = isset( $_POST['rclearance'] ) ? '1' : '0';

    $s = 'UPDATE ticket_route SET
            rname = %s,
            rstatus = %s,
            rdepart = %d,
            rhoppingpoint = %s,
            rarrive = %d,
            rcot = %d,
            rtimelimit = %d,
            rtype = %s,
            via_id = %d,
            rclearance = %s
          WHERE rid = %d';
    $q = $db->prepare_query( $s,
            $_POST['rname'],
            $status,
            $loc['depart'],
            $loc['hopping'],
            $loc['arrive'],
            $_POST['rcot'],
            $_POST['rtimelimit'],
            $_POST['rtype'],
            $via_id,
            $rclear,
            $_POST['rid'] );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return '';
    }
    else
    {
        update_route_detail( $_POST['rid'] );

        $logs = array();
        $typ  = array( 'Standard route', 'Use hopping point' );
        $clr  = array( 'Used clearance point', 'Not used clearance point' );

        $logs[] = $current['rname'] != $_POST['rname'] ? 'Change Route Name : ' . $current['rname'] . ' to ' . $_POST['rname'] : '';
        $logs[] = $current['rcot'] != $_POST['rcot'] ? 'Change Cut Of Time (COT) : ' . $current['rcot'] . ' to ' . $_POST['rcot'] : '';
        $logs[] = $current['rtimelimit'] != $_POST['rtimelimit'] ? 'Change Booking Time Limit OB/Agent Panel : ' . $current['rtimelimit'] . ' to ' . $_POST['rtimelimit'] : '';
        $logs[] = $current['rtype'] != $_POST['rtype'] ? 'Change Route Type : ' . $typ[ $current['rtype'] ] . ' to ' . $typ[ $_POST['rtype'] ] : '';
        $logs[] = $current['rclearance'] != $rclear ? 'Change Route Type : ' . $clr[ $current['rclearance'] ] . ' to ' . $rclear : '';

        if ( !empty( $logs ) )
        {
            foreach ( $logs as $i => $log )
            {
                if ( $log == '' )
                {
                    unset( $logs[$i] );
                }
            }

            save_log( $_POST['rid'], 'route', 'Update route - ' . $current['rname'] . '<br/>' . implode( '<br/>', $logs ) );
        }

        return $_POST['rid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Route Detail
| -------------------------------------------------------------------------------------
*/
function update_route_detail( $rid='' )
{
    global $db;

    if( !empty( $_POST['lcid'] ) && is_array( $_POST['lcid'] ) )
    {
        $s = 'DELETE FROM ticket_route_detail WHERE rid = %d';
        $q = $db->prepare_query( $s, $rid );

        if( $db->do_query( $q ) )
        {
            save_route_detail( $rid );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Route Order
| -------------------------------------------------------------------------------------
*/
function update_route_order( $id, $order )
{
    global $db;

    $s = 'UPDATE ticket_route AS a SET rorder = %d WHERE a.rid = %d';
    $q = $db->prepare_query( $s, $order, $id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Route
| -------------------------------------------------------------------------------------
*/
function delete_route( $rid = '', $is_ajax = false )
{
    global $db;

    $d = get_route( $rid );

    $s = 'DELETE FROM ticket_route WHERE rid = %d';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );

    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'schedules&sub=route&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $rid, 'route', 'Delete route - ' . $d['rname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Pickup / Dropoff Location Option
| -------------------------------------------------------------------------------------
*/
function get_pickup_drop_location_option( $type=0, $i=0, $rdid='', $lcid='', $rpd=array() )
{
    $locs = get_transport_area_list( true );
    $opt  = '';

    if( !empty( $locs ) )
    {
        if( empty( $_POST ) )
        {
            $no = 1;

            foreach( $locs as $key => $loc )
            {
                $sty  = !empty( $lcid ) && $lcid == $key ? '' : 'sr-only';
                $opt .= '
                <div class="item-wrapp ' . $sty . '" data-loc-id="' . $key . '">';

                    foreach( $loc as $idx => $dt )
                    {
                        $opt .= '
                        <div class="row-eq-height">';
                            foreach( $dt as $ii => $d )
                            {
                                $rpcot       = '';
                                $rpfrom      = '';
                                $rpto        = '';
                                $check_opt   = '';
                                $plctime_cls = 'hidden';

                                foreach( $rpd as $obj )
                                {
                                    if( $d['taid'] == $obj['taid'] )
                                    {
                                        $rpfrom      = empty( $sty ) ? date( 'H:i', strtotime( $obj['rpfrom'] ) ) : '';
                                        $rpto        = empty( $sty ) ? date( 'H:i', strtotime( $obj['rpto'] ) ) : '';
                                        $rpcot       = empty( $sty ) ? $obj['rpcot'] : '';
                                        $check_opt   = empty( $sty ) ? 'checked' : '';
                                        $plctime_cls = '';

                                        break;
                                    }
                                }

                                $opt .= '
                                <div class="item">
                                    <div class="checkbox">
                                        <input id="check-opt-' . $d['taid'] . '" class="use-pick-drop-location" value="' . $d['taid'] . '" name="taid[' .$type . '][' . $i . '][' . $key . '-' . $no . ']" autocomplete="off" type="checkbox" ' . $check_opt . ' >
                                        <label for="check-opt-' . $d['taid'] . '">' . $d['taname'] . '</label>
                                    </div>
                                    <div class="plctime ' . $plctime_cls . '">
                                        <div class="form-group">
                                            <p class="subtitle">From</p>
                                            <input id="rpfrom-' . $d['taid'] . '-' . $type . '-' . $i . '" type="text" class="text text-small form-control timepicker" name="rpfrom[' .$type . '][' . $i . '][' .  $key .'-'. $no . '][' . $d['taid'] . ']" value="' . $rpfrom . '" autocomplete="off" />
                                        </div>
                                        <div class="form-group">
                                            <p class="subtitle">To</p>
                                            <input id="rpto-' . $d['taid'] . '-' . $type . '-' . $i . '" type="text" class="text text-small form-control timepicker" name="rpto[' .$type . '][' . $i . '][' .  $key .'-'. $no . '][' . $d['taid'] . ']" value="' . $rpto . '" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="plctime ' . $plctime_cls . '">
                                        <div class="form-group ' . ( $d['taairport'] == '1' ? 'cot' : 'sr-only' ) . '">
                                            <p class="subtitle">COT</p>
                                            <select name="rpcot[' .$type . '][' . $i . '][' .  $key .'-'. $no . '][' . $d['taid'] . ']" autocomplete="off">
                                                ' . get_cut_of_time_option( $rpcot ) . '
                                            </select>
                                        </div>
                                    </div>
                                </div>';

                                $no++;
                            }
                            $opt .= '
                        </div>';
                    }
                    $opt .= '
                </div>';
            }
        }
        else
        {
            $no = 1;

            foreach( $locs as $key => $loc )
            {
                $sty  = !empty( $lcid ) && $lcid == $key ? '' : 'sr-only';
                $opt .= '
                <div class="item-wrapp ' . $sty . '" data-loc-id="' . $key . '">';

                    foreach( $loc as $idx => $dt )
                    {
                        $opt .= '
                        <div class="row-eq-height">';

                            foreach( $dt as $ii => $d )
                            {
                                $rpcot       = '';
                                $rpfrom      = '';
                                $rpto        = '';
                                $check_opt   = '';
                                $taid_val    = '';
                                $plctime_cls = 'hidden';

                                if( isset( $_POST['taid'][$type][$i][$key .'-'. $no] ) )
                                {
                                    $taid_val = $_POST['taid'][$type][$i][$key .'-'. $no];
                                }

                                if( !empty( $taid_val ) && isset( $_POST['rpfrom'][$type][$i][$key .'-'. $no][$taid_val] ) )
                                {
                                    $rpfrom = $_POST['rpfrom'][$type][$i][$key .'-'. $no][$taid_val];
                                }

                                if( !empty( $taid_val ) && isset( $_POST['rpto'][$type][$i][$key .'-'. $no][$taid_val] ) )
                                {
                                    $rpto = $_POST['rpto'][$type][$i][$key .'-'. $no][$taid_val];
                                }

                                if( !empty( $taid_val ) && isset( $_POST['rpcot'][$type][$i][$key .'-'. $no][$taid_val] ) )
                                {
                                    $rpcot = $_POST['rpcot'][$type][$i][$key .'-'. $no][$taid_val];
                                }

                                if( $taid_val == $d['taid'] )
                                {
                                    $check_opt   = 'checked';
                                    $plctime_cls = '';
                                }

                                $opt .= '
                                <div class="item">
                                    <div class="checkbox">
                                        <input id="check-opt-' . $d['taid'] . '" class="use-pick-drop-location" value="' . $d['taid'] . '" name="taid[' .$type . '][' . $i . '][' . $key .'-'. $no . ']" autocomplete="off" type="checkbox" ' . $check_opt . ' >
                                        <label for="check-opt-' . $d['taid'] . '">' . $d['taname'] . '</label>
                                    </div>
                                    <div class="plctime ' . $plctime_cls . '">
                                        <div class="form-group">
                                            <p class="subtitle">From</p>
                                            <input id="rpfrom-' . $d['taid'] . '-' . $type . '-' . $i . '" type="text" class="text text-small form-control timepicker" name="rpfrom[' .$type . '][' . $i . '][' .  $key .'-'. $no . '][' . $d['taid'] . ']" value="' . $rpfrom . '" autocomplete="off" />
                                        </div>
                                        <div class="form-group">
                                            <p class="subtitle">To</p>
                                            <input id="rpto-' . $d['taid'] . '-' . $type . '-' . $i . '" type="text" class="text text-small form-control timepicker" name="rpto[' .$type . '][' . $i . '][' .  $key .'-'. $no . '][' . $d['taid'] . ']" value="' . $rpto . '" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="plctime ' . $plctime_cls . '">
                                        <div class="form-group ' . ( $d['taairport'] == '1' ? 'cot' : 'sr-only' ) . '">
                                            <p class="subtitle">COT</p>
                                            <select name="rpcot[' .$type . '][' . $i . '][' .  $key .'-'. $no . '][' . $d['taid'] . ']" autocomplete="off">
                                                ' . get_cut_of_time_option( $rpcot ) . '
                                            </select>
                                        </div>
                                    </div>
                                </div>';

                                $no++;
                            }

                            $opt .= '
                        </div>';
                    }
                    $opt .= '
                </div>';
            }
        }
    }

    return $opt;
}

function get_pickup_drop_hopping_option( $type = 0, $i = 0, $is_pickup = false, $rdid = '', $lcid = '', $rpd = array() )
{
    $locs  = get_transport_area_list( true );
    $ttype = $is_pickup ? '0' : '1';
    $opt   = '';

    if( !empty( $locs ) )
    {
        if( empty( $_POST ) )
        {
            $no = 1;

            foreach( $locs as $key => $loc )
            {
                $sty  = !empty( $lcid ) && $lcid == $key ? '' : 'sr-only';
                $opt .= '
                <div class="item-wrapp ' . $sty . '" data-loc-id="' . $key . '">';

                    foreach( $loc as $idx => $dt )
                    {
                        $opt .= '
                        <div class="row-eq-height">';
                            foreach( $dt as $ii => $d )
                            {
                                $rpcot       = '';
                                $rpfrom      = '';
                                $rpto        = '';
                                $check_opt   = '';
                                $plctime_cls = 'hidden';

                                foreach( $rpd as $obj )
                                {
                                    if( $d['taid'] == $obj['taid'] )
                                    {
                                        $rpfrom      = empty( $sty ) ? date( 'H:i', strtotime( $obj['rpfrom'] ) ) : '';
                                        $rpto        = empty( $sty ) ? date( 'H:i', strtotime( $obj['rpto'] ) ) : '';
                                        $rpcot       = empty( $sty ) ? $obj['rpcot'] : '';
                                        $check_opt   = empty( $sty ) ? 'checked' : '';
                                        $plctime_cls = '';

                                        break;
                                    }
                                }

                                $opt .= '
                                <div class="item">
                                    <div class="checkbox">
                                        <input id="check-opt-' . $d['taid'] . '" class="use-pick-drop-location" value="' . $d['taid'] . '" name="hoptaid[' .$type . '][' . $i . '][' . $ttype . '][' . $key . '-' . $no . ']" autocomplete="off" type="checkbox" ' . $check_opt . ' >
                                        <label for="check-opt-' . $d['taid'] . '">' . $d['taname'] . '</label>
                                    </div>
                                    <div class="plctime ' . $plctime_cls . '">
                                        <div class="form-group">
                                            <p class="subtitle">From</p>
                                            <input id="rpfrom-' . $d['taid'] . '-' . $type . '-' . $i . '" type="text" class="text text-small form-control timepicker" name="hoprpfrom[' .$type . '][' . $i . '][' . $ttype . '][' . $key . '-' . $no . '][' . $d['taid'] . ']" value="' . $rpfrom . '" autocomplete="off" />
                                        </div>
                                        <div class="form-group">
                                            <p class="subtitle">To</p>
                                            <input id="rpto-' . $d['taid'] . '-' . $type . '-' . $i . '" type="text" class="text text-small form-control timepicker" name="hoprpto[' .$type . '][' . $i . '][' . $ttype . '][' . $key . '-' . $no . '][' . $d['taid'] . ']" value="' . $rpto . '" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="plctime ' . $plctime_cls . '">
                                        <div class="form-group ' . ( $d['taairport'] == '1' ? 'cot' : 'sr-only' ) . '">
                                            <p class="subtitle">COT</p>
                                            <select name="hoprpcot[' .$type . '][' . $i . '][' . $ttype . '][' . $key . '-' . $no . '][' . $d['taid'] . ']" autocomplete="off">
                                                ' . get_cut_of_time_option( $rpcot ) . '
                                            </select>
                                        </div>
                                    </div>
                                </div>';

                                $no++;
                            }
                            $opt .= '
                        </div>';
                    }
                    $opt .= '
                </div>';
            }
        }
        else
        {
            $no = 1;

            foreach( $locs as $key => $loc )
            {
                $sty  = !empty( $lcid ) && $lcid == $key ? '' : 'sr-only';
                $opt .= '
                <div class="item-wrapp ' . $sty . '" data-loc-id="' . $key . '">';

                    foreach( $loc as $idx => $dt )
                    {
                        $opt .= '
                        <div class="row-eq-height">';

                            foreach( $dt as $ii => $d )
                            {
                                $rpcot       = '';
                                $rpfrom      = '';
                                $rpto        = '';
                                $check_opt   = '';
                                $taid_val    = '';
                                $plctime_cls = 'hidden';

                                if( isset( $_POST['hoptaid'][$type][$i][$ttype][$key . '-' . $no] ) )
                                {
                                    $taid_val = $_POST['hoptaid'][$type][$i][$ttype][$key . '-' . $no];
                                }

                                if( !empty( $taid_val ) && isset( $_POST['hoprpfrom'][$type][$i][$ttype][$key . '-' . $no][$taid_val] ) )
                                {
                                    $rpfrom = $_POST['hoprpfrom'][$type][$i][$ttype][$key . '-' . $no][$taid_val];
                                }

                                if( !empty( $taid_val ) && isset( $_POST['hoprpto'][$type][$i][$ttype][$key . '-' . $no][$taid_val] ) )
                                {
                                    $rpto = $_POST['hoprpto'][$type][$i][$ttype][$key . '-' . $no][$taid_val];
                                }

                                if( !empty( $taid_val ) && isset( $_POST['hoprpcot'][$type][$i][$ttype][$key . '-' . $no][$taid_val] ) )
                                {
                                    $rpcot = $_POST['hoprpcot'][$type][$i][$ttype][$key . '-' . $no][$taid_val];
                                }

                                if( $taid_val == $d['taid'] )
                                {
                                    $check_opt   = 'checked';
                                    $plctime_cls = '';
                                }

                                $opt .= '
                                <div class="item">
                                    <div class="checkbox">
                                        <input id="check-opt-' . $d['taid'] . '" class="use-pick-drop-location" value="' . $d['taid'] . '" name="hoptaid[' .$type . '][' . $i . '][' . $ttype . '][' . $key . '-' . $no . ']" autocomplete="off" type="checkbox" ' . $check_opt . ' >
                                        <label for="check-opt-' . $d['taid'] . '">' . $d['taname'] . '</label>
                                    </div>
                                    <div class="plctime ' . $plctime_cls . '">
                                        <div class="form-group">
                                            <p class="subtitle">From</p>
                                            <input id="rpfrom-' . $d['taid'] . '-' . $type . '-' . $i . '" type="text" class="text text-small form-control timepicker" name="hoprpfrom[' .$type . '][' . $i . '][' . $ttype . '][' . $key . '-' . $no . '][' . $d['taid'] . ']" value="' . $rpfrom . '" autocomplete="off" />
                                        </div>
                                        <div class="form-group">
                                            <p class="subtitle">To</p>
                                            <input id="rpto-' . $d['taid'] . '-' . $type . '-' . $i . '" type="text" class="text text-small form-control timepicker" name="hoprpto[' .$type . '][' . $i . '][' . $ttype . '][' . $key . '-' . $no . '][' . $d['taid'] . ']" value="' . $rpto . '" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="plctime ' . $plctime_cls . '">
                                        <div class="form-group ' . ( $d['taairport'] == '1' ? 'cot' : 'sr-only' ) . '">
                                            <p class="subtitle">COT</p>
                                            <select name="hoprpcot[' .$type . '][' . $i . '][' . $ttype . '][' . $key . '-' . $no . '][' . $d['taid'] . ']" autocomplete="off">
                                                ' . get_cut_of_time_option( $rpcot ) . '
                                            </select>
                                        </div>
                                    </div>
                                </div>';

                                $no++;
                            }

                            $opt .= '
                        </div>';
                    }
                    $opt .= '
                </div>';
            }
        }
    }

    return $opt;
}

/*
| -------------------------------------------------------------------------------------
| Get Route Option
| -------------------------------------------------------------------------------------
*/
function get_route_option( $rid = '', $use_empty = true, $empty_text = 'Select Route' )
{
    $route  = get_route_list();
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $route ) )
    {
        if ( is_array( $rid ) )
        {
            foreach( $route as $d )
            {
                $option .= '<option value="' . $d['rid'] . '" ' . (  in_array( $d['rid'], $rid ) ? 'selected' : '' ) . ' >' . $d['rname'] . '</option>';
            }
        }
        else
        {
            foreach( $route as $d )
            {
                $option .= '<option value="' . $d['rid'] . '" ' . (  $d['rid'] == $rid ? 'selected' : '' ) . ' >' . $d['rname'] . '</option>';
            }
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Cut Of Time Option
| -------------------------------------------------------------------------------------
*/
function get_cut_of_time_option( $rcot = '' )
{
    $option = '';

    for( $i = 0; $i <= 24; $i++ )
    {
        $option .= '<option value="' . $i . '" ' . ( $rcot == $i ? 'selected' : '' ) . ' >' . $i . ' Hours</option>';
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Route Type Option
| -------------------------------------------------------------------------------------
*/
function get_route_type_option( $rtype )
{
    return '
    <option value="0" ' . ( $rtype == '0' ? 'selected' : '' ) . '>Standard route</option>
    <option value="1" ' . ( $rtype == '1' ? 'selected' : '' ) . '>Use hopping point</option>';
}

/*
| -------------------------------------------------------------------------------------
| Get Time Limit Option
| -------------------------------------------------------------------------------------
*/
function get_time_limit_option( $rtimelimit = 0 )
{
    $option = '';

    for( $i = 0; $i <= 24; $i++ )
    {
        $option .= '<option value="' . $i . '" ' . ( $rtimelimit == $i ? 'selected' : '' ) . ' >' . $i . ' Hours</option>';
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_route_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_route_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'reorder-data' )
    {
        $result = array( 'result' => 'failed' );

        if( empty( $_POST['reorder'] ) === false )
        {
            foreach( $_POST['reorder'] as $id => $order )
            {
                if( update_route_order( $id, $order ) )
                {
                    $result = array( 'result' => 'success' );
                }
            }
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-route' )
    {
        $d = delete_route( $_POST['rid'], true );

        if( $d===true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-departure' )
    {
        $departure = add_new_departure_option( null, $_POST['idx'] );

        if( empty( $departure ) )
        {
            echo '{"result":"failed"}';
        }
        else
        {
            echo json_encode( array( 'result'=>'success', 'data'=>$departure ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-hopping' )
    {
        $hopping = add_new_hopping_option( null, $_POST['idx'] );

        if( empty( $hopping ) )
        {
            echo '{"result":"failed"}';
        }
        else
        {
            echo json_encode( array( 'result'=>'success', 'data'=>$hopping ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-arrival' )
    {
        $arrival = add_new_arrival_option( null, $_POST['idx'] );

        if( empty( $arrival ) )
        {
            echo '{"result":"failed"}';
        }
        else
        {
            echo json_encode( array( 'result'=>'success', 'data'=>$arrival ) );
        }
    }
}

?>