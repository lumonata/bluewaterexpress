<?php

date_default_timezone_set( get_meta_data( 'time_zone' ) );

define( 'FILE', date("Ymd_His").'bwebackup.sql' );
define( 'FILEZIP', date("Ymd_His").'bwebackup' );
define( 'PATH_BACKUP', ROOT_PATH .'/backup/' );
define( 'PATH_RESTORE_TEMP', ROOT_PATH .'/backup/temp/' );

/*
| -------------------------------------------------------------------------------------
| Setting - List Table
| -------------------------------------------------------------------------------------
*/
function list_table()
{
	$tables = array(
		'ticket_booking_cancel',
		'ticket_port_clearance_detail',
		'ticket_port_clearance',
		'ticket_trip_transport_detail',
		'ticket_trip_transport',
		'ticket_booking_transport',
		'ticket_booking_payment',
		'ticket_booking_passenger',
		'ticket_booking_detail',
		'ticket_booking_by',
		'ticket_notification',
		'ticket_feedback',
		'ticket_agent_transaction',
		'ticket_booking',

		'ticket_allotment_detail',
		'ticket_allotment_agent',
		'ticket_allotment_detail_online',
		'ticket_allotment_online',
		'ticket_allotment',
		'ticket_agent_net_rate',
		'ticket_agent_freelance_code',
		'ticket_agent',

		'ticket_transport_vehicle',
		'ticket_transport_vehicle_owner',
		'ticket_transport_driver',
		'ticket_transport_area_fee',
		'ticket_route_pickup_drop',
		'ticket_hotel',
		'ticket_transport_area',

		'ticket_exception_detail',
		'ticket_exception',
		'ticket_promo',
		'ticket_booking_payment_method',

		'ticket_schedule',
		'ticket_boat_attachment',
		'ticket_boat',
		'ticket_rate_seat_discount',
		'ticket_rate_route_detail',
		'ticket_rate_early_discount',
		'ticket_rate_hopping_detail',
		'ticket_rate_detail',
		'ticket_rate',
		'ticket_route_detail',
		'ticket_route',
		'ticket_rate_period',
		'ticket_channel',
		'ticket_location',

		'l_country',
		'l_log',
	);

	return $tables;
}

/*
| -------------------------------------------------------------------------------------
| Setting - Backup Process
| -------------------------------------------------------------------------------------
*/
function data_backup_process( $type = 'zip' )
{
	global $db;

	$tables = list_table();
	$return = '';

	if ( !empty( $tables ) ) 
	{
		foreach ( array_reverse( $tables ) as $table ) 
		{
			$row_fields = $db->do_query( 'SHOW COLUMNS FROM ' . $table );
			$fields     = array();
			$fd 		= array();		

			while( $d = $db->fetch_array( $row_fields ) )
			{
				$fd[] = $d['Field'];
				$fields[ $d['Field'] ][] = $d['Null'];
			}

			if( !empty( $fields ) )
			{
				$result  = $db->do_query( 'SELECT * FROM '. $table );
				$num_row = $db->num_rows($result);
				$counter = 0;

				if ( $num_row > 0 ) 
				{
					$return .= 'INSERT INTO ' . $table . ' ( ' . implode( ',', $fd ) . ' ) VALUES' . "\n";

					while( $row = $db->fetch_array( $result ) )
					{
						$r = array();

						foreach( $fields as $i => $f )
						{
							if ( $row[ $i ] != '' ) 
							{
								$row[ $i ] = str_replace( ";", " ", $row[ $i ] );
								$row[ $i ] = str_replace( '"', "'", $row[ $i ] );
								$row[ $i ] = utf8_encode ( $row[ $i ] );

								$r[]	   = '"' . $row[ $i ] . '"';
							}
							else
							{
								if ( $f[0] == 'YES' ) 
								{
									$r[] = 'NULL';
								}
								else
								{
									$r[] = '""';
								}
							}
						}

						if ( ++$counter == $num_row ) 
						{
							$return .= '( ' . implode( ',', $r ) . ' );' . "\n";
						}
						else
						{
							$return .= '( ' . implode( ',', $r ) . ' ),' . "\n";
						}
					}
				}
			}
		}

		if ( !file_exists( PATH_BACKUP ) ) 
		{
			mkdir( ROOT_PATH . '/backup/', 0777 );
		}

		switch( $type )
		{
			case "sql":						
				$handle = fopen( PATH_BACKUP .FILE,'w+' );
				fwrite( $handle, $return );
				fclose( $handle );

				break;
			case "zip":
				$zip 		= new ZipArchive() ;
				$resOpen 	= $zip->open( PATH_BACKUP . FILEZIP . ".zip" , ZIPARCHIVE::CREATE ) ;

				if( $resOpen )
				{
					$zip->addFromString( FILE , $return );
				}

				$zip->close() ;

				break;
		}

		return 1;
	} 
	else 
	{
		return 0;
	}	
}

/*
| -------------------------------------------------------------------------------------
| Setting - Restore Process
| -------------------------------------------------------------------------------------
*/
function delete_data_all_table_list()
{
	global $db;

	$tables  = list_table();

	if ( !empty( $tables ) ) 
	{
		foreach ( $tables as $table ) 
		{
			$db->do_query( "DELETE FROM " . $table );
		}

		return true;
	}
	else
	{
		return false;
	}
}

function delete_temporary( $target ) 
{
    if( is_dir( $target ) )
    {
        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

        foreach( $files as $file )
        {
            delete_temporary( $file );      
        }

        rmdir( $target );
    } 
    elseif( is_file( $target ) ) 
    {
        unlink( $target );  
    }

    return true;
}

function data_restore_process( $files )
{
	if ( delete_data_all_table_list() ) 
	{
		$ext_file = pathinfo( PATH_BACKUP . $files, PATHINFO_EXTENSION );

		switch( $ext_file )
		{
			case 'zip':
				$dm = delete_temporary( PATH_RESTORE_TEMP );

				if ( !$dm ) 
				{
					return array( 'result'=>'failed', 'message' => 'failed to delete temporary folder' );
				} 
				else 
				{
					if ( !file_exists( PATH_RESTORE_TEMP ) ) 
					{
						mkdir( PATH_BACKUP . 'temp/', 0777 );
					}

					$zip = new ZipArchive;

					if ( $zip->open( PATH_BACKUP . $files ) ) 
					{
						$zip->extractTo( PATH_RESTORE_TEMP );

						$zip->close();
					}

					$scanfile = preg_grep( '/^([^.])/', scandir( PATH_RESTORE_TEMP ) );
					$files 	  = array_values( $scanfile );
					$result   = import_sql( PATH_RESTORE_TEMP . $files[0] );

					unlink( PATH_RESTORE_TEMP . $files[0] );

					if ( $result == 'success' ) 
					{
						return array( 'result'=>'success' );
					} 
					else 
					{
						return array( 'result'=>'failed', 'message' => $result );
					}
				}
				break;
			case 'sql':
				$result = import_sql( PATH_BACKUP . $files );

				if ( $result == 'success' ) 
				{
					return array( 'result'=>'success' );
				} 
				else 
				{
					return array( 'result'=>'failed', 'message' => $result );
				}
				break;
		}
	} 
	else 
	{
		return array( 'result'=>'failed', 'message'=>'Failed delete data table' );
	}
}

function import_sql( $path )
{
	global $db;

	$f        = fopen( $path, "r+" );
	$sqlFile  = fread( $f, filesize( $path ) );
	$sqlArray = explode( ';', $sqlFile );
	$error    = '';

	foreach ( $sqlArray as $stmt ) 
	{
	  	if ( strlen( $stmt ) > 3 && substr( ltrim( $stmt ), 0, 2 ) != '/*' && substr( ltrim( $stmt ), 0, 2 ) != '--' ) 
	  	{
	    	$result = $db->do_query( $stmt );

		    if ( is_array( $result ) ) 
		    {
		    	$error = $stmt . ' ->' . $result['message'] . ' ->' . $result['error_code'];

		      	break;
		    }
	  	}
	}

	if ( $error == '' ) 
	{
		return 'success';
	} 
	else
	{
		return 'failed' . " -> " . $error;
	}
}

/*
| -------------------------------------------------------------------------------------
| Setting - Delete File
| -------------------------------------------------------------------------------------
*/
function delete_file_backup( $file )
{
	if( file_exists( PATH_BACKUP . $file ) )
    {
        unlink( PATH_BACKUP . $file );

        return array( 'result' => 'success' );
    }
    else
    {
        return array( 'result'=>'failed', 'message'=>'Failed to delete this file' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Setting - List File
| -------------------------------------------------------------------------------------
*/
function list_file_backup( $dir = PATH_BACKUP )
{
	if ( !file_exists( PATH_BACKUP ) ) 
	{
		mkdir( ROOT_PATH . '/backup/', 0777 );
	}

	$retval = [];

	if( substr( $dir, -1 ) != "/") 
	{
	  $dir .= "/";
	}
	
	$d = @dir( $dir ) or die( "Warning!: Failed opening backup file" );
	while( FALSE !== ( $entry = $d->read() ) ) 
	{
		if( $entry{0} == "." ) 
			continue;
		if( !is_dir( "{$dir}{$entry}" ) )
		{
			if( is_readable( "{$dir}{$entry}" ) ) 
			{
				$retval[] = [
					'name' 		=> "{$dir}{$entry}",
					'size' 		=> format_size_unit( filesize( "{$dir}{$entry}" ) ),
					'lastmod' 	=> filemtime( "{$dir}{$entry}" )
				];
			}
		}
	}

	$d->close();
	
	return $retval;
}

function format_size_unit( $bytes )
{
    if ( $bytes >= 1073741824 )
    {
        $bytes = number_format( $bytes / 1073741824, 2 ) . ' GB';
    }
    elseif ($bytes >= 1048576 )
    {
        $bytes = number_format( $bytes / 1048576, 2 ) . ' MB';
    }
    elseif ( $bytes >= 1024 )
    {
        $bytes = number_format( $bytes / 1024, 2 ) . ' KB';
    }
    elseif ( $bytes > 1 )
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ( $bytes == 1 )
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}

?>