<?php

add_actions( 'new-booking', 'ticket_new_booking' );
add_actions( 'new-booking-ajax_page', 'ticket_new_booking_ajax' );

function ticket_new_booking()
{
    $sid_arr  = array();
    $site_url = site_url();
    $booking  = get_booking_post_data();

    extract( $booking );
    
    if( run_save_new_booking() )
    {
        header( 'Location:' . get_state_url( 'reservation&sub=booking' ) );
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/new-booking/form.html', 'new-booking' );

    add_block( 'no-departure-trip-block', 'ndtblock', 'new-booking' );
    add_block( 'no-return-trip-block', 'nrtblock', 'new-booking' );

    add_block( 'departure-trip-loop-block', 'dtlblock', 'new-booking' );
    add_block( 'departure-trip-block', 'dtblock', 'new-booking' );

    add_block( 'return-trip-loop-block', 'rtlblock', 'new-booking' );
    add_block( 'return-trip-block', 'rtblock', 'new-booking' );

    add_block( 'new-booking-block', 'ablock', 'new-booking' );

    if( isset( $_POST['check_trip_availability'] ) )
    {
        global $db;

        $arr = explode( '|', $booking_source );

        add_variable( 'bpaymethod_css', '' );
        add_variable( 'bpaymethod_option', get_payment_method_option( true, 6 ) );

        if( count( $arr ) == 2 )
        {
            $chid = $arr[0];
            $agid = $arr[1];

            $agent = get_agent( $agid );

            $bbname  = $agent['agname'];
            $bbphone = $agent['agphone'];
            $bbemail = $agent['agemail'];

            if( $agent['agpayment_type'] == 'Credit' )
            {
                add_variable( 'bpaymethod_css', 'sr-only' );
                add_variable( 'bpaymethod_option', get_payment_method_option( true, 8 ) );
            }

            add_variable( 'agtype', $agent['agpayment_type'] );
        }
        else
        {
            $chid = $arr[0];
            $agid = null;

            add_variable( 'agtype', '' );
        }

        $pass_num   = $adult + $child + $infant;
        $compliment = array( 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 );

        //-- Departure Trip
        if( !empty( $destination_from ) && !empty( $destination_to ) && !empty( $depart_date ) )
        {
            $depart_date = date( 'Y-m-d', strtotime( $depart_date ) );

            $s = 'SELECT * FROM ticket_schedule AS a
                  LEFT JOIN ticket_route AS b ON a.rid = b.rid
                  LEFT JOIN ticket_boat AS c ON a.boid = c.boid
                  WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
            $q = $db->prepare_query( $s, $depart_date, $depart_date, 'publish', 'publish', '1' );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
                $i = 0;

                while( $d = $db->fetch_array( $r ) )
                {
                    $available_depart  = is_available_depart_on_list( $d['rid'], $destination_from, $depart_date, true );
                    $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_to );

                    if( strtotime( $depart_date ) < time() )
                    {
                        $not_closed_date        = true;
                        $available_date         = true;
                        $is_available_allotment = is_available_allotment( $depart_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $destination_from, $destination_to, $agid, false );
                    }
                    else
                    {
                        $not_closed_date        = is_date_not_in_close_allotment_list( $agid, $d['sid'], $depart_date );
                        $available_date         = is_available_date_on_list( $d['sid'], $depart_date, true );
                        $is_available_allotment = is_available_allotment( $depart_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $destination_from, $destination_to, $agid, false );
                    }

                    if( $available_depart && $available_arrival && $available_date )
                    {
                        if( $is_available_allotment )
                        {
                            $p  = get_result_price( $d['sid'], $d['rid'], $depart_date, $destination_from, $destination_to, $adult, $child, $infant, $agid );
                            $rd = get_route_detail_content( $d['rid'], $destination_from, $destination_to );

                            $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                            $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                            $trip_inc_transport_val = base64_encode( json_encode( array( $d['sid'], 1 ) ) );
                            $trip_exc_transport_val = base64_encode( json_encode( array( $d['sid'], 0 ) ) );

                            if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                            {
                                $sid_arr[] = $d['sid'];

                                add_variable( 'sid', $d['sid'] );
                                add_variable( 'rid', $d['rid'] );
                                add_variable( 'boid', $d['boid'] );
                                add_variable( 'boname', $d['boname'] );

                                add_variable( 'bddeparttime', $rd['depart_time'] );
                                add_variable( 'bdarrivetime', $rd['arrive_time'] );

                                add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                                add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                                add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                                add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                                {
                                    if( !empty( $p['early_discount'] ) )
                                    {
                                        if( $p['early_discount']['type'] == '0' )
                                        {
                                            $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                            $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                            $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                            $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                            $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                            $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                            $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                            $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                        }
                                        else
                                        {
                                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                            $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                            $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                        }

                                        add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                    }

                                    if( !empty( $p['seat_discount'] ) )
                                    {
                                        if( $p['seat_discount']['type'] == '0' )
                                        {
                                            $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                            $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                            $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                            $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                            $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                            $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                            $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                            $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                        }
                                        else
                                        {
                                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                            $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                            $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                        }

                                        add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                    }

                                    add_variable( 'inc_disc_price', $inc_disc_price );
                                    add_variable( 'exc_disc_price', $exc_disc_price );

                                    add_variable( 'inc_disc_adult', $inc_disc_adult );
                                    add_variable( 'inc_disc_child', $inc_disc_child );
                                    add_variable( 'inc_disc_infant', $inc_disc_infant );

                                    add_variable( 'exc_disc_adult', $exc_disc_adult );
                                    add_variable( 'exc_disc_child', $exc_disc_child );
                                    add_variable( 'exc_disc_infant', $exc_disc_infant );

                                    add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                    add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                    add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                    add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                    add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                    add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                    add_variable( 'include_price_number', $inc_total_price - $inc_disc_price );
                                    add_variable( 'exclude_price_number', $exc_total_price - $exc_disc_price );

                                    add_variable( 'include_price_disc_number', '<span class="disc-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $inc_total_price . '</span>' );
                                    add_variable( 'exclude_price_disc_number', '<span class="disc-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $exc_total_price . '</span>' );
                                }
                                else
                                {
                                    add_variable( 'discount_notif', '' );

                                    add_variable( 'inc_disc_price', 0 );
                                    add_variable( 'exc_disc_price', 0 );

                                    add_variable( 'inc_disc_adult', 0 );
                                    add_variable( 'inc_disc_child', 0 );
                                    add_variable( 'inc_disc_infant', 0 );

                                    add_variable( 'exc_disc_adult', 0 );
                                    add_variable( 'exc_disc_child', 0 );
                                    add_variable( 'exc_disc_infant', 0 );

                                    add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                    add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                    add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                    add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                    add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                    add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                    add_variable( 'include_price_number', $inc_total_price );
                                    add_variable( 'exclude_price_number', $exc_total_price );

                                    add_variable( 'include_price_disc_number', '' );
                                    add_variable( 'exclude_price_disc_number', '' );
                                }

                                add_variable( 'inc_total_price', $inc_total_price );
                                add_variable( 'exc_total_price', $exc_total_price );

                                add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                                add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                                add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $destination_from, $destination_to ) );

                                add_variable( 'include_rate_price', get_new_rate_price_content( $d['rid'], $agid, $adult, $child, $infant, $p['inc_trans'] ) );
                                add_variable( 'exclude_rate_price', get_new_rate_price_content( $d['rid'], $agid, $adult, $child, $infant, $p['exc_trans'], true ) );

                                add_variable( 'include_transport', get_new_transport_option( $d, $destination_from, $destination_to, 'departure', $pass_num ) );
                                add_variable( 'exclude_transport', get_new_transport_option( $d, $destination_from, $destination_to, 'departure', $pass_num, 1 ) );

                                parse_template( 'departure-trip-loop-block', 'dtlblock', true );

                                $i++;
                            }
                        }
                    }
                }

                if( empty( $i )  )
                {
                    parse_template( 'no-departure-trip-block', 'ndtblock' );
                }
                else
                {
                    parse_template( 'departure-trip-block', 'dtblock' );
                }
            }
            else
            {
                parse_template( 'no-departure-trip-block', 'ndtblock' );
            }
        }

        //-- Return Trip
        if( !empty( $destination_return ) && !empty( $destination_return_to ) && !empty( $return_date ) )
        {
            $return_date = date( 'Y-m-d', strtotime( $return_date ) );

            $s = 'SELECT * FROM ticket_schedule AS a
                  LEFT JOIN ticket_route AS b ON a.rid = b.rid
                  LEFT JOIN ticket_boat AS c ON a.boid = c.boid
                  WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
            $q = $db->prepare_query( $s, $return_date, $return_date, 'publish', 'publish', '1' );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
                $i = 0;

                while( $d = $db->fetch_array( $r ) )
                {
                    $available_depart  = is_available_depart_on_list( $d['rid'], $destination_return, $return_date, true );
                    $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_return_to );

                    if( strtotime( $return_date ) < time() )
                    {
                        $not_closed_date        = true;
                        $available_date         = true;
                        $is_available_allotment = is_available_allotment( $return_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $destination_return, $destination_return_to, $agid, false );
                    }
                    else
                    {
                        $not_closed_date        = is_date_not_in_close_allotment_list( $agid, $d['sid'], $return_date );
                        $available_date         = is_available_date_on_list( $d['sid'], $return_date, true );
                        $is_available_allotment = is_available_allotment( $return_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $destination_return, $destination_return_to, $agid, false );
                    }

                    if( $available_depart && $available_arrival && $available_date )
                    {
                        if( $is_available_allotment )
                        {
                            $p  = get_result_price( $d['sid'], $d['rid'], $return_date, $destination_return, $destination_return_to, $adult, $child, $infant, $agid );
                            $rd = get_route_detail_content( $d['rid'], $destination_return, $destination_return_to );

                            $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                            $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                            $trip_inc_transport_val = base64_encode( json_encode( array( $d['sid'], 1 ) ) );
                            $trip_exc_transport_val = base64_encode( json_encode( array( $d['sid'], 0 ) ) );

                            if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                            {
                                $sid_arr[] = $d['sid'];

                                add_variable( 'sid', $d['sid'] );
                                add_variable( 'rid', $d['rid'] );
                                add_variable( 'boid', $d['boid'] );
                                add_variable( 'boname', $d['boname'] );

                                add_variable( 'bddeparttime', $rd['depart_time'] );
                                add_variable( 'bdarrivetime', $rd['arrive_time'] );

                                add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                                add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                                add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                                add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                                add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                                add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                                add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                                add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                                add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                                add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                                if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                                {
                                    if( !empty( $p['early_discount'] ) )
                                    {
                                        if( $p['early_discount']['type'] == '0' )
                                        {
                                            $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                            $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                            $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                            $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                            $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                            $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                            $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                            $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                        }
                                        else
                                        {
                                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                            $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                            $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                        }

                                        add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                    }

                                    if( !empty( $p['seat_discount'] ) )
                                    {
                                        if( $p['seat_discount']['type'] == '0' )
                                        {
                                            $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                            $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                            $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                            $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                            $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                            $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                            $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                            $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                        }
                                        else
                                        {
                                            $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                            $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                            $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                            $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                            $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                        }

                                        add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                    }

                                    add_variable( 'inc_disc_price', $inc_disc_price );
                                    add_variable( 'exc_disc_price', $exc_disc_price );

                                    add_variable( 'inc_disc_adult', $inc_disc_adult );
                                    add_variable( 'inc_disc_child', $inc_disc_child );
                                    add_variable( 'inc_disc_infant', $inc_disc_infant );

                                    add_variable( 'exc_disc_adult', $exc_disc_adult );
                                    add_variable( 'exc_disc_child', $exc_disc_child );
                                    add_variable( 'exc_disc_infant', $exc_disc_infant );

                                    add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                    add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                    add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                    add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                    add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                    add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                    add_variable( 'include_price_number', $inc_total_price - $inc_disc_price );
                                    add_variable( 'exclude_price_number', $exc_total_price - $exc_disc_price );

                                    add_variable( 'include_price_disc_number', '<span class="disc-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $inc_total_price . '</span>' );
                                    add_variable( 'exclude_price_disc_number', '<span class="disc-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $exc_total_price . '</span>' );
                                }
                                else
                                {
                                    add_variable( 'discount_notif', '' );

                                    add_variable( 'inc_disc_price', 0 );
                                    add_variable( 'exc_disc_price', 0 );

                                    add_variable( 'inc_disc_adult', 0 );
                                    add_variable( 'inc_disc_child', 0 );
                                    add_variable( 'inc_disc_infant', 0 );

                                    add_variable( 'exc_disc_adult', 0 );
                                    add_variable( 'exc_disc_child', 0 );
                                    add_variable( 'exc_disc_infant', 0 );

                                    add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                    add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                    add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                    add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                    add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                    add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                    add_variable( 'include_price_number', $inc_total_price );
                                    add_variable( 'exclude_price_number', $exc_total_price );

                                    add_variable( 'include_price_disc_number', '' );
                                    add_variable( 'exclude_price_disc_number', '' );
                                }

                                add_variable( 'inc_total_price', $inc_total_price );
                                add_variable( 'exc_total_price', $exc_total_price );

                                add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                                add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                                add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                                add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $destination_return, $destination_return_to ) );

                                add_variable( 'include_rate_price', get_new_rate_price_content( $d['rid'], $agid, $adult, $child, $infant, $p['inc_trans'], false, true ) );
                                add_variable( 'exclude_rate_price', get_new_rate_price_content( $d['rid'], $agid, $adult, $child, $infant, $p['exc_trans'], true, true ) );

                                add_variable( 'include_transport', get_new_transport_option( $d, $destination_return, $destination_return_to, 'return', $pass_num ) );
                                add_variable( 'exclude_transport', get_new_transport_option( $d, $destination_return, $destination_return_to, 'return', $pass_num, true ) );

                                parse_template( 'return-trip-loop-block', 'rtlblock', true );

                                $i++;
                            }
                        }
                    }
                }

                if( empty( $i )  )
                {
                    parse_template( 'no-return-trip-block', 'nrtblock' );
                }
                else
                {
                    parse_template( 'return-trip-block', 'rtblock' );
                }
            }
            else
            {
                parse_template( 'no-return-trip-block', 'nrtblock' );
            }
        }
    }

    $depart_end_point_name   = get_location( $destination_to, 'lcname' );
    $depart_start_point_name = get_location( $destination_from, 'lcname' );
    $return_end_point_name   = get_location( $destination_return_to, 'lcname' );
    $return_start_point_name = get_location( $destination_return, 'lcname' );

    if( isset( $route_type ) && $route_type == '0' )
    {
        $return_point_css = 'disabled';
        $one_way_check    = 'checked';
        $return_check     = '';
        $return_passenger = '';
        $depart_passenger = get_passenger_list_new_content( $booking, 'departure', $adult, $child, $infant );
    }
    else
    {
        $return_point_css = '';
        $one_way_check    = '';
        $return_check     = 'checked';
        $return_passenger = get_passenger_list_new_content( $booking, 'return', $adult, $child, $infant );
        $depart_passenger = get_passenger_list_new_content( $booking, 'departure', $adult, $child, $infant );
    }

    add_variable( 'chid', $chid );
    add_variable( 'agid', $agid );
    add_variable( 'adult', $adult );
    add_variable( 'child', $child );
    add_variable( 'infant', $infant );
    add_variable( 'route_type', $route_type );
    add_variable( 'depart_date', $depart_date );
    add_variable( 'return_date', $return_date );
    add_variable( 'one_way_check', $one_way_check );
    add_variable( 'return_check', $return_check );
    add_variable( 'return_point_css', $return_point_css );

    add_variable( 'bbname', $bbname );
    add_variable( 'bbphone', $bbphone );
    add_variable( 'bbemail', $bbemail );

    add_variable( 'bcommission', 0 );
    add_variable( 'bcommissiontype', '0' );
    add_variable( 'badditionaldisc', '2' );
    add_variable( 'bcommissioncondition', '0' );
    add_variable( 'bdiscountnum', 0 );
    add_variable( 'bdiscounttype', '0' );

    add_variable( 'depart_end_point_name', $depart_end_point_name );
    add_variable( 'depart_start_point_name', $depart_start_point_name );
    add_variable( 'return_end_point_name', $return_end_point_name );
    add_variable( 'return_start_point_name', $return_start_point_name );
                                
    add_variable( 'depart_start_point', $destination_from );
    add_variable( 'depart_end_point', $destination_to );
    add_variable( 'return_start_point', $destination_return );
    add_variable( 'return_end_point', $destination_return_to );

    add_variable( 'depart_passenger', $depart_passenger );
    add_variable( 'return_passenger', $return_passenger );

    add_variable( 'depart_date_full_format', date( 'D j F Y', strtotime( $depart_date ) ) );
    add_variable( 'return_date_full_format', date( 'D j F Y', strtotime( $return_date ) ) );

    add_variable( 'depart_date_format', date( 'd F Y', strtotime( $depart_date ) ) );
    add_variable( 'return_date_format', date( 'd F Y', strtotime( $return_date ) ) );

    add_variable( 'depart_routes', $depart_start_point_name . ' to ' . $depart_end_point_name );
    add_variable( 'return_routes', $return_start_point_name . ' to ' . $return_end_point_name );

    add_variable( 'bstatus', 'pa' );
    add_variable( 'bdate', date( 'Y-m-d' ) );
    
    add_variable( 'country_list_option', get_country_list_option() );
    add_variable( 'booking_source_list', get_booking_source_option( $booking_source ) );
    add_variable( 'promo_code_option', get_promo_code_new_option( $booking, $sid_arr ) );
    add_variable( 'destination_to_list', get_availibility_loc_option( null, $destination_to ) );
    add_variable( 'destination_from_list', get_availibility_loc_option( null, $destination_from ) );
    add_variable( 'destination_return_list', get_availibility_loc_option( null, $destination_return ) );
    add_variable( 'destination_return_to_list', get_availibility_loc_option( null, $destination_return_to ) );

    add_variable( 'site_url', site_url() );
    add_variable( 'message', generate_message_block() );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/new-booking-ajax/' );
    
    add_actions( 'section_title', 'New Booking' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
    add_actions( 'other_elements', 'get_custom_javascript', 'https://cdn.jsdelivr.net/npm/autonumeric@1.9.46' );

    parse_template( 'new-booking-block', 'ablock', false );

    return return_template( 'new-booking' );
}

function get_new_rate_price_content( $rid, $agid = '', $adult = 0, $child = 0, $infant = 0, $rate = array(), $exclude = false, $return = false )
{
    if( empty( $rate ) )
    {
        return '';
    }
    else
    {
        $type    = $return ? 'return' : 'departure';
        $name    = $exclude ? 'exc' : 'inc';
        $content = '';

        //-- Check Agent Type
        if( !empty( $agid ) )
        {
            $ag = get_agent( $agid );

            $agtype = $ag['agpayment_type'];
        }
        else
        {
            $agtype = '';
        }

        if( !empty( $adult ) )
        {
            $sprice_num = empty( $rate['adult']['selling_price'] ) ? 'Free' : 'IDR ' . number_format( $rate['adult']['selling_price'], 0, ',', '.' );
            $price_num  = empty( $rate['adult']['price'] ) ? 'Free' : 'IDR ' . number_format( $rate['adult']['price'], 0, ',', '.' );
            $content   .= '
            <p>
                <span class="ptxt" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $price_num . '</span>
                <span class="sptxt sr-only" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $sprice_num . '</span>
                <a class="edit"
                    data-price-num="' . $price_num . '"
                    data-selector="' . $type . '-' . $name . '-' . $rid . '"
                    data-price="' . $rate['adult']['price'] . '"
                    data-price-type="adult_price"
                    data-type="' . $type . '"';

                    if( $agtype == 'Pre Paid' )
                    {
                        $content .= '
                        data-sell-price-num="' . $sprice_num . '"
                        data-sell-price="' . $rate['adult']['selling_price'] . '"
                        data-sell-price-type="adult_sell_price"';
                    }
                    else
                    {
                        $content .= '
                        data-sell-price-num="' . $price_num . '"
                        data-sell-price="' . $rate['adult']['price'] . '"
                        data-sell-price-type="adult_sell_price"';
                    }

                    $content .= '
                    data-toggle="popover">Edit Price
                </a>
            </p>';
        }

        if( !empty( $child ) )
        {
            $sprice_num = empty( $rate['child']['selling_price'] ) ? 'Free' : 'IDR ' . number_format( $rate['child']['selling_price'], 0, ',', '.' );
            $price_num  = empty( $rate['child']['price'] ) ? 'Free' : 'IDR ' . number_format( $rate['child']['price'], 0, ',', '.' );
            $content   .= '
            <p>
                <span class="ptxt" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $price_num . '</span>
                <span class="sptxt sr-only" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $sprice_num . '</span>
                <a class="edit"
                    data-price-num="' . $price_num . '"
                    data-selector="' . $type . '-' . $name . '-' . $rid . '"
                    data-price="' . $rate['child']['price'] . '"
                    data-price-type="child_price"
                    data-type="' . $type . '"';

                    if( $agtype == 'Pre Paid' )
                    {
                        $content .= '
                        data-sell-price-num="' . $sprice_num . '"
                        data-sell-price="' . $rate['child']['selling_price'] . '"
                        data-sell-price-type="child_sell_price"';
                    }
                    else
                    {
                        $content .= '
                        data-sell-price-num="' . $price_num . '"
                        data-sell-price="' . $rate['child']['price'] . '"
                        data-sell-price-type="child_sell_price"';
                    }

                    $content .= '
                    data-toggle="popover">Edit Price
                </a>
            </p>';
        }

        if( !empty( $infant ) )
        {
            $sprice_num = empty( $rate['infant']['selling_price'] ) ? 'Free' : 'IDR ' . number_format( $rate['infant']['selling_price'], 0, ',', '.' );
            $price_num  = empty( $rate['infant']['price'] ) ? 'Free' : 'IDR ' . number_format( $rate['infant']['price'], 0, ',', '.' );
            $content   .= '
            <p>
                <span class="ptxt" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $price_num . '</span>
                <span class="sptxt sr-only" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . $sprice_num . '</span>
                <a class="edit"
                    data-price-num="' . $price_num . '"
                    data-selector="' . $type . '-' . $name . '-' . $rid . '"
                    data-price="' . $rate['infant']['price'] . '"
                    data-price-type="infant_price"
                    data-type="' . $type . '"';

                    if( $agtype == 'Pre Paid' )
                    {
                        $content .= '
                        data-sell-price-num="' . $sprice_num . '"
                        data-sell-price="' . $rate['infant']['selling_price'] . '"
                        data-sell-price-type="infant_sell_price"';
                    }
                    else
                    {
                        $content .= '
                        data-sell-price-num="' . $price_num . '"
                        data-sell-price="' . $rate['infant']['price'] . '"
                        data-sell-price-type="infant_sell_price"';
                    }

                    $content .= '
                    data-toggle="popover">Edit Price
                </a>
            </p>';
        }

        return $content;
    }
}

function get_new_availability_result( $booking )
{
    global $db;

    extract( $booking );

    $arr = explode( '|', $booking_source );

    if( count( $arr ) == 2 )
    {
        $chid = $arr[0];
        $agid = $arr[1];
    }
    else
    {
        $chid = $arr[0];
        $agid = null;
    }

    $pass_num   = $adult + $child + $infant;
    $compliment = array( 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 );

    //-- Departure Trip
    if( isset( $destination_from ) && isset( $destination_to ) && isset( $depart_date ) )
    {
        $depart_date = date( 'Y-m-d', strtotime( $depart_date ) );

        $s = 'SELECT * FROM ticket_schedule AS a
              LEFT JOIN ticket_route AS b ON a.rid = b.rid
              LEFT JOIN ticket_boat AS c ON a.boid = c.boid
              WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
        $q = $db->prepare_query( $s, $depart_date, $depart_date, 'publish', 'publish', '1' );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $i = 0;

            while( $d = $db->fetch_array( $r ) )
            {
                $available_depart  = is_available_depart_on_list( $d['rid'], $destination_from, $depart_date, true );
                $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_to );

                if( strtotime( $depart_date ) < time() )
                {
                    $not_closed_date        = true;
                    $available_date         = true;
                    $is_available_allotment = true;
                }
                else
                {
                    $not_closed_date        = is_date_not_in_close_allotment_list( $agid, $d['sid'], $depart_date );
                    $available_date         = is_available_date_on_list( $d['sid'], $depart_date );
                    $is_available_allotment = is_available_allotment( $depart_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $destination_from, $destination_to, $agid, false );
                }

                if( $available_depart && $available_arrival && $available_date )
                {
                    if( $is_available_allotment )
                    {
                        $p  = get_result_price( $d['sid'], $d['rid'], $depart_date, $destination_from, $destination_to, $adult, $child, $infant, $agid );
                        $rd = get_route_detail_content( $d['rid'], $destination_from, $destination_to );

                        $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                        $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                        $trip_inc_transport_val = base64_encode( json_encode( array( $d['sid'], 1 ) ) );
                        $trip_exc_transport_val = base64_encode( json_encode( array( $d['sid'], 0 ) ) );

                        if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                        {
                            add_variable( 'sid', $d['sid'] );
                            add_variable( 'rid', $d['rid'] );
                            add_variable( 'boid', $d['boid'] );
                            add_variable( 'boname', $d['boname'] );

                            add_variable( 'bddeparttime', $rd['depart_time'] );
                            add_variable( 'bdarrivetime', $rd['arrive_time'] );

                            add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                            add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                            add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                            add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                            add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                            add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                            add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                            add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                            add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                            add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                            if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                            {
                                if( !empty( $p['early_discount'] ) )
                                {
                                    if( $p['early_discount']['type'] == '0' )
                                    {
                                        $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                        $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                        $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                        $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                        $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                        $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                        $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                        $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                    }
                                    else
                                    {
                                        $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                        $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                        $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                        $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                        $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                        $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                        $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                        $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                    }

                                    add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                }

                                if( !empty( $p['seat_discount'] ) )
                                {
                                    if( $p['seat_discount']['type'] == '0' )
                                    {
                                        $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                        $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                        $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                        $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                        $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                        $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                        $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                        $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                    }
                                    else
                                    {
                                        $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                        $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                        $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                        $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                        $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                        $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                        $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                        $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                    }

                                    add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                }

                                add_variable( 'inc_disc_price', $inc_disc_price );
                                add_variable( 'exc_disc_price', $exc_disc_price );

                                add_variable( 'inc_disc_adult', $inc_disc_adult );
                                add_variable( 'inc_disc_child', $inc_disc_child );
                                add_variable( 'inc_disc_infant', $inc_disc_infant );

                                add_variable( 'exc_disc_adult', $exc_disc_adult );
                                add_variable( 'exc_disc_child', $exc_disc_child );
                                add_variable( 'exc_disc_infant', $exc_disc_infant );

                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                                add_variable( 'include_price_disc_number', '<span class="disc-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                add_variable( 'exclude_price_disc_number', '<span class="disc-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                            }
                            else
                            {
                                add_variable( 'discount_notif', '' );

                                add_variable( 'inc_disc_price', 0 );
                                add_variable( 'exc_disc_price', 0 );

                                add_variable( 'inc_disc_adult', 0 );
                                add_variable( 'inc_disc_child', 0 );
                                add_variable( 'inc_disc_infant', 0 );

                                add_variable( 'exc_disc_adult', 0 );
                                add_variable( 'exc_disc_child', 0 );
                                add_variable( 'exc_disc_infant', 0 );

                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                add_variable( 'include_price_disc_number', '' );
                                add_variable( 'exclude_price_disc_number', '' );
                            }

                            add_variable( 'inc_total_price', $inc_total_price );
                            add_variable( 'exc_total_price', $exc_total_price );

                            add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                            add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                            add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                            add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $destination_from, $destination_to ) );

                            add_variable( 'include_transport', get_new_transport_option( $d, $destination_from, $destination_to, 'departure', $pass_num ) );
                            add_variable( 'exclude_transport', get_new_transport_option( $d, $destination_from, $destination_to, 'departure', $pass_num, 1 ) );

                            parse_template( 'departure-trip-block', 'dtblock', true );

                            $i++;
                        }
                    }
                }
            }

            if( empty( $i )  )
            {
                parse_template( 'no-departure-trip-block', 'ndtblock', true );
            }
        }
        else
        {
            parse_template( 'no-departure-trip-block', 'ndtblock', true );
        }
    }

    //-- Return Trip
    if( isset( $destination_return ) && isset( $destination_return_to ) && isset( $return_date ) )
    {
        $return_date = date( 'Y-m-d', strtotime( $return_date ) );

        $s = 'SELECT * FROM ticket_schedule AS a
              LEFT JOIN ticket_route AS b ON a.rid = b.rid
              LEFT JOIN ticket_boat AS c ON a.boid = c.boid
              WHERE a.sfrom <= %s AND a.sto >= %s AND a.sstatus = %s AND b.rstatus = %s AND c.bostatus = %s';
        $q = $db->prepare_query( $s, $return_date, $return_date, 'publish', 'publish', '1' );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $i = 0;

            while( $d = $db->fetch_array( $r ) )
            {
                $available_depart  = is_available_depart_on_list( $d['rid'], $destination_return, $return_date, true );
                $available_arrival = is_available_arrival_on_list( $d['rid'], $destination_return_to );

                if( strtotime( $return_date ) < time() )
                {
                    $not_closed_date        = true;
                    $available_date         = true;
                    $is_available_allotment = true;
                }
                else
                {
                    $not_closed_date        = is_date_not_in_close_allotment_list( $agid, $d['sid'], $return_date );
                    $available_date         = is_available_date_on_list( $d['sid'], $return_date );
                    $is_available_allotment = is_available_allotment( $return_date, $d['sid'], $d['rid'], $pass_num, $d['bopassenger'], $destination_return, $destination_return_to, $agid, false );
                }

                if( $available_depart && $available_arrival && $available_date )
                {
                    if( $is_available_allotment )
                    {
                        $p  = get_result_price( $d['sid'], $d['rid'], $return_date, $destination_return, $destination_return_to, $adult, $child, $infant, $agid );
                        $rd = get_route_detail_content( $d['rid'], $destination_return, $destination_return_to );

                        $inc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['inc_trans'] ) );
                        $exc_total_price = array_sum( array_map( function( $a ){ return $a['total']; }, $p['exc_trans'] ) );

                        $trip_inc_transport_val = base64_encode( json_encode( array( $d['sid'], 1 ) ) );
                        $trip_exc_transport_val = base64_encode( json_encode( array( $d['sid'], 0 ) ) );

                        if( !empty( $inc_total_price ) || !empty( $exc_total_price) )
                        {
                            add_variable( 'sid', $d['sid'] );
                            add_variable( 'rid', $d['rid'] );
                            add_variable( 'boid', $d['boid'] );
                            add_variable( 'boname', $d['boname'] );

                            add_variable( 'bddeparttime', $rd['depart_time'] );
                            add_variable( 'bdarrivetime', $rd['arrive_time'] );

                            add_variable( 'closed_attr', $not_closed_date ? '' : 'disabled' );
                            add_variable( 'closed_css', $not_closed_date ? '' : 'closed-trip' );

                            add_variable( 'trip_inc_transport_val', $trip_inc_transport_val );
                            add_variable( 'trip_exc_transport_val', $trip_exc_transport_val );

                            add_variable( 'inc_adult_price', $p['inc_trans']['adult']['price'] );
                            add_variable( 'inc_child_price', $p['inc_trans']['child']['price'] );
                            add_variable( 'inc_infant_price', $p['inc_trans']['infant']['price'] );

                            add_variable( 'exc_adult_price', $p['exc_trans']['adult']['price'] );
                            add_variable( 'exc_child_price', $p['exc_trans']['child']['price'] );
                            add_variable( 'exc_infant_price', $p['exc_trans']['infant']['price'] );

                            if( !empty( $p['early_discount'] ) || !empty( $p['seat_discount'] ) )
                            {
                                if( !empty( $p['early_discount'] ) )
                                {
                                    if( $p['early_discount']['type'] == '0' )
                                    {
                                        $inc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                        $inc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                        $inc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                        $exc_disc_adult  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                        $exc_disc_child  = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                        $exc_disc_infant = ( $p['early_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                        $inc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $inc_total_price;
                                        $exc_disc_price  = ( $p['early_discount']['disc'] / 100 ) * $exc_total_price;
                                    }
                                    else
                                    {
                                        $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                        $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                        $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                        $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                        $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['early_discount']['disc'] : 0;
                                        $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['early_discount']['disc'] : 0;

                                        $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                        $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                    }

                                    add_variable( 'discount_notif', empty( $p['early_discount']['notif'] ) ? '' : $p['early_discount']['notif'] );
                                }

                                if( !empty( $p['seat_discount'] ) )
                                {
                                    if( $p['seat_discount']['type'] == '0' )
                                    {
                                        $inc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['adult']['price'];
                                        $inc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['child']['price'];
                                        $inc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['inc_trans']['infant']['price'];

                                        $exc_disc_adult  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['adult']['price'];
                                        $exc_disc_child  = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['child']['price'];
                                        $exc_disc_infant = ( $p['seat_discount']['disc'] / 100 ) * $p['exc_trans']['infant']['price'];

                                        $inc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $inc_total_price;
                                        $exc_disc_price  = ( $p['seat_discount']['disc'] / 100 ) * $exc_total_price;
                                    }
                                    else
                                    {
                                        $inc_disc_adult  = $p['inc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                        $inc_disc_child  = $p['inc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                        $inc_disc_infant = $p['inc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                        $exc_disc_adult  = $p['exc_trans']['adult']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                        $exc_disc_child  = $p['exc_trans']['child']['price'] > 0 ? $p['seat_discount']['disc'] : 0;
                                        $exc_disc_infant = $p['exc_trans']['infant']['price'] > 0 ? $p['seat_discount']['disc'] : 0;

                                        $inc_disc_price  = $inc_disc_adult + $inc_disc_child + $inc_disc_infant;
                                        $exc_disc_price  = $exc_disc_adult + $exc_disc_child + $exc_disc_infant;
                                    }

                                    add_variable( 'discount_notif', empty( $p['seat_discount']['notif'] ) ? '' : $p['seat_discount']['notif'] );
                                }

                                add_variable( 'inc_disc_price', $inc_disc_price );
                                add_variable( 'exc_disc_price', $exc_disc_price );

                                add_variable( 'inc_disc_adult', $inc_disc_adult );
                                add_variable( 'inc_disc_child', $inc_disc_child );
                                add_variable( 'inc_disc_infant', $inc_disc_infant );

                                add_variable( 'exc_disc_adult', $exc_disc_adult );
                                add_variable( 'exc_disc_child', $exc_disc_child );
                                add_variable( 'exc_disc_infant', $exc_disc_infant );

                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                add_variable( 'include_price_number', number_format( ( $inc_total_price - $inc_disc_price ), 0, ',', '.' ) );
                                add_variable( 'exclude_price_number', number_format( ( $exc_total_price - $exc_disc_price ), 0, ',', '.' ) );

                                add_variable( 'include_price_disc_number', '<span class="disc-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . number_format( $inc_total_price, 0, ',', '.' ) . '</span>' );
                                add_variable( 'exclude_price_disc_number', '<span class="disc-price" data-v-min="0" data-a-sep="." data-a-dec="," data-m-dec="0">' . number_format( $exc_total_price, 0, ',', '.' ) . '</span>' );
                            }
                            else
                            {
                                add_variable( 'discount_notif', '' );

                                add_variable( 'inc_disc_price', 0 );
                                add_variable( 'exc_disc_price', 0 );

                                add_variable( 'inc_disc_adult', 0 );
                                add_variable( 'inc_disc_child', 0 );
                                add_variable( 'inc_disc_infant', 0 );

                                add_variable( 'exc_disc_adult', 0 );
                                add_variable( 'exc_disc_child', 0 );
                                add_variable( 'exc_disc_infant', 0 );

                                add_variable( 'inc_sell_adult_price', $p['inc_trans']['adult']['selling_price'] );
                                add_variable( 'inc_sell_child_price', $p['inc_trans']['child']['selling_price'] );
                                add_variable( 'inc_sell_infant_price', $p['inc_trans']['infant']['selling_price'] );

                                add_variable( 'exc_sell_adult_price', $p['exc_trans']['adult']['selling_price'] );
                                add_variable( 'exc_sell_child_price', $p['exc_trans']['child']['selling_price'] );
                                add_variable( 'exc_sell_infant_price', $p['exc_trans']['infant']['selling_price'] );

                                add_variable( 'include_price_number', number_format( $inc_total_price, 0, ',', '.' ) );
                                add_variable( 'exclude_price_number', number_format( $exc_total_price, 0, ',', '.' ) );

                                add_variable( 'include_price_disc_number', '' );
                                add_variable( 'exclude_price_disc_number', '' );
                            }

                            add_variable( 'inc_total_price', $inc_total_price );
                            add_variable( 'exc_total_price', $exc_total_price );

                            add_variable( 'include_css', empty( $inc_total_price ) ? 'sr-only' : '' );
                            add_variable( 'exclude_css', empty( $exc_total_price ) ? 'sr-only' : '' );

                            add_variable( 'passenger_number', get_passenger_num_content( $adult, $child, $infant ) );
                            add_variable( 'route_list', get_route_detail_list_content( $d['rid'], $destination_return, $destination_return_to ) );

                            add_variable( 'include_transport', get_new_transport_option( $d, $destination_return, $destination_return_to, 'return', $pass_num ) );
                            add_variable( 'exclude_transport', get_new_transport_option( $d, $destination_return, $destination_return_to, 'return', $pass_num, true ) );

                            parse_template( 'return-trip-block', 'rtblock', true );

                            $i++;
                        }
                    }
                }
            }

            if( empty( $i )  )
            {
                parse_template( 'no-return-trip-block', 'nrtblock', true );
            }
        }
        else
        {
            parse_template( 'no-return-trip-block', 'nrtblock', true );
        }
    }    
}

function get_new_transport_option( $dt, $from, $to, $trip_type, $passanger, $exclude = false )
{
    $content = '';
    $dtrans  = get_pickup_drop_list_data( $dt['rid'], $from );
    $atrans  = get_pickup_drop_list_data( $dt['rid'], $to );
    $count   = ceil( $passanger / 4 );

    $trip_inc_transport_val = base64_encode( json_encode( array( $dt['sid'], 1 ) ) );
    $trip_exc_transport_val = base64_encode( json_encode( array( $dt['sid'], 0 ) ) );

    if( isset( $dtrans['pickup'] ) )
    {
        if( $exclude )
        {
            $content .= '
            <div class="trans-opt-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="cols col-md-4">
                            <div class="form-group">
                                <b>Pickup : </b>
                                <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][pickup][bttrans_type]" autocomplete="off">
                                    <option value="2">Own Transport</option>
                                </select>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group driver-block">
                                <div class="fg-inner">
                                    <b>Driver Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][pickup][btdrivername]" value="" />
                                </div>
                                <div class="fg-inner">
                                    <b>Driver Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][pickup][btdriverphone]" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group trans-fee-block">
                                <b>Transport Fee : </b>
                                <div class="input-group">
                                    <div class="input-group-addon">IDR</div>
                                    <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][pickup][bttrans_fee]" value="0" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }
        else
        {
            $content .= '
            <div class="trans-opt-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="cols col-md-4">
                            <div class="form-group">
                                <b>Pickup : </b>
                                <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bttrans_type]" autocomplete="off">
                                    <option value="0" selected>Shared Transport</option>
                                    <option value="1">Private Transport</option>
                                    <option value="2">Own Transport</option>
                                </select>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group hotel-block">
                                <div class="fg-inner">
                                    <b>Accommodation : </b>
                                    <select class="select-option hotels-area required" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][taid]" autocomplete="off" data-error="Area can\'t be empty">
                                        ' . get_transport_area_option_by_all( null, $dtrans['pickup'], $count ) . '
                                    </select>
                                </div>
                                <div class="fg-inner">
                                    <select class="select-option hotels required" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" disabled>
                                        ' . get_transport_hotel_option_by_all( null, $dtrans['pickup'], $count ) . '
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hotel-detail-block sr-only">
                                <div class="fg-inner">
                                    <b>Hotel Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthotelname]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Address : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthoteladdress]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthotelphone]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Email : </b>
                                    <input type="email" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthotelemail]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Room Number : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bthotelroomnumber]" value="" disabled/>
                                </div>
                            </div>
                            <div class="form-group flight-time sr-only">
                                <b>Flight Landing Time : </b>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                    <input type="text" class="trans-ftime text form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][btflighttime]" value="" readonly="readonly"/>
                                </div>
                            </div>
                            <div class="form-group driver-block sr-only">
                                <div class="fg-inner">
                                    <b>Driver Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][btdrivername]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Driver Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][btdriverphone]" value="" disabled/>
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group trans-fee-block">
                                <b>Transport Fee : </b>
                                <div class="input-group">
                                    <div class="input-group-addon">IDR</div>
                                    <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][pickup][bttrans_fee]" value="0" readonly="readonly"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }
    }

    if( isset( $atrans['drop-off'] ) )
    {
        if( $exclude )
        {
            $content .= '
            <div class="trans-opt-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="cols col-md-4">
                            <div class="form-group">
                                <b>Drop-off : </b>
                                <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][drop-off][bttrans_type]" autocomplete="off">
                                    <option value="2">Own Transport</option>
                                </select>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group driver-block">
                                <div class="fg-inner">
                                    <b>Driver Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][drop-off][btdrivername]" value="" />
                                </div>
                                <div class="fg-inner">
                                    <b>Driver Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][drop-off][btdriverphone]" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group trans-fee-block">
                                <b>Transport Fee : </b>
                                <div class="input-group">
                                    <div class="input-group-addon">IDR</div>
                                    <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $trip_exc_transport_val . '][drop-off][bttrans_fee]" value="0" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }
        else
        {
            $content .= '
            <div class="trans-opt-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="cols col-md-4">
                            <div class="form-group">
                                <b>Drop-off : </b>
                                <select class="select-option transport-type" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bttrans_type]" autocomplete="off">
                                    <option value="0" selected>Shared Transport</option>
                                    <option value="1">Private Transport</option>
                                    <option value="2">Own Transport</option>
                                </select>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group hotel-block">
                                <div class="fg-inner">
                                    <b>Accommodation : </b>
                                    <select class="select-option hotels-area required" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][taid]" autocomplete="off" data-error="Area can\'t be empty">
                                        ' . get_transport_area_option_by_all( null, $atrans['drop-off'], $count ) . '
                                    </select>
                                </div>
                                <div class="fg-inner">
                                    <select class="select-option hotels required" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][hid]" autocomplete="off" data-error="Hotel location can\'t be empty" disabled>
                                        ' . get_transport_hotel_option_by_all( null, $atrans['drop-off'], $count ) . '
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hotel-detail-block sr-only">
                                <div class="fg-inner">
                                    <b>Hotel Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthotelname]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Address : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthoteladdress]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthotelphone]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Email : </b>
                                    <input type="email" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthotelemail]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Hotel Room Number : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bthotelroomnumber]" value="" disabled/>
                                </div>
                            </div>
                            <div class="form-group flight-time sr-only">
                                <b>Flight Landing Time : </b>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></div>
                                    <input type="text" class="trans-ftime text form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][btflighttime]" value="" readonly="readonly"/>
                                </div>
                            </div>
                            <div class="form-group driver-block sr-only">
                                <div class="fg-inner">
                                    <b>Driver Name : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][btdrivername]" value="" disabled/>
                                </div>
                                <div class="fg-inner">
                                    <b>Driver Phone : </b>
                                    <input type="text" class="text text-full form-control" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][btdriverphone]" value="" disabled/>
                                </div>
                            </div>
                        </div>
                        <div class="cols col-md-4">
                            <div class="form-group trans-fee-block">
                                <b>Transport Fee : </b>
                                <div class="input-group">
                                    <div class="input-group-addon">IDR</div>
                                    <input type="text" class="text form-control text-right trans-fee" name="transport[' . $trip_type . '][' . $trip_inc_transport_val . '][drop-off][bttrans_fee]" value="0" readonly="readonly"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }
    }

    return $content;
}

function get_passenger_list_new_content( $booking, $btype, $adult = 0, $child = 0, $infant = 0 )
{
    $content = '';

    if( !empty( $adult ) )
    {
        for( $i = 0; $i < $adult; $i++ )
        {
            $bpname      = isset( $booking['passenger']['adult'][$i]['bpname'] ) ? $booking['passenger']['adult'][$i]['bpname'] : '';
            $bpgender    = isset( $booking['passenger']['adult'][$i]['bpgender'] ) ? $booking['passenger']['adult'][$i]['bpgender'] : 1;
            $lcountry_id = isset( $booking['passenger']['adult'][$i]['lcountry_id'] ) ? $booking['passenger']['adult'][$i]['lcountry_id'] : '';
            $bpbirthdate = isset( $booking['passenger']['adult'][$i]['bpbirthdate'] ) ? ( empty( $booking['passenger']['adult'][$i]['bpbirthdate'] ) ? '' : date( 'd F Y', strtotime( $booking['passenger']['adult'][$i]['bpbirthdate'] ) ) ) : '';

            $content .= '
            <tr>
                <td>
                    <span class="small-title">Adult ' . ( $i + 1 ) . '</span>
                    <input type="text" class="bpname text form-control" value="' . $bpname . '" name="passenger[' . $btype . '][adult][' . ( $i + 1 ) . '][bpname]" required>
                </td>
                <td>
                    <span class="small-title">Gender</span>
                    <div>
                        <select class="bpgender select-option form-control" name="passenger[' . $btype . '][adult][' . ( $i + 1 ) . '][bpgender]" autocomplete="off">
                            ' . get_gender_option( $bpgender, false ) . '
                        </select>
                    </div>
                </td>
                <td>
                    <span class="small-title">Date Of Birth</span>
                    <input type="text" class="bpbirthdate text form-control txt-date-adult" value="' . $bpbirthdate . '" name="passenger[' . $btype . '][adult][' . ( $i + 1 ) . '][bpbirthdate]" readonly>         
                </td>
                <td width="150">
                    <span class="small-title">Nationality</span>
                    <div>
                        <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][adult][' . ( $i + 1 ) . '][lcountry_id]" autocomplete="off">
                            ' . get_country_list_option( $lcountry_id ) . '
                        </select>  
                    </div>        
                </td>
            </tr>';
        }
    }

    if( !empty( $child ) )
    {       
        for( $i = 0; $i < $child; $i++ )
        {
            $bpname      = isset( $booking['passenger']['child'][$i]['bpname'] ) ? $booking['passenger']['child'][$i]['bpname'] : '';
            $bpgender    = isset( $booking['passenger']['child'][$i]['bpgender'] ) ? $booking['passenger']['child'][$i]['bpgender'] : 1;
            $lcountry_id = isset( $booking['passenger']['child'][$i]['lcountry_id'] ) ? $booking['passenger']['child'][$i]['lcountry_id'] : '';
            $bpbirthdate = isset( $booking['passenger']['child'][$i]['bpbirthdate'] ) ? ( empty( $booking['passenger']['child'][$i]['bpbirthdate'] ) ? '' : date( 'd F Y', strtotime( $booking['passenger']['child'][$i]['bpbirthdate'] ) ) ) : '';

            $content .= '
            <tr>
                <td>
                    <span class="small-title">Child ' . ( $i + 1 ) . '</span>
                    <input type="text" class="bpname text form-control" value="' . $bpname . '" name="passenger[' . $btype . '][child][' . ( $i + 1 ) . '][bpname]" required>
                </td>
                <td>
                    <span class="small-title">Gender</span>
                    <div>
                        <select class="bpgender select-option form-control" name="passenger[' . $btype . '][child][' . ( $i + 1 ) . '][bpgender]" autocomplete="off">
                            ' . get_gender_option( $bpgender, false ) . '
                        </select>
                    </div>
                </td>
                <td>
                    <span class="small-title">Date Of Birth</span>
                    <input type="text" class="bpbirthdate text form-control txt-date-child" value="' . $bpbirthdate . '" name="passenger[' . $btype . '][child][' . ( $i + 1 ) . '][bpbirthdate]" readonly>         
                </td>
                <td width="150">
                    <span class="small-title">Nationality</span>
                    <div>
                        <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][child][' . ( $i + 1 ) . '][lcountry_id]" autocomplete="off">
                            ' . get_country_list_option( $lcountry_id ) . '
                        </select>
                    </div>
                </td>
            </tr>';
        }
    }

    if( !empty( $infant ) )
    {       
        for( $i = 0; $i < $infant; $i++ )
        {
            $bpname      = isset( $booking['passenger']['infant'][$i]['bpname'] ) ? $booking['passenger']['infant'][$i]['bpname'] : '';
            $bpgender    = isset( $booking['passenger']['infant'][$i]['bpgender'] ) ? $booking['passenger']['infant'][$i]['bpgender'] : 1;
            $lcountry_id = isset( $booking['passenger']['infant'][$i]['lcountry_id'] ) ? $booking['passenger']['infant'][$i]['lcountry_id'] : '';
            $bpbirthdate = isset( $booking['passenger']['infant'][$i]['bpbirthdate'] ) ? ( empty( $booking['passenger']['infant'][$i]['bpbirthdate'] ) ? '' : date( 'd F Y', strtotime( $booking['passenger']['infant'][$i]['bpbirthdate'] ) ) ) : '';

            $content .= '
            <tr>
                <td>
                    <span class="small-title">Infant ' . ( $i + 1 ) . '</span>
                    <input type="text" class="bpname text form-control" value="' . $bpname . '" name="passenger[' . $btype . '][infant][' . ( $i + 1 ) . '][bpname]" required>
                </td>
                <td>
                    <span class="small-title">Gender</span>
                    <div>
                        <select class="bpgender select-option form-control" name="passenger[' . $btype . '][infant][' . ( $i + 1 ) . '][bpgender]" autocomplete="off">
                            ' . get_gender_option( $bpgender, false ) . '
                        </select>
                    </div>
                </td>
                <td>
                    <span class="small-title">Date Of Birth</span>
                    <input type="text" class="bpbirthdate text form-control txt-date-infant" value="' . $bpbirthdate . '" name="passenger[' . $btype . '][infant][' . ( $i + 1 ) . '][bpbirthdate]" readonly>         
                </td>
                <td width="150">
                    <span class="small-title">Nationality</span>
                    <div>
                        <select class="lcountry_id select-option form-control" name="passenger[' . $btype . '][infant][' . ( $i + 1 ) . '][lcountry_id]" autocomplete="off">
                            ' . get_country_list_option( $lcountry_id ) . '
                        </select>
                    </div>
                </td>
            </tr>';
        }
    }

    return $content;
}

function get_promo_code_new_option( $booking, $sid_arr = array() )
{
    global $db;

    $option = '';

    if( empty( $booking['booking_source'] ) === FALSE )
    {
        $s = 'SELECT * FROM ticket_promo WHERE pmstatus = %s';
        $q = $db->prepare_query( $s, '1' );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                //-- Check 24 Advance Booking
                if( $d['pmbook24inadvance'] == '2' )
                {
                    $dpfrom = strtotime( $booking['depart_date'] );
                    $drprm  = time() + 86400;
                    $bodate = time();
                    
                    if( $dpfrom <= $drprm && $bodate <= $drprm )
                    {
                        if( get_remain_limit( $d ) > 0 )
                        {
                            $option .= '<option value="' . $d['pmcode'] . '">' . $d['pmcode'] . '</option>';
                        }
                    }
                }
                else
                {
                    //-- Check Promo Code Application
                    $valid_apps = true;

                    if( $d['pmapplication'] != '' )
                    {
                        $pmapplication = json_decode( $d['pmapplication'], true );

                        if( $pmapplication !== null && json_last_error() === JSON_ERROR_NONE && in_array( 0, $pmapplication ) === false )
                        {
                            $valid_apps = false;
                        }
                    }

                    if( $valid_apps )
                    {
                        $pmfrom = strtotime( $d['pmfrom'] );
                        $pmto   = strtotime( $d['pmto'] );

                        //-- One Way Trip
                        if( empty( $booking['return_date'] ) )
                        {
                            //-- Check Baseon
                            if( $d['pmbaseon'] == '0' )
                            {
                                $trfrom = strtotime( $booking['depart_date'] );
                            }
                            else
                            {
                                $trfrom = time();
                            }

                            //-- Check Schedule
                            if( $d['pmsid'] != '' )
                            {
                                $pmsid = json_decode( $d['pmsid'], true );

                                if( $pmsid !== null && json_last_error() === JSON_ERROR_NONE )
                                {
                                    if( empty( $sid_arr ) === false )
                                    {
                                        $valid_sid = false;

                                        foreach( $sid_arr as $sid )
                                        {
                                            if( in_array( $sid, $pmsid ) )
                                            {
                                                $valid_sid = true;

                                                break;
                                            }
                                        }                            
                                    }
                                    else
                                    {
                                        $valid_sid = true;
                                    }
                                }
                                else
                                {
                                    $valid_sid = false;
                                }
                            }
                            else
                            {
                                $valid_sid = true;
                            }

                            if( $pmfrom <= $trfrom && $pmto >= $trfrom && $valid_sid )
                            {
                                if( get_remain_limit( $d ) > 0 )
                                {
                                    $option .= '<option value="' . $d['pmcode'] . '">' . $d['pmcode'] . '</option>';
                                }
                            }
                        }
                        else
                        {                
                            //-- Check Baseon
                            if( $d['pmbaseon'] == '0' )
                            {
                                $trfrom = strtotime( $booking['depart_date'] );
                                $trto   = strtotime( $booking['return_date'] );
                            }
                            else
                            {
                                $trfrom = time();
                                $trto   = time();
                            }

                            //-- Check Schedule
                            if( $d['pmsid'] != '' )
                            {
                                $pmsid = json_decode( $d['pmsid'], true );

                                if( $pmsid !== null && json_last_error() === JSON_ERROR_NONE )
                                {
                                    if( empty( $sid_arr ) === false )
                                    {
                                        $valid_sid = false;

                                        foreach( $sid_arr as $sid )
                                        {
                                            if( in_array( $sid, $pmsid ) )
                                            {
                                                $valid_sid = true;

                                                break;
                                            }
                                        }                            
                                    }
                                }
                                else
                                {
                                    $valid_sid = false;
                                }
                            }
                            else
                            {
                                $valid_sid = true;
                            }

                            if( ( ( $pmfrom <= $trfrom && $pmto >= $trfrom ) || ( $pmfrom <= $trto && $pmto >= $trto ) ) && $valid_sid )
                            {
                                if( get_remain_limit( $d ) > 0 )
                                {
                                    $option .= '<option value="' . $d['pmcode'] . '">' . $d['pmcode'] . '</option>';
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    $promo_code_option = '
    <select class="pmcode select-option form-control" name="booked[pmcode]" autocomplete="off" disabled>
        <option value="">No promo code</option>
        ' . $option . '
    </select>';

    return $promo_code_option;
}

function get_booking_post_data()
{
    $data = array(
        'chid' => '',
        'agid' => '',
        'adult' => 1,
        'child' => 0,
        'infant' => 0,
        'route_type' => 0,
        'booking_source' => '',
        'destination_to' => '',
        'destination_from' => '',
        'destination_return' => '',
        'destination_return_to' => '',
        'depart_date' => date( 'd F Y' ),
        'return_date' => date( 'd F Y' ),
        'bhotelname' => '',
        'bhoteladdress' => '',
        'bhotelphone' => '',
        'bhotelemail' => '',
        'bremark' => '',
        'bpaymethod' => '',
        'bbname' => '',
        'bbphone' => '',
        'bbemail' => get_meta_data( 'default_booking_email', 'ticket_setting' )
    );

    if( !empty( $_POST ) )
    {
        foreach( $_POST as $field => $val )
        {
            if( is_array( $val ) )
            {
                foreach( $val as $idx => $obj )
                {
                    if( is_array( $obj ) )
                    {
                        foreach( $obj as $i => $d )
                        {
                            $data[ $field ][ $idx ][ $i ] = $d;
                        }
                    }
                    else
                    {
                        $data[ $field ][ $idx ] = $obj;
                    }
                }
            }
            else
            {
                $data[ $field ] = $val;
            }
        }
    }

    return $data;
}

function run_save_new_booking()
{
    global $db;
    global $flash;

    if( isset( $_POST['save_booking'] ) )
    {
        $retval = 1;
        
        $db->begin();

        if( isset( $_POST[ 'booked' ][ 'pmcode' ] ) && empty( $_POST[ 'booked' ][ 'pmcode' ] ) === false )
        {
            $s = 'SELECT * FROM ticket_promo WHERE pmcode = %s';
            $q = $db->prepare_query( $s, $_POST[ 'booked' ][ 'pmcode' ] );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
                $d = $db->fetch_array( $r );

                if( empty( $d['agid'] ) === false )
                {
                    $_POST[ 'booked' ][ 'agid' ] = $d['agid'];
                    $_POST[ 'booked' ][ 'chid' ] = get_agent( $d['agid'], 'chid' );
                }
            }
        }

        if( isset( $_POST[ 'booked' ] ) )
        {
            $bparam = array();

            foreach( $_POST[ 'booked' ] as $field => $val )
            {
                if( $field == 'bcode' )
                {
                    $bparam[ $field ] = generate_code_number( $_POST[ 'booked' ][ 'btype' ] );
                }
                elseif( $field == 'bticket' )
                {
                    $bparam[ $field ] = generate_ticket_number();
                }
                elseif( $field == 'bstatus' )
                {
                    if( in_array( $_POST[ 'booked' ][ 'bpaymethod' ], array( 6, 8 ) ) === FALSE )
                    {
                        $bparam[ $field ] = 'pa';
                    }
                    elseif( $_POST[ 'booked' ][ 'bpaymethod' ] == 8 && $_POST[ 'booked' ][ 'agid' ] != '' )
                    {
                        $bparam[ $field ] = 'ca';
                    }
                    else
                    {
                        $bparam[ $field ] = 'pp';
                    }
                }
                elseif( $field == 'agid' )
                {
                    if( $val != '' )
                    {
                        $bparam[ $field ] = $val;
                    }
                }
                else
                {
                    $bparam[ $field ] = $val;
                }
            }

            if( !empty( $bparam ) )
            {
                //-- Insert New Booking Data
                $q = 'INSERT INTO ticket_booking(' . implode( ',', array_keys( $bparam ) ) . ') VALUES ("' . implode( '" , "', $bparam ) . '")';
                $r = $db->do_query( $q );

                if( is_array( $r ) )
                {
                    $retval = 0;
                }
                else
                {
                    $price_field = array( 'price_per_adult', 'price_per_child', 'price_per_infant', 'selling_price_per_adult', 'selling_price_per_child', 'selling_price_per_infant', 'disc_price_per_adult', 'disc_price_per_child', 'disc_price_per_infant', 'subtotal', 'discount', 'total' );
                    $field_not   = array( 'price_per_adult_o', 'price_per_child_o', 'price_per_infant_o', 'selling_price_per_adult_o', 'selling_price_per_child_o', 'selling_price_per_infant_o', 'disc_price_per_adult_o', 'disc_price_per_child_o', 'disc_price_per_infant_o' );
                    $compliment  = array( 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 );
                    $bid         = $db->insert_id();
                    $bsubtotal   = 0;

                    if( in_array( $_POST[ 'booked' ][ 'bpaymethod'], $compliment ) && ( $_POST['agtype'] == 'Pre Paid' || empty( $_POST[ 'booked' ][ 'agid' ] ) ) )
                    {
                        $use_comp = true;
                    }
                    else
                    {
                        $use_comp = false;
                    }

                    foreach( $_POST[ 'booked_detail' ] as $bdtype => $detail )
                    {
                        $bdparam  = array();
                        $data_key = $_POST[ $bdtype . '_trip_transport_val' ];

                        $detail[ $data_key ][ 'bid' ] = $bid;

                        if( $use_comp )
                        {
                            $detail[ $data_key ][ 'transport_fee' ] = 0;  

                            $detail[ $data_key ][ 'discount' ]      = 0;
                            $detail[ $data_key ][ 'subtotal' ]      = 0;                          
                        }
                        else
                        {
                            $detail[ $data_key ][ 'transport_fee' ] = array_sum( array_column( $_POST[ 'transport' ][ $bdtype ][ $data_key ], 'bttrans_fee' ) );
                        }

                        //-- Set discount early birth & last minute to 0
                        //-- if additional disc = No
                        if( $_POST[ 'booked' ][ 'badditionaldisc' ] == '1' )
                        {
                            $detail[ $data_key ][ 'discount' ] = 0;
                        }
                        else
                        {
                            /* SET DISCOUNT WHEN EDITED SELLING PRICE OR NET PRICE */
                            if( ( $detail[ $data_key ]['price_per_adult'] != $detail[ $data_key ]['price_per_adult_o'] ) || ( $detail[ $data_key ]['selling_price_per_adult'] != $detail[ $data_key ]['selling_price_per_adult_o'] ) ) 
                            {
                                $detail[ $data_key ][ 'discount' ] = $detail[ $data_key ][ 'discount' ] - ( $detail[ $data_key ]['disc_price_per_adult'] * $detail[ $data_key ]['num_adult'] );
                            }

                            if( ( $detail[ $data_key ]['price_per_child'] != $detail[ $data_key ]['price_per_child_o'] ) || ( $detail[ $data_key ]['selling_price_per_child'] != $detail[ $data_key ]['selling_price_per_child_o'] ) ) 
                            {
                                $detail[ $data_key ][ 'discount' ] = $detail[ $data_key ][ 'discount' ] - ( $detail[ $data_key ]['disc_price_per_child'] * $detail[ $data_key ]['num_child'] );
                            }

                            if( ( $detail[ $data_key ]['price_per_infant'] != $detail[ $data_key ]['price_per_infant_o'] ) || ( $detail[ $data_key ]['selling_price_per_infant'] != $detail[ $data_key ]['selling_price_per_infant_o'] ) ) 
                            {
                                $detail[ $data_key ][ 'discount' ] = $detail[ $data_key ][ 'discount' ] - ( $detail[ $data_key ]['disc_price_per_infant'] * $detail[ $data_key ]['num_infant'] );
                            }
                            /* ==== END ==== */
                        }

                        $detail[ $data_key ][ 'total' ] = $detail[ $data_key ][ 'subtotal' ] + $detail[ $data_key ][ 'transport_fee' ] - $detail[ $data_key ][ 'discount' ];  

                        foreach( $detail[ $data_key ] as $field => $val )
                        {
                            if ( !in_array( $field , $field_not ) ) 
                            {
                                if( $val !== '' )
                                {
                                    if( $use_comp && in_array( $field, $price_field ) )
                                    {
                                        $bdparam[ $field ] = 0;
                                    }
                                    else
                                    {
                                        //-- Set discount early birth & last minute to 0
                                        //-- if additional disc = No
                                        if( $_POST[ 'booked' ][ 'badditionaldisc' ] == '1' )
                                        {
                                            if( in_array( $field, array( 'disc_price_per_adult', 'disc_price_per_child', 'disc_price_per_infant' ) ) )
                                            {
                                                $val = 0;
                                            }
                                        }
                                        else
                                        {
                                            /* UPDATE DISCOUNT WHEN EDITED SELLING PRICE OR NET PRICE */
                                            if( $field == 'disc_price_per_adult' ) 
                                            {
                                                $val = ( $detail[ $data_key ]['price_per_adult'] != $detail[ $data_key ]['price_per_adult_o'] ) || ( $detail[ $data_key ]['selling_price_per_adult'] != $detail[ $data_key ]['selling_price_per_adult_o'] ) ? 0 : $detail[ $data_key ]['disc_price_per_adult'];
                                            }

                                            if( $field == 'disc_price_per_child' ) 
                                            {
                                                $val = ( $detail[ $data_key ]['price_per_child'] != $detail[ $data_key ]['price_per_child_o'] ) || ( $detail[ $data_key ]['selling_price_per_child'] != $detail[ $data_key ]['selling_price_per_child_o'] ) ? 0 : $detail[ $data_key ]['disc_price_per_child'];
                                            }

                                            if( $field == 'disc_price_per_infant' ) 
                                            {
                                                $val = ( $detail[ $data_key ]['price_per_infant'] != $detail[ $data_key ]['price_per_infant_o'] ) || ( $detail[ $data_key ]['selling_price_per_infant'] != $detail[ $data_key ]['selling_price_per_infant_o'] ) ? 0 : $detail[ $data_key ]['disc_price_per_infant'];
                                            }
                                            /* ==== END ==== */
                                        }

                                        /* ADD FIELD NET PRICE FOR DATABASE */
                                        if ( $field == 'selling_price_per_adult' ) 
                                        {
                                            $bdparam['net_price_per_adult'] = $val;
                                        }

                                        if ( $field == 'selling_price_per_child' ) 
                                        {
                                            $bdparam['net_price_per_child'] = $val;
                                        }

                                        if ( $field == 'selling_price_per_infant' ) 
                                        {
                                            $bdparam['net_price_per_infant'] = $val;
                                        }
                                        /* ==== END ==== */

                                        $bdparam[ $field ] = $val;
                                    }
                                }
                                else
                                {
                                    if( $field == 'bdpstatus' )
                                    {
                                        $bdparam[ $field ] = $bparam[ 'bstatus' ];
                                    }
                                }
                            }
                        }

                        $bdparam[ 'bdrevstatus' ] = $bparam[ 'bbrevstatus' ];

                        if( !empty( $bdparam ) )
                        {
                            //-- Insert New Booking Detail Data
                            $q = 'INSERT INTO ticket_booking_detail(' . implode( ',', array_keys( $bdparam ) ) . ') VALUES ("' . implode( '" , "', $bdparam ) . '")';
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                $retval = 0;
                            }
                            else
                            {
                                $new_bdid = $db->insert_id();
                                
                                foreach( $_POST[ 'passenger' ][ $bdtype ] as $bptype => $passenger )
                                {                                
                                    foreach( $passenger as $obj )
                                    {
                                        $obj[ 'bdid' ]   = $new_bdid;
                                        $obj[ 'bptype' ] = $bptype;
                                        
                                        $bpparam = array();

                                        foreach( $obj as $field => $val )
                                        {
                                            if( $val != '' )
                                            {
                                                $bpparam[ $field ] = $field == 'bpbirthdate' ? date( 'Y-m-d', strtotime( $val ) ) : $val;
                                            }
                                        }

                                        if( !empty( $bpparam ) )
                                        {
                                            //-- Insert New Booking Passenger Data
                                            $q = 'INSERT INTO ticket_booking_passenger(' . implode( ',', array_keys( $bpparam ) ) . ') VALUES ("' . implode( '" , "', $bpparam ) . '")';
                                            $r = $db->do_query( $q );

                                            if( is_array( $r ) )
                                            {
                                                $retval = 0;
                                            }
                                        }
                                    }
                                }

                                foreach( $_POST[ 'transport' ][ $bdtype ][ $data_key ] as $bttype => $obj )
                                {
                                    if( $use_comp )
                                    {
                                        $obj['bttrans_fee'] = 0;
                                    }

                                    if( $obj['bttrans_type'] == 2 )
                                    {
                                        $s = 'INSERT INTO ticket_booking_transport( 
                                                bdid, 
                                                bttype, 
                                                bttrans_type, 
                                                btdrivername, 
                                                btdriverphone, 
                                                bttrans_fee ) VALUES( %d, %s, %s, %s, %s, %s )';
                                        $q = $db->prepare_query( $s, $new_bdid, $bttype, $obj['bttrans_type'], $obj['btdrivername'], $obj['btdriverphone'], $obj['bttrans_fee'] );
                                    }
                                    else
                                    {
                                        if( empty( $obj['taid'] ) || empty( $obj['hid'] ) )
                                        {                
                                            $s = 'INSERT INTO ticket_booking_transport( 
                                                    bdid, 
                                                    bttype, 
                                                    bttrans_type, 
                                                    bttrans_fee ) VALUES( %d, %s, %s, %s )';
                                            $q = $db->prepare_query( $s, $new_bdid, $bttype, $obj['bttrans_type'], $obj['bttrans_fee'] );
                                        }
                                        else
                                        {                    
                                            if( $obj['hid'] == 1 )
                                            {
                                                $bthotelname       = isset( $obj['bthotelname'] ) ? $obj['bthotelname'] : '';
                                                $bthotelphone      = isset( $obj['bthotelphone'] ) ? $obj['bthotelphone'] : '';
                                                $bthotelemail      = isset( $obj['bthotelemail'] ) ? $obj['bthotelemail'] : '';
                                                $bthoteladdress    = isset( $obj['bthoteladdress'] ) ? $obj['bthoteladdress'] : '';
                                                $bthotelroomnumber = isset( $obj['bthotelroomnumber'] ) ? $obj['bthotelroomnumber'] : '';
                                            }
                                            else
                                            {
                                                $h = get_hotel( $obj['hid'] );

                                                $bthotelname       = $h['hname'];
                                                $bthotelphone      = $h['hphone'];
                                                $bthotelemail      = $h['hemail'];
                                                $bthoteladdress    = $h['haddress'];
                                                $bthotelroomnumber = '';
                                            }
                                            
                                            list( $taid, $btrpfrom, $btrpto ) = explode( '|', $obj['taid'] );

                                            $btflighttime = isset( $obj['btflighttime'] ) ? ( empty( $obj['btflighttime'] ) ? '' : date( 'Y-m-d H:i:s', strtotime( $obj['btflighttime'] ) ) ) : '';

                                            //-- Insert New Booking Transport Data
                                            $s = 'INSERT INTO ticket_booking_transport(
                                                    bdid,
                                                    hid,
                                                    taid,
                                                    bttype,
                                                    bttrans_type,
                                                    bthotelname,
                                                    bthoteladdress,
                                                    bthotelphone,
                                                    bthotelemail,
                                                    bthotelroomnumber,
                                                    btflighttime,
                                                    btrpfrom,
                                                    btrpto,
                                                    bttrans_fee ) VALUES( %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
                                            $q = $db->prepare_query( $s,
                                                    $new_bdid,
                                                    $obj['hid'], 
                                                    $taid,
                                                    $bttype,
                                                    $obj['bttrans_type'],
                                                    $bthotelname,
                                                    $bthoteladdress,
                                                    $bthotelphone,
                                                    $bthotelemail,
                                                    $bthotelroomnumber,
                                                    $btflighttime,
                                                    $btrpfrom,
                                                    $btrpto,
                                                    $obj['bttrans_fee'] );
                                        }
                                    }
                                    
                                    $r = $db->do_query( $q );

                                    if( is_array( $r ) )
                                    {
                                        $retval = 0;
                                    }
                                }
                            }
                        }

                        //-- Count Subtotal
                        if( isset( $bdparam[ 'total' ] ) )
                        {
                            $bsubtotal += $bdparam[ 'total' ];
                        }
                    }
                }
            }

            if( $retval == 1 )
            {
                $btotal = $bsubtotal - $_POST[ 'booked' ][ 'bdiscount' ];

                //-- Update brcode, bsubtotal, btotal, bonhandtotal Of New Booking Data
                if( $bparam[ 'bstatus' ] == 'pa' )
                {
                    //-- Update onhand
                    $s2 = 'UPDATE ticket_booking SET bsubtotal = %s, btotal = %s, bonhandtotal = %s WHERE bid = %d';
                    $q2 = $db->prepare_query( $s2, $bsubtotal, $btotal, $btotal, $bid );
                    $r2 = $db->do_query( $q2 );
                }
                else
                {
                    $s2 = 'UPDATE ticket_booking SET bsubtotal = %s, btotal = %s WHERE bid = %d';
                    $q2 = $db->prepare_query( $s2, $bsubtotal, $btotal, $bid );
                    $r2 = $db->do_query( $q2 );
                }

                if( is_array( $r2 ) )
                {
                    $retval = 0;
                }
                else
                {
                    //-- Insert Booking Payment Data
                    $pmethod = get_payment_method_by_id( $_POST[ 'booked' ][ 'bpaymethod' ], 'mname' );
                    $pid     = save_booking_payment( $bid, generate_payment_id(), '', time(), $btotal, 0, $pmethod, '1' );

                    if( empty( $pid ) )
                    {
                        $retval = 0;
                    }
                    
                    //-- Insert Agent Transaction Data
                    if( !empty( $_POST[ 'booked' ][ 'agid' ] ) )
                    {
                        $atgname = array();

                        foreach( $_POST[ 'passenger' ][ 'departure' ] as $bptype => $obj )
                        {
                            foreach( $obj as $dt )
                            {
                                $atgname[] = $dt['bpname'];
                            }
                        }

                        $s3 = 'INSERT INTO ticket_agent_transaction( agid, bcode, bid, atdate, atgname, atdebet, atcredit, atstatus, atmid, atmethod ) VALUES( %d, %s, %d, %s, %s, %s, %s, %d, %d, %s )';
                        $q3 = $db->prepare_query( $s3, $_POST[ 'booked' ][ 'agid' ], $bparam[ 'bcode' ], $bid, date( 'Y-m-d' ), implode( ', ', $atgname ), $btotal, 0, 1, 0, '' );
                        $r3 = $db->do_query( $q3 );

                        if( is_array( $r3 ) )
                        {
                            $retval = 0;
                        }
                        else
                        {
                            //-- Create transaction history
                            if( $bparam[ 'bstatus' ] == 'pa' )
                            {
                                $s4 = 'INSERT INTO ticket_agent_transaction( agid, bcode, bid, atdate, atgname, atdebet, atcredit, atstatus, atmid, atmethod ) VALUES( %d, %s, %d, %s, %s, %s, %s, %d, %d, %s )';
                                $q4 = $db->prepare_query( $s4, $_POST[ 'booked' ][ 'agid' ], $bparam[ 'bcode' ], $bid, date( 'Y-m-d' ), implode( ', ', $atgname ), 0, $btotal, 2, $_POST[ 'booked' ][ 'bpaymethod' ], $pmethod );
                                $r4 = $db->do_query( $q4 );

                                if( is_array( $r4 ) )
                                {
                                    $retval = 0;
                                }
                            }
                        }
                    }
                }

                save_log( $bid, 'reservation', 'Create new reservation #' . $bparam[ 'bticket' ] );
            }
        }
        
        if( $retval == 0 )
        {
            $db->rollback();
            
            $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to create new reservation' ) ) );

            return false;
        }
        else
        {
            $db->commit();

            $flash->add( array( 'type'=> 'success', 'content' => array( 'New reservation successfully created' ) ) );

            return true;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Apply Promo Code Functions
| -------------------------------------------------------------------------------------
*/
function new_booking_apply_code( $post = array() )
{
    global $db;

    extract( $post );

    $data = array( 'commission' => 0, 'commissiontype' => '0', 'commissioncondition' => '0', 'additionaldisc' => '2', 'discountnum' => 0, 'discounttype' => '0' );

    if( empty( $code ) === false )
    {
        $s = 'SELECT * FROM ticket_promo WHERE pmcode = %s AND pmstatus = %s';
        $q = $db->prepare_query( $s, $code, '1' );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $d = $db->fetch_array( $r );
            $v = true;

            //-- Check Schedule
            if( $d['pmsid'] != '' && $d['pmbook24inadvance'] != '2' )
            {
                $pmsid = json_decode( $d['pmsid'], true );

                if( $pmsid !== null && json_last_error() === JSON_ERROR_NONE )
                {
                    if( empty( $sid ) === false )
                    {
                        $v = false;

                        foreach( $sid as $id )
                        {
                            if( in_array( $id, $pmsid ) )
                            {
                                $v = true;

                                break;
                            }
                        }                            
                    }
                    else
                    {
                        $v = true;
                    }
                }
                else
                {
                    $v = false;
                }
            }
            else
            {
                $v = true;
            }

            if( $v )
            {
                $data['commission']          = $d['pmcommission'];
                $data['commissiontype']      = $d['pmcommissiontype'];
                $data['additionaldisc']      = $d['pmadditionaldisc'];
                $data['commissioncondition'] = $d['pmcommissioncondition'];
                $data['discountnum']         = $d['pmdiscount'];
                $data['discounttype']        = $d['pmdiscounttype'];
            }
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_new_booking_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'apply-coupon-code' )
    {
        $data = new_booking_apply_code( $_POST );

        if( $data['discountnum'] == 0 )
        {
            echo json_encode( 
                array( 
                    'result' => 'failed',
                    'commission' => $data['commission'], 
                    'commissiontype' => $data['commissiontype'], 
                    'additionaldisc' => $data['additionaldisc'], 
                    'commissioncondition' => $data['commissioncondition'], 
                    'promo_code' => $_POST['code'],
                    'discountnum' => $data['discountnum'],
                    'discounttype' => $data['discounttype']
                ) 
            );
        }
        else
        {
            echo json_encode( 
                array( 
                    'result' => 'success',
                    'commission' => $data['commission'], 
                    'commissiontype' => $data['commissiontype'], 
                    'additionaldisc' => $data['additionaldisc'],
                    'commissioncondition' => $data['commissioncondition'], 
                    'promo_code' => $_POST['code'],
                    'discountnum' => $data['discountnum'],
                    'discounttype' => $data['discounttype']
                ) 
            );
        }
    }
}