<?php

add_actions( 'outstanding-payment', 'ticket_outstanding_payment' );
add_actions( 'ticket-outstanding-payment-ajax_page', 'ticket_outstanding_payment_ajax' );

function ticket_outstanding_payment()
{
    if( is_detail() )
    {
        if( isset( $_GET['id'] ) )
        {
            return ticket_detail_outstanding_payment();
        }
        else
        {
            return not_found_template();
        }
    }

    return ticket_outstanding_payment_table();
}

function ticket_outstanding_payment_table()
{
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/outstanding-payment/list.html', 'outstanding-payment' );

    add_block( 'list-block', 'lcblock', 'outstanding-payment' );

    add_variable( 'site_url', $site_url );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-outstanding-payment-ajax/' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'action', get_state_url( 'outstanding-payment' ) );
    add_variable( 'edit_link', get_state_url( 'outstanding-payment&prc=edit' ) );

    parse_template( 'list-block', 'lcblock', false );

    add_actions( 'section_title', 'Outstanding Payment List' );
    add_actions( 'other_elements', 'get_javascript', 'jquery.base64.min' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'outstanding-payment' );
}

function ticket_detail_outstanding_payment()
{
    run_save_outstanding_payment();

    if( isset( $_POST ) && !empty( $_POST ) )
    {
        header( 'Location:' . get_state_url('outstanding-payment&prc=detail&id=' . $_GET['id'] ) . '&prm=' . base64_encode( json_encode( $_POST ) ) );

        exit;
    }

    $site_url = site_url();
    $filter   = get_outstanding_filter();
    $agent    = get_agent( $_GET['id'] );
    $agchanel = get_channel( $agent['chid'], 'chname' );

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/outstanding-payment/detail.html', 'outstanding-payment' );

    add_block( 'no-loop-block', 'nlclblock', 'outstanding-payment' );
    add_block( 'field-block', 'fclblock', 'outstanding-payment' );
    add_block( 'loop-block', 'lclblock', 'outstanding-payment' );
    add_block( 'detail-block', 'lcblock', 'outstanding-payment' );

    ticket_detail_outstanding_list( $_GET['id'], $rid, $bddate );

    if( is_array( $bdid ) && empty( $bdid ) === false )
    {
        foreach( $bdid as $i => $val )
        {
            add_variable( 'f_bdid', $val );
            add_variable( 'f_bid', $bid[ $i ] );
            add_variable( 'f_agid', $agent['agid'] );
            add_variable( 'f_btotal', $btotal[ $i ] );
            add_variable( 'f_atdate', $atdate[ $i ] );
            add_variable( 'f_atgname', $atgname[ $i ] );
            add_variable( 'f_bticket', $bticket[ $i ] );
            add_variable( 'f_atdebet', $atdebet[ $i ] );
            add_variable( 'f_atcredit', $atcredit[ $i ] );
            add_variable( 'f_atpaidoff', $atpaidoff[ $i ] );
            add_variable( 'f_atremaining', $atremaining[ $i ] );
            add_variable( 'f_vmax', 'data-v-max="'. $atremaining[ $i ] . '"' );
            add_variable( 'f_payment_method_list', get_booking_payment_method_option( $atmethod[ $i ] ) );

            parse_template( 'field-block', 'fclblock', true );
        }
    }

    add_variable( 'bddate', $bddate );
    add_variable( 'agchanel', $agchanel );
    add_variable( 'agid', $agent['agid'] );
    add_variable( 'atdate', date( 'd F Y' ) );
    add_variable( 'agname', $agent['agname'] );
    add_variable( 'message', generate_message_block() );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );
    add_variable( 'agent_invoice_list', get_agent_invoice_list( $agent['agid'], $bid_opt, $rid, $bddate ) );
    add_variable( 'payment_method_list', get_booking_payment_method_option( $atmethod ) );

    add_variable( 'site_url', $site_url );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-outstanding-payment-ajax/' );

    add_actions( 'section_title', 'Outstanding Payment Detail' );

    add_actions( 'header_elements', 'get_custom_css', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    add_actions( 'other_elements', 'get_javascript', 'jquery.base64.min' );
    add_actions( 'other_elements', 'get_custom_javascript', 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js' );
    add_actions( 'other_elements', 'get_custom_javascript', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' );

    parse_template( 'detail-block', 'lcblock', false );

    return return_template( 'outstanding-payment' );
}

function run_save_outstanding_payment()
{
    global $flash;

    if( is_publish() )
    {
        $error = validate_outstanding_payment_data();

        if( empty( $error ) )
        {
            $id = save_batch_outstanding_payment();

            if( empty( $id ) === false )
            {
                $prm = array(
                    'bid' => '',
                    'bdid' => '',
                    'btotal' => 0,
                    'atdebet' => 0,
                    'bid_opt' => '',
                    'bticket' => '',
                    'atgname' => '',
                    'atcredit' => 0,
                    'atpaidoff' => 0,
                    'atmethod' => '',
                    'atremaining' => 0,
                    'rid' => $_POST['rid'],
                    'atdate' => date( 'd F Y'),
                    'bddate' => $_POST['bddate']
                );

                $flash->add( array( 'type'=> 'success', 'content' => array( 'This outstanding payment successfully added to list' ) ) );

                header( 'Location:' . get_state_url( 'outstanding-payment&prc=detail&id=' . $_GET['id'] . '&prm=' . base64_encode( json_encode( $prm ) ) ) );

                exit;
            }
            elseif( $id == 0 )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'This outstanding payment failed' ) ) );
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_outstanding_payment_data()
{
    $error = array();

    if( isset( $_POST['atdate'] ) )
    {
        foreach( $_POST['atdate'] as $val )
        {
            if( empty( $val ) )
            {
                $error[] = 'Date of payment can\'t be empty';
            }

            break;
        }
    }

    if( isset( $_POST['atmethod'] ) )
    {
        foreach( $_POST['atmethod'] as $val )
        {
            if( empty( $val ) )
            {
                $error[] = 'Payment method can\'t be empty';
            }

            break;
        }
    }

    if( isset( $_POST['atcredit'] ) )
    {
        foreach( $_POST['atcredit'] as $i => $val )
        {
            if( empty( $val ) )
            {
                $error[] = 'Paid amount can\'t be empty';
            }
            else if( $val > $_POST['atremaining'][$i] )
            {
                $error[] = 'Paid amount can\'t be greater than remaining amount';
            }

            break;
        }
    }

    return $error;
}

function save_batch_outstanding_payment()
{
    global $db;

    if( isset( $_POST ) && empty( $_POST ) === false )
    {
        $fields = array();
        $values = array();
        $flogs  = array();
        $fname  = array(
            'atdate'   => 'Date of payment',
            'atgname'  => 'Guest name',
            'atdebet'  => 'Debet',
            'atcredit' => 'Credit',
            'atstatus' => 'Status',
            'atmethod' => 'Payment method',
        );

        foreach( $_POST as $field => $val )
        {
            if( is_array( $val ) )
            {
                foreach( $val as $i => $dt )
                {
                    if( in_array( $field, array( 'atdebet', 'atpaidoff', 'atremaining', 'pkey', 'bddate', 'rid', 'bid_opt', 'btotal', 'bticket' ) ) === FALSE && $dt != '' )
                    {
                        if( $field == 'atdate' )
                        {
                            $ppaydate = strtotime( $dt );

                            $fields[ $i ][] = $field;
                            $values[ $i ][] = date( 'Y-m-d', $ppaydate );

                            $flogs[]  = $fname[ $field ] . ' : ' . date( 'd F Y', $ppaydate );
                        }
                        else if( $field == 'atmethod' )
                        {
                            list( $id, $method ) = explode( '|', $dt );

                            $fields[ $i ][] = 'atmid';
                            $values[ $i ][] = $id;

                            $fields[ $i ][] = 'atmethod';
                            $values[ $i ][] = $method;

                            $flogs[]  = $fname[ $field ] . ' : ' . $method;
                        }
                        else
                        {
                            $fields[ $i ][] = $field;
                            $values[ $i ][] = $dt;

                            if( in_array( $field, array( 'bid', 'bdid', 'agid', 'bcode' ) ) === false )
                            {
                                if( $field == 'atcredit' )
                                {
                                    $flogs[] = $fname[ $field ] . ' : ' . number_format( $dt, 0, ',', '.' );
                                }
                                else if( $field == 'atstatus' )
                                {
                                    $flogs[] = $fname[ $field ] . ' : Paid Balance';
                                }
                                else
                                {
                                    $flogs[] = $fname[ $field ] . ' : ' . ( empty( $dt ) ? 'Empty Value' : $dt );
                                }
                            }
                        }
                    }
                }
            }
        }

        $pid = array();

        if( empty( $fields ) === false && empty( $values ) === false )
        {
            foreach( $fields as $i => $field )
            {
                $q = 'INSERT INTO ticket_agent_transaction(' . implode( ',', $fields[ $i ] ) . ') VALUES ("' . implode( '" , "', $values[ $i ] ) . '")';
                $r = $db->do_query( $q );

                if( is_array( $r ) === false )
                {         
                    $pid[] = $db->insert_id();

                    //-- Update Booking Status If Payment Completed
                    $data         = ticket_booking_all_data( $_POST['bid'][ $i ] );
                    $bonhandtotal = get_agent_invoice_paid_off( $_POST['agid'][ $i ], $_POST['bid'][ $i ] );
                    $bonhandtotal = $bonhandtotal > $_POST['btotal'][ $i ] ? $_POST['btotal'][ $i ] : $bonhandtotal;

                    if( $bonhandtotal == $_POST['btotal'][ $i ] )
                    {
                        ticket_booking_update_status( $_POST['bid'][ $i ], 'pa' );
                    }

                    //-- Update Booking Last Payment Method
                    if( empty( $_POST['atmethod'][ $i ] ) === false )
                    {
                        list( $id, $method ) = explode( '|', $_POST['atmethod'][ $i ] );

                        $s = 'UPDATE ticket_booking SET bpaymethod = %d WHERE bid = %d';
                        $q = $db->prepare_query( $s, $id, $_POST['bid'][ $i ] );
                        $r = $db->do_query( $q );
                    }

                    if( empty( $flogs ) === false )
                    {
                        save_log( $_POST['bid'][ $i ], 'reservation', 'Add Outstanding Payment #' . $_POST['bticket'][ $i ] . ' : <br/>' . implode( '<br/>', array_unique( $flogs ) ) );
                    }
                }
            }
        }

        return $pid;
    }
}

function save_outstanding_payment()
{
    global $db;

    if( isset( $_POST ) )
    {
        $data   = ticket_booking_all_data( $_POST['bid'] );
        $fields = array();
        $values = array();
        $flogs  = array();
        $fname  = array(
            'atdate'   => 'Date of payment',
            'atgname'  => 'Guest name',
            'atdebet'  => 'Debet',
            'atcredit' => 'Credit',
            'atstatus' => 'Status',
            'atmethod' => 'Payment method',
        );

        foreach( $_POST as $field => $val )
        {
            if( in_array( $field, array( 'atdebet', 'atpaidoff', 'atremaining', 'pkey', 'bddate', 'rid' ) ) === FALSE && $val != '' )
            {
                if( $field == 'atdate' )
                {
                    $ppaydate = strtotime( $val );

                    $fields[] = $field;
                    $values[] = date( 'Y-m-d', $ppaydate );

                    $flogs[]  = $fname[ $field ] . ' : ' . date( 'd F Y', $ppaydate );
                }
                else if( $field == 'atmethod' )
                {
                    list( $id, $method ) = explode( '|', $val );

                    $fields[] = 'atmid';
                    $values[] = $id;

                    $fields[] = 'atmethod';
                    $values[] = $method;

                    $flogs[]  = $fname[ $field ] . ' : ' . $method;
                }
                else
                {
                    $fields[] = $field;
                    $values[] = $val;

                    if( !in_array( $field, array( 'bid', 'agid', 'bcode' ) ) )
                    {
                        if( $field == 'atcredit' )
                        {
                            $flogs[] = $fname[ $field ] . ' : ' . number_format( $val, 0, ',', '.' );
                        }
                        else if( $field == 'atstatus' )
                        {
                            $flogs[] = $fname[ $field ] . ' : Paid Balance';
                        }
                        else
                        {
                            $flogs[] = $fname[ $field ] . ' : ' . ( empty( $val ) ? 'Empty Value' : $val );
                        }
                    }
                }
            }
        }

        //-- Add Guest Name To List
        $pdata   = get_pass_name_and_num_of_pass( $data['detail'] );
        $atgname = isset( $pdata['pass_name'] ) ? implode( ', ', $pdata['pass_name'] ) : '-';

        $fields[] = 'atgname';
        $values[] = $atgname;
        $flogs[]  = $fname[ 'atgname' ] . ' : ' . $atgname;

        $pid = 0;

        if( !empty( $fields ) && !empty( $values ) )
        {
            $q = 'INSERT INTO ticket_agent_transaction(' . implode( ',', $fields ) . ') VALUES ("' . implode( '" , "', $values ) . '")';
            $r = $db->do_query( $q );

            if( !is_array( $r ) )
            {         
                $pid = $db->insert_id();

                //-- Update Booking Status If Payment Completed
                $bonhandtotal = get_agent_invoice_paid_off( $data['agid'], $data['bid'] );
                $bonhandtotal = $bonhandtotal > $data['btotal'] ? $data['btotal'] : $bonhandtotal;

                if( $bonhandtotal == $data['btotal'] )
                {
                    ticket_booking_update_status( $data['bid'], 'pa' );
                }

                //-- Update Booking Last Payment Method
                if( !empty( $_POST['atmethod'] ) )
                {
                    list( $id, $method ) = explode( '|', $_POST['atmethod'] );

                    $s = 'UPDATE ticket_booking SET bpaymethod = %d WHERE bid = %d';
                    $q = $db->prepare_query( $s, $id, $data['bid'] );
                    $r = $db->do_query( $q );
                }

                if( !empty( $flogs ) )
                {
                    save_log( $data[ 'bid' ], 'reservation', 'Add Outstanding Payment #' . $data['bticket'] . ' : <br/>' . implode( '<br/>', array_unique( $flogs ) ) );
                }
            }
        }

        return $pid;
    }
}

function update_outstanding_payment()
{
    global $db;

    if( isset( $_POST ) )
    {
        $odata  = ticket_outstanding_payment_by_id( $_POST[ 'atid' ] );
        $fields = array();
        $flogs  = array();
        $fname  = array(
            'atdate'   => 'Date of payment',
            'atgname'  => 'Guest name',
            'atdebet'  => 'Debet',
            'atcredit' => 'Credit',
            'atstatus' => 'Status',
            'atmethod' => 'Payment method',
        );

        foreach( $_POST as $field => $val )
        {
            if( !in_array( $field, array( 'atdebet', 'atpaidoff', 'atremaining', 'pkey', 'atid', 'bid', 'bcode' ) ) )
            {
                if( $field == 'atmethod' )
                {
                    list( $id, $method ) = explode( '|', $val );

                    $fields[] = $db->prepare_query( 'atmid = %d', $id );
                    $fields[] = $db->prepare_query( 'atmethod = %s', $method );

                    if( $odata[ 'atmethod' ] != $method )
                    {
                        $flogs[] = $fname[ 'atmethod' ] . ' : ' . $odata[ 'atmethod' ] . ' --> ' . $method;
                    }
                }
                else if( $field == 'atdate' )
                {
                    $fields[] = $db->prepare_query( $field . ' = %s', $val );

                    if( $odata[ $field ] != $val )
                    {
                        $flogs[] = $fname[ $field ] . ' : ' . $odata[ 'atdate_format' ] . ' --> ' . date( 'd F Y', strtotime( $val ) );
                    }
                }
                else
                {
                    $fields[] = $db->prepare_query( $field . ' = %s', $val );

                    if( $odata[ $field ] != $val )
                    {
                        if( $field == 'atcredit' )
                        {
                            $flogs[] = $fname[ $field ] . ' : ' . number_format( $odata[ $field ], 0, ',', '.' ) . ' --> ' . number_format( $val, 0, ',', '.' );
                        }
                        else
                        {
                            $flogs[] = $fname[ $field ] . ' : ' . ( empty( $odata[ $field ] ) ? 'Empty Value' : $odata[ $field ] ) . ' --> ' . ( empty( $val ) ? 'Empty Value' : $val );
                        }
                    }
                }
            }
        }

        if( !empty( $fields ) )
        {
            $q = 'UPDATE ticket_agent_transaction SET ' . implode( ',', $fields ) . ' WHERE atid = '. $_POST[ 'atid' ];
            $r = $db->do_query( $q );

            if( !is_array( $r ) )
            {
                $d = ticket_booking_all_data( $_POST['bid'] );

                if( $d['bonhandtotal'] >= $d['btotal'] )
                {
                    ticket_booking_update_status( $_POST['bid'], 'pa' );
                }

                if( !empty( $flogs ) )
                {
                    save_log( $_POST[ 'bid' ], 'reservation', 'Update Outstanding Payment #' . $d['bticket'] . ' : <br/>' . implode( '<br/>', array_unique( $flogs ) ) );
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    else
    {
        return true;
    }
}

function delete_outstanding_payment()
{
    global $db;

    $s = 'DELETE FROM ticket_agent_transaction WHERE atid = %d';
    $q = $db->prepare_query( $s, $_POST[ 'atid' ] );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        $d = ticket_booking_all_data( $_POST['bid'] );

        save_log( $d['bid'], 'reservation', 'Delete Outstanding Payment #' . $d['bticket'] );

        return true;
    }
}

function ticket_outstanding_payment_by_id( $atid )
{
    global $db;

    $s = 'SELECT * FROM ticket_agent_transaction WHERE atid = %d';
    $q = $db->prepare_query( $s, $atid );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) )
    {
        $data = $db->fetch_array( $r );

        $data['atdate_format']   = date( 'd F Y', strtotime( $data['atdate'] ) );
        $data['atcredit_format'] = number_format( ( $data['atcredit'] ), 0, ',', '.' );
    }

    return $data;
}

function get_agent_invoice_summary( $filter )
{
    global $db;

    extract( $filter );

    $w = '';

    if( empty( $rid ) === false )
    {
        $w .= ' AND c.rid =' . $rid;
    }

    if( empty( $bddate ) === false )
    {
        list( $start, $end ) = explode( ', ', $bddate );

        $w .= ' AND c.bddate BETWEEN "' . date( 'Y-m-d', strtotime( $start ) ) . '" AND "' . date( 'Y-m-d', strtotime( $end ) ) . '"';
    }

    $q = 'SELECT
            a.bid,
            c.bdid,
            c.total,
            c.bddate,
            c.bdtype,
            b.bticket,
            a.atgname,
            a.atstatus, (
                SELECT COALESCE( SUM( a2.atcredit ), 0 )
                FROM ticket_agent_transaction AS a2
                WHERE a2.atstatus = 2 AND
                CASE
                    WHEN a2.bdid = 0 THEN a2.bid = a.bid
                    ELSE a2.bdid = c.bdid
                END
            ) AS atpaidoff
          FROM ticket_agent_transaction AS a
          LEFT JOIN ticket_booking AS b ON a.bid = b.bid
          LEFT JOIN ticket_booking_detail AS c ON c.bid = b.bid
          WHERE a.agid = ' . $agid . ' AND a.atstatus = 1 AND b.bstt <> "ar"
          AND c.bdpstatus NOT IN( "cn", "bc" )' . $w . '
          ORDER BY a.atdate ASC';
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            if( $d['total'] > 0 )
            {
                if( $d['atstatus'] == 1 )
                {
                    $d['atstatus'] = 'Confirmed';
                    $d['atdebet']  = $d['total'];
                }
                elseif( $d['atstatus'] == 2 )
                {
                    $d['atstatus'] = 'Paid Balance';
                    $d['atdebet']  = 0;
                }
                elseif( $d['atstatus'] == 3 )
                {
                    $d['atstatus'] = 'Canceled, cancelation fee applied';
                    $d['atdebet']  = 0;
                }

                $data[] = $d;
            }
        }

        krsort( $data );
    }

    return $data;
}

function get_agent_print_invoice_summary( $filter )
{
    $data = get_agent_invoice_summary( $filter );

    if( empty( $data ) === false )
    {
        extract( $filter );

        $from_date  = '';
        $until_date = '';

        if( empty( $bddate ) === false )
        {
            list( $start, $end ) = explode( ', ', $bddate );

            $from_date  = date( 'd F Y', strtotime( $start ) );
            $until_date = date( 'd F Y', strtotime( $end ) );
        }

        $agent   = get_agent( $agid );
        $content = '
        <h1>Open Invoices and Orders Summary</h1>
        <div class="container">
            <div class="row clearfix">
                <div class="col-md-6">
                    <strong>Bill To:</strong>
                    <p><strong>' . $agent['agname'] . '</strong><br/>' . $agent['iagaddress'] . '<br/>Phone: ' . $agent['iagphone'] . '</p>
                </div>
                <div class="push-right">
                    <strong>Date Summary:</strong>
                    <p>From: ' . $from_date . '<br/>To: ' . $until_date . '<br/>Print date: ' . date( 'd M Y, H:i:s' ) . '</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table>
                        <thead>
                            <tr>
                                <th>Date/Trv Date</th>
                                <th>Invoice#</th>
                                <th>Cust. Ref. PO</th>
                                <th width="250">Guest Name</th>
                                <th align="right">Amount</th>
                                <th align="right">Amount Due</th>
                                <th align="center" width="90">Due Date</th>
                            </tr>
                        </thead>
                        <tbody>';

                            $total     = 0;
                            $total_due = 0;

                            foreach( $data as $i => $d )
                            {
                                $atdebet  = $d['atdebet'];
                                $atdebdue = $d['atdebet'] - $d['atpaidoff'];

                                if( $atdebdue == 0 )
                                {
                                    continue;
                                }

                                $tdate = date( 'd F Y', strtotime( $d['bddate'] ) );
                                $due   = date( 'd F Y', strtotime( $d['bddate'] . ' +' . $agent['agcod'] . 'days' ) );

                                $total     += $atdebet;
                                $total_due += $atdebdue;

                                $content .= '
                                <tr>
                                    <td valign="top">' . $tdate . '</td>
                                    <td valign="top">' . $d['bticket'] . '</td>
                                    <td valign="top">' . ( empty( $d['bbrevagent'] ) ? '-' : $d['bbrevagent'] ) . '</td>
                                    <td valign="top">' . $d['atgname'] . '</td>
                                    <td valign="top" align="right">' . ( number_format( $atdebet, 0, ',', '.' ) ) . '</td>
                                    <td valign="top" align="right">' . ( number_format( $atdebdue, 0, ',', '.' ) ) . '</td>
                                    <td valign="top" align="center">' . $due . '</td>
                                </tr>';
                            }

                            $content .= '
                            <tr style="background:rgb(245, 245, 245);">
                                <td colspan="4" align="right"><b>Total :</b></td>
                                <td align="right"><b>' . ( number_format( $total, 0, ',', '.' ) ) . '</b></td>
                                <td align="right"><b>' . ( number_format( $total_due, 0, ',', '.' ) ) . '</b></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>';

        return $content;
    }
}

function get_agent_csv_invoice_summary( $filter )
{
    $data    = get_agent_invoice_summary( $filter );
    $content = '';

    if( empty( $data ) === false )
    {
        extract( $filter );

        $from_date  = '';
        $until_date = '';

        if( empty( $bddate ) === false )
        {
            list( $start, $end ) = explode( ', ', $bddate );

            $from_date  = date( 'd F Y', strtotime( $start ) );
            $until_date = date( 'd F Y', strtotime( $end ) );
        }

        $agent = get_agent( $agid );

        $total     = 0;
        $total_due = 0;

        add_variable( 'from_date', $from_date );
        add_variable( 'until_date', $until_date );
        add_variable( 'print_date', date( 'd M Y, H:i:s' ) );

        add_variable( 'agname', $agent['agname'] );
        add_variable( 'agphone', empty( $agent['iagphone'] ) ? '-' : $agent['iagphone'] );
        add_variable( 'agaddress', empty( $agent['iagaddress'] ) ? '-' : $agent['iagaddress'] );

        $content .= '
        <table width="100%" border="1">
            <tr>
                <td>BlueWater Express</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Jl. Tukad Punggawa, Banjar Ponjok, Kelurahan Serangan</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Denpasar Selatan - 80229, Bali - Indonesia</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Telp. +62 (361) 8951111/12 | Mobile +62 813-3963-8640</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>ar@bluewater-express.com | www.bluewater-express.com</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Open Invoices and Orders Summary</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Bill To:</td>
                <td></td>
                <td></td>
                <td></td>
                <td>Date Summary</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Buffalo Tours</td>
                <td></td>
                <td></td>
                <td></td>
                <td>From:</td>
                <td>' . $from_date . '</td>
                <td></td>
            </tr>
            <tr>
                <td>Jl. Tirta Ening No.9X Sanur</td>
                <td></td>
                <td></td>
                <td></td>
                <td>To</td>
                <td>' . $until_date . '</td>
                <td></td>
            </tr>
            <tr>
                <td>Phone : 0361 289023</td>
                <td></td>
                <td></td>
                <td></td>
                <tdTanggal Cetak:</td>
                <td>' . date( 'd M Y, H:i:s' ) . '</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Date/Trv Date</td>
                <td>Invoice#</td>
                <td>Cust. Ref. PO</td>
                <td>Guest Name</td>
                <td>Amount</td>
                <td>Amount Due</td>
                <td>Due Date</td>
            </tr>';

            $total     = 0;
            $total_due = 0;

            foreach( $data as $i => $d )
            {
                $atdebet  = $d['atdebet'];
                $atdebdue = $d['atdebet'] - $d['atpaidoff'];

                if( $atdebdue == 0 )
                {
                    continue;
                }

                $tdate = date( 'd F Y', strtotime( $d['bddate'] ) );
                $due   = date( 'd F Y', strtotime( $d['bddate'] . ' +' . $agent['agcod'] . 'days' ) );

                $total     += $atdebet;
                $total_due += $atdebdue;

                $content .= '
                <tr>
                    <td>' . $tdate . '</td>
                    <td>' . $d['bticket'] . '</td>
                    <td>' . ( empty( $d['bbrevagent'] ) ? '-' : $d['bbrevagent'] ) . '</td>
                    <td>' . $d['atgname'] . '</td>
                    <td>' . $atdebet . '</td>
                    <td>' . $atdebdue . '</td>
                    <td>' . $due . '</td>
                </tr>';
            }

            $content .= '
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total</td>
                <td>' . $total . '</td>
                <td>' . $total_due . '</td>
                <td></td>
            </tr>
        </table>';
    }

    return $content;
}

function get_agent_pdf_invoice_summary( $filter )
{
    $data = get_agent_invoice_summary( $filter );

    if( empty( $data ) === false )
    {
        extract( $filter );

        $from_date  = '';
        $until_date = '';

        if( empty( $bddate ) === false )
        {
            list( $start, $end ) = explode( ', ', $bddate );

            $from_date  = date( 'd F Y', strtotime( $start ) );
            $until_date = date( 'd F Y', strtotime( $end ) );
        }

        $agent = get_agent( $agid );

        set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-agent-invoice-summary.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        $total     = 0;
        $total_due = 0;

        foreach( $data as $i => $d )
        {
            $atdebet  = $d['atdebet'];
            $atdebdue = $d['atdebet'] - $d['atpaidoff'];

            if( $atdebdue == 0 )
            {
                continue;
            }

            $tdate = date( 'd F Y', strtotime( $d['bddate'] ) );
            $due   = date( 'd F Y', strtotime( $d['bddate'] . ' +' . $agent['agcod'] . 'days' ) );

            $total     += $atdebet;
            $total_due += $atdebdue;

            add_variable( 'due', $due );
            add_variable( 'tdate', $tdate );
            add_variable( 'bticket', $d['bticket'] );
            add_variable( 'atgname', $d['atgname'] );
            add_variable( 'atdebet', number_format( $atdebet, 0, ',', '.' ) );
            add_variable( 'atdebdue', number_format( $atdebdue, 0, ',', '.' ) );
            add_variable( 'bbrevagent', empty( $d['bbrevagent'] ) ? '-' : $d['bbrevagent'] );

            parse_template( 'report-loop-block', 'rl-block', true );
        }

        add_variable( 'from_date', $from_date );
        add_variable( 'until_date', $until_date );
        add_variable( 'print_date', date( 'd M Y, H:i:s' ) );

        add_variable( 'agname', $agent['agname'] );
        add_variable( 'agphone', empty( $agent['iagphone'] ) ? '-' : $agent['iagphone'] );
        add_variable( 'agaddress', empty( $agent['iagaddress'] ) ? '-' : $agent['iagaddress'] );

        add_variable( 'admurl', get_admin_url() );
        add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

        add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
        add_variable( 'total_due', number_format( $total_due, 0, '.' , '.' ) );
        add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
    }
}

function ticket_detail_outstanding_list( $id, $rid, $bddate )
{
    global $db;

    $w = '';

    if( empty( $rid ) === false )
    {
        $w .= ' AND d.rid =' . $rid;
    }

    if( empty( $bddate ) === false )
    {
        list( $start, $end ) = explode( ', ', $bddate );

        $w .= ' AND d.bddate BETWEEN "' . date( 'Y-m-d', strtotime( $start ) ) . '" AND "' . date( 'Y-m-d', strtotime( $end ) ) . '"';
    }

    // queeryy terdahulu bli kadek
    // $q = 'SELECT
    //         a.bid,
    //         a.atid,
    //         a.bdid,
    //         a.atdate,
    //         a.atgname,
    //         b.bticket,
    //         a.atstatus,
    //         a.atcredit,
    //         b.bbrevagent,
    //         GROUP_CONCAT(c.bdid) AS bdid2,
    //         GROUP_CONCAT(c.total) AS total,
    //         GROUP_CONCAT(c.bddate) AS bddate
    //       FROM ticket_agent_transaction AS a
    //       LEFT JOIN ticket_booking AS b ON a.bid = b.bid
    //       LEFT JOIN ticket_booking_detail AS c ON c.bid = b.bid
    //       LEFT JOIN ticket_agent AS d ON a.agid = d.agid
    //       WHERE  a.agid = ' . $id . ' AND b.bstt <> "ar"
    //       AND ( SELECT GROUP_CONCAT( DISTINCT( a2.bdpstatus ) ) FROM ticket_booking_detail AS a2 WHERE a2.bid = a.bid ) NOT IN( "cn","bc" )
    //       AND d.agpayment_type = "Credit" ' . $w . '
    //       GROUP BY a.atid ORDER BY b.bticket ASC';
    
    $s = 'SELECT *
          FROM ticket_agent_transaction AS a
          LEFT JOIN ticket_booking AS b ON a.bid = b.bid
          LEFT JOIN ticket_agent AS c ON a.agid = c.agid
          LEFT JOIN ticket_booking_detail AS d ON d.bid = b.bid
          WHERE b.bstt <> "ar" AND c.agpayment_type = "Credit"
          AND (
            SELECT GROUP_CONCAT( DISTINCT( a2.bdpstatus ) )
            FROM ticket_booking_detail AS a2
            WHERE a2.bid = a.bid
          ) NOT IN( "cn","bc" ) AND d.bdtype = "departure"
          AND a.agid = %d
          ' . $w . '
          ORDER BY a.atdate ASC';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) == 0 )
    {
        parse_template( 'no-loop-block', 'nlclblock', false );
    }
    else
    {
        $data = array();

        $atoutstanding = 0;

        // while( $d = $db->fetch_array( $r ) )
        // {
        //     $bdid = explode( ',', $d['bdid2'] );
        //
        //     if( count( $bdid ) == 2 )
        //     {
        //         $total = explode( ',', $d['total'] );
        //         $date  = explode( ',', $d['bddate'] );
        //
        //         if( $d['atstatus'] == 1 )
        //         {
        //             foreach( $bdid as $i => $v )
        //             {
        //                 if( $total[ $i ] > 0 )
        //                 {
        //                     if( $d['atstatus'] == 1 )
        //                     {
        //                         $d['atstatuss'] = 'Confirmed';
        //                         $d['atdebet']  = $total[ $i ];
        //                     }
        //                     elseif( $d['atstatus'] == 2 )
        //                     {
        //                         $d['atstatuss'] = 'Paid Balance';
        //                         $d['atdebet']  = 0;
        //                     }
        //                     elseif( $d['atstatus'] == 3 )
        //                     {
        //                         $d['atstatuss'] = 'Canceled, cancelation fee applied';
        //                         $d['atdebet']  = 0;
        //                     }
        //
        //                     $atoutstanding = $atoutstanding + ( $d['atdebet'] - $d['atcredit'] );
        //
        //                     $d['atoutstanding'] = $atoutstanding;
        //                     $d['bddate']        = $date[ $i ];
        //
        //                     $data[] = $d;
        //                 }
        //             }
        //         }
        //         else
        //         {
        //             if( $d['bdid'] == 0 )
        //             {
        //                 if( $total[0] > 0 )
        //                 {
        //                     if( $d['atstatus'] == 1 )
        //                     {
        //                         $d['atstatuss'] = 'Confirmed';
        //                         $d['atdebet']  = $total[0];
        //                     }
        //                     elseif( $d['atstatus'] == 2 )
        //                     {
        //                         $d['atstatuss'] = 'Paid Balance';
        //                         $d['atdebet']  = 0;
        //                     }
        //                     elseif( $d['atstatus'] == 3 )
        //                     {
        //                         $d['atstatuss'] = 'Canceled, cancelation fee applied';
        //                         $d['atdebet']  = 0;
        //                     }
        //
        //                     $atoutstanding = $atoutstanding + ( $d['atdebet'] - $d['atcredit'] );
        //
        //                     $d['atoutstanding'] = $atoutstanding;
        //                     $d['bddate']        = $date[0];
        //
        //                     $data[] = $d;
        //                 }
        //             }
        //             else
        //             {
        //                 foreach( $bdid as $i => $v )
        //                 {
        //                     if( $d['bdid'] == $v )
        //                     {
        //                         if( $total[ $i ] > 0 )
        //                         {
        //                             if( $d['atstatus'] == 1 )
        //                             {
        //                                 $d['atstatuss'] = 'Confirmed';
        //                                 $d['atdebet']  = $total[ $i ];
        //                             }
        //                             elseif( $d['atstatus'] == 2 )
        //                             {
        //                                 $d['atstatuss'] = 'Paid Balance';
        //                                 $d['atdebet']  = 0;
        //                             }
        //                             elseif( $d['atstatus'] == 3 )
        //                             {
        //                                 $d['atstatuss'] = 'Canceled, cancelation fee applied';
        //                                 $d['atdebet']  = 0;
        //                             }
        //
        //                             $atoutstanding = $atoutstanding + ( $d['atdebet'] - $d['atcredit'] );
        //
        //                             $d['atoutstanding'] = $atoutstanding;
        //                             $d['bddate']        = $date[ $i ];
        //
        //                             $data[] = $d;
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        //     else
        //     {
        //         if( $d['total'] > 0 )
        //         {
        //             if( $d['atstatus'] == 1 )
        //             {
        //                 $d['atstatuss'] = 'Confirmed';
        //                 $d['atdebet']  = $d['total'];
        //             }
        //             elseif( $d['atstatus'] == 2 )
        //             {
        //                 $d['atstatuss'] = 'Paid Balance';
        //                 $d['atdebet']  = 0;
        //             }
        //             elseif( $d['atstatus'] == 3 )
        //             {
        //                 $d['atstatuss'] = 'Canceled, cancelation fee applied';
        //                 $d['atdebet']  = 0;
        //             }
        //
        //             $atoutstanding = $atoutstanding + ( $d['atdebet'] - $d['atcredit'] );
        //
        //             $d['atoutstanding'] = $atoutstanding;
        //
        //             $data[] = $d;
        //         }
        //     }
        // }
        
        while( $d = $db->fetch_array( $r ) )
        {
            if( $d['atstatus'] == 1 )
            {
                $d['atstatus'] = 'Confirmed';
            }
            elseif( $d['atstatus'] == 2 )
            {
                $d['atstatus'] = 'Paid Balance';
            }
            elseif( $d['atstatus'] == 3 )
            {
                $d['atstatus'] = 'Canceled, cancelation fee applied';
            }

            $atoutstanding = $atoutstanding + ( $d['atdebet'] - $d['atcredit'] );

            $d['atoutstanding'] = $atoutstanding;

            $data[] = $d;
        }

        krsort( $data );

        $url       = get_state_url( 'outstanding-payment&prc=detail&id=' . $id . ( isset( $_GET['prm'] ) ? '&prm=' . $_GET['prm'] : '' ) . '&page=' );
        $page      = empty( $_GET['page'] ) ? 1 : (int) $_GET['page'];
        $total     = count( $data );
        $viewed    = list_viewed();
        $limit     = post_viewed();
        $ttl_pages = ceil( $total / $limit );
        $page      = max( $page, 1 );
        $page      = min( $page, $ttl_pages );
        $offset    = $limit * ( $page - 1 );
        $offset    = $offset < 0 ? 0 : $offset;
        $data      = array_slice( $data, $offset, $limit );

        foreach( $data as $d )
        {
            add_variable( 'bid', $d['bid'] );
            add_variable( 'bdid', $d['bdid'] );
            add_variable( 'bticket', $d['bticket'] );
            add_variable( 'atgname', $d['atgname'] );
            add_variable( 'atstatus', $d['atstatus'] );
            add_variable( 'aclass', $d['atstatus'] == 'Paid Balance' ? '' : 'sr-only' );

            add_variable( 'atid', $d['atid'] );
            add_variable( 'atoutstanding', $d['atoutstanding'] );
            add_variable( 'tdate', date( 'd F Y', strtotime( $d['bddate'] ) ) );
            add_variable( 'atdate', date( 'd F Y', strtotime( $d['atdate'] ) ) );
            add_variable( 'bbrevagent', empty( $d['bbrevagent'] ) ? '-' : $d['bbrevagent'] );

            add_variable( 'atdebet', $d['atdebet'] );
            add_variable( 'atcredit', $d['atcredit'] );

            parse_template( 'loop-block', 'lclblock', true );
        }

        add_variable( 'paging', paging( $url, $total, $page, $limit, $limit ) );
    }
}

function ticket_outstanding_payment_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        0 => 'c.agname',
        1 => 'c.agemail',
        2 => 'outstanding'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'outstanding DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT
                a.agid,
                c.agname,
                c.iagemail,
                SUM(a.atdebet - a.atcredit) AS outstanding
              FROM ticket_agent_transaction AS a
              LEFT JOIN ticket_booking AS b ON a.bid = b.bid
              LEFT JOIN ticket_agent AS c ON a.agid = c.agid
			  LEFT JOIN ticket_booking_detail AS d ON d.bid = b.bid
              WHERE b.bstt <> "ar" AND c.agpayment_type = "Credit" AND (
                SELECT GROUP_CONCAT( DISTINCT( a2.bdpstatus ) )
                FROM ticket_booking_detail AS a2
                WHERE a2.bid = a.bid
              ) NOT IN( "cn","bc" ) AND d.bdtype = "departure"
              GROUP BY a.agid ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search  = array();

        unset( $cols[2] );

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                a.agid,
                c.agname,
                c.iagemail,
                SUM(a.atdebet - a.atcredit) AS outstanding
              FROM ticket_agent_transaction AS a
              LEFT JOIN ticket_booking AS b ON a.bid = b.bid
              LEFT JOIN ticket_agent AS c ON a.agid = c.agid
			  LEFT JOIN ticket_booking_detail AS d ON d.bid = b.bid
              WHERE ( ' . implode( ' OR ', $search ) . ' )
              AND b.bstt <> "ar" AND c.agpayment_type = "Credit" AND (
                SELECT GROUP_CONCAT( DISTINCT( a2.bdpstatus ) )
                FROM ticket_booking_detail AS a2
                WHERE a2.bid = a.bid
              ) NOT IN( "cn","bc" ) AND d.bdtype = "departure"
              GROUP BY a.agid ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $surl = site_url();

            $data[] = array(
                'agid'         => $d2['agid'],
                'agname'       => $d2['agname'],
                'iagemail'     => $d2['iagemail'],
                'outstanding2' => $d2['outstanding'],
                'outstanding'  => number_format( $d2['outstanding'], 0, ',', '.' ),
                'detail_link'  => HTSERVER . $surl . '/l-admin/?state=outstanding-payment&prc=detail&id=' . $d2['agid']
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

function get_agent_invoice_paid_off( $agid, $bid )
{
    global $db;

    $s = 'SELECT atcredit FROM ticket_agent_transaction AS a WHERE a.agid = %d AND a.bid = %s AND a.atstatus = %d';
    $q = $db->prepare_query( $s, $agid, $bid, 2 );
    $r = $db->do_query( $q );

    $atpaidoff = 0;

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $atpaidoff = $atpaidoff + $d['atcredit'];
        }
    }

    return $atpaidoff;
}

function get_agent_invoice_list( $agid, $bid = '', $rid = '', $bddate = '' )
{
    global $db;

    $w = '';

    if( empty( $rid ) === false )
    {
        $w .= ' AND c.rid =' . $rid;
    }

    if( empty( $bddate ) === false )
    {
        list( $start, $end ) = explode( ', ', $bddate );

        $w .= ' AND c.bddate BETWEEN "' . date( 'Y-m-d', strtotime( $start ) ) . '" AND "' . date( 'Y-m-d', strtotime( $end ) ) . '"';
    }

    $q = 'SELECT
            a.bid,
            c.bdid,
            c.total,
            b.btotal,
            c.bdtype,
            c.bddate,
            a.atgname,
            b.bticket, (
                SELECT COALESCE( SUM( a2.atcredit ), 0 )
                FROM ticket_agent_transaction AS a2
                WHERE a2.atstatus = 2 AND
                CASE
                    WHEN a2.bdid = 0 THEN a2.bid = a.bid
                    ELSE a2.bdid = c.bdid
                END
            ) AS atpaidoff
          FROM ticket_agent_transaction AS a
          LEFT JOIN ticket_booking AS b ON a.bid = b.bid
          LEFT JOIN ticket_booking_detail AS c ON c.bid = b.bid
          WHERE DATE(c.bddate) <= CURRENT_DATE() AND a.agid = ' . $agid . ' AND a.atstatus = 1 AND b.bstt <> "ar"
          AND c.bdpstatus NOT IN( "cn", "bc" )' . $w . '
          ORDER BY c.bddate DESC';
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
            if( $d['total'] > $d['atpaidoff'] )
            {
                $d['atremaining'] = $d['total'] - $d['atpaidoff'];

                $data[] = $d;
            }
        }

        $option = '';

        foreach( $data as $d )
        {
            if( is_array( $bid ) )
            {
                $selected = empty( $bid ) === false && in_array( $d['bdid'], $bid ) ? 'selected="selected"' : '';
            }
            else
            {
                $selected = empty( $bid ) === false && $bid == $d['bdid'] ? 'selected="selected"' : '';
            }

            $option  .= '
            <option ' . $selected .' value="' . $d['bdid'] . '" data-atgname="' . $d['atgname'] . '" data-bticket="' . $d['bticket'] . '" data-btotal="' . $d['btotal'] . '" data-bid="' . $d['bid'] . '"  data-amount="' . $d['total'] . '" data-paidoff="' . $d['atpaidoff'] . '" data-remaining="' . $d['atremaining'] . '" data-tripdate="' . date( 'd F Y', strtotime( $d['bddate'] . ' +15days' ) ) . '">
                ' . $d['bticket'] . ' - ' . ucfirst( $d['bdtype'] ) . '
            </option>';
        }

        return $option;
    }
}

function get_outstanding_payment( $atid = '' )
{
    global $db;

    $s = 'SELECT
            a.bid,
            a.atid,
            a.atdate,
            a.atgname,
            b.bticket,
            a.atcredit,
            CONCAT_WS("|", NULLIF(a.atmid,""), NULLIF(a.atmethod,"")) AS apmethod
          FROM ticket_agent_transaction AS a
          LEFT JOIN ticket_booking AS b ON a.bid = b.bid
          WHERE a.atid = %d';
    $q = $db->prepare_query( $s, $atid );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        $data = $db->fetch_array( $r );
    }

    return $data;
}

function get_outstanding_filter()
{
    $filter = array(
        'btotal'      => 0,
        'atdebet'     => 0,
        'atpaidoff'   => 0,
        'atcredit'    => 0,
        'atremaining' => 0,
        'rid'         => '',
        'bddate'      => '',
        'bid'         => '',
        'bid_opt'     => '',
        'atgname'     => '',
        'bticket'     => '',
        'atmethod'    => '',
        'bdid'        => '',
        'atdate'      => date( 'd F Y')
    );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        foreach( $data as $field => $value )
        {
            $filter[ $field ] = $value;
        }
    }

    return $filter;
}

function ticket_outstanding_payment_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'load-data' )
    {
        extract( $_POST );

        $data = ticket_outstanding_payment_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'view-outstanding-payment' )
    {
        extract( $_POST );

        $data = get_outstanding_payment( $atid );

        if( empty( $data ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'update-outstanding-payment' )
    {
        if( update_outstanding_payment() )
        {
            echo json_encode( array( 'result'=> 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result'=> 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-outstanding-payment' )
    {
        if( delete_outstanding_payment() )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    exit;
}
