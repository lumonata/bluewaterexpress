<?php

add_actions( 'rate-period', 'ticket_rate_period' );
add_actions( 'ticket-rate-period-ajax_page', 'ticket_rate_period_ajax' );

function ticket_rate_period()
{
    if( is_num_rate_period() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'schedules&sub=rate-period&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_rate_period();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_rate_period( $_GET['id'] ) )
        {
            return ticket_edit_rate_period();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_rate_period();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_rate_period( $val );
        }
    }

    return ticket_rate_period_table();
}

/*
| -------------------------------------------------------------------------------------
| Rate Period Table List
| -------------------------------------------------------------------------------------
*/
function ticket_rate_period_table()
{
	global $db;

    $site_url = site_url();

    $s = 'SELECT * FROM ticket_rate_period ORDER BY rpid DESC';
    $q = $db->prepare_query( $s );
	$r = $db->do_query( $q );

	set_template( PLUGINS_PATH . '/ticket/tpl/rate-period/list.html', 'rate' );

    add_block( 'list-block', 'rtblock', 'rate' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', ticket_rate_period_table_data( $r ) );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'schedules&sub=rate-period' ) );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=rate-period&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'schedules&sub=rate-period&prc=edit' ) );
    
    parse_template( 'list-block', 'rtblock', false );
    
    add_actions( 'section_title', 'Rate Period List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate' );
}

/*
| -------------------------------------------------------------------------------------
| Rate Period Table List Item
| -------------------------------------------------------------------------------------
*/
function ticket_rate_period_table_data( $r )
{
    global $db;
    
    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="3">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/rate-period/loop.html', 'rate-loop' );

    add_block( 'loop-block', 'rtloop', 'rate-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'id', $d['rpid'] );
        add_variable( 'rpfrom', date('d F Y', strtotime( $d['rpfrom'] ) ) );
        add_variable( 'rpto', date('d F Y', strtotime( $d['rpto'] ) ) );
        add_variable( 'edit_link', get_state_url( 'schedules&sub=rate-period&prc=edit&id=' . $d['rpid'] ) );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-rate-period-ajax/' );

        parse_template( 'loop-block', 'rtloop', true );
    }

    return return_template( 'rate-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Rate Period
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_rate_period()
{
	$message = run_save_rate_period();
    $data    = get_rate_period();

	set_template( PLUGINS_PATH . '/ticket/tpl/rate-period/form.html', 'rate' );
    add_block( 'form-block', 'rtblock', 'rate' );

    add_variable( 'rpid', $data['rpid'] );
    add_variable( 'rpto', empty( $data['rpto'] ) ? '' : date( 'd F Y', strtotime( $data['rpto'] ) ) );
    add_variable( 'rpfrom', empty( $data['rpfrom'] ) ? '' : date( 'd F Y', strtotime( $data['rpfrom'] ) ) );

    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'schedules&sub=rate-period&prc=add_new' ) );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=rate-period' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'rtblock', false );
    
    add_actions( 'section_title', 'New Rate Period' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Rate Period
| -------------------------------------------------------------------------------------
*/
function ticket_edit_rate_period()
{
    $message = run_update_rate_period();
    $data    = get_rate_period( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/rate-period/form.html', 'rate' );
    add_block( 'form-block', 'rtblock', 'rate' );

    add_variable( 'rpid', $data['rpid'] );
    add_variable( 'rpto', empty( $data['rpto'] ) ? '' : date( 'd F Y', strtotime( $data['rpto'] ) ) );
    add_variable( 'rpfrom', empty( $data['rpfrom'] ) ? '' : date( 'd F Y', strtotime( $data['rpfrom'] ) ) );    

    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'schedules&sub=rate-period&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'rate-period' ) );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-rate-period-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=rate-period' ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'rtblock', false );
    
    add_actions( 'section_title', 'Edit Rate Period' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Rate Period
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_rate_period()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/rate-period/batch-delete.html', 'rate' );
    add_block( 'loop-block', 'rtlblock', 'rate' );
    add_block( 'delete-block', 'rtblock', 'rate' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_rate_period( $val );

        add_variable('rpfrom', date( 'd F Y', strtotime( $d['rpfrom'] ) ) );
        add_variable('rpto', date( 'd F Y', strtotime( $d['rpto'] ) ) );
        add_variable('rpid', $d['rpid'] );

        parse_template('loop-block', 'rtlblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' period? :' );
    add_variable('action', get_state_url('schedules&sub=rate-period'));

    parse_template( 'delete-block', 'rtblock', false );
    
    add_actions( 'section_title', 'Delete Rate Period' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate' );
}

function run_save_rate_period()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_rate_period_data();

        if( empty($error) )
        {
            $post_id = save_rate_period();
            
            header( 'location:'.get_state_url( 'schedules&sub=rate-period&prc=add_new&result=1' ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'New rate period successfully saved' ) );
    }
}

function run_update_rate_period()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_rate_period_data();

        if( empty($error) )
        {
            $post_id = update_rate_period();
            
            header( 'location:'.get_state_url( 'schedules&sub=rate-period&prc=edit&result=1&id=' . $post_id ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This rate period successfully edited' ) );
    }
}

function validate_rate_period_data()
{
    $error = array();

    if( isset($_POST['rpfrom']) && empty( $_POST['rpfrom'] ) )
    {
        $error[] = 'Start period date can\'t be empty';
    }

    if( isset($_POST['rpto']) && empty( $_POST['rpto'] ) )
    {
        $error[] = 'End period date can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Period List Count
| -------------------------------------------------------------------------------------
*/
function is_num_rate_period()
{
    global $db;

    $s = 'SELECT * FROM ticket_rate_period';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Period By ID
| -------------------------------------------------------------------------------------
*/
function get_rate_period( $id = '', $field = '' )
{
    global $db;

    $data = array( 
        'rpid'          => ( isset( $_POST['rpid'] ) ? $_POST['rpid'] : null ), 
        'rpfrom'        => ( isset( $_POST['rpfrom'] ) ? $_POST['rpfrom'] : '' ), 
        'rpto'          => ( isset( $_POST['rpto'] ) ? $_POST['rpto'] : '' ), 
        'rpstatus'      => ( isset( $_POST['rpstatus'] ) ? $_POST['rpstatus'] : '' ), 
        'rpcreateddate' => ( isset( $_POST['rpcreateddate'] ) ? $_POST['rpcreateddate'] : '' ), 
        'luser_id'      => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' ),
        'rperiod'       => ( isset( $_POST['rperiod'] ) ? $_POST['rperiod'] : '' ),
    );

    $s = 'SELECT * FROM ticket_rate_period WHERE rpid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'rpid' => $d['rpid'], 
                'rpfrom' => $d['rpfrom'], 
                'rpto' => $d['rpto'], 
                'rpstatus' => $d['rpstatus'], 
                'rpcreateddate' => $d['rpcreateddate'], 
                'luser_id' => $d['luser_id'],
                'rperiod' => date( 'd F Y', strtotime( $d['rpfrom'] ) ) . ' - ' . date( 'd F Y', strtotime( $d['rpto'] ) )
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Period List
| -------------------------------------------------------------------------------------
*/
function get_rate_period_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_rate_period';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array( 
            'rpid' => $d['rpid'], 
            'rpfrom' => $d['rpfrom'], 
            'rpto' => $d['rpto'], 
            'rpstatus' => $d['rpstatus'], 
            'rpcreateddate' => $d['rpcreateddate'], 
            'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Rate Period
| -------------------------------------------------------------------------------------
*/
function save_rate_period()
{
    global $db;

    $status = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );
    
    $s = 'INSERT INTO ticket_rate_period(
            rpfrom,
            rpto,
            rpstatus,
            rpcreateddate,
            luser_id)
          VALUES( %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s,
              date( 'Y-m-d', strtotime( $_POST['rpfrom'] ) ),
              date( 'Y-m-d', strtotime( $_POST['rpto'] ) ),
              $status,
              date( 'Y-m-d H:i:s' ),
              $_COOKIE['user_id'] );
       
    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();

        $from = date( 'd/m/Y', strtotime( $_POST['rpfrom'] ) );
        $to   = date( 'd/m/Y', strtotime( $_POST['rpto'] ) );

        save_log( $id, 'rate-period', 'Add new rate period from ' . $from . ' until ' . $to );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Rate Period
| -------------------------------------------------------------------------------------
*/
function update_rate_period()
{
    global $db;

    $current = get_rate_period( $_POST['rpid'] );
    $status  = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );

    $s = 'UPDATE ticket_rate_period SET 
            rpfrom = %s, 
            rpto = %s, 
            rpstatus = %s
          WHERE rpid = %d';     
    $q = $db->prepare_query( $s,
              date( 'Y-m-d', strtotime( $_POST['rpfrom'] ) ),
              date( 'Y-m-d', strtotime( $_POST['rpto'] ) ),
              $status,
              $_POST['rpid'] );
       
    if( $db->do_query( $q ) )
    {
        $from = date( 'd/m/Y', strtotime( $_POST['rpfrom'] ) );
        $to   = date( 'd/m/Y', strtotime( $_POST['rpto'] ) );

        if( $current['rpfrom'] != $_POST['rpfrom'] || $current['rpto'] != $_POST['rpto'] )
        {
            $cfrom = date( 'd/m/Y', strtotime( $current['rpfrom'] ) );
            $cto   = date( 'd/m/Y', strtotime( $current['rpto'] ) );

            save_log( $_POST['rpid'], 'rate-period', 'Update rate period<br/>From : ' . $cfrom . ' until ' . $cto . '<br/>To : ' . $from . ' until ' . $to );
        }
        else
        {
            save_log( $_POST['rpid'], 'rate-period', 'Update rate period from ' . $from . ' until ' . $to );
        }

        return $_POST['rpid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Rate Period
| -------------------------------------------------------------------------------------
*/
function delete_rate_period( $rpid = '', $is_ajax = false )
{
    global $db;

    $d = get_rate_period( $rpid );

    $s = 'DELETE FROM ticket_rate_period WHERE rpid = %d';          
    $q = $db->prepare_query( $s, $rpid );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'schedules&sub=rate-period&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        $from = date( 'd/m/Y', strtotime( $d['rpfrom'] ) );
        $to   = date( 'd/m/Y', strtotime( $d['rpto'] ) );

        save_log( $rpid, 'rate-period', 'Delete rate period from ' . $from . ' until ' . $to );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Period Option
| -------------------------------------------------------------------------------------
*/
function get_rate_period_option( $rpid = '', $empty_value = true )
{
    $rperiod = get_rate_period_list();
    $option  = $empty_value ? '<option value="">Select Rate Period</option>' : '';

    if( !empty( $rperiod ) )
    {
        foreach( $rperiod as $d )
        {
            $option .= '
            <option value="' . $d['rpid'] . '" ' . ( $d['rpid'] == $rpid ? 'selected' : '' ) . ' >
                ' . date( 'd F Y', strtotime( $d['rpfrom'] ) ) . ' - ' . date( 'd F Y', strtotime( $d['rpto'] ) ) .'
            </option>';   
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_rate_period_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-rate-period' )
    {
        $d = delete_rate_period( $_POST['rpid'], true );

        if( $d===true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}

?>