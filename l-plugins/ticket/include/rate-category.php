<?php

add_actions( 'rate-category', 'ticket_rate_category' );
add_actions( 'rate-category-ajax_page', 'ticket_rate_category_ajax' );

function ticket_rate_category()
{
    if( is_num_rate_category() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'schedules&sub=rate-category&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_rate_category();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_rate_category( $_GET['id'] ) )
        {
            return ticket_edit_rate_category();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_rate_category();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_rate_category( $val );
        }
    }

    return ticket_rate_category_table();
}

/*
| -------------------------------------------------------------------------------------
| Rate Category Table List
| -------------------------------------------------------------------------------------
*/
function ticket_rate_category_table()
{
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/rate-category/list.html', 'rate-category' );

    add_block( 'list-block', 'lcblock', 'rate-category' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'schedules&sub=rate-category' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/rate-category-ajax/' );
    add_variable( 'edit_link', get_state_url( 'schedules&sub=rate-category&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=rate-category&prc=add_new' ) );

    parse_template( 'list-block', 'lcblock', false );

    add_actions( 'section_title', 'Rate Category List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate-category' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Rate Category
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_rate_category()
{
	run_save_rate_category();

    $site_url = site_url();
    $data     = get_rate_category();

	set_template( PLUGINS_PATH . '/ticket/tpl/rate-category/form.html', 'rate-category' );
    add_block( 'form-block', 'lcblock', 'rate-category' );

    add_variable( 'rctid', $data['rctid'] );
    add_variable( 'rctname', $data['rctname'] );
    add_variable( 'rctvalue', get_rate_category_table( $data['rctvalue'] ) );
    add_variable( 'rcstatus', get_rate_category_status_option( $data['rcstatus'], false ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=rate-category' ) );
    add_variable( 'action', get_state_url( 'schedules&sub=rate-category&prc=add_new' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'New Rate Category' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate-category' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Rate Category
| -------------------------------------------------------------------------------------
*/
function ticket_edit_rate_category()
{
    run_update_rate_category();

    $site_url = site_url();
    $data     = get_rate_category( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/rate-category/form.html', 'rate-category' );
    add_block( 'form-block', 'lcblock', 'rate-category' );

    add_variable( 'rctid', $data['rctid'] );
    add_variable( 'rctname', $data['rctname'] );
    add_variable( 'rctvalue', get_rate_category_table( $data['rctvalue'] ) );
    add_variable( 'rcstatus', get_rate_category_status_option( $data['rcstatus'], false ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=rate-category' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/rate-category-ajax/' );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'rate-category' ) );
    add_variable( 'action', get_state_url( 'schedules&sub=rate-category&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'lcblock', false );

    add_actions( 'section_title', 'Edit Rate Category' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate-category' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Rate Category
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_rate_category()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/rate-category/batch-delete.html', 'rate-category' );
    add_block( 'loop-block', 'lclblock', 'rate-category' );
    add_block( 'delete-block', 'lcblock', 'rate-category' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_rate_category( $val );

        add_variable('rctname',  $d['rctname'] );
        add_variable('rctid', $d['rctid'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' rate-category? :' );
    add_variable('action', get_state_url('schedules&sub=rate-category'));

    parse_template( 'delete-block', 'lcblock', false );

    add_actions( 'section_title', 'Delete Rate Category' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate-category' );
}

function run_save_rate_category()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_rate_category_data();

        if( empty( $error ) )
        {
            $post_id = save_rate_category();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to add new rate category' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New rate category successfully saved' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=rate-category&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_rate_category()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_rate_category_data();

        if( empty($error) )
        {
            $post_id = update_rate_category();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this sales channel' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This rate category successfully edited' ) ) );

                header( 'location:' . get_state_url( 'schedules&sub=rate-category&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_rate_category_data()
{
    $error = array();

    if( isset( $_POST['rctname'] ) && empty( $_POST['rctname'] ) )
    {
        $error[] = 'Rate Category name can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Category List Count
| -------------------------------------------------------------------------------------
*/
function is_num_rate_category()
{
    global $db;

    $s = 'SELECT * FROM ticket_rate_category';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Rate Category Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_rate_category_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        1 => 'a.rctid'
    );

    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.rctid DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT * FROM ticket_rate_category AS a ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT * FROM ticket_rate_category AS a WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'rctid'     => $d2['rctid'],
                'rctname'   => $d2['rctname'],
                'rcstatus'  => ( $d2['rcstatus'] == '0' ? 'Publish' : 'Unpublish' ),
                'edit_link' => get_state_url( 'schedules&sub=rate-category&prc=edit&id=' . $d2['rctid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Category By ID
| -------------------------------------------------------------------------------------
*/
function get_rate_category( $id = '', $field = '' , $type = '' )
{
    global $db;

    $data = array(
        'rctid'    => ( isset( $_POST['rctid'] ) ? $_POST['rctid'] : null ),
        'rctname'  => ( isset( $_POST['rctname'] ) ? $_POST['rctname'] : '' ),
        'rctvalue' => ( isset( $_POST['rctvalue'] ) ? $_POST['rctvalue'] : '' ),
        'rcstatus' => ( isset( $_POST['rcstatus'] ) ? $_POST['rcstatus'] : '' ),
        'luser_id' => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' ),
        'rctorder' => ( isset( $_POST['rctorder'] ) ? $_POST['rctorder'] : '' )
    );

    $s = 'SELECT * FROM ticket_rate_category WHERE rctid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'rctid'    => $d['rctid'],
                'rctname'  => $d['rctname'],
                'rctvalue' => $d['rctvalue'],
                'rcstatus' => $d['rcstatus'],
                'luser_id' => $d['luser_id'],
                'rctorder' => $d['rctorder']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Category By Name
| -------------------------------------------------------------------------------------
*/
function get_rate_category_by_name( $name = '', $field = '' )
{
    global $db;

    $data = array(
        'rctid'    => ( isset( $_POST['rctid'] ) ? $_POST['rctid'] : null ),
        'rctname'  => ( isset( $_POST['rctname'] ) ? $_POST['rctname'] : '' ),
        'rctvalue' => ( isset( $_POST['rctvalue'] ) ? $_POST['rctvalue'] : '' ),
        'rcstatus' => ( isset( $_POST['rcstatus'] ) ? $_POST['rcstatus'] : '' ),
        'luser_id' => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' )
    );

    $s = 'SELECT * FROM ticket_rate_category WHERE rctname = %s';
    $q = $db->prepare_query( $s, $name );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'rctid'    => $d['rctid'],
                'rctname'  => $d['rctname'],
                'rctvalue' => $d['rctvalue'],
                'rcstatus' => $d['rcstatus'],
                'luser_id' => $d['luser_id']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Category List
| -------------------------------------------------------------------------------------
*/
function get_rate_category_list( $where = array() )
{
    global $db;

    $s = 'SELECT * FROM ticket_rate_category';

    if( empty( $where ) === false )
    {
        $w = array();

        foreach( $where as $field => $val )
        {
            $w[] = $db->prepare_query( $field . ' = %s', $val );
        }

        $s .= ' WHERE ' . implode( 'AND', $w );
    }
    
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'rctid'    => $d['rctid'],
            'rctname'  => $d['rctname'],
	        'rctvalue' => $d['rctvalue'],
	        'rcstatus' => $d['rcstatus'],
	        'luser_id' => $d['luser_id']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Rate Category
| -------------------------------------------------------------------------------------
*/
function save_rate_category()
{
    global $db;

    $status = is_save_draft() ? 'draft' : ( is_publish() ? 'publish' : '' );

    $s = 'INSERT INTO ticket_rate_category(
            rctname,
            rctvalue,
            rcstatus,
            luser_id)
          VALUES( %s, %s, %s, %d )';
    $q = $db->prepare_query( $s, $_POST['rctname'], json_encode( $_POST['rate'] ), $_POST['rcstatus'], $_COOKIE['user_id'] );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        $id = $db->insert_id();

        save_log( $id, 'rate-category', 'Add new rate category - ' . $_POST['rctname'] );

        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Rate Category
| -------------------------------------------------------------------------------------
*/
function update_rate_category()
{
    global $db;

    $current = get_rate_category( $_POST['rctid'] );
    $status  = is_save_draft() ? '1' : ( is_publish() ? '0' : '' );

    $s = 'UPDATE ticket_rate_category SET
            rctname = %s,
            rctvalue = %s,
            rcstatus = %s
          WHERE rctid = %d';
    $q = $db->prepare_query( $s, $_POST['rctname'], json_encode( $_POST['rate'] ), $status, $_POST['rctid'] );
    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        if( $current['rctname'] != $_POST['rctname'] )
        {
            save_log( $_POST['rctid'], 'location', 'Update rate category - ' . $current['rctname'] . ' to ' . $_POST['rctname'] );
        }
        else
        {
            save_log( $_POST['rctid'], 'location', 'Update rate category - ' . $_POST['rctname'] );
        }

        return $_POST['rctid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Rate Category
| -------------------------------------------------------------------------------------
*/
function delete_rate_category( $id = '', $is_ajax = false )
{
    global $db;

    $d = get_rate_category( $id );

    $s = 'DELETE FROM ticket_rate_category WHERE rctid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'schedules&sub=rate-category&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'location', 'Delete rate category - ' . $d['rctname'] );

        return true;
    }
}

function get_rate_category_table( $rctvalue = '' )
{
	$rctvalue = json_decode( $rctvalue, true );
	$location = get_location_list();

    if( !empty( $location ) )
    {
        $depart = array();
        $destin = array();

        foreach( $location as $key => $d )
        {
            if( $d['lctype'] == '0' )
            {
                $depart[] = $d;
            }
            elseif( $d['lctype'] == '1' )
            {
                $destin[] = $d;
            }
            elseif( $d['lctype'] == '2' )
            {
                $depart[] = $d;
                $destin[] = $d;
            }
        }

        $list = '
        <div class="net-price-block">
            <table class="table-striped net-rate-list" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th rowspan="3">From/To</th>
                        <th rowspan="3">To/From</th>
                        <th colspan="6">Rate</th>
                    </tr>
                    <tr>
                        <th colspan="3">Include Transport</th>
                        <th colspan="3">Exclude Transport</th>
                    </tr>
                    <tr>
                        <th>Adult</th>
                        <th>Child</th>
                        <th>Infant</th>
                        <th>Adult</th>
                        <th>Child</th>
                        <th>Infant</th>
                    </tr>
                </thead>
                <tbody>';

                	foreach( $depart as $dt )
                    {
                        foreach( $destin as $ds )
                        {
                            if( $dt['lcid'] == $ds['lcid'] )
                            {
                                continue;
                            }

                            $index = $dt['lcid'] . '|' . $ds['lcid'];

                            $adult_rate_inc_trans  = isset( $rctvalue[ $index ]['adult_rate_inc_trans'] ) ? $rctvalue[ $index ]['adult_rate_inc_trans'] : 0;
                            $child_rate_inc_trans  = isset( $rctvalue[ $index ]['child_rate_inc_trans'] ) ? $rctvalue[ $index ]['child_rate_inc_trans'] : 0;
                            $infant_rate_inc_trans = isset( $rctvalue[ $index ]['infant_rate_inc_trans'] ) ? $rctvalue[ $index ]['infant_rate_inc_trans'] : 0;

                            $adult_rate_exc_trans  = isset( $rctvalue[ $index ]['adult_rate_exc_trans'] ) ? $rctvalue[ $index ]['adult_rate_exc_trans'] : 0;
                            $child_rate_exc_trans  = isset( $rctvalue[ $index ]['child_rate_exc_trans'] ) ? $rctvalue[ $index ]['child_rate_exc_trans'] : 0;
                            $infant_rate_exc_trans = isset( $rctvalue[ $index ]['infant_rate_exc_trans'] ) ? $rctvalue[ $index ]['infant_rate_exc_trans'] : 0;

                            $list .= '
                            <tr>
                                <td>' . $dt['lcname'] . '</td>
                                <td>' . $ds['lcname'] . '</td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][adult_rate_inc_trans]" value="' . $adult_rate_inc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][child_rate_inc_trans]" value="' . $child_rate_inc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][infant_rate_inc_trans]" value="' . $infant_rate_inc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][adult_rate_exc_trans]" value="' . $adult_rate_exc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][child_rate_exc_trans]" value="' . $child_rate_exc_trans . '" type="text"></td>
                                <td><input class="text form-control" name="rate[' . $dt['lcid'] . '|' . $ds['lcid'] . '][infant_rate_exc_trans]" value="' . $infant_rate_exc_trans . '" type="text"></td>
                            </tr>';
                        }
                    }

                    $list .= '
                </tbody>
            </table>
        </div>';

    	return $list;
   	}
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Category Option
| -------------------------------------------------------------------------------------
*/
function get_rate_category_option( $rctid = '', $use_empty = true, $empty_text = 'Select Rate Category', $type = '' )
{
    $rcat   = get_rate_category_list();
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $rcat ) )
    {
        foreach( $rcat as $d )
        {
            $option .= '<option value="' . $d['rctid'] . '" ' . ( $d['rctid'] == $rctid ? 'selected' : '' ) . ' >' . $d['rctname'] . '</option>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Category Status Option
| -------------------------------------------------------------------------------------
*/
function get_rate_category_status_option( $rctid = '', $use_empty = true, $empty_text = 'Select Rate Category', $type = '' )
{
    $option  = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';
    $option .= '
    <option value="0" ' . ( $rctid == '0' ? 'selected' : '' ) . '>Publish</option>
    <option value="1" ' . ( $rctid == '1' ? 'selected' : '' ) . '>Unpublish</option>';

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_rate_category_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data' )
    {
        $data = ticket_rate_category_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-rate-category' )
    {
        $d = delete_rate_category( $_POST['rctid'], true );

        if( $d === true )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo json_encode( $d );
        }
    }
}

?>