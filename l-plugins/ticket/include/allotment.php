<?php

add_actions( 'by-schedule', 'ticket_allotment_by_schedule' );
add_actions( 'by-online', 'ticket_allotment_by_online' );
add_actions( 'by-agent', 'ticket_allotment_by_agent' );
add_actions( 'all-agent', 'ticket_allotment_all_agent' );
add_actions( 'ticket-allotment-ajax_page', 'ticket_allotment_ajax' );

function ticket_allotment_by_schedule()
{
	if( is_edit() )
    {
        if( isset( $_GET['id'] ) )
        {
            return ticket_edit_allotment_by_schedule();
        }
        else
        {
        	return not_found_template();
        }
    }

	return ticket_allotment_by_schedule_table();
}

function ticket_allotment_by_agent()
{
	if( is_edit() )
    {
        if( isset( $_GET['id'] ) )
        {
            return ticket_edit_allotment_by_agent();
        }
        else
        {
        	return not_found_template();
        }
    }
    elseif( is_detail() )
    {
    	if( isset( $_GET['id'] ) )
        {
            return ticket_detail_allotment_by_agent();
        }
        else
        {
        	return not_found_template();
        }
    }

	return ticket_allotment_by_agent_table();
}

function ticket_allotment_by_online()
{
    if( is_edit() )
    {
        return ticket_edit_allotment_by_online();
    }
    
    return ticket_detail_allotment_by_online();
}

function ticket_allotment_all_agent()
{
    return ticket_detail_allotment_all_agent();
}

/*
| -------------------------------------------------------------------------------------
| Allotment By Schedule Table List
| -------------------------------------------------------------------------------------
*/
function ticket_allotment_by_schedule_table()
{
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/allotment/sc-list.html', 'al-template' );

    add_block( 'list-block', 'al-list', 'al-template' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'action', get_state_url( 'allotment&sub=by-schedule' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-allotment-ajax/' );
    
    parse_template( 'list-block', 'al-list', false );
    
    add_actions( 'section_title', 'Allotment By Schedule List' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'al-template' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Allotment By Schedule
| -------------------------------------------------------------------------------------
*/
function ticket_edit_allotment_by_schedule()
{
    run_update_allotment_by_schedule();

    $site_url = site_url();
    $data     = get_allotment_by_schedule( $_GET['id'] );

    if( empty( $data ) )
    {
    	return not_found_template();
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/allotment/sc-form.html', 'al-form' );
    add_block( 'form-block', 'alform', 'al-form' );

    add_variable( 'sid', $data['sid'] );
    add_variable( 'aid', $data['aid'] );
    add_variable( 'route', $data['rname'] );
    add_variable( 'boat', $data['boname'] );
    add_variable( 'passenger', $data['bopassenger'] );
    add_variable( 'agent_option', get_agent_option() );
    add_variable( 'allot_exist', count( $data['allotment'] ) );
    add_variable( 'total', array_sum( array_column( $data['allotment'], 'allotment' ) ) );
    add_variable( 'allotment_item', get_allotment_item_by_schedule( $data['sid'], $data['allotment'], $data['new_allotment'] ) );
    add_variable( 'schedule', date( 'd M Y', strtotime( $data['sfrom'] ) ) . ' - ' . date( 'd M Y', strtotime( $data['sto'] ) ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'allotment&sub=by-schedule&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'cancel_link', get_state_url( 'allotment&sub=by-schedule' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-allotment-ajax/' );

    parse_template( 'form-block', 'alform', false );
    
    add_actions( 'section_title', 'Edit Allotment By Schedule' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'al-form' );
}

/*
| -------------------------------------------------------------------------------------
| Allotment By Agent Table List
| -------------------------------------------------------------------------------------
*/
function ticket_allotment_by_agent_table()
{
    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/allotment/ag-list.html', 'al-template' );

    add_block( 'list-block', 'al-list', 'al-template' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'action', get_state_url( 'allotment&sub=by-agent' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-allotment-ajax/' );
    
    parse_template( 'list-block', 'al-list', false );
    
    add_actions( 'section_title', 'Allotment By Agent List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'al-template' );
}

/*
| -------------------------------------------------------------------------------------
| Detail Allotment By Agent
| -------------------------------------------------------------------------------------
*/
function ticket_detail_allotment_by_agent()
{
    $site_url = site_url();
    $data     = get_allotment_by_agent( $_GET['id'] );

    if( empty( $data ) )
    {
        return not_found_template();
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/allotment/ag-detail.html', 'al-detail' );
    add_block( 'detail-block', 'aldetail', 'al-detail' );
    add_block( 'detail-all-block', 'alldetail', 'al-detail' );

    add_variable( 'agname', $data['agname'] );
    add_variable( 'agchanel', $data['chname'] );
    add_variable( 'calendar_detail', get_agent_allotment_calendar_detail( $data['agid'] ) );
    add_variable( 'allotment_table', get_allotment_table_by_agent( $data['agid'], $data['allotment'] ) );
    add_variable( 'last_update_note', get_last_update_note( $data['agid'], 'allotment' ) );
    add_variable( 'edit_link', get_state_url( 'allotment&sub=by-agent&prc=edit&id=' . $data['agid'] ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-allotment-ajax/' );

    parse_template( 'detail-block', 'aldetail', false );
    
    add_actions( 'section_title', 'Detail Allotment Agent' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'al-detail' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Allotment By Agent
| -------------------------------------------------------------------------------------
*/
function ticket_edit_allotment_by_agent()
{
    run_update_allotment_by_agent();

    $site_url = site_url();
    $data     = get_allotment_by_agent( $_GET['id'] );

    if( empty( $data ) )
    {
    	return not_found_template();
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/allotment/ag-form.html', 'al-form' );
    add_block( 'form-block', 'alform', 'al-form' );

    add_variable( 'agid', $data['agid'] );
    add_variable( 'agname', $data['agname'] );
    add_variable( 'agchanel', $data['chname'] );
    add_variable( 'allot_exist', count( $data['allotment'] ) );
    add_variable( 'allotment_item', get_allotment_item_by_agent( $data['agid'], $data['allotment'], $data['new_allotment'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'action', get_state_url( 'allotment&sub=by-agent&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'cancel_link', get_state_url( 'allotment&sub=by-agent' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-allotment-ajax/' );

    parse_template( 'form-block', 'alform', false );
    
    add_actions( 'section_title', 'Edit Allotment By Agent' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'al-form' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Allotment By Online
| -------------------------------------------------------------------------------------
*/
function ticket_edit_allotment_by_online()
{
    run_update_allotment_by_online();

    $site_url = site_url();
    $data     = get_allotment_by_online();

    if( empty( $data ) )
    {
        return not_found_template();
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/allotment/ol-form.html', 'ol-form' );
    add_block( 'form-block', 'alform', 'ol-form' );

    add_variable( 'allot_exist', count( $data['allotment'] ) );
    add_variable( 'allotment_item', get_allotment_item_by_online( $data['allotment'], $data['new_allotment'] ) );

    add_variable( 'message', generate_message_block() );
    add_variable( 'cancel_link', get_state_url( 'allotment&sub=by-online' ) );
    add_variable( 'action', get_state_url( 'allotment&sub=by-online&prc=edit' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-allotment-ajax/' );

    parse_template( 'form-block', 'alform', false );
    
    add_actions( 'section_title', 'Edit Allotment Online' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'ol-form' );
}

/*
| -------------------------------------------------------------------------------------
| Detail Allotment All Agent
| -------------------------------------------------------------------------------------
*/
function ticket_detail_allotment_all_agent()
{
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/allotment/ag-detail.html', 'al-detail' );
    add_block( 'detail-block', 'aldetail', 'al-detail' );
    add_block( 'detail-all-block', 'alldetail', 'al-detail' );

    add_variable( 'agname', 'All Agent' );
    add_variable( 'calendar_detail', get_agent_allotment_calendar_detail( null, true ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-allotment-ajax/' );

    parse_template( 'detail-all-block', 'alldetail', false );
    
    add_actions( 'section_title', 'Detail Allotment Agent' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'al-detail' );
}

/*
| -------------------------------------------------------------------------------------
| Detail Allotment By Online
| -------------------------------------------------------------------------------------
*/
function ticket_detail_allotment_by_online()
{
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/allotment/ol-detail.html', 'ol-detail' );
    add_block( 'detail-block', 'oldetail', 'ol-detail' );

    add_variable( 'name', 'Online' );
    add_variable( 'calendar_detail', get_online_allotment_calendar_detail() );

    add_variable( 'edit_link', get_state_url( 'allotment&sub=by-online&prc=edit' ) );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-allotment-ajax/' );

    parse_template( 'detail-block', 'oldetail', false );
    
    add_actions( 'section_title', 'Detail Allotment Online' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'ol-detail' );
}

/*
| -------------------------------------------------------------------------------------
| Run Update Allotment By Schedule Function
| -------------------------------------------------------------------------------------
*/
function run_update_allotment_by_schedule()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_allotment_by_schedule_data();

        if( empty( $error ) )
        {
            if( empty( $_POST['aexist'] ) )
            {
                save_allotment_by_schedule();
            }
            else
            {
                edit_allotment_by_schedule();
            }
            
            $flash->add( array( 'type'=> 'success', 'content' => array( 'Allotment for this schedule has been updated' ) ) );

            header( 'location:' . get_state_url( 'allotment&sub=by-schedule&prc=edit&id=' . $_GET['id'] ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Run Update Allotment By Agent Function
| -------------------------------------------------------------------------------------
*/
function run_update_allotment_by_agent()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_allotment_by_agent_data();

        if( empty( $error ) )
        {
            if( empty( $_POST['aexist'] ) )
            {
                save_allotment_by_agent();
            }
            else
            {
                edit_allotment_by_agent();
            }
            
            $flash->add( array( 'type'=> 'success', 'content' => array( 'Allotment for this agent has been updated' ) ) );

            header( 'location:' . get_state_url( 'allotment&sub=by-agent&prc=edit&id=' . $_GET['id'] ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Run Update Allotment By Online Function
| -------------------------------------------------------------------------------------
*/
function run_update_allotment_by_online()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_allotment_by_online_data();

        if( empty( $error ) )
        {
            if( empty( $_POST['aexist'] ) )
            {
                save_allotment_by_online();
            }
            else
            {
                edit_allotment_by_online();
            }
            
            $flash->add( array( 'type'=> 'success', 'content' => array( 'Allotment has been updated' ) ) );

            header( 'location:' . get_state_url( 'allotment&sub=by-online&prc=edit' ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Check Allotment data before saving to database
| -------------------------------------------------------------------------------------
*/
function validate_allotment_by_schedule_data()
{
    $error = array();
    $allot = 0;

    if( isset( $_POST['dagid'] ) )
    {
        foreach( $_POST['dagid'] as $i => $agid )
        {        
            if( empty( $agid ) )
            {
                $error[0] = 'Area name can\'t be empty';
            }

            if( empty( $_POST['dallotment'][$i] ) )
            {
                $error[1] = 'Allotment can\'t be empty';
            }
            else
            {
            	$allot = $allot + $_POST['dallotment'][$i];
            }
        }
    }

    if( isset( $_POST['agid'] ) )
    {
        foreach( $_POST['agid'] as $i => $agid )
        {
            if( empty( $agid ) )
            {
                $error[0] = 'Area name can\'t be empty';
            }

            if( empty( $_POST['allotment'][$i] ) )
            {
                $error[1] = 'Allotment can\'t be empty';
            }
            else
            {
            	$allot = $allot + $_POST['allotment'][$i];
            }
        }
    }

    if( isset( $_POST['bopassenger'] ) && $_POST['bopassenger'] < $allot )
    {
    	$error[] = 'Sorry, you already reach maximum capacity of this boat';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Check Allotment data before saving to database
| -------------------------------------------------------------------------------------
*/
function validate_allotment_by_agent_data()
{
    $error = array();
    $sarr  = array();

    if( isset( $_POST['dsid'] ) )
    {
        foreach( $_POST['dsid'] as $i => $sid )
        {        
            if( empty( $sid ) )
            {
                $error[0] = 'Schedule/route can\'t be empty';
            }
            else
            {
                $sarr[] = $sid;
            }

            if( empty( $_POST['dallotment'][$i] ) )
            {
                $error[1] = 'Allotment can\'t be empty';
            }

            if( $_POST['dallotment'][$i] > $_POST['bopassenger'][$i] )
            {
            	$error[2] = 'Sorry, some of allotment already reach maximum capacity of boat';
            }
        }
    }

    if( isset( $_POST['sid'] ) )
    {
        foreach( $_POST['sid'] as $i => $sid )
        {
            if( empty( $sid ) )
            {
                $error[0] = 'Schedule/route can\'t be empty';
            }
            else
            {
                $sarr[] = $sid;
            }

            if( empty( $_POST['allotment'][$i] ) )
            {
                $error[1] = 'Allotment can\'t be empty';
            }

            if( $_POST['allotment'][$i] > $_POST['bopassenger'][$i] )
            {
            	$error[2] = 'Sorry, some of allotment already reach maximum capacity of boat';
            }
        }
    }

    if( count( array_unique( $sarr ) ) < count( $sarr ) )
    {
        $error[3] = 'Schedule/route can\'t have same value';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Check Allotment data before saving to database
| -------------------------------------------------------------------------------------
*/
function validate_allotment_by_online_data()
{
    $error = array();
    $sarr  = array();

    if( isset( $_POST['dsid'] ) )
    {
        foreach( $_POST['dsid'] as $i => $sid )
        {        
            if( empty( $sid ) )
            {
                $error[0] = 'Schedule/route can\'t be empty';
            }
            else
            {
                $sarr[] = $sid;
            }

            if( $_POST['dallotment'][$i] > $_POST['bopassenger'][$i] )
            {
                $error[1] = 'Sorry, some of allotment already reach maximum capacity of boat';
            }
        }
    }

    if( isset( $_POST['sid'] ) )
    {
        foreach( $_POST['sid'] as $i => $sid )
        {
            if( empty( $sid ) )
            {
                $error[0] = 'Schedule/route can\'t be empty';
            }
            else
            {
                $sarr[] = $sid;
            }

            if( $_POST['allotment'][$i] > $_POST['bopassenger'][$i] )
            {
                $error[1] = 'Sorry, some of allotment already reach maximum capacity of boat';
            }
        }
    }

    if( count( array_unique( $sarr ) ) < count( $sarr ) )
    {
        $error[2] = 'Schedule/route can\'t have same value';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Save Allotment
| -------------------------------------------------------------------------------------
*/
function save_allotment( $sid )
{
    global $db;

    $s = 'SELECT aid FROM ticket_allotment WHERE sid = %s';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        return $d['aid'];
    }
    else
    {
        $s = 'INSERT INTO ticket_allotment( sid, acreateddate, luser_id ) VALUES( %d, %s, %d )';
        $q = $db->prepare_query( $s, $sid, date( 'Y-m-d H:i:s' ), $_COOKIE['user_id'] );
           
        if( $db->do_query( $q ) )
        {
            return $db->insert_id();
        }       
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Allotment Agent
| -------------------------------------------------------------------------------------
*/
function save_allotment_agent( $aid, $agid, $allotment )
{
    global $db;

    $s = 'INSERT INTO ticket_allotment_agent( aid, agid, avallot ) VALUES( %d, %d, %d )';
    $q = $db->prepare_query( $s, $aid, $agid, $allotment );

    if( $db->do_query( $q ) )
    {
        $s2 = 'SELECT
                a.aid,
                a.sid,
                a.acreateddate,
                a.luser_id,
                c.sfrom,
                c.sto,
                d.rname,
                e.agname
               FROM ticket_allotment AS a
               LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
               LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
               LEFT JOIN ticket_route AS d ON c.rid = d.rid
               LEFT JOIN ticket_agent AS e ON b.agid = e.agid
               WHERE a.aid = %d AND b.agid = %d';
        $q2 = $db->prepare_query( $s2, $aid, $agid );
        $r2 = $db->do_query( $q2 );

        if( $db->num_rows( $r2 ) > 0 )
        {
            $d2 = $db->fetch_array( $r2 );

            save_log( $aid, 'allotment', 'Add new allotment for agent : ' . $d2['agname'] . '<br/>Route : ' . $d2['rname'] . '<br/>Schedule : ' . date( 'd/m/Y', strtotime( $d2['sfrom'] ) ) . ' until ' . date( 'd/m/Y', strtotime( $d2['sto'] ) ) );
        }

        return $db->insert_id();
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Allotment Online
| -------------------------------------------------------------------------------------
*/
function save_allotment_online( $aid, $allotment )
{
    global $db;

    $s = 'INSERT INTO ticket_allotment_online( aid, avallot ) VALUES( %d, %d )';
    $q = $db->prepare_query( $s, $aid, $allotment );

    if( $db->do_query( $q ) )
    {
        $s2 = 'SELECT
                a.aid,
                a.sid,
                a.acreateddate,
                a.luser_id,
                c.sfrom,
                c.sto,
                d.rname
               FROM ticket_allotment AS a
               LEFT JOIN ticket_allotment_online AS b ON b.aid = a.aid
               LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
               LEFT JOIN ticket_route AS d ON c.rid = d.rid
               WHERE a.aid = %d';
        $q2 = $db->prepare_query( $s2, $aid );
        $r2 = $db->do_query( $q2 );

        if( $db->num_rows( $r2 ) > 0 )
        {
            $d2 = $db->fetch_array( $r2 );

            save_log( $aid, 'allotment', 'Add new allotment online<br/>Route : ' . $d2['rname'] . '<br/>Schedule : ' . date( 'd/m/Y', strtotime( $d2['sfrom'] ) ) . ' until ' . date( 'd/m/Y', strtotime( $d2['sto'] ) ) );
        }

        return $db->insert_id();
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Allotment Detail
| -------------------------------------------------------------------------------------
*/
function save_allotment_detail( $dagid, $allot, $adate, $close = 0, $rallot = 0 )
{
    global $db;

    $s = 'SELECT adid, allotment FROM ticket_allotment_detail WHERE dagid = %d AND adate = %s';
    $q = $db->prepare_query( $s, $dagid, $adate );
    $r = $db->do_query( $q );

    $total = 0;

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $s2 = 'SELECT 
                b.num_adult, 
                b.num_child, 
                b.num_infant, 
                a.bblockingtime, 
                a.bbrevstatus
              FROM ticket_booking AS a 
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              WHERE b.bdpstatus NOT IN ( "cn", "bc", "pf", "ol" ) AND a.bstt <> %s AND b.sid = %d AND b.bddate = %s AND a.agid = %d';
        $q2 = $db->prepare_query( $s2, 'ar', $_POST['sid'], $adate, $_POST['agid'] );
        $r2 = $db->do_query( $q2 );

        if( $db->num_rows( $r2 ) > 0 )
        {
            while( $d2 = $db->fetch_array( $r2 ) )
            {
                if( $d2['bbrevstatus'] == 'Blocking' && strtotime( date( 'Y-m-d H:i:s' ) ) >= $d2['bblockingtime'] )
                {
                    continue;
                }

                $total = $total + ( $d2['num_adult'] + $d2['num_child'] + $d2['num_infant'] );
            }
        }

        if( $total > 0 )
        {
            if( $allot < $rallot )
            {
                $allot = abs( ( $d['allotment'] - $allot ) - $rallot );
            }
            elseif( $allot > $rallot )
            {
                $allot = abs( ( $d['allotment'] + $allot ) - $rallot );
            }
            else
            {
                $allot = $d['allotment'];
            }
            // $remain_allot = $d['allotment'] - $total;

            // if( $remain_allot == 0 )
            // {
            //     $allot = $remain_allot + $allot;
            // }
            // elseif( $remain_allot < 0 )
            // {
            //     $allot = $allot;
            // }
            // else
            // {
            //     if( $d['allotment'] < $allot )
            //     {
            //         $rem_allot  = abs( $remain_allot - $d['allotment'] );
            //         $rem_allot2 = abs( $remain_allot );

            //         if( $rem_allot2 < $rem_allot )
            //         {
            //             $allot = $allot + $rem_allot;
            //         }
            //         else
            //         {
            //             $allot = $allot + $rem_allot2;
            //         }
            //     }
            //     else
            //     {
            //         $allot = $allot + abs( $remain_allot - $d['allotment'] );
            //     }
            // }
        }

        $s3 = 'UPDATE ticket_allotment_detail SET allotment = %d, aclose = %s WHERE adid = %d';
        $q3 = $db->prepare_query( $s3, $allot, $close, $d['adid'] );

        if( $db->do_query( $q3 ) )
        {
            return $d['adid'];
        }
    }
    else
    {
        $remain_allot = 0 - $total;

        $allot = $allot + abs( $remain_allot - 0 );

        $s2 = 'INSERT INTO ticket_allotment_detail( dagid, allotment, adate, aclose ) VALUES( %d, %d, %s, %s )';
        $q2 = $db->prepare_query( $s2, $dagid, $allot, $adate, $close );

        if( $db->do_query( $q2 ) )
        {
            return $db->insert_id();
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Allotment Detail
| -------------------------------------------------------------------------------------
*/
function save_allotment_detail_online( $daoid, $allot, $adate, $close = 0, $rallot = 0 )
{
    global $db;

    //-- Get Total Seat Taken
    $chid = get_meta_data( 'channel_id', 'ticket_setting' );

    $s2 = 'SELECT 
            b.num_adult, 
            b.num_child, 
            b.num_infant, 
            a.bblockingtime, 
            a.bbrevstatus
          FROM ticket_booking AS a 
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          WHERE b.bdpstatus NOT IN ( "cn", "bc", "pf", "ol" ) AND a.bstt <> %s AND b.sid = %d AND b.bddate = %s AND a.chid = %d';
    $q2 = $db->prepare_query( $s2, 'ar', $_POST['sid'], $adate, $chid );
    $r2 = $db->do_query( $q2 );

    $total = 0;

    if( $db->num_rows( $r2 ) > 0 )
    {
        while( $d2 = $db->fetch_array( $r2 ) )
        {
            if( $d2['bbrevstatus'] == 'Blocking' && strtotime( date( 'Y-m-d H:i:s' ) ) >= $d2['bblockingtime'] )
            {
                continue;
            }

            $total = $total + ( $d2['num_adult'] + $d2['num_child'] + $d2['num_infant'] );
        }
    }

    $s = 'SELECT aodid, allotment FROM ticket_allotment_detail_online WHERE daoid = %d AND adate = %s';
    $q = $db->prepare_query( $s, $daoid, $adate );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        if( $total > 0 )
        {
            $remain_allot = $d['allotment'] - $total;

            if( $remain_allot == 0 )
            {
                $allot = $d['allotment'] + $allot;
            }
            elseif( $remain_allot < 0 )
            {
                $allot = $total + $allot;
            }
            else
            {
                if( $d['allotment'] < $allot )
                {
                    $rem_allot  = abs( $remain_allot - $d['allotment'] );
                    $rem_allot2 = abs( $remain_allot );

                    if( $rem_allot2 < $rem_allot )
                    {
                        $allot = $allot + $rem_allot;
                    }
                    else
                    {
                        $allot = $allot + $rem_allot2;
                    }
                }
                else
                {
                    $allot = $allot + abs( $remain_allot - $d['allotment'] );
                }
            }
        }

        $s3 = 'UPDATE ticket_allotment_detail_online SET allotment = %d, aclose = %s WHERE aodid = %d';
        $q3 = $db->prepare_query( $s3, $allot, $close, $d['aodid'] );

        if( $db->do_query( $q3 ) )
        {
            return $d['aodid'];
        }
    }
    else
    {
        $remain_allot = 0 - $total;

        $allot = $allot + abs( $remain_allot - 0 );

        $s2 = 'INSERT INTO ticket_allotment_detail_online( daoid, allotment, adate, aclose ) VALUES( %d, %d, %s, %s )';
        $q2 = $db->prepare_query( $s2, $daoid, $allot, $adate, $close );

        if( $db->do_query( $q2 ) )
        {
            return $db->insert_id();
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Allotment
| -------------------------------------------------------------------------------------
*/
function update_allotment( $sid, $aid )
{
    global $db;

    $s = 'UPDATE ticket_allotment SET sid = %d WHERE aid = %d';
    $q = $db->prepare_query( $s, $sid, $aid );
       
    return $db->do_query( $q );
}

/*
| -------------------------------------------------------------------------------------
| Update Allotment Agent
| -------------------------------------------------------------------------------------
*/
function update_allotment_agent( $agid, $allotment, $dagid )
{
    global $db;

    $s = 'UPDATE ticket_allotment_agent SET agid = %d, avallot = %d WHERE dagid = %d';
    $q = $db->prepare_query( $s, $agid, $allotment, $dagid );
               
    if( $db->do_query( $q ) )
    {
        $s2 = 'SELECT
                a.aid,
                a.sid,
                a.acreateddate,
                a.luser_id,
                c.sfrom,
                c.sto,
                d.rname,
                e.agname
               FROM ticket_allotment AS a
               LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
               LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
               LEFT JOIN ticket_route AS d ON c.rid = d.rid
               LEFT JOIN ticket_agent AS e ON b.agid = e.agid
               WHERE b.dagid = %d AND b.agid = %d';
        $q2 = $db->prepare_query( $s2, $dagid, $agid );
        $r2 = $db->do_query( $q2 );

        if( $db->num_rows( $r2 ) > 0 )
        {
            $d2 = $db->fetch_array( $r2 );

            save_log( $d2['aid'], 'allotment', 'Update allotment for agent : ' . $d2['agname'] . '<br/>Route : ' . $d2['rname'] . '<br/>Schedule : ' . date( 'd/m/Y', strtotime( $d2['sfrom'] ) ) . ' until ' . date( 'd/m/Y', strtotime( $d2['sto'] ) ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Allotment Online
| -------------------------------------------------------------------------------------
*/
function update_allotment_online( $allotment, $daoid )
{
    global $db;

    $s = 'UPDATE ticket_allotment_online SET avallot = %d WHERE daoid = %d';
    $q = $db->prepare_query( $s, $allotment, $daoid );

    if( $db->do_query( $q ) )
    {
        $s2 = 'SELECT
                a.aid,
                a.sid,
                a.acreateddate,
                a.luser_id,
                c.sfrom,
                c.sto,
                d.rname
               FROM ticket_allotment AS a
               LEFT JOIN ticket_allotment_online AS b ON b.aid = a.aid
               LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
               LEFT JOIN ticket_route AS d ON c.rid = d.rid
               WHERE b.daoid = %d';
        $q2 = $db->prepare_query( $s2, $daoid );
        $r2 = $db->do_query( $q2 );

        if( $db->num_rows( $r2 ) > 0 )
        {
            $d2 = $db->fetch_array( $r2 );

            save_log( $d2['aid'], 'allotment', 'Update allotment online<br/>Route : ' . $d2['rname'] . '<br/>Schedule : ' . date( 'd/m/Y', strtotime( $d2['sfrom'] ) ) . ' until ' . date( 'd/m/Y', strtotime( $d2['sto'] ) ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Allotment Agent
| -------------------------------------------------------------------------------------
*/
function delete_allotment_agent( $dagid )
{
    global $db;

    $s = 'SELECT
            a.aid,
            a.sid,
            a.acreateddate,
            a.luser_id,
            c.sfrom,
            c.sto,
            d.rname,
            e.agname
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          LEFT JOIN ticket_route AS d ON c.rid = d.rid
          LEFT JOIN ticket_agent AS e ON b.agid = e.agid
          WHERE b.dagid = %d';
    $q = $db->prepare_query( $s, $dagid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $s2 = 'DELETE FROM ticket_allotment_agent WHERE dagid = %d';
        $q2 = $db->prepare_query( $s2, $dagid );
           
        if( $db->do_query( $q2 ) )
        {
            save_log( $d['aid'], 'allotment', 'Delete allotment agent : ' . $d['agname'] . '<br/>Route : ' . $d['rname'] . '<br/>Schedule : ' . date( 'd/m/Y', strtotime( $d['sfrom'] ) ) . ' until ' . date( 'd/m/Y', strtotime( $d['sto'] ) ) );

            return true;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Allotment Online
| -------------------------------------------------------------------------------------
*/
function delete_allotment_online( $daoid )
{
    global $db;

    $s = 'SELECT
            a.aid,
            a.sid,
            a.acreateddate,
            a.luser_id,
            c.sfrom,
            c.sto,
            d.rname
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_online AS b ON b.aid = a.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          LEFT JOIN ticket_route AS d ON c.rid = d.rid
          WHERE b.daoid = %d';
    $q = $db->prepare_query( $s, $daoid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $s2 = 'DELETE FROM ticket_allotment_online WHERE daoid = %d';
        $q2 = $db->prepare_query( $s2, $daoid );
           
        if( $db->do_query( $q2 ) )
        {
            save_log( $d['aid'], 'allotment', 'Delete allotment online<br/>Route : ' . $d['rname'] . '<br/>Schedule : ' . date( 'd/m/Y', strtotime( $d['sfrom'] ) ) . ' until ' . date( 'd/m/Y', strtotime( $d['sto'] ) ) );

            return true;
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Allotment Data By Schedule
| -------------------------------------------------------------------------------------
*/
function save_allotment_by_schedule()
{
    $aid = save_allotment( $_POST['sid'] );

    if( !empty( $aid ) )
    {
        foreach( $_POST['agid'] as $i => $agid )
        {
            save_allotment_agent( $aid, $agid, $_POST['allotment'][$i] );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Edit Allotment Data By Schedule
| -------------------------------------------------------------------------------------
*/
function edit_allotment_by_schedule()
{
    if( isset( $_POST['agid'] ) )
    {
        foreach( $_POST['agid'] as $i => $agid )
        {
            save_allotment_agent( $_POST['aid'], $agid, $_POST['allotment'][$i] );
        }
    }

    if( isset( $_POST['dagid'] ) )
    {
        foreach( $_POST['dagid'] as $id => $agid )
        {
            update_allotment_agent( $agid, $_POST['dallotment'][$id], $id );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Allotment Data By Agent
| -------------------------------------------------------------------------------------
*/
function save_allotment_by_agent()
{
	foreach( $_POST['sid'] as $i => $sid )
	{
        $aid = save_allotment( $sid );

        if( !empty( $aid ) )
        {
            save_allotment_agent( $aid, $_POST['agid'], $_POST['allotment'][$i] );
        }
	}
}

/*
| -------------------------------------------------------------------------------------
| Save Allotment Data By Online
| -------------------------------------------------------------------------------------
*/
function save_allotment_by_online()
{
    foreach( $_POST['sid'] as $i => $sid )
    {
        $aid = save_allotment( $sid );

        if( !empty( $aid ) )
        {
            save_allotment_online( $aid, $_POST['allotment'][$i] );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Edit Allotment Data By Agent
| -------------------------------------------------------------------------------------
*/
function edit_allotment_by_agent()
{
    if( isset( $_POST['sid'] ) )
    {
        save_allotment_by_agent();
    }

    if( isset( $_POST['dsid'] ) )
    {
        foreach( $_POST['dsid'] as $id => $sid )
        {
            if( update_allotment( $sid, $_POST['aid'][$id] ) )
            {
                update_allotment_agent( $_POST['agid'], $_POST['dallotment'][$id], $id );
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Edit Allotment Data By Online
| -------------------------------------------------------------------------------------
*/
function edit_allotment_by_online()
{
    if( isset( $_POST['sid'] ) )
    {
        save_allotment_by_online();
    }

    if( isset( $_POST['dsid'] ) )
    {
        foreach( $_POST['dsid'] as $id => $sid )
        {
            if( update_allotment( $sid, $_POST['aid'][$id] ) )
            {
                update_allotment_online( $_POST['dallotment'][$id], $id );
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Allotment By Schedule Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_allotment_by_schedule_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        0 => 'a.sfrom',
        1 => 'b.rname',
        2 => 'c.boname',
        3 => 'alloted'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.sfrom DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT
                a.sid,
                a.sfrom,
                a.sto,
                b.rname,
                c.boname,
                ( 
                    SELECT COALESCE( SUM(b2.avallot), 0 ) AS total 
                    FROM ticket_allotment AS a2 
                    LEFT JOIN ticket_allotment_agent AS b2 ON b2.aid = a2.aid WHERE a2.sid = a.sid
                ) AS alloted
              FROM ticket_schedule AS a
              LEFT JOIN ticket_route AS b ON a.rid = b.rid
              LEFT JOIN ticket_boat AS c ON a.boid = c.boid ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        unset( $cols[3] );

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT
                a.sid,
                a.sfrom,
                a.sto,
                b.rname,
                c.boname,
                ( 
                    SELECT COALESCE( SUM(b2.avallot), 0 ) AS total 
                    FROM ticket_allotment AS a2 
                    LEFT JOIN ticket_allotment_agent AS b2 ON b2.aid = a2.aid WHERE a2.sid = a.sid
                ) AS alloted
              FROM ticket_schedule AS a
              LEFT JOIN ticket_route AS b ON a.rid = b.rid
              LEFT JOIN ticket_boat AS c ON a.boid = c.boid
              WHERE ' . implode( ' OR ', $search ) . ' ORDER BY ' . $order;;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'sid'       => $d2['sid'],
                'rname'     => $d2['rname'],
                'boname'    => $d2['boname'],
                'alloted'   => $d2['alloted'],
                'sto'       => date('d F Y', strtotime( $d2['sto'] ) ),
                'sfrom'     => date('d F Y', strtotime( $d2['sfrom'] ) ),
                'edit_link' => get_state_url( 'allotment&sub=by-schedule&prc=edit&id=' . $d2['sid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Allotment By Agent Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_allotment_by_agent_table_query()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array(
        0 => 'a.agname',
        1 => 'alloted'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.agid DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT 
                a.agid, 
                a.agname,
                ( 
                    SELECT COALESCE( ROUND( AVG(a2.avallot) ), 0 ) as avallot 
                    FROM ticket_allotment_agent as a2 
                    WHERE a2.agid = a.agid 
                ) AS alloted 
              FROM ticket_agent AS a 
              WHERE a.agparent = 0 ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        unset( $cols[1] );

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $s = 'SELECT 
                a.agid, 
                a.agname,
                ( 
                    SELECT COALESCE( ROUND( AVG(a2.avallot) ), 0 ) as avallot 
                    FROM ticket_allotment_agent as a2 
                    WHERE a2.agid = a.agid 
                ) AS alloted 
              FROM ticket_agent AS a 
              WHERE a.agparent = 0 AND ( ' . implode( ' OR ', $search ) . ' ) ORDER BY ' . $order;
        $r = $db->do_query( $s );
        $n = $db->num_rows( $r );

        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $surl = site_url();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'agid'      => $d2['agid'],
                'agname'    => $d2['agname'],
                'alloted'   => $d2['alloted'],
                'edit_link' => get_state_url( 'allotment&sub=by-agent&prc=edit&id=' . $d2['agid'] ),
                'view_link' => get_state_url( 'allotment&sub=by-agent&prc=detail&id=' . $d2['agid'] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Get Allotment Schedule By ID
| -------------------------------------------------------------------------------------
*/
function get_allotment_by_schedule( $id='' )
{
    global $db;

    $data = array();

    $s = 'SELECT
            a.sid,
            a.rid,
            a.boid,
            a.sfrom,
            a.sto,
            a.sstatus,
            a.screateddate,
            a.luser_id,
            b.rname,
            c.boname,
            c.bopassenger,
            d.aid
          FROM ticket_schedule AS a
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          LEFT JOIN ticket_boat AS c ON a.boid = c.boid 
          LEFT JOIN ticket_allotment AS d ON d.sid = a.sid 
          WHERE a.sid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'sid' => $d['sid'],
                'rid' => $d['rid'],
                'aid' => $d['aid'],
                'rname' => $d['rname'],
                'boid' => $d['boid'],
                'boname' => $d['boname'],
                'bopassenger' => $d['bopassenger'],
                'sfrom' => $d['sfrom'],
                'sto' => $d['sto'],
                'sstatus' => $d['sstatus'],
                'allotment' => get_allotment_detail_by_schedule( $d['sid'] ),
                'new_allotment' => get_new_allotment_item_by_schedule()
            );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Allotment Agent By ID
| -------------------------------------------------------------------------------------
*/
function get_allotment_by_agent( $agid )
{
    global $db;

    $data = array();

    $s = 'SELECT a.agid, a.agname, b.chid, b.chname FROM ticket_agent AS a
		  LEFT JOIN ticket_channel AS b ON a.chid = b.chid WHERE a.agid = %d';
    $q = $db->prepare_query( $s, $agid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'agid' => $d['agid'],
                'chid' => $d['chid'],
                'agname' => $d['agname'],
                'chname' => $d['chname'],
                'allotment' => get_allotment_detail_by_agent( $d['agid'] ),
                'new_allotment' => get_new_allotment_item_by_agent()
            );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Allotment Online
| -------------------------------------------------------------------------------------
*/
function get_allotment_by_online()
{
    $data = array(
        'allotment' => get_allotment_detail_by_online(),
        'new_allotment' => get_new_allotment_item_by_online()
    );

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Total Allotment By Schedule
| -------------------------------------------------------------------------------------
*/
function get_total_allotment_by_schedule( $sid )
{
    global $db;

    $s = 'SELECT SUM(b.avallot) AS total FROM ticket_allotment AS a LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid WHERE a.sid = %d';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return empty( $d['total'] ) ? 0 : $d['total'];
}

/*
| -------------------------------------------------------------------------------------
| Get Total Allotment By Agent
| -------------------------------------------------------------------------------------
*/
function get_total_allotment_by_agent( $agid )
{
    global $db;

    $s = 'SELECT ROUND(AVG(avallot)) as avallot FROM ticket_allotment_agent as a WHERE a.agid = %d';
    $q = $db->prepare_query( $s, $agid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return empty( $d['avallot'] ) ? 0 : $d['avallot'];
}

/*
| -------------------------------------------------------------------------------------
| Get Allotment By Schedule
| -------------------------------------------------------------------------------------
*/
function get_allotment_detail_by_schedule( $sid )
{
    global $db;

    $data = array();

   	$s = 'SELECT * FROM ticket_allotment AS a RIGHT JOIN ticket_allotment_agent AS b ON b.aid = a.aid WHERE a.sid = %d';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
    	extract( $_POST );

        while( $d = $db->fetch_array( $r ) )
        {
        	$id     = isset( $dagid[ $d['dagid'] ] ) ? $dagid[ $d['dagid'] ] : $d['agid'];
        	$allot  = isset( $dallotment[ $d['dagid'] ] ) ? $dallotment[ $d['dagid'] ] : $d['avallot'];
        	$data[] = array( 'dagid' => $d['dagid'], 'aid' => $d['aid'], 'agid' => $id, 'allotment' => $allot );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get New Allotment From Post Data - Schedule
| -------------------------------------------------------------------------------------
*/
function get_new_allotment_item_by_schedule()
{
    $data = array();

	if( isset( $_POST['agid'] ) )
    {
    	foreach( $_POST['agid'] as $i => $agid )
    	{
    		$data[] = array( 'agid' => $agid, 'allotment' => $_POST['allotment'][$i] );
    	}
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Allotment By Agent
| -------------------------------------------------------------------------------------
*/
function get_allotment_detail_by_agent( $agid, $sid = '' )
{
    global $db;

    $data = array();

    if( empty( $sid ) )
   	{
        $s = 'SELECT
    			a.aid,
    			a.sid,
    			b.dagid,
    			b.avallot,
    			d.boname,
    			d.bopassenger
       		  FROM ticket_allotment AS a 
       		  RIGHT JOIN ticket_allotment_agent AS b ON b.aid = a.aid 
       		  LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
    		  LEFT JOIN ticket_boat AS d ON c.boid = d.boid 
    		  WHERE b.agid = %d ORDER BY c.sfrom';
        $q = $db->prepare_query( $s, $agid );
        $r = $db->do_query( $q );
    }
    else
    {
        $s = 'SELECT
                a.aid,
                a.sid,
                b.dagid,
                b.avallot,
                d.boname,
                d.bopassenger
              FROM ticket_allotment AS a 
              RIGHT JOIN ticket_allotment_agent AS b ON b.aid = a.aid 
              LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
              LEFT JOIN ticket_boat AS d ON c.boid = d.boid 
              WHERE b.agid = %d AND a.sid = %d ORDER BY c.sfrom';
        $q = $db->prepare_query( $s, $agid, $sid );
        $r = $db->do_query( $q );
    }

    if( $db->num_rows( $r ) > 0 )
    {
    	extract( $_POST );

        while( $d = $db->fetch_array( $r ) )
        {
        	$id     = isset( $dsid[ $d['dagid'] ] ) ? $dsid[ $d['dagid'] ] : $d['sid'];
        	$allot  = isset( $dallotment[ $d['dagid'] ] ) ? $dallotment[ $d['dagid'] ] : $d['avallot'];
        	$data[] = array( 'dagid' => $d['dagid'], 'aid' => $d['aid'], 'sid' => $id, 'allotment' => $allot, 'boname' => $d['boname'], 'bopassenger' => $d['bopassenger'] );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get New Allotment From Post Data - Agent
| -------------------------------------------------------------------------------------
*/
function get_new_allotment_item_by_agent()
{
    $data = array();

	if( isset( $_POST['sid'] ) )
    {
    	foreach( $_POST['sid'] as $i => $sid )
    	{
    		$data[] = array( 'sid' => $sid, 'allotment' => $_POST['allotment'][$i], 'boname' => $_POST['boname'][$i], 'bopassenger' => $_POST['bopassenger'][$i] );
    	}
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Allotment By Online
| -------------------------------------------------------------------------------------
*/
function get_allotment_detail_by_online( $sid = '' )
{
    global $db;

    $data = array();

    if( empty( $sid ) )
    {
        $s = 'SELECT
                a.aid,
                a.sid,
                b.daoid,
                b.avallot,
                d.boname,
                d.bopassenger
              FROM ticket_allotment AS a 
              RIGHT JOIN ticket_allotment_online AS b ON b.aid = a.aid 
              LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
              LEFT JOIN ticket_boat AS d ON c.boid = d.boid 
              ORDER BY c.sfrom';
        $q = $db->prepare_query( $s );
        $r = $db->do_query( $q );
    }
    else
    {
        $s = 'SELECT
                a.aid,
                a.sid,
                b.daoid,
                b.avallot,
                d.boname,
                d.bopassenger
              FROM ticket_allotment AS a 
              RIGHT JOIN ticket_allotment_online AS b ON b.aid = a.aid 
              LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
              LEFT JOIN ticket_boat AS d ON c.boid = d.boid 
              WHERE a.sid = %d ORDER BY c.sfrom';
        $q = $db->prepare_query( $s, $sid );
        $r = $db->do_query( $q );
    }

    if( $db->num_rows( $r ) > 0 )
    {
        extract( $_POST );

        while( $d = $db->fetch_array( $r ) )
        {
            $id     = isset( $dsid[ $d['daoid'] ] ) ? $dsid[ $d['daoid'] ] : $d['sid'];
            $allot  = isset( $dallotment[ $d['daoid'] ] ) ? $dallotment[ $d['daoid'] ] : $d['avallot'];
            $data[] = array( 'daoid' => $d['daoid'], 'aid' => $d['aid'], 'sid' => $id, 'allotment' => $allot, 'boname' => $d['boname'], 'bopassenger' => $d['bopassenger'] );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get New Allotment From Post Data - Online
| -------------------------------------------------------------------------------------
*/
function get_new_allotment_item_by_online()
{
    $data = array();

    if( isset( $_POST['sid'] ) )
    {
        foreach( $_POST['sid'] as $i => $sid )
        {
            $data[] = array( 'sid' => $sid, 'allotment' => $_POST['allotment'][$i], 'boname' => $_POST['boname'][$i], 'bopassenger' => $_POST['bopassenger'][$i] );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get List Date From Given Date Period
| -------------------------------------------------------------------------------------
*/
function get_list_date_period( $start, $end )
{
	$start = new DateTime( date( 'Y-m-d', strtotime( $start ) ) );
	$end   = new DateTime( date( 'Y-m-d', strtotime( $end ) ) );
	$range = new DatePeriod( $start, new DateInterval('P1D'), $end );
	$date  = array();

	if( !empty( $range ) )
	{
		foreach( $range as $d )
		{
		    $date[] = $d->format( 'Y-m-d' );
		}

		$date[] = $end->format( 'Y-m-d' );
	}

	return $date;
}

/*
| -------------------------------------------------------------------------------------
| Get Allotment Item
| -------------------------------------------------------------------------------------
*/
function get_allotment_item_by_schedule( $sid, $items = array(), $new_items = array() )
{
    global $db;

    $i = time();

    if( empty( $items ) && empty( $new_items ) )
    {
    	return '
        <div class="row">
            <div class="col-md-3">
                <select class="select-option" name="agid[' . $i . ']">
                    ' . get_agent_option() . '
                </select>
            </div>
            <div class="col-md-1">
                <input type="text" class="text form-control" name="allotment[' . $i . ']" value="0">
                <a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel=""></a>
            </div>
        </div>';
    }
    else
    {
    	$content = '';

    	if( !empty( $items ) )
    	{
		    foreach( $items as $d )
		    {
		    	$content .= '
	            <div class="row">
	                <div class="col-md-3">
	                    <select class="select-option" name="dagid[' . $d['dagid'] . ']">
	                        ' . get_agent_option( $d['agid'] ) . '
	                    </select>
	                </div>
	                <div class="col-md-1">
	                    <input type="text" class="text form-control" name="dallotment[' . $d['dagid'] . ']" value="' . $d['allotment'] . '">
	                    <a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel="' . $d['dagid'] . '"></a>
	                </div>
	            </div>';
		    }
		}

    	if( !empty( $new_items ) )
    	{
		    foreach( $new_items as $d )
		    {
		    	$content .= '
	            <div class="row">
	                <div class="col-md-3">
	                    <select class="select-option" name="agid[' . $i . ']">
	                        ' . get_agent_option( $d['agid'] ) . '
	                    </select>
	                </div>
	                <div class="col-md-1">
	                    <input type="text" class="text form-control" name="allotment[' . $i . ']" value="' . $d['allotment'] . '">
	                    <a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel=""></a>
	                </div>
	            </div>';
		    }
		}

	    return $content;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Allotment Item By Agent
| -------------------------------------------------------------------------------------
*/
function get_allotment_item_by_agent( $agid, $items = array(), $new_items = array() )
{
    global $db;

    $i = time();

    if( empty( $items ) && empty( $new_items ) )
    {
    	return '
        <div class="row">
            <div class="col-md-6">
	            <select class="select-option" name="sid[' . $i . ']">
	                ' . get_schedule_allotment_option() . '
	            </select>
            </div>
            <div class="col-md-2 col-boat-name">
                <label>-</label>
                <input type="text" class="sr-only" name="boname[' . $i . ']" />
            </div>
            <div class="col-md-2 col-boat-passenger">
                <label>-</label>
                <input type="text" class="sr-only" name="bopassenger[' . $i . ']" value="0" />
            </div>
            <div class="col-md-2">
		        <input type="text" class="sr-only" name="aid[' . $i . ']" />
            	<input type="text" class="text form-control" name="allotment[' . $i . ']" value="0">
            	<a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel=""></a>
            </div>
	    </div>';
    }
    else
    {
    	$content = '';

    	if( !empty( $items ) )
    	{
		    foreach( $items as $d )
		    {
		    	$content .= '
	            <div class="row">
	                <div class="col-md-6">
			            <select class="select-option" name="dsid[' . $d['dagid'] . ']">
			                ' . get_schedule_allotment_option( $d['sid'] ) . '
			            </select>
	                </div>
		            <div class="col-md-2 col-boat-name">
		                <label>' . $d['boname'] . '</label>
		                <input type="text" class="sr-only" name="boname[' . $d['dagid'] . ']" value="' . $d['boname'] . '" />
		            </div>
		            <div class="col-md-2 col-boat-passenger">
		                <label>' . $d['bopassenger'] . '</label>
		                <input type="text" class="sr-only" name="bopassenger[' . $d['dagid'] . ']" value="' . $d['bopassenger'] . '" />
		            </div>
	                <div class="col-md-2">
		                <input type="text" class="sr-only" name="aid[' . $d['dagid'] . ']" value="' . $d['aid'] . '" />
		            	<input type="text" class="text form-control" name="dallotment[' . $d['dagid'] . ']" value="' . $d['allotment'] . '">
		            	<a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel="' . $d['dagid'] . '"></a>
	                </div>
	            </div>';
		    }
		}

    	if( !empty( $new_items ) )
    	{
		    foreach( $new_items as $d )
		    {
                $boname = empty( $d['boname'] ) ? '-' : $d['boname'];
                $bopass = empty( $d['bopassenger'] ) ? 0 : $d['bopassenger'];

		    	$content .= '
	            <div class="row">
	                <div class="col-md-6">
			            <select class="select-option" name="sid[' . $i . ']">
			                ' . get_schedule_allotment_option( $d['sid'] ) . '
			            </select>
	                </div>
		            <div class="col-md-2 col-boat-name">
		                <label>' . $boname . '</label>
		                <input type="text" class="sr-only" name="boname[' . $i . ']" value="' . $boname . '" />
		            </div>
		            <div class="col-md-2 col-boat-passenger">
		                <label>' . $bopass . '</label>
		                <input type="text" class="sr-only" name="bopassenger[' . $i . ']" value="' . $bopass . '" />
		            </div>
	                <div class="col-md-2">
		        		<input type="text" class="sr-only" name="aid[' . $i . ']" />
		            	<input type="text" class="text form-control" name="allotment[' . $i . ']" value="' . $d['allotment'] . '">
		            	<a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel=""></a>
	                </div>
	            </div>';
		    }
		}

	    return $content;
    }
}

function get_allotment_item_by_online( $items = array(), $new_items = array() )
{
    global $db;

    $i = time();

    if( empty( $items ) && empty( $new_items ) )
    {
        return '
        <div class="row">
            <div class="col-md-6">
                <select class="select-option" name="sid[' . $i . ']">
                    ' . get_schedule_allotment_option() . '
                </select>
            </div>
            <div class="col-md-2 col-boat-name">
                <label>-</label>
                <input type="text" class="sr-only" name="boname[' . $i . ']" />
            </div>
            <div class="col-md-2 col-boat-passenger">
                <label>-</label>
                <input type="text" class="sr-only" name="bopassenger[' . $i . ']" value="0" />
            </div>
            <div class="col-md-2">
                <input type="text" class="sr-only" name="aid[' . $i . ']" />
                <input type="text" class="text form-control" name="allotment[' . $i . ']" value="0">
                <a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel=""></a>
            </div>
        </div>';
    }
    else
    {
        $content = '';

        if( !empty( $items ) )
        {
            foreach( $items as $d )
            {
                $content .= '
                <div class="row">
                    <div class="col-md-6">
                        <select class="select-option" name="dsid[' . $d['daoid'] . ']">
                            ' . get_schedule_allotment_option( $d['sid'] ) . '
                        </select>
                    </div>
                    <div class="col-md-2 col-boat-name">
                        <label>' . $d['boname'] . '</label>
                        <input type="text" class="sr-only" name="boname[' . $d['daoid'] . ']" value="' . $d['boname'] . '" />
                    </div>
                    <div class="col-md-2 col-boat-passenger">
                        <label>' . $d['bopassenger'] . '</label>
                        <input type="text" class="sr-only" name="bopassenger[' . $d['daoid'] . ']" value="' . $d['bopassenger'] . '" />
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="sr-only" name="aid[' . $d['daoid'] . ']" value="' . $d['aid'] . '" />
                        <input type="text" class="text form-control" name="dallotment[' . $d['daoid'] . ']" value="' . $d['allotment'] . '">
                        <a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel="' . $d['daoid'] . '"></a>
                    </div>
                </div>';
            }
        }

        if( !empty( $new_items ) )
        {
            foreach( $new_items as $d )
            {
                $boname = empty( $d['boname'] ) ? '-' : $d['boname'];
                $bopass = empty( $d['bopassenger'] ) ? 0 : $d['bopassenger'];

                $content .= '
                <div class="row">
                    <div class="col-md-6">
                        <select class="select-option" name="sid[' . $i . ']">
                            ' . get_schedule_allotment_option( $d['sid'] ) . '
                        </select>
                    </div>
                    <div class="col-md-2 col-boat-name">
                        <label>' . $boname . '</label>
                        <input type="text" class="sr-only" name="boname[' . $i . ']" value="' . $boname . '" />
                    </div>
                    <div class="col-md-2 col-boat-passenger">
                        <label>' . $bopass . '</label>
                        <input type="text" class="sr-only" name="bopassenger[' . $i . ']" value="' . $bopass . '" />
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="sr-only" name="aid[' . $i . ']" />
                        <input type="text" class="text form-control" name="allotment[' . $i . ']" value="' . $d['allotment'] . '">
                        <a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel=""></a>
                    </div>
                </div>';
            }
        }

        return $content;
    }
}

/*
| -------------------------------------------------------------------------------------
| Allotment Table By Agent
| -------------------------------------------------------------------------------------
*/
function get_allotment_table_by_agent( $agid, $items = array() )
{
    if( !empty( $items ) )
    {
        $content = '
        <table class="table">
            <thead>
                <tr>
                    <th>Schedule/Route</th>
                    <th>Boat</th>
                    <th>Passenger</th>
                    <th>Daily Allot.</th>
                </tr>
            </thead>
            <tbody>';

            foreach( $items as $d )
            {
                $content .= '
                <tr>
                    <td>' . get_schedule_allotment_text( $d['sid'] ) . '</td>
                    <td>' . $d['boname'] . '</td>
                    <td>' . $d['bopassenger'] . '</td>
                    <td>' . $d['allotment'] . '</td>
                </tr>';
            }

            $content .= '
            </tbody>
        </table>';
    }
    else
    {
        $content = '
        <table class="table">
            <thead>
                <tr>
                    <th>Schedule/Route</th>
                    <th>Boat</th>
                    <th>Passenger</th>
                    <th>Daily Allot.</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="4">No allotment was found</td>
                </tr>
            </tbody>
        </table>';
    }

    return $content;
}

/*
| -------------------------------------------------------------------------------------
| Add New Allotment Item
| -------------------------------------------------------------------------------------
*/
function add_new_agent_allotment( $type = 0 )
{
    $i = time();

	if( empty( $type ) )
    {
    	return '
	    <div class="row">
	        <div class="col-md-3">
	            <select class="select-option" name="agid[' . $i . ']">
	                ' . get_agent_option() . '
	            </select>
	        </div>
	        <div class="col-md-1">
	            <input type="text" class="text form-control" name="allotment[' . $i . ']" value="0">
	            <a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel=""></a>
	        </div>
	    </div>';
	}
	else
	{
    	return '
        <div class="row">
            <div class="col-md-6">
	            <select class="select-option" name="sid[' . $i . ']">
	                ' . get_schedule_allotment_option() . '
	            </select>
            </div>
            <div class="col-md-2 col-boat-name">
                <label>-</label>
                <input type="text" class="sr-only" name="boname[' . $i . ']" />
            </div>
            <div class="col-md-2 col-boat-passenger">
                <label>-</label>
                <input type="text" class="sr-only" name="bopassenger[' . $i . ']" value="0" />
            </div>
            <div class="col-md-2">
		        <input type="text" class="sr-only" name="aid[' . $i . ']" />
            	<input type="text" class="text form-control" name="allotment[' . $i . ']" value="0">
            	<a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel=""></a>
            </div>
        </div>';
	}
}

/*
| -------------------------------------------------------------------------------------
| Add New Allotment Item - Online
| -------------------------------------------------------------------------------------
*/
function add_new_online_allotment()
{
    $i = time();

    return '
    <div class="row">
        <div class="col-md-6">
            <select class="select-option" name="sid[' . $i . ']">
                ' . get_schedule_allotment_option() . '
            </select>
        </div>
        <div class="col-md-2 col-boat-name">
            <label>-</label>
            <input type="text" class="sr-only" name="boname[' . $i . ']" />
        </div>
        <div class="col-md-2 col-boat-passenger">
            <label>-</label>
            <input type="text" class="sr-only" name="bopassenger[' . $i . ']" value="0" />
        </div>
        <div class="col-md-2">
            <input type="text" class="sr-only" name="aid[' . $i . ']" />
            <input type="text" class="text form-control" name="allotment[' . $i . ']" value="0">
            <a class="delete-agent" href="' . HTSERVER . site_url() . '/ticket-allotment-ajax/" rel=""></a>
        </div>
    </div>';
}

/*
| -------------------------------------------------------------------------------------
| Schedule Allotment Option
| -------------------------------------------------------------------------------------
*/
function get_schedule_allotment_option( $sid = '' )
{
	global $db;

	$s = 'SELECT 
			a.sid, 
			b.rname, 
			a.sfrom, 
			a.sto, 
			c.boname, 
			c.bopassenger 
		  FROM ticket_schedule AS a 
		  INNER JOIN ticket_route AS b ON a.rid = b.rid
		  LEFT JOIN ticket_boat AS c ON a.boid = c.boid';
	$q = $db->prepare_query( $s );
	$r = $db->do_query( $q );

	$option = '<option value="">Select Schedule</option>';

	while( $d = $db->fetch_array( $r ) )
	{
		$option .= '
		<option value="' . $d['sid'] . '" ' . ( $d['sid'] == $sid ? 'selected' : '' ) . ' data-name="' . $d['boname'] . '" data-passenger = "' . $d['bopassenger'] . '" >
			' . date( 'd M y', strtotime( $d['sfrom'] ) ) . ' - ' . date( 'd M y', strtotime( $d['sto'] ) ) . ' / ' . $d['rname'] .'
		</option>';
	}

	return $option;
}

/*
| -------------------------------------------------------------------------------------
| Schedule Allotment Text
| -------------------------------------------------------------------------------------
*/
function get_schedule_allotment_text( $sid = '' )
{
    global $db;

    $s = 'SELECT 
            b.rname, 
            a.sfrom, 
            a.sto
          FROM ticket_schedule AS a 
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          WHERE a.sid = %d';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return date( 'd M y', strtotime( $d['sfrom'] ) ) . ' - ' . date( 'd M y', strtotime( $d['sto'] ) ) . ' / ' . $d['rname'];
}

/*
| -------------------------------------------------------------------------------------
| Get Agent Allotment Calendar Data
| -------------------------------------------------------------------------------------
*/
function get_agent_allotment( $month, $year, $sid = '', $agid = '' )
{
    global $db;

    $list = array();

    $s = 'SELECT
            a.aid,
            a.sid,
            b.agid,
            b.dagid,
            b.avallot,
            c.sfrom,
            c.sto,
            c.rid,
            d.rtype
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          LEFT JOIN ticket_route AS d ON c.rid = d.rid
          WHERE a.sid = %d AND b.agid = %d';
    $q = $db->prepare_query( $s, $sid, $agid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        $d = $db->fetch_array( $r );

        $fdate  = $year . '-' . $month . '-01';
        $next   = strtotime( '+1 month', strtotime( $fdate ) );
        $nmonth = date( 'n', $next );
        $nyear  = date( 'Y', $next );
        $ldate  = $nyear . '-' . $nmonth . '-01';

        $period = get_interval_date( $d['sfrom'], $d['sto'] );
        $dlist  = get_interval_date( $fdate, $ldate );

        foreach( $dlist as $day )
        {
            $time  = strtotime( $day );
            $max   = get_total_seat_capacity( $d['rid'], $time );
            $stat  = get_agent_allotment_status_by_date( $d['dagid'], $time );
            $gseat = get_global_allotment_by_date_and_schedule( $d, $max, $time );
            $key   = date( 'j', $time ) . '-' . date( 'n', $time ) . '-' . date( 'Y', $time );
            $allot = get_seat_availibility_agent_num_by_schedule( $max, $d['rid'], $sid, $time, $agid );

            if( in_array( date( 'm', $time ), array( $month, $nmonth ) ) == $month && in_array( $day, $period ) )
            {
                $list[$key] = $d['dagid'] . '|' . $allot . '/' . $d['avallot'] . '|' . $d['sid'] . '|' . $stat . '|' . $gseat;
            }
            else
            {
                $list[$key] = $d['dagid'] . '|0/0|0|' . $stat . '|' . $gseat;
            }
        }
    }

    return $list;
}

/*
| -------------------------------------------------------------------------------------
| Get Online Allotment Calendar Data
| -------------------------------------------------------------------------------------
*/
function get_online_allotment( $month, $year, $sid = '' )
{
    global $db;

    $list = array();

    $s = 'SELECT
            a.aid,
            a.sid,
            b.daoid,
            b.avallot,
            c.sfrom,
            c.sto,
            c.rid,
            d.rtype
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_online AS b ON b.aid = a.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          LEFT JOIN ticket_route AS d ON c.rid = d.rid
          WHERE a.sid = %d';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        $d = $db->fetch_array( $r );

        $fdate  = $year . '-' . $month . '-01';
        $next   = strtotime( '+1 month', strtotime( $fdate ) );
        $nmonth = date( 'n', $next );
        $nyear  = date( 'Y', $next );
        $ldate  = $nyear . '-' . $nmonth . '-01';

        $period = get_interval_date( $d['sfrom'], $d['sto'] );
        $dlist  = get_interval_date( $fdate, $ldate );

        foreach( $dlist as $day )
        {
            $time  = strtotime( $day );
            $max   = get_total_seat_capacity( $d['rid'], $time );
            $stat  = get_online_allotment_status_by_date( $d['daoid'], $time );
            $gseat = get_global_allotment_by_date_and_schedule( $d, $max, $time );
            $key   = date( 'j', $time ) . '-' . date( 'n', $time ) . '-' . date( 'Y', $time );
            $allot = get_seat_availibility_online_num_by_schedule( $max, $d['rid'], $sid, $time );
            $avlot = empty( $d['avallot'] ) ? 0 : $d['avallot'];

            if( in_array( date( 'm', $time ), array( $month, $nmonth ) ) == $month && in_array( $day, $period ) )
            {
                $list[$key] = $d['daoid'] . '|' . $allot . '/' . $avlot . '|' . $d['sid'] . '|' . $stat . '|' . $gseat;
            }
            else
            {
                $list[$key] = $d['daoid'] . '|0/0|0|' . $stat . '|' . $gseat;
            }
        }
    }

    return $list;
}

function get_global_allotment_by_date_and_schedule( $data, $max, $time )
{
    if( $data['rtype'] == '1' && !empty( $data['rhoppingpoint'] ) )
    {
        $allot = get_seat_availibility_num_by_hopping_trip( $max, $data['rid'], $time, true );
    }
    else
    {
        $allot = get_seat_availibility_num_by_schedule( $max, $data['rid'], $data['sid'], $time );
    }

    if( is_array( $allot ) )
    {
        $temp = array();

        foreach( $allot as $a )
        {
            $temp[] = $a['seat_max'];
        }

        $allot = implode( ',', $temp );
    }

    return $allot;
}

function get_total_all_agent_daily_allotment( $sid, $time )
{
    global $db;

    $data = array();

    $s = 'SELECT
            c.rid,
            b.agid
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          WHERE a.sid = %d AND b.avallot IS NOT NULL';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $max   = get_total_seat_capacity( $d['rid'], $time );
            $allot = get_seat_availibility_agent_num_by_schedule( $max, $d['rid'], $sid, $time, $d['agid'] );

            $data[ $time ][ $d['rid'] ][ $d['agid'] ] = array( 'allot' => $allot );
        }
    }

    return $data;
}

function get_all_agent_allotment( $month, $year, $sid = '' )
{
    global $db;

    $temp = array();
    $list = array();

    $s = 'SELECT
            a.aid,
            a.sid,
            b.agid,
            b.dagid,
            b.avallot,
            c.rid,
            c.sfrom,
            c.sto
          FROM ticket_allotment AS a
          LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
          LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
          WHERE a.sid = %d AND b.avallot IS NOT NULL';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );
    
    if( $db->num_rows( $r ) )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $fdate  = $year . '-' . $month . '-01';
            $next   = strtotime( '+1 month', strtotime( $fdate ) );
            $nmonth = date( 'n', $next );
            $nyear  = date( 'Y', $next );
            $ldate  = $nyear . '-' . $nmonth . '-01';

            $period = get_interval_date( $d['sfrom'], $d['sto'] );
            $dlist  = get_interval_date( $fdate, $ldate );

            foreach( $dlist as $day )
            {
                $time  = strtotime( $day );
                $max   = get_total_seat_capacity( $d['rid'], $time );
                $key   = date( 'j', $time ) . '-' . date( 'n', $time ) . '-' . date( 'Y', $time );
                $allot = get_seat_availibility_agent_num_by_schedule( $max, $d['rid'], $sid, $time, $d['agid'] );

                if( in_array( date( 'm', $time ), array( $month, $nmonth ) ) == $month && in_array( $day, $period ) )
                {
                    $temp[$key][] = array( 'rallot' => $allot, 'dallot' => $d['avallot'] );
                }
                else
                {
                    $temp[$key][] = array( 'rallot' => 0, 'dallot' => 0 );
                }
            }
        }

        foreach( $temp as $date => $obj )
        {
            $rallot = 0;
            $dallot = 0;

            foreach( $obj as $d )
            {
                $rallot += $d['rallot'];
                $dallot += $d['dallot'];
            }
            
            $list[$date] = $rallot . '/' . $dallot . '|' . $sid;
        }
    }

    return $list;
}

function get_agent_allotment_detail_by_date( $dagid, $time )
{
    global $db;

    $s = 'SELECT a.allotment, a.aclose FROM ticket_allotment_detail AS a WHERE a.dagid = %d AND a.adate = %s';
    $q = $db->prepare_query( $s, $dagid, date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        return array( 'result' => 1, 'allotment' => $d['allotment'], 'aclose' => $d['aclose'] );
    }
    else
    {
        return array( 'result' => 0, 'allotment' => 0, 'aclose' => 0 );
    }
}

function get_agent_allotment_status_by_date( $dagid, $time )
{
    global $db;

    $s = 'SELECT a.aclose FROM ticket_allotment_detail AS a WHERE a.dagid = %d AND a.adate = %s';
    $q = $db->prepare_query( $s, $dagid, date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        return $d['aclose'];
    }
    else
    {
        return 0;
    }
}

function get_online_allotment_status_by_date( $daoid, $time )
{
    global $db;

    $s = 'SELECT a.aclose FROM ticket_allotment_detail_online AS a WHERE a.daoid = %d AND a.adate = %s';
    $q = $db->prepare_query( $s, $daoid, date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        return $d['aclose'];
    }
    else
    {
        return 0;
    }
}

function get_remaining_agent_allotment_by_schedule( $sid = '', $agid = '', $time = '', $allot = 0 )
{
    global $db;

    $remaining = 0;

    $s = 'SELECT SUM( b.num_adult + b.num_child + b.num_infant ) as num_pass
          FROM ticket_booking AS a
          LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
          WHERE b.bdpstatus NOT IN ("cn", "bc") AND a.bstt <> %s  AND b.sid = %d AND a.agid = %d AND b.bddate = %s';
    $q = $db->prepare_query( $s, 'ar', $sid, $agid, date( 'Y-m-d', $time ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) )
    {
        $d = $db->fetch_array( $r );
        
        $remaining = $allot - $d['num_pass'];        
        $remaining = $remaining < 0 ? 0 : $remaining;
    }

    return $remaining;
}

function get_interval_date( $from, $to )
{
    $from = date( 'Y-m-d', strtotime( $from ) );
    $to   = date( 'Y-m-t', strtotime( $to ) );

    $period = new DatePeriod(
         new DateTime( $from ),
         new DateInterval('P1D'),
         new DateTime( $to )
    );

    $data = array();

    foreach( $period as $date)
    { 
        $data[] = $date->format( 'Y-m-d' ); 
    }

    $data[] = $to;

    return $data;
}

function get_agent_allotment_calendar_detail( $agid = '', $is_all = false )
{
    global $db;

    if( $is_all )
    {
        $s = 'SELECT
                a.sid,
                b.rname,
                a.sfrom,
                a.sto,
                c.boid,
                c.boname,
                c.bopassenger
              FROM ticket_schedule AS a
              LEFT JOIN ticket_route AS b ON a.rid = b.rid
              LEFT JOIN ticket_boat AS c ON a.boid = c.boid
              WHERE a.sstatus = %s AND a.sto >= %s ORDER BY a.sfrom, a.rid';
        $q = $db->prepare_query( $s, 'publish', date( 'Y-m-d' ) );
        $r = $db->do_query( $q );
    }
    else
    {
        $s = 'SELECT
                a.aid,
                a.sid,
                b.agid,
                d.rname,
                b.avallot,
                c.sfrom,
                c.sto,
                c.boid,
                e.boname,
                e.bopassenger
              FROM ticket_allotment AS a
              LEFT JOIN ticket_allotment_agent AS b ON b.aid = a.aid
              LEFT JOIN ticket_schedule AS c ON a.sid = c.sid
              LEFT JOIN ticket_route AS d ON c.rid = d.rid
              LEFT JOIN ticket_boat AS e ON c.boid = e.boid
              WHERE c.sstatus = %s AND b.agid = %d AND c.sto >= %s ORDER BY c.sfrom, c.rid';
        $q = $db->prepare_query( $s, 'publish', $agid, date( 'Y-m-d' ) );
        $r = $db->do_query( $q );
    }

    if( $db->num_rows( $r ) > 0 )
    {
        $content = '
        <div class="row">
            <div class="col-md-12">
                <div class="calendar-detail">
                    <h3>Allotment Calendar</h3>                    
                    <p>
                        <strong>Current Schedule:</strong>
                        <select name="agent_allotment_trip_boat" style="width:200px;">
                            ' . get_boat_option() . '
                        </select>
                        <select name="agent_allotment_schedule" style="width:500px;">';
                            $idx         = 0;
                            $sid         = '';
                            $sfrom       = '';
                            $sto         = '';
                            $boname      = '';
                            $bopassenger = '';

                            while( $d = $db->fetch_array( $r ) )
                            {
                                if( $idx == 0 )
                                {
                                    $sid         = $d['sid'];
                                    $sfrom       = $d['sfrom'];
                                    $sto         = $d['sto'];
                                    $boname      = $d['boname'];
                                    $bopassenger = $d['bopassenger'];
                                }

                                $content .= '
                                <option value="' . $d['sid'] . '" data-boid="' . $d['boid'] . '" data-name="' . $d['boname'] . '" data-passenger = "' . $d['bopassenger'] . '" data-from="' . $d['sfrom'] . '" data-to="' . $d['sto'] . '">
                                    ' . date( 'd M y', strtotime( $d['sfrom'] ) ) . ' - ' . date( 'd M y', strtotime( $d['sto'] ) ) . ' / ' . $d['rname'] .'
                                </option>';

                                $idx++;
                            }

                            $content .= '
                        </select>
                        <strong>Boat:</strong>
                        <span class="boat-passenger">' . $bopassenger . '</span>
                        <span>, <i>NB. : available seats / capacity</i></span>
                        <input name="sid" class="sr-only" value="' . $sid . '" />
                        <input name="sto" class="sr-only" value="' . $sto . '" />
                        <input name="agid" class="sr-only" value="' . $agid . '" />
                        <input name="sfrom" class="sr-only" value="' . $sfrom . '" />
                        <input name="bopassenger" class="sr-only" value="' . $bopassenger . '" />
                    </p>
                </div>

                <div id="availability-calendar" class="availability-calendar"></div>
            </div>
        </div>';

        return $content;
    }
}

function get_online_allotment_calendar_detail()
{
    global $db;

    $s = 'SELECT
            a.sid,
            b.rname,
            a.sfrom,
            a.sto,
            a.boid,
            c.boname,
            c.bopassenger
          FROM ticket_schedule AS a
          LEFT JOIN ticket_route AS b ON a.rid = b.rid
          LEFT JOIN ticket_boat AS c ON a.boid = c.boid
          WHERE a.sstatus = %s AND a.sto >= %s ORDER BY a.sfrom ASC, a.rid ASC';
    $q = $db->prepare_query( $s, 'publish', date( 'Y-m-d' ) );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $content = '
        <div class="row">
            <div class="col-md-12">
                <div class="calendar-detail">
                    <h3>Allotment Calendar</h3>                    
                    <p>
                        <strong>Current Schedule:</strong>
                        <select name="trip_boat" style="width:200px;">
                            ' . get_boat_option() . '
                        </select>
                        <select name="trip_schedule" style="width:500px;">';
                            $idx         = 0;
                            $sid         = '';
                            $sfrom       = '';
                            $sto         = '';
                            $boname      = '';
                            $bopassenger = '';

                            while( $d = $db->fetch_array( $r ) )
                            {
                                if( $idx == 0 )
                                {
                                    $sid         = $d['sid'];
                                    $sfrom       = $d['sfrom'];
                                    $sto         = $d['sto'];
                                    $boname      = $d['boname'];
                                    $bopassenger = $d['bopassenger'];
                                }

                                $content .= '
                                <option value="' . $d['sid'] . '"  data-boid="' . $d['boid'] . '" data-name="' . $d['boname'] . '" data-passenger = "' . $d['bopassenger'] . '" data-from="' . $d['sfrom'] . '" data-to="' . $d['sto'] . '">
                                    ' . date( 'd M y', strtotime( $d['sfrom'] ) ) . ' - ' . date( 'd M y', strtotime( $d['sto'] ) ) . ' / ' . $d['rname'] .'
                                </option>';

                                $idx++;
                            }

                            $content .= '
                        </select>
                        <strong>Boat:</strong> 
                        <span class="boat-passenger">' . $bopassenger . '</span>
                        <span>, <i>NB. : available seats / capacity</i></span>
                        <input name="sid" class="sr-only" value="' . $sid . '" />
                        <input name="sto" class="sr-only" value="' . $sto . '" />
                        <input name="sfrom" class="sr-only" value="' . $sfrom . '" />
                        <input name="bopassenger" class="sr-only" value="' . $bopassenger . '" />
                    </p>
                </div>

                <div id="availability-calendar" class="availability-calendar"></div>
            </div>
        </div>';

        return $content;
    }
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_allotment_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data-allotment-by-schedule' )
    {
        $data = ticket_allotment_by_schedule_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-data-allotment-by-agent' )
    {
        $data = ticket_allotment_by_agent_table_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-agent-allotment' )
    {
        echo add_new_agent_allotment( $_POST['type'] );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-allotment-detail' )
    {
        if( delete_allotment_agent( $_POST['dagid'] ) )
        {
            echo json_encode( array( 'result' => 'success', 'alloted' => get_total_allotment_by_schedule( $_POST['sid'] ) ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-agent-allotment' )
    {
        $data = get_agent_allotment( $_POST['month'], $_POST['year'], $_POST['sid'], $_POST['agid'] );

        if( !empty( $data ) )
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'edit-agent-allotment-detail' )
    {
        $close  = isset( $_POST['close'] ) ? $_POST['close'] : 0;
        $rallot = intval( $_POST['rallot'] );
        $gbseat = intval( $_POST['gbseat'] );
        $nallot = intval( $_POST['allot'] );

        if( $gbseat <= 0 && $nallot > $rallot )
        {
            echo json_encode( array( 'result' => 'error', 'message' => 'Sorry, you can\'t add allotment greater then previous allotment because global seat for this schedule already full' ) );
        }
        else
        {
            $sallot = abs( $rallot - $nallot );
            $mallot = $rallot + $gbseat;

            if( $nallot > $rallot && $sallot > $gbseat )
            {
                echo json_encode( array( 'result' => 'error', 'message' => 'Sorry, you can\'t add allotment greater then remain global seat. Remain global seat right now is <b>' . $gbseat . ' seat</b> and maximun allotment you can add is <b>' . $mallot . '</b> allotment' ) );
            }
            else
            {
                if( save_allotment_detail( $_POST['dagid'], $_POST['allot'], date( 'Y-m-d', strtotime( $_POST['adate'] ) ), $close, $rallot ) )
                {
                    echo json_encode( array( 'result' => 'success' ) );
                }
                else
                {
                    echo json_encode( array( 'result' => 'failed' ) );
                }
            }
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-online-allotment' )
    {
        echo add_new_online_allotment( $_POST['type'] );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-allotment-detail-online' )
    {
        if( delete_allotment_online( $_POST['daoid'] ) )
        {
            echo json_encode( array( 'result' => 'success', 'alloted' => get_total_allotment_by_schedule( $_POST['sid'] ) ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-online-allotment' )
    {
        $data = get_online_allotment( $_POST['month'], $_POST['year'], $_POST['sid'] );

        if( !empty( $data ) )
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'edit-online-allotment-detail' )
    {
        $close  = isset( $_POST['close'] ) ? $_POST['close'] : 0;
        $rallot = intval( $_POST['rallot'] );
        $gbseat = intval( $_POST['gbseat'] );
        $nallot = intval( $_POST['allot'] );

        if( $gbseat <= 0 && $nallot > $rallot )
        {
            echo json_encode( array( 'result' => 'error', 'message' => 'Sorry, you can\'t add allotment greater then previous allotment because global seat for this schedule already full' ) );
        }
        else
        {
            $sallot = abs( $rallot - $nallot );
            $mallot = $rallot + $gbseat;

            if( $nallot > $rallot && $sallot > $gbseat )
            {
                echo json_encode( array( 'result' => 'error', 'message' => 'Sorry, you can\'t add allotment greater then remain global seat. Remain global seat right now is <b>' . $gbseat . ' seat</b> and maximun allotment you can add is <b>' . $mallot . '</b> allotment' ) );
            }
            else
            {
                if( save_allotment_detail_online( $_POST['daoid'], $_POST['allot'], date( 'Y-m-d', strtotime( $_POST['adate'] ) ), $close, $rallot ) )
                {
                    echo json_encode( array( 'result' => 'success' ) );
                }
                else
                {
                    echo json_encode( array( 'result' => 'failed' ) );
                }
            }
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-all-agent-allotment' )
    {
        $data = get_all_agent_allotment( $_POST['month'], $_POST['year'], $_POST['sid'] );

        if( !empty( $data ) )
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}