<?php

add_actions( 'rate', 'ticket_rate' );
add_actions( 'ticket-rate-ajax_page', 'ticket_rate_ajax' );

function ticket_rate()
{
    if( is_num_rate() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'schedules&sub=rate&prc=add_new' ) );
    }

	if( is_add_new() )
    {
        return ticket_add_new_rate();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_rate( $_GET['id'] ) )
        {
            return ticket_edit_rate();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_rate();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key=>$val )
        {
            delete_rate( $val );
        }
    }

    return ticket_rate_table();
}

/*
| -------------------------------------------------------------------------------------
| Rate Table List
| -------------------------------------------------------------------------------------
*/
function ticket_rate_table()
{
    if( isset( $_POST ) && !empty( $_POST ) && !is_confirm_delete() )
    {
        header( 'Location:' . get_state_url( 'schedules&sub=rate' ) . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    extract( ticket_filter_rate() );

    $result   = ticket_rate_table_query( $rid, $rpid );
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/rate/list.html', 'rate' );

    add_block( 'list-block', 'rtblock', 'rate' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', ticket_rate_table_data( $result ) );
    add_variable( 'message', generate_error_query_message_block() );
    add_variable( 'route_option', get_route_option( $rid, false ) );
    add_variable( 'period_option', get_rate_period_option( $rpid, false) );

    add_variable( 'action', get_state_url( 'schedules&sub=rate' ) );
    add_variable( 'add_new_link', get_state_url( 'schedules&sub=rate&prc=add_new' ) );
    add_variable( 'edit_link', get_state_url( 'schedules&sub=rate&prc=edit' ) );
    
    parse_template( 'list-block', 'rtblock', false );
    
    add_actions( 'section_title', 'Rate List' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate' );
}

/*
| -------------------------------------------------------------------------------------
| Rate Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_rate_table_query( $rid = '', $rpid = '' )
{
    global $db;
    
    $w = array();

    if( isset( $_GET['prm'] ) )
    {
        if( $rid != '' )
        {
            $w[] = $db->prepare_query( 'b.rid = %d', $rid );
        }
        
        if( $rpid != '' )
        {
            $w[] = $db->prepare_query( 'a.rpid = %d', $rpid );
        }
    }

    if( empty( $w ) )
    {
        $s = 'SELECT * FROM ticket_rate ORDER BY rtid DESC';
        $q = $db->prepare_query( $s );
        $r = $db->do_query( $q );
    }
    else
    {
        $s = 'SELECT a.* FROM ticket_rate AS a
              LEFT JOIN ticket_rate_detail AS b ON b.rtid = a.rtid
              WHERE ' . implode( ' AND ', $w ) . ' GROUP BY a.rtid
              ORDER BY rtid DESC';
        $q = $db->prepare_query( $s );
        $r = $db->do_query( $q );
    }

    return $r;
}

/*
| -------------------------------------------------------------------------------------
| Rate Table List Item
| -------------------------------------------------------------------------------------
*/
function ticket_rate_table_data( $r )
{
    global $db;
    
    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="10">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/rate/loop.html', 'rate-loop' );

    add_block( 'loop-block', 'rtloop', 'rate-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'id', $d['rtid'] );
        add_variable( 'period', get_rate_period( $d['rpid'], 'rperiod' ) );
        add_variable( 'route', get_rate_table_route_row( $d['rtid'] ) );
        add_variable( 'depart', get_rate_detail_table( $d['rtid'] ) );
        add_variable( 'seat_disc', $d['rtseatdisc'] == '1' ? 'Yes' : 'No' );
        add_variable( 'early_disc', $d['rtearlydisc'] == '1' ? 'Yes' : 'No' );

        add_variable( 'edit_link', get_state_url( 'schedules&sub=rate&prc=edit&id=' . $d['rtid'] ) );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-rate-ajax/' );

        parse_template( 'loop-block', 'rtloop', true );
    }

    return return_template( 'rate-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Rate
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_rate()
{
    $message = run_save_rate();
    $data    = get_rate();

    set_template( PLUGINS_PATH . '/ticket/tpl/rate/form.html', 'rate' );
    add_block( 'form-block', 'rtblock', 'rate' );

    add_variable( 'rtid', $data['rtid'] );
    add_variable( 'rtinfo', $data['rtinfo'] );
    add_variable( 'rtinclude', $data['rtinclude'] );
    add_variable( 'rpid', get_rate_period_option( $data['rpid'] ) );
    add_variable( 'rttype', get_rate_type_option( $data['rttype'] ) );
    add_variable( 'rid', get_applicable_route( $data['rtid'] ) );
    add_variable( 'rate_option', get_rate_options( $data['rtid'] ) );
    add_variable( 'seat_discount', get_seat_discount( $data['rtid'] ) );
    add_variable( 'early_discount', get_early_discount( $data['rtid'] ) );
    add_variable( 'seat_disc_check', empty( $data['rtseatdisc'] ) ? '' : 'checked' );
    add_variable( 'seat_disc_css', empty( $data['rtseatdisc'] ) ? 'hidden' : '' );
    add_variable( 'early_disc_check', empty( $data['rtearlydisc'] ) ? '' : 'checked' );
    add_variable( 'early_disc_css', empty( $data['rtearlydisc'] ) ? 'hidden' : '' );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'schedules&sub=rate&prc=add_new' ) );
    add_variable( 'admin_theme', get_meta_data('admin_theme','themes') );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-rate-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=rate' ) );
    add_variable( 'delete_class', 'sr-only' );

    parse_template( 'form-block', 'rtblock', false );
    
    add_actions( 'section_title', 'New Rate' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
    add_actions( 'header_elements', 'get_javascript_inc', 'tiny_mce/tinymce.min.js' );

    return return_template( 'rate' );    
}

/*
| -------------------------------------------------------------------------------------
| Edit Rate
| -------------------------------------------------------------------------------------
*/
function ticket_edit_rate()
{
    $message = run_update_rate();
    $data    = get_rate( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/rate/form.html', 'rate' );
    add_block( 'form-block', 'rtblock', 'rate' );
    
    add_variable( 'rtid', $data['rtid'] );
    add_variable( 'rtinfo', $data['rtinfo'] );
    add_variable( 'rtinclude', $data['rtinclude'] );
    add_variable( 'rpid', get_rate_period_option( $data['rpid'] ) );
    add_variable( 'rttype', get_rate_type_option( $data['rttype'] ) );
    add_variable( 'rid', get_applicable_route( $data['rtid'] ) );
    add_variable( 'rate_option', get_rate_options( $data['rtid'] ) );
    add_variable( 'seat_discount', get_seat_discount( $data['rtid'] ) );
    add_variable( 'early_discount', get_early_discount( $data['rtid'] ) );
    add_variable( 'seat_disc_check', empty( $data['rtseatdisc'] ) ? '' : 'checked' );
    add_variable( 'seat_disc_css', empty( $data['rtseatdisc'] ) ? 'hidden' : '' );
    add_variable( 'early_disc_check', empty( $data['rtearlydisc'] ) ? '' : 'checked' );
    add_variable( 'early_disc_css', empty( $data['rtearlydisc'] ) ? 'hidden' : '' );

    add_variable( 'img_url', get_theme_img() );
    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'action', get_state_url( 'schedules&sub=rate&prc=edit&id=' . $_GET['id'] ) );
    add_variable( 'admin_theme', get_meta_data('admin_theme','themes') );
    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-rate-ajax/' );
    add_variable( 'cancel_link', get_state_url( 'schedules&sub=rate' ) );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'rate' ) );
    add_variable( 'delete_class', '' );

    parse_template( 'form-block', 'rtblock', false );
    
    add_actions( 'section_title', 'Edit Rate' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
    add_actions( 'header_elements', 'get_javascript_inc', 'tiny_mce/tinymce.min.js' );

    return return_template( 'rate' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Rate
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_rate()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/rate/batch-delete.html', 'rate' );
    add_block( 'loop-block', 'pmlblock', 'rate' );
    add_block( 'delete-block', 'rtblock', 'rate' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = get_rate( $val );

        add_variable( 'rtid', $d['rtid'] );
        add_variable( 'period', get_rate_period( $d['rpid'], 'rperiod' ) );
        add_variable( 'route', get_rate_table_route_row( $d['rtid'] ) );

        parse_template('loop-block', 'pmlblock', true);
    }

    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' rate? :' );
    add_variable( 'action', get_state_url('schedules&sub=rate') );

    parse_template( 'delete-block', 'rtblock', false );
    
    add_actions( 'section_title', 'Delete Rate' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'rate' );
}

function run_save_rate()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_rate_data();

        if( empty($error) )
        {
            $post_id = save_rate();
            
            header( 'location:'.get_state_url( 'schedules&sub=rate&prc=add_new&result=1' ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'New rate successfully saved' ) );
    }
}

function run_update_rate()
{    
    if( is_save_draft() || is_publish() )
    {
        $error = validate_rate_data();

        if( empty($error) )
        {
            $post_id = update_rate();
            
            header( 'location:'.get_state_url( 'schedules&sub=rate&prc=edit&result=1&id=' . $post_id ) );
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
    elseif( isset( $_GET['result'] ) && $_GET['result'] == 1 )
    {
        return array( 'type'=> 'success', 'content' => array( 'This rate successfully edited' ) );
    }
}

function validate_rate_data()
{
    $error = array();

    if( isset($_POST['rpid']) && empty( $_POST['rpid'] ) )
    {
        $error[] = 'Rate period can\'t be empty';
    }

    if( isset($_POST['rttype']) && $_POST['rttype'] == '' )
    {
        $error[] = 'Rate options type can\'t be empty';
    }

    if( isset( $_POST['rid'] ) && !empty( $_POST['rid'] ) )
    {
        $i  = 0;
        $id = array();

        foreach( $_POST['rid'] as $key => $rid )
        {
            if( empty( $rid ) )
            {
                $i++;
            }
            else
            {
                $id[] = $rid;
            }
        }

        if( $i > 0 )
        {
            $error[] = 'Route can\'t be empty';
        }

        if( !empty( $id ) )
        {
            if( count( array_unique( $id) ) < count( $id ) )
            {
                $error[] = 'Some of route have same value';
            }
        }
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate List Count
| -------------------------------------------------------------------------------------
*/
function is_num_rate()
{
    global $db;

    $s = 'SELECT * FROM ticket_rate';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Rate By ID
| -------------------------------------------------------------------------------------
*/
function get_rate( $id='' )
{
    global $db;

    $data = array( 
        'rtid'          => ( isset( $_POST['rtid'] ) ? $_POST['rtid'] : null ),
        'rpid'          => ( isset( $_POST['rpid'] ) ? $_POST['rpid'] : '' ),
        'rttype'        => ( isset( $_POST['rttype'] ) ? $_POST['rttype'] : '' ),
        'rtinclude'     => ( isset( $_POST['rtinclude'] ) ? $_POST['rtinclude'] : '' ),
        'rtinfo'        => ( isset( $_POST['rtinfo'] ) ? $_POST['rtinfo'] : '' ),
        'rtseatdisc'    => ( isset( $_POST['rtseatdisc'] ) ? $_POST['rtseatdisc'] : '' ),
        'rtearlydisc'   => ( isset( $_POST['rtearlydisc'] ) ? $_POST['rtearlydisc'] : '' ),
        'rtcreateddate' => ( isset( $_POST['rtcreateddate'] ) ? $_POST['rtcreateddate'] : '' ),
        'luser_id'      => ( isset( $_POST['luser_id'] ) ? $_POST['luser_id'] : '' )
    );

    $s = 'SELECT * FROM ticket_rate WHERE rtid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array(
                'rtid' => $d['rtid'],
                'rpid' => $d['rpid'],
                'rttype' => $d['rttype'],
                'rtinclude' => $d['rtinclude'],
                'rtinfo' => $d['rtinfo'],
                'rtseatdisc' => $d['rtseatdisc'],
                'rtearlydisc' => $d['rtearlydisc'],
                'rtcreateddate' => $d['rtcreateddate'],
                'luser_id' => $d['luser_id']
            );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Rate
| -------------------------------------------------------------------------------------
*/
function save_rate()
{
    global $db;

    $rtseatdisc  = isset( $_POST['rtseatdisc'] ) ? '1' : '0';
    $rtearlydisc = isset( $_POST['rtearlydisc'] ) ? '1' : '0';
    
    $s = 'INSERT INTO ticket_rate(
            rpid,
            rttype,
            rtinclude,
            rtinfo,
            rtseatdisc,
            rtearlydisc,
            rtcreateddate,
            luser_id)
          VALUES( %d, %s, %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s,
            $_POST['rpid'],
            $_POST['rttype'],
            $_POST['rtinclude'],
            $_POST['rtinfo'],
            $rtseatdisc,
            $rtearlydisc,
            date( 'Y-m-d H:i:s' ),
            $_COOKIE['user_id'] );

    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        $rtid  = $db->insert_id();
        $rtdid = save_rate_detail( $rtid );

        save_rate_seat_discount( $rtid );
        save_rate_early_discount( $rtid );
        
        save_rate_route_detail( $rtdid );

        save_log( $rtid, 'rate', 'Add New Schedule - Rate ID#' . $rtid );
    
        return $rtid;
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Rate Detail
| -------------------------------------------------------------------------------------
*/
function save_rate_detail( $rtid = '' )
{
    global $db;

    $rtdid = array();

    if( $_POST['rttype'] == 0 )
    {
        $rtiadult  = $_POST['rinclude']['adult'];
        $rtichild  = $_POST['rinclude']['child'];
        $rtiinfant = $_POST['rinclude']['infant'];
        $rteadult  = $_POST['rexclude']['adult'];
        $rtechild  = $_POST['rexclude']['child'];
        $rteinfant = $_POST['rexclude']['infant'];
    }
    else
    {
        $rtiadult  = 0;
        $rtichild  = 0;
        $rtiinfant = 0;
        $rteadult  = 0;
        $rtechild  = 0;
        $rteinfant = 0;
    }

    foreach( $_POST['rid'] as $rid )
    {
        $s = 'INSERT INTO ticket_rate_detail(
                rtid,
                rid,
                rtiadult,
                rtichild,
                rtiinfant,
                rteadult,
                rtechild,
                rteinfant)
              VALUES( %d, %d, %s, %s, %s, %s, %s, %s )';
        $q = $db->prepare_query( $s,
                $rtid,
                $rid,
                $rtiadult,
                $rtichild,
                $rtiinfant,
                $rteadult,
                $rtechild,
                $rteinfant);
        $r = $db->do_query( $q );

        if( !is_array( $r ) )
        {
            $rtdid[$rid] = $db->insert_id();
        }
    }

    return $rtdid;
}

/*
| -------------------------------------------------------------------------------------
| Save Rate Route Detail
| -------------------------------------------------------------------------------------
*/
function save_rate_route_detail( $rtdid = array() )
{
    global $db;

    if( $_POST['rttype'] == 1 && !empty( $rtdid ) )
    {
        foreach( $rtdid as $rid => $id )
        {
            foreach( $_POST['lcid'][$rid] as $lcid )
            {
                list( $fpoint, $tpoint ) = explode( '|', $lcid );

                $s = 'INSERT INTO ticket_rate_route_detail(
                        rtdid,
                        lcid,
                        lcid_to,
                        rtiadult,
                        rtichild,
                        rtiinfant,
                        rteadult,
                        rtechild,
                        rteinfant)
                      VALUES( %d, %d, %d, %s, %s, %s, %s, %s, %s )';
                $q = $db->prepare_query( $s,
                        $id,
                        $fpoint,
                        $tpoint,
                        $_POST['rinclude'][$rid][$lcid]['adult'],
                        $_POST['rinclude'][$rid][$lcid]['child'],
                        $_POST['rinclude'][$rid][$lcid]['infant'],
                        $_POST['rexclude'][$rid][$lcid]['adult'],
                        $_POST['rexclude'][$rid][$lcid]['child'],
                        $_POST['rexclude'][$rid][$lcid]['infant']);
                
                $db->do_query( $q );
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Rate Seat Discount
| -------------------------------------------------------------------------------------
*/
function save_rate_seat_discount( $rtid = '' )
{
    global $db;

    if( isset( $_POST['rtseatdisc'] ) )
    {
        if( isset( $_POST['sdisc'] ) && is_array( $_POST['sdisc'] ) )
        {
            foreach( $_POST['sdisc'] as $key => $disc )
            {
                if( !empty( $_POST['seat'][$key] ) && !empty( $disc ) )
                {
                    $s = 'INSERT INTO ticket_rate_seat_discount( rtid, notif, seat, disc, type ) VALUES( %d, %s, %d, %s, %s )';
                    $q = $db->prepare_query( $s, $rtid, $_POST['notif'][$key], $_POST['seat'][$key], $disc, $_POST['stype'][$key] );
                    
                    $db->do_query( $q );
                }
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Rate Early Discount
| -------------------------------------------------------------------------------------
*/
function save_rate_early_discount( $rtid = '' )
{
    global $db;

    if( isset( $_POST['rtearlydisc'] ) )
    {
        if( isset( $_POST['ddisc'] ) && is_array( $_POST['ddisc'] ) )
        {
            foreach( $_POST['ddisc'] as $key => $disc )
            {
                if( !empty( $_POST['pfrom'][$key] ) && !empty( $_POST['pto'][$key] ) && !empty( $disc ) && !empty( $_POST['rtnotif'][$key] ) )
                {
                    $s = 'INSERT INTO ticket_rate_early_discount( 
                            rtid, 
                            rtnotif, 
                            pfrom, 
                            pto, 
                            disc, 
                            type ) 
                          VALUES( %d, %s, %s, %s, %s, %s )';
                    $q = $db->prepare_query( $s, 
                            $rtid,
                            $_POST['rtnotif'][$key],
                            date( 'Y-m-d', strtotime( $_POST['pfrom'][$key] ) ), 
                            date( 'Y-m-d', strtotime( $_POST['pto'][$key] ) ), 
                            $disc, 
                            $_POST['dtype'][$key] );
                    
                    $db->do_query( $q );
                }
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Rate
| -------------------------------------------------------------------------------------
*/
function update_rate()
{    
    global $db;

    $data            = rate_data_all( $_POST['rtid'] );
    $rtseatdisc_new  = isset( $_POST['rtseatdisc'] ) ? '1' : '0';
    $rtearlydisc_new = isset( $_POST['rtearlydisc'] ) ? '1' : '0';
    $fields          = array();

    extract( $data );

    $s = 'UPDATE ticket_rate SET 
            rpid = %s,
            rttype = %s,
            rtinclude = %s, 
            rtinfo = %s,
            rtseatdisc = %s,
            rtearlydisc = %s
          WHERE rtid = %d';     
    $q = $db->prepare_query( $s,
            $_POST['rpid'],
            $_POST['rttype'],
            $_POST['rtinclude'],
            $_POST['rtinfo'],
            $rtseatdisc_new,
            $rtearlydisc_new,
            $_POST['rtid'] );

    /* BEGIN CREATE LOG */
    $t_rate   = array();    
    $t_rate[] = $rpid != $_POST['rpid'] ? 'Rate Period Code : ' . $rpid . ' --> ' . $_POST['rpid'] : '';
    $t_rate[] = $rttype != $_POST['rttype'] ? 'Change Rate Option : ' . ( $rttype == '0' ? 'Same rate (all departure)' : 'Different rate (different departure)' ) . ' to ' . ( $_POST['rttype'] == '0' ? 'Same rate (all departure)' : 'Different rate (different departure)' ) : '';
    $t_rate[] = $rtinclude != $_POST['rtinclude'] ? 'Inclusion : ' . strip_tags( $rtinclude ) . ' --> ' . strip_tags( $_POST['rtinclude'] ) : '';
    $t_rate[] = $rtinfo != $_POST['rtinfo'] ? 'Additional Info : ' . strip_tags( $rtinfo ) . ' --> ' . strip_tags( $_POST['rtinfo'] ) : '';
    $t_rate[] = $rtseatdisc != $rtseatdisc_new ? 'Seat Discount : --> ' . ( $rtseatdisc_new == '0' ? 'Remove Seat Discount' : 'Add Seat Discount' ) : ''; 
    $t_rate[] = $rtearlydisc != $rtearlydisc_new ? 'Early Bird Discount : --> ' . ( $rtearlydisc_new == '0' ? 'Remove Early Bird Discount' : 'Add Early Bird Discount' ) : '';

    if ( !empty( $t_rate ) ) 
    {
        $fields[] = $t_rate;
    }

    if ( $_POST['rttype'] == '0' ) 
    {
        $log_same_rate   = array();
        $log_same_rate[] = $detail['rtiadult'] != $_POST['rinclude']['adult'] ? 'Rate Include (Adult) : ' . $detail['rtiadult'] . ' --> ' . $_POST['rinclude']['adult'] : '';
        $log_same_rate[] = $detail['rtichild'] != $_POST['rinclude']['child'] ? 'Rate Include (Child) : ' . $detail['rtichild'] . ' --> ' . $_POST['rinclude']['child'] : '';
        $log_same_rate[] = $detail['rtiinfant'] != $_POST['rinclude']['infant'] ? 'Rate Include (Infant) : ' . $detail['rtiinfant'] . ' --> ' . $_POST['rinclude']['infant'] : '';
        $log_same_rate[] = $detail['rteadult'] != $_POST['rexclude']['adult'] ? 'Rate Exclude (Adult) : ' . $detail['rteadult'] . ' --> ' . $_POST['rexclude']['adult'] : '';
        $log_same_rate[] = $detail['rtechild'] != $_POST['rexclude']['child'] ? 'Rate Exclude (Child) : ' . $detail['rtechild'] . ' --> ' . $_POST['rexclude']['child'] : '';
        $log_same_rate[] = $detail['rteinfant'] != $_POST['rexclude']['infant'] ? 'Rate Exclude (Infant) : ' . $detail['rteinfant'] . ' --> ' . $_POST['rexclude']['infant'] : '';
        
        if ( !empty( $log_same_rate ) ) 
        {
            $fields[] = $log_same_rate;
        }
    }
    /* END CREATE LOG */

    if( $db->do_query( $q ) )
    {
        $rtdid = update_rate_detail( $_POST['rtid'] );

        update_rate_seat_discount( $_POST['rtid'] );
        update_rate_early_discount( $_POST['rtid'] );

        update_rate_route_detail( $rtdid );

        /* BEGIN SAVE LOG */
        if ( !empty( $fields ) ) 
        {
            $field_log = array();
            $field_log[] = '<b>Detail Edit Rate</b> ------>';

            foreach ( $fields as $id => $logs ) 
            {
                foreach ( $logs as $vky => $log ) 
                {
                    if ( $log != '' ) 
                    {
                        $field_log[] = $log;
                    }
                }               
            }

            if ( !empty( $field_log ) ) 
            {
                save_log( $_POST['rtid'], 'rate', 'Edit Schedule - Rate  ID#' . $_POST['rtid'] . '<br/>' . implode( '<br/>', $field_log ) );
            }           
        }

        /* END SAVE LOG */

        return $_POST['rtid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Rate Detail
| -------------------------------------------------------------------------------------
*/
function update_rate_detail( $rtid = '' )
{
    global $db;

    $s = 'DELETE FROM ticket_rate_detail WHERE rtid = %d';          
    $q = $db->prepare_query( $s, $rtid );
    $r = $db->do_query( $q );

    if( !isset( $r['error_code'] ) )
    {
        return save_rate_detail( $rtid );
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Rate Route Detail
| -------------------------------------------------------------------------------------
*/
function update_rate_route_detail( $rtdid = array() )
{
    return save_rate_route_detail( $rtdid );
}

/*
| -------------------------------------------------------------------------------------
| Update Rate Seat Discount
| -------------------------------------------------------------------------------------
*/
function update_rate_seat_discount( $rtid = '' )
{
    global $db;

    $s = 'DELETE FROM ticket_rate_seat_discount WHERE rtid = %d';          
    $q = $db->prepare_query( $s, $rtid );
    $r = $db->do_query( $q );
       
    if( !isset( $r['error_code'] ) )
    {
        return save_rate_seat_discount( $rtid );
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Rate Early Discount
| -------------------------------------------------------------------------------------
*/
function update_rate_early_discount( $rtid = '' )
{
    global $db;

    $s = 'DELETE FROM ticket_rate_early_discount WHERE rtid = %d';          
    $q = $db->prepare_query( $s, $rtid );
    $r = $db->do_query( $q );
       
    if( !isset( $r['error_code'] ) )
    {
        return save_rate_early_discount( $rtid );
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Rate
| -------------------------------------------------------------------------------------
*/
function delete_rate( $rtid = '', $is_ajax = false )
{
    global $db;

    $s = 'DELETE FROM ticket_rate WHERE rtid = %d';          
    $q = $db->prepare_query( $s, $rtid );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'schedules&sub=rate&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $rtid, 'rate', 'Delete Schedule - Rate ID#' . $rtid );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Rate Detail
| -------------------------------------------------------------------------------------
*/
function delete_rate_detail( $rtdid = '', $is_ajax = false )
{
    global $db;

    $s = 'DELETE FROM ticket_rate_detail WHERE rtdid = %d';          
    $q = $db->prepare_query( $s, $rtdid );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'schedules&sub=rate&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $rtdid, 'rate-detail', 'Delete rate detail - ID : #' . $rtdid );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Type Options
| -------------------------------------------------------------------------------------
*/
function get_rate_type_option( $type = '' )
{
    if( isset( $_POST['rttype'] ) )
    {
        $option = '
        <option value="">Select rate options</option>
        <option value="0" ' . ( $_POST['rttype']=='0' ? 'selected' : '' ) . '>Same Rate for all departure</option>
        <option value="1" ' . ( $_POST['rttype']=='1' ? 'selected' : '' ) . '>Different rate for different departure</option>';
    }
    else
    {
        $option = '
        <option value="">Select rate options</option>
        <option value="0" ' . ( $type=='0' ? 'selected' : '' ) . '>Same Rate for all departure</option>
        <option value="1" ' . ( $type=='1' ? 'selected' : '' ) . '>Different rate for different departure</option>';
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Applicable Route Options
| -------------------------------------------------------------------------------------
*/
function get_applicable_route( $rtid = '' )
{
    global $db;

    $row = '';

    if( isset( $_POST['rid'] ) && !empty( $_POST['rid'] ) )
    {
        foreach( $_POST['rid'] as $rid )
        {
            $row .= '
            <div class="form-group-block">
                <select class="select-option route-list" name="rid[]" autocomplete="off">
                    '. get_route_option( $rid ) .'
                </select>
                <a class="delete-route" data-id="">
                    <img src="../l-plugins/ticket/images/delete.svg">
                </a>
            </div>';
        }
    }
    else
    {
        $s = 'SELECT a.rtdid, b.rname, b.rid FROM ticket_rate_detail AS a LEFT JOIN ticket_route AS b ON a.rid = b.rid WHERE rtid = %d';
        $q = $db->prepare_query( $s, $rtid );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                $row .= '
                <div class="form-group-block">
                    <select class="select-option route-list" name="rid[]" autocomplete="off">
                        '. get_route_option( $d['rid'] ) .'
                    </select>
                    <a class="delete-route" data-id="' . $d['rtdid'] . '">
                        <img src="../l-plugins/ticket/images/delete.svg">
                    </a>
                </div>';
            }
        }
        else
        {
            $row .= '
            <div class="form-group-block">
                <select class="select-option route-list" name="rid[]" autocomplete="off">
                    '. get_route_option() .'
                </select>
                <a class="delete-route" data-id="">
                    <img src="../l-plugins/ticket/images/delete.svg">
                </a>
            </div>';
        }
    }

    return $row;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Options
| -------------------------------------------------------------------------------------
*/
function get_rate_options( $rtid = '' )
{
    global $db;

    $wrap = '';

    if( empty( $rtid ) )
    {
        return $wrap;
    }

    $s = 'SELECT * FROM ticket_rate WHERE rtid = %d';
    $q = $db->prepare_query( $s, $rtid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $type = isset( $_POST['rttype'] ) ? $_POST['rttype'] : $d['rttype'];

    if( $type == '0' )
    {
        $s2 = 'SELECT * FROM ticket_rate_detail WHERE rtid = %d';
        $q2 = $db->prepare_query( $s2, $rtid );
        $r2 = $db->do_query( $q2 );
        $d2 = $db->fetch_array( $r2 );

        $wrap .= '
        <div class="include-transport-block">
            <label>Rate (Include Transport)</label>
            <div class="form-group">
                <p class="subtitle">Adult</p>
                <input type="text" class="text text-small form-control" name="rinclude[adult]" value="' . $d2['rtiadult'] . '" >
            </div>
            <div class="form-group">
                <p class="subtitle">Child</p>
                <input type="text" class="text text-small form-control" name="rinclude[child]" value="' . $d2['rtichild'] . '" >
            </div>
            <div class="form-group">
                <p class="subtitle">Infant</p>
                <input type="text" class="text text-small form-control" name="rinclude[infant]" value="' . $d2['rtiinfant'] . '" >
            </div>
        </div>
        <div class="exclude-transport-block">
            <label>Rate (Exclude Transport)</label>
            <div class="form-group">
                <p class="subtitle">Adult</p>
                <input type="text" class="text text-small form-control" name="rexclude[adult]" value="' . $d2['rteadult'] . '" >
            </div>
            <div class="form-group">
                <p class="subtitle">Child</p>
                <input type="text" class="text text-small form-control" name="rexclude[child]" value="' . $d2['rtechild'] . '" >
            </div>
            <div class="form-group">
                <p class="subtitle">Infant</p>
                <input type="text" class="text text-small form-control" name="rexclude[infant]" value="' . $d2['rteinfant'] . '" >
            </div>
        </div>';
    }
    elseif( $type == '1' )
    {
        $s2 = 'SELECT
                    a.rid,
                    b.lcid,
                    b.lcid_to,
                    c.lcname,
                    b.rtiadult,
                    b.rtichild,
                    b.rtiinfant,
                    b.rteadult,
                    b.rtechild,
                    b.rteinfant,
                    d.rname,
                    d.rdepart,
                    d.rhoppingpoint
               FROM ticket_rate_detail AS a
               LEFT JOIN ticket_rate_route_detail AS b ON b.rtdid = a.rtdid
               LEFT JOIN ticket_location AS c ON b.lcid = c.lcid
               LEFT JOIN ticket_route AS d ON a.rid = d.rid
               WHERE a.rtid = %d';
        $q2 = $db->prepare_query( $s2, $rtid );
        $r2 = $db->do_query( $q2 );

        if( $db->num_rows( $r2 ) > 0 )
        {
            $rows = array();
            $hoop = array();

            while( $d2 = $db->fetch_array( $r2 ) )
            {
                $from = get_location( $d2['lcid'], 'lcname' );
                $to   = get_location( $d2['lcid_to'], 'lcname' );

                $rows[ $d2['rname'] ]['detail'][] = '
                <div class="inner">
                    <h4>From ' . $from . ' to ' . $to . '</h4>
                    <div class="include-transport-block">
                        <label>Rate (Include Transport)</label>
                        <div class="form-group">
                            <p class="subtitle">Adult</p>
                            <input type="text" class="text text-small form-control" name="rinclude[' . $d2['rid'] . '][' . $d2['lcid'] . '|' . $d2['lcid_to'] . '][adult]" value="' . $d2['rtiadult'] . '" >
                        </div>
                        <div class="form-group">
                            <p class="subtitle">Child</p>
                            <input type="text" class="text text-small form-control" name="rinclude[' . $d2['rid'] . '][' . $d2['lcid'] . '|' . $d2['lcid_to'] . '][child]" value="' . $d2['rtichild'] . '" >
                        </div>
                        <div class="form-group">
                            <p class="subtitle">Infant</p>
                            <input type="text" class="text text-small form-control" name="rinclude[' . $d2['rid'] . '][' . $d2['lcid'] . '|' . $d2['lcid_to'] . '][infant]" value="' . $d2['rtiinfant'] . '" >
                        </div>
                    </div>
                    <div class="exclude-transport-block">
                        <label>Rate (Exclude Transport)</label>
                        <div class="form-group">
                            <p class="subtitle">Adult</p>
                            <input type="text" class="text text-small form-control" name="rexclude[' . $d2['rid'] . '][' . $d2['lcid'] . '|' . $d2['lcid_to'] . '][adult]" value="' . $d2['rteadult'] . '" >
                        </div>
                        <div class="form-group">
                            <p class="subtitle">Child</p>
                            <input type="text" class="text text-small form-control" name="rexclude[' . $d2['rid'] . '][' . $d2['lcid'] . '|' . $d2['lcid_to'] . '][child]" value="' . $d2['rtechild'] . '" >
                        </div>
                        <div class="form-group">
                            <p class="subtitle">Infant</p>
                            <input type="text" class="text text-small form-control" name="rexclude[' . $d2['rid'] . '][' . $d2['lcid'] . '|' . $d2['lcid_to'] . '][infant]" value="' . $d2['rteinfant'] . '" >
                        </div>
                    </div>
                    <div class="sr-only">
                        <input type="text" name="lcid[' . $d2['rid'] . '][]" value="' . $d2['lcid'] . '|' . $d2['lcid_to'] . '" />
                    </div>
                </div>';
            }

            foreach( $rows as $name => $row )
            {
                $wrap .= '
                <div class="rate-options-wrap">
                    <h3>' . $name . '</h3>
                    ' . implode( '', $row['detail'] ) . '
                </div>';
            }
        }
    }

    return $wrap;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Detail Table
| -------------------------------------------------------------------------------------
*/
function get_rate_detail_table( $rtid = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_rate WHERE rtid = %d';
    $q = $db->prepare_query( $s, $rtid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( $d['rttype'] == '0' )
    {
        $s2 = 'SELECT * FROM ticket_rate_detail WHERE rtid = %d';
        $q2 = $db->prepare_query( $s2, $rtid );
        $r2 = $db->do_query( $q2 );
        $d2 = $db->fetch_array( $r2 );

        $row = '
        <td class="depart"></td>
        <td class="adult text-right">
            <ul class="rate-price">
                <li>'. number_format( $d2['rtiadult'], 0, '', '.' ) .'</li>
                <li>'. number_format( $d2['rteadult'], 0, '', '.' ) .'</li>
            </ul>
        </td>
        <td class="child text-right">
            <ul class="rate-price">
                <li>'. number_format( $d2['rtichild'], 0, '', '.' ) .'</li>
                <li>'. number_format( $d2['rtechild'], 0, '', '.' ) .'</li>
            </ul>
        </td>
        <td class="infant text-right">
            <ul class="rate-price">
                <li>'. number_format( $d2['rtiinfant'], 0, '', '.' ) .'</li>
                <li>'. number_format( $d2['rteinfant'], 0, '', '.' ) .'</li>
            </ul>
        </td>
        <td class="trans text-center">
            <ul class="rate-trans">
                <li>Yes</li>
                <li>No</li>
            </ul>
        </td>';
    }
    elseif( $d['rttype'] == '1' )
    {
        $s2 = 'SELECT * FROM ticket_rate_detail AS a
               LEFT JOIN ticket_rate_route_detail AS b ON b.rtdid = a.rtdid
               WHERE a.rtid = %d';
        $q2 = $db->prepare_query( $s2, $rtid );
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );

        $depart = '<ul class="rate-location">';
        $adult  = '<ul class="rate-price">';
        $child  = '<ul class="rate-price">';
        $infant = '<ul class="rate-price">';
        $trans  = '<ul class="rate-trans">';

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $from = get_location( $d2['lcid'], 'lcname' );
            $to   = get_location( $d2['lcid_to'], 'lcname' );

            $depart .= '
            <li>'. $from  . ' - ' . $to . '</li>
            <li>&nbsp;</li>';

            $adult .= '
            <li>'. number_format( $d2['rtiadult'], 0, '', '.' ) .'</li>
            <li>'. number_format( $d2['rteadult'], 0, '', '.' ) .'</li>';
            
            $child .= '
            <li>'. number_format( $d2['rtichild'], 0, '', '.' ) .'</li>
            <li>'. number_format( $d2['rtechild'], 0, '', '.' ) .'</li>';
            
            $infant .= '
            <li>'. number_format( $d2['rtiinfant'], 0, '', '.' ) .'</li>
            <li>'. number_format( $d2['rteinfant'], 0, '', '.' ) .'</li>';
            
            $trans .= '
            <li>Yes</li>
            <li>No</li>';
        }

        $depart .= '</ul>';
        $adult  .= '</ul>';
        $child  .= '</ul>';
        $infant .= '</ul>';
        $trans  .= '</ul>';

        $row = '
        <td class="depart">
            ' . $depart . '
        </td>
        <td class="adult text-right">
            ' . $adult . '
        </td>
        <td class="child text-right">
            ' . $child . '
        </td>
        <td class="infant text-right">
            ' . $infant . '
        </td>
        <td class="trans text-center">
            ' . $trans . '
        </td>';
    }

    return $row;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Route List
| -------------------------------------------------------------------------------------
*/
function get_rate_table_route_row( $rtid = '' )
{
    global $db;

    $row = '';

    $s = 'SELECT b.rname FROM ticket_rate_detail AS a LEFT JOIN ticket_route AS b ON a.rid = b.rid WHERE rtid = %d';
    $q = $db->prepare_query( $s, $rtid );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $row .= '<li>' . $d['rname'] . '</li>';
        }
    }

    return '<ul>' . $row . '</ul>';
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Early Discount
| -------------------------------------------------------------------------------------
*/
function get_rate_early_discount( $rtid, $agid = '' )
{
    global $db;

    if( !empty( $agid ) )
    {
        $a = get_agent( $agid );

        if( $a['agtype'] == 'Net Rate' )
        {
            return 0;
        }
    }

    $rate  = get_rate( $rtid );
    $date  = date( 'Y-m-d' );
    $edisc = array();

    if( isset( $rate['rtearlydisc'] ) && $rate['rtearlydisc'] == 1 )
    {
        $s = 'SELECT * FROM ticket_rate_early_discount WHERE rtid = %d AND pfrom <= %s AND pto >= %s';
        $q = $db->prepare_query( $s, $rtid, $date, $date );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $d = $db->fetch_array( $r );

            $edisc = array( 'type' => $d['type'], 'disc' => $d['disc'], 'notif' => $d['rtnotif'] );
        }
    }

    return $edisc;
}

/*
| -------------------------------------------------------------------------------------
| Get Rate Seat Discount
| -------------------------------------------------------------------------------------
*/
function get_rate_seat_discount( $rtid, $agid = '', $sid, $rid, $depart_date )
{
    global $db;

    if( !empty( $agid ) )
    {
        $a = get_agent( $agid );

        if( $a['agtype'] == 'Net Rate' )
        {
            return 0;
        }
    }

    $rate  = get_rate( $rtid );
    $sdisc = array();

    if( isset( $rate['rtseatdisc'] ) && $rate['rtseatdisc'] == 1 )
    {
        $time   = strtotime( $depart_date );
        $seat   = get_total_seat_capacity( $rid, $time );
        $remain = get_seat_availibility_num_by_schedule( $seat, $rid, $sid, $time, true );
        
        $s = 'SELECT * FROM ticket_rate_seat_discount WHERE rtid = %d AND seat < %d ORDER BY seat DESC LIMIT 1';
        $q = $db->prepare_query( $s, $rtid, $remain );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            $d = $db->fetch_array( $r );

            if( $remain > $d['seat'] )
            {
                $sdisc = array( 'type' => $d['type'], 'disc' => $d['disc'], 'notif' => $d['notif'] );
            }
        }
    }

    return $sdisc;
}

function get_seat_discount( $rtid = '' )
{
    global $db;

    $item = '';

    if( isset( $_POST['sdisc'] ) && !empty( $_POST['sdisc'] ) )
    {
        foreach( $_POST['sdisc'] as $idx => $disc )
        {
            $item .= '
            <div class="item">
                <div class="form-group">
                    <span class="subtitle">For more than </span>
                    <input class="text text-small form-control" type="text" name="seat[]" value="' . $_POST['seat'][$idx] . '" />
                </div>
                <div class="form-group">
                    <span class="subtitle">seat left, discount </span>
                    <input class="text text-small form-control" type="text" name="sdisc[]" value="' . $disc . '" />
                </div>
                <div class="form-group">
                    <select name="stype[]" autocomplete="off">
                        <option value="0" ' . ( $_POST['stype'][$idx]=='0' ? 'selected' : '' ) . '>%</option>
                        <option value="1" ' . ( $_POST['stype'][$idx]=='1' ? 'selected' : '' ) . '>Rp.</option>
                    </select>
                </div>
                <div class="form-group">
                    <span class="subtitle">Notif </span>
                    <input type="text" class="text text-medium form-control" name="notif[]" value="' . $_POST['notif'][$idx] . '" >
                </div>
            </div>';
        }
    }
    else
    {
        $s = 'SELECT * FROM ticket_rate_seat_discount WHERE rtid = %d';
        $q = $db->prepare_query( $s, $rtid );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                $item .= '
                <div class="item">
                    <div class="form-group">
                        <span class="subtitle">For more than </span>
                        <input class="text text-small form-control" type="text" name="seat[]" value="' . $d['seat'] . '" />
                    </div>
                    <div class="form-group">
                        <span class="subtitle">seat left, discount </span>
                        <input class="text text-small form-control" type="text" name="sdisc[]" value="' . $d['disc'] . '" />
                    </div>
                    <div class="form-group">
                        <select name="stype[]" autocomplete="off">
                            <option value="0" ' . ( $d['type']=='0' ? 'selected' : '' ) . '>%</option>
                            <option value="1" ' . ( $d['type']=='1' ? 'selected' : '' ) . '>Rp.</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <span class="subtitle">Notif </span>
                        <input type="text" class="text text-medium form-control" name="notif[]" value="' . $d['notif'] . '" >
                    </div>
                </div>';
            }
        }
        else
        {
            $item .= '
            <div class="item">
                <div class="form-group">
                    <span class="subtitle">For more than </span>
                    <input class="text text-small form-control" type="text" name="seat[]" value="0" />
                </div>
                <div class="form-group">
                    <span class="subtitle">seat left, discount </span>
                    <input class="text text-small form-control" type="text" name="sdisc[]" value="0" />
                </div>
                <div class="form-group">
                    <select name="stype[]" autocomplete="off">
                        <option value="0">%</option>
                        <option value="1">Rp.</option>
                    </select>
                </div>
                <div class="form-group">
                    <span class="subtitle">Notif </span>
                    <input type="text" class="text text-medium form-control" name="notif[]" value="" >
                </div>
            </div>';
        }
    }

    return $item;
}

function get_early_discount( $rtid = '' )
{
    global $db;

    $item = '';

    if( isset( $_POST['ddisc'] ) && !empty( $_POST['ddisc'] ) )
    {
        foreach( $_POST['ddisc'] as $idx => $disc )
        {
            $item .= '
            <div class="item">
                <div class="form-group">
                    <p class="subtitle">Book Period</p>
                    <input type="text" class="text text-date text-date-from text-small form-control" name="pfrom[]" value="'. date( 'd F Y', strtotime( $_POST['pfrom'][$idx] ) ) .'" placeholder="Date" readonly>
                    <span class="subtitle">to</span>
                </div>
                <div class="form-group">
                    <p class="subtitle">&nbsp;</p>
                    <input type="text" class="text text-date text-date-to text-small form-control" name="pto[]" value="'. date( 'd F Y', strtotime( $_POST['pto'][$idx] ) ) .'" placeholder="Date" readonly>
                </div>
                <div class="form-group">
                    <p class="subtitle">Get Discount</p>
                    <input type="text" class="text text-small form-control" name="ddisc[]" value="' . $disc . '" >
                </div>
                <div class="form-group">
                    <p class="subtitle">&nbsp;</p>
                    <select name="dtype[]" autocomplete="off">
                        <option value="0" ' . ( $_POST['dtype'][$idx]=='0' ? 'selected' : '' ) . '>%</option>
                        <option value="1" ' . ( $_POST['dtype'][$idx]=='1' ? 'selected' : '' ) . '>Rp.</option>
                    </select>
                </div>
                <div class="form-group">
                    <p class="subtitle">Notif</p>
                    <input type="text" class="text text-medium form-control" name="rtnotif[]" value="' . $_POST['rtnotif'][$idx] . '" >
                </div>
            </div>';
        }
    }
    else
    {
        $s = 'SELECT * FROM ticket_rate_early_discount WHERE rtid = %d';
        $q = $db->prepare_query( $s, $rtid );
        $r = $db->do_query( $q );

        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                $item .= '
                <div class="item">
                    <div class="form-group">
                        <p class="subtitle">Book Period</p>
                        <input type="text" class="text text-date text-date-from text-small form-control" name="pfrom[]" value="'. date( 'd F Y', strtotime( $d['pfrom'] ) ) .'" placeholder="Date" readonly>
                        <span class="subtitle">to</span>
                    </div>
                    <div class="form-group">
                        <p class="subtitle">&nbsp;</p>
                        <input type="text" class="text text-date text-date-to text-small form-control" name="pto[]" value="'. date( 'd F Y', strtotime( $d['pto'] ) ) .'" placeholder="Date" readonly>
                    </div>
                    <div class="form-group">
                        <p class="subtitle">Get Discount</p>
                        <input type="text" class="text text-small form-control" name="ddisc[]" value="' . $d['disc'] . '" >
                    </div>
                    <div class="form-group">
                        <p class="subtitle">&nbsp;</p>
                        <select name="dtype[]" autocomplete="off">
                            <option value="0" ' . ( $d['type']=='0' ? 'selected' : '' ) . '>%</option>
                            <option value="1" ' . ( $d['type']=='1' ? 'selected' : '' ) . '>Rp.</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <p class="subtitle">Notif</p>
                        <input type="text" class="text text-medium form-control" name="rtnotif[]" value="' . $d['rtnotif'] . '" >
                    </div>
                </div>';
            }
        }
        else
        {
            $item .= '
            <div class="item">
                <div class="form-group">
                    <p class="subtitle">Book Period</p>
                    <input type="text" class="text text-date text-date-from text-small form-control" name="pfrom[]" value="" placeholder="Date" readonly>
                    <span class="subtitle">to</span>
                </div>
                <div class="form-group">
                    <p class="subtitle">&nbsp;</p>
                    <input type="text" class="text text-date text-date-to text-small form-control" name="pto[]" value="" placeholder="Date" readonly>
                </div>
                <div class="form-group">
                    <p class="subtitle">Get Discount</p>
                    <input type="text" class="text text-small form-control" name="ddisc[]" value="0" >
                </div>
                <div class="form-group">
                    <p class="subtitle">&nbsp;</p>
                    <select name="dtype[]" autocomplete="off">
                        <option value="0">%</option>
                        <option value="1">Rp.</option>
                    </select>
                </div>
                <div class="form-group">
                    <p class="subtitle">Notif</p>
                    <input type="text" class="text text-medium form-control" name="rtnotif[]" value="" >
                </div>
            </div>';
        }
    }

    return $item;
}

/*
| -------------------------------------------------------------------------------------
| Add New Applicable Route Options
| -------------------------------------------------------------------------------------
*/
function add_rate_route_options()
{
    return '
    <div class="form-group-block">
        <select class="select-option route-list" name="rid[]" autocomplete="off">
            ' . get_route_option() . '
        </select>
        <a class="delete-route" data-id="">
            <img src="../l-plugins/ticket/images/delete.svg">
        </a>
    </div>';
}

/*
| -------------------------------------------------------------------------------------
| Add New Rate Options
| -------------------------------------------------------------------------------------
*/
function add_rate_options()
{
    if( $_POST['rtype']=='0' )
    {
        return '
        <div class="include-transport-block">
            <label>Rate (Include Transport)</label>
            <div class="form-group">
                <p class="subtitle">Adult</p>
                <input type="text" class="text text-small form-control" name="rinclude[adult]" value="0" >
            </div>
            <div class="form-group">
                <p class="subtitle">Child</p>
                <input type="text" class="text text-small form-control" name="rinclude[child]" value="0" >
            </div>
            <div class="form-group">
                <p class="subtitle">Infant</p>
                <input type="text" class="text text-small form-control" name="rinclude[infant]" value="0" >
            </div>
        </div>
        <div class="exclude-transport-block">
            <label>Rate (Exclude Transport)</label>
            <div class="form-group">
                <p class="subtitle">Adult</p>
                <input type="text" class="text text-small form-control" name="rexclude[adult]" value="0" >
            </div>
            <div class="form-group">
                <p class="subtitle">Child</p>
                <input type="text" class="text text-small form-control" name="rexclude[child]" value="0" >
            </div>
            <div class="form-group">
                <p class="subtitle">Infant</p>
                <input type="text" class="text text-small form-control" name="rexclude[infant]" value="0" >
            </div>
        </div>';
    }
    else if( $_POST['rtype']=='1' )
    {
        if( !empty( $_POST['rid'] ) )
        {
            $rate_opt = '';

            foreach( $_POST['rid'] as $obj )
            {
                //-- Get Departure, Hopping & Arrival Point
                $dep = get_route_departure_point_list( $obj );
                $rtn = get_route_arrival_point_list( $obj );

                if( !empty( $dep ) && !empty( $rtn ) )
                {
                    $dt = get_route( $obj );

                    $rate_opt .= '
                    <div class="rate-options-wrap">
                        <h3>' . $dt['rname'] . '</h3>';

                            if( isset( $_POST['rtid'] ) )
                            {
                                global $db;

                                $exist_rate = array();

                                $s2 = 'SELECT
                                            c.lcid,
                                            c.lcid_to,
                                            b.rid,
                                            IFNULL(c.rtiadult, 0) as rtiadult,
                                            IFNULL(c.rtichild, 0) as rtichild,
                                            IFNULL(c.rtiinfant, 0) as rtiinfant,
                                            IFNULL(c.rteadult, 0) as rteadult,
                                            IFNULL(c.rtechild, 0) as rtechild,
                                            IFNULL(c.rteinfant, 0) as rteinfant
                                       FROM ticket_rate AS a
                                       LEFT JOIN ticket_rate_detail AS b ON b.rtid = a.rtid
                                       LEFT JOIN ticket_rate_route_detail AS c ON c.rtdid = b.rtdid
                                       WHERE a.rtid = %d';
                                $q2 = $db->prepare_query( $s2, $_POST['rtid'] );
                                $r2 = $db->do_query( $q2 );

                                if( $db->num_rows( $r2 ) > 0 )
                                {
                                    while( $d2 = $db->fetch_array( $r2 ) )
                                    {
                                        $exist_rate[ $d2['rid'] ][ $d2['lcid'] . '|' . $d2['lcid_to'] ]['rinclude_adult'] = $d2['rtiadult'];
                                        $exist_rate[ $d2['rid'] ][ $d2['lcid'] . '|' . $d2['lcid_to'] ]['rinclude_child'] = $d2['rtichild'];
                                        $exist_rate[ $d2['rid'] ][ $d2['lcid'] . '|' . $d2['lcid_to'] ]['rinclude_infant'] = $d2['rtiinfant'];
                                        $exist_rate[ $d2['rid'] ][ $d2['lcid'] . '|' . $d2['lcid_to'] ]['rexclude_adult'] = $d2['rteadult'];
                                        $exist_rate[ $d2['rid'] ][ $d2['lcid'] . '|' . $d2['lcid_to'] ]['rexclude_child'] = $d2['rtechild'];
                                        $exist_rate[ $d2['rid'] ][ $d2['lcid'] . '|' . $d2['lcid_to'] ]['rexclude_infant'] = $d2['rteinfant'];
                                    }
                                }
                            }

                            foreach( $dep as $key => $d )
                            {
                                foreach( $rtn as $key2 => $d2 )
                                {
                                    if( $d['lcid'] == $d2['lcid'] )
                                    {
                                        continue;
                                    }

                                    $rate_opt .= '
                                    <div class="inner">
                                        <h4>From ' . $d['lcname'] . ' to ' . $d2['lcname'] . '</h4>
                                        <div class="include-transport-block">
                                            <label>Rate (Include Transport)</label>
                                            <div class="form-group">
                                                <p class="subtitle">Adult</p>
                                                <input type="text" class="text text-small form-control" name="rinclude[' . $d['rid'] . '][' . $d['lcid'] . '|' . $d2['lcid'] . '][adult]" value="' . ( isset( $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rinclude_adult'] ) ? $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rinclude_adult'] : 0  ) . '" >
                                            </div>
                                            <div class="form-group">
                                                <p class="subtitle">Child</p>
                                                <input type="text" class="text text-small form-control" name="rinclude[' . $d['rid'] . '][' . $d['lcid'] . '|' . $d2['lcid'] . '][child]" value="' . ( isset( $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rinclude_child'] ) ? $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rinclude_child'] : 0  ) . '" >
                                            </div>
                                            <div class="form-group">
                                                <p class="subtitle">Infant</p>
                                                <input type="text" class="text text-small form-control" name="rinclude[' . $d['rid'] . '][' . $d['lcid'] . '|' . $d2['lcid'] . '][infant]" value="' . ( isset( $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rinclude_infant'] ) ? $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rinclude_infant'] : 0  ) . '" >
                                            </div>
                                        </div>
                                        <div class="exclude-transport-block">
                                            <label>Rate (Exclude Transport)</label>
                                            <div class="form-group">
                                                <p class="subtitle">Adult</p>
                                                <input type="text" class="text text-small form-control" name="rexclude[' . $d['rid'] . '][' . $d['lcid'] . '|' . $d2['lcid'] . '][adult]" value="' . ( isset( $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rexclude_adult'] ) ? $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rexclude_adult'] : 0  ) . '" >
                                            </div>
                                            <div class="form-group">
                                                <p class="subtitle">Child</p>
                                                <input type="text" class="text text-small form-control" name="rexclude[' . $d['rid'] . '][' . $d['lcid'] . '|' . $d2['lcid'] . '][child]" value="' . ( isset( $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rexclude_child'] ) ? $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rexclude_child'] : 0  ) . '" >
                                            </div>
                                            <div class="form-group">
                                                <p class="subtitle">Infant</p>
                                                <input type="text" class="text text-small form-control" name="rexclude[' . $d['rid'] . '][' . $d['lcid'] . '|' . $d2['lcid'] . '][infant]" value="' . ( isset( $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rexclude_infant'] ) ? $exist_rate[ $d['rid'] ][ $d['lcid'] . '|' . $d2['lcid'] ]['rexclude_infant'] : 0  ) . '" >
                                            </div>
                                        </div>
                                        <div class="sr-only">
                                            <input type="text" name="lcid[' . $d['rid'] . '][]" value="' . $d['lcid'] . '|' . $d2['lcid'] . '" />
                                        </div>
                                    </div>';
                                }
                            }

                        $rate_opt .= '
                    </div>';
                }
            }

            return $rate_opt;
        }
    }
    else
    {
        return '';
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Departure & Hopping Route Point
| -------------------------------------------------------------------------------------
*/
function get_route_departure_point_list( $rid = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_route AS a
          INNER JOIN ticket_route_detail AS b ON b.rid = a.rid
          INNER JOIN ticket_location AS c ON b.lcid = c.lcid
          WHERE b.rdtype IN ( %s, %s ) AND a.rid = %d ORDER BY b.rdid';
    $q = $db->prepare_query( $s, '0', '2', $rid );
    $r = $db->do_query( $q );

    $point = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $point[] = $d;
        }
    }

    return $point;
}

/*
| -------------------------------------------------------------------------------------
| Get Arrival & Hopping Route Point
| -------------------------------------------------------------------------------------
*/
function get_route_arrival_point_list( $rid = '' )
{
    global $db;

    $s = 'SELECT * FROM ticket_route AS a
          INNER JOIN ticket_route_detail AS b ON b.rid = a.rid
          INNER JOIN ticket_location AS c ON b.lcid = c.lcid
          WHERE b.rdtype IN ( %s, %s ) AND a.rid = %d ORDER BY b.rdid';
    $q = $db->prepare_query( $s, '2', '1', $rid );
    $r = $db->do_query( $q );

    $point = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $point[] = $d;
        }
    }

    return $point;
}

function add_seat_discount()
{
    return '
    <div class="item">
        <div class="form-group">
            <span class="subtitle">For more than </span>
            <input class="text text-small form-control" type="text" name="seat[]" value="0" />
        </div>
        <div class="form-group">
            <span class="subtitle">seat left, discount </span>
            <input class="text text-small form-control" type="text" name="sdisc[]" value="0" />
        </div>
        <div class="form-group">
            <select name="stype[]" autocomplete="off">
                <option value="0">%</option>
                <option value="1">Rp.</option>
            </select>
        </div>
        <div class="form-group">
            <span class="subtitle">Notif </span>
            <input type="text" class="text text-large form-control" name="notif[]" value="" >
        </div>
    </div>';
}

function add_early_discount()
{
    return '
    <div class="item">
        <div class="form-group">
            <p class="subtitle">Book Period</p>
            <input type="text" class="text text-date text-date-from text-small form-control" name="pfrom[]" value="" placeholder="Date">
            <span class="subtitle">to</span>
        </div>
        <div class="form-group">
            <p class="subtitle">&nbsp;</p>
            <input type="text" class="text text-date text-date-to text-small form-control" name="pto[]" value="" placeholder="Date">
        </div>
        <div class="form-group">
            <p class="subtitle">Get Discount</p>
            <input type="text" class="text text-small form-control" name="ddisc[]" value="0" >
        </div>
        <div class="form-group">
            <p class="subtitle">&nbsp;</p>
            <select name="dtype[]" autocomplete="off">
                <option value="0">%</option>
                <option value="1">Rp.</option>
            </select>
        </div>
        <div class="form-group">
            <p class="subtitle">Notif</p>
            <input type="text" class="text text-large form-control" name="rtnotif[]" value="" >
        </div>
    </div>';
}

/*
| -------------------------------------------------------------------------------------
| Rate Filter Functions
| -------------------------------------------------------------------------------------
*/
function ticket_filter_rate()
{
    $filter = array( 'rid' => '', 'rpid' => '' );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array( 'rid' => isset( $rid ) ? $rid : '', 'rpid' => isset( $rpid ) ? $rpid : '' );
    }

    return $filter;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_rate_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-rate' )
    {
        $d = delete_rate( $_POST['rtid'], true );

        if( $d === true )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( $d );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-applicable-route' )
    {
        $d = delete_rate_detail( $_POST['rtdid'], true );

        if( $d === true )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( $d );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-route-options' )
    {
        echo add_rate_route_options();
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-rate-options' )
    {
        echo add_rate_options();
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-seat-disc' )
    {
        echo add_seat_discount();
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-early-disc' )
    {
        echo add_early_discount();
    }
}

/*
| -------------------------------------------------------------------------------------
| Log | Data Old
| -------------------------------------------------------------------------------------
*/
function rate_data_all( $rtid )
{
    global $db;

    $data = array();

    $s = 'SELECT * FROM ticket_rate WHERE rtid=%d';
    $q = $db->prepare_query( $s, $rtid );
    $r = $db->do_query( $q );

    $data = $db->fetch_array( $r );

    if ( !empty( $data ) ) 
    {
        $srd = 'SELECT * FROM ticket_rate_detail WHERE rtid=%d';
        $qrd = $db->prepare_query( $srd, $rtid );
        $rrd = $db->do_query( $qrd );

        if ( $db->num_rows( $rrd ) ) 
        {
            while ( $d = $db->fetch_array( $rrd ) ) 
            {
                $data['detail'] = $d;

                $rrrd = $db->do_query( 'SELECT * FROM ticket_rate_route_detail WHERE rtdid='.$d['rtdid'] );

                if ( $db->num_rows( $rrrd ) ) 
                {
                    while ( $rr = $db->fetch_array( $rrrd ) ) 
                    {
                        $data['detail'][ $d['rtdid'] ]['rate_route_detail'][ $rr['rtrid'] ] = $rr;
                    }
                }
            }
        }
    }

    return $data;
}