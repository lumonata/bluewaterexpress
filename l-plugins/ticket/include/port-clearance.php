<?php

add_actions( 'port-clearance-report', 'ticket_port_clearance_passenger' );
add_actions( 'passenger-list', 'ticket_port_clearance_passenger' );
add_actions( 'port-clearance-doc', 'ticket_port_clearance_doc' );
add_actions( 'ticket-port-clearance-ajax_page', 'ticket_port_clearance_ajax' );

/*
| -------------------------------------------------------------------------------------
| Port Clearance - Passenger List
| -------------------------------------------------------------------------------------
*/
function ticket_port_clearance_passenger()
{
    if( is_add_new() )
    {
        return ticket_add_to_port_clearance();
    }

    return ticket_port_clearance_passenger_table_data();
}

/*
| -------------------------------------------------------------------------------------
| Port Clearance - Passenger List Table
| -------------------------------------------------------------------------------------
*/
function ticket_port_clearance_passenger_table_data( $message = array() )
{
    if( isset( $_POST ) && !empty( $_POST ) && empty( $message ) )
    {
        header( 'Location:' . get_state_url( 'port-clearance-report&sub=passenger-list&prm=' . base64_encode( json_encode( $_POST ) ) ) );
    }

    $filter   = get_filter_port_clearance();
    $site_url = site_url();

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/port-clearance/list.html', 'port-clearance' );

    add_block( 'list-block', 'lblock', 'port-clearance' );
   
    add_variable( 'date', $bddate );
    add_variable( 'title', 'Port Clearance - Passenger List' );
    add_variable( 'message', generate_message_block( $message ) );
    add_variable( 'trip_option', get_trip_option( $tid, true, 'All Trip') );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );

    add_variable( 'limit', post_viewed() );
    add_variable( 'state', get_state_url( 'port-clearance-report&sub=passenger-list' ) );
    add_variable( 'action', get_state_url( 'port-clearance-report&sub=passenger-list&prc=add_new' ) );
    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-port-clearance-ajax/' );

    add_actions( 'section_title', 'Port Clearance - Passenger List' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'list-block', 'lblock', false );

    return return_template( 'port-clearance' );
}

/*
| -------------------------------------------------------------------------------------
| Port Clearance - Passenger Table Query
| -------------------------------------------------------------------------------------
*/
function ticket_port_clearance_passenger_list_data( $rid, $tid, $bddate )
{
    global $db;

    $rdata = $_REQUEST;

    if( empty( $rdata['search']['value'] ) )
    {
        $w = '';

        if ( $tid != '' ) 
        {
            list( $from, $to ) = explode( ' - ', $tid );

            $w .= $db->prepare_query( ' AND b.bdfrom = %s AND b.bdto = %s', $from, $to );
        }

        if( $rid != '' )
        {
            $w .= $db->prepare_query( ' AND b.rid = %d', $rid );
        }
        
        if( $bddate != '' )
        {
            $w .= $db->prepare_query( ' AND b.bddate = %s', date( 'Y-m-d', strtotime( $bddate ) ) );
        }
        
        $s = 'SELECT
                b.rid,
                d.rname,
                c.bpid,
                c.bptype,
                c.bpname,
                c.bpgender,
                c.bpbirthdate,
                c.lcountry_id,
                b.bdfrom,
                b.bdto,
                b.bddate,
                TIMESTAMPDIFF(
                    YEAR, c.bpbirthdate, CURDATE()
                ) AS age
              FROM ticket_booking AS a
              LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
              LEFT JOIN ticket_booking_passenger AS c ON c.bdid = b.bdid
              LEFT JOIN ticket_route AS d ON b.rid = d.rid
              LEFT JOIN l_country AS e ON c.lcountry_id = e.lcountry_id
              WHERE b.bdrevstatus = "Devinitive" 
              AND c.bpid NOT IN ( SELECT bpid FROM ticket_port_clearance_detail ) 
              AND b.bdpstatus NOT IN( "cr", "cn" ) 
              AND a.bstt <> "ar" ' . $w;
        $q = $db->prepare_query( $s );
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );
       
        $s2 = $s . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $s2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        while ( $d = $db->fetch_array( $r2 ) ) 
        {
            $data[] = array(
                'id'          => $d['bpid'],
                'rid'         => $d['rid'],
                'age'         => $d['age'],
                'port'        => $d['bdfrom'],
                'route'       => $d['rname'],
                'guest_name'  => $d['bpname'],
                'guest_type'  => $d['bptype'],
                'lcountry_id' => $d['lcountry_id'],
                'trip'        => $d['bdfrom'] . ' - ' . $d['bdto'],
                'is_adult'    => $d['bptype'] == 'adult' ? '√' : '-',
                'is_child'    => $d['bptype'] == 'child' ? '√' : '-',
                'is_infant'   => $d['bptype'] == 'infant' ? '√' : '-',
                'gender'      => $d['bpgender'] == '1' ? 'Male' : 'Female',
                'bddate'      => date( 'd F Y', strtotime( $d['bddate'] ) ),
                'nationality' => get_nationality( $d['lcountry_id'], 'lcountry_code' ),
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw'              => intval( $rdata['draw'] ),
        'recordsTotal'      => intval( $n ),
        'recordsFiltered'   => intval( $n ),
        'data'              => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Add Passenger To Port Clearance Form
| -------------------------------------------------------------------------------------
*/
function ticket_add_to_port_clearance()
{
    $message = run_save_port_clearance();

    if( isset( $_POST['pdata'] ) )
    {
        $detail = is_valid_port_clearance( $_POST['pdata'] );

        if( empty( $detail ) )
        {
            $message = array( 'type' => 'error', 'content' => array( 'Some of passenger data not valid, please choose data with same route and departure date' ) ) ;

            return ticket_port_clearance_passenger_table_data( $message );
        }
        else
        {
            global $db;

            extract( $detail );

             $s = 'SELECT
                    c.bpid,
                    c.bptype,
                    c.bpname,
                    c.bpgender,
                    c.bpbirthdate,
                    c.lcountry_id,
                    TIMESTAMPDIFF(
                        YEAR, c.bpbirthdate, CURDATE()
                    ) AS age
                  FROM ticket_booking AS a
                  LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
                  LEFT JOIN ticket_booking_passenger AS c ON c.bdid = b.bdid
                  LEFT JOIN ticket_route AS d ON b.rid = d.rid
                  LEFT JOIN l_country AS e ON c.lcountry_id = e.lcountry_id
                  WHERE a.bstt <> %s AND c.bpid IN (' . implode( ',', $bpid ) . ')';
            $q = $db->prepare_query( $s, 'ar' );
            $r = $db->do_query( $q );

            $captain = empty( $_POST['captain'] ) ? '' : $_POST['captain'];
            $kkm     = empty( $_POST['kkm'] ) ? '' : $_POST['kkm'];
            $crew    = empty( $_POST['crew'] ) ? '' : $_POST['crew'];
            $boid    = empty( $_POST['boid'] ) ? '' : $_POST['boid'];

            set_template( PLUGINS_PATH . '/ticket/tpl/port-clearance/form.html', 'port-clearance' );
            add_block( 'loop-block', 'lplock', 'port-clearance' );
            add_block( 'form-block', 'lblock', 'port-clearance' );

            add_variable( 'message', generate_message_block( $message ) );

            add_variable( 'pcid', '' );
            add_variable( 'pcdate', $date );
            add_variable( 'pctrip', $trip );
            add_variable( 'pcport', $port );
            add_variable( 'pcroute', $route );

            add_variable( 'captain', $captain );
            add_variable( 'kkm', $kkm );
            add_variable( 'crew', $crew );

            add_variable( 'port', $port );
            add_variable( 'trip', $trip );
            add_variable( 'boat_option', get_boat_option( $boid ) );
            add_variable( 'rname', get_route( $route, 'rname' ) );
            add_variable( 'date', date( 'd, F Y', strtotime( $date ) ) );

            $no = 1;

            while( $d = $db->fetch_array( $r ) )
            {
                add_variable( 'no', $no );
                add_variable( 'age', $d['age'] );
                add_variable( 'bpid', $d['bpid'] );
                add_variable( 'bptype', $d['bptype'] );
                add_variable( 'bbname', $d['bpname'] );
                add_variable( 'country_id', $d['lcountry_id'] );
                add_variable( 'is_adult', $d['bptype'] == 'adult' ? '√' : '-' );
                add_variable( 'is_child', $d['bptype'] == 'child' ? '√' : '-' );
                add_variable( 'is_infant', $d['bptype'] == 'infant' ? '√' : '-' );
                add_variable( 'gender', $d['bpgender'] == '1' ? 'Male' : 'Female' );
                add_variable( 'nationality', get_nationality( $d['lcountry_id'], 'lcountry_code' ) );

                parse_template( 'loop-block', 'lplock', true );

                $no++;
            }

            add_variable( 'title', 'Add to Port Clearance' );
            add_variable( 'publish_title', 'Save to Post Clearance List' );
            add_variable( 'print_title', 'Save to Post Clearance List And Print' );
            add_variable( 'action', get_state_url( 'port-clearance-report&sub=passenger-list&prc=add_new' ) );
            add_variable( 'cancel_link', get_state_url( 'port-clearance-report&sub=passenger-list' ) );
            add_variable( 'ajax_url', HTSERVER . site_url() . '/ticket-port-clearance-ajax/' );
            add_variable( 'delete_class', 'sr-only' );

            parse_template( 'form-block', 'lblock', false );
            
            add_actions( 'section_title', 'Add Port Clearance' );
            add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

            return return_template( 'port-clearance' );
        }
    }
    else
    {
        $message = array( 'type' => 'error', 'content' => array( 'Please select one passenger from the list, by checked the checkbox field' ) ) ;

        return ticket_port_clearance_passenger_table_data( $message );
    }
}

/*
| -------------------------------------------------------------------------------------
| Port Clearance Document List
| -------------------------------------------------------------------------------------
*/
function ticket_port_clearance_doc()
{
    if( is_detail() )
    {
        if( isset( $_GET['id'] ) )
        {
            if( is_add_more_port_clearance_passenger() )
            {
                return ticket_add_more_port_clearance_passenger();
            }
            else
            {
                return ticket_view_port_clearance_doc();
            }
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) )
        {
            return ticket_edit_port_clearance_doc();
        }
        else
        {
            return not_found_template();
        }
    }

    return ticket_port_clearance_doc_table_data();
}

/*
| -------------------------------------------------------------------------------------
| Port Clearance Document List Table
| -------------------------------------------------------------------------------------
*/
function ticket_port_clearance_doc_table_data()
{
    if( isset( $_POST ) && !empty( $_POST )  )
    {
        header( 'Location:' . get_state_url( 'port-clearance-report&sub=port-clearance-doc' ) . '&prm=' . base64_encode( json_encode( $_POST ) ) );
    }

    $filter   = get_filter_port_clearance_doc();
    $site_url = site_url();

    extract( $filter );

    set_template( PLUGINS_PATH . '/ticket/tpl/port-clearance/pc-list.html', 'port-clearance' );

    add_block( 'list-block', 'lblock', 'port-clearance' );

    add_variable( 'date', $bddate );
    add_variable( 'limit', post_viewed() );
    add_variable( 'title', 'Port Clearance Documents' );
    add_variable( 'date_placeholder', date( 'd F Y', time() ) );
    add_variable( 'trip_option', get_trip_option( $tid, true, 'All Trip') );
    add_variable( 'route_option', get_route_option( $rid, true, 'All Route') );

    add_variable( 'ajax_url', HTSERVER . $site_url . '/ticket-port-clearance-ajax/' );
    add_variable( 'state', get_state_url( 'port-clearance-report&sub=port-clearance-doc' ) );
    
    add_actions( 'section_title', 'Port Clearance Documents' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'list-block', 'lblock', false );

    return return_template( 'port-clearance' );
}

function ticket_port_clearance_doc_table_query( $tid = '', $rid = '', $bddate = '' )
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array( 
        0 => 'a.pcdate',
        1 => 'a.pcport',
        2 => 'a.pctrip',
        3 => 'a.pcboat',
        4 => 'a.pcroute'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'a.pcdate DESC';
    }

    $w = array();

    if( $tid != '' )
    {
        $w[] = $db->prepare_query( ' a.pctrip = %s', $tid );
    }
    
    if( $rid != '' )
    {
        $w[] = $db->prepare_query( ' a.rid = %d', $rid );
    }

    if( empty( $bddate ) )
    {
        $w[] = $db->prepare_query( ' a.pcdate BETWEEN %s AND %s', date( 'Y-m-d', time() - 2592000 ), date( 'Y-m-d' ) );
    }
    else
    {
        $w[] = $db->prepare_query( ' a.pcdate = %s', date( 'Y-m-d', strtotime( $bddate ) ) );
    }

    if( empty( $w ) )
    {
        $s = 'SELECT * FROM ticket_port_clearance as a ORDER BY ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $q = 'SELECT * FROM ticket_port_clearance as a WHERE ' . implode( ' AND', $w ) . ' ORDER BY ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $prm = base64_encode( json_encode( array( 'rid' => $rid, 'tid' => $tid, 'bddate' => $bddate ) ) );

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'pcport'  => $d2['pcport'],
                'pctrip'  => $d2['pctrip'],
                'pcboat'  => $d2['pcboat'],
                'pcroute' => $d2['pcroute'],
                'pcdate'  => date( 'd F Y', strtotime( $d2['pcdate'] ) ),
                'vlink'   => get_state_url( 'port-clearance-report&sub=port-clearance-doc&prc=detail&id=' . $d2['pcid'] . '&prm=' . $prm )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Port Clearance Document List Table Data
| -------------------------------------------------------------------------------------
*/
function ticket_view_port_clearance_doc()
{
    $param = isset( $_GET['prm'] ) ? '&prm=' . $_GET['prm'] : '';
    $data  = get_port_clearance( $_GET['id'] );

    if( !empty( $data ) )
    {
        extract( $data );

        set_template( PLUGINS_PATH . '/ticket/tpl/port-clearance/pc-detail.html', 'port-clearance' );
        add_block( 'loop-block', 'lplock', 'port-clearance' );
        add_block( 'detail-block', 'lblock', 'port-clearance' );

        add_variable( 'kkm', $kkm );
        add_variable( 'pcid', $pcid );
        add_variable( 'port', $pcport );
        add_variable( 'trip', $pctrip );
        add_variable( 'boat', $pcboat );
        add_variable( 'rname', $pcroute );
        add_variable( 'captain', $captain );
        add_variable( 'date', date( 'd, F Y', strtotime( $pcdate ) ) );
        add_variable( 'crew', str_replace( '<br />', ', ', nl2br( $crew ) ) );

        if( isset( $detail ) )
        {
            $number  = 1;

            foreach( $detail as $d )
            {
                add_variable( 'no', $number );
                add_variable( 'bpid', $d['bpid'] );
                add_variable( 'pcdid', $d['pcdid'] );
                add_variable( 'age', $d['guest_age'] );
                add_variable( 'bptype', $d['guest_type'] );
                add_variable( 'bbname', $d['guest_name'] );
                add_variable( 'country_id', $d['lcountry_id'] );
                add_variable( 'is_adult', $d['guest_type'] == 'adult' ? '√' : '-' );
                add_variable( 'is_child', $d['guest_type'] == 'child' ? '√' : '-' );
                add_variable( 'is_infant', $d['guest_type'] == 'infant' ? '√' : '-' );
                add_variable( 'gender', $d['guest_gender'] );
                add_variable( 'nationality', $d['guest_country'] );

                parse_template( 'loop-block', 'lplock', true );

                $number++;
            }
        }
        else
        {
            add_variable( 'empty_txt', 'No data guest' );
            
            parse_template( 'empty-block', 'emlock', true );
        }

        add_variable( 'title', 'Port Clearance' );
        add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'port-clearance' ) );

        add_variable( 'site_url', HTSERVER . site_url() );
        add_variable( 'ajax_url', HTSERVER . site_url() . '/ticket-port-clearance-ajax/' );
        add_variable( 'cancel_link', get_state_url( 'port-clearance-report&sub=port-clearance-doc' . $param ) );
        add_variable( 'edit_link', get_state_url( 'port-clearance-report&sub=port-clearance-doc&prc=edit&id=' . $pcid . $param  ) );

        parse_template( 'detail-block', 'lblock', false );
        
        add_actions( 'section_title', 'Edit Port Clearance' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        return return_template( 'port-clearance' );
    }
    else
    {
        return not_found_template();
    }
}

/*
| -------------------------------------------------------------------------------------
| Port Clearance Document Edit Passenger
| -------------------------------------------------------------------------------------
*/
function ticket_edit_port_clearance_doc()
{
    $message = run_edit_port_clearance();
    $data    = get_port_clearance( $_GET['id'] );
    $param   = isset( $_GET['prm'] ) ? '&prm=' . $_GET['prm'] : '';

    if( !empty( $data ) )
    {
        extract( $data );

        set_template( PLUGINS_PATH . '/ticket/tpl/port-clearance/pc-form.html', 'port-clearance' );
        add_block( 'loop-block', 'lplock', 'port-clearance' );
        add_block( 'empty-block', 'emlock', 'port-clearance' );
        add_block( 'form-block', 'lblock', 'port-clearance' );
        add_block( 'more-block', 'mblock', 'port-clearance' );

        add_variable( 'message', generate_message_block( $message ) );

        add_variable( 'pcid', $pcid );
        add_variable( 'pcdate', $pcdate );
        add_variable( 'pctrip', $pctrip );
        add_variable( 'pcport', $pcport );
        add_variable( 'pcroute', $rid );

        add_variable( 'captain', $captain );
        add_variable( 'kkm', $kkm );
        add_variable( 'crew', $crew );

        add_variable( 'port', $pcport );
        add_variable( 'trip', $pctrip );
        add_variable( 'rname', $pcroute );
        add_variable( 'boat_option', get_boat_option( $boid ) );
        add_variable( 'date', date( 'd, F Y', strtotime( $pcdate ) ) );

        if( isset( $detail ) )
        {
            $no = 1;
            
            foreach( $detail as $d )
            {
                add_variable( 'no', $no );
                add_variable( 'bpid', $d['bpid'] );
                add_variable( 'pcdid', $d['pcdid'] );
                add_variable( 'age', $d['guest_age'] );
                add_variable( 'bptype', $d['guest_type'] );
                add_variable( 'bbname', $d['guest_name'] );
                add_variable( 'country_id', $d['lcountry_id'] );
                add_variable( 'is_adult', $d['guest_type'] == 'adult' ? '√' : '-' );
                add_variable( 'is_child', $d['guest_type'] == 'child' ? '√' : '-' );
                add_variable( 'is_infant', $d['guest_type'] == 'infant' ? '√' : '-' );
                add_variable( 'gender', $d['guest_gender'] );
                add_variable( 'nationality', $d['guest_country'] );

                parse_template( 'loop-block', 'lplock', true );

                $no++;
            }
        }
        else
        {
            add_variable( 'empty_txt', 'No data guest' );

            parse_template( 'empty-block', 'emlock', true );
        }

        add_variable( 'delete_class', '' );
        add_variable( 'title', 'Edit Port Clearance' );
        add_variable( 'publish_title', 'Edit Post Clearance' );
        add_variable( 'print_title', 'Edit Clearance And Print' );

        add_variable( 'ajax_url', HTSERVER . site_url() . '/ticket-port-clearance-ajax/' );
        add_variable( 'table_link', get_state_url( 'port-clearance-report&sub=port-clearance-doc' ) );
        add_variable( 'action', get_state_url( 'port-clearance-report&sub=port-clearance-doc&prc=edit&id=' . $pcid ) );
        add_variable( 'cancel_link', get_state_url( 'port-clearance-report&sub=port-clearance-doc&prc=detail&id=' . $pcid . $param ) );
        
        add_actions( 'section_title', 'Edit Port Clearance' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        parse_template( 'form-block', 'lblock', false );

        return return_template( 'port-clearance' );
    }
    else
    {
        return not_found_template();
    }
}

/*
| -------------------------------------------------------------------------------------
| Port Clearance Document Add More Passenger
| -------------------------------------------------------------------------------------
*/
function ticket_add_more_port_clearance_passenger()
{        
    $message = run_add_more_port_clearance_passenger();
    $data    = get_port_clearance( $_GET['id'] );
    $param   = isset( $_GET['prm'] ) ? '&prm=' . $_GET['prm'] : '';

    if( !empty( $data ) )
    {
        extract( $data );

        set_template( PLUGINS_PATH . '/ticket/tpl/port-clearance/pc-form.html', 'port-clearance' );

        add_block( 'form-block', 'lblock', 'port-clearance' );
        add_block( 'more-block', 'mblock', 'port-clearance' );

        add_variable( 'rid', $rid );
        add_variable( 'kkm', $kkm );
        add_variable( 'pcid', $pcid );
        add_variable( 'crew', $crew );
        add_variable( 'pcboat', $pcboat );
        add_variable( 'pctrip', $pctrip );
        add_variable( 'pcport', $pcport );
        add_variable( 'pcdate', $pcdate );
        add_variable( 'captain', $captain );
        add_variable( 'pcroute', $pcroute );
        add_variable( 'limit', post_viewed() );
        add_variable( 'date', date( 'd, F Y', strtotime( $pcdate ) ) );

        add_variable( 'title', 'Add More Guest To Port Clearance' );
        add_variable( 'message', generate_message_block( $message ) );

        add_variable( 'ajax_url', HTSERVER . site_url() . '/ticket-port-clearance-ajax/' );
        add_variable( 'action', get_state_url( 'port-clearance-report&sub=port-clearance-doc&prc=detail&id=' . $pcid . $param ) );
        add_variable( 'cancel_link', get_state_url( 'port-clearance-report&sub=port-clearance-doc&prc=detail&id=' . $pcid . $param ) );
        
        add_actions( 'section_title', 'Port Clearance Add More Guest' );
        add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

        parse_template( 'more-block', 'mblock', false );

        return return_template( 'port-clearance' );
    }
    else
    {
        return not_found_template();
    }
}

/*
| -------------------------------------------------------------------------------------
| Execute Save Port Clearance Form
| -------------------------------------------------------------------------------------
*/
function run_save_port_clearance()
{
    if( is_save_draft() || is_publish() || is_print() )
    {
        $error = validate_port_clearance_data();

        if( empty( $error ) )
        {
            if( is_exist_in_port_clearance( $_POST['bpid'] ) )
            {
                return array( 'type'=> 'error', 'content' => array( 'Sorry this passenger already added to list before' ) );
            }
            else
            {
                $id = save_port_clearance();

                if( empty( $id ) )
                {
                    return array( 'type'=> 'error', 'content' => array( 'Sorry can\'t add passenger to port clearance' ) );
                }
                else
                {
                    if( is_print() )
                    {
                        header( 'location:' . get_state_url( 'port-clearance-report&sub=port-clearance-doc&prc=detail&id=' . '&print=true' ) );
                    }
                    else
                    {
                        header( 'location:' . get_state_url( 'port-clearance-report&sub=port-clearance-doc&prc=detail&id=' . $id ) );
                    }
                }
            }
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Execute Edit Port Clearance Form
| -------------------------------------------------------------------------------------
*/
function run_edit_port_clearance()
{
    if( is_save_draft() || is_publish() )
    {
        $error = validate_port_clearance_data();

        if( empty( $error ) )
        {
            if( update_port_clearance() )
            {
                return array( 'type'=> 'success', 'content' => array( 'This port clearance has been successfully edited' ) );
            }
            else
            {
                return array( 'type'=> 'error', 'content' => array( 'Sorry can\'t edit this port clearance' ) );
            }
        }
        else
        {
            return array( 'type'=> 'error', 'content' => $error );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Execute Add More Passenger Port Clearance Form
| -------------------------------------------------------------------------------------
*/
function run_add_more_port_clearance_passenger()
{
    global $flash;

    if( is_save_draft() || is_more_publish() )
    {
        $error = array();

        extract( $_POST );

        if( isset( $pdata ) && empty( $pdata ) )
        {
            $error[] = 'Guest can\'t be empty';
        }

        if( empty( $error ) )
        {
            if( update_port_clearance_passenger() )
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New passenger successfully added into this port clearance' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Sorry can\'t added new passenger to this port clearance' ) ) );
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Validate Port Clearance
| -------------------------------------------------------------------------------------
*/
function validate_port_clearance_data()
{    
    $error = array();

    extract( $_POST );

    if( isset( $boid ) && empty( $boid ) )
    {
        $error[] = 'Boat name can\'t be empty';
    }

    if( isset( $captain ) && empty( $captain ) )
    {
        $error[] = 'Captain name can\'t be empty';
    }

    if( isset( $kkm ) && empty( $kkm ) )
    {
        $error[] = 'KKM can\'t be empty';
    }

    if( isset( $crew ) && empty( $crew ) )
    {
        $error[] = 'Crew can\'t be empty';
    }

    if( isset( $bid ) && empty( $bid ) )
    {
        $error[] = 'Guest can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Save Port Clearance
| -------------------------------------------------------------------------------------
*/
function save_port_clearance()
{
    global $db;

    $rname  = get_route( $_POST['pcroute'], 'rname' );
    $pcdate = date( 'Y-m-d', strtotime( $_POST['pcdate'] ) );

    $s = 'INSERT INTO ticket_port_clearance( 
            rid,
            boid,
            pcroute,
            captain,
            kkm,
            crew,
            pcdate, 
            pcport, 
            pctrip, 
            pcboat,
            pccreateddate, 
            luser_id ) 
          VALUES( %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s, 
            $_POST['pcroute'], 
            $_POST['boid'],
            $rname,
            $_POST['captain'], 
            $_POST['kkm'],
            $_POST['crew'],
            $pcdate, 
            $_POST['pcport'],
            $_POST['pctrip'],
            get_boat( $_POST['boid'], 'boname' ),
            date( 'Y-m-d H:i:s' ),
            $_COOKIE['user_id'] );
       
    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();
        
        save_port_clearance_detail( $id );

        save_log( $id, 'port-clearance', 'Add new port clearance<br/>Route : ' . $rname . '<br/>Date : ' . date( 'd/m/Y', strtotime( $pcdate ) ) );
    
        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Port Clearance Detail
| -------------------------------------------------------------------------------------
*/
function save_port_clearance_detail( $pcid )
{
    global $db;
    
    foreach( $_POST['bpid'] as $key => $bpid )
    {
        $s = 'INSERT INTO ticket_port_clearance_detail( 
                pcid, 
                bpid, 
                lcountry_id, 
                guest_name, 
                guest_type, 
                guest_country, 
                guest_gender, 
                guest_age) 
              VALUES( %d, %d, %d, %s, %s, %s, %s, %d )';
        $q = $db->prepare_query( $s, 
                $pcid, 
                $bpid, 
                $_POST['lcountry_id'][$key], 
                $_POST['guest_name'][$key], 
                $_POST['guest_type'][$key], 
                $_POST['guest_country'][$key], 
                $_POST['guest_gender'][$key], 
                $_POST['guest_age'][$key]);
           
        $db->do_query( $q );
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Port Clearance
| -------------------------------------------------------------------------------------
*/
function update_port_clearance()
{
    global $db;

    $rname  = get_route( $_POST['pcroute'], 'rname' );
    $pcdate = date( 'Y-m-d', strtotime( $_POST['pcdate'] ) );
     
    $s = 'UPDATE ticket_port_clearance SET 
            rid = %d,
            boid = %d,
            pcroute = %s,
            captain = %s,
            kkm = %s,
            crew = %s,
            pcdate = %s,
            pcport = %s, 
            pctrip = %s, 
            pcboat = %s
          WHERE pcid = %d';
    $q = $db->prepare_query( $s,  
            $_POST['pcroute'], 
            $_POST['boid'],
            $rname,
            $_POST['captain'], 
            $_POST['kkm'],
            $_POST['crew'],
            $pcdate, 
            $_POST['pcport'],
            $_POST['pctrip'],
            get_boat( $_POST['boid'], 'boname' ),
            $_POST['pcid'] );

    if( $db->do_query( $q ) )
    {
        save_log( $_POST['pcid'], 'port-clearance', 'Update port clearance<br/>Route : ' . $rname . '<br/>Date : ' . date( 'd/m/Y', strtotime( $pcdate ) ) );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Port Clearance Passenger
| -------------------------------------------------------------------------------------
*/
function update_port_clearance_passenger()
{
    global $db;

    $error = 0;
    
    foreach( $_POST['pdata'] as $key => $d )
    {
        list( $pcid, $bpid, $lcountry_id, $guest_name, $guest_type, $guest_country, $guest_gender, $guest_age ) = explode( '|', $d );

        $s = 'SELECT COUNT(a.bpid) AS num FROM `ticket_port_clearance_detail` AS a WHERE a.bpid = %d AND a.pcid = %d';
        $q = $db->prepare_query( $s, $bpid, $pcid );
        $r = $db->do_query( $q );
        $d = $db->fetch_array( $r );

        if( $d['num'] == 0 )
        {
            $s = 'INSERT INTO ticket_port_clearance_detail( 
                    pcid, 
                    bpid, 
                    lcountry_id, 
                    guest_name, 
                    guest_type, 
                    guest_country, 
                    guest_gender, 
                    guest_age) 
                  VALUES( %d, %d, %d, %s, %s, %s, %s, %d )';
            $q = $db->prepare_query( $s, $pcid, $bpid, $lcountry_id, $guest_name, $guest_type, $guest_country, $guest_gender, $guest_age );
               
            if( !$db->do_query( $q ) )
            {
                $error++;
            }
        }
        else
        {
            $error++;
        }
    }

    if( $error > 0 )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Port Clearance By ID
| -------------------------------------------------------------------------------------
*/
function delete_port_clearance( $pcid )
{
    global $db;

    $d = get_port_clearance( $pcid );

    $s = 'DELETE FROM ticket_port_clearance WHERE pcid = %d';
    $q = $db->prepare_query( $s, $pcid );

    if( $db->do_query( $q ) )
    {
        save_log( $pcid, 'port-clearance', 'Delete port clearance<br/>Route : ' . $d['pcroute'] . '<br/>Date : ' . date( 'd/m/Y', strtotime( $d['pcdate'] ) ) );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Port Clearance Detail By ID
| -------------------------------------------------------------------------------------
*/
function delete_port_clearance_detail( $pcdid )
{
    global $db;

    $d = get_port_clearance_detail( $pcdid );

    $s = 'DELETE FROM ticket_port_clearance_detail WHERE pcdid = %d';
    $q = $db->prepare_query( $s, $pcdid );

    if( $db->do_query( $q ) )
    {
        $d2 = get_port_clearance( $d['pcid'] );

        save_log( $d['pcid'], 'port-clearance', 'Delete port clearance detail<br/>Route : ' . $d2['pcroute'] . '<br/>Date : ' . date( 'd/m/Y', strtotime( $d2['pcdate'] ) ) . '<br/>Guest Name : ' . $d['guest_name'] );

        return true;
    }
}


/*
| -------------------------------------------------------------------------------------
| Get Port Clearance By ID
| -------------------------------------------------------------------------------------
*/
function get_port_clearance( $pcid )
{
    global $db;

    $s = 'SELECT * FROM ticket_port_clearance as a WHERE a.pcid = %d';
    $q = $db->prepare_query( $s, $pcid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $s2 = 'SELECT * FROM ticket_port_clearance_detail AS a WHERE a.pcid = %d';
    $q2 = $db->prepare_query( $s2, $pcid );
    $r2 = $db->do_query( $q2 );

    while( $d2 = $db->fetch_array( $r2 ) )
    {
        $d['detail'][] = $d2;
    }

    if( !empty( $field ) && isset( $d[$field] ) )
    {
        return $d[$field];
    }
    else
    {
        return $d;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Port Clearance Deteail By ID
| -------------------------------------------------------------------------------------
*/
function get_port_clearance_detail( $pcdid )
{
    global $db;

    $s = 'SELECT * FROM ticket_port_clearance_detail AS a WHERE a.pcdid = %d';
    $q = $db->prepare_query( $s, $pcdid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d;
}

/*
| -------------------------------------------------------------------------------------
| Get Filter
| -------------------------------------------------------------------------------------
*/
function get_filter_port_clearance()
{
    $filter = array( 'rid' => '', 'tid' => '', 'bddate' => date( 'd F Y', time() + 86400 ) );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array( 'rid' => $rid, 'tid' => $tid, 'bddate' => $bddate );
    }

    return $filter;
}

function get_filter_port_clearance_doc()
{
    $filter = array( 'rid' => '', 'tid' => '', 'bddate' => '' );

    if( isset( $_GET['prm'] ) )
    {
        $data = json_decode( base64_decode( $_GET['prm'] ), true );

        extract( $data );

        $filter = array( 'rid' => $rid, 'tid' => $tid, 'bddate' => $bddate );
    }

    return $filter;
}

/*
| -------------------------------------------------------------------------------------
| Check if reservation valid for added in transport list
| -------------------------------------------------------------------------------------
*/
function is_valid_port_clearance( $data )
{
    foreach( $data as $d )
    {
        list( $bpid[], $route[], $date[], $trip[], $port[] ) = explode( '|', $d );
    }

    $num_route = count( array_count_values( $route ) );
    $num_date  = count( array_count_values( $date ) );
    $num_trip  = count( array_count_values( $trip ) );
    $num_port  = count( array_count_values( $port ) );

    if( $num_route == 1 && $num_date == 1 )
    {
        return array( 
            'bpid'  => $bpid, 
            'route' => $route[0], 
            'date'  => $date[0], 
            'trip'  => $trip[0], 
            'port'  => $port[0]
        );
    }
}

/*
| -------------------------------------------------------------------------------------
| Check if page used filter
| -------------------------------------------------------------------------------------
*/
function is_port_clearance_view()
{
    if( isset( $_GET['state'] ) && $_GET['state']=='port-clearance-report' && isset( $_GET['prm'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Check if passenger exist in Port Clearance
| -------------------------------------------------------------------------------------
*/
function is_exist_in_port_clearance( $bpid = array() )
{
    global $db;

    $s = 'SELECT bpid FROM ticket_port_clearance_detail WHERE bpid IN ( ' . implode( ',', $bpid ) . ' )';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Check if page is port clearance added more form
| -------------------------------------------------------------------------------------
*/
function is_add_more_port_clearance_passenger()
{
    if( isset( $_POST['add_more_port_clearance_passenger'] ) || isset( $_POST['more_publish'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_more_publish()
{
    if( isset( $_POST['more_publish'] ) && allow_action( $_GET['state'], 'insert' ) && $_COOKIE['user_type'] != 'contributor' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -------------------------------------------------------------------------------------
| Port Clearance Ajax Request
| -------------------------------------------------------------------------------------
*/
function ticket_port_clearance_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    extract( $_POST );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $pkey ) && $pkey == 'load-port-clearance-doc-data' )
    {
        extract( $_POST );
        
        $data = ticket_port_clearance_doc_table_query( $tid, $rid, $bddate );

        echo json_encode( $data );
    }

    if( isset( $pkey ) && $pkey == 'load-passenger-list-data' )
    {
        extract( $_POST );

        $data   = ticket_port_clearance_passenger_list_data( $rid, $tid, $bddate );

        echo json_encode( $data );
    }

    if( isset( $pkey ) && $pkey == 'delete-port-clearance-passenger' )
    {
        if( delete_port_clearance_detail( $pcdid ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $pkey ) && $pkey == 'delete-port-clearance' )
    {
        if( delete_port_clearance( $pcid ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    exit;
}