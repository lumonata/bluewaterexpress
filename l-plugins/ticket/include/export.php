<?php

/*
| -------------------------------------------------------------------------------------
| Print Report Actions
| -------------------------------------------------------------------------------------
*/
add_actions( 'passenger-list-pdf-report_page', 'ticket_passenger_list_pdf_report' );
add_actions( 'passenger-list-csv-report_page', 'ticket_passenger_list_csv_report' );
add_actions( 'passenger-row-csv-report_page', 'ticket_passenger_row_csv_report' );
add_actions( 'passenger-list-print-report_page', 'ticket_passenger_list_print_report' );
add_actions( 'passenger-list-fo-print-report_page', 'ticket_passenger_list_fo_print_report' );
add_actions( 'passenger-list-accounting-print-report_page', 'ticket_passenger_list_accounting_print_report' );

add_actions( 'transport-list-pdf-report_page', 'ticket_transport_list_pdf_report' );
add_actions( 'transport-list-csv-report_page', 'ticket_transport_list_csv_report' );

add_actions( 'rsv-pdf-report_page', 'ticket_reservation_pdf_report' );
add_actions( 'rsv-csv-report_page', 'ticket_reservation_csv_report' );
add_actions( 'rsv-print-report_page', 'ticket_reservation_print_report' );

add_actions( 'daily-addons-pdf-report_page', 'ticket_daily_addons_pdf_report' );
add_actions( 'daily-addons-csv-report_page', 'ticket_daily_addons_csv_report' );
add_actions( 'daily-addons-print-report_page', 'ticket_daily_addons_print_report' );

add_actions( 'summary-addons-pdf-report_page', 'ticket_summary_addons_pdf_report' );
add_actions( 'summary-addons-csv-report_page', 'ticket_summary_addons_csv_report' );
add_actions( 'summary-addons-print-report_page', 'ticket_summary_addons_print_report' );

add_actions( 'rsv-timeline-pdf-report_page', 'ticket_reservation_timeline_pdf_report' );
add_actions( 'rsv-timeline-csv-report_page', 'ticket_reservation_timeline_csv_report' );
add_actions( 'rsv-timeline-print-report_page', 'ticket_reservation_timeline_print_report' );

add_actions( 'daily-pax-pdf-report_page', 'ticket_daily_pax_pdf_report' );
add_actions( 'daily-pax-csv-report_page', 'ticket_daily_pax_csv_report' );
add_actions( 'daily-pax-print-report_page', 'ticket_daily_pax_print_report' );

add_actions( 'promo-pdf-report_page', 'ticket_promo_code_pdf_report' );
add_actions( 'promo-csv-report_page', 'ticket_promo_code_csv_report' );
add_actions( 'promo-print-report_page', 'ticket_promo_code_print_report' );

add_actions( 'seats-production-pdf-report_page', 'ticket_seats_production_pdf_report' );
add_actions( 'seats-production-csv-report_page', 'ticket_seats_production_csv_report' );

add_actions( 'revenue-pdf-report_page', 'ticket_revenue_pdf_report' );
add_actions( 'revenue-csv-report_page', 'ticket_revenue_csv_report' );

add_actions( 'transport-revenue-pdf-report_page', 'ticket_transport_revenue_pdf_report' );
add_actions( 'transport-revenue-csv-report_page', 'ticket_transport_revenue_csv_report' );

add_actions( 'average-rates-pdf-report_page', 'ticket_average_rates_pdf_report' );
add_actions( 'average-rates-csv-report_page', 'ticket_average_rates_csv_report' );

add_actions( 'hs-sales-sum-pdf-report_page', 'ticket_hs_sales_sum_pdf_report' );
add_actions( 'hs-sales-sum-csv-report_page', 'ticket_hs_sales_sum_csv_report' );
add_actions( 'hs-sales-sum-print-report_page', 'ticket_hs_sales_sum_print_report' );

add_actions( 'market-segment-pdf-report_page', 'ticket_market_segment_pdf_report' );
add_actions( 'market-segment-csv-report_page', 'ticket_market_segment_csv_report' );

add_actions( 'port-clearance-pdf-report_page', 'ticket_port_clearance_pdf_report' );
add_actions( 'port-clearance-csv-report_page', 'ticket_port_clearance_csv_report' );

add_actions( 'agent-outstanding-pdf-report_page', 'ticket_agent_outstanding_pdf_report' );
add_actions( 'agent-outstanding-print-report_page', 'ticket_agent_outstanding_print_report' );

add_actions( 'trip-transport-print-report_page', 'ticket_trip_transport_print_report' );

add_actions( 'pickup-list-print-report_page', 'ticket_pickup_list_print_report' );
add_actions( 'pickup-list-pdf-report_page', 'ticket_pickup_list_pdf_report' );
add_actions( 'pickup-list-csv-report_page', 'ticket_pickup_list_csv_report' );

add_actions( 'dropoff-list-print-report_page', 'ticket_dropoff_list_print_report' );
add_actions( 'dropoff-list-pdf-report_page', 'ticket_dropoff_list_pdf_report' );
add_actions( 'dropoff-list-csv-report_page', 'ticket_dropoff_list_csv_report' );

add_actions( 'print-and-export-report-ajax_page', 'ticket_print_and_export_report_ajax' );
add_actions( 'print-and-download-report_page', 'ticket_print_and_download_report' );

add_actions( 'rsv-retrive-print-report_page', 'ticket_retrive_rsv_print_report' );
add_actions( 'rsv-retrive-csv-report_page', 'ticket_retrive_rsv_csv_report' );

add_actions( 'agent-rsv-retrive-print-report_page', 'ticket_agent_retrive_rsv_print_report' );
add_actions( 'agent-rsv-retrive-csv-report_page', 'ticket_agent_retrive_rsv_csv_report' );
add_actions( 'agent-print-invoice-summary_page', 'ticket_print_agent_invoice_summary' );
add_actions( 'agent-pdf-invoice-summary_page', 'ticket_pdf_agent_invoice_summary' );
add_actions( 'agent-csv-invoice-summary_page', 'ticket_csv_agent_invoice_summary' );

add_actions( 'outstanding-print-list_page', 'ticket_outstanding_print_list' );
add_actions( 'outstanding-export-csv_page', 'ticket_outstanding_export_csv' );

/*
| -------------------------------------------------------------------------------------
| Export - Reservation List Print
| -------------------------------------------------------------------------------------
*/
function ticket_retrive_rsv_print_report()
{
	$site_url = site_url();

    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
	add_actions( 'include_js', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
    add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/accounting.min.js?v=' . TICKET_VERSION );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-reservation-list.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'order', $_GET['order'] );
	add_variable( 'filter', $_GET['filter'] );

	add_variable( 'admurl', get_admin_url() );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Report - Reservation List' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );
    return return_template( 'report' );
}

function ticket_retrive_rsv_csv_report()
{
	$content = get_retrive_rsv_csv( $_GET['filter'], $_GET['order'] );

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=reservation-list.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_retrive_rsv_csv( $filter, $order )
{
	$filter = json_decode( base64_decode( $filter ), true );
	$order  = json_decode( base64_decode( $order ), true );
	$res    = get_reservation_list_report( $filter, $order, true );

	if( !empty( $res ) )
    {
		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-reservation-list.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

    	foreach( $res['data'] as $d )
		{
	        add_variable( 'gpax', $d['gpax'] );
	        add_variable( 'gadult', $d['gadult'] );
	        add_variable( 'gchild', $d['gchild'] );
	        add_variable( 'ginfant', $d['ginfant'] );
	        add_variable( 'transport', $d['transport'] );
	        add_variable( 'guest_name', $d['guest_name'] );

	        add_variable( 'phone', $d['phone'] );
	        add_variable( 'route', $d['route'] );
	        add_variable( 'chanel', $d['chanel'] );
	        add_variable( 'btotal', $d['btotal'] );
	        add_variable( 'status', $d['status'] );
	        add_variable( 'bbemail', $d['bbemail'] );
	        add_variable( 'rev_date', $d['rev_date'] );
	        add_variable( 'ref_agent', $d['ref_agent'] );
	        add_variable( 'booked_by', $d['booked_by'] );
	        add_variable( 'dept_date', $d['dept_date'] );
	        add_variable( 'dept_time', $d['dept_time'] );
	        add_variable( 'no_ticket', $d['no_ticket'] );
	        add_variable( 'rsv_status', $d['rsv_status'] );
	        add_variable( 'arrive_time', $d['arrive_time'] );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'pax', $res['pax'] );
	    add_variable( 'adult', $res['adult'] );
	    add_variable( 'child', $res['child'] );
	    add_variable( 'infant', $res['infant'] );
    	add_variable( 'print_date', date( ' d F Y ' ) );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Export - Outstanding Payment List
| -------------------------------------------------------------------------------------
*/
function get_data_outstanding_payment()
{
	global $db;

	$order  = json_decode( base64_decode( $_GET['order'] ), true );
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$cols   = array( 
        0 => 'c.agname',
        1 => 'c.agemail',
        2 => 'outstanding'
    );

	extract( $filter );

	if( is_array( $order ) && !empty( $order ) )
    {
        $o = array();

        foreach( $order as $i => $od )
        {
            $o[] = $cols[ $od[0] ] . ' ' . $od[1];
        }

        $order = implode( ', ', $o );
    }
    else
    {
        $order = 'outstanding DESC';
    }

    if ( !empty( $src ) && $src != '' ) 
    {
    	$search = array();

    	unset( $cols[2] );

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $src . '%' );
        }

        $filter = ' AND (' . implode( ' OR ' , $search) . ')';
    }
    else
    {
    	$filter = '';
    }

    $s = 'SELECT 
            a.agid, 
            c.agname, 
            c.agemail, 
            SUM(a.atdebet - a.atcredit) AS outstanding 
          FROM ticket_agent_transaction AS a 
          LEFT JOIN ticket_booking AS b ON a.bid = b.bid 
          LEFT JOIN ticket_agent AS c ON a.agid = c.agid 
          WHERE b.bstt <> "ar" AND c.agpayment_type = "Credit" AND ( 
            SELECT GROUP_CONCAT( DISTINCT( a2.bdpstatus ) ) 
            FROM ticket_booking_detail AS a2 
            WHERE a2.bid = a.bid 
          ) NOT IN( "cn","bc" )' . $filter
          . ' GROUP BY a.agid ORDER BY ' . $order;
    $r = $db->do_query( $s );
    $n = $db->num_rows( $r );

    $data = array();

    if( $n > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data[] = array(
                'agname'       => $d['agname'],
                'agemail'      => $d['agemail'],
                'outstanding'  => number_format( $d['outstanding'], 0, ',', '.' ),
                'outstanding2' => $d['outstanding']
            );
        }
    }

    return $data;
}

function ticket_outstanding_print_list()
{
	$data  = get_data_outstanding_payment();
	$total = 0;

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/print-outstanding-payment-list.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

		foreach( $data as $d )
	    {
			add_variable( 'agname', $d['agname'] );
			add_variable( 'agemail', $d['agemail'] );
			add_variable( 'outstanding', $d['outstanding'] );
        	
			$total = $total + $d['outstanding2'];

        	parse_template( 'report-loop-block', 'rl-block', true );
	    }

		add_variable( 'admurl', get_admin_url() );
		add_variable( 'print_date', date( ' d F Y ' ) );
	    add_variable( 'web_title', get_meta_data('web_title') );
	    add_variable( 'section_title', 'Report - Outstanding Payment' );
		add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );
		add_variable( 'total_outstanding', number_format( $total, 0, ',', '.' ) );
		add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

	    parse_template( 'report-block', 'r-block', false );

	    echo return_template( 'report' );
    }

    exit;
}

function ticket_outstanding_export_csv()
{
	$content = get_outstanding_payment_csv();

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=outstanding-payment-list.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_outstanding_payment_csv()
{
	$data  = get_data_outstanding_payment();
	$total = 0;

	if( !empty( $data ) )
    {
		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-outstanding-payment-list.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

    	foreach( $data as $d )
		{
			add_variable( 'agname', $d['agname'] );
			add_variable( 'agemail', $d['agemail'] );
			add_variable( 'outstanding', $d['outstanding'] );
        	
			$total = $total + $d['outstanding2'];

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

    	add_variable( 'print_date', date( ' d F Y ' ) );
	    add_variable( 'total_outstanding', number_format( $total, 0, ',', '.' ) );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Export - Agent Invoice Summary
| -------------------------------------------------------------------------------------
*/
function ticket_print_agent_invoice_summary()
{
	$site_url = site_url();

    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
	add_actions( 'include_js', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
    add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/accounting.min.js?v=' . TICKET_VERSION );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-agent-invoice-summary.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'filter', $_GET['filter'] );
	add_variable( 'admurl', get_admin_url() );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Report - Agent Reservation List' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_pdf_agent_invoice_summary()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_agent_pdf_invoice_summary( $filter );

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 10, 15 );
	    $mpdf->SetTitle( 'Open Invoices and Orders Summary' );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'open-invoices-and-order-summary.pdf', 'D' );
	}
}

function ticket_csv_agent_invoice_summary()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_agent_csv_invoice_summary( $filter );

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=open-invoices-and-order-summary.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

/*
| -------------------------------------------------------------------------------------
| Export - Agent Reservation List
| -------------------------------------------------------------------------------------
*/
function ticket_agent_retrive_rsv_print_report()
{
	$site_url = site_url();

    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
	add_actions( 'include_js', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
    add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/accounting.min.js?v=' . TICKET_VERSION );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-agent-reservation-list.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'order', $_GET['order'] );
	add_variable( 'filter', $_GET['filter'] );

	add_variable( 'admurl', get_admin_url() );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Report - Agent Reservation List' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

/*
| -------------------------------------------------------------------------------------
| Export - Passanger List Export
| -------------------------------------------------------------------------------------
*/
function ticket_passenger_list_print_report()
{
	$site_url = site_url();

    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
	add_actions( 'include_js', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
    add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/accounting.min.js?v=' . TICKET_VERSION );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-passenger-list.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'admurl', get_admin_url() );
	add_variable( 'filter', $_GET['filter'] );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Report - Passenger List' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_passenger_list_pdf_report()
{
	$content = get_passenger_list_pdf_report_data();

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Passenger List' );

	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
	    $mpdf->SetTitle( 'Report - Passenger List' );
	    $mpdf->SetHTMLHeader( $header );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'passenger-list-report.pdf', 'D' );
	}
}

function get_passenger_list_pdf_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_passenger_list_report( $filter );

	if( !empty( $data ) )
	{
		extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-passenger-list.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

		foreach( $items as $obj => $groups )
	    {
	    	list( $location, $total_passenger, $total_percent ) = json_decode( base64_decode( $obj ) );

			$tblcontent = '';

	    	foreach( $groups as $channel => $group )
			{
				list( $chid, $chcode, $chname ) = json_decode( base64_decode( $channel ) );

				$num = count( $group );
				$prc = round( ( $num * 100 ) / $total, 2 );

				$tblcontent .= '
				<div class="item">
					<div class="head clearfix">
						<div class="col-md-6">
							<p>' . $chname . ' - [' . $chcode . ']</p>
						</div>
						<div class="col-md-6">
							<p class="text-right medium-bold">' . $num . ' (' . $prc . '%)</p>
						</div>
					</div>
					<div class="table-block">
						<div class="table">
							<div class="t-header">
								<p class="name">Booked By</p>
								<p class="gname">Guest Name</p>
								<p class="route">Route</p>
								<p class="ticket">Resv/Ticket #</p>
							</div>
							<div class="t-content">';

								foreach( $group as $d )
								{
									$bbname = empty( $d['agid'] ) ? $d['bbname'] : get_agent( $d['agid'], 'agname' );
									$taname = empty( $d['hid'] ) ? '' : '<br />Trans. area : ' . get_hotel( $d['hid'], 'taname' );

									$tblcontent .= '
									<div class="item">
										<p class="name">' . $bbname . '</p>
										<p class="gname">' . $d['gname'] . '</p>
										<p class="route">' . $d['rname'] . $taname . '<br />Boat : ' . $d['bocode'] . '<br /> Travel Date : ' . $d['bddate'] . '</p>
										<p class="ticket">' . $d['bticket'] . '</p>
									</div>';
			                    }

			                    $tblcontent .= '
				            </div>
				        </div>
					</div>
				</div>';
			}

	    	add_variable( 'trip_data', $tblcontent );
	    	add_variable( 'trip_location', $location );
	    	add_variable( 'total_percent', $total_percent );
	    	add_variable( 'total_passenger', $total_passenger );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );
	    add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
    }
}

function ticket_passenger_list_csv_report()
{
	$content = get_passenger_list_csv_report_data();
	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=passenger-list-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function ticket_passenger_row_csv_report()
{
	$content = get_passenger_row_csv_report_data();

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=passenger-row-data.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_passenger_list_csv_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_passenger_list_report( $filter );

	if( !empty( $data ) )
    {
        extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-passenger-list.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        foreach( $items as $obj => $groups )
	    {
	    	list( $location, $total_passenger, $total_percent ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '';

	    	foreach( $groups as $channel => $group )
			{
				list( $chid, $chcode, $chname, $gtotal, $gpercent ) = json_decode( base64_decode( $channel ) );

				$tblcontent .= '
				<tr>
					<td>(' . $chcode . ') ' . $chname . '</td>
					<td></td>
					<td></td>
					<td></td>
				    <td></td>
				    <td></td>
				    <td></td>
					<td>' . $gtotal . ' (' . $gpercent . '%)</td>
				</tr>
				<tr>
					<td>Booked By</td>
					<td>Guest Name</td>
					<td>Chn</td>
					<td>Route</td>
					<td>Resv/Ticket #</td>
					<td>Travel Date</td>
				    <td>Boat</td>
				    <td>Trans Area</td>
				    <td>Remark</td>
				</tr>';				

            	foreach( $group as $i => $d )
				{							
					$remark = empty( $d['bremark'] ) ? '-' : $d['bremark'];
					$bbname = empty( $d['agid'] ) ? $d['bbname'] : get_agent( $d['agid'], 'agname' );
					$taname = empty( $d['hid'] ) ? '-' : get_hotel( $d['hid'], 'taname' );

					$tblcontent .= '				
					<tr>
                        <td>' . $bbname . '</td>
                        <td>' . $d['gname'] . '</td>
                        <td>' . $d['chcode'] . '</td>
                        <td>' . $d['rname'] . '</td>
                        <td>' . $d['bticket'] . '</td>
                        <td>' . $d['bddate'] . '</td>
                        <td>' . $d['bocode'] . '</td>
                        <td>' . $taname . '</td>
                        <td>' . $remark . '</td>
					</tr>';
				}
			}

	    	add_variable( 'trip_data', $tblcontent );
	    	add_variable( 'trip_location', $location );
	    	add_variable( 'total_percent', $total_percent );
	    	add_variable( 'total_passenger', $total_passenger );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
    }
}

function get_passenger_row_csv_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_passenger_row_list_report( $filter );

	$tblcontent =
	'<table width="100%" border="1">
		<tr>
			<td>Booking Detail</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Guest Detail</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Accomodation</td>
			<td></td>
			<td></td>
			<td>Booker Detail</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Trip Detail</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Transport Detail</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Booking Code</td>
			<td>Booking Type</td>
			<td>Booking Date</td>
			<td>RSV Type</td>
			<td>Rsv Status</td>
			<td>Payment Methode</td>
			<td>Payment Status</td>
			<td>Guest Name</td>
			<td>Gender</td>
			<td>DOB</td>
			<td>Nationality</td>
			<td>Remark</td>
			<td>Adult</td>
			<td>Child</td>
			<td>Infant</td>
			<td>Accomodation Hotel</td>
			<td>Hotel Address</td>
			<td>Hotel Phone</td>
			<td>Booking Source</td>
			<td>Booked By</td>
			<td>Nationality</td>
			<td>Email</td>
			<td>Phone</td>
			<td>Travel Date</td>
			<td>Schedule/Route</td>
			<td>Departure</td>
			<td>Arrival</td>
			<td>ETD</td>
			<td>ETA</td>
			<td>Pickup</td>
			<td>Trans Type</td>
			<td>Pickup Area</td>
			<td>Pickup Hotel</td>
			<td>Hotel Address</td>
			<td>Hotel Phone</td>
			<td>Drop-Off</td>
			<td>Trans Type</td>
			<td>Drop-Off Area</td>
			<td>Drop-Off Hotel</td>
			<td>Hotel Address</td>
			<td>Hotel Phone</td>
		</tr>';

	if( !empty( $data ) )
    {
		foreach ( $data as $r ) 
		{
		    if( $r['bpstatus'] != 'cn' )
		    {
    			$tblcontent .=
    			'<tr>
    				<td>' . $r['bticket'] . '</td>
    				<td>' . $r['btype'] . '</td>
    				<td>' . $r['bdate'] . '</td>
    				<td>' . $r['bbrevtype'] . '</td>
    				<td>' . $r['bbrevstatus'] . '</td>
    				<td>' . $r['mname'] . '</td>
    				<td>' . $r['bstatus'] . '</td>
    				<td>' . $r['bpname'] . '</td>
    				<td>' . $r['gender'] . '</td>
    				<td>' . $r['bpbirthdate'] . '</td>
    				<td>' . $r['lcountry'] . '</td>
    				<td>' . $r['remark'] . '</td>
    				<td>' . $r['adult'] . '</td>
    				<td>' . $r['child'] . '</td>
    				<td>' . $r['infant'] . '</td>
    				<td>' . $r['bhotelname'] . '</td>
    				<td>' . $r['bhoteladd'] . '</td>
    				<td>' . $r['bhotelphone'] . '</td>
    				<td>' . $r['chname'] . '</td>
    				<td>' . $r['bbname'] . '</td>
    				<td>' . $r['bbcountry'] . '</td>
    				<td>' . $r['bbemail'] . '</td>
    				<td>' . $r['bbphone'] . '</td>
    				<td>' . $r['bddate'] . '</td>
    				<td>' . $r['rname'] . '</td>
    				<td>' . $r['bdfrom'] . '</td>
    				<td>' . $r['bdto'] . '</td>
    				<td>' . $r['bddeparttime'] . '</td>
    				<td>' . $r['bdarrivetime'] . '</td>
    				<td>' . $r['p_type'] . '</td>
    				<td>' . $r['p_transtype'] . '</td>
    				<td>' . $r['p_area'] . '</td>
    				<td>' . $r['p_hotel'] . '</td>
    				<td>' . $r['p_hoteladdr'] . '</td>
    				<td>' . $r['p_hotelphone'] . '</td>
    				<td>' . $r['d_type'] . '</td>
    				<td>' . $r['d_transtype'] . '</td>
    				<td>' . $r['d_area'] . '</td>
    				<td>' . $r['d_hotel'] . '</td>
    				<td>' . $r['d_hoteladdr'] . '</td>
    				<td>' . $r['d_hotelphone'] . '</td>
    			</tr>';
		    }
		}		
    }

    $tblcontent .=
	'</table>';

    return $tblcontent;
}

function ticket_passenger_list_fo_print_report()
{
	$site_url = site_url();

    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
	add_actions( 'include_js', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
    add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/accounting.min.js?v=' . TICKET_VERSION );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-passenger-list-fo.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'admurl', get_admin_url() );
	add_variable( 'filter', $_GET['filter'] );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Report - FO Passenger List' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_passenger_list_accounting_print_report()
{
	$site_url = site_url();

    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
	add_actions( 'include_js', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
    add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/accounting.min.js?v=' . TICKET_VERSION );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-passenger-list-accounting.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'admurl', get_admin_url() );
	add_variable( 'filter', $_GET['filter'] );
	add_variable( 'printed_date', date( 'd F Y; H:i:s' ) );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Report - Accounting Passenger List' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

/*
| -------------------------------------------------------------------------------------
| Export - Transport List Export
| -------------------------------------------------------------------------------------
*/
function ticket_transport_list_pdf_report()
{
	$content = get_transport_list_pdf_report_data();

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Transport List' );

	    if( isset( $_GET['print'] ) && $_GET['print']  )
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Transport List' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
	    	$mpdf->SetJS( 'this.print();' );
			$mpdf->Output( 'transport-list-report.pdf', 'I' );
	    }
	    else
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Transport List' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
			$mpdf->Output( 'transport-list-report.pdf', 'D' );
	    }
	}
}

function get_transport_list_pdf_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_transport_list_report( $filter );

	if( !empty( $data ) )
	{
		extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-transport-list.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

		foreach( $items as $obj => $groups )
	    {
	    	list( $area, $total_passenger, $total_percent ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '';

	    	foreach( $groups as $location => $group )
			{
				list( $from, $to ) = json_decode( base64_decode( $location ) );

				$num = count( $group );
				$prc = round( ( $num * 100 ) / $total, 2 );

				$tblcontent .= '
				<div class="item">
					<div class="head clearfix">
						<div class="col-md-6">
							<p>' . $from . '-' . $to .'</p>
						</div>
						<div class="col-md-6">
							<p class="text-right medium-bold">' . $num . ' (' . $prc . '%)</p>
						</div>
					</div>
					<div class="table-block">
						<div class="table">
							<div class="t-header">
								<p class="name">Booked By</p>
								<p class="gname-small">Guest Name</p>
								<p class="hotel-small">Hotel</p>
								<p class="ticket">Resv/Ticket #</p>
								<p class="transport-small">Transport</p>
							</div>
							<div class="t-content">';

								foreach( $group as $d )
								{							
									$bbname = empty( $d['agid'] ) ? $d['bbname'] : get_agent( $d['agid'], 'agname' );

									$tblcontent .= '
									<div class="item">
										<p class="name">' . $bbname . '</p>
										<p class="gname-small">' . $d['gname'] . '</p>
										<p class="hotel-small">' . $d['hname'] . '<br/>Addr: ' . $d['haddress'] .'<br />Phone: ' . $d['hphone'] . '</p>
										<p class="ticket">' . $d['bticket'] . '</p>
										<p class="transport-small">' . $d['vname'] . '<br/>Driver: ' . $d['dname'] . '<br/>(' . $d['phone'] . ')<br/>Trans Type: ' . $d['tttype'] . '<br/>Type: ' . ucwords( $d['type'] ) . '</p>
									</div>';
			                    }

			                    $tblcontent .= '
				            </div>
				        </div>
					</div>
				</div>';
			}

	    	add_variable( 'transport_area', $area );
	    	add_variable( 'transport_data', $tblcontent );
	    	add_variable( 'total_percent', $total_percent );
	    	add_variable( 'total_passenger', $total_passenger );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );
	    add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

function ticket_transport_list_csv_report()
{
	$content = get_transport_list_csv_report_data();

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=transport-list-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_transport_list_csv_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_transport_list_report( $filter );

	if( !empty( $data ) )
    {
        extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-transport-list.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        foreach( $items as $obj => $groups )
	    {
	    	list( $area, $total_passenger, $total_percent ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '';

	    	foreach( $groups as $location => $group )
			{
				list( $from, $to ) = json_decode( base64_decode( $location ) );

				$num = count( $group );
				$prc = round( ( $num * 100 ) / $total, 2 );

				$tblcontent .= '
				<tr>
					<td>' . $from . '-' . $to .'</td>
					<td></td>
					<td></td>
					<td></td>
				    <td></td>
				    <td></td>
				    <td></td>
				    <td></td>
					<td>' . $num . ' (' . $prc . '%)</td>
				</tr>
				<tr>
					<td>Booked By</td>
					<td>Guest Name</td>
					<td>Hotel Address</td>
					<td>Phone</td>
					<td>Trans. Type</td>
					<td>Type</td>
				    <td>Resv/Ticket #</td>
				    <td>Vehicle</td>
				    <td>Driver</td>
				    <td>Phone</td>
				</tr>';

				foreach( $group as $i => $d )
				{							
					$tblcontent .= '
					<tr>
                        <td>' . $d['bbname'] . '</td>
                        <td>' . $d['gname'] . '</td>
                        <td>' . $d['hname'] . ' - ' . $d['haddress'] .'</td>
                        <td>' . $d['hphone'] . '</td>
                        <td>' . $d['tttype'] .'</td>
                        <td>' . ucwords( $d['type'] ) .'</td>
                        <td>' . $d['bticket'] . '</td>
                        <td>' . $d['vname'] . '</td>
                        <td>' . $d['dname'] . '</td>
                        <td>' . $d['phone'] . '</td>
                    </tr>';
                }
			}

	    	add_variable( 'transport_area', $area );
	    	add_variable( 'transport_data', $tblcontent );
	    	add_variable( 'total_percent', $total_percent );
	    	add_variable( 'total_passenger', $total_passenger );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Transport Revenue Export
| -------------------------------------------------------------------------------------
*/
function ticket_transport_revenue_pdf_report()
{
	$content = get_transport_revenue_pdf_report_data();

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Transport Revenue' );

	    if( isset( $_GET['print'] ) && $_GET['print']  )
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Transport Revenue' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
	    	$mpdf->SetJS( 'this.print();' );
			$mpdf->Output( 'transport-revenue-report.pdf', 'I' );
	    }
	    else
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Transport Revenue' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
			$mpdf->Output( 'transport-revenue-report.pdf', 'D' );
	    }
	}
}

function get_transport_revenue_pdf_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_transport_revenue_report( $filter );

	if( !empty( $data ) )
	{
		extract( $data );
		extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-transport-revenue.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

	    foreach( $items as $obj => $groups )
	    {
	    	list( $location, $total_by_location ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '
			<div class="item">
				<div class="table-block">
					<div class="table">
						<div class="t-header">
							<p class="vehicle">Vehicle</p>
							<p class="driver">Driver Name</p>
							<p class="type">Type</p>
							<p class="tdate">Date</p>
							<p class="time">Time</p>
							<p class="fee">Fee</p>
						</div>
						<div class="t-content">';

				    	foreach( $groups as $d )
						{
							$tblcontent .= '
							<div class="item">
								<p class="vehicle">' . $d['vname'] . '</p>
								<p class="driver">' . $d['dname'] . '</p>
								<p class="type">' . ucwords( $d['type'] ) . '</p>
								<p class="tdate">' . date( 'd F, Y', strtotime( $d['ttdate'] ) ) . '</p>
								<p class="time">' . $d['tttime'] . '</p>
								<p class="fee">' . number_format( $d['ttfee'], 0, '.' , '.' ) .'</p>
							</div>';			        
						}

	                    $tblcontent .= '
			            </div>
			        </div>
				</div>
			</div>';

	    	add_variable( 'trip_data', $tblcontent );
	    	add_variable( 'trip_location', $location );
	    	add_variable( 'total_by_location', number_format( $total_by_location, 0, '.' , '.' ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
	    add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

function ticket_transport_revenue_csv_report()
{
	$content = get_transport_revenue_csv_report_data();

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=transport-revenue-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_transport_revenue_csv_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_transport_revenue_report( $filter );

	if( !empty( $data ) )
    {
        extract( $data );
		extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-transport-revenue.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

		$tblcontent = '
		<tr>
            <td>Vehicle</td>
            <td>Driver Name</td>
            <td>Type</td>
            <td>Date</td>
            <td>Time</td>
            <td>Fee</td>
		</tr>';

	    foreach( $items as $obj => $groups )
	    {
	    	list( $location, $total_by_location ) = json_decode( base64_decode( $obj ) );

	    	foreach( $groups as $d )
			{
				$tblcontent .= '
				<tr>
                    <td>' . $d['vname'] . '</td>
                    <td>' . $d['dname'] . '</td>
                    <td>' . ucwords( $d['type'] ) . '</td>
                    <td>' . date( 'd F, Y', strtotime( $d['ttdate'] ) ) . '</td>
                    <td>' . $d['tttime'] . '</td>
                    <td>' . number_format( $d['ttfee'], 0, '.' , ',' ) .'</td>
                </tr>';
            }

	    	add_variable( 'trip_data', $tblcontent );
	    	add_variable( 'trip_location', $location );
	    	add_variable( 'total_by_location', number_format( $total_by_location, 0, '.' , ',' ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', number_format( $total, 0, '.' , ',' ) );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Promo Code Export
| -------------------------------------------------------------------------------------
*/
function ticket_promo_code_print_report()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_ticket_promo_code_report( $filter );

	if( empty( $data ) === false )
	{
    	extract( $data );
    	extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/print-promo-code.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        add_block( 'report-agent-loop-block', 'ral-block', 'report' );
        add_block( 'report-agent-block', 'ra-block', 'report' );

    	if( empty( $chid ) )
	    {
	    	add_variable( 'booking_source', 'All Booking Source' );
	    }
	    else
	    {
	    	$arr    = explode( '|', $chid );
	    	$agid   = isset( $arr[1] ) ? $arr[1] : null;
	    	$chid   = $arr[0];
	    	$source = get_booking_source( $chid, $agid );

	    	add_variable( 'booking_source', $source['name'] );
	    }

    	if( empty( $bstatus ) )
	    {
	    	add_variable( 'payment_status', 'All Status' );
	    }
	    else
	    {
	    	$status  = ticket_booking_status();
	    	$pstatus = array();

		    if( !empty( $status ) )
		    {
		        foreach( $status as $st => $stat )
		        {
		            if( is_array( $bstatus ) && in_array( $st, $bstatus ) )
		            {
		                $pstatus[] = $stat;
		            }
		            elseif( $st == $bstatus )
		            {
		            	$pstatus[] = $stat; 
		            }
		        }
		    }

	    	add_variable( 'payment_status', implode( ', ', $pstatus ) );
	    }

    	if( empty( $promo_code ) )
	    {
	    	add_variable( 'promo_code', 'All Promo Code' );
	    }
	    else
	    {
	    	$promos = get_promo_list();
	    	$parray = array();

		    if( !empty( $promos ) )
		    {
		        foreach( $promos as $p )
		        {
		            if( is_array( $promo_code ) && in_array( $p['pmcode'], $promo_code ) )
		            {
		                $parray[] = $p['pmcode'];
		            }
		            elseif( $p['pmcode'] == $promo_code )
		            {
		            	$parray[] = $p['pmcode']; 
		            }
		        }
		    }

	    	add_variable( 'promo_code', implode( ', ', $parray ) );
	    }

	    if( empty( $rbased ) )
	    {
	    	$base_on = 'Booking date';

	    	add_variable( 'base_on', $base_on );
	    }
	    else
	    {
	    	$base_on = 'Travel date';

	    	add_variable( 'base_on', $base_on );
	    }

	    if( $rtype != '' && $bdate != '' )
	    {
	        if( $rtype == '0'  )
	        {
	    		add_variable( 'period', 'on ' . date( 'd F Y', strtotime( $bdate ) ) );
	        }
	        elseif( $rtype == '1' )
	        {
	    		add_variable( 'period', 'on ' . date( 'F Y', strtotime( $bdate ) ) );
	        }
	        elseif( $rtype == '2' )
	        {
	    		add_variable( 'period', 'on ' . date( 'Y', strtotime( $bdate ) ) );
	        }
	        elseif( $rtype == '3' )
	        {
	            $date_from = date( 'Y-m-d', strtotime( $bdate ) );
	            $date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

	    		add_variable( 'period', 'on ' . $date_from . ' until ' . $date_to );
	        }
	    }

        foreach( $items as $pmcode => $groups )
        {
            $total_sum_by_code = 0;
            $total_sum_com_by_code = 0;

            $list = '';

        	foreach( $groups['items'] as $d )
			{
                $total_sum_by_code += $d['bdiscount'];
                $total_sum_com_by_code += $d['bcommission'];

                $list .= '
                <tr>
                    <td>' . date( 'd F Y', strtotime( $d['bdate'] ) ) . '</td>
                    <td>Booking #' . $d['bticket'] . '</td>
                    <td>' . $d['bsource'] . '}</td>
                    <td>' . $d['bddate'] . '</td>
                    <td>' . $d['route'] . '</td>
                    <td align="right">' . number_format( $d['bcommission'], 0, '.' , '.' ) . '</td>';

                    if( !isset( $agentpanel ) )
                    {
                		$list .= '
                    	<td align="right">' . number_format( $d['bdiscount'], 0, '.' , '.' ) . '</td>';

                    }

                	$list .= '
                </tr>';
            }

			add_variable( 'list', $list );
			add_variable( 'code', $pmcode );
			add_variable( 'total_by_code', $groups['totalbycode'] );
			add_variable( 'total_sum_by_code', number_format( $total_sum_by_code, 0, '.' , '.' ) );
			add_variable( 'total_by_code_prc', round( ( $groups['totalbycode'] * 100 ) / $total, 2 ) );
			add_variable( 'total_sum_com_by_code', number_format( $total_sum_com_by_code, 0, '.' , '.' ) );

            if( isset( $agentpanel ) )
            {
	    		parse_template( 'report-agent-loop-block', 'ral-block', true );
	    	}
	    	else
	    	{
	    		parse_template( 'report-loop-block', 'rl-block', true );
	    	}
	    }

		add_variable( 'total', $total );
		add_variable( 'admurl', get_admin_url() );
	    add_variable( 'web_title', get_meta_data('web_title') );
	    add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	    add_variable( 'section_title', 'Report - Promo Code' );
		add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        if( isset( $agentpanel ) )
        {
	    	parse_template( 'report-agent-block', 'ra-block', false );
    	}
    	else
    	{
	    	parse_template( 'report-block', 'r-block', false );
    	}

	    echo return_template( 'report' );
    }

    exit;
}

function ticket_promo_code_pdf_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$header  = ticket_header_report( 'Report - Promo Code' );
	$content = get_promo_code_pdf_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';
	
	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
	    $mpdf->SetTitle( 'Report - Promo Code' );
	    $mpdf->SetHTMLHeader( $header );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'promo-code-report.pdf', 'D' );
	}
}

function get_promo_code_pdf_report_data( $filter )
{
	$data = get_ticket_promo_code_report( $filter );

	if( empty( $data ) === false )
	{
    	extract( $data );
    	extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-promo-code.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );
		
        add_block( 'report-agent-loop-block', 'ral-block', 'report' );
        add_block( 'report-agent-block', 'ra-block', 'report' );

    	if( empty( $chid ) )
	    {
	    	add_variable( 'booking_source', 'All Booking Source' );
	    }
	    else
	    {
	    	$arr    = explode( '|', $chid );
	    	$agid   = isset( $arr[1] ) ? $arr[1] : null;
	    	$chid   = $arr[0];
	    	$source = get_booking_source( $chid, $agid );

	    	add_variable( 'booking_source', $source['name'] );
	    }

    	if( empty( $bstatus ) )
	    {
	    	add_variable( 'payment_status', 'All Status' );
	    }
	    else
	    {
	    	$status  = ticket_booking_status();
	    	$pstatus = array();

		    if( !empty( $status ) )
		    {
		        foreach( $status as $st => $stat )
		        {
		            if( is_array( $bstatus ) && in_array( $st, $bstatus ) )
		            {
		                $pstatus[] = $stat;
		            }
		            elseif( $st == $bstatus )
		            {
		            	$pstatus[] = $stat; 
		            }
		        }
		    }

	    	add_variable( 'payment_status', implode( ', ', $pstatus ) );
	    }

    	if( empty( $promo_code ) )
	    {
	    	add_variable( 'promo_code', 'All Promo Code' );
	    }
	    else
	    {
	    	$promos = get_promo_list();
	    	$parray = array();

		    if( !empty( $promos ) )
		    {
		        foreach( $promos as $p )
		        {
		            if( is_array( $promo_code ) && in_array( $p['pmcode'], $promo_code ) )
		            {
		                $parray[] = $p['pmcode'];
		            }
		            elseif( $p['pmcode'] == $promo_code )
		            {
		            	$parray[] = $p['pmcode']; 
		            }
		        }
		    }

	    	add_variable( 'promo_code', implode( ', ', $parray ) );
	    }

	    if( empty( $rbased ) )
	    {
	    	$base_on = 'Booking date';

	    	add_variable( 'base_on', $base_on );
	    }
	    else
	    {
	    	$base_on = 'Travel date';

	    	add_variable( 'base_on', $base_on );
	    }

	    if( $rtype != '' && $bdate != '' )
	    {
	        if( $rtype == '0'  )
	        {
	    		add_variable( 'period', 'on ' . date( 'd F Y', strtotime( $bdate ) ) );
	        }
	        elseif( $rtype == '1' )
	        {
	    		add_variable( 'period', 'on ' . date( 'F Y', strtotime( $bdate ) ) );
	        }
	        elseif( $rtype == '2' )
	        {
	    		add_variable( 'period', 'on ' . date( 'Y', strtotime( $bdate ) ) );
	        }
	        elseif( $rtype == '3' )
	        {
	            $date_from = date( 'Y-m-d', strtotime( $bdate ) );
	            $date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

	    		add_variable( 'period', 'on ' . $date_from . ' until ' . $date_to );
	        }
	    }

        foreach( $items as $pmcode => $groups )
        {
            $total_sum_by_code = 0;
            $total_sum_com_by_code = 0;

            $list = '';

        	foreach( $groups['items'] as $d )
			{
                $total_sum_by_code += $d['bdiscount'];
                $total_sum_com_by_code += $d['bcommission'];

                $list .= '
                <tr>
                    <td>' . date( 'd M Y', strtotime( $d['bdate'] ) ) . '</td>
                    <td>Booking #' . $d['bticket'] . '</td>
                    <td>' . $d['bsource'] . '</td>
                    <td>' . $d['bddate'] . '</td>
                    <td>' . $d['route'] . '</td>
                    <td align="right">' . number_format( $d['bcommission'], 0, '.' , '.' ) . '</td>';

                    if( !isset( $agentpanel ) )
                    {
                		$list .= '
                    	<td align="right">' . number_format( $d['bdiscount'], 0, '.' , '.' ) . '</td>';

                    }

                	$list .= '
                </tr>';
            }

			add_variable( 'list', $list );
			add_variable( 'code', $pmcode );
			add_variable( 'total_by_code', $groups['totalbycode'] );
			add_variable( 'total_sum_by_code', number_format( $total_sum_by_code, 0, '.' , '.' ) );
			add_variable( 'total_by_code_prc', round( ( $groups['totalbycode'] * 100 ) / $total, 2 ) );
			add_variable( 'total_sum_com_by_code', number_format( $total_sum_com_by_code, 0, '.' , '.' ) );

            if( isset( $agentpanel ) )
            {
	    		parse_template( 'report-agent-loop-block', 'ral-block', true );
	    	}
	    	else
	    	{
	    		parse_template( 'report-loop-block', 'rl-block', true );
	    	}
	    }

		add_variable( 'total', $total );
		add_variable( 'admurl', get_admin_url() );
	    add_variable( 'web_title', get_meta_data('web_title') );
	    add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	    add_variable( 'section_title', 'Report - Promo Code' );
		add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        if( isset( $agentpanel ) )
        {
	    	parse_template( 'report-agent-block', 'ra-block', false );
    	}
    	else
    	{
	    	parse_template( 'report-block', 'r-block', false );
    	}

	    return return_template( 'report' );
	}
}

function ticket_promo_code_csv_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_promo_code_csv_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=promo-code-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_promo_code_csv_report_data( $filter )
{
	$data = get_ticket_promo_code_report( $filter, false );

	if( empty( $data ) === false )
	{
		extract( $data );
    	extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-promo-code.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );
		
        add_block( 'report-agent-loop-block', 'ral-block', 'report' );
        add_block( 'report-agent-block', 'ra-block', 'report' );

    	if( empty( $chid ) )
	    {
	    	add_variable( 'booking_source', 'All Booking Source' );
	    }
	    else
	    {
	    	$arr    = explode( '|', $chid );
	    	$agid   = isset( $arr[1] ) ? $arr[1] : null;
	    	$chid   = $arr[0];
	    	$source = get_booking_source( $chid, $agid );

	    	add_variable( 'booking_source', $source['name'] );
	    }

    	if( empty( $bstatus ) )
	    {
	    	add_variable( 'payment_status', 'All Status' );
	    }
	    else
	    {
	    	$status  = ticket_booking_status();
	    	$pstatus = array();

		    if( !empty( $status ) )
		    {
		        foreach( $status as $st => $stat )
		        {
		            if( is_array( $bstatus ) && in_array( $st, $bstatus ) )
		            {
		                $pstatus[] = $stat;
		            }
		            elseif( $st == $bstatus )
		            {
		            	$pstatus[] = $stat; 
		            }
		        }
		    }

	    	add_variable( 'payment_status', implode( ', ', $pstatus ) );
	    }

    	if( empty( $promo_code ) )
	    {
	    	add_variable( 'promo_code', 'All Promo Code' );
	    }
	    else
	    {
	    	$promos = get_promo_list();
	    	$parray = array();

		    if( !empty( $promos ) )
		    {
		        foreach( $promos as $p )
		        {
		            if( is_array( $promo_code ) && in_array( $p['pmcode'], $promo_code ) )
		            {
		                $parray[] = $p['pmcode'];
		            }
		            elseif( $p['pmcode'] == $promo_code )
		            {
		            	$parray[] = $p['pmcode']; 
		            }
		        }
		    }

	    	add_variable( 'promo_code', implode( ', ', $parray ) );
	    }

	    if( empty( $rbased ) )
	    {
	    	$base_on = 'Booking date';

	    	add_variable( 'base_on', $base_on );
	    }
	    else
	    {
	    	$base_on = 'Travel date';

	    	add_variable( 'base_on', $base_on );
	    }

	    if( $rtype != '' && $bdate != '' )
	    {
	        if( $rtype == '0'  )
	        {
	    		add_variable( 'period', 'on ' . date( 'd F Y', strtotime( $bdate ) ) );
	        }
	        elseif( $rtype == '1' )
	        {
	    		add_variable( 'period', 'on ' . date( 'F Y', strtotime( $bdate ) ) );
	        }
	        elseif( $rtype == '2' )
	        {
	    		add_variable( 'period', 'on ' . date( 'Y', strtotime( $bdate ) ) );
	        }
	        elseif( $rtype == '3' )
	        {
	            $date_from = date( 'Y-m-d', strtotime( $bdate ) );
	            $date_to   = empty( $bdateto ) ? $date_from : date( 'Y-m-d', strtotime( $bdateto ) );

	    		add_variable( 'period', 'on ' . $date_from . ' until ' . $date_to );
	        }
	    }

        foreach( $items as $pmcode => $groups )
        {
            $total_sum_by_code = 0;
            $total_sum_com_by_code = 0;

            $list = '';

        	foreach( $groups['items'] as $d )
			{
                $total_sum_by_code += $d['bdiscount'];
                $total_sum_com_by_code += $d['bcommission'];

				$list .= '
				<tr>
                    <td>' . date( 'd, F Y', strtotime( $d['bdate'] ) ) . '</td>
                    <td>Booking #' . $d['bticket'] .'</td>
                    <td>' . $d['bsource'] . '</td>
                    <td>' . $d['bddate'] . '</td>
                    <td>' . $d['route'] . '</td>
                    <td>' . $d['bcommission'] . '</td>';

                    if( !isset( $agentpanel ) )
                    {
                		$list .= '
                    	<td>' . $d['bdiscount'] . '</td>';

                    }
                	
                	$list .= '
                </tr>';
            }

			add_variable( 'list', $list );
			add_variable( 'code', $pmcode );
			add_variable( 'total_by_code', $groups['totalbycode'] );
			add_variable( 'total_sum_by_code', $total_sum_by_code );
			add_variable( 'total_sum_com_by_code', $total_sum_com_by_code );
			add_variable( 'total_by_code_prc', round( ( $groups['totalbycode'] * 100 ) / $total, 2 ) );

            if( isset( $agentpanel ) )
            {
	    		parse_template( 'report-agent-loop-block', 'ral-block', true );
	    	}
	    	else
	    	{
	    		parse_template( 'report-loop-block', 'rl-block', true );
	    	}
	    }

	    add_variable( 'total', $total );

        if( isset( $agentpanel ) )
        {
	    	parse_template( 'report-agent-block', 'ra-block', false );
    	}
    	else
    	{
	    	parse_template( 'report-block', 'r-block', false );
    	}
    	
	    return return_template( 'report' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Export - Daily Pax Export
| -------------------------------------------------------------------------------------
*/
function ticket_daily_pax_print_report()
{
	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-daily-reservation.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	
	extract( $filter );

	$data = get_daily_reservation_report( $filter );

	if( empty( $data ) )
	{
    	$tblcontent = '';
	}
	else
	{
        $dt = get_all_location_probability();

    	$tblcontent = '
    	<table width="100%">
    		<thead>
				<tr>
					<td rowspan="2">NO</td>
					<td rowspan="2" width="35">TRIP DATE</td>
					<td colspan="2">GILI DIRECT</td>
					<td colspan="' . ( $dt['num'] + 2 ) .'" align="center">ALL TRIP</td>
				</tr>
				<tr>
					<td>OUT</td>
					<td>RTN</td>';

					foreach( $dt['trip'] as $idx => $trip )
					{
						foreach( $trip as $td )
						{
							$tblcontent .= '
							<td width="35">' . $td . '</td>';
						}

						$tblcontent .= '
						<td class="gray">' . ( $idx == 0 ? 'FR. BALI' : 'FR. GILI' ) . '</td>';
					}

	    			$tblcontent .= '
	    		</tr>
	    	</thead>
    		<tbody>';

				if( empty( $bdateto ) )
				{
					$dates  = get_list_date_period( $bdate, $bdate );
				}
				else
				{
					$dates  = get_list_date_period( $bdate, $bdateto );
				}

    			$gtotal = array();
    			$number = 1;

				foreach( $dates as $date )
				{
    				$tblcontent .= '
    				<tr>	    					
					    <td>' . $number . '</td>
					    <td align="center">' . date( 'd M', strtotime( $date ) ) . '</td>
					    <td>0</td>
					    <td>0</td>';

						foreach( $dt['trip'] as $idx => $trip )
						{
							$total = 0;

							foreach( $trip as $td )
							{
								$stotal = 0;

								if( isset( $data[ $date ][ $td ] ) && !empty( $data[ $date ][ $td ] ) )
								{
									foreach( $data[ $date ][ $td ] as $obj )
									{
										$stotal = $stotal + $obj['bdnum'];
									}

									$total = $total + $stotal;

									$tblcontent .= '
									<td>' . $stotal . '</td>';
								}
								else
								{
									$tblcontent .= '
									<td>0</td>';
								}

								$gtotal[ $td ][] = $stotal;
							}

							$tblcontent .= '
							<td class="gray">' . $total . '</td>';
						}

					    $tblcontent .= '
    				</tr>';

    				$number++;
				}

    			$tblcontent .= '
    		</tbody>
    		<tfoot>	    			
				<tr>
					<td colspan="2" align="center" class="bold">TOTAL</td>
					<td>0</td>
					<td>0</td>';

					foreach( $dt['trip'] as $idx => $trip )
					{
						$ttl = 0;

						foreach( $trip as $td )
						{
							$sttl = isset( $gtotal[ $td ] ) ? array_sum( $gtotal[ $td ] ) : 0;
							$ttl  = $ttl + $sttl;

							$tblcontent .= '
							<td>' . $sttl . '</td>';
						}

						$tblcontent .= '
						<td class="gray">' . $ttl . '</td>';
					}

	    			$tblcontent .= '			
				<tr>
    		</tfoot>
    	</table>';

    	$chnl = get_channel( $chid, 'chname' );
    	$trip = get_route_multiple( $rid, 'rname' );
    	$trip = empty( $trip ) ? '' : implode( ',<br/>', $trip );

    	add_variable( 'data_tbl', $tblcontent );
    }

	$chnl = get_channel( $chid, 'chname' );
	$trip = get_route_multiple( $rid, 'rname' );
	$trip = empty( $trip ) ? '' : implode( ',<br/>', $trip );

	add_variable( 'trip', $trip );
	add_variable( 'channel', $chnl );
	add_variable( 'trip_css', $trip == '' ? 'display:none;' : '' );
	add_variable( 'channel_css', $chnl == '' ? 'display:none;' : '' );
	add_variable( 'daily_css', $rtype == '0' ? '' : 'display:none;' );
	add_variable( 'range_css', $rtype == '3' ? '' : 'display:none;' );
	add_variable( 'date_from', date( 'd M Y', strtotime( $bdate ) ) );
	add_variable( 'date_to', date( 'd M Y', strtotime( $bdateto ) ) );

	add_variable( 'admurl', get_admin_url() );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Report - Daily Pax Numbers' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );
	add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

    parse_template( 'report-block', 'r-block', false );

    echo return_template( 'report' );

    exit;
}

function ticket_daily_pax_pdf_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_daily_pax_pdf_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Daily Pax Numbers' );
	
	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
	    $mpdf->SetTitle( 'Report - Daily Pax Numbers' );
	    $mpdf->SetHTMLHeader( $header );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'daily-pax-report.pdf', 'D' );
	}
}

function get_daily_pax_pdf_report_data( $filter )
{
	extract( $filter );

	$data = get_daily_reservation_report( $filter );

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-daily-reservation.html', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

    	$dt = get_all_location_probability();

    	$tblcontent = '
    	<table width="100%">
    		<thead>
				<tr>
					<td rowspan="2">NO</td>
					<td rowspan="2" width="35">TRIP DATE</td>
					<td colspan="2">GILI DIRECT</td>
					<td colspan="' . ( $dt['num'] + 2 ) .'" align="center">ALL TRIP</td>
				</tr>
				<tr>
					<td>OUT</td>
					<td>RTN</td>';

					foreach( $dt['trip'] as $idx => $trip )
					{
						foreach( $trip as $td )
						{
							$tblcontent .= '
							<td width="35">' . $td . '</td>';
						}

						$tblcontent .= '
						<td class="gray">' . ( $idx == 0 ? 'FR. BALI' : 'FR. GILI' ) . '</td>';
					}

	    			$tblcontent .= '
	    		</tr>
	    	</thead>
    		<tbody>';

				if( empty( $bdateto ) )
				{
					$dates  = get_list_date_period( $bdate, $bdate );
				}
				else
				{
					$dates  = get_list_date_period( $bdate, $bdateto );
				}

    			$gtotal = array();
    			$number = 1;

				foreach( $dates as $date )
				{
    				$tblcontent .= '
    				<tr>	    					
					    <td>' . $number . '</td>
					    <td align="center">' . date( 'd M', strtotime( $date ) ) . '</td>
					    <td>0</td>
					    <td>0</td>';

						foreach( $dt['trip'] as $idx => $trip )
						{
							$total = 0;

							foreach( $trip as $td )
							{
								$stotal = 0;

								if( isset( $data[ $date ][ $td ] ) && !empty( $data[ $date ][ $td ] ) )
								{
									foreach( $data[ $date ][ $td ] as $obj )
									{
										$stotal = $stotal + $obj['bdnum'];
									}

									$total = $total + $stotal;

									$tblcontent .= '
									<td>' . $stotal . '</td>';
								}
								else
								{
									$tblcontent .= '
									<td>0</td>';
								}

								$gtotal[ $td ][] = $stotal;
							}

							$tblcontent .= '
							<td class="gray">' . $total . '</td>';
						}

					    $tblcontent .= '
    				</tr>';

    				$number++;
				}

    			$tblcontent .= '
    		</tbody>
    		<tfoot>	    			
				<tr>
					<td colspan="2" align="center" class="bold">TOTAL</td>
					<td>0</td>
					<td>0</td>';

					foreach( $dt['trip'] as $idx => $trip )
					{
						$ttl = 0;

						foreach( $trip as $td )
						{
							$sttl = isset( $gtotal[ $td ] ) ? array_sum( $gtotal[ $td ] ) : 0;
							$ttl  = $ttl + $sttl;

							$tblcontent .= '
							<td>' . $sttl . '</td>';
						}

						$tblcontent .= '
						<td class="gray">' . $ttl . '</td>';
					}

	    			$tblcontent .= '			
				<tr>
    		</tfoot>
    	</table>';

    	$chnl = get_channel( $chid, 'chname' );
    	$trip = get_route_multiple( $rid, 'rname' );
    	$trip = empty( $trip ) ? '' : implode( ',<br/>', $trip );

    	add_variable( 'trip', $trip );
    	add_variable( 'channel', $chnl );
    	add_variable( 'data_tbl', $tblcontent );
    	add_variable( 'trip_css', $trip == '' ? 'display:none;' : '' );
    	add_variable( 'channel_css', $chnl == '' ? 'display:none;' : '' );
    	add_variable( 'daily_css', $rtype == '0' ? '' : 'display:none;' );
    	add_variable( 'range_css', $rtype == '3' ? '' : 'display:none;' );
    	add_variable( 'date_from', date( 'd M Y', strtotime( $bdate ) ) );
    	add_variable( 'date_to', date( 'd M Y', strtotime( $bdateto ) ) );
    }

	add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_daily_pax_csv_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_daily_pax_csv_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=reservation-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_daily_pax_csv_report_data( $filter )
{
	extract( $filter );

	$data = get_daily_reservation_report( $filter );

	if( !empty( $data ) )
    {
    	$dt = get_all_location_probability();

    	$chnl = get_channel( $chid, 'chname' );
    	$trip = get_route_multiple( $rid, 'rname' );
    	$trip = empty( $trip ) ? '' : implode( ',', $trip );

    	$tblcontent = '
		<table width="100%" border="0">';

			if( $rtype == '0' )
			{
    			$tblcontent .= '
				<tr>
					<td>Trip Date : </td>
					<td>' . date( 'd M Y', strtotime( $bdate ) ) . '</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>';
			}
			else if( $rtype == '3' )
			{
    			$tblcontent .= '
				<tr>
					<td>From Date : </td>
					<td>' . date( 'd M Y', strtotime( $bdate ) ) . '</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>To Date : </td>
					<td>' . date( 'd M Y', strtotime( $bdateto ) ) . '</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>';
			}

			if( $trip != '' )
			{
				$tblcontent .= '
				<tr>
					<td>Trip : </td>
					<td>' . $trip . '</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>';
			}

			if( $chnl != '' )
			{
				$tblcontent .= '
				<tr>
					<td>Channel : </td>
					<td>' . $chnl . '</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>';
			}
			
			$tblcontent .= '
			<tr>
				<td>NO</td>
				<td>TRIP DATE</td>
				<td>GILI DIRECT</td>
				<td>ALL TRIP</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>OUT</td>
				<td>RTN</td>';

				foreach( $dt['trip'] as $idx => $trip )
				{
					foreach( $trip as $td )
					{
						$tblcontent .= '
						<td>' . $td . '</td>';
					}

					$tblcontent .= '
					<td>' . ( $idx == 0 ? 'FR. BALI' : 'FR. GILI' ) . '</td>';
				}

				$tblcontent .= '
			</tr>';

			if( empty( $bdateto ) )
			{
				$dates  = get_list_date_period( $bdate, $bdate );
			}
			else
			{
				$dates  = get_list_date_period( $bdate, $bdateto );
			}

			$gtotal = array();
			$number = 1;

			foreach( $dates as $date )
			{
				$tblcontent .= '
				<tr>
					<td>' . $number . '</td>
					<td>' . date( 'd M', strtotime( $date ) ) . '</td>
					<td>0</td>
					<td>0</td>';

					foreach( $dt['trip'] as $idx => $trip )
					{
						$total = 0;

						foreach( $trip as $td )
						{
							$stotal = 0;

							if( isset( $data[ $date ][ $td ] ) && !empty( $data[ $date ][ $td ] ) )
							{
								foreach( $data[ $date ][ $td ] as $obj )
								{
									$stotal = $stotal + $obj['bdnum'];
								}

								$total = $total + $stotal;

								$tblcontent .= '
								<td>' . $stotal . '</td>';
							}
							else
							{
								$tblcontent .= '
								<td>0</td>';
							}

							$gtotal[ $td ][] = $stotal;
						}

						$tblcontent .= '
						<td>' . $total . '</td>';
					}

				    $tblcontent .= '
				</tr>';

				$number++;
			}
			
			$tblcontent .= '
			<tr>
				<td>TOTAL</td>
				<td></td>
				<td>0</td>
				<td>0</td>';

				foreach( $dt['trip'] as $idx => $trip )
				{
					$ttl = 0;

					foreach( $trip as $td )
					{
						$sttl = isset( $gtotal[ $td ] ) ? array_sum( $gtotal[ $td ] ) : 0;
						$ttl  = $ttl + $sttl;

						$tblcontent .= '
						<td>' . $sttl . '</td>';
					}

					$tblcontent .= '
					<td>' . $ttl . '</td>';
				}

    			$tblcontent .= '	
			</tr>
		</table>';

		return $tblcontent;
    }
}

/*
| -------------------------------------------------------------------------------------
| Export - Summary Add-ons Export
| -------------------------------------------------------------------------------------
*/
function ticket_summary_addons_print_report()
{
	$site_url = site_url();

    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
	add_actions( 'include_js', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
    add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/accounting.min.js?v=' . TICKET_VERSION );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-summary-addons.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'admurl', get_admin_url() );
	add_variable( 'filter', $_GET['filter'] );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Add-ons Summary Report' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_summary_addons_pdf_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_summary_addons_pdf_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Add-ons Summary Report' );
	
	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 2, 2, 35, 15 );
	    $mpdf->SetTitle( 'Add-ons Summary Report' );
	    $mpdf->SetHTMLHeader( $header );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'summary-add-ons-report.pdf', 'I' );
	}
}

function get_summary_addons_pdf_report_data( $filter )
{
	extract( $filter );

	$data = get_summary_addons_report( $filter );

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-summary-addons.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        extract( $data );

		foreach( $items as $obj => $trips )
	    {
	    	list( $trip, $total_by_trip, $total_by_trip_format, $total_by_trip_percent ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '';

	    	foreach( $trips as $item => $groups )
			{
	    		list( $port, $total_by_port, $total_by_port_format, $total_by_port_percent ) = json_decode( base64_decode( $item ) );

	    		$tblcontent .= '
				<div class="item">
					<div class="sub-head clearfix">
						<div class="col-md-6">
							<p>Port : ' . $port . '</p>
						</div>
						<div class="col-md-6">
							<p class="text-right medium-bold"></p>
						</div>
					</div>';					

			    	foreach( $groups as $source => $group )
					{
			    		list( $bddate, $total_by_date, $total_by_date_format, $total_by_date_percent ) = json_decode( base64_decode( $source ) );

	    				$tblcontent .= '
	    				<div class="sub-content">
		    				<div class="sub-item">
								<div class="sub-head-2 clearfix">
									<div class="col-md-12">
										<p>' . date( 'd F Y', strtotime( $bddate ) ) . '</p>
									</div>
								</div>
								<div class="table-block">
									<div class="table">
										<div class="t-header clearfix" style="padding-left:7px;">
		                                    <p class="addons">Add-ons</p>
		                                    <p class="addons-pax">Qty</p>
		                                    <p class="addons-price">Price</p>
		                                    <p class="addons-total">Total</p>
										</div>
										<div class="t-content">';

											foreach( $group as $d )
											{
												$tblcontent .= '
												<div class="item clearfix" style="padding-left:7px;">
				                                    <p class="addons">' . $d['aoname'] . '</p>
				                                    <p class="addons-pax">' . $d['baopax'] . '</p>
				                                    <p class="addons-price">' . $d['bfaoprice'] . '</p>
				                                    <p class="addons-total">' . $d['bfaototal'] . '</p>
												</div>';
						                    }

						                    $tblcontent .= '
										</div>
									</div>
								</div>
							</div>
	    				</div>';
					}

	    			$tblcontent .= '
				</div>';
			}

	    	add_variable( 'data', $tblcontent );
	    	add_variable( 'travel_date', $trip );
	    	add_variable( 'total', $total_by_trip_format );
	    	add_variable( 'total_percent', $total_by_trip_percent );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total_format );
	    add_variable( 'total_pax', $total_pax );
	}

	add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_summary_addons_csv_report()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_summary_addons_report( $filter );

	if( !empty( $data ) )
	{
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment;filename="add-ons-summary-report.csv"');
		header('Cache-Control: max-age=0');

        extract( $data );
        
        $csv = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $csv->getProperties()
            ->setCategory( 'CSV File' )
            ->setTitle( 'Add-ons Summary Report' )
            ->setSubject('Add Ons Report' )
            ->setCreator( 'Bluewater Express' )
            ->setLastModifiedBy( 'Bluewater Express Staf' )
            ->setKeywords( 'add-ons summary report' )
            ->setDescription('Add-ons Summary Report, generated in ' . date( 'd F Y' ) );

        $csv->getActiveSheet(0)
            ->setCellValue( 'A1', 'Add-ons Summary Report' );

        $i = 2;

        foreach( $items as $obj => $trips )
	    {
	    	list( $trip, $total_by_trip, $total_format, $total_percent ) = json_decode( base64_decode( $obj ) );

	        $csv->getActiveSheet(0)
	            ->setCellValue( 'A' . $i, 'Trip : ' . $trip )
	            ->setCellValue( 'F' . $i, $total_by_trip . ' (' . $total_percent . '%)' );

	        $i++;

	    	foreach( $trips as $item => $groups )
			{
				list( $port, $total_by_port, $total_by_port_format, $total_by_port_percent ) = json_decode( base64_decode( $item ) );

		        $csv->getActiveSheet(0)
		            ->setCellValue( 'A' . $i, 'Port ' . $port )
		            ->setCellValue( 'F' . $i, $total_by_port . ' (' . $total_by_port_percent . '%)' );

	        	$i++;

		    	foreach( $groups as $source => $group )
				{
		    		list( $bddate, $total_by_date, $total_by_date_format, $total_by_date_percent ) = json_decode( base64_decode( $source ) );

			        $csv->getActiveSheet(0)
			            ->setCellValue( 'A' . $i, $bddate );

	        		$i++;

					$csv->getActiveSheet(0)
		            	->setCellValue( 'A' . $i, 'Add-ons' )
		            	->setCellValue( 'B' . $i, 'Qty' )
		            	->setCellValue( 'C' . $i, 'Price' )
		            	->setCellValue( 'D' . $i, 'Total' );

	        		$i++;

	            	foreach( $group as $d )
					{
						$csv->getActiveSheet(0)
			            	->setCellValue( 'A' . $i, $d['aoname'] )
			            	->setCellValue( 'B' . $i, $d['baopax'] )
			            	->setCellValue( 'C' . $i, $d['baoprice'] )
			            	->setCellValue( 'D' . $i, $d['baototal'] );

	        			$i++;
					}
		    	}
			}
	    }

		$csv->getActiveSheet(0)
        	->setCellValue( 'A' . $i, 'Total' )
        	->setCellValue( 'B' . $i, $total_pax )
        	->setCellValue( 'D' . $i, $total );
            
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv( $csv );

        $writer->setSheetIndex(0);
        $writer->setEnclosure('');
        $writer->setDelimiter(';');
        $writer->setLineEnding("\r\n");
        
        $writer->save('php://output');

        exit;
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Daily Add-ons Export
| -------------------------------------------------------------------------------------
*/
function ticket_daily_addons_print_report()
{
	$site_url = site_url();

    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
	add_actions( 'include_js', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
    add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $site_url . '/l-plugins/ticket/js/accounting.min.js?v=' . TICKET_VERSION );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-daily-addons.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'admurl', get_admin_url() );
	add_variable( 'filter', $_GET['filter'] );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Add-ons - Daily Pre-Book Report' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_daily_addons_pdf_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_daily_addons_pdf_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Daily Pre-Book Report' );
	
	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 2, 2, 35, 15 );
	    $mpdf->SetTitle( 'Daily Pre-Book Report' );
	    $mpdf->SetHTMLHeader( $header );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'daily-add-ons-report.pdf', 'D' );
	}
}

function get_daily_addons_pdf_report_data( $filter )
{
	extract( $filter );

	$data = get_daily_addons_report( $filter );

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-daily-addons.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        extract( $data );

		foreach( $items as $obj => $trips )
	    {
	    	list( $bddate, $total_by_date, $total_by_date_format, $total_by_date_percent ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '';

	    	foreach( $trips as $item => $groups )
			{
	    		list( $trip, $total_by_trip, $total_by_trip_format, $total_by_trip_percent ) = json_decode( base64_decode( $item ) );

	    		$tblcontent .= '
				<div class="item">
					<div class="sub-head clearfix">
						<div class="col-md-6">
							<p>Dept. ' . $trip . '</p>
						</div>
						<div class="col-md-6">
							<p class="text-right medium-bold">' . $total_by_trip_format . ' (' . $total_by_trip_percent . '%)</p>
						</div>
					</div>';					

			    	foreach( $groups as $source => $group )
					{
			    		list( $chcode, $chname, $total_by_source, $total_by_source_format, $total_by_source_percent ) = json_decode( base64_decode( $source ) );

	    				$tblcontent .= '
	    				<div class="sub-content">
		    				<div class="sub-item">
								<div class="sub-head-2 clearfix">
									<div class="col-md-12">
										<p>(' . $chcode .') ' . $chname . '</p>
									</div>
								</div>
								<div class="table-block">
									<div class="table">
										<div class="t-header clearfix" style="padding-left:7px;">
		                                    <p class="addons-ticket">Booking ref. #</p>
		                                    <p class="addons-guest-name">Guest Name</p>
		                                    <p class="addons-daily">Add-ons</p>
		                                    <p class="addons-pax">Qty</p>
		                                    <p class="addons-price">Price</p>
		                                    <p class="addons-total">Total</p>
										</div>
										<div class="t-content">';

											foreach( $group as $d )
											{
												$tblcontent .= '
												<div class="item clearfix" style="padding-left:7px;">
				                                    <p class="addons-ticket">' . $d['bticket'] . '</p>
				                                    <p class="addons-guest-name">' . $d['gname'] . '</p>
				                                    <p class="addons-daily">' . $d['aoname'] . '</p>
				                                    <p class="addons-pax">' . $d['baopax'] . '</p>
				                                    <p class="addons-price">' . $d['bfaoprice'] . '</p>
				                                    <p class="addons-total">' . $d['bfaototal'] . '</p>
												</div>';
						                    }

						                    $tblcontent .= '
										</div>
									</div>
								</div>
							</div>
	    				</div>';
					}

	    			$tblcontent .= '
				</div>';
			}

	    	add_variable( 'data', $tblcontent );
	    	add_variable( 'total', $total_by_date_format );
	    	add_variable( 'total_percent', $total_by_date_percent );
	    	add_variable( 'travel_date', date( 'd F Y', strtotime( $bddate ) ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total_format );
	    add_variable( 'total_pax', $total_pax );
	}

	add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_daily_addons_csv_report()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_daily_addons_report( $filter );

	if( !empty( $data ) )
	{
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment;filename="daily-pre-book-report.csv"');
		header('Cache-Control: max-age=0');

        extract( $data );
        
        $csv = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $csv->getProperties()
            ->setCategory( 'CSV File' )
            ->setTitle( 'Daily Pre-Book Report' )
            ->setSubject('Add Ons Report' )
            ->setCreator( 'Bluewater Express' )
            ->setLastModifiedBy( 'Bluewater Express Staf' )
            ->setKeywords( 'add-ons daily pre-book report' )
            ->setDescription('Daily Pre-Book Report, generated in ' . date( 'd F Y' ) );

        $csv->getActiveSheet(0)
            ->setCellValue( 'A1', 'Daily Pre-Book Report' );

        $csv->getActiveSheet(0)
            ->setCellValue( 'A2', 'Total Amount' )
            ->setCellValue( 'B2', 'Total Qty : ' . $total_pax )
            ->setCellValue( 'F2', $total . ' (100%)' );

        $i = 3;

        foreach( $items as $obj => $trips )
	    {
	    	list( $bddate, $total, $total_format, $total_percent ) = json_decode( base64_decode( $obj ) );

	        $csv->getActiveSheet(0)
	            ->setCellValue( 'A' . $i, 'Book For : ' . date( 'd F Y', strtotime( $bddate ) ) )
	            ->setCellValue( 'F' . $i, $total . ' (' . $total_percent . '%)' );

	        $i++;

	    	foreach( $trips as $item => $groups )
			{
				list( $trip, $total_by_trip, $total_by_trip_format, $total_by_trip_percent ) = json_decode( base64_decode( $item ) );

		        $csv->getActiveSheet(0)
		            ->setCellValue( 'A' . $i, 'Dept. ' . $trip )
		            ->setCellValue( 'F' . $i, $total_by_trip . ' (' . $total_by_trip_percent . '%)' );

	        	$i++;

		    	foreach( $groups as $source => $group )
				{
		    		list( $chcode, $chname, $total_by_source, $total_by_source_format, $total_by_source_percent ) = json_decode( base64_decode( $source ) );

			        $csv->getActiveSheet(0)
			            ->setCellValue( 'A' . $i, '(' . $chcode . ')' . $chname );

	        		$i++;

					$csv->getActiveSheet(0)
		            	->setCellValue( 'A' . $i, 'Booking ref. #' )
		            	->setCellValue( 'B' . $i, 'Guest Name' )
		            	->setCellValue( 'C' . $i, 'Add-ons' )
		            	->setCellValue( 'D' . $i, 'Qty' )
		            	->setCellValue( 'E' . $i, 'Price' )
		            	->setCellValue( 'F' . $i, 'Total' );

	        		$i++;

	            	foreach( $group as $d )
					{
						$csv->getActiveSheet(0)
			            	->setCellValue( 'A' . $i, $d['bticket'] )
			            	->setCellValue( 'B' . $i, $d['gname'] )
			            	->setCellValue( 'C' . $i, $d['aoname'] )
			            	->setCellValue( 'D' . $i, $d['baopax'] )
			            	->setCellValue( 'E' . $i, $d['baoprice'] )
			            	->setCellValue( 'F' . $i, $d['baototal'] );

	        			$i++;
					}
		    	}
			}
	    }
            
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv( $csv );

        $writer->setSheetIndex(0);
        $writer->setEnclosure('');
        $writer->setDelimiter(';');
        $writer->setLineEnding("\r\n");
        
        $writer->save('php://output');

        exit;
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Reservation Export
| -------------------------------------------------------------------------------------
*/
function ticket_reservation_print_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	
	extract( $filter );

	$data = get_reservation_report( $filter );

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/print-reservation.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        extract( $data );

		foreach( $items as $obj => $groups )
	    {
	    	list( $rev_date, $total_rev, $total_rev_percent ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '';

	    	foreach( $groups as $source => $group )
			{
				list( $chcode, $chname, $total_rev_by_source, $total_percent_rev_by_source ) = json_decode( base64_decode( $source ) );	

				$tblcontent .= '
				<div class="item">
					<div class="head clearfix">
						<div class="col-md-6">
							<p>(' . $chcode .') ' . $chname . '</p>
						</div>
						<div class="col-md-6">
							<p class="text-right medium-bold">' . $total_rev_by_source . ' (' . $total_percent_rev_by_source . '%)</p>
						</div>
					</div>
					<div class="sub-content">
						<div class="table-block">
							<div class="table">
								<div class="t-header clearfix">
									<p class="ticket">Resv #</p>
				                    <p class="trips">Trip</p>
									<p class="adult">Adult</p>
									<p class="child">Child</p>
									<p class="infant">Infant</p>
									<p class="amount">Amount</p>
								</div>
								<div class="t-content">';

									foreach( $group as $d )
									{
										$tblcontent .= '
										<div class="item clearfix">
											<p class="ticket">' . $d['bticket'] . '</p>
				                            <p class="trips">' . $d['trips'] . '</p>
											<p class="adult">' . $d['num_adult'] . '</p>
											<p class="child">' . $d['num_child'] . '</p>
											<p class="infant">' . $d['num_infant'] . '</p>
											<p class="amount">' . $d['amount'] . '</p>
										</div>';
				                    }

				                    $tblcontent .= '
						        </div>
							</div>
						</div>
					</div>
				</div>';
			}

	    	add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'total_rev', $total_rev );
	    	add_variable( 'total_rev_percent', $total_rev_percent );
	    	add_variable( 'rev_date', date( 'd F Y', strtotime( $rev_date ) ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );

		add_variable( 'admurl', get_admin_url() );
	    add_variable( 'web_title', get_meta_data('web_title') );
	    add_variable( 'section_title', 'Report - Reservation List' );
		add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );
		add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

	    parse_template( 'report-block', 'r-block', false );

	    echo return_template( 'report' );
	}

    exit;
}

function ticket_reservation_timeline_print_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	
	extract( $filter );

	$data = get_reservation_timeline_report( $filter );

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/print-reservation-timeline.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        extract( $data );

		foreach( $items as $obj => $vals )
	    {
	    	list( $category, $ctbooking, $ctpax, $percent ) = json_decode( base64_decode( $obj ) );

	    	$tblcontent =
			'<div class="table-block">
				<div class="table">
					<div class="t-header clearfix">
						<p class="ticket" style="width:60px;">Resv #</p>
						<p class="trips" style="width:120px;">Agent/Booker</p>
						<p class="trips" style="width:120px;">Trip</p>
						<p class="trips" style="width:50px;">Booking Date</p>
						<p class="trips" style="width:50px;">Date Trip</p>
						<p class="trips" style="width:60px">Payment Status</p>
						<p class="trips" style="width:25px;">Adult</p>
						<p class="trips" style="width:25px;">Child</p>
						<p class="trips" style="width:25px;">Infant</p>
						<p class="child" style="width:25px;">Amount</p>						
					</div>
					<div class="t-content">';

						foreach( $vals as $d )
						{
							$tblcontent .= '
							<div class="item clearfix">
								<p class="ticket" style="width:60px;">'. $d['code'] .'</p>
								<p class="trips" style="width:120px;">'. $d['agent'] .'</p>
								<p class="trips" style="width:120px;">'. $d['trip'] .'</p>
								<p class="trips" style="width:50px;">'. $d['bdate'] .'</p>
								<p class="trips" style="width:50px;">'. $d['dtrip'] .'</p>
								<p class="trips" style="width:60px;">'. $d['status'] .'</p>
								<p class="trips" style="width:25px;">'. $d['adult'] .'</p>
								<p class="trips" style="width:25px;">'. $d['child'] .'</p>
								<p class="trips" style="width:25px;">'. $d['infant'] .'</p>
								<p class="child" style="width:25px;">'. $d['pax'] .'</p>
							</div>';
	                    }

	                    $tblcontent .= '
		            </div>
		        </div>
			</div>';

			add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'category', $category );
	    	add_variable( 'ctbooking', $ctbooking );
	    	add_variable( 'ctpax', $ctpax );
	    	add_variable( 'percent', $percent );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );
	    add_variable( 'tbooking', $tbooking );

		add_variable( 'admurl', get_admin_url() );
	    add_variable( 'web_title', get_meta_data('web_title') );
	    add_variable( 'section_title', 'Report - Reservation Timeline List' );
		add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );
		add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

	    parse_template( 'report-block', 'r-block', false );

	    echo return_template( 'report' );
	}

    exit;
}

function ticket_reservation_pdf_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_reservation_pdf_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Reservation' );
	
	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
	    $mpdf->SetTitle( 'Report - Reservation' );
	    $mpdf->SetHTMLHeader( $header );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'reservation-report.pdf', 'D' );
	}
}

function get_reservation_pdf_report_data( $filter )
{
	extract( $filter );

	$data = get_reservation_report( $filter );

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-reservation.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        extract( $data );

		foreach( $items as $obj => $groups )
	    {
	    	list( $rev_date, $total_rev, $total_rev_percent ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '';

	    	foreach( $groups as $source => $group )
			{
				list( $chcode, $chname, $total_rev_by_source, $total_percent_rev_by_source ) = json_decode( base64_decode( $source ) );	

				$tblcontent .= '
				<div class="item">
					<div class="head clearfix">
						<div class="col-md-6">
							<p>(' . $chcode .') ' . $chname . '</p>
						</div>
						<div class="col-md-6">
							<p class="text-right medium-bold">' . $total_rev_by_source . ' (' . $total_percent_rev_by_source . '%)</p>
						</div>
					</div>
					<div class="sub-content">
						<div class="table-block">
							<div class="table">
								<div class="t-header clearfix">
									<p class="ticket" style="width:90px;">Resv #</p>
									<p class="trips" style="width:120px;">Trip</p>
									<p class="adult" style="width:80px;">Adult</p>
									<p class="child" style="width:80px;">Child</p>
									<p class="infant" style="width:80px;">Infant</p>
									<p class="amount" style="width:80px;">Amount</p>
								</div>
								<div class="t-content">';

									foreach( $group as $d )
									{
										$tblcontent .= '
										<div class="item clearfix">
											<p class="ticket" style="width:90px;">' . $d['bticket'] . '</p>
											<p class="trips" style="width:120px;">' . $d['trips'] . '</p>
											<p class="adult" style="width:80px;">' . $d['num_adult'] . '</p>
											<p class="child" style="width:80px;">' . $d['num_child'] . '</p>
											<p class="infant" style="width:80px;">' . $d['num_infant'] . '</p>
											<p class="amount" style="width:80px;">' . $d['amount'] . '</p>
										</div>';
				                    }

				                    $tblcontent .= '
					            </div>
					        </div>
						</div>
					</div>
				</div>';
			}

	    	add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'total_rev', $total_rev );
	    	add_variable( 'total_rev_percent', $total_rev_percent );
	    	add_variable( 'rev_date', date( 'd F Y', strtotime( $rev_date ) ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );
	    add_variable( 'total_pax', $total_pax );
	}

	add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_reservation_timeline_pdf_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_reservation_timeline_pdf_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Reservation Timeline' );
	
	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
	    $mpdf->SetTitle( 'Report - Reservation Timeline' );
	    $mpdf->SetHTMLHeader( $header );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'reservation-timeline-report.pdf', 'D' );
	}
}

function get_reservation_timeline_pdf_report_data( $filter )
{
	extract( $filter );

	$data = get_reservation_timeline_report( $filter );

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-reservation-timeline.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        extract( $data );

		foreach( $items as $obj => $vals )
	    {
	    	list( $category, $ctbooking, $ctpax, $percent ) = json_decode( base64_decode( $obj ) );

	    	$tblcontent =
			'<div class="table-block">
				<div class="table">
					<div class="t-header clearfix">
						<p class="ticket" style="width:60px;">Resv #</p>
						<p class="trips" style="width:60px;">Agent/Booker</p>
						<p class="trips" style="width:60px;">Trip</p>
						<p class="trips" style="width:50px;">Booking Date</p>
						<p class="trips" style="width:50px;">Date Trip</p>
						<p class="trips" style="width:60px">Payment Status</p>
						<p class="trips" style="width:25px;">Adult</p>
						<p class="trips" style="width:25px;">Child</p>
						<p class="trips" style="width:25px;">Infant</p>
						<p class="child" style="width:45px;">Amount</p>						
					</div>
					<div class="t-content">';

						foreach( $vals as $d )
						{
							$tblcontent .= '
							<div class="item clearfix">
								<p class="ticket" style="width:60px;">'. $d['code'] .'</p>
								<p class="trips" style="width:60px;">'. $d['agent'] .'</p>
								<p class="trips" style="width:60px;">'. $d['trip'] .'</p>
								<p class="trips" style="width:50px;">'. $d['bdate'] .'</p>
								<p class="trips" style="width:50px;">'. $d['dtrip'] .'</p>
								<p class="trips" style="width:60px;">'. $d['status'] .'</p>
								<p class="trips" style="width:25px;">'. $d['adult'] .'</p>
								<p class="trips" style="width:25px;">'. $d['child'] .'</p>
								<p class="trips" style="width:25px;">'. $d['infant'] .'</p>
								<p class="child" style="width:45px;">'. $d['pax'] .'</p>
							</div>';
	                    }

	                    $tblcontent .= '
		            </div>
		        </div>
			</div>';

			add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'category', $category );
	    	add_variable( 'ctbooking', $ctbooking );
	    	add_variable( 'ctpax', $ctpax );
	    	add_variable( 'percent', $percent );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );
	    add_variable( 'tbooking', $tbooking );
	}

	add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_reservation_csv_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_reservation_csv_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=reservation-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_reservation_csv_report_data( $filter )
{
	extract( $filter );

	$data = get_reservation_report( $filter );

	if( !empty( $data ) )
    {
        extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-reservation.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        foreach( $items as $obj => $groups )
	    {
	    	list( $rev_date, $total_rev, $total_rev_percent ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '';

	    	foreach( $groups as $source => $group )
			{
				list( $chcode, $chname, $total_rev_by_source, $total_percent_rev_by_source ) = json_decode( base64_decode( $source ) );

				$tblcontent .= '
				<tr>
					<td>(' . $chcode .') ' . $chname . '</td>
					<td></td>
					<td></td>
					<td></td>
				    <td></td>
					<td>' . $total_rev_by_source . ' (' . $total_percent_rev_by_source . '%)</td>
				</tr>
				<tr>
					<td>Resv #</td>
					<td>Trip</td>
					<td>Adult</td>
					<td>Child</td>
					<td>Infant</td>
				    <td>Amount</td>
				</tr>';

            	foreach( $group as $d )
				{
					$tblcontent .= '
					<tr>
                        <td>' . $d['bticket'] . '</td>
						<td>' . $d['trips'] . '</td>
                        <td>' . $d['num_adult'] . '</td>
                        <td>' . $d['num_child'] . '</td>
                        <td>' . $d['num_infant'] . '</td>
                        <td>' . $d['amount'] . '</td>
                    </tr>';
                }
			}

	    	add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'total_rev', $total_rev );
	    	add_variable( 'total_rev_percent', $total_rev_percent );
	    	add_variable( 'rev_date', date( 'd F Y', strtotime( $rev_date ) ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );
	    add_variable( 'total_pax', $total_pax );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

function ticket_reservation_timeline_csv_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_reservation_timeline_csv_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=reservation-timeline-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_reservation_timeline_csv_report_data( $filter )
{
	extract( $filter );

	$data = get_reservation_timeline_report( $filter );

	if( !empty( $data ) )
    {
        extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-reservation-timeline.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        foreach( $items as $obj => $vals )
	    {
	    	list( $category, $ctbooking, $ctpax, $percent ) = json_decode( base64_decode( $obj ) );

			$tblcontent = '
			<tr>
				<td>Resv #</td>
				<td>Agent/Booker</td>
				<td>Trip</td>
				<td>Booking Date</td>
				<td>Date Trip</td>
				<td>Payment Status</td>
			    <td>Adult</td>
			    <td>Child</td>
			    <td>Infant</td>
			    <td>Amount</td>
			</tr>';

        	foreach( $vals as $d )
			{
				$tblcontent .= '
				<tr>
                    <td>' . $d['code'] . '</td>
					<td>' . $d['agent'] . '</td>
                    <td>' . $d['trip'] . '</td>
                    <td>' . $d['bdate'] . '</td>
                    <td>' . $d['dtrip'] . '</td>
                    <td>' . $d['status'] . '</td>
                    <td>' . $d['adult'] . '</td>
                    <td>' . $d['child'] . '</td>
                    <td>' . $d['infant'] . '</td>
                    <td>' . $d['pax'] . '</td>
                </tr>';
            }
			

	    	add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'category', $category );
	    	add_variable( 'ctbooking', $ctbooking );
	    	add_variable( 'ctpax', $ctpax );
	    	add_variable( 'percent', $percent );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', $total );
	    add_variable( 'tbooking', $tbooking );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Seats Production Export
| -------------------------------------------------------------------------------------
*/
function ticket_seats_production_pdf_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_seats_production_pdf_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$title  = $filter['rview'] == 2 ? 'Report - Seats Production By Nationality' : ( $filter['rview'] == 1 ? 'Report - Seats Production By Booking Source' : 'Report - Seats Production By Channel' );
		$header = ticket_header_report( $title );

	    if( isset( $_GET['print'] ) && $_GET['print']  )
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( $title );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
	    	$mpdf->SetJS( 'this.print();' );
			$mpdf->Output( 'seats-production-report.pdf', 'I' );
	    }
	    else
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( $title );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
			$mpdf->Output( 'seats-production-report.pdf', 'D' );
	    }
	}
}

function get_seats_production_pdf_report_data( $filter )
{
	extract( $filter );

	$data = get_seats_production_sum_report( $filter );

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-seats-production.html', 'report' );
        add_block( 'report-loop', 'r-loop', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        $page = 1;

		foreach( $data['items'] as $year => $items )
	    {
		    $tblcontent = '';
		    $tblsumdata = '';
		    $last_year  = date( 'Y', strtotime( '-1 year', strtotime( $year . '-01-01' ) ) );

            $sum_01 = array();
            $sum_02 = array();
            $sum_03 = array();
            $sum_04 = array();
            $sum_05 = array();
            $sum_06 = array();
            $sum_07 = array();
            $sum_08 = array();
            $sum_09 = array();
            $sum_10 = array();
            $sum_11 = array();
            $sum_12 = array();
            $sum_al = array();

            $tblarray = array(); 

	    	foreach( $items as $obj => $item )
	    	{
	    		$ttl_01 = isset( $item['01'] ) ? $item['01'] : 0;
	    		$ttl_02 = isset( $item['02'] ) ? $item['02'] : 0;
	    		$ttl_03 = isset( $item['03'] ) ? $item['03'] : 0;
	    		$ttl_04 = isset( $item['04'] ) ? $item['04'] : 0;
	    		$ttl_05 = isset( $item['05'] ) ? $item['05'] : 0;
	    		$ttl_06 = isset( $item['06'] ) ? $item['06'] : 0;
	    		$ttl_07 = isset( $item['07'] ) ? $item['07'] : 0;
	    		$ttl_08 = isset( $item['08'] ) ? $item['08'] : 0;
	    		$ttl_09 = isset( $item['09'] ) ? $item['09'] : 0;
	    		$ttl_10 = isset( $item['10'] ) ? $item['10'] : 0;
	    		$ttl_11 = isset( $item['11'] ) ? $item['11'] : 0;
	    		$ttl_12 = isset( $item['12'] ) ? $item['12'] : 0;
	    		$ttl_al = array_sum( $item );

                $sum_01[] = $ttl_01;
                $sum_02[] = $ttl_02;
                $sum_03[] = $ttl_03;
                $sum_04[] = $ttl_04;
                $sum_05[] = $ttl_05;
                $sum_06[] = $ttl_06;
                $sum_07[] = $ttl_07;
                $sum_08[] = $ttl_08;
                $sum_09[] = $ttl_09;
                $sum_10[] = $ttl_10;
                $sum_11[] = $ttl_11;
                $sum_12[] = $ttl_12;
                $sum_al[] = $ttl_al;

	    		if( $rview == 1 )
	    		{
	    			list( $code, $name, $sales_person ) = json_decode( base64_decode( $obj ) );

			    	$tblarray[] = array( 
			    		'code' => $code, 
			    		'name' => $name, 
			    		'ttl_01' => $ttl_01, 
			    		'ttl_02' => $ttl_02, 
			    		'ttl_03' => $ttl_03, 
			    		'ttl_04' => $ttl_04, 
			    		'ttl_05' => $ttl_05, 
			    		'ttl_06' => $ttl_06, 
			    		'ttl_07' => $ttl_07, 
			    		'ttl_08' => $ttl_08, 
			    		'ttl_09' => $ttl_09, 
			    		'ttl_10' => $ttl_10, 
			    		'ttl_11' => $ttl_11, 
			    		'ttl_12' => $ttl_12, 
			    		'ttl_al' => $ttl_al, 
			    		'sales_person' => $sales_person
			    	);
	    		}
	    		else
	    		{
	    			list( $code, $name ) = json_decode( base64_decode( $obj ) );

			    	$tblarray[] = array( 
			    		'code' => $code, 
			    		'name' => $name, 
			    		'ttl_01' => $ttl_01, 
			    		'ttl_02' => $ttl_02, 
			    		'ttl_03' => $ttl_03, 
			    		'ttl_04' => $ttl_04, 
			    		'ttl_05' => $ttl_05, 
			    		'ttl_06' => $ttl_06, 
			    		'ttl_07' => $ttl_07, 
			    		'ttl_08' => $ttl_08, 
			    		'ttl_09' => $ttl_09, 
			    		'ttl_10' => $ttl_10, 
			    		'ttl_11' => $ttl_11, 
			    		'ttl_12' => $ttl_12, 
			    		'ttl_al' => $ttl_al
			    	);
	    		}
	    	}

	    	array_multisort( array_map( function( $element ){
				return $element['ttl_al'];
			}, $tblarray ), SORT_DESC, $tblarray );

            $no = 1;

	    	foreach( $tblarray as $key => $val )
	    	{
	    		extract( $val );

	    		if( $rview == 1 )
	    		{
			    	$spcol = '<td align="center">' . $sales_person . '</td>';
				}
				else
				{
			    	$spcol = '';
				}

	    		$tblcontent .= '		    	
				<tr>
					<td align="center">' . $no . '</td>
					<td align="center">' . $code . '</td>
					<td>' . $name . '</td>
					<td align="right" bgcolor="rgb(216,228,188)">' . $ttl_01 . '</td>
					<td align="right" bgcolor="rgb(216,228,188)">' . $ttl_02 . '</td>
					<td align="right" bgcolor="rgb(216,228,188)">' . $ttl_03 . '</td>
					<td align="right" bgcolor="rgb(242,220,219)">' . $ttl_04 . '</td>
					<td align="right" bgcolor="rgb(242,220,219)">' . $ttl_05 . '</td>
					<td align="right" bgcolor="rgb(242,220,219)">' . $ttl_06 . '</td>
					<td align="right" bgcolor="rgb(220,230,241)">' . $ttl_07 . '</td>
					<td align="right" bgcolor="rgb(220,230,241)">' . $ttl_08 . '</td>
					<td align="right" bgcolor="rgb(220,230,241)">' . $ttl_09 . '</td>
					<td align="right" bgcolor="rgb(217,217,217)">' . $ttl_10 . '</td>
					<td align="right" bgcolor="rgb(217,217,217)">' . $ttl_11 . '</td>
					<td align="right" bgcolor="rgb(217,217,217)">' . $ttl_12 . '</td>
					<td align="right" bgcolor="rgb(204,192,218)">' . $ttl_al . '</td>
					' . $spcol . '
				</tr>';

				$no++;
	    	}

            $sum_01_prev = isset( $data['previous'][$last_year]['01'] ) ? $data['previous'][$last_year]['01'] : 0;
            $sum_02_prev = isset( $data['previous'][$last_year]['02'] ) ? $data['previous'][$last_year]['02'] : 0;
            $sum_03_prev = isset( $data['previous'][$last_year]['03'] ) ? $data['previous'][$last_year]['03'] : 0;
            $sum_04_prev = isset( $data['previous'][$last_year]['04'] ) ? $data['previous'][$last_year]['04'] : 0;
            $sum_05_prev = isset( $data['previous'][$last_year]['05'] ) ? $data['previous'][$last_year]['05'] : 0;
            $sum_06_prev = isset( $data['previous'][$last_year]['06'] ) ? $data['previous'][$last_year]['06'] : 0;
            $sum_07_prev = isset( $data['previous'][$last_year]['07'] ) ? $data['previous'][$last_year]['07'] : 0;
            $sum_08_prev = isset( $data['previous'][$last_year]['08'] ) ? $data['previous'][$last_year]['08'] : 0;
            $sum_09_prev = isset( $data['previous'][$last_year]['09'] ) ? $data['previous'][$last_year]['09'] : 0;
            $sum_10_prev = isset( $data['previous'][$last_year]['10'] ) ? $data['previous'][$last_year]['10'] : 0;
            $sum_11_prev = isset( $data['previous'][$last_year]['11'] ) ? $data['previous'][$last_year]['11'] : 0;
            $sum_12_prev = isset( $data['previous'][$last_year]['12'] ) ? $data['previous'][$last_year]['12'] : 0;
            $sum_al_prev = isset( $data['previous'][$last_year] ) ? array_sum( $data['previous'][$last_year] ) : 0;

            $sum_01_curr = array_sum( $sum_01 );
            $sum_02_curr = array_sum( $sum_02 );
            $sum_03_curr = array_sum( $sum_03 );
            $sum_04_curr = array_sum( $sum_04 );
            $sum_05_curr = array_sum( $sum_05 );
            $sum_06_curr = array_sum( $sum_06 );
            $sum_07_curr = array_sum( $sum_07 );
            $sum_08_curr = array_sum( $sum_08 );
            $sum_09_curr = array_sum( $sum_09 );
            $sum_10_curr = array_sum( $sum_10 );
            $sum_11_curr = array_sum( $sum_11 );
            $sum_12_curr = array_sum( $sum_12 );
            $sum_al_curr = array_sum( $sum_al );

            $sum_01_diff = $sum_01_curr - $sum_01_prev;
            $sum_02_diff = $sum_02_curr - $sum_02_prev;
            $sum_03_diff = $sum_03_curr - $sum_03_prev;
            $sum_04_diff = $sum_04_curr - $sum_04_prev;
            $sum_05_diff = $sum_05_curr - $sum_05_prev;
            $sum_06_diff = $sum_06_curr - $sum_06_prev;
            $sum_07_diff = $sum_07_curr - $sum_07_prev;
            $sum_08_diff = $sum_08_curr - $sum_08_prev;
            $sum_09_diff = $sum_09_curr - $sum_09_prev;
            $sum_10_diff = $sum_10_curr - $sum_10_prev;
            $sum_11_diff = $sum_11_curr - $sum_11_prev;
            $sum_12_diff = $sum_12_curr - $sum_12_prev;
            $sum_al_diff = $sum_al_curr - $sum_al_prev;

            $sum_01_perc = $sum_01_diff > 0 ? round( ( $sum_01_diff * 100 ) / $sum_01_curr, 2 ) : 0;
            $sum_02_perc = $sum_02_diff > 0 ? round( ( $sum_02_diff * 100 ) / $sum_02_curr, 2 ) : 0;
            $sum_03_perc = $sum_03_diff > 0 ? round( ( $sum_03_diff * 100 ) / $sum_03_curr, 2 ) : 0;
            $sum_04_perc = $sum_04_diff > 0 ? round( ( $sum_04_diff * 100 ) / $sum_04_curr, 2 ) : 0;
            $sum_05_perc = $sum_05_diff > 0 ? round( ( $sum_05_diff * 100 ) / $sum_05_curr, 2 ) : 0;
            $sum_06_perc = $sum_06_diff > 0 ? round( ( $sum_06_diff * 100 ) / $sum_06_curr, 2 ) : 0;
            $sum_07_perc = $sum_07_diff > 0 ? round( ( $sum_07_diff * 100 ) / $sum_07_curr, 2 ) : 0;
            $sum_08_perc = $sum_08_diff > 0 ? round( ( $sum_08_diff * 100 ) / $sum_08_curr, 2 ) : 0;
            $sum_09_perc = $sum_09_diff > 0 ? round( ( $sum_09_diff * 100 ) / $sum_09_curr, 2 ) : 0;
            $sum_10_perc = $sum_10_diff > 0 ? round( ( $sum_10_diff * 100 ) / $sum_10_curr, 2 ) : 0;
            $sum_11_perc = $sum_11_diff > 0 ? round( ( $sum_11_diff * 100 ) / $sum_11_curr, 2 ) : 0;
            $sum_12_perc = $sum_12_diff > 0 ? round( ( $sum_12_diff * 100 ) / $sum_12_curr, 2 ) : 0;
            $sum_al_perc = $sum_al_diff > 0 ? round( ( $sum_al_diff * 100 ) / $sum_al_curr, 2 ) : 0;

    		if( $rview == 1 )
    		{
		    	$spcol = '<td align="right">&nbsp;</td>';
		    	$data_sp_column = '<td rowspan="3" align="center"><b></b></td>';
			}
			else
			{
		    	$spcol = '';
		    	$data_sp_column = '';
			}

	    	$tblvar = '
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><b>%</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_01_perc . '</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_02_perc . '</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_03_perc . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_04_perc . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_05_perc . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_06_perc . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_07_perc . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_08_perc . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_09_perc . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_10_perc . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_11_perc . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_12_perc . '</b></td>
                <td align="right" bgcolor="rgb(204,192,218)"><b>' . $sum_al_perc . '</b></td>
                ' . $spcol . '
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><b>Variance</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_01_diff . '</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_02_diff . '</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_03_diff . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_04_diff . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_05_diff . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_06_diff . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_07_diff . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_08_diff . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_09_diff . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_10_diff . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_11_diff . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_12_diff . '</b></td>
                <td align="right" bgcolor="rgb(204,192,218)"><b>' . $sum_al_diff . '</b></td>
                ' . $spcol . '
            </tr>';

	    	$tblprevsum = '
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><b>' . $last_year . '</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_01_prev . '</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_02_prev . '</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_03_prev . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_04_prev . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_05_prev . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_06_prev . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_07_prev . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_08_prev . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_09_prev . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_10_prev . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_11_prev . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_12_prev . '</b></td>
                <td align="right" bgcolor="rgb(204,192,218)"><b>' . $sum_al_prev . '</b></td>
                ' . $spcol . '
            </tr>';

	    	$tblsumdata = '
            <tr>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_01_curr . '</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_02_curr . '</b></td>
                <td align="right" bgcolor="rgb(216,228,188)"><b>' . $sum_03_curr . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_04_curr . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_05_curr . '</b></td>
                <td align="right" bgcolor="rgb(242,220,219)"><b>' . $sum_06_curr . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_07_curr . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_08_curr . '</b></td>
                <td align="right" bgcolor="rgb(220,230,241)"><b>' . $sum_09_curr . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_10_curr . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_11_curr . '</b></td>
                <td align="right" bgcolor="rgb(217,217,217)"><b>' . $sum_12_curr . '</b></td>
                <td align="right" bgcolor="rgb(204,192,218)"><b>' . $sum_al_curr . '</b></td>
            </tr>';

	    	$tblcontent .= '		    	
			<tr>
				<td align="center" colspan="15"><b>GRAND TOTAL</b></td>
				<td align="right" bgcolor="rgb(73,73,85)" color="rgb(255,255,255)"><b>' . $sum_al_curr . '</b></td>
                ' . $spcol . '
			</tr>';

	    	add_variable( 'year', $year );
	    	add_variable( 'data_tbl', $tblcontent );
	    	add_variable( 'data_tbl_sum', $tblsumdata );
	    	add_variable( 'data_tbl_prev_sum', $tblprevsum );
	    	add_variable( 'data_sp_column', $data_sp_column );
	    	add_variable( 'data_tbl_variance_and_percent', $tblvar );
	    	add_variable( 'pagebreak', count( $data['items'] ) == $page ? '' : '<pagebreak>' );

        	parse_template( 'report-loop', 'r-loop', true );

        	$page++;
	    }

		add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

function ticket_seats_production_csv_report()
{
	$filter  = json_decode( base64_decode( $_GET['filter'] ), true );
	$content = get_seats_production_csv_report_data( $filter );

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=seats-production-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_seats_production_csv_report_data( $filter )
{
	extract( $filter );

	$data = get_seats_production_sum_report( $filter );

	if( !empty( $data ) )
	{
		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-seats-production.html', 'report' );
        add_block( 'report-loop', 'r-loop', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        $page = 1;

		foreach( $data['items'] as $year => $items )
	    {
		    $tblcontent = '';
		    $tblsumdata = '';
		    $last_year  = date( 'Y', strtotime( '-1 year', strtotime( $year . '-01-01' ) ) );

            $sum_01 = array();
            $sum_02 = array();
            $sum_03 = array();
            $sum_04 = array();
            $sum_05 = array();
            $sum_06 = array();
            $sum_07 = array();
            $sum_08 = array();
            $sum_09 = array();
            $sum_10 = array();
            $sum_11 = array();
            $sum_12 = array();
            $sum_al = array();

            $tblarray = array();

	    	foreach( $items as $obj => $item )
	    	{
	    	    
    	        // bayu edit
				$ttl_bmethod_by = '';
				if( isset($items[$obj]['bmethod_by']) ){
					for ($i = 0; $i <= 2; $i++) {
						if(	empty($items[$obj]['bmethod_by'][$i+1]) && empty($items[$obj]['bmethod_by'][$i+2])){
							( !empty($items[$obj]['bmethod_by'][$i]) && $items[$obj]['bmethod_by'][$i] != null) ? ($ttl_bmethod_by .= $items[$obj]['bmethod_by'][$i]) : "";
						}
						else{
							( !empty($items[$obj]['bmethod_by'][$i]) && $items[$obj]['bmethod_by'][$i] != null) ? ($ttl_bmethod_by .= $items[$obj]['bmethod_by'][$i].', ') : "";
						}
					}
				}
				if( !isset($items[$obj]['bmethod_by']) ){
						$ttl_bmethod_by = 'Admin';
				}
				
				$ttl_agpayment = '';
				if( isset($items[$obj]['agpayment_type']) ){
					for ($i = 0; $i <= 1; $i++) {
						if(	empty($items[$obj]['agpayment_type'][$i+1]) ){
							( !empty($items[$obj]['agpayment_type'][$i]) && $items[$obj]['agpayment_type'][$i] != null) ? ($ttl_agpayment .= $items[$obj]['agpayment_type'][$i]) : "";
						}
						if( !empty($items[$obj]['agpayment_type'][$i+1]) ){
							( !empty($items[$obj]['agpayment_type'][$i]) && $items[$obj]['agpayment_type'][$i] != null) ? ($ttl_agpayment .= $items[$obj]['agpayment_type'][$i].', ') : "";
						}
					}
				}

				$ttl_agpayment = !empty($ttl_agpayment)? $ttl_agpayment : 'prepaid';
				$ttl_pmcode = isset( $items[$obj]['pmcode'] ) && !empty($items[$obj]['pmcode'])? 'YES' : 'NO';
				// /akhirbayuedit
				
	    		$ttl_01 = isset( $item['01'] ) ? $item['01'] : 0;
	    		$ttl_02 = isset( $item['02'] ) ? $item['02'] : 0;
	    		$ttl_03 = isset( $item['03'] ) ? $item['03'] : 0;
	    		$ttl_04 = isset( $item['04'] ) ? $item['04'] : 0;
	    		$ttl_05 = isset( $item['05'] ) ? $item['05'] : 0;
	    		$ttl_06 = isset( $item['06'] ) ? $item['06'] : 0;
	    		$ttl_07 = isset( $item['07'] ) ? $item['07'] : 0;
	    		$ttl_08 = isset( $item['08'] ) ? $item['08'] : 0;
	    		$ttl_09 = isset( $item['09'] ) ? $item['09'] : 0;
	    		$ttl_10 = isset( $item['10'] ) ? $item['10'] : 0;
	    		$ttl_11 = isset( $item['11'] ) ? $item['11'] : 0;
	    		$ttl_12 = isset( $item['12'] ) ? $item['12'] : 0;
	    		$ttl_al = array_sum( $item );

                $sum_01[] = $ttl_01;
                $sum_02[] = $ttl_02;
                $sum_03[] = $ttl_03;
                $sum_04[] = $ttl_04;
                $sum_05[] = $ttl_05;
                $sum_06[] = $ttl_06;
                $sum_07[] = $ttl_07;
                $sum_08[] = $ttl_08;
                $sum_09[] = $ttl_09;
                $sum_10[] = $ttl_10;
                $sum_11[] = $ttl_11;
                $sum_12[] = $ttl_12;
                $sum_al[] = $ttl_al;

	    		if( $rview == 1 )
	    		{
	    		    add_variable( 'head_filter', 'Sales Person' );
	    		    
	    			list( $code, $name ) = json_decode( base64_decode( $obj ) );
	    			
					$ttl_sales_person = isset( $items[$obj]['sales_person'] ) ||  $items[$obj]['sales_person'] == 'NULL' ? $items[$obj]['sales_person'] : 'Office';
					$ttl_bmethod_by = $code == 'ONL' ? 'Direct Online' : $ttl_bmethod_by;
			    	$tblarray[] = array( 
			    		'code' => $code, 
			    		'name' => $name, 
			    		'ttl_01' => $ttl_01, 
			    		'ttl_02' => $ttl_02, 
			    		'ttl_03' => $ttl_03, 
			    		'ttl_04' => $ttl_04, 
			    		'ttl_05' => $ttl_05, 
			    		'ttl_06' => $ttl_06, 
			    		'ttl_07' => $ttl_07, 
			    		'ttl_08' => $ttl_08, 
			    		'ttl_09' => $ttl_09, 
			    		'ttl_10' => $ttl_10, 
			    		'ttl_11' => $ttl_11, 
			    		'ttl_12' => $ttl_12, 
			    		'ttl_al' => $ttl_al, 
			    		//bayu edit
			    		'ttl_sales_person' => $ttl_sales_person,
			    		'ttl_bmethod_by' => $ttl_bmethod_by,
						'ttl_agpayment' => $ttl_agpayment,
						'ttl_pmcode' =>$ttl_pmcode
						//akhir bayu edit
			    	);
	    		}
	    		else
	    		{
					list( $code, $name) = json_decode( base64_decode( $obj ) );
                    $ttl_bmethod_by = $code == 'ONL' ? 'Direct Online' : $ttl_bmethod_by;
			    	$tblarray[] = array(
			    		'code' => $code,
			    		'name' => $name,
			    		'ttl_01' => $ttl_01,
			    		'ttl_02' => $ttl_02,
			    		'ttl_03' => $ttl_03,
			    		'ttl_04' => $ttl_04,
			    		'ttl_05' => $ttl_05,
			    		'ttl_06' => $ttl_06,
			    		'ttl_07' => $ttl_07,
			    		'ttl_08' => $ttl_08,
			    		'ttl_09' => $ttl_09,
			    		'ttl_10' => $ttl_10,
			    		'ttl_11' => $ttl_11,
			    		'ttl_12' => $ttl_12,
			    		'ttl_al' => $ttl_al,
						'ttl_bmethod_by' => $ttl_bmethod_by,
						'ttl_agpayment' => $ttl_agpayment,
						'ttl_pmcode' =>$ttl_pmcode
			    	);
	    		}
	    	}

	    	array_multisort( array_map( function( $element ){
				return $element['ttl_al'];
			}, $tblarray ), SORT_DESC, $tblarray );

            $no = 1;

	    	foreach( $tblarray as $key => $val )
	    	{
	    		extract( $val );

	    		if( $rview == 1 )
	    		{
			    	$spcol = '<td>' . $ttl_sales_person . '</td>';
				}
				else
				{
			    	$spcol = '<td></td>';
				}

	    		$tblcontent .= '	
		    	<tr>
					<td>' . $no . '</td>
					<td>' . $code . '</td>
					<td>' . $name . '</td>
					<td>' . $ttl_01 . '</td>
					<td>' . $ttl_02 . '</td>
					<td>' . $ttl_03 . '</td>
					<td>' . $ttl_04 . '</td>
					<td>' . $ttl_05 . '</td>
					<td>' . $ttl_06 . '</td>
					<td>' . $ttl_07 . '</td>
					<td>' . $ttl_08 . '</td>
					<td>' . $ttl_09 . '</td>
					<td>' . $ttl_10 . '</td>
					<td>' . $ttl_11 . '</td>
					<td>' . $ttl_12 . '</td>
					<td>' . $ttl_al . '</td>
					' . $spcol . '
				    <td>' . $ttl_bmethod_by . '</td>
					<td>' . $ttl_agpayment . '</td>
					<td>' . $ttl_pmcode . '</td>
				</tr>';

				$no++;
	    	}

            $sum_01_prev = isset( $data['previous'][$last_year]['01'] ) ? $data['previous'][$last_year]['01'] : 0;
            $sum_02_prev = isset( $data['previous'][$last_year]['02'] ) ? $data['previous'][$last_year]['02'] : 0;
            $sum_03_prev = isset( $data['previous'][$last_year]['03'] ) ? $data['previous'][$last_year]['03'] : 0;
            $sum_04_prev = isset( $data['previous'][$last_year]['04'] ) ? $data['previous'][$last_year]['04'] : 0;
            $sum_05_prev = isset( $data['previous'][$last_year]['05'] ) ? $data['previous'][$last_year]['05'] : 0;
            $sum_06_prev = isset( $data['previous'][$last_year]['06'] ) ? $data['previous'][$last_year]['06'] : 0;
            $sum_07_prev = isset( $data['previous'][$last_year]['07'] ) ? $data['previous'][$last_year]['07'] : 0;
            $sum_08_prev = isset( $data['previous'][$last_year]['08'] ) ? $data['previous'][$last_year]['08'] : 0;
            $sum_09_prev = isset( $data['previous'][$last_year]['09'] ) ? $data['previous'][$last_year]['09'] : 0;
            $sum_10_prev = isset( $data['previous'][$last_year]['10'] ) ? $data['previous'][$last_year]['10'] : 0;
            $sum_11_prev = isset( $data['previous'][$last_year]['11'] ) ? $data['previous'][$last_year]['11'] : 0;
            $sum_12_prev = isset( $data['previous'][$last_year]['12'] ) ? $data['previous'][$last_year]['12'] : 0;
            $sum_al_prev = isset( $data['previous'][$last_year] ) ? array_sum( $data['previous'][$last_year] ) : 0;

            $sum_01_curr = array_sum( $sum_01 );
            $sum_02_curr = array_sum( $sum_02 );
            $sum_03_curr = array_sum( $sum_03 );
            $sum_04_curr = array_sum( $sum_04 );
            $sum_05_curr = array_sum( $sum_05 );
            $sum_06_curr = array_sum( $sum_06 );
            $sum_07_curr = array_sum( $sum_07 );
            $sum_08_curr = array_sum( $sum_08 );
            $sum_09_curr = array_sum( $sum_09 );
            $sum_10_curr = array_sum( $sum_10 );
            $sum_11_curr = array_sum( $sum_11 );
            $sum_12_curr = array_sum( $sum_12 );
            $sum_al_curr = array_sum( $sum_al );

            $sum_01_diff = $sum_01_curr - $sum_01_prev;
            $sum_02_diff = $sum_02_curr - $sum_02_prev;
            $sum_03_diff = $sum_03_curr - $sum_03_prev;
            $sum_04_diff = $sum_04_curr - $sum_04_prev;
            $sum_05_diff = $sum_05_curr - $sum_05_prev;
            $sum_06_diff = $sum_06_curr - $sum_06_prev;
            $sum_07_diff = $sum_07_curr - $sum_07_prev;
            $sum_08_diff = $sum_08_curr - $sum_08_prev;
            $sum_09_diff = $sum_09_curr - $sum_09_prev;
            $sum_10_diff = $sum_10_curr - $sum_10_prev;
            $sum_11_diff = $sum_11_curr - $sum_11_prev;
            $sum_12_diff = $sum_12_curr - $sum_12_prev;
            $sum_al_diff = $sum_al_curr - $sum_al_prev;

            $sum_01_perc = $sum_01_diff > 0 ? round( ( $sum_01_diff * 100 ) / $sum_01_curr, 2 ) : 0;
            $sum_02_perc = $sum_02_diff > 0 ? round( ( $sum_02_diff * 100 ) / $sum_02_curr, 2 ) : 0;
            $sum_03_perc = $sum_03_diff > 0 ? round( ( $sum_03_diff * 100 ) / $sum_03_curr, 2 ) : 0;
            $sum_04_perc = $sum_04_diff > 0 ? round( ( $sum_04_diff * 100 ) / $sum_04_curr, 2 ) : 0;
            $sum_05_perc = $sum_05_diff > 0 ? round( ( $sum_05_diff * 100 ) / $sum_05_curr, 2 ) : 0;
            $sum_06_perc = $sum_06_diff > 0 ? round( ( $sum_06_diff * 100 ) / $sum_06_curr, 2 ) : 0;
            $sum_07_perc = $sum_07_diff > 0 ? round( ( $sum_07_diff * 100 ) / $sum_07_curr, 2 ) : 0;
            $sum_08_perc = $sum_08_diff > 0 ? round( ( $sum_08_diff * 100 ) / $sum_08_curr, 2 ) : 0;
            $sum_09_perc = $sum_09_diff > 0 ? round( ( $sum_09_diff * 100 ) / $sum_09_curr, 2 ) : 0;
            $sum_10_perc = $sum_10_diff > 0 ? round( ( $sum_10_diff * 100 ) / $sum_10_curr, 2 ) : 0;
            $sum_11_perc = $sum_11_diff > 0 ? round( ( $sum_11_diff * 100 ) / $sum_11_curr, 2 ) : 0;
            $sum_12_perc = $sum_12_diff > 0 ? round( ( $sum_12_diff * 100 ) / $sum_12_curr, 2 ) : 0;
            $sum_al_perc = $sum_al_diff > 0 ? round( ( $sum_al_diff * 100 ) / $sum_al_curr, 2 ) : 0;

    		if( $rview == 1 )
    		{
		    	$spcol = '<td></td>';
		    	$data_sp_column = '<td>Sales Person</td>';
			}
			else
			{
		    	$spcol = '';
		    	$data_sp_column = '';
			}

	    	$tblvar = '
	    	<tr>
				<td></td>
				<td></td>
				<td>%</td>
                <td>' . $sum_01_perc . '</td>
                <td>' . $sum_02_perc . '</td>
                <td>' . $sum_03_perc . '</td>
                <td>' . $sum_04_perc . '</td>
                <td>' . $sum_05_perc . '</td>
                <td>' . $sum_06_perc . '</td>
                <td>' . $sum_07_perc . '</td>
                <td>' . $sum_08_perc . '</td>
                <td>' . $sum_09_perc . '</td>
                <td>' . $sum_10_perc . '</td>
                <td>' . $sum_11_perc . '</td>
                <td>' . $sum_12_perc . '</td>
                <td>' . $sum_al_perc . '</td>
                ' . $spcol . '
                <td></td>
                <td></td>
                <td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Variance</td>
                <td>' . $sum_01_diff . '</td>
                <td>' . $sum_02_diff . '</td>
                <td>' . $sum_03_diff . '</td>
                <td>' . $sum_04_diff . '</td>
                <td>' . $sum_05_diff . '</td>
                <td>' . $sum_06_diff . '</td>
                <td>' . $sum_07_diff . '</td>
                <td>' . $sum_08_diff . '</td>
                <td>' . $sum_09_diff . '</td>
                <td>' . $sum_10_diff . '</td>
                <td>' . $sum_11_diff . '</td>
                <td>' . $sum_12_diff . '</td>
                <td>' . $sum_al_diff . '</td>
                ' . $spcol . '
                <td></td>
                <td></td>
                <td></td>
			</tr>';

	    	$tblprevsum = '
			<tr>
				<td></td>
				<td></td>
				<td>' . $last_year . '</td>
                <td>' . $sum_01_prev . '</td>
                <td>' . $sum_02_prev . '</td>
                <td>' . $sum_03_prev . '</td>
                <td>' . $sum_04_prev . '</td>
                <td>' . $sum_05_prev . '</td>
                <td>' . $sum_06_prev . '</td>
                <td>' . $sum_07_prev . '</td>
                <td>' . $sum_08_prev . '</td>
                <td>' . $sum_09_prev . '</td>
                <td>' . $sum_10_prev . '</td>
                <td>' . $sum_11_prev . '</td>
                <td>' . $sum_12_prev . '</td>
                <td>' . $sum_al_prev . '</td>
                ' . $spcol . '
			</tr>';

	    	$tblsumdata = '
	    	<tr>
				<td></td>
				<td></td>
				<td></td>
                <td>' . $sum_01_curr . '</td>
                <td>' . $sum_02_curr . '</td>
                <td>' . $sum_03_curr . '</td>
                <td>' . $sum_04_curr . '</td>
                <td>' . $sum_05_curr . '</td>
                <td>' . $sum_06_curr . '</td>
                <td>' . $sum_07_curr . '</td>
                <td>' . $sum_08_curr . '</td>
                <td>' . $sum_09_curr . '</td>
                <td>' . $sum_10_curr . '</td>
                <td>' . $sum_11_curr . '</td>
                <td>' . $sum_12_curr . '</td>
                <td>' . $sum_al_curr . '</td>
                ' . $spcol . '
			</tr>';

	    	$tblcontent .= '	    	
			<tr>
				<td>GRAND TOTAL</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>' . $sum_al_curr . '</td>
                ' . $spcol . '
			</tr>';

	    	add_variable( 'year', $year );
	    	add_variable( 'data_tbl', $tblcontent );
	    	add_variable( 'data_tbl_sum', $tblsumdata );
	    	add_variable( 'data_tbl_prev_sum', $tblprevsum );
	    	add_variable( 'data_sp_column', $data_sp_column );
	    	add_variable( 'data_tbl_variance_and_percent', $tblvar );
	    	add_variable( 'pagebreak', count( $data['items'] ) == $page ? '' : '<pagebreak>' );

        	parse_template( 'report-loop', 'r-loop', true );

        	$page++;
	    }

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Revenue Export
| -------------------------------------------------------------------------------------
*/
function ticket_revenue_pdf_report()
{
	$content = get_revenue_pdf_report_data();

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Revenue', 'revenue-report' );

	    if( isset( $_GET['print'] ) && $_GET['print']  )
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Revenue' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
	    	$mpdf->SetJS( 'this.print();' );
			$mpdf->Output( 'revenue-report.pdf', 'I' );
	    }
	    else
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Revenue' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
			$mpdf->Output( 'revenue-report.pdf', 'D' );
	    }
	}
}

function get_revenue_pdf_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_ticket_revenue_report( $filter );

	if( !empty( $data ) )
	{
		extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-revenue.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        if ( $rtype == '4' ) 
        {
        	$charttyp = 'column';
        	foreach ( $items as $obj => $groups ) 
	    	{
	    		list( $rev_year, $total_years, $total_percent_per_year, $booking_year ) = json_decode( base64_decode( $obj ) );	 	

	    		$tblcontent = '';
				$frev_date  = $rev_year;
				
	    		foreach ( $groups as $val ) 
	    		{    			
	    			$tblcontent .= '
					<div class="item">
						<div class="head clearfix">
							<div class="col-md-6">
								<p>' . $val['bmonth'] .'</p>
							</div>
							<div class="col-md-6">
								<p class="text-right medium-bold">' . $val['mtotal'] . ' (' . $val['mavg']  . '%)</p>
							</div>
						</div>
					</div>';
	    		}

	    		add_variable( 'rev_date', $frev_date );
		    	add_variable( 'rev_data', $tblcontent );
		    	add_variable( 'total_percent_revenue_by_date', $total_percent_per_year );
		    	add_variable( 'total_revenue_by_date', $total_years );

	        	parse_template( 'report-loop-block', 'rl-block', true );
	    	}
        } 
        else 
        {
        	foreach( $items as $obj => $groups )
		    {
		    	list( $rev_date, $total_revenue_by_date, $total_percent_revenue_by_date, $booking_by_date ) = json_decode( base64_decode( $obj ) );
				
				$tblcontent = '';
				$frev_date  = date( 'd M Y', strtotime( $rev_date ) );

		    	foreach( $groups as $channel => $group )
				{
					list( $chcode, $chname, $total_revenue_by_channel, $total_percent_revenue_by_channel, $booking_by_channel ) = json_decode( base64_decode( $channel ) );

					$tblcontent .= '
					<div class="item">
						<div class="head clearfix">
							<div class="col-md-6">
								<p>' . $chname .' [' . $chcode . ']</p>
							</div>
							<div class="col-md-6">
								<p class="text-right medium-bold">' . $total_revenue_by_channel . ' / ' . $booking_by_channel . ' booking (' . $total_percent_revenue_by_channel . '%)</p>
							</div>
						</div>
						<div class="table-block">
							<div class="table">
								<div class="t-header">
									<p class="date">Date</p>
									<p class="ticket-large">Resv #</p>
									<p class="source">Booking Source</p>
									<p class="amount-large">Amount</p>
								</div>
								<div class="t-content">';

									foreach( $group as $d )
									{
										$tblcontent .= '
										<div class="item">
											<p class="date">' . date( 'd F Y', strtotime( $d['bdate'] ) ) . '</p>
											<p class="ticket-large">Booking #' . $d['bticket'] .'</p>
											<p class="source">(' . $d['chcode'] . ') ' . $d['agname'] .'</p>
											<p class="amount-large">' . $d['btotal'] .'</p>
										</div>';
				                    }

				                    $tblcontent .= '
					            </div>
					        </div>
						</div>
					</div>';
				}

		    	add_variable( 'rev_date', $frev_date );
		    	add_variable( 'rev_data', $tblcontent );
		    	add_variable( 'total_percent_revenue_by_date', $total_percent_revenue_by_date );
		    	add_variable( 'total_revenue_by_date', $total_revenue_by_date );
		    	add_variable( 'total_booking_by_date', $booking_by_date );

	        	parse_template( 'report-loop-block', 'rl-block', true );
			}
        }

	    add_variable( 'total', $total );
	    add_variable( 'booking', $booking );
	    add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
    }
}

function ticket_revenue_csv_report()
{
	$content = get_revenue_csv_report_data();

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=revenue-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_revenue_csv_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_ticket_revenue_report( $filter );

	if( !empty( $data ) )
    {
        extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-revenue.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        foreach( $items as $obj => $groups )
	    {
	    	list( $rev_date, $total_revenue_by_date, $total_percent_revenue_by_date ) = json_decode( base64_decode( $obj ) );
				
			$tblcontent = '';
			$frev_date  = date( 'd M Y', strtotime( $rev_date ) );

	    	foreach( $groups as $channel => $group )
			{
				list( $chcode, $chname, $total_revenue_by_channel, $total_percent_revenue_by_channel, $count ) = json_decode( base64_decode( $channel ) );

				$tblcontent .= '
				<tr>
					<td>' . $chname .' [' . $chcode . ']</td>
					<td></td>
					<td>' . str_replace( '.', '', $total_revenue_by_channel ) . '</td>
					<td>' . $total_percent_revenue_by_channel . '%</td>
				</tr>
                <tr>
                    <td>Resv #</td>
                    <td>Booking Source</td>
                    <td>Amount</td>
                </tr>';

            	foreach( $group as $i => $d )
				{
					$tblcontent .= '
					<tr>
                        <td>Booking #' . $d['bticket'] .'</td>
                        <td>(' . $d['chcode'] . ') ' . $d['agname'] . '</td>
                        <td>' . str_replace( '.', '', $d['btotal'] ) . '</td>
                    </tr>';
                }
			}

	    	add_variable( 'rev_date', $frev_date );
	    	add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'total_percent_revenue_by_date', $total_percent_revenue_by_date );
	    	add_variable( 'total_revenue_by_date', str_replace( '.', '', $total_revenue_by_date ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
	    }

	    add_variable( 'total', str_replace( '.', '', $total ) );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Average Rates Export
| -------------------------------------------------------------------------------------
*/
function ticket_average_rates_pdf_report()
{
	$content = get_average_rates_pdf_report_data();

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Average Rates' );

	    if( isset( $_GET['print'] ) && $_GET['print']  )
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Average Rates' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
	    	$mpdf->SetJS( 'this.print();' );
			$mpdf->Output( 'average-rates-report.pdf', 'I' );
	    }
	    else
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Average Rates' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
			$mpdf->Output( 'average-rates-report.pdf', 'D' );
	    }
	}
}

function get_average_rates_pdf_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_ticket_average_rates_report( $filter );

	if( !empty( $data ) )
	{
		extract( $data );
		extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-average-rates.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

		foreach( $items as $year => $groups )
	    {
			$tblcontent = '';
			$average    = $total_price_per_year[$year] / $total_seat_per_year[$year];

			foreach( $groups as $month => $d )
			{
				$tblcontent .= '
				<div class="item clearfix">                               
                    <div class="col-md-6">
                        <p>' . $month . '</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-right medium-bold">
                        	' . number_format( $d['avgprice'], 0, '.' , '.' ) . '
                        </p>
                    </div>
				</div>';
			}

	    	add_variable( 'rates_year', $year );
	    	add_variable( 'rates_data', $tblcontent );
	    	add_variable( 'average', number_format( $average , 0, '.' , '.' ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
	    }

	    add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
	    add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
    }
}

function ticket_average_rates_csv_report()
{
	$content = get_average_rates_csv_report_data();

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=average-rates-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_average_rates_csv_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_ticket_average_rates_report( $filter );

	if( !empty( $data ) )
    {
        extract( $data );
		extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-average-rates.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

		foreach( $items as $year => $groups )
	    {
			$tblcontent = '';
			$average    = $total_price_per_year[$year] / $total_seat_per_year[$year];

			foreach( $groups as $month => $d )
			{
				$tblcontent .= '
				<tr>
                    <td>' . $month . '</td>
                    <td>' . number_format( $d['avgprice'] , 0, '.' , ',' ) . '</td>
                </tr>';
			}

	    	add_variable( 'rates_year', $year );
	    	add_variable( 'rates_data', $tblcontent );
	    	add_variable( 'average', number_format( $average, 0, '.' , ',' ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
	    }

	    add_variable( 'total', number_format( $total, 0, '.' , ',' ) );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - HS sales sum Export
| -------------------------------------------------------------------------------------
*/
function ticket_hs_sales_sum_print_report()
{
	$site_url = site_url();

    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );
	add_actions( 'include_js', 'get_javascript', 'jquery.base64.min' );
	add_actions( 'include_js', 'get_javascript_inc', 'jquery-ui-1.12.1/jquery-ui.min.js' );
	add_actions( 'include_js', 'get_custom_javascript', HTSERVER . $site_url . '/l-admin/themes/custom/js/scripts.js' );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-hs-sales-sum.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'admurl', get_admin_url() );
	add_variable( 'filter', $_GET['filter'] );
    add_variable( 'web_title', get_meta_data('web_title') );
    add_variable( 'section_title', 'Report - Hi-Season Sales Summary' );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_hs_sales_sum_pdf_report()
{
	$content = get_hs_sales_sum_pdf_report_data();

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Hi-Season Sales Summary' );

	    if( isset( $_GET['print'] ) && $_GET['print']  )
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Hi-Season Sales Summary' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
	    	$mpdf->SetJS( 'this.print();' );
			$mpdf->Output( 'hs-sales-sum-report.pdf', 'I' );
	    }
	    else
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Hi-Season Sales Summary' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
			$mpdf->Output( 'hs-sales-sum-report.pdf', 'D' );
	    }
	}
}

function get_hs_sales_sum_pdf_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_ticket_hs_sales_sum_report( $filter );

	if( empty( $data ) === false )
	{
		extract( $data );
		extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-hs-sales-sum.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

	    foreach( $items as $bmonth => $dt_bbm )
	    {
            $total_bbm_num = $total_bbm[ $bmonth ];
            $total_bbm_prc = round( ( $total_bbm_num * 100 ) / $total, 2 );

			$tblcontent = '';

	    	foreach( $dt_bbm['list'] as $bdmonth => $dt_bdm )
			{
	            $total_bdm_num = $total_bdm[ $bmonth ][ $bdmonth ];
	            $total_bdm_prc = round( ( $total_bdm_num * 100 ) / $total_bbm_num, 2 );

				$tblcontent .= '
				<div class="item">
					<div class="rs-head clearfix">
						<div class="col-md-6">
							<p>' . $bdmonth .' Departure</p>
						</div>
						<div class="col-md-6">
							<p class="text-right medium-bold">' . $total_bdm_num . ' (' . $total_bdm_prc . '%)</p>
						</div>
					</div>
					<div class="sub-content">';

						foreach( $dt_bdm['list'] as $port_type => $dt_btt )
						{
				            $total_btt_num = $total_btt[ $bmonth ][ $bdmonth ][ $port_type ];
				            $total_btt_prc = round( ( $total_btt_num * 100 ) / $total_bdm_num, 2 );

							$tblcontent .= '
							<div class="rs-sub-head clearfix">
								<div class="col-md-6">
									<p>' . $port_type .'</p>
								</div>
								<div class="col-md-6">
									<p class="text-right medium-bold">' . $total_btt_num . ' (' . $total_btt_prc . '%)</p>
								</div>
                            </div>
                            <div class="rs-table">';

								foreach( $dt_btt as $port => $dt_bpt )
								{
						            $total_bpt_num = $total_bpt[ $bmonth ][ $bdmonth ][ $port_type ][ $port ];
						            $total_bpt_prc = round( ( $total_bpt_num * 100 ) / $total_btt_num, 2 );

									$tblcontent .= '
									<div class="sub-more-content">
										<div class="rs-more-sub-head clearfix">
											<div class="col-md-6">
												<p>' . $port . '</p>
											</div>
											<div class="col-md-6">
												<p class="text-right medium-bold">' . $total_bpt_num . ' (' . $total_bpt_prc . '%)</p>
											</div>
										</div>
										<div class="table-block">
											<div class="table">
												<div class="t-content">';

													foreach( $dt_bpt as $trip => $dt_btn )
													{
											            $total_btn_num = $total_btn[ $bmonth ][ $bdmonth ][ $port_type ][ $port ][ $trip ];
											            $total_btn_prc = round( ( $total_btn_num * 100 ) / $total_bpt_num, 2 );

														$tblcontent .= '
														<div class="item clearfix">
															<p class="trip-name">- ' . $trip . '</p>
															<p class="trip-num">' . $total_btn_num . ' (' . $total_btn_prc . '%)</p>
														</div>';
								                    }

								                    $tblcontent .= '
									            </div>
									        </div>
										</div>
									</div>';
								}

								$tblcontent .= '
							</div>';
						}
							
						$tblcontent .= '
					</div>
				</div>';
			}

	    	add_variable( 'rev_month', $bmonth );
	    	add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'total_by_booking_month', $total_bbm_num );
	    	add_variable( 'total_percent_by_booking_month', $total_bbm_prc );

        	parse_template( 'report-loop-block', 'rl-block', true );
	    }

	    add_variable( 'total', $total );
	    add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

function ticket_hs_sales_sum_csv_report()
{
	$content = get_hs_sales_sum_csv_report_data();

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=hs-sales-sum-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_hs_sales_sum_csv_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_ticket_hs_sales_sum_report( $filter );

	if( empty( $data ) === false )
    {
		extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-hs-sales-sum.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

	    foreach( $items as $bmonth => $dt_bbm )
	    {
            $total_bbm_num = $total_bbm[ $bmonth ];
            $total_bbm_prc = round( ( $total_bbm_num * 100 ) / $total, 2 );
			
			$tblcontent = '';

	    	foreach( $dt_bbm['list'] as $bdmonth => $dt_bdm )
			{
	            $total_bdm_num = $total_bdm[ $bmonth ][ $bdmonth ];
	            $total_bdm_prc = round( ( $total_bdm_num * 100 ) / $total_bbm_num, 2 );

				$tblcontent .= '
                <tr>
                    <td>' . $bdmonth .' Departure</td>
                    <td>' . $total_bdm_num . ' (' . $total_bdm_prc . '%)</td>
                </tr>';

				foreach( $dt_bdm['list'] as $port_type => $dt_btt )
				{
		            $total_btt_num = $total_btt[ $bmonth ][ $bdmonth ][ $port_type ];
		            $total_btt_prc = round( ( $total_btt_num * 100 ) / $total_bdm_num, 2 );

					$tblcontent .= '
	                <tr>
	                    <td>' . $port_type .'</td>
	                    <td>' . $total_btt_num . ' (' . $total_btt_prc . '%)</td>
	                </tr>';

					foreach( $dt_btt as $port => $dt_bpt )
					{
			            $total_bpt_num = $total_bpt[ $bmonth ][ $bdmonth ][ $port_type ][ $port ];
			            $total_bpt_prc = round( ( $total_bpt_num * 100 ) / $total_btt_num, 2 );

						$tblcontent .= '
		                <tr>
		                    <td>' . $port . '</td>
		                    <td>' . $total_bpt_num . ' (' . $total_bpt_prc . '%)</td>
		                </tr>';

						foreach( $dt_bpt as $trip => $dt_btn )
						{
				            $total_btn_num = $total_btn[ $bmonth ][ $bdmonth ][ $port_type ][ $port ][ $trip ];
				            $total_btn_prc = round( ( $total_btn_num * 100 ) / $total_bpt_num, 2 );

							$tblcontent .= '
			                <tr>
			                    <td>- ' . $trip . '</td>
			                    <td>' . $total_btn_num . ' (' . $total_btn_prc . '%)</td>
			                </tr>';
	                    }
					}
				}
			}

	    	add_variable( 'rev_month', $bmonth );
	    	add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'total_by_booking_month', $total_bbm_num );
	    	add_variable( 'total_percent_by_booking_month', $total_bbm_prc );

        	parse_template( 'report-loop-block', 'rl-block', true );
	    }

	    add_variable( 'total', $total );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Market Segment Export
| -------------------------------------------------------------------------------------
*/
function ticket_market_segment_pdf_report()
{
	$content = get_market_segment_pdf_report_data();

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Market Segment' );

	    if( isset( $_GET['print'] ) && $_GET['print']  )
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 30, 15 );
		    $mpdf->SetTitle( 'Report - Market Segment' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
	    	$mpdf->SetJS( 'this.print();' );
			$mpdf->Output( 'market-segment-report.pdf', 'I' );
	    }
	    else
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Report - Market Segment' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
			$mpdf->Output( 'market-segment.pdf', 'D' );
	    }
	}
}

function get_market_segment_pdf_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_ticket_market_segment_report( $filter );

	if( !empty( $data ) )
	{
		extract( $data );
		extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-market-segment.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

	    foreach( $items as $obj => $groups )
	    {
	    	list( $rev_month, $total_revenue_by_month, $total_percent_revenue_by_month ) = json_decode( base64_decode( $obj ) );
			
			$tblcontent = '';

	    	foreach( $groups as $channel => $group )
			{
				list( $chcode, $chname, $total_revenue_by_channel, $total_percent_revenue_by_channel ) = json_decode( base64_decode( $channel ) );

				$tblcontent .= '
				<div class="item">
					<div class="head clearfix">
						<div class="col-md-6">
							<p>' . $chname .' [' . $chcode . ']</p>
						</div>
						<div class="col-md-6">
							<p class="text-right medium-bold">' . number_format( $total_revenue_by_channel, 0, '.' , '.' ) . ' (' . round( ( $total_revenue_by_channel * 100 ) / $total, 2 ) . '%)</p>
						</div>
					</div>
					<div class="table-block">
						<div class="table">
							<div class="t-header">
								<p class="date">Date</p>
								<p class="ticket-large">Resv #</p>
								<p class="source">Booking Source</p>
								<p class="amount-large">Amount</p>
							</div>
							<div class="t-content">';

								foreach( $group as $d )
								{
									$tblcontent .= '
									<div class="item">
										<p class="date">' . date( 'd F Y', strtotime( $d['bdate'] ) ) . '</p>
										<p class="ticket-large">Booking #' . $d['bticket'] .'</p>
										<p class="source">(' . $d['chcode'] . ') ' . $d['chname'] .'</p>
										<p class="amount-large">' . number_format( $d['btotal'], 0, '.' , '.' ) .'</p>
									</div>';
			                    }

			                    $tblcontent .= '
				            </div>
				        </div>
					</div>
				</div>';
			}

	    	add_variable( 'rev_month', $rev_month );
	    	add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'total_percent_by_booking_month', $total_percent_revenue_by_month );
	    	add_variable( 'total_by_booking_month',  number_format( $total_revenue_by_month, 0, '.' , '.' ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', number_format( $total, 0, '.' , '.' ) );
	    add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

function ticket_market_segment_csv_report()
{
	$content = get_market_segment_csv_report_data();

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=market-segment-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_market_segment_csv_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_ticket_market_segment_report( $filter );

	if( !empty( $data ) )
    {
		extract( $data );
		extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-market-segment.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        foreach( $items as $obj => $groups )
	    {
	    	list( $rev_month, $total_revenue_by_month, $total_percent_revenue_by_month ) = json_decode( base64_decode( $obj ) );
				
			$tblcontent = '';

	    	foreach( $groups as $channel => $group )
			{
				list( $chcode, $chname, $total_revenue_by_channel, $total_percent_revenue_by_channel ) = json_decode( base64_decode( $channel ) );

				$tblcontent .= '
                <tr>
                    <td>' . $chname .' [' . $chcode . ']</td>
                    <td></td>
                    <td></td>
                    <td>' . number_format( $total_revenue_by_channel, 0, '.' , ',' ) . ' (' . round( ( $total_revenue_by_channel * 100 ) / $total, 2 ) . '%)</td>
                </tr>
                <tr>
                    <th>Date</th>
                    <th>Resv #</th>
                    <th>Booking Source</th>
                    <th>Amount</th>
                </tr>';

	        	foreach( $group as $i => $d )
				{
					$tblcontent .= '
					<tr>
	                    <td>- ' . date( 'd F Y', strtotime( $d['bdate'] ) ) . '</td>
	                    <td>Booking #' . $d['bticket'] .'</td>
	                    <td>(' . $d['chcode'] . ') ' . $d['chname'] . '</td>
	                    <td>' . number_format( $d['btotal'], 0, '.' , ',' ) . '</td>
	                </tr>';
	            }
			}

	    	add_variable( 'rev_month', $rev_month );
	    	add_variable( 'rev_data', $tblcontent );
	    	add_variable( 'total_percent_by_booking_month', $total_percent_revenue_by_month );
	    	add_variable( 'total_by_booking_month',  number_format( $total_revenue_by_month, 0, '.' , ',' ) );

        	parse_template( 'report-loop-block', 'rl-block', true );
		}

	    add_variable( 'total', number_format( $total, 0, '.' , ',' ) );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Port Clearance Export
| -------------------------------------------------------------------------------------
*/
function ticket_port_clearance_pdf_report()
{
	$content = get_port_clearance_pdf_report_data( $_GET['id'] );

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Port Clearance Document' );

	    if( isset( $_GET['print'] ) && $_GET['print']  )
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 30, 15 );
		    $mpdf->SetTitle( 'Port Clearance Document' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
	    	$mpdf->SetJS( 'this.print();' );
			$mpdf->Output( 'port-clearance-report.pdf', 'I' );
	    }
	    else
	    {
		    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
		    $mpdf->SetTitle( 'Port Clearance Document' );
		    $mpdf->SetHTMLHeader( $header );
			$mpdf->WriteHTML( $content );
			$mpdf->Output( 'port-clearance.pdf', 'D' );
	    }
	}
}

function get_port_clearance_pdf_report_data( $id )
{
	$data = get_port_clearance( $id );   

    if( !empty( $data ) )
    {
        extract( $data );

        if( isset( $detail ) )
        {
        	$content = '';
        	$number  = 1;

            foreach( $detail as $d )
            {
            	$content .= ' 
            	<tr>
					<td align="left">' . $number . '</td>
					<td align="left">' . $d['guest_name'] . '</td>
					<td align="center">' . ( $d['guest_type'] == 'adult' ? '√' : '-' ) . '</td>
					<td align="center">' . ( $d['guest_type'] == 'child' ? '√' : '-' ) . '</td>
					<td align="center">' . ( $d['guest_type'] == 'infant' ? '√' : '-' ) . '</td>
					<td align="left">' . $d['guest_country'] . '</td>
					<!--td align="left">' . ( $d['guest_gender'] == '1' ? 'Male' : 'Female' ) . '</td-->
					<td align="left">' . $d['guest_gender'] . '</td>
					<td align="right">' . $d['guest_age'] . '</td>
				</tr>';

				$number++;
            }
        }
        else
        {
        	$content = '
        	<tr>
				<td colspan="8">
					<p class="text-center text-danger">No data guest</p>
				</td>
			</tr>';
        }

		set_template( PLUGINS_PATH . '/ticket/tpl/export/pdf-port-clearance.html', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        add_variable( 'pcid', $pcid );
        add_variable( 'captain', $captain );
        add_variable( 'kkm', $kkm );
        add_variable( 'port', $pcport );
        add_variable( 'trip', $pctrip );
        add_variable( 'rname', $pcroute );
        add_variable( 'boat', $pcboat );
        add_variable( 'detail', $content );
        add_variable( 'date', date( 'd, F Y', strtotime( $pcdate ) ) );
        add_variable( 'crew', str_replace( '<br />', ', ', nl2br( $crew ) ) );
	    add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
    }
}

function ticket_port_clearance_csv_report()
{
	$content = get_port_clearance_csv_report_data( $_GET['id'] );

	if( !empty( $content ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=port-clearance-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );
	}

	exit;
}

function get_port_clearance_csv_report_data( $id )
{
	$data = get_port_clearance( $id );   

    if( !empty( $data ) )
    {
        extract( $data );

        if( isset( $detail ) )
        {
        	$content = '';

            foreach( $detail as $d )
            {
            	$content .= '
			    <tr>
					<td>' . $d['guest_name'] . '</td>
					<td>' . ( $d['guest_type'] == 'adult' ? 'Yes' : 'No' ) . '</td>
					<td>' . ( $d['guest_type'] == 'child' ? 'Yes' : 'No' ) . '</td>
					<td>' . ( $d['guest_type'] == 'infant' ? 'Yes' : 'No' ) . '</td>
					<td>' . $d['guest_country'] . '</td>
					<!--td>' . ( $d['guest_gender'] == '1' ? 'Male' : 'Female' ) . '</td-->
					<td>' . $d['guest_gender'] . '</td>
					<td>' . $d['guest_age'] . '</td>
			    </tr>';
            }
        }
        else
        {
        	$content = '
		    <tr>
		        <td colspan="7">No data guest</td>
		    </tr>';
        }

		set_template( PLUGINS_PATH . '/ticket/tpl/export/csv-port-clearance.html', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        add_variable( 'pcid', $pcid );
        add_variable( 'captain', $captain );
        add_variable( 'kkm', $kkm );
        add_variable( 'crew', $crew );
        add_variable( 'port', $pcport );
        add_variable( 'trip', $pctrip );
        add_variable( 'rname', $pcroute );
        add_variable( 'boat', $pcboat );
        add_variable( 'detail', $content );
        add_variable( 'date', date( 'd, F Y', strtotime( $pcdate ) ) );

        parse_template( 'report-block', 'r-block', false );

        return return_template( 'report' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Export - Agent Outstanding Export
| -------------------------------------------------------------------------------------
*/
function ticket_agent_outstanding_print_report()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_agent_outstanding_list_report( $filter );

	if( !empty( $data ) )
	{
		extract( $filter );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/print-agent-outstanding-list.html', 'report' );

        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

		foreach( $data as $d )
	    {
			add_variable( 'ticket', $d['bticket'] );
			add_variable( 'atgname', $d['atgname'] );
			add_variable( 'atstatus', $d['atstatus'] );
			add_variable( 'atdate', date( 'd F, Y', strtotime( $d['atdate'] ) ) );
			add_variable( 'atdebet', number_format( $d['atdebet'], 0, '', '.' ) );
			add_variable( 'atcredit', number_format( $d['atcredit'], 0, '', '.' ) );
			add_variable( 'atoutstanding', number_format( $d['atoutstanding'], 0, '', '.' ) );
        	
        	parse_template( 'report-loop-block', 'rl-block', true );
	    }

		add_variable( 'bticket', $bticket );
		add_variable( 'atstatus', $atstatus );
		add_variable( 'date_end', $date_end );
		add_variable( 'date_start', $date_start );

		add_variable( 'fticket_css', empty( $bticket ) ? 'display:none' : '' );
		add_variable( 'fstatus_css', empty( $atstatus ) ? 'display:none' : '' );
		add_variable( 'fperiod_css', empty( $date_start ) ? 'display:none' : '' );

		add_variable( 'admurl', get_admin_url() );
	    add_variable( 'web_title', get_meta_data('web_title') );
	    add_variable( 'section_title', 'Report - Outstanding' );
		add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );
		add_variable( 'style', HTSERVER. site_url() .'/l-plugins/ticket/css/report.css' );

	    parse_template( 'report-block', 'r-block', false );

	    echo return_template( 'report' );
    }

    exit;
}

function ticket_agent_outstanding_pdf_report()
{
    $content = get_agent_outstanding_pdf_report_data();

	if( !empty( $content ) )
	{
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';

		$header = ticket_header_report( 'Report - Outstanding Agent' );
		
	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 10, 35, 15 );
	    $mpdf->SetTitle( 'Report - Outstanding Agent' );
	    $mpdf->SetHTMLHeader( $header );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'agent-outstanding-report.pdf', 'D' );
	}
}

function get_agent_outstanding_pdf_report_data()
{
	$filter = json_decode( base64_decode( $_GET['filter'] ), true );
	$data   = get_agent_outstanding_list_report( $filter );

	if( !empty( $data ) )
	{
		$content .= '
		<link rel="stylesheet" type="text/css" href="' . HTSERVER . site_url() .'/l-plugins/ticket/css/report.css">
		<div class="content">
			<div class="table-block">
				<div class="table agent-report">
					<div class="t-header">
						<p class="ticket">Reference #</p>
						<p class="name">Guest Name</p>
						<p class="status">Status</p>
						<p class="debet">Debet</p>
						<p class="credit">Credit</p>
						<p class="outstanding">Outstanding</p>
					</div>
					<div class="t-content">';

						foreach( $data as $d )
					    {
							$content .= '
							<div class="item">
								<p class="ticket">
									<span>' . $d['bcode'] . '</span><br/>
									' . date( 'd F, Y', strtotime( $d['atdate'] ) ) . '
								</p>
								<p class="name">' . $d['atgname'] . '</p>
								<p class="status">' . $d['atstatus'] . '</p>
								<p class="debet">' . number_format( $d['atdebet'], 0, '', '.' ) . '</p>
								<p class="credit">' . number_format( $d['atcredit'], 0, '', '.' ) . '</p>
								<p class="outstanding">' . number_format( $d['atoutstanding'], 0, '', '.' ) . '</p>
							</div>';
					    }

					    $content .= '
					</div>
				</div>
			</div>
		</div>';

		return $content;
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Trip Transport Export
| -------------------------------------------------------------------------------------
*/
function ticket_trip_transport_print_report()
{
	$data = get_trip_transport( $_GET['id'], $_GET['type'] );

    if( !empty( $data ) )
    {
		extract( $data );

		set_template( PLUGINS_PATH . '/ticket/tpl/export/print-trip-transport.html', 'report' );
        add_block( 'report-loop-block', 'rl-block', 'report' );
        add_block( 'report-block', 'r-block', 'report' );

        $tttime = date( 'H:i', strtotime( $tttime ) );
        $ttdate = date( 'd M Y', strtotime( $ttdate ) );

        if( isset( $detail ) && !empty( $detail) )
        {            
            foreach( $detail as $d )
            {
                $guest = array();

                if( !empty( $d['num_adult'] ) )
                {
                    $guest[] = $d['num_adult'] . ( count( $d['num_adult'] ) > 1 ? ' adults' : ' adult' );
                }

                if( !empty( $d['num_child'] ) )
                {
                    $guest[] = $d['num_child'] . ( count( $d['num_child'] ) > 1 ? ' childs' : ' child' );
                }

                if( !empty( $d['num_infant'] ) )
                {
                    $guest[] = $d['num_infant'] . ( count( $d['num_infant'] ) > 1 ? ' infants' : ' infant' );
                }

                $pass  = ticket_passenger_list( $d['bdid'] );
                $gname = ticket_passenger_list_name( $pass );

                add_variable( 'gname', $gname );
                add_variable( 'ttdid', $d['ttdid'] );
                add_variable( 'area', $d['taname'] );
                add_variable( 'bbname', $d['bbname'] );
                add_variable( 'phone', $d['bthotelphone'] );
                add_variable( 'address', $d['bthotelname'] . '<br>' . nl2br( $d['bthoteladdress'] ) );
                add_variable( 'guest_num', empty( $guest ) ? '-' : implode( ', ', $guest ) );

                parse_template( 'report-loop-block', 'rl-block', true );
            }
        }

        $dtype = get_transport_driver( $did, 'dtype' );

        add_variable( 'ttid', $ttid );
        add_variable( 'type', $type );
        add_variable( 'date', $ttdate );
        add_variable( 'time', $tttime );
        add_variable( 'rname', $rname );
        add_variable( 'driver', $dname );
        add_variable( 'vehicle', $vname );
        add_variable( 'boname', $boname );
        add_variable( 'location', $ttlocation );
        add_variable( 'transport_type', $tttype );
        add_variable( 'type_txt', ucfirst( $_GET['type'] ) );
        add_variable( 'capacity', get_vehicle_capacity( $vid ) );
        add_variable( 'fee_format', number_format( $ttfee, 0, '', '.' ) );
        add_variable( 'passenger', get_trip_transport_passenger( $type, $ttlocation, $vid, $ttdate, $tttime ) );
        
        add_variable( 'status', $ttstatus == '1' ? 'Delivered' : 'Open' );
        add_variable( 'driver_type_style', $dtype == '1' ? '' : 'style="display:none;"' );

		add_variable( 'admurl', get_admin_url() );
	    add_variable( 'web_title', get_meta_data('web_title') );
		add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );
	    add_variable( 'section_title', 'Transport - ' . ucfirst( $_GET['type'] ) . ' List' );
	    add_variable( 'style', HTSERVER . site_url() . '/l-plugins/ticket/css/report.css' );

        parse_template( 'report-block', 'r-block', false );

        echo return_template( 'report' );

        exit;
    }
}

/*
| -------------------------------------------------------------------------------------
| Export - Transport Pickup List
| -------------------------------------------------------------------------------------
*/
function ticket_pickup_list_print_report()
{
	$site_url = site_url();
	$filter   = json_decode( base64_decode( $_GET['prm'] ), true );

	extract( $filter );
    
    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-pickup-dropoff-list.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'filter', $_GET['prm'] );
	add_variable( 'pkey', 'print-pickup-list' );
    add_variable( 'section_title', 'Report - Pickup List' );

	add_variable( 'admurl', get_admin_url() );
    add_variable( 'web_title', get_meta_data( 'web_title' ) );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_pickup_list_pdf_report()
{
	ini_set( 'memory_limit', '-1' );

	$filter = json_decode( base64_decode( $_GET['prm'] ), true );
	$param  = get_pickup_list_report( $filter );

	if( !empty( $param ) )
	{
		extract( $param );

		$admurl = get_admin_url();
		$wtitle = get_meta_data( 'web_title' );
		$themes = get_meta_data( 'admin_theme', 'themes' );

		$content = '
		<!DOCTYPE html>
		<html moznomarginboxes mozdisallowselectionprint>
		    <head>
		        <meta charset="utf-8">
		        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		        <title>Report - Pickup List | ' . $wtitle . '</title>

		        <link href="' . HTSERVER . site_url() . '/l-plugins/ticket/css/report.css" rel="stylesheet" type="text/css">
		        <style type="text/css">
		            body .wrapper{ font-family:serif; width:297mm; font-size:12px; padding:5mm 10mm; margin:0 auto; }
		            .transport-list-report p{ font-size:12px; }
	                .transport-list-report table{ page-break-inside:auto; }
	                .transport-list-report tr{ page-break-inside:avoid; page-break-after:auto }
		        </style>
		    </head>
		    <body style="">
		        <div id="transport-report" class="wrapper">
		            <div class="header">
		                <img src="' . $admurl . '/themes/' . $themes . '/images/logo.png" alt="Bluewater Express" width="120" />
		                <h1>Report - Pickup List</h1>
		            </div>
		            <div class="content">
		                <div class="transport-list">
		                	<div class="transport-list-report">
			                    <div class="row clearfix">
			                        <div class="col-md-4">
			                            <p><b>Route</b></p>
			                            <p>' . $rname . '</p>
			                        </div>
			                        <div class="col-md-4">
			                            <p><b>' . $plabel . '</b></p>
			                            <p>' . $point . '</p>
			                        </div>
			                        <div class="col-md-4">
			                            <p><b>Area</b></p>
			                            <p>' . $area . '</p>
			                        </div>
			                    </div>
			                    <div class="row clearfix">
			                        <div class="col-md-4">
			                            <p><b>Boat</b></p>
			                            <p>' . $boname . '</p>
			                        </div>
			                        <div class="col-md-4">
			                            <p><b>Trip Date</b></p>
			                            <p>' . $bddate . '</p>
			                        </div>
			                        <div class="col-md-4">
			                            <p><b>Transport</b></p>
			                            <p>' . $ttype . '</p>
			                        </div>
			                    </div>
			                    <table width="100%" border="0">
			                        <tr class="head">
			                            <th align="left" colspan="2">Guest Name</th>
			                            <th align="left" width="60">Pax</th>
			                            <th align="left" width="70">' . $tlabel . '</th>
			                            <th align="left" width="180">Address</th>
			                            <th align="left" width="95">Phone No</th>
			                            <th align="left" width="80">Transport</th>
			                            <th align="left" width="70">Vehicle</th>
			                            <th align="left" width="70">Driver</th>
			                            <th align="left" width="70">Port</th>
			                        </tr>';
		                            	
							        $adult  = 0;
							        $child  = 0;
							        $infant = 0;
							        $num    = 1;

							        foreach( $data as $location => $dt )
							        {               	
								        $l_adult  = 0;
								        $l_child  = 0;
								        $l_infant = 0;

		                				$content .= '
				        				<tr class="group">
					        				<td colspan="10" valign="top">
					        					<b>' . $location . '</b>
					        				</td>
					        			</tr>';

					        			foreach( $dt as $driver => $trans )
					        			{
									        $d_adult  = 0;
									        $d_child  = 0;
									        $d_infant = 0;

		            						$content .= '
					        				<tr class="group-2">
						        				<td colspan="10" valign="top">
						        					<b>Driver Name : ' . ucwords( strtolower( $driver ) ) . '</b>
						        				</td>
						        			</tr>';

						        			foreach( $trans as $ttype => $prm )
					        				{
							        			foreach( $prm as $index => $obj )
						        				{
				                            		$adult  = $adult + intval( $obj['num_adult'] );
				                            		$child  = $child + intval( $obj['num_child'] );
				                            		$infant = $infant + intval( $obj['num_infant'] );

				                            		$d_adult  = $d_adult + intval( $obj['num_adult'] );
				                            		$d_child  = $d_child + intval( $obj['num_child'] );
				                            		$d_infant = $d_infant + intval( $obj['num_infant'] );

				                            		$l_adult  = $l_adult + intval( $obj['num_adult'] );
				                            		$l_child  = $l_child + intval( $obj['num_child'] );
				                            		$l_infant = $l_infant + intval( $obj['num_infant'] );

		            								$content .= '
							        				<tr class="loop">
							        				    <td width="30" class="num" valign="top" align="center">' . $num . '</td>
							        				    <td class="guest-name" valign="top">' . ucwords( strtolower( $obj['guest_name'] ) ) . '</td>
							        				    <td class="guest" valign="top">' . $obj['guest_num'] . '</td>
							        				    <td class="pickup" valign="top">' . $obj['taname'] . '</td>
							        				    <td class="pickup-addr" valign="top">' . ucwords( strtolower( $obj['hotel_detail'] ) ) . '</td>
							        				    <td class="phone" valign="top">' . $obj['hotel_phone'] . '</td>
							        				    <td class="trans-type" valign="top">' . $obj['trans_type'] . '</td>
							        				    <td class="vehicle" valign="top">' . $obj['vehicle'] . '</td>
							        				    <td class="driver" valign="top">' . ucwords( strtolower( $obj['driver'] ) ) . '<br/>' . $obj['drvphone'] . '</td>
							        				    <td class="loc" valign="top">' . $obj['loc'] . '</td>
							        				</tr>';

							        				$num ++;
							        			}
							        		}

			                            	if( $d_adult > 0 )
			                            	{
			                            		$separator = $d_child > 0 || $d_infant > 0 ? ', ' : '';
			                            		$text      = $d_adult > 1 ? ' adults' : ' adult';

			                            		$d_adult = $d_adult . $text . $separator;
			                            	}
			                            	else
			                            	{
			                            		$d_adult = '';
			                            	}

			                            	if( $d_child > 0 )
			                            	{
			                            		$separator = $d_infant > 0 ? ', ' : '';
			                            		$text      = $d_child > 1 ? ' childs' : ' child';

			                            		$d_child = $d_child . $text . $separator;
			                            	}
			                            	else
			                            	{
			                            		$d_child = '';
			                            	}

			                            	if( $d_infant > 0 )
			                            	{
			                            		$text = $d_infant > 1 ? ' infants' : ' infant';

			                            		$d_infant = $d_infant . $text;
			                            	}
			                            	else
			                            	{
			                            		$d_infant = '';
			                            	}

		            						$content .= '
					        				<tr class="group-foot">
					                            <td colspan="2" align="right"><b>Total</b></td>
					                            <td colspan="8" valign="top"><b>' . $d_adult . $d_child . $d_infant . '</b></td>
						        			</tr>';
						        		}

		                            	if( $l_adult > 0 )
		                            	{
		                            		$separator = $l_child > 0 || $l_infant > 0 ? ', ' : '';
		                            		$text      = $l_adult > 1 ? ' adults' : ' adult';

		                            		$l_adult = $l_adult . $text . $separator;
		                            	}
		                            	else
		                            	{
		                            		$l_adult = '';
		                            	}

		                            	if( $l_child > 0 )
		                            	{
		                            		$separator = $l_infant > 0 ? ', ' : '';
		                            		$text      = $l_child > 1 ? ' childs' : ' child';

		                            		$l_child = $l_child . $text . $separator;
		                            	}
		                            	else
		                            	{
		                            		$l_child = '';
		                            	}

		                            	if( $l_infant > 0 )
		                            	{
		                            		$text = $l_infant > 1 ? ' infants' : ' infant';

		                            		$l_infant = $l_infant . $text;
		                            	}
		                            	else
		                            	{
		                            		$l_infant = '';
		                            	}

		            					$content .= '
				        				<tr class="group-foot">
				                            <td colspan="2" align="right"><b>Subtotal</b></td>
				                            <td colspan="8" valign="top"><b>' . $l_adult . $l_child . $l_infant . '</b></td>
					        			</tr>';
							        }

		                        	if( $adult > 0 )
		                        	{
		                        		$separator = $child > 0 || $infant > 0 ? ', ' : '';
		                        		$text      = $adult > 1 ? ' adults' : ' adult';

		                        		$adult = $adult . $text . $separator;
		                        	}
		                        	else
		                        	{
		                        		$adult = '';
		                        	}

		                        	if( $child > 0 )
		                        	{
		                        		$separator = $infant > 0 ? ', ' : '';
		                        		$text      = $child > 1 ? ' childs' : ' child';

		                        		$child = $child . $text . $separator;
		                        	}
		                        	else
		                        	{
		                        		$child = '';
		                        	}

		                        	if( $infant > 0 )
		                        	{
		                        		$text = $infant > 1 ? ' infants' : ' infant';

		                        		$infant = $infant . $text;
		                        	}
		                        	else
		                        	{
		                        		$infant = '';
		                        	}

		                			$content .= '
			                        <tr class="foot">
			                            <td colspan="2" valign="top" align="right"><b>Total Pax</b></td>
			                            <td colspan="8" valign="top"><b>' . $adult . $child . $infant . '</b></td>
			                        </tr>
		                        </table>
			                </div>
		                </div>
		            </div>
		        </div>
		    </body>
		</html>';
        
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';
		
	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 5, 5, 5, 5 );
	    $mpdf->SetTitle( 'Report - Pickup List' );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'pickup-list-report.pdf', 'D' );

        exit;
	}
}

function ticket_pickup_list_csv_report()
{
	ini_set( 'memory_limit', '-1' );

	$filter = json_decode( base64_decode( $_GET['prm'] ), true );
	$param  = get_pickup_list_report( $filter );

	if( !empty( $param ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		extract( $param );

		$content = '
		<table>
			<tr>
				<td>Route</td>
				<td></td>
				<td></td>
				<td>' . $plabel . '</td>
				<td></td>
				<td></td>
				<td>Area</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>' . $rname . '</td>
				<td></td>
				<td></td>
				<td>' . $point . '</td>
				<td></td>
				<td></td>
				<td>' . $area . '</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Boat</td>
				<td></td>
				<td></td>
				<td>Trip Date</td>
				<td></td>
				<td></td>
				<td>Transport</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>' . $boname . '</td>
				<td></td>
				<td></td>
				<td>' . $bddate . '</td>
				<td></td>
				<td></td>
				<td>' . $ttype . '</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>No</td>
				<td>Guest Name</td>
				<td>Pax</td>
				<td>' . $tlabel . '</td>
				<td>Address</td>
				<td>Phone No</td>
				<td>Transport</td>
				<td>Vehicle></td>
				<td>Driver</td>
				<td>Phone Driver</td>
				<td>Port</td>
			</tr>';
		                            	
	        $adult  = 0;
	        $child  = 0;
	        $infant = 0;
	        $num    = 1;

	        foreach( $data as $location => $dt )
	        {               	
		        $l_adult  = 0;
		        $l_child  = 0;
		        $l_infant = 0;

				$content .= '
				<tr>
					<td>' . $location . '</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>';

    			foreach( $dt as $driver => $trans )
    			{
			        $d_adult  = 0;
			        $d_child  = 0;
			        $d_infant = 0;

					$content .= '
					<tr>
						<td>Driver Name : ' . ucwords( strtolower( $driver ) ) . '</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>';

        			foreach( $trans as $ttype => $prm )
    				{
	        			foreach( $prm as $index => $obj )
        				{
                    		$adult  = $adult + intval( $obj['num_adult'] );
                    		$child  = $child + intval( $obj['num_child'] );
                    		$infant = $infant + intval( $obj['num_infant'] );

                    		$d_adult  = $d_adult + intval( $obj['num_adult'] );
                    		$d_child  = $d_child + intval( $obj['num_child'] );
                    		$d_infant = $d_infant + intval( $obj['num_infant'] );

                    		$l_adult  = $l_adult + intval( $obj['num_adult'] );
                    		$l_child  = $l_child + intval( $obj['num_child'] );
                    		$l_infant = $l_infant + intval( $obj['num_infant'] );

							$content .= '
	        				<tr>
	        				    <td>' . $num . '</td>
	        				    <td>' . ucwords( strtolower( $obj['guest_name'] ) ) . '</td>
	        				    <td>' . $obj['guest_num'] . '</td>
	        				    <td>' . $obj['taname'] . '</td>
	        				    <td>' . ucwords( strtolower( $obj['hotel_detail'] ) ) . '</td>
	        				    <td>' . $obj['hotel_phone'] . '</td>
	        				    <td>' . $obj['trans_type'] . '</td>
	        				    <td>' . $obj['vehicle'] . '</td>
	        				    <td>' . ucwords( strtolower( $obj['driver'] ) ) . '</td>
	        				    <td>' . $obj['drvphone'] . '</td>
	        				    <td>' . $obj['loc'] . '</td>
	        				</tr>';

	        				$num ++;
						}
					}

                	if( $d_adult > 0 )
                	{
                		$separator = $d_child > 0 || $d_infant > 0 ? ', ' : '';
                		$text      = $d_adult > 1 ? ' adults' : ' adult';

                		$d_adult = $d_adult . $text . $separator;
                	}
                	else
                	{
                		$d_adult = '';
                	}

                	if( $d_child > 0 )
                	{
                		$separator = $d_infant > 0 ? ', ' : '';
                		$text      = $d_child > 1 ? ' childs' : ' child';

                		$d_child = $d_child . $text . $separator;
                	}
                	else
                	{
                		$d_child = '';
                	}

                	if( $d_infant > 0 )
                	{
                		$text = $d_infant > 1 ? ' infants' : ' infant';

                		$d_infant = $d_infant . $text;
                	}
                	else
                	{
                		$d_infant = '';
                	}

					$content .= '
					<tr>
						<td></td>
						<td>Total</td>
						<td>' . $d_adult . $d_child . $d_infant . '</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>';
				}

            	if( $l_adult > 0 )
            	{
            		$separator = $l_child > 0 || $l_infant > 0 ? ', ' : '';
            		$text      = $l_adult > 1 ? ' adults' : ' adult';

            		$l_adult = $l_adult . $text . $separator;
            	}
            	else
            	{
            		$l_adult = '';
            	}

            	if( $l_child > 0 )
            	{
            		$separator = $l_infant > 0 ? ', ' : '';
            		$text      = $l_child > 1 ? ' childs' : ' child';

            		$l_child = $l_child . $text . $separator;
            	}
            	else
            	{
            		$l_child = '';
            	}

            	if( $l_infant > 0 )
            	{
            		$text = $l_infant > 1 ? ' infants' : ' infant';

            		$l_infant = $l_infant . $text;
            	}
            	else
            	{
            		$l_infant = '';
            	}

				$content .= '
				<tr>
					<td></td>
					<td>Subtotal</td>
					<td>' . $l_adult . $l_child . $l_infant . '</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>';
			}

        	if( $adult > 0 )
        	{
        		$separator = $child > 0 || $infant > 0 ? ', ' : '';
        		$text      = $adult > 1 ? ' adults' : ' adult';

        		$adult = $adult . $text . $separator;
        	}
        	else
        	{
        		$adult = '';
        	}

        	if( $child > 0 )
        	{
        		$separator = $infant > 0 ? ', ' : '';
        		$text      = $child > 1 ? ' childs' : ' child';

        		$child = $child . $text . $separator;
        	}
        	else
        	{
        		$child = '';
        	}

        	if( $infant > 0 )
        	{
        		$text = $infant > 1 ? ' infants' : ' infant';

        		$infant = $infant . $text;
        	}
        	else
        	{
        		$infant = '';
        	}

			$content .= '
			<tr>
				<td></td>
				<td>Total Pax</td>
				<td>' . $adult . $child . $infant . '</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=pickup-list-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );

		exit;
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Transport Drop-off List
| -------------------------------------------------------------------------------------
*/
function ticket_dropoff_list_print_report()
{
	$site_url = site_url();
    
    add_actions( 'is_use_ajax', true );	
	add_actions( 'include_js', 'get_javascript', 'jquery-3.2.1.min' );

	set_template( PLUGINS_PATH . '/ticket/tpl/export/print-pickup-dropoff-list.html', 'report' );

    add_block( 'report-block', 'r-block', 'report' );

	add_variable( 'filter', $_GET['prm'] );
	add_variable( 'pkey', 'print-drop-off-list' );
    add_variable( 'section_title', 'Report - Drop-off List' );

	add_variable( 'admurl', get_admin_url() );
    add_variable( 'web_title', get_meta_data( 'web_title' ) );
	add_variable( 'themes', get_meta_data( 'admin_theme', 'themes' ) );

	add_variable( 'include_js', attemp_actions( 'include_js' ) );
	add_variable( 'include_css', attemp_actions( 'include_css' ) );

	add_variable( 'ajax_url', HTSERVER . $site_url . '/print-and-export-report-ajax/' );
	add_variable( 'style', HTSERVER . $site_url . '/l-plugins/ticket/css/report.css' );

    parse_template( 'report-block', 'r-block', false );

    return return_template( 'report' );
}

function ticket_dropoff_list_pdf_report()
{
	ini_set( 'memory_limit', '-1' );

	$filter = json_decode( base64_decode( $_GET['prm'] ), true );
	$param  = get_drop_off_list_report( $filter );

	if( !empty( $param ) )
	{
		extract( $param );

		$admurl = get_admin_url();
		$wtitle = get_meta_data( 'web_title' );
		$themes = get_meta_data( 'admin_theme', 'themes' );

		$content = '
		<!DOCTYPE html>
		<html moznomarginboxes mozdisallowselectionprint>
		    <head>
		        <meta charset="utf-8">
		        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		        <title>Report - Drop-off List | ' . $wtitle . '</title>

		        <link href="' . HTSERVER . site_url() . '/l-plugins/ticket/css/report.css" rel="stylesheet" type="text/css">
		        <style type="text/css">
		            body .wrapper{ font-family:serif; width:297mm; font-size:12px; padding:5mm 10mm; margin:0 auto; }
		            .transport-list-report p{ font-size:12px; }
	                .transport-list-report table{ page-break-inside:auto; }
	                .transport-list-report tr{ page-break-inside:avoid; page-break-after:auto }
		        </style>
		    </head>
		    <body style="">
		        <div id="transport-report" class="wrapper">
		            <div class="header">
		                <img src="' . $admurl . '/themes/' . $themes . '/images/logo.png" alt="Bluewater Express" width="120" />
		                <h1>Report - Drop-off List</h1>
		            </div>
		            <div class="content">
		                <div class="transport-list">
		                	<div class="transport-list-report">
			                    <div class="row clearfix">
			                        <div class="col-md-4">
			                            <p><b>Route</b></p>
			                            <p>' . $rname . '</p>
			                        </div>
			                        <div class="col-md-4">
			                            <p><b>' . $plabel . '</b></p>
			                            <p>' . $point . '</p>
			                        </div>
			                        <div class="col-md-4">
			                            <p><b>Area</b></p>
			                            <p>' . $area . '</p>
			                        </div>
			                    </div>
			                    <div class="row clearfix">
			                        <div class="col-md-4">
			                            <p><b>Boat</b></p>
			                            <p>' . $boname . '</p>
			                        </div>
			                        <div class="col-md-4">
			                            <p><b>Trip Date</b></p>
			                            <p>' . $bddate . '</p>
			                        </div>
			                        <div class="col-md-4">
			                            <p><b>Transport</b></p>
			                            <p>' . $ttype . '</p>
			                        </div>
			                    </div>
			                    <table width="100%" border="0">
			                        <tr class="head">
			                            <th align="left" colspan="2">Guest Name</th>
			                            <th align="left" width="60">Pax</th>
			                            <th align="left" width="70">' . $tlabel . '</th>
			                            <th align="left" width="180">Address</th>
			                            <th align="left" width="95">Phone No</th>
			                            <th align="left" width="80">Transport</th>
			                            <th align="left" width="70">Vehicle</th>
			                            <th align="left" width="70">Driver</th>
			                            <th align="left" width="70">Port</th>
			                        </tr>';
		                            	
							        $adult  = 0;
							        $child  = 0;
							        $infant = 0;
							        $num    = 1;

							        foreach( $data as $location => $dt )
							        {               	
								        $l_adult  = 0;
								        $l_child  = 0;
								        $l_infant = 0;

		                				$content .= '
				        				<tr class="group">
					        				<td colspan="10" valign="top">
					        					<b>' . $location . '</b>
					        				</td>
					        			</tr>';

					        			foreach( $dt as $driver => $trans )
					        			{
									        $d_adult  = 0;
									        $d_child  = 0;
									        $d_infant = 0;

		            						$content .= '
					        				<tr class="group-2">
						        				<td colspan="10" valign="top">
						        					<b>Driver Name : ' . ucwords( strtolower( $driver ) ) . '</b>
						        				</td>
						        			</tr>';

						        			foreach( $trans as $ttype => $prm )
					        				{
							        			foreach( $prm as $index => $obj )
						        				{
				                            		$adult  = $adult + intval( $obj['num_adult'] );
				                            		$child  = $child + intval( $obj['num_child'] );
				                            		$infant = $infant + intval( $obj['num_infant'] );

				                            		$d_adult  = $d_adult + intval( $obj['num_adult'] );
				                            		$d_child  = $d_child + intval( $obj['num_child'] );
				                            		$d_infant = $d_infant + intval( $obj['num_infant'] );

				                            		$l_adult  = $l_adult + intval( $obj['num_adult'] );
				                            		$l_child  = $l_child + intval( $obj['num_child'] );
				                            		$l_infant = $l_infant + intval( $obj['num_infant'] );

		            								$content .= '
							        				<tr class="loop">
							        				    <td width="30" class="num" valign="top" align="center">' . $num . '</td>
							        				    <td class="guest-name" valign="top">' . ucwords( strtolower( $obj['guest_name'] ) ) . '</td>
							        				    <td class="guest" valign="top">' . $obj['guest_num'] . '</td>
							        				    <td class="pickup" valign="top">' . $obj['taname'] . '</td>
							        				    <td class="pickup-addr" valign="top">' . ucwords( strtolower( $obj['hotel_detail'] ) ) . '</td>
							        				    <td class="phone" valign="top">' . $obj['hotel_phone'] . '</td>
							        				    <td class="trans-type" valign="top">' . $obj['trans_type'] . '</td>
							        				    <td class="vehicle" valign="top">' . $obj['vehicle'] . '</td>
							        				    <td class="driver" valign="top">' . ucwords( strtolower( $obj['driver'] ) ) . '<br/>' . $obj['drvphone'] . '</td>
							        				    <td class="loc" valign="top">' . $obj['loc'] . '</td>
							        				</tr>';

							        				$num ++;
							        			}
							        		}

			                            	if( $d_adult > 0 )
			                            	{
			                            		$separator = $d_child > 0 || $d_infant > 0 ? ', ' : '';
			                            		$text      = $d_adult > 1 ? ' adults' : ' adult';

			                            		$d_adult = $d_adult . $text . $separator;
			                            	}
			                            	else
			                            	{
			                            		$d_adult = '';
			                            	}

			                            	if( $d_child > 0 )
			                            	{
			                            		$separator = $d_infant > 0 ? ', ' : '';
			                            		$text      = $d_child > 1 ? ' childs' : ' child';

			                            		$d_child = $d_child . $text . $separator;
			                            	}
			                            	else
			                            	{
			                            		$d_child = '';
			                            	}

			                            	if( $d_infant > 0 )
			                            	{
			                            		$text = $d_infant > 1 ? ' infants' : ' infant';

			                            		$d_infant = $d_infant . $text;
			                            	}
			                            	else
			                            	{
			                            		$d_infant = '';
			                            	}

		            						$content .= '
					        				<tr class="group-foot">
					                            <td colspan="2" align="right"><b>Total</b></td>
					                            <td colspan="8" valign="top"><b>' . $d_adult . $d_child . $d_infant . '</b></td>
						        			</tr>';
						        		}

		                            	if( $l_adult > 0 )
		                            	{
		                            		$separator = $l_child > 0 || $l_infant > 0 ? ', ' : '';
		                            		$text      = $l_adult > 1 ? ' adults' : ' adult';

		                            		$l_adult = $l_adult . $text . $separator;
		                            	}
		                            	else
		                            	{
		                            		$l_adult = '';
		                            	}

		                            	if( $l_child > 0 )
		                            	{
		                            		$separator = $l_infant > 0 ? ', ' : '';
		                            		$text      = $l_child > 1 ? ' childs' : ' child';

		                            		$l_child = $l_child . $text . $separator;
		                            	}
		                            	else
		                            	{
		                            		$l_child = '';
		                            	}

		                            	if( $l_infant > 0 )
		                            	{
		                            		$text = $l_infant > 1 ? ' infants' : ' infant';

		                            		$l_infant = $l_infant . $text;
		                            	}
		                            	else
		                            	{
		                            		$l_infant = '';
		                            	}

		            					$content .= '
				        				<tr class="group-foot">
				                            <td colspan="2" align="right"><b>Subtotal</b></td>
				                            <td colspan="8" valign="top"><b>' . $l_adult . $l_child . $l_infant . '</b></td>
					        			</tr>';
							        }

		                        	if( $adult > 0 )
		                        	{
		                        		$separator = $child > 0 || $infant > 0 ? ', ' : '';
		                        		$text      = $adult > 1 ? ' adults' : ' adult';

		                        		$adult = $adult . $text . $separator;
		                        	}
		                        	else
		                        	{
		                        		$adult = '';
		                        	}

		                        	if( $child > 0 )
		                        	{
		                        		$separator = $infant > 0 ? ', ' : '';
		                        		$text      = $child > 1 ? ' childs' : ' child';

		                        		$child = $child . $text . $separator;
		                        	}
		                        	else
		                        	{
		                        		$child = '';
		                        	}

		                        	if( $infant > 0 )
		                        	{
		                        		$text = $infant > 1 ? ' infants' : ' infant';

		                        		$infant = $infant . $text;
		                        	}
		                        	else
		                        	{
		                        		$infant = '';
		                        	}

		                			$content .= '
			                        <tr class="foot">
			                            <td colspan="2" valign="top" align="right"><b>Total Pax</b></td>
			                            <td colspan="8" valign="top"><b>' . $adult . $child . $infant . '</b></td>
			                        </tr>
		                        </table>
			                </div>
		                </div>
		            </div>
		        </div>
		    </body>
		</html>';
        
		require_once ADMIN_PATH . '/includes/mpdf/vendor/autoload.php';
		
	    $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 0, 0, 10, 10 );
	    $mpdf->SetTitle( 'Report - Drop-off List' );
		$mpdf->WriteHTML( $content );
		$mpdf->Output( 'drop-off-list-report.pdf', 'D' );

        exit;
	}
}

function ticket_dropoff_list_csv_report()
{
	ini_set( 'memory_limit', '-1' );

	$filter = json_decode( base64_decode( $_GET['prm'] ), true );
	$param  = get_drop_off_list_report( $filter );

	if( !empty( $param ) )
	{
		require_once ROOT_PATH . '/l-functions/simple_html_dom.php';

		extract( $param );

		$content = '
		<table>
			<tr>
				<td>Route</td>
				<td></td>
				<td></td>
				<td>' . $plabel . '</td>
				<td></td>
				<td></td>
				<td>Area</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>' . $rname . '</td>
				<td></td>
				<td></td>
				<td>' . $point . '</td>
				<td></td>
				<td></td>
				<td>' . $area . '</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Boat</td>
				<td></td>
				<td></td>
				<td>Trip Date</td>
				<td></td>
				<td></td>
				<td>Transport</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>' . $boname . '</td>
				<td></td>
				<td></td>
				<td>' . $bddate . '</td>
				<td></td>
				<td></td>
				<td>' . $ttype . '</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>No</td>
				<td>Guest Name</td>
				<td>Pax</td>
				<td>' . $tlabel . '</td>
				<td>Address</td>
				<td>Phone No</td>
				<td>Transport</td>
				<td>Vehicle></td>
				<td>Driver</td>
				<td>Phone Driver</td>
				<td>Port</td>
			</tr>';
		                            	
	        $adult  = 0;
	        $child  = 0;
	        $infant = 0;
	        $num    = 1;

	        foreach( $data as $location => $dt )
	        {               	
		        $l_adult  = 0;
		        $l_child  = 0;
		        $l_infant = 0;

				$content .= '
				<tr>
					<td>' . $location . '</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>';

    			foreach( $dt as $driver => $trans )
    			{
			        $d_adult  = 0;
			        $d_child  = 0;
			        $d_infant = 0;

					$content .= '
					<tr>
						<td>Driver Name : ' . ucwords( strtolower( $driver ) ) . '</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>';

        			foreach( $trans as $ttype => $prm )
    				{
	        			foreach( $prm as $index => $obj )
        				{
                    		$adult  = $adult + intval( $obj['num_adult'] );
                    		$child  = $child + intval( $obj['num_child'] );
                    		$infant = $infant + intval( $obj['num_infant'] );

                    		$d_adult  = $d_adult + intval( $obj['num_adult'] );
                    		$d_child  = $d_child + intval( $obj['num_child'] );
                    		$d_infant = $d_infant + intval( $obj['num_infant'] );

                    		$l_adult  = $l_adult + intval( $obj['num_adult'] );
                    		$l_child  = $l_child + intval( $obj['num_child'] );
                    		$l_infant = $l_infant + intval( $obj['num_infant'] );

							$content .= '
	        				<tr>
	        				    <td>' . $num . '</td>
	        				    <td>' . ucwords( strtolower( $obj['guest_name'] ) ) . '</td>
	        				    <td>' . $obj['guest_num'] . '</td>
	        				    <td>' . $obj['taname'] . '</td>
	        				    <td>' . ucwords( strtolower( $obj['hotel_detail'] ) ) . '</td>
	        				    <td>' . $obj['hotel_phone'] . '</td>
	        				    <td>' . $obj['trans_type'] . '</td>
	        				    <td>' . $obj['vehicle'] . '</td>
	        				    <td>' . ucwords( strtolower( $obj['driver'] ) ) . '</td>
	        				    <td>' . $obj['drvphone'] . '</td>
	        				    <td>' . $obj['loc'] . '</td>
	        				</tr>';

	        				$num ++;
						}
					}

                	if( $d_adult > 0 )
                	{
                		$separator = $d_child > 0 || $d_infant > 0 ? ', ' : '';
                		$text      = $d_adult > 1 ? ' adults' : ' adult';

                		$d_adult = $d_adult . $text . $separator;
                	}
                	else
                	{
                		$d_adult = '';
                	}

                	if( $d_child > 0 )
                	{
                		$separator = $d_infant > 0 ? ', ' : '';
                		$text      = $d_child > 1 ? ' childs' : ' child';

                		$d_child = $d_child . $text . $separator;
                	}
                	else
                	{
                		$d_child = '';
                	}

                	if( $d_infant > 0 )
                	{
                		$text = $d_infant > 1 ? ' infants' : ' infant';

                		$d_infant = $d_infant . $text;
                	}
                	else
                	{
                		$d_infant = '';
                	}

					$content .= '
					<tr>
						<td></td>
						<td>Total</td>
						<td>' . $d_adult . $d_child . $d_infant . '</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>';
				}

            	if( $l_adult > 0 )
            	{
            		$separator = $l_child > 0 || $l_infant > 0 ? ', ' : '';
            		$text      = $l_adult > 1 ? ' adults' : ' adult';

            		$l_adult = $l_adult . $text . $separator;
            	}
            	else
            	{
            		$l_adult = '';
            	}

            	if( $l_child > 0 )
            	{
            		$separator = $l_infant > 0 ? ', ' : '';
            		$text      = $l_child > 1 ? ' childs' : ' child';

            		$l_child = $l_child . $text . $separator;
            	}
            	else
            	{
            		$l_child = '';
            	}

            	if( $l_infant > 0 )
            	{
            		$text = $l_infant > 1 ? ' infants' : ' infant';

            		$l_infant = $l_infant . $text;
            	}
            	else
            	{
            		$l_infant = '';
            	}

				$content .= '
				<tr>
					<td></td>
					<td>Subtotal</td>
					<td>' . $l_adult . $l_child . $l_infant . '</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>';
			}

        	if( $adult > 0 )
        	{
        		$separator = $child > 0 || $infant > 0 ? ', ' : '';
        		$text      = $adult > 1 ? ' adults' : ' adult';

        		$adult = $adult . $text . $separator;
        	}
        	else
        	{
        		$adult = '';
        	}

        	if( $child > 0 )
        	{
        		$separator = $infant > 0 ? ', ' : '';
        		$text      = $child > 1 ? ' childs' : ' child';

        		$child = $child . $text . $separator;
        	}
        	else
        	{
        		$child = '';
        	}

        	if( $infant > 0 )
        	{
        		$text = $infant > 1 ? ' infants' : ' infant';

        		$infant = $infant . $text;
        	}
        	else
        	{
        		$infant = '';
        	}

			$content .= '
			<tr>
				<td></td>
				<td>Total Pax</td>
				<td>' . $adult . $child . $infant . '</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>';

		$html = str_get_html( $content );

		header( 'Content-type: application/ms-excel' );
		header( 'Content-Disposition: attachment; filename=drop-off-list-report.csv' );

		$fp = fopen( 'php://output', 'w' );

		foreach( $html->find( 'tr' ) as $element )
		{
		    $td = array();

			foreach( $element->find( 'th' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			foreach( $element->find( 'td' ) as $row )
			{
			    $td [] = $row->plaintext;
			}

			fputcsv( $fp, $td );
		}

		fclose( $fp );

		exit;
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Header
| -------------------------------------------------------------------------------------
*/
function ticket_header_report( $title = '', $type = '' )
{
	$themes = get_meta_data( 'admin_theme', 'themes' );
	$admurl = get_admin_url();
	
	$content = 
	'<div style="text-align:right; width:100%; padding-right:10px;">
		<span style="font-size:12px;">Printed date : '. date('d M Y') .'</span>	
	</div>';
	
	$date    = ( $type != '' ) ? $content : '';

	return '
	<div class="header">
        <img src="' . $admurl . '/themes/' . $themes . '/images/logo.png" alt="Blue Water Express" width="120" />
		<h1>' . $title . '</h1>
		'. $date .'	
	</div>';
}

/*
| -------------------------------------------------------------------------------------
| Export - Print & Donwload Booking Data
| -------------------------------------------------------------------------------------
*/
function ticket_print_and_download_report()
{
	if( isset( $_GET['id'] ) )
	{
		$data = ticket_booking_all_data( $_GET['id'] );
		
		if( validate_booking_status( $data, array( 'pa', 'ca', 'pp' ) ) ) 
		{
			if ( $_GET['action'] == 0 ) 
			{
				return generate_booking_pdf_ticket( $data, 'I' );
			} 
			elseif( $_GET['action'] == 4 )
			{
				return generate_booking_pdf_ticket( $data, 'D' );
			}
			elseif( $_GET['action'] == 5 )
			{
				return generate_booking_pdf_receipt( $data, 'D' );
			}
			elseif( $_GET['action'] == 6 )
			{
				return generate_reconfirmation_pdf( $data, 'I' );
			}
			elseif( $_GET['action'] == 8 )
			{
				return generate_reconfirmation_pdf( $data, 'D' );
			}
			elseif( $_GET['action'] == 10 )
			{
				return generate_boarding_pass_pdf( $data, 'I' );
			}
			elseif( $_GET['action'] == 11 )
			{
				return generate_reconfirmation_doc( $data );
			}
			elseif( $_GET['action'] == 13 )
			{
				if ( $data['agid'] != '' ) 
                {
                    generate_invoice_agent_pdf( $data, 'D' );
                }
			}
		}
	}
}

/*
| -------------------------------------------------------------------------------------
| Export - Ajax Request
| -------------------------------------------------------------------------------------
*/
function ticket_print_and_export_report_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-passenger-list' )
    {
    	$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$data   = get_passenger_list_report( $filter );

		if( !empty( $data ) )
		{
			echo json_encode( array( 'result' => 'success', 'data' => $data ) );
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-passenger-list-fo' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$data   = get_passenger_list_fo_report( $filter );

		if( !empty( $data ) )
		{
			echo json_encode( array( 'result' => 'success', 'data' => $data, 'num' => count( $data ) ) );
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-passenger-list-acounting' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$data   = get_passenger_list_accounting_report( $filter );

		if( !empty( $data ) )
		{
			echo json_encode( array( 'result' => 'success', 'data' => $data, 'num' => count( $data ) ) );
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-reservation-list' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$order  = json_decode( base64_decode( $_POST['order'] ), true );
		$param  = get_reservation_list_report( $filter, $order );
		if( !empty( $param ) )
		{
			echo json_encode( array( 
				'result' => 'success', 
				'pax'    => $param['pax'],
				'data'   => $param['data'], 
				'adult'  => $param['adult'], 
				'child'  => $param['child'], 
				'infant' => $param['infant']
			));
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-hs-sales-sum' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$data   = get_ticket_hs_sales_sum_report( $filter );

		if( empty( $data ) === false )
		{
			echo json_encode( array( 'result' => 'success', 'data' => $data ) );
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-agent-reservation-list' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$order  = json_decode( base64_decode( $_POST['order'] ), true );
		$param  = get_agent_reservation_list_report( $filter, $order );

		if( !empty( $param ) )
		{
			echo json_encode( array( 
				'result' => 'success', 
				'pax'    => $param['pax'],
				'data'   => $param['data'], 
				'adult'  => $param['adult'], 
				'child'  => $param['child'], 
				'infant' => $param['infant']
			));
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-pickup-list' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$param  = get_pickup_list_report( $filter );

		if( !empty( $param ) )
		{
			echo json_encode( array( 
				'result' => 'success',
				'data'   => $param['data'], 
				'area'   => $param['area'], 
				'rname'  => $param['rname'], 
				'point'  => $param['point'], 
				'ttype'  => $param['ttype'],
				'plabel' => $param['plabel'],
				'tlabel' => $param['tlabel'],
				'bddate' => $param['bddate'], 
				'search' => $param['search'], 
				'boname' => $param['boname']
			));
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-drop-off-list' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$param  = get_drop_off_list_report( $filter );

		if( !empty( $param ) )
		{
			echo json_encode( array( 
				'result' => 'success',
				'data'   => $param['data'], 
				'area'   => $param['area'], 
				'rname'  => $param['rname'],
				'point'  => $param['point'], 
				'ttype'  => $param['ttype'],
				'plabel' => $param['plabel'],
				'tlabel' => $param['tlabel'],
				'bddate' => $param['bddate'], 
				'search' => $param['search'], 
				'boname' => $param['boname']
			));
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-agent-invoice-summary' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$param  = get_agent_print_invoice_summary( $filter );

		if( !empty( $param ) )
		{
			echo json_encode( array( 
				'result' => 'success',
				'data'   => $param
			));
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-daily-addons-list' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$data   = get_daily_addons_report( $filter );

		if( empty( $data ) === false )
		{
			echo json_encode( array( 'result' => 'success', 'data' => $data ) );
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'print-summary-addons-list' )
    {
		$filter = json_decode( base64_decode( $_POST['filter'] ), true );
		$data   = get_summary_addons_report( $filter );

		if( empty( $data ) === false )
		{
			echo json_encode( array( 'result' => 'success', 'data' => $data ) );
		}
		else
		{
			echo json_encode( array( 'result' => 'failed' ) );
		}
    }

    exit;
}

?>
