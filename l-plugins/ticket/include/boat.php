<?php

add_actions( 'boat', 'ticket_boat' );
add_actions( 'ticket-boat-ajax_page', 'ticket_boat_ajax' );

function ticket_boat()
{
    if( is_num_boat() == 0 && !isset( $_GET['prc'] ) )
    {
        header( 'location:' . get_state_url( 'boat&prc=add_new' ) );
    }

    if( is_add_new() )
    {
        return ticket_add_new_boat();
    }
    elseif( is_edit() )
    {
        if( isset( $_GET['id'] ) && get_boat( $_GET['id'] ) )
        {
            return ticket_edit_boat();
        }
        else
        {
            return not_found_template();
        }
    }
    elseif( is_delete_all() )
    {
        return ticket_batch_delete_boat();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key => $val )
        {
            delete_boat( $val );
        }
    }

    return ticket_boat_table();
}

/*
| -------------------------------------------------------------------------------------
| Rate Boat List
| -------------------------------------------------------------------------------------
*/
function ticket_boat_table()
{
	global $db;

    $s = 'SELECT * FROM ticket_boat ORDER BY boid DESC';
    $q = $db->prepare_query( $s );
	$r = $db->do_query( $q );

    $site_url = site_url();

	set_template( PLUGINS_PATH . '/ticket/tpl/boat/list.html', 'boat' );

    add_block( 'list-block', 'boblock', 'boat' );

    add_variable( 'site_url', $site_url );
    add_variable( 'limit', post_viewed() );
    add_variable( 'list', ticket_boat_table_data( $r ) );
    add_variable( 'message', generate_error_query_message_block() );

    add_variable( 'action', get_state_url( 'boat' ) );
    add_variable( 'edit_link', get_state_url( 'boat&prc=edit' ) );
    add_variable( 'add_new_link', get_state_url( 'boat&prc=add_new' ) );
    
    parse_template( 'list-block', 'boblock', false );
    
    add_actions( 'section_title', 'Boat List' );
	add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    return return_template( 'boat' );
}

/*
| -------------------------------------------------------------------------------------
| Boat Table List Item
| -------------------------------------------------------------------------------------
*/
function ticket_boat_table_data( $r, $i = 1 )
{
    global $db;
    
    if( $db->num_rows( $r ) == 0 )
    {
        return '
        <tr>
            <td colspan="3">
                <p class="text-center text-danger">No data found</p>
            </td>
        </tr>';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/boat/loop.html', 'boat-loop' );

    add_block( 'loop-block', 'boloop', 'boat-loop' );

    while( $d = $db->fetch_array( $r ) )
    {
        $a = get_boat_first_attachment( $d['boid'] );

        add_variable( 'id', $d['boid'] );
        add_variable( 'boname', $d['boname'] );
        add_variable( 'bopicture', $a['thumbnail'] );
        add_variable( 'bopicturetitle', $a['title'] );
        add_variable( 'bopassenger', $d['bopassenger'] );
        add_variable( 'bostatus', $d['bostatus'] = 1 ? 'Ready' : ( $d['bostatus'] = 2 ? 'Maintenance' : '-' ) );
        add_variable( 'edit_link', get_state_url( 'boat&prc=edit&id=' . $d['boid'] ) );
        add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-boat-ajax/' );

        parse_template( 'loop-block', 'boloop', true );
    }

    return return_template( 'boat-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Boat
| -------------------------------------------------------------------------------------
*/
function ticket_add_new_boat()
{
	run_save_boat();

    $site_url = site_url();
    $data     = get_boat();

	set_template( PLUGINS_PATH . '/ticket/tpl/boat/form.html', 'boat' );
    add_block( 'form-block', 'boblock', 'boat' );

    add_variable( 'boid', $data['boid'] );
    add_variable( 'bocode', $data['bocode'] );
    add_variable( 'boname', $data['boname'] );
    add_variable( 'bodesc', $data['bodesc'] );
    add_variable( 'bopassenger', $data['bopassenger'] );

    add_variable( 'message', generate_message_block() );
    add_variable( 'bostatus', get_boat_status_option( $data['bostatus'] ) );

    add_variable( 'cancel_link', get_state_url( 'boat' ) );
    add_variable( 'action', get_state_url( 'boat&prc=add_new' ) );
    add_variable( 'admin_theme', get_meta_data('admin_theme','themes') );

	add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-boat-ajax/' );

    add_variable( 'delete_class', 'sr-only' );
    
    add_actions( 'section_title', 'New Boat' );

	add_actions( 'header_elements', 'get_css', 'dropzone.css' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

	add_actions( 'header_elements', 'get_javascript', 'dropzone.js' );
	add_actions( 'header_elements', 'get_javascript_inc', 'tiny_mce/tinymce.min.js' );

    parse_template( 'form-block', 'boblock', false );

    return return_template( 'boat' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Boat
| -------------------------------------------------------------------------------------
*/
function ticket_edit_boat()
{
    run_update_boat();

    $site_url = site_url();
    $data     = get_boat( $_GET['id'] );

    set_template( PLUGINS_PATH . '/ticket/tpl/boat/form.html', 'boat' );
    add_block( 'form-block', 'boblock', 'boat' );

    add_variable( 'boid', $data['boid'] );
    add_variable( 'bocode', $data['bocode'] );
    add_variable( 'boname', $data['boname'] );
    add_variable( 'bodesc', $data['bodesc'] );
    add_variable( 'bopassenger', $data['bopassenger'] );

    add_variable( 'message', generate_message_block() );
    add_variable( 'bostatus', get_boat_status_option( $data['bostatus'] ) );

    add_variable( 'cancel_link', get_state_url( 'boat' ) );
    add_variable( 'admin_theme', get_meta_data('admin_theme','themes') );
    add_variable( 'action', get_state_url( 'boat&prc=edit&id=' . $_GET['id'] ) );

    add_variable( 'ajax_link', HTSERVER . site_url() . '/ticket-boat-ajax/' );

    add_variable( 'delete_class', '' );
    add_variable( 'last_update_note', get_last_update_note( $_GET['id'], 'boat' ) );

    parse_template( 'form-block', 'boblock', false );
    
    add_actions( 'section_title', 'Edit Boat' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER. site_url() .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
	add_actions( 'header_elements', 'get_css', 'dropzone.css' );
	add_actions( 'header_elements', 'get_javascript', 'dropzone' );
	add_actions( 'header_elements', 'get_javascript_inc', 'tiny_mce/tinymce.min.js' );

    return return_template( 'boat' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Boat
| -------------------------------------------------------------------------------------
*/
function ticket_batch_delete_boat()
{
    set_template( PLUGINS_PATH . '/ticket/tpl/boat/batch-delete.html', 'boat' );
    add_block( 'loop-block', 'rtlblock', 'boat' );
    add_block( 'delete-block', 'boblock', 'boat' );
    
    foreach( $_POST['select'] as $key => $val )
    {
        $d = get_boat( $val );
        
        add_variable( 'boname', $d['boname'] );
        add_variable( 'boid', $d['boid'] );
        
        parse_template( 'loop-block', 'rtlblock', true );
    }
    
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' boat? :' );
    add_variable( 'action', get_state_url( 'boat' ) );
    
    parse_template( 'delete-block', 'boblock', false );
    
    add_actions( 'section_title', 'Delete Boat' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . site_url() . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );
    
    return return_template( 'boat' );
}

function run_save_boat()
{
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_boat_data();

        if( empty( $error ) )
        {
            $post_id = save_boat();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to save new boat' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'New boat successfully saved' ) ) );

                header( 'Location:' . get_state_url( 'boat&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function run_update_boat()
{    
    global $flash;

    if( is_save_draft() || is_publish() )
    {
        $error = validate_boat_data();

        if( empty( $error ) )
        {
            $post_id = update_boat();

            if( empty( $post_id ) )
            {
                $flash->add( array( 'type'=> 'error', 'content' => array( 'Failed to edit this boat' ) ) );
            }
            else
            {
                $flash->add( array( 'type'=> 'success', 'content' => array( 'This boat successfully edited' ) ) );

                header( 'Location:' . get_state_url( 'boat&prc=edit&id=' . $post_id ) );

                exit;
            }
        }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

function validate_boat_data()
{
    $error = array();

    if( isset($_POST['bocode']) && empty( $_POST['bocode'] ) )
    {
        $error[] = 'Boat code can\'t be empty';
    }

    if( isset($_POST['boname']) && empty( $_POST['boname'] ) )
    {
        $error[] = 'Boat name can\'t be empty';
    }

    if( isset($_POST['bopassenger']) && empty( $_POST['bopassenger'] ) )
    {
        $error[] = 'Number passenger on boat can\'t be empty';
    }

    if( isset($_POST['bostatus']) && empty( $_POST['bostatus'] ) )
    {
        $error[] = 'Boat status can\'t be empty';
    }

    return $error;
}

/*
| -------------------------------------------------------------------------------------
| Get Boat List Count
| -------------------------------------------------------------------------------------
*/
function is_num_boat()
{
    global $db;
    
    $s = 'SELECT * FROM ticket_boat';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Boat By ID
| -------------------------------------------------------------------------------------
*/
function get_boat( $id = '', $field = '' )
{
    global $db;

    $data = array( 
        'boid'        => ( isset( $_POST['boid'] ) ? $_POST['boid'] : null ), 
        'bocode'      => ( isset( $_POST['bocode'] ) ? $_POST['bocode'] : '' ), 
        'boname'      => ( isset( $_POST['boname'] ) ? $_POST['boname'] : '' ), 
        'bopassenger' => ( isset( $_POST['bopassenger'] ) ? $_POST['bopassenger'] : 0 ), 
        'bodesc'      => ( isset( $_POST['bodesc'] ) ? $_POST['bodesc'] : '' ), 
        'bostatus'    => ( isset( $_POST['bostatus'] ) ? $_POST['bostatus'] : '' )
    );

    $s = 'SELECT * FROM ticket_boat WHERE boid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'boid' => $d['boid'], 
                'bocode' => $d['bocode'], 
                'boname' => $d['boname'], 
                'bopassenger' => $d['bopassenger'], 
                'bodesc' => $d['bodesc'], 
                'bostatus' => $d['bostatus']
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Boat List
| -------------------------------------------------------------------------------------
*/
function get_boat_list()
{
    global $db;

    $s = 'SELECT * FROM ticket_boat';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array( 
            'boid' => $d['boid'], 
            'bocode' => $d['bocode'], 
            'boname' => $d['boname'], 
            'bopassenger' => $d['bopassenger'], 
            'bodesc' => $d['bodesc'], 
            'bostatus' => $d['bostatus']
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Save Boat
| -------------------------------------------------------------------------------------
*/
function save_boat()
{
    global $db;
    
    $s = 'INSERT INTO ticket_boat(
            bocode,
            boname,
            bopassenger,
            bodesc,
            bostatus,
            bocreateddate,
            luser_id)
          VALUES( %s, %s, %d, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s,
			$_POST['bocode'],
			$_POST['boname'],
			$_POST['bopassenger'],
			$_POST['bodesc'],
			$_POST['bostatus'],
			date( 'Y-m-d H:i:s' ),
			$_COOKIE['user_id'] );
       
    if( $db->do_query( $q ) )
    {
        $id = $db->insert_id();
        
        save_log( $id, 'boat', 'Add new boat - ' . $_POST['boname'] );

        sync_boat_attachment( $id );

        return $id;
    }
}

/*
| -------------------------------------------------------------------------------------
| Update Boat
| -------------------------------------------------------------------------------------
*/
function update_boat()
{
    global $db;

    $d = get_boat( $_POST['boid'] );

    $s = 'UPDATE ticket_boat SET 
            bocode = %s, 
            boname = %s, 
            bopassenger = %d,
            bodesc = %s,
            bostatus = %s
          WHERE boid = %d';     
    $q = $db->prepare_query( $s,
			$_POST['bocode'],
			$_POST['boname'],
			$_POST['bopassenger'],
			$_POST['bodesc'],
			$_POST['bostatus'],
			$_POST['boid'] );
       
    if( $db->do_query( $q ) )
    {
        $logs   = array();
        $st_old = $d['bostatus'] == '1' ? 'Ready' : 'Maintenance'; 
        $st     = $_POST['bostatus'] == '1' ? 'Ready' : 'Maintenance';       
        $logs[] = $d['boname'] != $_POST['boname'] ? 'Boat Name : ' . $d['boname'] . ' to ' . $_POST['boname'] : '';
        $logs[] = $d['bocode'] != $_POST['bocode'] ? 'Boat Code : ' . $d['bocode'] . ' to ' . $_POST['bocode'] : '';
        $logs[] = $d['bopassenger'] != $_POST['bopassenger'] ? 'Passenger : ' . $d['bopassenger'] . ' to ' . $_POST['bopassenger'] : '';
        $logs[] = $d['bodesc'] != $_POST['bodesc'] ? 'Boat Description : ' . strip_tags( $d['bodesc'] ) . ' to ' . strip_tags( $_POST['bodesc'] ) : '';
        $logs[] = $d['bostatus'] != $_POST['bostatus'] ? 'Change Status : ' . $st_old . ' to ' . $st : '';

        if ( !empty( $logs ) ) 
        {
            foreach ( $logs as $i => $log ) 
            {
                if ( $log == '' ) 
                {
                    unset( $logs[ $i ] );
                }
            }

            save_log( $_POST['boid'], 'boat', 'Update Boat - ' . $d['boname'] . '<br/>' . implode( '<br/>', $logs ) );
        }

        sync_boat_attachment( $_POST['boid'] );

        return $_POST['boid'];
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Boat
| -------------------------------------------------------------------------------------
*/
function delete_boat( $boid = '', $is_ajax = false )
{
    global $db;

    //-- Get Boat
    $data = get_boat( $boid );

    //-- Get list boat attachment 
    //-- before deleted this boat
    $attach = array();

    $s = 'SELECT * FROM ticket_boat_attachment WHERE boid = %d ORDER BY border_id';          
    $q = $db->prepare_query( $s, $boid );
    $r = $db->do_query( $q );

    while( $d = $db->fetch_array( $r ) )
    {
        $attach[] = $d;
    }

    //-- Delete boat
    $s = 'DELETE FROM ticket_boat WHERE boid = %d';          
    $q = $db->prepare_query( $s, $boid );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return $r;
        }
        else
        {
            return header( 'location:' . get_state_url( 'boat&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        //-- Delete boat attachment 
        //-- base on img array
        if( !empty( $attach ) )
        {
            foreach( $attach as $att )
            {
                if( file_exists( ROOT_PATH . $att['baloc_thumb'] ) )
                {
                    unlink( ROOT_PATH . $att['baloc_thumb'] );
                }

                if( file_exists( ROOT_PATH . $att['baloc_medium'] ) )
                {
                    unlink( ROOT_PATH . $att['baloc_medium'] );
                }

                if( file_exists( ROOT_PATH . $att['baloc_large'] ) )
                {
                    unlink( ROOT_PATH . $att['baloc_large'] );
                }

                if( file_exists( ROOT_PATH . $att['baloc'] ) )
                {
                    unlink( ROOT_PATH . $att['baloc'] );
                }
            }
        }

        save_log( $boid, 'boat', 'Delete boat - ' . $data['boname'] );

        return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Boat By baid
| -------------------------------------------------------------------------------------
*/
function get_boat_attachment( $id='' )
{
    global $db;

    $s = 'SELECT * FROM ticket_boat_attachment WHERE baid = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    return $db->fetch_array( $r );
}

/*
| -------------------------------------------------------------------------------------
| Get Boat Gallery
| -------------------------------------------------------------------------------------
*/
function get_boat_attachment_gallery( $boid='' )
{
    global $db;

    $s = 'SELECT * FROM ticket_boat_attachment WHERE boid = %d ORDER BY border_id';          
    $q = $db->prepare_query( $s, $boid );
    $r = $db->do_query( $q );

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'id' => $d['baid'],
            'title' => $d['batitle'],
            'alt_txt' => $d['baalt_txt'],
            'caption' => $d['bacaption'],
            'mime_type' => $d['bamime_type'],
            'original' => HTSERVER . site_url() . $d['baloc'],
            'large' => HTSERVER . site_url() . $d['baloc_large'],
            'medium' => HTSERVER . site_url() . $d['baloc_medium'],
            'thumbnail' => HTSERVER . site_url() . $d['baloc_thumb'],
        );
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Boat First Attachment
| -------------------------------------------------------------------------------------
*/
function get_boat_first_attachment( $boid='' )
{
    $attachment = get_boat_attachment_gallery( $boid );

    if( empty( $attachment ) )
    {
        return array(
            'id' => '',
            'title' => '',
            'alt_txt' => '',
            'caption' => '',
            'mime_type' => '',
            'original' => 'https://dummyimage.com/16:9x1080/DDDDDD/333333',
            'large' => 'https://dummyimage.com/1024x4:3/DDDDDD/333333',
            'medium' => 'https://dummyimage.com/700x4:3/DDDDDD/333333',
            'thumbnail' => 'https://dummyimage.com/150x150/DDDDDD/333333',
        );
    }
    else
    {
        return $attachment[0];
    }
}

/*
| -------------------------------------------------------------------------------------
| Save Boat Picture Into Database
| -------------------------------------------------------------------------------------
*/
function save_boat_attachment( $loc='', $loc_thumb='', $loc_medium='', $loc_large='', $mime_type='', $title='', $alt_txt='', $caption='', $order_id=1, $boid = null )
{
    global $db;
    
    $title    = rem_slashes( $title );
    $alt_text = rem_slashes( $alt_txt );
    $caption  = rem_slashes( $caption );

    if( empty( $boid) )
    {
        $s = 'INSERT INTO ticket_boat_attachment(
                baloc,
                baloc_thumb,
                baloc_medium,
                baloc_large,
                batitle,
                baalt_txt,
                bacaption,
                bamime_type,
                border_id)
             VALUES( %s, %s ,%s, %s, %s, %s, %s, %s, %d )';
        $q = $db->prepare_query( $s,
                $loc,
                $loc_thumb,
                $loc_medium,
                $loc_large,
                $title,
                $alt_txt,
                $caption,
                $mime_type,
                $order_id);
    }
    else
    {
        $s = 'INSERT INTO ticket_boat_attachment(
                boid,
                baloc,
                baloc_thumb,
                baloc_medium,
                baloc_large,
                batitle,
                baalt_txt,
                bacaption,
                bamime_type,
                border_id)
             VALUES( %d, %s, %s ,%s, %s, %s, %s, %s, %s, %d )';
        $q = $db->prepare_query( $s,
                $boid,
                $loc,
                $loc_thumb,
                $loc_medium,
                $loc_large,
                $title,
                $alt_txt,
                $caption,
                $mime_type,
                $order_id);
    }
    
    if( reset_boat_attachment_order() )
    {
        if( $db->do_query( $q ) )
        {
            return $db->insert_id();
        }
    }

    return false;
}

/*
| -------------------------------------------------------------------------------------
| Synchronize Boat Picture In Database
| -------------------------------------------------------------------------------------
*/
function sync_boat_attachment( $boid )
{
    global $db;

    if( isset( $_POST['boattachment'] ) )
    {
        foreach( $_POST['boattachment'] as $baid )
        {
            $s = 'UPDATE ticket_boat_attachment SET boid = %d WHERE baid = %d';
            $q = $db->prepare_query( $s, $boid, $baid );
            $r = $db->do_query( $q );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Reset Boat Picture Order
| -------------------------------------------------------------------------------------
*/
function reset_boat_attachment_order()
{
    global $db;

    $s = 'UPDATE ticket_boat_attachment SET border_id = border_id + 1';
    $q = $db->prepare_query( $s );

    return $db->do_query( $q );
}

/*
| -------------------------------------------------------------------------------------
| Get Boat Picture
| -------------------------------------------------------------------------------------
*/
function get_boat_pictures()
{
    global $db;

    if( isset( $_POST['boid'] ) )
    {
        $gallery = get_boat_attachment_gallery( $_POST['boid'] );

        if( empty( $gallery ) )
        {
            return array( 'result'=>'failed' );
        }
        else
        {
            return array( 'result'=>'success', 'gallery' => $gallery );
        }
    }
    else
    {
         return array( 'result'=>'failed' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Upload Boat Picture
| -------------------------------------------------------------------------------------
*/
function upload_boat_pictures()
{
    if( isset( $_FILES['file'] ) && $_FILES['file']['error'] == 0 )
    {
        $file_name   = $_FILES['file']['name'];
        $file_size   = $_FILES['file']['size'];
        $file_type   = $_FILES['file']['type'];
        $file_source = $_FILES['file']['tmp_name'];
                        
        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {        
                $file_ext    = file_name_filter( $file_name, true );
                $sef_url     = file_name_filter( $file_name ) . '-'. time();
                $file_name_n = $sef_url . $file_ext;
                $file_name_m = $sef_url . '-medium' . $file_ext;
                $file_name_l = $sef_url . '-large' . $file_ext;
                $file_name_t = $sef_url . '-thumb' . $file_ext;

                if( !file_exists( PLUGINS_PATH . '/ticket/uploads/boat/' ) )
                {
                    mkdir( PLUGINS_PATH . '/ticket/uploads/boat/');
                }
                    
                $dest   = PLUGINS_PATH . '/ticket/uploads/boat/' . $file_name_n;
                $dest_m = PLUGINS_PATH . '/ticket/uploads/boat/' . $file_name_m;
                $dest_l = PLUGINS_PATH . '/ticket/uploads/boat/' . $file_name_l;
                $dest_t = PLUGINS_PATH . '/ticket/uploads/boat/' . $file_name_t;
                
                $dest_save   = '/l-plugins/ticket/uploads/boat/' . $file_name_n;
                $dest_save_m = '/l-plugins/ticket/uploads/boat/' . $file_name_m;
                $dest_save_l = '/l-plugins/ticket/uploads/boat/' . $file_name_l;
                $dest_save_t = '/l-plugins/ticket/uploads/boat/' . $file_name_t;

                upload_crop( $file_source, $dest_t, $file_type, 300, 300 );
                upload_resize( $file_source, $dest_m, $file_type, 700, 700 );
                upload_resize( $file_source, $dest_l, $file_type, 1024, 1024 );
                upload( $file_source, $dest );
                
                $attach_id = save_boat_attachment( $dest_save, $dest_save_t, $dest_save_m, $dest_save_l, $file_type, $_POST['boid'] );

                if( $attach_id )
                {
                    return array( 'result'=>'success', 'id'=>$attach_id, 'thumb'=>'http//' . site_url() . $dest_save_t );
                }
                else
                {
                    return array( 'result'=>'failed', 'message'=>'Something wrong' );
                }
           }
           else
           {
                return array( 'result'=>'error', 'message'=>'File type must be image (jpg, png, gif)' );
           }
        }
        else
        {
            return array( 'result'=>'error', 'message'=>'The maximum file size is 2MB' );
        }
    }
    else
    {
        return array( 'result'=>'error', 'message'=>'Image file is empty' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Boat Picture
| -------------------------------------------------------------------------------------
*/
function delete_boat_pictures( $baid = '' )
{
    global $db;

    $baid = isset( $_POST['att_id'] ) ? $_POST['att_id'] : $baid;

    if( !empty( $baid ) )
    {
        $attachment = get_boat_attachment( $baid );

        if( empty( $attachment ) )
        {
            return false;
        }
        else
        {
            $s = 'DELETE FROM ticket_boat_attachment WHERE baid = %d';          
            $q = $db->prepare_query( $s, $_POST['att_id'] );
               
            if( $db->do_query( $q ) )
            {
                if( file_exists( ROOT_PATH . $attachment['baloc_thumb'] ) )
                {
                    unlink( ROOT_PATH . $attachment['baloc_thumb'] );
                }

                if( file_exists( ROOT_PATH . $attachment['baloc_medium'] ) )
                {
                    unlink( ROOT_PATH . $attachment['baloc_medium'] );
                }

                if( file_exists( ROOT_PATH . $attachment['baloc_large'] ) )
                {
                    unlink( ROOT_PATH . $attachment['baloc_large'] );
                }

                if( file_exists( ROOT_PATH . $attachment['baloc'] ) )
                {
                    unlink( ROOT_PATH . $attachment['baloc'] );
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }       
}

/*
| -------------------------------------------------------------------------------------
| Get Boat Status Option
| -------------------------------------------------------------------------------------
*/
function get_boat_status_option( $status='' )
{
	$option = '
	<option value="">Select Boat Status</option>
	<option value="1" ' . ( $status=='1' ? 'selected' : '' ) . '>Ready</option>
	<option value="2" ' . ( $status=='2' ? 'selected' : '' ) . '>Maintenance</option>';

	return $option;
}

/*
| -------------------------------------------------------------------------------------
| Get Boat Option
| -------------------------------------------------------------------------------------
*/
function get_boat_option( $boid = '', $use_empty = true, $empty_text = 'Select Boat' )
{
    $boat   = get_boat_list();
    $option = $use_empty ? '<option value="">' . $empty_text . '</option>' : '';

    if( !empty( $boat ) )
    {
        foreach( $boat as $d )
        {
            $option .= '<option value="' . $d['boid'] . '" ' . ( $d['boid'] == $boid ? 'selected' : '' ) . ' >' . $d['boname'] . '</option>';   
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Ajax Functions
| -------------------------------------------------------------------------------------
*/
function ticket_boat_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-boat' )
    {
        $d = delete_boat( $_POST['boid'], true );

        if( $d === true )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( $d );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-boat-pictures' )
    {
        $data = get_boat_pictures();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-boat-pictures' )
    {
        $data = upload_boat_pictures();

        echo json_encode( $data );
    }
    
    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-boat-pictures' )
    {
        if( delete_boat_pictures() )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

?>