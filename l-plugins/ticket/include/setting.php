<?php

add_apps_privileges( 'ticket', 'Ticket Setting' );

add_actions( 'tab_admin_aditional', 'ticket_view_setting', array( 'ticket' => 'Other Setting' ) );
add_actions( 'tab_admin_aditional', 'ticket_payment_method_view_setting', array( 'payment-method' => 'Payment Method' ) );
add_actions( 'tab_admin_aditional', 'ticket_backup_restore_view_setting', array( 'backup-restore' => 'Backup and Restore' ) );

add_actions( 'ticket-setting-ajax_page', 'ticket_setting_ajax' );
add_actions( 'ajax-backup-restore_page', 'ajax_backup_restore_request' );

/*
| -------------------------------------------------------------------------------------
| Setting - Backup and Restore
| -------------------------------------------------------------------------------------
*/

function ajax_backup_restore_request()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'backup-process' )
    {
        $result = data_backup_process();

        echo json_encode( array( 'result' => $result ) );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'restore-process' )
    {
        extract( $_POST );

        $result = data_restore_process( $pmfile );

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-file' )
    {
        extract( $_POST );

        $result = delete_file_backup( $pmfile );

        echo json_encode( $result );
    }
}

function ticket_backup_restore_view_setting()
{
    $site_url = site_url();

    set_template( PLUGINS_PATH . '/ticket/tpl/backup/backuprestore.html', 'backup-restore' );

    add_block( 'backuprestore', 'b-block', 'backup-restore' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'list', data_backup_list() );
    add_variable( 'title', 'Backup and Restore' );
    add_variable( 'site_link', HTSERVER . $site_url );
    add_variable( 'ajax_process', HTSERVER . $site_url . '/ajax-backup-restore' );

    add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'backuprestore', 'b-block', false );
    
    return return_template( 'backup-restore' );
}

function data_backup_list()
{
    $dirlist = list_file_backup();
    
    if( empty( $dirlist ) )
    {
        return '';
    }

    set_template( PLUGINS_PATH . '/ticket/tpl/backup/loop.html', 'backup-loop' );

    add_block( 'loop-block', 'bcblock', 'backup-loop' );

    foreach( $dirlist as $file )
    {
        add_variable( 'bsize', $file['size'] );
        add_variable( 'bname', substr($file['name'], -28) );
        add_variable( 'bdate', date("d/m/Y H:i:s", $file['lastmod']) );
        add_variable( 'bid', str_replace(".", "", substr($file['name'], -28)));
        add_variable( 'downlod_link', HTSERVER . site_url() . '/backup/' . substr($file['name'], -28) );
        add_variable( 'restore_link', HTSERVER . site_url() . '/ajax-backup-restore' );
        add_variable( 'delete_link', HTSERVER . site_url() . '/ajax-backup-restore' );

        parse_template( 'loop-block', 'bcblock', true );
    }

    return return_template( 'backup-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Setting - System setting
| -------------------------------------------------------------------------------------
*/
function ticket_payment_method_view_setting()
{
    $site_url = site_url();
    $alert    = '';

    if( isset( $_POST['save_changes'] ) )
    {
        $update = set_ticket_setting();
        
        if( $update )
        {
            $alert = '
            <div class="alert_green_form">
                Your setting has been updated.
            </div>';
        }
        else
        {
            $alert = '
            <div class="alert_green_form">
                Your setting failed to update.
            </div>';
        }
    }
    
    set_template( PLUGINS_PATH . '/ticket/tpl/setting/form.html', 'payment-method' );

    add_block( 'form-block', 'f-block', 'payment-method' );
    add_block( 'method-form-block', 'mf-block', 'payment-method' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'title', 'Payment Method' );
    add_variable( 'site_link', HTSERVER . $site_url );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-setting-ajax' );

    add_actions( 'header_elements', 'get_custom_css', HTSERVER. $site_url .'/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    parse_template( 'method-form-block', 'mf-block', false );

    return return_template( 'payment-method' );
}

function ticket_view_setting()
{
    $site_url = site_url();
    $alert    = '';

    if( isset( $_POST['save_changes'] ) )
    {
        $update = set_ticket_setting();
        
        if( $update )
        {
            $alert = '
            <div class="alert_green_form">
                Your setting has been updated.
            </div>';
        }
        else
        {
            $alert = '
            <div class="alert_green_form">
                Your setting failed to update.
            </div>';
        }
    }

    $doku_url = get_meta_data( 'doku_url', 'ticket_setting' );
    
    set_template( PLUGINS_PATH . '/ticket/tpl/setting/form.html', 'setting' );

    add_block( 'form-block', 'f-block', 'setting' );
    add_block( 'method-form-block', 'mf-block', 'setting' );

    add_variable( 'title', 'Ticket Setting' );
    add_variable( 'site_link', HTSERVER . $site_url );
    add_variable( 'ajax_link', HTSERVER . $site_url . '/ticket-setting-ajax' );
    add_variable( 'default_booking_email', get_meta_data( 'default_booking_email', 'ticket_setting' ) );
    add_variable( 'feedback_email_content', get_meta_data( 'feedback_email_content', 'ticket_setting' ) );
    add_variable( 'term_and_condition_content', get_meta_data( 'term_and_condition_content', 'ticket_setting' ) );
    add_variable( 'reconfirmation_email_content', get_meta_data( 'reconfirmation_email_content', 'ticket_setting' ) );

    add_variable( 'reschedule_email_single_guest_subject', get_meta_data( 'reschedule_email_single_guest_subject', 'ticket_setting' ) );
    add_variable( 'reschedule_email_multi_guest_subject', get_meta_data( 'reschedule_email_multi_guest_subject', 'ticket_setting' ) );
    add_variable( 'force_majeure_email_single_guest_subject', get_meta_data( 'force_majeure_email_single_guest_subject', 'ticket_setting' ) );
    add_variable( 'force_majeure_email_multi_guest_subject', get_meta_data( 'force_majeure_email_multi_guest_subject', 'ticket_setting' ) );
    add_variable( 'cancellation_email_single_guest_subject', get_meta_data( 'cancellation_email_single_guest_subject', 'ticket_setting' ) );
    add_variable( 'cancellation_email_multi_guest_subject', get_meta_data( 'cancellation_email_multi_guest_subject', 'ticket_setting' ) );
    add_variable( 'recheck_reservation_email_subject', get_meta_data( 'recheck_reservation_email_subject', 'ticket_setting' ) );

    add_variable( 'reschedule_email_single_guest_content', get_meta_data( 'reschedule_email_single_guest_content', 'ticket_setting' ) );
    add_variable( 'reschedule_email_multi_guest_content', get_meta_data( 'reschedule_email_multi_guest_content', 'ticket_setting' ) );
    add_variable( 'force_majeure_email_single_guest_content', get_meta_data( 'force_majeure_email_single_guest_content', 'ticket_setting' ) );
    add_variable( 'force_majeure_email_multi_guest_content', get_meta_data( 'force_majeure_email_multi_guest_content', 'ticket_setting' ) );
    add_variable( 'cancellation_email_single_guest_content', get_meta_data( 'cancellation_email_single_guest_content', 'ticket_setting' ) );
    add_variable( 'cancellation_email_multi_guest_content', get_meta_data( 'cancellation_email_multi_guest_content', 'ticket_setting' ) );
    add_variable( 'recheck_reservation_email_content', get_meta_data( 'recheck_reservation_email_content', 'ticket_setting' ) );
    add_variable( 'recheck_reservation_email_interval', get_meta_data( 'recheck_reservation_email_interval', 'ticket_setting' ) );

    add_variable( 'paypal_client_id', get_meta_data( 'paypal_client_id', 'ticket_setting' ) );
    add_variable( 'paypal_client_secret', get_meta_data( 'paypal_client_secret', 'ticket_setting' ) );
    add_variable( 'background_img', get_background_image() );
    add_variable( 'staging_doku_url_checked', empty( $doku_url ) ? 'checked' : '' );
    add_variable( 'live_doku_url_checked', empty( $doku_url ) ? '' : 'checked' );
    add_variable( 'doku_mall_id', get_meta_data( 'doku_mall_id', 'ticket_setting' ) );
    add_variable( 'doku_shared_key', get_meta_data( 'doku_shared_key', 'ticket_setting' ) );
    add_variable( 'mandrill_api_key', get_meta_data( 'mandrill_api_key', 'ticket_setting' ) );
    add_variable( 'nicepay_imid', get_meta_data( 'nicepay_imid', 'ticket_setting' ) );
    add_variable( 'nicepay_merchant_key', get_meta_data( 'nicepay_merchant_key', 'ticket_setting' ) );
    add_variable( 'nicepay_callback_url', get_meta_data( 'nicepay_callback_url', 'ticket_setting' ) );
    add_variable( 'nicepay_dbprocess_url', get_meta_data( 'nicepay_dbprocess_url', 'ticket_setting' ) );

    add_variable( 'mailchimp_list_id', get_meta_data( 'mailchimp_list_id', 'ticket_setting' ) );
    add_variable( 'mailchimp_api_key', get_meta_data( 'mailchimp_api_key', 'ticket_setting' ) );    

    add_actions( 'header_elements', 'get_css', 'dropzone.css' );
    add_actions( 'header_elements', 'get_custom_css', HTSERVER . $site_url . '/l-plugins/ticket/css/admin.css?v=' . TICKET_VERSION );

    add_actions( 'header_elements', 'get_javascript', 'dropzone.js' );
    add_actions( 'header_elements', 'get_javascript_inc', 'tiny_mce/tinymce.min.js' );
    
    parse_template( 'form-block', 'f-block', false );

    return return_template( 'setting' );
}

function is_num_ticket_setting( $key )
{
    global $db;
    
    $s = 'SELECT * FROM l_meta_data WHERE lmeta_name=%s AND lapp_name=%s';
    $q = $db->prepare_query( $s, $key, 'ticket_setting' );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    if( $n > 0 )
    {
        return true;
    }

    return false;
}

function set_ticket_setting()
{
    global $db;
    
    foreach( $_POST as $key => $val )
    {
        if( !in_array( $key, array( 'save_changes', 'fimage', 'bimage' ) ) )
        {
            if( is_num_ticket_setting( $key ) )
            {
                $s = 'UPDATE l_meta_data SET lmeta_value=%s
                      WHERE lmeta_name=%s AND lapp_name=%s';
                $q = $db->prepare_query( $s, $val, $key, 'ticket_setting' );
                $r = $db->do_query( $q );
            }
            else
            {
                $s = 'INSERT INTO l_meta_data(
                        lmeta_name,
                        lmeta_value,
                        lapp_name)
                      VALUES(%s, %s, %s)';
                $q = $db->prepare_query( $s, $key, $val, 'ticket_setting' );
                $r = $db->do_query( $q );
            }
        }
    }
    
    return true;
}

/*
| -------------------------------------------------------------------------------------
| Get Footer Picture
| -------------------------------------------------------------------------------------
*/
function get_ticket_footer_pictures()
{
    $meta_value = get_meta_data( 'fimage', 'ticket_setting' );

    if( !empty( $meta_value ) )
    {
        $arr = json_decode( $meta_value, true );
        $url = site_url();

        if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
        {
            $image = array();

            foreach( $arr as $d )
            {
                $image[] = array( 
                    'filename' => $d, 
                    'link' => HTSERVER . $url . '/l-functions/mthumb.php?src=' . HTSERVER . $url . '/l-plugins/ticket/uploads/footer/' . $d . '&w=120&h=120'
                );
            }

            return array( 'result'=>'success', 'image' => $image );
        }
        else
        {
            return array( 'result'=>'failed' );
        }
    }    
    else
    {
        return array( 'result'=>'failed' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Upload Footer Picture
| -------------------------------------------------------------------------------------
*/
function upload_footer_pictures()
{
    if( isset( $_FILES['file'] ) && $_FILES['file']['error'] == 0 )
    {
        $file_name   = $_FILES['file']['name'];
        $file_size   = $_FILES['file']['size'];
        $file_type   = $_FILES['file']['type'];
        $file_source = $_FILES['file']['tmp_name'];
                        
        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {
                $file_ext  = file_name_filter( $file_name, true );
                $sef_url   = file_name_filter( $file_name ) . '-'. time();
                $file_name = $sef_url . $file_ext;

                if( !file_exists( PLUGINS_PATH . '/ticket/uploads/footer/' ) )
                {
                    mkdir( PLUGINS_PATH . '/ticket/uploads/footer/');
                }

                upload( $file_source, PLUGINS_PATH . '/ticket/uploads/footer/' . $file_name );
                
                $meta_value = get_meta_data( 'fimage', 'ticket_setting' );

                if( empty( $meta_value ) )
                {
                    $meta_value = json_encode( array( $file_name ) );
                }
                else
                {
                    $arr = json_decode( $meta_value, true );

                    if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
                    {
                        $arr[] = $file_name;

                        $meta_value = json_encode( $arr );
                    }
                    else
                    {
                        $meta_value = '';
                    }
                }

                if( update_meta_data( 'fimage', $meta_value, 'ticket_setting' ) )
                {
                    return array( 'result' => 'success', 'filename' => $file_name );
                }
                else
                {
                    return array( 'result' => 'failed', 'message' => 'Something wrong' );
                }
           }
           else
           {
                return array( 'result' =>' error', 'message' => 'File type must be image (jpg, png, gif)' );
           }
        }
        else
        {
            return array( 'result' =>' error', 'message' => 'The maximum file size is 2MB' );
        }
    }
    else
    {
        return array( 'result' => 'error', 'message' => 'Image file is empty' );
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete Footer Picture
| -------------------------------------------------------------------------------------
*/
function delete_footer_pictures()
{
    $meta_value = get_meta_data( 'fimage', 'ticket_setting' );

    if( !empty( $meta_value ) )
    {
        $arr = json_decode( $meta_value, true );

        if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
        {
            if( ( $key = array_search( $_POST['file'], $arr ) ) !== false )
            {
                unset( $arr[$key] );

                if( empty( $arr ) )
                {
                    $meta_value = '';
                }
                else
                {
                    $meta_value = json_encode( array_values( $arr ) );
                }

                if( update_meta_data( 'fimage', $meta_value, 'ticket_setting' ) )
                {
                    if( file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/footer/' . $_POST['file'] ) )
                    {
                        unlink( ROOT_PATH . '/l-plugins/ticket/uploads/footer/' . $_POST['file'] );
                    }

                    return array( 'result' => 'success' );
                }
                else
                {
                    return array( 'result' => 'failed' );
                }
            }
            else
            {
                return array( 'result' => 'failed' );
            }
        }
        else
        {
            return array( 'result' => 'failed' );
        }
    }
    else
    {
        return array( 'result' => 'failed' );
    }
}

function get_payment_method_list()
{
    global $db;

    $rdata = $_REQUEST;
    $cols  = array( 0 => 'mid', 1 => 'mname' );

    if( empty( $rdata['search']['value']) )
    {
        $s = 'SELECT * FROM ticket_booking_payment_method WHERE mid NOT IN( 1, 2, 3, 4, 5 ) ORDER BY ' . $cols[$rdata['order'][0]['column']] . ' ' . $rdata['order'][0]['dir'];
        $q = $db->prepare_query( $s );

        $s2 = 'SELECT * FROM ticket_booking_payment_method WHERE mid NOT IN( 1, 2, 3, 4, 5 ) ORDER BY ' . $cols[$rdata['order'][0]['column']] . ' ' . $rdata['order'][0]['dir'] . '  LIMIT %d, %d';
        $q2 = $db->prepare_query( $s2, $rdata['start'], $rdata['length'] );
    }
    else
    {
        $s = 'SELECT * FROM ticket_booking_payment_method WHERE mid NOT IN( 1, 2, 3, 4, 5 ) AND mname LIKE %s ORDER BY ' . $cols[$rdata['order'][0]['column']] . ' ' . $rdata['order'][0]['dir'];
        $q = $db->prepare_query( $s, '%' . $rdata['search']['value'] . '%' );

        $s2 = 'SELECT * FROM ticket_booking_payment_method WHERE mid NOT IN( 1, 2, 3, 4, 5 ) AND mname LIKE %s ORDER BY ' . $cols[$rdata['order'][0]['column']] . ' ' . $rdata['order'][0]['dir'] . '  LIMIT %d, %d';
        $q2 = $db->prepare_query( $s2, '%' . $rdata['search']['value'] . '%', $rdata['start'], $rdata['length'] );
    }

    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );

    if( $n2 > 0 )
    {
        $data = array();

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = $d2;
        }

        $result = array(
            'draw' => intval( $rdata['draw'] ),
            'recordsTotal' => intval( $n ),
            'recordsFiltered' => intval( $n ),
            'data' => $data
        );

        return $result;
    }
}

function get_payment_method()
{
    global $db;
    
    $s = 'SELECT * FROM ticket_booking_payment_method';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    $data = array();

    if( $n > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data[] = $d;
        }
    }

    return $data;
}

function get_payment_method_by_id( $id, $field = '' )
{
    global $db;
    
    $s = 'SELECT * FROM ticket_booking_payment_method WHERE mid = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    if( $n > 0 )
    {
        $d = $db->fetch_array( $r );

        return $d['mname'];
    }
}

function get_payment_method_by_name( $name, $field = '' )
{
    global $db;
    
    $s = 'SELECT * FROM ticket_booking_payment_method WHERE mname = %s';
    $q = $db->prepare_query( $s, $name );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    if( $n > 0 )
    {
        $d = $db->fetch_array( $r );

        if( empty( $field ) )
        {
            return $d;
        }
        else
        {
            return $d[ $field ];
        }
    }
}

function add_new_payment_method()
{
    global $db;

    $s = 'INSERT INTO ticket_booking_payment_method(mname, mcreateddate, luser_id) VALUES(%s, %s, %s)';
    $q = $db->prepare_query( $s, $_POST['mname'], date( 'Y-m-d H:i:s' ), $_COOKIE['user_id'] );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        return array( 'result' => 'failed', 'message' => $r );
    }
    else
    {
        $id = $db->insert_id();

        save_log( $id, 'payment-method', 'Add new payment method - ' . $_POST['mname'] );

        return array( 'result' => 'success' );
    }
}

function edit_payment_method()
{
    global $db;

    $mname = get_payment_method_by_id( $_POST['mid'] );

    $s = 'UPDATE ticket_booking_payment_method SET mname = %s WHERE mid = %d';
    $q = $db->prepare_query( $s, $_POST['mname'], $_POST['mid'] );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        return array( 'result' => 'failed', 'message' => $r );
    }
    else
    {
        if( $mname != $_POST['mname'] )
        {
            save_log( $_POST['mid'], 'payment-method', 'Update payment method - ' . $mname . ' to ' . $_POST['mname'] );
        }
        else
        {
            save_log( $_POST['mid'], 'payment-method', 'Update payment method - ' . $_POST['mname'] );
        }

        return array( 'result' => 'success' );
    }
}

function delete_payment_method( $id = '', $is_ajax = false )
{
    global $db;

    $mname = get_payment_method_by_id( $id );

    $s = 'DELETE FROM ticket_booking_payment_method WHERE mid = %d';          
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    
    if( isset( $r['error_code'] ) )
    {
        if( $is_ajax )
        {
            return array( 'result' => 'failed', 'message' => $r );
        }
        else
        {
            return header( 'location:' . get_state_url( 'global_settings&tab=payment-method&error-query=' . base64_encode( json_encode( $r ) ) ) );
        }
    }
    else
    {
        save_log( $id, 'payment-method', 'Delete payment method - ' . $mname );

        return array( 'result' => 'success' );
    }
}

function upload_background_image()
{
    global $db;

    if( isset( $_FILES['background'] ) && $_FILES['background']['error'] == 0 )
    {
        $file_name   = $_FILES['background']['name'];
        $file_size   = $_FILES['background']['size'];
        $file_type   = $_FILES['background']['type'];
        $file_source = $_FILES['background']['tmp_name'];
                        
        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {        
                $file_ext    = file_name_filter( $file_name, true );
                $sef_url     = file_name_filter( $file_name ) . '-'. time();
                $file_name_n = $sef_url . $file_ext;
                $file_name_l = $sef_url . '-large' . $file_ext;
                $file_name_m = $sef_url . '-medium' . $file_ext;

                if( !file_exists( PLUGINS_PATH . '/ticket/uploads/background/' ) )
                {
                    mkdir( PLUGINS_PATH . '/ticket/uploads/background/');
                }
                    
                $dest   = PLUGINS_PATH . '/ticket/uploads/background/' . $file_name_n;
                $dest_l = PLUGINS_PATH . '/ticket/uploads/background/' . $file_name_l;
                $dest_m = PLUGINS_PATH . '/ticket/uploads/background/' . $file_name_m;

                if( upload_resize( $file_source, $dest_l, $file_type, 1920, 1920 ) )
                {
                    if( upload_resize( $file_source, $dest_m, $file_type, 500, 500 ) )
                    {
                        if( upload( $file_source, $dest ) )
                        {
                            $ovalue = get_meta_data( 'bimage', 'ticket_setting', 0 );
                            $mvalue = json_encode( array( 'large' => $file_name_l, 'medium' => $file_name_m,  'original' => $file_name_n ) );

                            if( update_meta_data( 'bimage', $mvalue, 'ticket_setting' ) )
                            {
                                $arr = json_decode( $ovalue, true );

                                if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
                                {
                                    if( isset( $arr['large'] ) && !empty( $arr['large'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['large'] ) )
                                    {
                                        unlink( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['large'] );
                                    }

                                    if( isset( $arr['medium'] ) && !empty( $arr['medium'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['medium'] ) )
                                    {
                                        unlink( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['medium'] );
                                    }

                                    if( isset( $arr['original'] ) && !empty( $arr['original'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['original'] ) )
                                    {
                                        unlink( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['original'] );
                                    }
                                }
                            }
                        }
                    }

                    return array( 'result' => 'success', 'filename' => $file_name_m );
                }
                else
                {
                    return array( 'result' =>' error', 'message' => 'Something wrong' );
                }
            }
            else
            {
                return array( 'result' =>' error', 'message' => 'File type must be image (jpg, png, gif)' );
            }
        }
        else
        {
            return array( 'result' =>' error', 'message' => 'The maximum file size is 2MB' );
        }
    }
    else
    {
        return array( 'result' => 'error', 'message' => 'Image file is empty' );
    }
}

function delete_background_image()
{
    $ovalue = get_meta_data( 'bimage', 'ticket_setting', 0 );

    if( update_meta_data( 'bimage', '', 'ticket_setting' ) )
    {
        if( !empty( $ovalue ) )
        {
            $arr = json_decode( $ovalue, true );

            if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
            {
                if( isset( $arr['large'] ) && !empty( $arr['large'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['large'] ) )
                {
                    unlink( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['large'] );
                }

                if( isset( $arr['medium'] ) && !empty( $arr['medium'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['medium'] ) )
                {
                    unlink( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['medium'] );
                }

                if( isset( $arr['original'] ) && !empty( $arr['original'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['original'] ) )
                {
                    unlink( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['original'] );
                }
            }
        }

        return array( 'result' => 'success' );
    }
    else
    {
        return array( 'result' => 'failed' );
    }
}

function get_background_image()
{
    $mvalue = get_meta_data( 'bimage', 'ticket_setting', 0 );

    $arr = json_decode( $mvalue, true );

    if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
    {
        if( isset( $arr['medium'] ) && !empty( $arr['medium'] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr['medium'] ) )
        {
            return '
            <div class="background-overlay">
                <a class="delete-background"></a>
            </div>
            <div class="background-img">
                <img src="' . HTSERVER . site_url() . '/l-plugins/ticket/uploads/background/' . $arr['medium'] . '" alt="Background Image" />
            </div>';
        }
    }
}

function get_background_image_url( $size = 'large' )
{
    $mvalue = get_meta_data( 'bimage', 'ticket_setting', 0 );

    $arr = json_decode( $mvalue, true );

    if( $arr !== null && json_last_error() === JSON_ERROR_NONE )
    {
        if( isset( $arr[ $size ] ) && !empty( $arr[ $size ] ) && file_exists( ROOT_PATH . '/l-plugins/ticket/uploads/background/' . $arr[ $size ] ) )
        {
            return HTSERVER . site_url() . '/l-plugins/ticket/uploads/background/' . $arr[ $size ];
        }
        else
        {
            return HTSERVER . TEMPLATE_URL . '/images/first-bg.jpeg';
        }
    }
    else
    {
        return HTSERVER . TEMPLATE_URL . '/images/first-bg.jpeg';
    }
}

function return_email_setting( $name, $param )
{
    $value = get_meta_data( $name, 'ticket_setting', 0 );

    if( !empty( $value ) )
    {
        preg_match_all( '/\[([a-zA-Z0-9-_]+?)\]/', $value, $output );
    
        foreach( $output[1] as $obj )
        {
            $value = str_replace( '['.$obj.']', run_email_shortcode( $obj, $param ), $value );
        }
    }

    return $value;
}

function run_email_shortcode( $code, $param=array() )
{
    if( isset( $param[$code] ) )
    {
        if( $code == 'feedback_link' )
        {
            return '<a href="' . $param[$code] . '" style="text-decoration:none; background-color:rgb(198, 216, 220); border: 1px solid rgb(62, 117, 128); color: rgb(21, 54, 76); padding:7px 15px;">Give Feedback</a>';
        }
        else
        {
            return $param[$code];
        }
    }
    else
    {
        if( $code == 'web_name' )
        {
            return get_meta_data('web_title');
        }
        else
        {
            return $code;
        }
    }
}

function ticket_setting_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-footer-pictures' )
    {
        $data = get_ticket_footer_pictures();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-footer-pictures' )
    {
        $data = upload_footer_pictures();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-footer-pictures' )
    {
        $data = delete_footer_pictures();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'load-payment-method' )
    {
        $data = get_payment_method_list();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-new-payment-method' )
    {
        $data = add_new_payment_method();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'edit-payment-method' )
    {
        $data = edit_payment_method();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-payment-method' )
    {
        $data = delete_payment_method( $_POST['pmid'], true );

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'add-background-image' )
    {
        $data = upload_background_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-background-image' )
    {
        $data = delete_background_image();

        echo json_encode( $data );
    }
}

?>